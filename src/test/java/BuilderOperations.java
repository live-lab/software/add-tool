import addtool.add.model.ADD;
import addtool.add.model.ADDBuilder;
import addtool.add.model.Vertex;
import addtool.dot2add.*;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * Test for operations used in learning.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 04.03.2021
 */
public class BuilderOperations {
    final List<Vertex> ops= Stream.of("And","Or","SAnd","SOr","Not","Nat")//,"SAnd","SOr","Not","Nat"
            .map(ADDBuilder::buildOperatorForKey)
            .collect(Collectors.toList());

    /**
     * Counts the occurrence of classes in a set of vertices
     * @param vertices A collection of vertices
     * @return The counts by Simple class Name
     */
    private static Map<String, Integer> getClassOccurrences(Collection<Vertex> vertices){
        Map<String, Integer> out=new HashMap<>();
        for(Vertex v:vertices){
            out.compute(v.getClass().getSimpleName(),(k,ov)->ov==null?1:ov+1);
        }
        return out;
    }

    /**
     * Calculates the difference in the maps. Using the absolute difference in occurrences
     * @param first A Map with occurrences
     * @param second A Map with occurrences
     * @return The sum of absolute differences in occurrence
     */
    private static int getOccurrenceDistance(Map<String, Integer> first, Map<String, Integer> second){
        Set<String> keys=new HashSet<>(first.keySet());
        keys.addAll(second.keySet());
        int diff=0;
        for(String key:keys){
            diff+=Math.abs(first.getOrDefault(key,0)-second.getOrDefault(key,0));
        }
        return diff;
    }
    @Test
    public void testOPSwitch() throws WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, IOException, WrongValueForSinkException {
        ADD m1=new DOT2ADD().importModel(new File("data/Learning/Crossover/T1.dot"));
        Map<String, Integer> occurrencesStart=getClassOccurrences(m1.getOperators());
        ADDBuilder builder1=new ADDBuilder(m1);
        List<Vertex> ops= Stream.of("And","Or","SAnd","SOr","Not","Nat")//,"SAnd","SOr","Not","Nat"
                .map(ADDBuilder::buildOperatorForKey)
                .collect(Collectors.toList());
        builder1.switchRandomOperator(new Random(),ops);

        ADD m3=builder1.get();
        Map<String, Integer> occurrencesEnd=getClassOccurrences(m3.getOperators());
        assertEquals(getOccurrenceDistance(occurrencesStart,occurrencesEnd),2);
    }
    @Test
    public void testUnaryInsert() throws WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, IOException, WrongValueForSinkException {
        ADD m1= new DOT2ADD().importModel(new File("data/Learning/Crossover/T1.dot"));
        Map<String, Integer> occurrencesStart=getClassOccurrences(m1.getOperators());

        ADDBuilder builder1=new ADDBuilder(m1);
        builder1.insertUnary(new Random(),ops);
        builder1.removeUnary(new Random());
        builder1.insertUnary(new Random(),ops);

        ADD m3=builder1.get();
        Map<String, Integer> occurrencesEnd=getClassOccurrences(m3.getOperators());
        assertEquals(getOccurrenceDistance(occurrencesStart,occurrencesEnd),1);
    }
    @Test
    public void testCrossOver() throws WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, IOException, WrongValueForSinkException {
        ADD m1=new DOT2ADD().importModel(new File("data/Learning/Crossover/T1.dot"));
        ADD m2=new DOT2ADD().importModel(new File("data/Learning/Crossover/T2.dot"));
        ADDBuilder builder1=new ADDBuilder(m1);
        builder1.insertUnary(new Random(),ops);
        ADDBuilder builder2=new ADDBuilder(m2);
        builder2.insertUnary(new Random(),ops);
        Set<String> m1BEIDs=m1.getBasicEventIds();
        Set<String> m2BEIDs=m2.getBasicEventIds();

        ADDBuilder.crossOver(builder1,builder2);

        ADD m3=builder1.get();
        //System.out.println(m3.getId2Vertex());
        ADD m4=builder2.get();
        //System.out.println(m4.getId2Vertex());
        Set<String> m3BEIDs=m3.getBasicEventIds();
        Set<String> m4BEIDs=m4.getBasicEventIds();
        assertEquals(m1BEIDs,m3BEIDs);
        assertEquals(m2BEIDs,m4BEIDs);
    }
}
