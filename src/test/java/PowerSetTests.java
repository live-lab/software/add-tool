import addtool.helpers.PowerSet;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Tests the function of PowerSets.
 * @author Alexander Taepper alexander.taepper@tum.de
 * @since 20.01.2020
 * @see PowerSet
 */


public class PowerSetTests {
    @Test
    public void testEmpty(){
        List<Integer> list = new LinkedList<>();
        PowerSet<Integer> powerSet = new PowerSet<>(list, 2);
        assertTrue(powerSet.hasNext());
        assertTrue(powerSet.next().isEmpty());
        assertFalse(powerSet.hasNext());
    }

    @Test
    public void testUpToZero(){
        List<Integer> list = Arrays.asList(2,3,5,7,11);
        PowerSet<Integer> powerSet = new PowerSet<>(list, 0);
        assertTrue(powerSet.hasNext());
        assertTrue(powerSet.next().isEmpty());
        assertFalse(powerSet.hasNext());
    }

    @Test
    public void testCorrectSize5UpTo2(){
        Collection<Integer> list = Arrays.asList(2,3,5,7,11);
        PowerSet<Integer> powerSet = new PowerSet<>(list, 2);
        LinkedList<List<Integer>> powerSetResult = new LinkedList<>();
        for(Set<Integer> s : powerSet){
            powerSetResult.add(new LinkedList<>(s));
        }
        assertEquals(powerSetResult.size(), 16);
        Integer[] tmp = new Integer[16];
        Arrays.fill(tmp, 1);
        List<Integer> testResults = Arrays.asList(tmp);
        for(int i = 0; i<16; i++){
            for(int num : powerSetResult.get(i)) {
                testResults.set(i, testResults.get(i)*num);
            }
        }
        int[] realResults = {1, 2, 3, 5, 7, 11, 3 * 2, 5 * 2, 5 * 3, 7 * 2, 7 * 3, 7 * 5, 11 * 2, 11 * 3, 11 * 5, 11 * 7};
        for(int realResult : realResults){
            assertTrue(testResults.contains(realResult));
        }
    }

    @Test
    public void testCorrectSize5UpTo2Min2(){
        Collection<Integer> list = Arrays.asList(2,3,5,7,11);
        PowerSet<Integer> powerSet = new PowerSet<>(list, 2, 2);
        LinkedList<List<Integer>> powerSetResult = new LinkedList<>();
        for(Set<Integer> s : powerSet){
            powerSetResult.add(new LinkedList<>(s));
        }
        assertEquals(powerSetResult.size(), 10);
        Integer[] tmp = new Integer[10];
        Arrays.fill(tmp, 1);
        List<Integer> testResults = Arrays.asList(tmp);
        for(int i = 0; i<10; i++){
            for(int num : powerSetResult.get(i)) {
                testResults.set(i, testResults.get(i)*num);
            }
        }
        int[] realResults = {3 * 2, 5 * 2, 5 * 3, 7 * 2, 7 * 3, 7 * 5, 11 * 2, 11 * 3, 11 * 5, 11 * 7};
        for(int realResult : realResults){
            assertTrue(testResults.contains(realResult));
        }
    }

    @Test
    public void testCorrectSize5UpTo3Min2(){
        Collection<Integer> list = Arrays.asList(2,3,5,7,11);
        PowerSet<Integer> powerSet = new PowerSet<>(list, 2, 3);
        LinkedList<List<Integer>> powerSetResult = new LinkedList<>();
        for(Set<Integer> s : powerSet){
            powerSetResult.add(new LinkedList<>(s));
        }
        int[] realResults = {3 * 2, 5 * 2, 5 * 3, 7 * 2, 7 * 3, 7 * 5, 11 * 2, 11 * 3, 11 * 5, 11 * 7,
                2*3*5, 2*3*7, 2*3*11, 2*5*7, 2*5*11, 2*7*11, 3*5*7, 3*5*11, 3*7*11, 5*7*11};
        int len = realResults.length;
        assertEquals(powerSetResult.size(), len);
        Integer[] tmp = new Integer[len];
        Arrays.fill(tmp, 1);
        List<Integer> testResults = Arrays.asList(tmp);
        for(int i = 0; i<len; i++){
            for(int num : powerSetResult.get(i)) {
                testResults.set(i, testResults.get(i)*num);
            }
        }
        for(int realResult : realResults){
            assertTrue(testResults.contains(realResult));
        }
    }

    @Test
    public void testCorrectSize4FullUpTo4(){
        Collection<Integer> list = Arrays.asList(2,3,5,7);
        PowerSet<Integer> powerSet = new PowerSet<>(list, 4);
        LinkedList<List<Integer>> powerSetResult = new LinkedList<>();
        for(Set<Integer> s : powerSet){
            powerSetResult.add(new LinkedList<>(s));
        }
        assertEquals(powerSetResult.size(), 16);
        Integer[] tmp = new Integer[16];
        Arrays.fill(tmp, 1);
        List<Integer> testResults = Arrays.asList(tmp);
        for(int i = 0; i<16; i++){
            for(int num : powerSetResult.get(i)) {
                testResults.set(i, testResults.get(i)*num);
            }
        }
        int[] realResults = {1, 2, 3, 5, 7, 3 * 2, 5 * 2, 5 * 3, 7 * 2, 7 * 3, 7 * 5,
                2 * 3 * 5, 2 * 3 * 7, 2 * 5 * 7, 3 * 5 * 7, 2 * 3 * 5 * 7};
        for(int realResult : realResults){
            assertTrue(testResults.contains(realResult));
        }
    }

    @Test
    public void testCorrectSize4FullUpTo4Min2(){
        Collection<Integer> list = Arrays.asList(2,3,5,7);
        PowerSet<Integer> powerSet = new PowerSet<>(list, 2, 4);
        LinkedList<List<Integer>> powerSetResult = new LinkedList<>();
        for(Set<Integer> s : powerSet){
            powerSetResult.add(new LinkedList<>(s));
        }
        assertEquals(powerSetResult.size(), 11);
        Integer[] tmp = new Integer[11];
        Arrays.fill(tmp, 1);
        List<Integer> testResults = Arrays.asList(tmp);
        for(int i = 0; i<11; i++){
            for(int num : powerSetResult.get(i)) {
                testResults.set(i, testResults.get(i)*num);
            }
        }
        int[] realResults = {3 * 2, 5 * 2, 5 * 3, 7 * 2, 7 * 3, 7 * 5,
                2 * 3 * 5, 2 * 3 * 7, 2 * 5 * 7, 3 * 5 * 7, 2 * 3 * 5 * 7};
        for(int realResult : realResults){
            assertTrue(testResults.contains(realResult));
        }
    }

    @Test
    public void testCorrectSize4Full(){
        Collection<Integer> list = Arrays.asList(2,3,5,7);
        PowerSet<Integer> powerSet = new PowerSet<>(list);
        LinkedList<List<Integer>> powerSetResult = new LinkedList<>();
        for(Set<Integer> s : powerSet){
            powerSetResult.add(new LinkedList<>(s));
        }
        assertEquals(powerSetResult.size(), 16);
        Integer[] tmp = new Integer[16];
        Arrays.fill(tmp, 1);
        List<Integer> testResults = Arrays.asList(tmp);
        for(int i = 0; i<16; i++){
            for(int num : powerSetResult.get(i)) {
                testResults.set(i, testResults.get(i)*num);
            }
        }
        int[] realResults = {1, 2, 3, 5, 7, 3 * 2, 5 * 2, 5 * 3, 7 * 2, 7 * 3, 7 * 5,
                2 * 3 * 5, 2 * 3 * 7, 2 * 5 * 7, 3 * 5 * 7, 2 * 3 * 5 * 7};
        for(int realResult : realResults){
            assertTrue(testResults.contains(realResult));
        }
    }

    @Test
    public void testCorrectSize4FullMin2(){
        Collection<Integer> list = Arrays.asList(2,3,5,7);
        PowerSet<Integer> powerSet = new PowerSet<>(list, 2, list.size());
        LinkedList<List<Integer>> powerSetResult = new LinkedList<>();
        for(Set<Integer> s : powerSet){
            powerSetResult.add(new LinkedList<>(s));
        }
        assertEquals(powerSetResult.size(), 11);
        Integer[] tmp = new Integer[11];
        Arrays.fill(tmp, 1);
        List<Integer> testResults = Arrays.asList(tmp);
        for(int i = 0; i<11; i++){
            for(int num : powerSetResult.get(i)) {
                testResults.set(i, testResults.get(i)*num);
            }
        }
        int[] realResults = {3 * 2, 5 * 2, 5 * 3, 7 * 2, 7 * 3, 7 * 5,
                2 * 3 * 5, 2 * 3 * 7, 2 * 5 * 7, 3 * 5 * 7, 2 * 3 * 5 * 7};
        for(int realResult : realResults){
            assertTrue(testResults.contains(realResult));
        }
    }
}
