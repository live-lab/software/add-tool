import addtool.add.model.ADD;
import addtool.dot2add.*;
import org.jscience.mathematics.number.Rational;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

public class GameGenBenchmarks {

    private static final String pathPrefix = "templates/";
    private static final String[] randPathPrefixes =
            {pathPrefix+"randomized_0/",
            pathPrefix+"randomized_1/",
            pathPrefix+"randomized_2/",
            pathPrefix+"randomized_3/",
            pathPrefix+"randomized_4/",
            pathPrefix+"randomized_5/",
            pathPrefix+"randomized_6/",
            pathPrefix+"randomized_7/",
            pathPrefix+"randomized_8/",
            pathPrefix+"randomized_9/"};

    private static final List<String> results = new LinkedList<>();

    private static String saveNameSuffix = "results.csv";

    /*
    Only meant for single use, uncomment if you want to create new randomized instances.
    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void randomizeADDsAndSave(String testName, String fileName, int numAttacker, int numDefender,
                          String expectedProbStr){
        try (FileReader testIn = new FileReader(pathPrefix+fileName)) {
            ADD add = ParserDOT.parseADD(testIn);
            for(int i = 0; i<10; i++) {
                add.getBasicEventsAsBEs().forEach(BasicEvent::randomize);
                File output = new File(pathPrefix + "randomized_"+i+"/"+fileName);
                output.getParentFile().mkdirs();
                ADDIOHandler.export("DOT", output, add);
            }
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n" + e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        } catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e) {
            e.printStackTrace();
        }
    }
    */

    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkOrig(String testName, String fileName, int numAttacker, int numDefender,
                       String expectedProbStr){
        saveNameSuffix = "orig_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 1, TestTactic.orig);
    }

    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkInterPOR(String testName, String fileName, int numAttacker, int numDefender,
                           String expectedProbStr){
        saveNameSuffix = "inter_por_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 10, TestTactic.inter);
    }
/*
    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkInter_randPOR(String testName, String fileName, int numAttacker, int numDefender,
                           String expectedProbStr){
        saveNameSuffix = "inter_rand_por_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 10, TestTactic.inter_random);
    } */


    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkCongPOR(String testName, String fileName, int numAttacker, int numDefender,
                          String expectedProbStr){
        saveNameSuffix = "cong_por_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 10, TestTactic.cong);
    }

    /*
    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkCong_randPOR(String testName, String fileName, int numAttacker, int numDefender,
                                String expectedProbStr){
        saveNameSuffix = "cong_rand_por_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 10, TestTactic.cong_random);
    }
    */

    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkSP(String testName, String fileName, int numAttacker, int numDefender,
                     String expectedProbStr){
        saveNameSuffix = "sp_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 10, TestTactic.sp_orig);
    }

    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkSP_inter_POR(String testName, String fileName, int numAttacker, int numDefender,
                               String expectedProbStr){
        saveNameSuffix = "sp_inter_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 10, TestTactic.sp_inter);
    }

    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkSP_cong_POR(String testName, String fileName, int numAttacker, int numDefender,
                              String expectedProbStr){
        saveNameSuffix = "sp_cong_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 10, TestTactic.sp_cong);
    }

    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkSP_confl(String testName, String fileName, int numAttacker, int numDefender,
                           String expectedProbStr){
        saveNameSuffix = "sp_confl_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 10, TestTactic.sp_confl);
    }

    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkSP_cong_POR_confl(String testName, String fileName, int numAttacker, int numDefender,
                                    String expectedProbStr){
        saveNameSuffix = "sp_cong_confl_results.csv";
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProbStr, null, 10, TestTactic.sp_cong_confl);
    }

    @Test
    public void benchmark21(){
        saveNameSuffix = "case21_results.csv";
        String testName = "file-21";
        String fileName = "data/Casestudy/Performance/_file-021.dot";
        int numAttacker = 1;
        int numDefender = 1;
        String expectedProbStr = "1/1";
        int max = doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, null, 1, TestTactic.orig);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.inter);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.cong);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_orig);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_inter);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_cong);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_confl);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_cong_confl);
    }

    @Test
    public void benchmark41(){
        saveNameSuffix = "case41_results.csv";
        String testName = "file-41";
        String fileName = "data/Casestudy/Performance/_file-041.dot";
        int numAttacker = 1;
        int numDefender = 1;
        String expectedProbStr = "1/1";
        // int max = doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, null, 1, TestTactic.orig);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, TestTactic.inter);
        // take long skip (302047) doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, 1, TestTactic.cong);
        int max = 1000000;
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_orig);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_inter);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_cong);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_confl);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_cong_confl);
    }

    @Test
    public void benchmark61(){
        saveNameSuffix = "case61_results.csv";
        String testName = "file-61";
        String fileName = "data/Casestudy/Performance/_file-061.dot";
        int numAttacker = 1;
        int numDefender = 1;
        String expectedProbStr = "1/1";
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.orig);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.inter);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.cong);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_orig);
        int max = 784908;
        // max = doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, null, 1, TestTactic.sp_inter);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_cong);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_confl);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_cong_confl);
    }

    @Test
    public void benchmark81(){
        saveNameSuffix = "case81_results.csv";
        String testName = "file-81";
        String fileName = "data/Casestudy/Performance/_file-081.dot";
        int numAttacker = 1;
        int numDefender = 1;
        String expectedProbStr = "1/1";
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.orig);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.inter);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.cong);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_orig);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_inter);
        // TIEMOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_cong);
        int max = doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, null, 1, TestTactic.sp_confl);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_cong_confl);
    }

    @Test
    public void benchmark101(){
        saveNameSuffix = "case101_results.csv";
        String testName = "file-101";
        String fileName = "data/Casestudy/Performance/_file-101.dot";
        int numAttacker = 1;
        int numDefender = 1;
        String expectedProbStr = "1/1";
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.orig);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.inter);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.cong);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_orig);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_inter);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_cong);
        // TIMEOUT doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, max, 1, TestTactic.sp_confl);
        doBenchmark(testName, fileName, numAttacker, numDefender, expectedProbStr, null, 1, TestTactic.sp_cong_confl);
    }

    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkRandomizedRealAll(String testName, String fileName, int numAttacker, int numDefender,
                                    String expectedProbStr){
        saveNameSuffix = "real_randomized.csv";
        for(String randPref : randPathPrefixes){
            Integer gameSize = null;
            // int gameSize = doBenchmark(testName, randPref+fileName, numAttacker, numDefender, null, null, 1, TestTactic.orig);
            // doBenchmark(testName, randPref+fileName, numAttacker, numDefender, null, gameSize, 1, TestTactic.inter);
            // doBenchmark(testName, randPref+fileName, numAttacker, numDefender, null, gameSize, 1, TestTactic.cong);
            // doBenchmark(testName, randPref+fileName, numAttacker, numDefender, null, gameSize, 1, TestTactic.sp_orig);
            // doBenchmark(testName, randPref+fileName, numAttacker, numDefender, null, gameSize, 1, TestTactic.sp_inter);
            // doBenchmark(testName, randPref+fileName, numAttacker, numDefender, null, gameSize, 1, TestTactic.sp_cong);
            // doBenchmark(testName, randPref+fileName, numAttacker, numDefender, null, gameSize, 1, TestTactic.sp_confl);
            doBenchmark(testName, randPref+fileName, numAttacker, numDefender, null, gameSize, 1, TestTactic.sp_cong_confl);
        }
    }

    @ParameterizedTest
    @CsvFileSource(files = pathPrefix + "realTestsDef.csv", numLinesToSkip = 1)
    void benchmarkRealAll(String testName, String fileName, int numAttacker, int numDefender,
                          String expectedProb){
        saveNameSuffix = "real_all.csv";
        int reps = 10;
        Integer gameSize = null;
        gameSize = doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProb, null, reps, TestTactic.orig);
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.inter);
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.cong);
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_orig);
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_inter);
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_cong);
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_confl);
        doBenchmark(testName, pathPrefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_cong_confl);
    }

    @ParameterizedTest
    @CsvFileSource(files = "data/TestADDs/smallTests.csv", numLinesToSkip = 1)
    void benchmarkSmallAll(String fileName, int numAttacker, int numDefender,
                          String expectedProb){
        String prefix = "data/TestADDs/";
        String testName = fileName;
        saveNameSuffix = "small_all.csv";
        int reps = 10;
        int gameSize = doBenchmark(testName, prefix+fileName, numAttacker, numDefender, expectedProb, null, reps, TestTactic.orig);
        doBenchmark(testName, prefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.inter);
        doBenchmark(testName, prefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.cong);
        doBenchmark(testName, prefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_orig);
        doBenchmark(testName, prefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_inter);
        int cong_size = doBenchmark(testName, prefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_cong);
        doBenchmark(testName, prefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_confl);
        int cc_size = doBenchmark(testName, prefix+fileName, numAttacker, numDefender, expectedProb, gameSize, reps, TestTactic.sp_cong_confl);
        if(cong_size<cc_size){
            System.out.println(fileName);
        }
    }


    @AfterAll
    static void printResultsFile(){
        try {
            String dateString = (DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")).format(LocalDateTime.now());
            String resultFileName = dateString + '_' + saveNameSuffix;
            FileWriter resultFile = new FileWriter(pathPrefix + resultFileName);
            resultFile.write("TestName, Optimization Tactic, GameSize, SizeReduction, MeanTime, MinTime, MaxTime\n");
            for(String result : results){
                resultFile.write(result);
            }
            resultFile.close();
            results.clear();
            System.out.println("Successfully wrote benchmark results.");
        } catch (IOException e) {
            System.out.println("Could not save benchmark results to file.");
            e.printStackTrace();
        }
    }

    private enum TestTactic{
        orig, inter, cong, sp_orig, sp_inter, sp_cong, sp_confl, sp_cong_confl
    }

    static int doBenchmark(String testName, String fileName, int numAttacker, int numDefender,
                           String expectedProbStr, Integer sizeCompare, int reps, TestTactic tactic){
        try (FileReader testIn = new FileReader(fileName)) {
            ADD add = ParserDOT.parseADD(testIn);
            Rational expectedProb = expectedProbStr==null?null:Rational.valueOf(expectedProbStr);
            return doBenchmark(add, fileName, numAttacker, numDefender, expectedProb, sizeCompare, reps, tactic);
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n" + e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        } catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e) {
            e.printStackTrace();
        }
        return -1;
    }


    static int doBenchmark(ADD add, String testName, int numAttacker, int numDefender,
                            Rational expectedProb, Integer size_compare, int reps, TestTactic tactic){
        if(reps <= 0){
            throw new IllegalArgumentException("Min 1 Repetition.");
        }
        long totalTime = 0;
        long maxTime = 0;
        long minTime = Long.MAX_VALUE;
        int gameSize = -1;
        for(int i = 0; i<reps; i++) {
            long startTime = System.currentTimeMillis();
            int newGameSize = gameSizeByTactic(add, numAttacker, numDefender, expectedProb, tactic);
            if(gameSize != -1 && newGameSize != gameSize){
                System.err.println("Game Size changed over different attempts in iteration??! Should be deterministic");
            }
            gameSize = newGameSize;
            long curTime = System.currentTimeMillis() - startTime;
            totalTime += curTime;
            minTime = Long.min(minTime, curTime);
            maxTime = Long.max(maxTime, curTime);
        }
        String benchmark = testName + ',' + tactic + ',' + gameSize + ',' +
                (((double) gameSize)/ (double) (size_compare==null?gameSize:size_compare))+ ',' +
                (totalTime/reps) + ',' + minTime + ',' + maxTime + '\n';
        System.out.println(benchmark);
        results.add(benchmark);
        return gameSize;
    }


    private static int gameSizeByTactic(ADD add, int numAttacker, int numDefender, Rational expectedProb, TestTactic tactic) {
        switch (tactic){
            case orig -> {
                return ADD2PRISMTest.testADDProb(add, numAttacker, numDefender, expectedProb);
            }
            case cong -> {
                return ADD2PRISMTest.testADDProbCongPOR(add, numAttacker, numDefender, expectedProb);
            }
            case inter -> {
                return ADD2PRISMTest.testADDProbInterPOR(add, numAttacker, numDefender, expectedProb);
            }
            case sp_orig -> {
                return ADD2PRISMTest.testSpADTProb(add, numAttacker, numDefender, expectedProb);
            }
            case sp_inter -> {
                return ADD2PRISMTest.testSpADTProbInterPOR(add, numAttacker, numDefender, expectedProb);
            }
            case sp_cong -> {
                return ADD2PRISMTest.testSpADTProbCongPOR(add, numAttacker, numDefender, expectedProb);
            }
            case sp_confl -> {
                return ADD2PRISMTest.testSpADTProbConfl(add, numAttacker, numDefender, expectedProb);
            }
            case sp_cong_confl -> {
                return ADD2PRISMTest.testSpADTProbCongrPORConfl(add, numAttacker, numDefender,expectedProb);
            }
        }
        throw new IllegalArgumentException("Invalid TestTactic provided.");
    }


    /** Just for ad-hoc execution of the different program parts
     *
     * @param args CLI args are not being considered
     */
    /*
    public static void main(String[] args) {
        String fileName = "data/TestADDs/OR.dot";
        Rational expectedProb = Rational.valueOf("24/25");
        try (FileReader testIn = new FileReader(fileName)) {
            System.out.println("Expected Probability: " + expectedProb);
            ADD add = ParserDOT.parseADD(testIn);
            Game game = MainDot2Semantics.generateSemantics(add, new InterchangableBEReduction());
            System.out.println(game);
            Rational actualProb = game.calculateRootProbability();
            System.out.println("Calculated Probability of Root of ADD: " + actualProb);
            if(!expectedProb.isNegative()){
                assertEquals(expectedProb, actualProb);
            }
            System.out.println("Size: " + game.getSize());
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n" + e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        } catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e) {
            e.printStackTrace();
        }
    }  */
}
