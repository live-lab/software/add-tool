import addtool.add.model.ADD;
import addtool.add.model.ADDBuilder;
import addtool.add.model.NaryOp;
import addtool.add.model.Vertex;
import addtool.add2dot.ADD2Dot;
import addtool.dot2add.ParserDOT;
import addtool.dot2add.WrongStructureADDException;
import junit.framework.TestCase;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Tests functionality in {@link ADDBuilder}to join {@link ADD} with each other.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 13.07.20
 */
class Joiner_Test extends TestCase {
    ADD model,model2;
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        FileReader in2 = new FileReader(Path.of("data","ADDs","file-436.dot").toString());
        ADD add = ParserDOT.parseADD(in2);
        in2.close();
        add.validateConnections();
        model=add;


        in2 = new FileReader(Path.of("data","ADDs","caseStudy.dot").toString());
        add = ParserDOT.parseADD(in2);
        in2.close();
        add.validateConnections();
        model2=add;

    }
    public void testSequence1(){
        try {
            PrintWriter pw=new PrintWriter(Path.of("data","ADDs","joined.dot").toString());
            ADD2Dot.writeADD2DotFile(pw,new ADDBuilder(model).simpleJoin("And",model).get());
            pw.close();
            String basePath=Path.of("data","ADDs","joined").toString();
            final Process p=Runtime.getRuntime().exec(String.format("dot -Tpng %s.dot -o %s.png",basePath,basePath));
        } catch (IOException | WrongStructureADDException e) {
            e.printStackTrace();
        }
        assertEquals(true,true);
    }
    public void testSequence2() throws FileNotFoundException, WrongStructureADDException {
        PrintWriter pw=new PrintWriter(Path.of("data","ADDs","joined2.dot").toString());
        ADD model3=ADDBuilder.copyADD(model);
        if(false) {
            //Test Method with a list for each model in header
            ADD2Dot.writeADD2DotFile(pw, new ADDBuilder(model).
                    join((NaryOp) ADDBuilder.buildOperatorForKey("And"), model3, List.of(model.getVertex2Id("1")),
                            List.of(model3.getVertex2Id("26"), model3.getVertex2Id("1")), List.of(), List.of()).get());
            pw.close();
        }else {
            //Test Method with a combined vertex list
            List<Vertex> predecessor1=new ArrayList<>();
            predecessor1.add(model.getVertex2Id("1"));
            List<Vertex> predecessor2=new ArrayList<>();
            predecessor2.add(model3.getVertex2Id("26"));
            List<Vertex> successor1=new ArrayList<>();
            successor1.add(model.getVertex2Id("7"));
            List<Vertex> successor2=new ArrayList<>();
            ADD2Dot.writeADD2DotFile(pw, new ADDBuilder(model).
                    join((NaryOp) ADDBuilder.buildOperatorForKey("And"), model3,
                            predecessor1, predecessor2,
                            successor1,successor2)
                    .get());
            pw.close();
        }
        try {
            String basePath=Path.of("data","ADDs","joined2").toString();
            final Process p=Runtime.getRuntime().exec(String.format("dot -Tpng %s.dot -o %s.png",basePath,basePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(true,true);
    }
}
