import addtool.add.model.ADD;
import addtool.add.model.Vertex;
import addtool.dot2add.ParserDOT;
import addtool.helpers.Constants;
import junit.framework.TestCase;

import java.io.FileReader;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Minor tests for sequence evaluation.
 * Should be removed or replaces.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 09.07.2020
 */
public class ADD_SEQUENCE_Test extends TestCase {

    ADD model;
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        FileReader in = new FileReader(Path.of("data","ADDs","file-436_noprob.dot").toString());
        ADD add = ParserDOT.parseADD(in);
        in.close();
        add.validateConnections();
        model=add;
        List<List<String>> subsets=Constants.getSubsets(add.getBasicEvents().stream().map(Vertex::getId).collect(Collectors.toList()));
        AtomicInteger success=new AtomicInteger();
        subsets.forEach(s -> {
            String[] seq = new String[s.size()];
            s.toArray(seq);
            boolean su = model.evaluateSequence(seq);
            if (su) success.incrementAndGet();
            //System.out.println(su + "|" + s);
        });
        System.out.printf("Of %d attacks %d where successful. That's %d percent%n", subsets.size(), success.get(), 100 * success.get() / subsets.size());
    }
    public void testSequence1(){
        boolean res=model.evaluateSequence("29", "10", "17", "0", "27", "5", "22", "1", "21", "26", "11", "25");
        System.out.println(model.getBasicEvents().size()+" basic events");
        /*long time=System.currentTimeMillis();
        //model.isValidSequence("29","29", "10", "17", "0", "27", "5", "22", "1", "21", "26", "11", "25");
        List<List<String>> sequences=new ADDBuilder(model).getAllValidSequences(20);
        System.out.println(System.currentTimeMillis()-time);
        System.out.println(sequences);*/
        assertEquals(false,res);
    }
    public void testSequence2(){
        boolean res=model.evaluateSequence("4","11","15");
        assertEquals(false,res);
    }
    /*public void testSequence3(){
        int threads=model.getBasicEvents().size(); int max=300;
        List<Runnable> runs=new ArrayList<>();
        CopyOnWriteArrayList<List<String>> res=new CopyOnWriteArrayList<>();
        model.getBasicEvents().stream().forEach(b->{
                ArrayList<String> par=new ArrayList<>();par.add(b.getId());
                runs.add(new Runnable() {
                    @Override
                    public void run() {
                        new ADDBuilder(ADDBuilder.copyADD(model)).getAllValidSequences(par,20,res,max);
                    }
                });
        });
        ExecutorService runner=Executors.newFixedThreadPool(threads);
        Executors.newSingleThreadExecutor().submit(()->{
            while(res.size()<max){//
                //Wait
                System.out.println(res.size()+" Size");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            runner.shutdownNow();
            assertEquals(true,true);
        });
        runs.forEach(runner::submit);
    }*/
}
