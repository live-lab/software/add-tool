import addtool.add.model.*;
import addtool.dot2add.*;
import junit.framework.TestCase;
import org.jscience.mathematics.number.Rational;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ParseADDfromDOTTest extends TestCase {
/*
    private BasicEvent[] events;
    public static final String prefix = "data/ADDs/";

    @Override
    public void setUp() throws Exception {
        super.setUp();

        events = new BasicEvent[4];
        events[0] = new BasicEventPlayer("a", Rational.ONE, Rational.valueOf(2, 1), Player.Attacker, "1");
        events[1] = new BasicEventPlayer("b", Rational.valueOf(1, 2), Rational.valueOf(4, 1), Player.Attacker, "2");
        events[2] = new BasicEventPlayer("c", Rational.valueOf(1, 2), Rational.ONE, Player.Attacker, "3");
        events[3] = new BasicEventPlayer("d", Rational.valueOf(1, 2), Rational.valueOf(5, 1), Player.Attacker, "4");

    }

    public void testAnd() throws WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, IOException, WrongValueForSinkException {
        FileReader fileReader = new FileReader(prefix + "and.dot");
        ADD parsed = ParserDOT.parseADD(fileReader);

        And and = new And("7", "7");
        and.addPredecessor(events[0]);
        and.addPredecessor(events[1]);
        and.setSinkAttacker(true);

        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("7", and);

        HashSet<Vertex> noSuccessor = new HashSet<>();
        noSuccessor.add(and);

        ADD constructed = new ADD(id2Vertex, noSuccessor, false);

        assertEquals(constructed.toString(), parsed.toString());
    }


    public void testOr() throws WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, IOException, WrongValueForSinkException {
        FileReader fileReader = new FileReader(prefix + "or.dot");
        ADD parsed = ParserDOT.parseADD(fileReader);

        Or or = new Or("7", "7");
        or.addPredecessor(events[0]);
        or.addPredecessor(events[1]);
        or.setSinkAttacker(true);

        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("7", or);

        HashSet<Vertex> noSuccessor = new HashSet<>();
        noSuccessor.add(or);

        ADD constructed = new ADD(id2Vertex, noSuccessor, false);

        assertEquals(constructed.toString(), parsed.toString());
    }

    public void testSand() throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        FileReader fileReader = new FileReader(prefix + "sand.dot");
        ADD parsed = ParserDOT.parseADD(fileReader);

        SAnd sand = new SAnd("7", "7");
        sand.addPredecessor(events[0]);
        sand.addPredecessor(events[1]);
        sand.setSinkAttacker(true);

        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("7", sand);

        HashSet<Vertex> noSuccessor = new HashSet<>();
        noSuccessor.add(sand);

        ADD constructed = new ADD(id2Vertex, noSuccessor, false);

        assertEquals(constructed.toString(), parsed.toString());
    }

    public void testSor() throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        FileReader fileReader = new FileReader(prefix + "sor.dot");
        ADD parsed = ParserDOT.parseADD(fileReader);

        SOr sor = new SOr("7", "7");
        sor.addPredecessor(events[0]);
        sor.addPredecessor(events[1]);
        sor.setSinkAttacker(true);

        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("7", sor);

        HashSet<Vertex> noSuccessor = new HashSet<>();
        noSuccessor.add(sor);

        ADD constructed = new ADD(id2Vertex, noSuccessor, false);

        assertEquals(constructed.toString(), parsed.toString());
    }

    public void testTrigger() throws WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, IOException, WrongValueForSinkException {
        FileReader fileReader = new FileReader(prefix + "trigger.dot");
        ADD parsed = ParserDOT.parseADD(fileReader);

        Trigger trigger = new Trigger("7", "7");

        trigger.setPredecessor(events[0]);
        trigger.addToTrigger(events[1]);
        events[1].setSinkAttacker(true);
        events[1].setTrigger_state(TriggerState.TRIGGER_ABLE);

        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("7", trigger);

        Set<Vertex> noSuccessor = new HashSet<>();
        noSuccessor.add(trigger);
        noSuccessor.add(events[1]);

        assertEquals(trigger.getToTrigger().toString(), ((Trigger) parsed.getVertex2Id("7")).getToTrigger().toString());
        ADD constructed = new ADD(id2Vertex, noSuccessor, false);
        assertEquals(trigger.getToTrigger().toString(), ((Trigger) parsed.getVertex2Id("7")).getToTrigger().toString());
        assertEquals(id2Vertex, parsed.getId2Vertex());

        assertEquals(constructed.toString(), parsed.toString());
        events[1].setSinkAttacker(false);
    }

    public void testReset() throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        FileReader fileReader = new FileReader(prefix + "reset.dot");
        ADD parsed = ParserDOT.parseADD(fileReader);

        Reset reset = new Reset("7", "7");
        reset.setPredecessor(events[0]);
        reset.addToReset(events[1]);
        events[1].setSinkAttacker(true);
        events[1].setResettable(true);

        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("7", reset);

        Set<Vertex> noSuccessor = new HashSet<>();
        noSuccessor.add(reset);
        noSuccessor.add(events[1]);

        assertEquals(reset.getToReset().toString(), ((Reset) parsed.getVertex2Id("7")).getToReset().toString());
        ADD constructed = new ADD(id2Vertex, noSuccessor, false);
        assertEquals(reset.getToReset().toString(), ((Reset) parsed.getVertex2Id("7")).getToReset().toString());

        assertEquals(constructed.toString(), parsed.toString());
        events[1].setSinkAttacker(false);
    }


    public void testBE() throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        ADD[] adds = new ADD[4];
        for (int i = 0; i < adds.length; i++) {
            FileReader fileReader = new FileReader(prefix + "be-" + (i + 1) + ".dot");
            adds[i] = ParserDOT.parseADD(fileReader);
        }

        for (int i = 0; i < adds.length; i++) {
            events[i].setSinkAttacker(true);
            assertEquals(adds[i].getTop().toString(), events[i].toString());
        }
    }

    public void testIvr() throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        FileReader fileReader = new FileReader(prefix + "ivr.dot");
        ADD parsed = ParserDOT.parseADD(fileReader);

        Ivr ivr = new Ivr("2", "2");
        ivr.setPredecessor(events[0]);
        ivr.setSinkAttacker(true);

        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", ivr);

        HashSet<Vertex> noSuccessor = new HashSet<>();
        noSuccessor.add(ivr);

        ADD constructed = new ADD(id2Vertex, noSuccessor, false);

        assertEquals(constructed, parsed);
    }

    public void testNot() throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        FileReader fileReader = new FileReader(prefix + "not.dot");
        ADD parsed = ParserDOT.parseADD(fileReader);

        Not ivr = new Not("2", "2");
        ivr.setPredecessor(events[0]);
        ivr.setSinkAttacker(true);

        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", ivr);

        HashSet<Vertex> noSuccessor = new HashSet<>();
        noSuccessor.add(ivr);

        ADD constructed = new ADD(id2Vertex, noSuccessor, false);

        assertEquals(constructed, parsed);
    }

    @Test
    public void testCyclic() throws FileNotFoundException {
        FileReader fileReader = new FileReader(prefix + "cyclic.dot");
        FileReader fileReader2 = new FileReader(prefix + "cyclic2.dot");

        Executable ex = () -> ParserDOT.parseADD(fileReader);

        Executable ex2 = () -> ParserDOT.parseADD(fileReader2);

        //Todo: try to handle StackOverflowError to WrongStructureADDException, does not work right now
        StackOverflowError e = Assertions.assertThrows(StackOverflowError.class, ex);
        StackOverflowError e2 = Assertions.assertThrows(StackOverflowError.class, ex2);
    }

    @Test(expected = NoValueSpecifiedException.class)
    public void testIncomplete() throws FileNotFoundException {
        FileReader fileReader = new FileReader(prefix + "be-incomplete.dot");
        FileReader fileReader2 = new FileReader(prefix + "be-incomplete-2.dot");


        Executable ex = () -> ParserDOT.parseADD(fileReader);

        Executable ex2 = () -> ParserDOT.parseADD(fileReader2);

        NoValueSpecifiedException e = Assertions.assertThrows(NoValueSpecifiedException.class, ex);
        NoValueSpecifiedException e2 = Assertions.assertThrows(NoValueSpecifiedException.class, ex2);
    }

    @Test(expected = WrongValueForLabelOperatorException.class)
    public void testUnknownOperator() throws FileNotFoundException {
        FileReader fileReader = new FileReader(prefix + "unknown.dot");

        Executable ex = () -> ParserDOT.parseADD(fileReader);

        WrongValueForLabelOperatorException e = Assertions.assertThrows(WrongValueForLabelOperatorException.class, ex);
    }

    @Test(expected = WrongValueForSinkException.class)
    public void testUnknownLabelBE() throws FileNotFoundException {
        FileReader fileReader = new FileReader(prefix + "be-wrong.dot");

        Executable ex = () -> ParserDOT.parseADD(fileReader);

        NoValueSpecifiedException e = Assertions.assertThrows(NoValueSpecifiedException.class, ex);
    }

    @Test(expected = WrongStructureADDException.class)
    public void testMissingOperand() throws FileNotFoundException {
        FileReader fileReader = new FileReader(prefix + "missing-operand.dot");

        Executable ex = ()->System.out.println(ParserDOT.parseADD(fileReader));
        WrongStructureADDException e = Assertions.assertThrows(WrongStructureADDException.class, ex);
    }


    @Test(expected = WrongStructureADDException.class)
    public void testTooManyOperands() throws FileNotFoundException {
        FileReader fileReader = new FileReader(prefix + "too-many-operands.dot");

        Executable ex = ()->System.out.println(ParserDOT.parseADD(fileReader));

        WrongStructureADDException e = Assertions.assertThrows(WrongStructureADDException.class, ex);
    }
    */
}
