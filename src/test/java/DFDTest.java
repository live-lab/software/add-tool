import addtool.dfd.DFD;
import addtool.dfd.Process;
import addtool.dfd.io.DFDExporter;
import addtool.dfd.io.DFDImporter;
import addtool.helpers.Threat;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class DFDTest {
    @Test
    public void outputDFD(){
        DFD testDFD=new DFD();
        File testFile=new File("Testfile.json");

        Process p=new Process();
        Threat t=new Threat();
        p.addThreat(t);
        testDFD.addEntity(p);

        DFDExporter.writeJSONString(testDFD,testFile);
        DFD d2=DFDImporter.importDFD(testFile);

        assertEquals(testDFD.toString(),d2.toString());
    }
}
