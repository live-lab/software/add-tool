import addtool.add.model.*;
import addtool.analysisonadd.Direction;
import addtool.dot2add.*;
import addtool.semantics.Valuation;
import addtool.semantics.Value;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.Assert.assertSame;

/**
 * Tests the ValuationVisitor.
 *
 * @author Alexander Taepper, alexander.taepper@tum.de
 * @since 30.09.2021
 */

public class ValuationTests {

    @Test
    public void basicAndTest(){
        try(FileReader testIn = new FileReader("data/TestADDs/AND.dot")) {
            ADD add = ParserDOT.parseADD(testIn);
            BasicEventPlayer be1 = (BasicEventPlayer) add.getVertex2Id("1");
            BasicEventPlayer be2 = (BasicEventPlayer) add.getVertex2Id("2");
            And andGate = (And) add.getVertex2Id("0");

            Valuation initial = new Valuation(add);
            assertSame(Value.UNDECIDED, initial.next(be1, Value.TRUE).getValue(add.getTop()));
            assertSame(Value.FALSE, initial.next(be1, Value.FALSE).getValue(add.getTop()));
            assertSame(Value.UNDECIDED, initial.next(be2, Value.TRUE).getValue(add.getTop()));
            assertSame(Value.FALSE, initial.next(be2, Value.FALSE).getValue(add.getTop()));

            assertSame(Value.UNDECIDED, initial.next(andGate, Direction.DOWN).getValue(add.getTop()));


        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file: data/TestADDs/AND.dot\n" + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    @Test
    public void basicOrTest(){
        try(FileReader testIn = new FileReader("data/TestADDs/OR.dot")) {
            ADD add = ParserDOT.parseADD(testIn);
            BasicEventPlayer be1 = (BasicEventPlayer) add.getVertex2Id("1");
            BasicEventPlayer be2 = (BasicEventPlayer) add.getVertex2Id("2");
            Or orGate = (Or) add.getVertex2Id("0");

            Valuation initial = new Valuation(add);
            assertSame(Value.TRUE, initial.next(be1, Value.TRUE).getValue(add.getTop()));
            assertSame(Value.TRUE, initial.next(be1, Value.TRUE).next(be2, Value.TRUE).getValue(add.getTop()));
            assertSame(Value.TRUE, initial.next(be1, Value.TRUE).next(be2, Value.FALSE).getValue(add.getTop()));
            assertSame(Value.UNDECIDED, initial.next(be1, Value.FALSE).getValue(add.getTop()));
            assertSame(Value.TRUE, initial.next(be1, Value.FALSE).next(be2, Value.TRUE).getValue(add.getTop()));
            assertSame(Value.FALSE, initial.next(be1, Value.FALSE).next(be2, Value.FALSE).getValue(add.getTop()));
            assertSame(Value.TRUE, initial.next(be2, Value.TRUE).getValue(add.getTop()));
            assertSame(Value.TRUE, initial.next(be2, Value.TRUE).next(be1, Value.TRUE).getValue(add.getTop()));
            assertSame(Value.TRUE, initial.next(be2, Value.TRUE).next(be1, Value.FALSE).getValue(add.getTop()));
            assertSame(Value.UNDECIDED, initial.next(be2, Value.FALSE).getValue(add.getTop()));
            assertSame(Value.TRUE, initial.next(be2, Value.FALSE).next(be1, Value.TRUE).getValue(add.getTop()));
            assertSame(Value.FALSE, initial.next(be2, Value.FALSE).next(be1, Value.FALSE).getValue(add.getTop()));

            assertSame(Value.TRUE, initial.next(be1, Value.FALSE).next(be1, Value.TRUE).getValue(add.getTop()));
            assertSame(Value.UNDECIDED, initial.next(be1, Value.TRUE).next(be1, Value.FALSE).getValue(add.getTop()));

            assertSame(Value.UNDECIDED, initial.next(orGate, Direction.DOWN).getValue(add.getTop()));


        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file: data/TestADDs/OR.dot\n" + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    @Test
    public void obscureInitial(){
        try(FileReader testIn = new FileReader("data/TestADDs/ObscureInitial.dot")) {
            ADD add = ParserDOT.parseADD(testIn);
            NotTrue nt1 = (NotTrue) add.getVertex2Id("1");
            NotTrue nt2 = (NotTrue) add.getVertex2Id("2");
            BasicEventPlayer be1 = (BasicEventPlayer) add.getVertex2Id("3");
            BasicEventPlayer be2 = (BasicEventPlayer) add.getVertex2Id("4");

            Valuation initial = new Valuation(add);
            assertSame(Value.FALSE, initial.getValue(add.getTop()));
            assertSame(Value.TRUE, initial.getValue(nt1));
            assertSame(Value.TRUE, initial.getValue(nt2));
            assertSame(Value.UNDECIDED, initial.getValue(be1));
            assertSame(Value.UNDECIDED, initial.getValue(be2));


        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file: data/TestADDs/ObscureInitial.dot\n" + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    @Test
    public void parseTestOfSANDRace() {
        try (FileReader fileReader = new FileReader("data/TestADDs/SANDRace.dot")) {
            ADD add = ParserDOT.parseADD(fileReader);
            assertSame(2, add.getVertex2Id("2").getSuccessors().size());
        } catch (IOException | WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void sandRace1(){
        try(FileReader testIn = new FileReader("data/TestADDs/SANDRace.dot")) {
            ADD add = ParserDOT.parseADD(testIn);
            NotTrue nt = (NotTrue) add.getVertex2Id("1");
            BasicEventPlayer be = (BasicEventPlayer) add.getVertex2Id("2");

            Valuation initial = new Valuation(add);
            assertSame(Value.UNDECIDED, initial.getValue(add.getTop()));
            assertSame(Value.TRUE, initial.getValue(nt));
            assertSame(Value.UNDECIDED, initial.getValue(be));

            Valuation beFalse = initial.next(be, Value.FALSE);
            assertSame(Value.FALSE, beFalse.getValue(add.getTop()));
            assertSame(Value.TRUE, beFalse.getValue(nt));
            assertSame(Value.FALSE, beFalse.getValue(be));

            // Important case!!! Here the True propagation into Sand must not be before the switching off of the NotTrue
            Valuation beTrue = initial.next(be, Value.TRUE);
            assertSame(Value.FALSE, beTrue.getValue(add.getTop()));
            assertSame(Value.FALSE, beTrue.getValue(nt));
            assertSame(Value.TRUE, beTrue.getValue(be));


        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file: data/TestADDs/ObscureInitial.dot\n" + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }


    @Test
    public void sandRaceEvaluate(){
        try(FileReader testIn = new FileReader("data/TestADDs/SANDRace.dot")) {
            ADD add = ParserDOT.parseADD(testIn);
            NotTrue nt = (NotTrue) add.getVertex2Id("1");
            BasicEventPlayer be = (BasicEventPlayer) add.getVertex2Id("2");

            be.setValue(Value.TRUE);
            be.evaluate();

            assertSame(Value.FALSE, add.getTop().getValue());
            assertSame(Value.FALSE, nt.getValue());
            assertSame(Value.TRUE, be.getValue());


        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file: data/TestADDs/ObscureInitial.dot\n" + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    @Test
    public void evaluateException(){
        try(FileReader testIn = new FileReader("data/TestADDs/ROOT_NOT_obfuscated.dot")) {
            ADD add = ParserDOT.parseADD(testIn);
            BasicEventPlayer be = (BasicEventPlayer) add.getVertex2Id("3");

            be.setValue(Value.FALSE);
            be.evaluate();

            assertSame(Value.FALSE, be.getValue());
            assertSame(Value.TRUE, add.getTop().getValue());

            be.setValue(Value.UNDECIDED);
            be.evaluate();

            assertSame(Value.UNDECIDED, be.getValue());
            assertSame(Value.UNDECIDED, add.getTop().getValue());
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file: data/TestADDs/ObscureInitial.dot\n" + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

}
