import addtool.add.model.*;
import junit.framework.TestCase;
import org.jscience.mathematics.number.Rational;
import addtool.xml2add.ParserXML;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
public class ParseADDfromXMLTest extends TestCase {

/*
    private BasicEvent[] events;
    public static final String prefix = "data/ADDs/";
    public ParseADDfromXMLTest(){

    }
    @Override
    public void setUp() throws Exception {
        super.setUp();

        events = new BasicEvent[4];
        events[0] = new BasicEventPlayer("a", Rational.ONE, Rational.ZERO, Player.Attacker, "1");
        events[1] = new BasicEventPlayer("b", Rational.ONE, Rational.ZERO, Player.Attacker, "2");
        events[2] = new BasicEventPlayer("c", Rational.ONE, Rational.ZERO, Player.Attacker, "3");
        events[3] = new BasicEventPlayer("d", Rational.ONE, Rational.ZERO, Player.Attacker, "4");
    }

    public void testAnd() {
        // parse ADD
        ADD parsed = ParserXML.parse(prefix + "AND-test.xml");

        //construct equal ADD
        And and = new And("0", "0");
        and.setPredecessor(0, events[0]);
        and.setPredecessor(1, events[1]);
        and.setSinkAttacker(true);
        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("0", and);
        Set<Vertex> noSuccessor = Set.of(and);
        ADD constructed = new ADD(id2Vertex, noSuccessor, false);

        // check whether ADDs are equal or there is something wrong with parsing
        assertEquals(constructed, parsed);
    }


    public void testOr() {
        //parse ADD
        ADD parsed = ParserXML.parse(prefix + "OR-test.xml");

        // construct equal ADD
        Or or = new Or("0", "0");
        or.setPredecessor(0, events[0]);
        or.setPredecessor(1, events[1]);
        or.setSinkAttacker(true);
        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("0", or);
        ADD constructed = new ADD(id2Vertex, Set.of(or), false);

        assertEquals(constructed, parsed);
    }

    public void testSand() {
        ADD parsed = ParserXML.parse(prefix + "SAND-test.xml");

        // construct equal ADD
        SAnd sand = new SAnd("0", "0");
        sand.setPredecessor(0, events[0]);
        sand.setPredecessor(1, events[1]);
        sand.setSinkAttacker(true);
        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("0", sand);
        ADD constructed = new ADD(id2Vertex, Set.of(sand), false);

        assertEquals(constructed, parsed);
    }

    public void testNAry() {
        ADD parsed = ParserXML.parse(prefix + "NARY-test.xml");

        // construct equal ADD
        SAnd sand = new SAnd("0", "0");
        sand.setPredecessor(0, events[0]);
        sand.setPredecessor(1, events[1]);
        sand.setPredecessor(2, events[2]);
        sand.setPredecessor(3, events[3]);
        sand.setSinkAttacker(true);
        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("3", events[2]);
        id2Vertex.put("4", events[3]);
        id2Vertex.put("0", sand);
        ArrayList<Vertex> noSuccessor = new ArrayList<>();
        noSuccessor.add(sand);
        ADD constructed = new ADD(id2Vertex, Set.of(sand), false);

        assertEquals(constructed, parsed);
    }


    public void testCountermeasure() {
        // parse ADD
        ADD parsed = ParserXML.parse(prefix + "COUNTER-test.xml");

        //construct equal ADD
        And and = new And("root", "0");
        and.setPredecessor(0, events[0]);
        and.setPredecessor(1, events[1]);
        Countermeasure counter = new Countermeasure("counter", "6");
        and.setPredecessor(2, counter);
        And and2 = new And("counter", "3");
        counter.setPredecessor(and2);
        BasicEvent b1 = new BasicEventPlayer("c", Rational.ONE, Rational.ZERO, Player.Defender, "4");
        BasicEvent b2 = new BasicEventPlayer("d", Rational.ONE, Rational.ZERO, Player.Defender, "5");
        and2.setPredecessor(0, b1);
        and2.setPredecessor(1, b2);
        and.setSinkAttacker(true);
        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        id2Vertex.put("1", events[0]);
        id2Vertex.put("2", events[1]);
        id2Vertex.put("6", counter);
        id2Vertex.put("3", and2);
        id2Vertex.put("4", b1);
        id2Vertex.put("5", b2);
        id2Vertex.put("0", and);
        ArrayList<Vertex> noSuccessor = new ArrayList<>();
        noSuccessor.add(and);
        ADD constructed = new ADD(id2Vertex, Set.of(and), false);

        // check whether ADDs are equal or there is something wrong with parsing
        assertEquals(constructed, parsed);
    }
*/
}