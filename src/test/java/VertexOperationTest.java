import addtool.add.model.*;
import junit.framework.TestCase;
import org.jscience.mathematics.number.Rational;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class VertexOperationTest extends TestCase {
    final Function<Vertex,String> mapToOut= v->String.valueOf(v.getSuccessors().size());
    final Function<Vertex,String> mapToIn= v->String.valueOf(v.getPredecessors().size());
    public VertexOperationTest(){

    }
    public void testSimpleModel(){
        List<String> valenzOut= List.of( "1","1","0");
        List<String> valenzIn= List.of(  "0","0","2");
        Vertex and=new And("1","1");
        Vertex be1=new BasicEventPlayer("2",Rational.ONE,Rational.ZERO, Player.Attacker,"2");
        Vertex be2=new BasicEventPlayer("3",Rational.ONE,Rational.ZERO, Player.Attacker,"3");
        and.addPredecessor(be1);
        and.addPredecessor(be2);

        assertEquals(valenzOut,valenzOf(mapToOut,be1,be2,and));
        assertEquals(valenzIn,valenzOf(mapToIn,be1,be2,and));

    }
    public void testSimpleModel2(){
        List<String> valenzOut= List.of( "1","1","0");
        List<String> valenzIn= List.of(  "0","0","2");
        And and=new And("1","1");
        Vertex be1=new BasicEventPlayer("2",Rational.ONE,Rational.ZERO, Player.Attacker,"2");
        Vertex be2=new BasicEventPlayer("3",Rational.ONE,Rational.ZERO, Player.Attacker,"3");
        and.addPredecessors(List.of(be1,be2));

        assertEquals(valenzOut,valenzOf(mapToOut,be1,be2,and));
        assertEquals(valenzIn,valenzOf(mapToIn,be1,be2,and));

    }
    public void testSimpleModel3(){
        List<String> valenzOut= List.of( "0","1","0");
        List<String> valenzIn= List.of(  "0","0","1");
        Not not=new Not("1","1");
        Vertex be1=new BasicEventPlayer("2",Rational.ONE,Rational.ZERO, Player.Attacker,"2");
        Vertex be2=new BasicEventPlayer("3",Rational.ONE,Rational.ZERO, Player.Attacker,"3");
        not.addPredecessor(be1);
        not.addPredecessor(be2);

        assertEquals(valenzOut,valenzOf(mapToOut,be1,be2,not));
        assertEquals(valenzIn,valenzOf(mapToIn,be1,be2,not));

    }
    public void testSimpleModel4(){
        List<String> valenzOut= List.of( "0","1","0");
        List<String> valenzIn= List.of(  "0","0","1");
        And not=new And("1","1");
        Vertex be1=new BasicEventPlayer("2",Rational.ONE,Rational.ZERO, Player.Attacker,"2");
        Vertex be2=new BasicEventPlayer("3",Rational.ONE,Rational.ZERO, Player.Attacker,"3");
        not.addPredecessor(be1);
        not.replacePredecessor(be1,be2);

        assertEquals(valenzOut,valenzOf(mapToOut,be1,be2,not));
        assertEquals(valenzIn,valenzOf(mapToIn,be1,be2,not));

    }
    public void testSimpleModel5(){
        List<String> valenzOut= List.of( "1","1","0");
        List<String> valenzIn= List.of(  "0","0","2");
        And not=new And("1","1");
        Vertex be1=new BasicEventPlayer("2",Rational.ONE,Rational.ZERO, Player.Attacker,"2");
        Vertex be2=new BasicEventPlayer("3",Rational.ONE,Rational.ZERO, Player.Attacker,"3");
        be1.addSuccessor(not);
        be2.addSuccessor(not);

        assertEquals(valenzOut,valenzOf(mapToOut,be1,be2,not));
        assertEquals(valenzIn,valenzOf(mapToIn,be1,be2,not));

    }
    public List<String> valenzOf(Function<Vertex,String> map,Vertex... vs){
        return Arrays.stream(vs).map(map).collect(Collectors.toList());
    }
}
