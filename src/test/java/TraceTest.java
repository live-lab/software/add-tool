import addtool.add.model.Vertex;
import addtool.learning.DataSet;
import addtool.learning.LearnHelper;
import addtool.learning.Sequence;
import addtool.learning.TraceIO;
import junit.framework.TestCase;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TraceTest extends TestCase {
    DataSet ref;
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Sequence s1= new Sequence(List.of("1","5","9","6"));
        Sequence s2= new Sequence(List.of("9","2","9","6","7"));
        Sequence s3= new Sequence(List.of("3","5","6"));
        ref=new DataSet();
        ref.put(s1,Boolean.TRUE);
        ref.put(s2,Boolean.FALSE);
        ref.put(s3,Boolean.FALSE);
    }
    public void testIO() throws IOException {
        File t=new File("dasas.trace");
        TraceIO.writeBinTraces(t,ref);

        DataSet ref2=TraceIO.readBinTraces(t);

        t.deleteOnExit();

        assertEquals(ref,ref2);
    }
    public void testBE() {
        Set<Vertex> be= LearnHelper.getBeFromTraces(ref);

        Set<String> neededID= Set.of("1","2","3","5","6","7","9");
        assertEquals(neededID,be.stream().map(Vertex::getId).collect(Collectors.toSet()));
    }
}
