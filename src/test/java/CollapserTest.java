import addtool.add.model.ADD;
import addtool.add2prism.MainDot2Semantics;
import addtool.dot2add.*;
import addtool.analysisonadd.ADDCollapser;
import addtool.analysisongame.ProbabilityVisitor;
import org.jscience.mathematics.number.Rational;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import addtool.semantics.Game;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CollapserTest {
    private static final String testPath = "data/TestADDs/";

    @ParameterizedTest
    @CsvFileSource(files = testPath + "smallTests.csv", numLinesToSkip = 1)
    void testSmallAndOr(String fileName, int numAttacker, int numDefender,
                        String expectedProbStr){
        Rational expectedProb = Rational.valueOf(expectedProbStr);
        parameterizedAndOrCollapse(testPath+fileName, numAttacker, numDefender, expectedProb);
    }

    @ParameterizedTest
    @CsvFileSource(files = testPath + "smallTests.csv", numLinesToSkip = 1)
    void testSmallUnaryGatesCorrect(String fileName, int numAttacker, int numDefender,
                                    String expectedProbStr){
        Rational expectedProb = Rational.valueOf(expectedProbStr);
        unaryCollapseCorrect(testPath+fileName, numAttacker, numDefender, expectedProb);
    }

    @ParameterizedTest
    @CsvFileSource(files = testPath + "unaryTests.csv", numLinesToSkip = 1)
    void testSmallUnaryGates(String fileName, int numAttacker, int numDefender,
                                    String expectedProbStr, int minimizedNum){
        Rational expectedProb = Rational.valueOf(expectedProbStr);
        unaryCollapseCorrect(testPath+fileName, numAttacker, numDefender, expectedProb);
        unaryCollapseDoneCorrectSize(testPath+fileName, numAttacker, numDefender, minimizedNum);
    }

    // Compares collapsed ADD Solution with actual solution.
    void parameterizedAndOrCollapse(String fileName, int numAttacker, int numDefender, Rational expectedProb){
        try(FileReader testIn = new FileReader(fileName)) {
            System.out.println("Test-ADD: " + fileName);
            System.out.println("Actual Probability: " + expectedProb);
            ADD add = ParserDOT.parseADD(testIn);
            ADD collapsedADD =  ADDCollapser.collapseAndOr(add);
            assertADDProb(collapsedADD, numAttacker, numDefender, expectedProb);
        } catch (FileNotFoundException e) {
            fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    // Compares collapsed ADD Solution with actual solution.
    void unaryCollapseCorrect(String fileName, int numAttacker, int numDefender, Rational expectedProb){
        try(FileReader testIn = new FileReader(fileName)) {
            System.out.println("Test-ADD: " + fileName);
            System.out.println("Expected Probability: " + expectedProb);
            ADD add = ParserDOT.parseADD(testIn);
            ADD collapsedADD =  ADDCollapser.collapseUnaryGates(add);
            assertADDProb(collapsedADD, numAttacker, numDefender, expectedProb);
        } catch (FileNotFoundException e) {
            fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    // Confirms that unary collapses are performed.
    void unaryCollapseDoneCorrectSize(String fileName, int numAttacker, int numDefender, int minimizedNum){
        try(FileReader testIn = new FileReader(fileName)) {
            System.out.println("Test-ADD: " + fileName);
            System.out.println("Expected minimized number of vertices: " + minimizedNum);
            ADD add = ParserDOT.parseADD(testIn);
            ADD collapsedADD = ADDCollapser.collapseUnaryGates(add);
            assertEquals(minimizedNum, collapsedADD.getReachableVertices().size());
        } catch (FileNotFoundException e) {
            fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    protected static void assertADDProb(ADD add, int numAttacker, int numDefender, Rational expected){
        // translation to game
        Game game = MainDot2Semantics.generateSemantics(add);
        // Get probability of root
        Rational actualProb = new ProbabilityVisitor(game).getRootProb();
        System.out.println("Probability of Root with AndOr-collapsed ADD: " + actualProb);
        assertEquals(expected, actualProb);
    }
}
