import org.junit.Test;

import javax.crypto.Cipher;
import java.io.File;
import java.security.SecureRandom;
import java.util.Arrays;

import static addtool.helpers.Crypter.*;
import static org.junit.Assert.assertTrue;

public class CrypterTest {
    @Test
    public void testAsymmetric() throws Exception {
        byte[] clear = new byte[512];
        SecureRandom.getInstanceStrong().nextBytes(clear);
        byte[] encoded=encrypt(clear,readPrivateKey(new File("test_encryption\\key.pem"),"test".toCharArray()));
        byte[] decoded=decrypt(encoded,getKey(new File("test_encryption\\cert2.pem"),true));
        assertTrue(Arrays.equals(clear,decoded));
    }
    @Test
    public void testSymmetric() throws Exception {
        byte[] clear = new byte[512];
        SecureRandom.getInstanceStrong().nextBytes(clear);
        byte[] encoded=cryptSymmetric("AES/CBC/PKCS5Padding",clear, Cipher.ENCRYPT_MODE,getSymmetricKeyFromPassword("test123"));
        byte[] decoded=cryptSymmetric("AES/CBC/PKCS5Padding",encoded,Cipher.DECRYPT_MODE,getSymmetricKeyFromPassword("test123"));
        assertTrue(Arrays.equals(clear,decoded));
    }
}
