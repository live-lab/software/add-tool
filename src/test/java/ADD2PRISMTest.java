import addtool.add.model.ADD;
import addtool.add2prism.GameGeneration.CongruentBEReduction;
import addtool.add2prism.GameGeneration.DefaultAvailableMoves;
import addtool.add2prism.GameGeneration.InterchangableBEReduction;
import addtool.add2prism.MainDot2Semantics;
import addtool.dot2add.*;
import addtool.semantics.Game;
import addtool.semantics.selfpruning.*;
import org.jscience.mathematics.number.Rational;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;


/**
 * Test for ADD conversion to PRISM.
 * @author Alexander Taepper alexander.taepper@tum.de
 * @since 20.01.2020
 */

// Compares results with optimization enabled to the standard results.
public class ADD2PRISMTest {

    private static final String testPath = "data/TestADDs/";

    @ParameterizedTest
    @CsvFileSource(files = testPath + "smallTests.csv", numLinesToSkip = 1)
    void testSmall(String fileName, int numAttacker, int numDefender,
                   String expectedProbStr){
        Rational expectedProb = Rational.valueOf(expectedProbStr);
        parameterizedADDTest(testPath+fileName, numAttacker, numDefender, expectedProb);
    }

    @Test
    void test21Dot(){
        Rational expectedProb = Rational.valueOf(1, 1);
        parameterizedADDTest(testPath + "_file-021.dot", 1, 1, expectedProb);
    }

    @Test
    void test41Dot(){
        Rational expectedProb = Rational.valueOf(1, 1);
        String fileName = testPath + "_file-041.dot";
        try(FileReader testIn = new FileReader(fileName)) {
            ADD add = ParserDOT.parseADD(testIn);
            testSpADTProbCongPOR(add, 1, 1, expectedProb);
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    @Test
    void test61Dot(){
        Rational expectedProb = Rational.valueOf(1, 1);
        String fileName = "data/Casestudy/Performance/_file-061.dot";
        try(FileReader testIn = new FileReader(fileName)) {
            ADD add = ParserDOT.parseADD(testIn);
            testSpADTProbCongPOR(add, 1, 1, expectedProb);
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    @Test
    void test81Dot(){
        Rational expectedProb = Rational.valueOf(1, 1);
        String fileName = "data/Casestudy/Performance/_file-081.dot";
        try(FileReader testIn = new FileReader(fileName)) {
            ADD add = ParserDOT.parseADD(testIn);
            testSpADTProbCongrPORConfl(add, 1, 1, expectedProb);
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    void test101Dot(){
        Rational expectedProb = Rational.valueOf(1, 1);
        String fileName = "data/Casestudy/Performance/_file-101.dot";
        try(FileReader testIn = new FileReader(fileName)) {
            ADD add = ParserDOT.parseADD(testIn);
            testSpADTProbCongrPORConfl(add, 1, 1, expectedProb);
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    void test121Dot(){
        Rational expectedProb = Rational.valueOf(1, 1);
        String fileName = "data/Casestudy/Performance/_file-121.dot";
        try(FileReader testIn = new FileReader(fileName)) {
            ADD add = ParserDOT.parseADD(testIn);
            testSpADTProbCongrPORConfl(add, 1, 1, expectedProb);
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    static void parameterizedADDTest(String fileName, int numAttacker, int numDefender, Rational expectedProb){
        try(FileReader testIn = new FileReader(fileName)) {
            System.out.println("Test-ADD: " + fileName);
            System.out.println("Expected Probability: " + expectedProb);
            ADD add = ParserDOT.parseADD(testIn);
            testADDProb(add, numAttacker, numDefender, expectedProb);
            testADDProbInterPOR(add, numAttacker, numDefender, expectedProb);
            testADDProbCongPOR(add, numAttacker, numDefender, expectedProb);
            testSpADTProb(add, numAttacker, numDefender, expectedProb);
            testSpADTProbInterPOR(add, numAttacker, numDefender, expectedProb);
            testSpADTProbCongPOR(add, numAttacker, numDefender, expectedProb);
            testSpADTProbConfl(add,numAttacker, numDefender, expectedProb);
            testSpADTProbCongrPORConfl(add,numAttacker,numDefender,expectedProb);
        } catch (FileNotFoundException e) {
            Assert.fail("Input / Result file not found.\n"+e.getMessage());
        } catch (IOException e) {
            Assert.fail("IOException while dealing with input-file:" + fileName + '\n' + e.getMessage());
        }catch (WrongValueForLabelOperatorException | NoValueSpecifiedException |
                WrongStructureADDException | WrongValueForSinkException e){
            e.printStackTrace();
        }
    }

    protected static int testADDProb(ADD add, int numAttacker, int numDefender, Rational expected){
        // translation to game
        Game game = MainDot2Semantics.generateSemantics(add, new DefaultAvailableMoves());
        // Get probability of root
        System.out.println("Game generated.");
        Rational actualProb = game.calculateRootProbability();
        System.out.println("Actual calc. probability: " + actualProb);
        System.out.println(game.getSize());
        if(expected != null) {
            assertEquals(expected, actualProb);
        }
        return game.getSize();
    }

    protected static int testADDProbInterPOR(ADD add, int numAttacker, int numDefender, Rational expected){
        // translation to game
        Game game = MainDot2Semantics.generateSemantics(add, new InterchangableBEReduction());
        // Get probability of root
        System.out.println("Game generated.");
        Rational actualProb = game.calculateRootProbability();
        System.out.println("Actual calc. probability: " + actualProb);
        System.out.println(game.getSize());
        if(expected != null) {
            assertEquals(expected, actualProb);
        }
        return game.getSize();
    }

    protected static int testADDProbCongPOR(ADD add, int numAttacker, int numDefender, Rational expected){
        // translation to game
        Game game = MainDot2Semantics.generateSemantics(add, new CongruentBEReduction());
        // Get probability of root
        System.out.println("Game generated.");
        Rational actualProb = game.calculateRootProbability();
        System.out.println("Actual calc. probability: " + actualProb);
        System.out.println(game.getSize());
        if(expected != null) {
            assertEquals(expected, actualProb);
        }
        return game.getSize();
    }

    protected static int testSpADTProb(ADD add, int numAttacker, int numDefender, Rational expected) {
        prADT adt = ADDtoSelfPruning.convert(add);
        prGame game = prGameGen.gameGen(adt, new prDefaultAvailableMoves(adt));
        // Get probability of root
        System.out.println("Game generated.");
        Rational actualProb = game.calculateRootOptimalTup();
        System.out.println("Actual calc. probability: " + actualProb);
        System.out.println(game.getSize());
        if(expected != null) {
            assertEquals(expected, actualProb);
        }
        return game.getSize();
    }

    protected static int testSpADTProbInterPOR(ADD add, int numAttacker, int numDefender, Rational expected) {
        prADT adt = ADDtoSelfPruning.convert(add);
        prGame game = prGameGen.gameGen(adt, new prInterchangeableBEReduction(adt));
        // Get probability of root
        System.out.println("Game generated.");
        Rational actualProb = game.calculateRootOptimalTup();
        System.out.println("Actual calc. probability: " + actualProb);
        System.out.println(game.getSize());
        if(expected != null) {
            assertEquals(expected, actualProb);
        }
        return game.getSize();
    }

    protected static int testSpADTProbCongPOR(ADD add, int numAttacker, int numDefender, Rational expected) {
        prADT adt = ADDtoSelfPruning.convert(add);
        prGame game = prGameGen.gameGen(adt, new prCongruentBEReduction(adt));
        // Get probability of root
        System.out.println("Game generated.");
        Rational actualProb = game.calculateRootOptimalTup();
        System.out.println("Actual calc. probability: " + actualProb);
        System.out.println(game.getSize());
        if(expected != null) {
            assertEquals(expected, actualProb);
        }
        return game.getSize();
    }

    protected static int testSpADTProbConfl(ADD add, int numAttacker, int numDefender, Rational expected) {
        if(numAttacker != 1 || numDefender != 1){
            throw new IllegalArgumentException("Confluence Reduction only defined for nums = 1");
        }
        prADT adt = ADDtoSelfPruning.convert(add);
        prGame game = prGameGen.gameGen(adt, new prConfluence(adt));
        // Get probability of root
        System.out.println("Game generated.");
        Rational actualProb = game.calculateRootOptimalTup();
        System.out.println("Actual calc. probability: " + actualProb);
        System.out.println(game.getSize());
        if(expected != null) {
            assertEquals(expected, actualProb);
        }
        return game.getSize();
    }

    protected static int testSpADTProbCongrPORConfl(ADD add, int numAttacker, int numDefender, Rational expected) {
        if(numAttacker != 1 || numDefender != 1){
            throw new IllegalArgumentException("Confluence Reduction only defined for nums = 1");
        }
        prADT adt = ADDtoSelfPruning.convert(add);
        prGame game = prGameGen.gameGen(adt, new prCongruentConfluence2(adt));
        // Get probability of root
        System.out.println("Game generated.");
        Rational actualProb = game.calculateRootOptimalTup();
        System.out.println("Actual calc. probability: " + actualProb);
        System.out.println(game.getSize());
        if(expected != null) {
            assertEquals(expected, actualProb);
        }
        return game.getSize();
    }
}
