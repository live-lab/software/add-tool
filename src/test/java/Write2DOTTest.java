import addtool.add.model.ADD;
import addtool.add2dot.ADD2Dot;
import addtool.dot2add.*;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

class Write2DOTTest {
    public static final String prefix = "data/ADDs/";

    void testWrite() throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        ADD parsed = ParserDOT.parseADD(new FileReader(prefix + "and.dot"));

        PrintWriter writer = new PrintWriter(prefix + "and.dot.new", "UTF-8");
        ADD2Dot.writeADD2DotFile(writer,parsed);
    }
}