import addtool.helpers.CaseStringList;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests the case in-sensitive List version
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 17.02.2021
 */
class CaseStringListTest{
    @Test
    void testExample(){
        CaseStringList be = CaseStringList.ofCSL("BE", "BasicEvent");
        assertEquals(true,be.contains("be"));
        assertEquals(false,be.contains("bes"));
    }
}
