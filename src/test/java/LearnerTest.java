import addtool.add.model.ADD;
import addtool.add.model.Vertex;
import addtool.learning.*;
import junit.framework.TestCase;
import org.jscience.mathematics.number.Rational;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.BiPredicate;

/**
 * Tests for basic {@link LearnADD} functions.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 28.06.2021
 */
public class LearnerTest extends TestCase {
    DataSet ref;
    LearnADD learner;
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Sequence s1= new Sequence(List.of("1","5","9","6"));
        Sequence s2= new Sequence(List.of("9","2","9","6","7"));
        Sequence s3= new Sequence(List.of("3","5","6"));
        ref=new DataSet();
        ref.put(s1,Boolean.TRUE);
        ref.put(s2,Boolean.FALSE);
        ref.put(s3,Boolean.FALSE);

        Set<Vertex> bes= LearnHelper.getBeFromTraces(ref);
        LearnerConstraints learnerConstraints=new LearnerConstraints();
        learner=new LearnADD(learnerConstraints);
        learner.replaceIfBetter(LearnHelper.generateRandomTree(bes,learner.getRand(),learnerConstraints.getOps()), Rational.valueOf(1,1));
        learner.replaceIfBetter(LearnHelper.generateRandomTree(bes,learner.getRand(),learnerConstraints.getOps()), Rational.valueOf(1,2));
        learner.replaceIfBetter(LearnHelper.generateRandomTree(bes,learner.getRand(),learnerConstraints.getOps()), Rational.valueOf(1,3));
    }
    public void testMomentum() {
        ADD best=learner.getBest();
        ADD worst=learner.getWorst();

        BiPredicate<Rational,Rational> compareBest= Rational::isLargerThan;
        BiPredicate<Rational,Rational> compareWorst= Rational::isLessThan;

        assertEquals(best,learner.getMomentFromPool(compareBest,Rational.ZERO));
        assertEquals(worst,learner.getMomentFromPool(compareWorst,Rational.ONE));
    }
    public void testSplitting() {
        List<DataSet> splits=LearnHelper.getPartitions(ref,3, new Random());
        DataSet ref2=LearnHelper.joinDataSet(splits);
        assertEquals(ref,ref2);
        splits=LearnHelper.getPartitions(ref,2, new Random());
        ref2=LearnHelper.joinDataSet(splits);
        assertEquals(ref,ref2);
    }
}
