package addtool.add2prism;

import addtool.add.model.ADD;
import addtool.add.model.BasicEvent;
import addtool.add.model.Player;
import addtool.helpers.Pair;
import addtool.helpers.PollSet;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class AvailableMovePoller {

    // Components, where only attacker BEs are present.
    private final List<SubtreeComponent> attackerComponents;
    // Components which consists of two players. True game.
    private final List<SubtreeComponent> gameComponents;

    // Call only with top-AND currently!
    public AvailableMovePoller(ADD add){
        List<SubtreeComponent> allComponents = SubtreePartitioner.partition(add);
        attackerComponents = new LinkedList<>();
        gameComponents = new LinkedList<>();
        for(SubtreeComponent component : allComponents){
            if(component.onlyAttacker()){
                attackerComponents.add(component);
            }
            else{
                gameComponents.add(component);
            }
        }
    }

    private AvailableMovePoller(Collection<SubtreeComponent> attackerComponents, Collection<SubtreeComponent> gameComponents,
                                Set<SubtreeComponent> sortIn) {
        this.attackerComponents = new LinkedList<>(attackerComponents);
        this.gameComponents = new LinkedList<>(gameComponents);
        for(SubtreeComponent subtreeComponent : sortIn){
            if(subtreeComponent.onlyAttacker()){
                this.attackerComponents.add(subtreeComponent);
            }
            else {
                this.gameComponents.add(subtreeComponent);
            }
        }
    }

    public Stream<Pair<AvailableMovePoller, Set<? extends BasicEvent>>> poll(Player player){
        if(gameComponents.isEmpty()){
            SubtreeComponent curComponent = attackerComponents.remove(0);
            return pollThenCopy(curComponent, player, attackerComponents, gameComponents);
        }
        else{
            Stream<Pair<AvailableMovePoller, Set<? extends BasicEvent>>> stream1 =
                    StreamSupport.stream(new PollSet<>(gameComponents).spliterator(), false)
                            .flatMap(comp -> pollThenCopy(comp.getFirst(), player, attackerComponents, comp.getSecond()));
            Stream<Pair<AvailableMovePoller, Set<? extends BasicEvent>>> stream2 =
                    StreamSupport.stream(new PollSet<>(attackerComponents).spliterator(), false)
                            .flatMap(comp -> pollThenCopy(comp.getFirst(), player, comp.getSecond(), gameComponents));
            return Stream.concat(stream1, stream2);
        }
    }

    private static Stream<Pair<AvailableMovePoller, Set<? extends BasicEvent>>> pollThenCopy
            (SubtreeComponent pollComponent, Player player,
             Collection<SubtreeComponent> attackerComponents, Collection<SubtreeComponent> gameComponents) {
        Stream<Pair<Set<SubtreeComponent>, Set<? extends BasicEvent>>> iter = pollComponent.poll(1, player);
        return iter.map(x -> new Pair<>(
                new AvailableMovePoller(attackerComponents, gameComponents, x.getFirst())
                , x.getSecond()));
    }

    public Stream<Pair<AvailableMovePoller, Set<? extends BasicEvent>>> poll(int n, Player player){
        if(n<=0){
            return Stream.of(new Pair<>(this, new HashSet<>()));
        }
        else if(n == 1){
            return poll(player);
        }
        else{
            // return poll(n-1, player).flatMap();
            // TODO Implement n BE polling. Not too easy... Which component to poll from and in which order?
            throw new UnsupportedOperationException();
        }
    }


}
