package addtool.add2prism;

import addtool.add.model.*;
import addtool.semantics.*;
import org.jscience.mathematics.number.Rational;

import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

final class PRISMModuleDefinition {

    private PRISMModuleDefinition() {
    }

    private static String startModule(String name) {
        return "module " + name;
    }

    private static String endModule() {
        return "endmodule";
    }

    private static String vertex2Variable(Vertex vertex, Valuation initial) {
        return 'v' + vertex.getId() + " : [-1..1] init " + PRISMModuleDefinition.value2Int(initial.getValue(vertex)) + ';';
    }

    static String vertex2VariableName(Vertex vertex) {
        return 'v' + vertex.getId();
    }


    private static String trigger2Guard(Set<Trigger> trigger) {
        StringBuilder builder = new StringBuilder();

        int i = 0;
        for (Trigger b : trigger) {
            builder.append(PRISMModuleDefinition.vertex2VariableName(b)).append("=1");
            i++;

            if (i != trigger.size()) {
                builder.append(" & ");
            }
        }

        return builder.toString();
    }

    private static String move2Guard(Move move, Valuation valuation, ADD add) {
        StringBuilder builder = new StringBuilder();

        // composed guard must be written in brackets
        builder.append('(');

        // only the correct players can execute events
        if (move.getPlayer() == Player.Attacker) {
            builder.append("turn=0");
        } else {
            builder.append("turn=1");
        }

        builder.append(" & ");

//        //
//        if (!move.getMove().isEmpty() || !valuation.getAllValuations().isEmpty()) {
//            builder.append(" & ");
//        }
//
//        for (BasicEvent b : move) {
//            builder.append(PRISMModuleDefinition.vertex2VariableName(b) + "=0");
//            if (b.isTriggerable()) {
//                builder.append(" & ");
//                builder.append(PRISMModuleDefinition.trigger2Guard(move.getTriggered(b)));
//            }
//
//            builder.append(" & ");
//        }
//
        int i = 0;
        for (Vertex vertex : add) {
            builder.append(PRISMModuleDefinition.vertex2VariableName(vertex));
            builder.append('=');
            builder.append(PRISMModuleDefinition.value2Int(valuation.getValue(vertex)));

            i++;
            if (i != (add.getVertices().size())) {
                builder.append(" & ");
            }
        }
        builder.append(')');
        return builder.toString();
    }

    private static String move2ActionName(Move move) {
        StringBuilder builder = new StringBuilder();
        for (BasicEvent event : move) {
            builder.append(PRISMPlayerDefinition.getActionName(event));
        }
        // default action if no basic event is taken
        if (move.getPlayer() == Player.Defender && move.isEmpty()) {
            builder.append(PRISMPlayerDefinition.getDefaultName());
        }
        return builder.toString();
    }

    private static String valuation2Change(Move move, Map<Vertex, Value> val) {
        StringBuilder builder = new StringBuilder();
        if (move.getPlayer() == Player.Attacker) {
            builder.append("(turn'=1)");
        } else {
            builder.append("(turn'=0)");
        }

        if (!val.isEmpty()) {
            builder.append(" & ");
        }

        int i = 0;
        for (Map.Entry<Vertex, Value> entry : val.entrySet()) {
            String name = PRISMModuleDefinition.vertex2VariableName(entry.getKey())+"'";
            int value = PRISMModuleDefinition.value2Int(entry.getValue());
            builder.append('(').append(name).append('=').append(value).append(')');

            i++;
            if (i != val.keySet().size()) {
                builder.append(" & ");
            }
        }

        return builder.toString();
    }

    private static String dist2Transition(State state, Move move, ADD add) {
        StringBuilder builder = new StringBuilder();
        String guard = PRISMModuleDefinition.move2Guard(move, state.getValuation(), add);
        String name = PRISMModuleDefinition.move2ActionName(move);

        builder.append(name);
        builder.append(' ');
        builder.append(guard);
        builder.append(" -> ");

        Distribution<State> dist = state.getSuccessor(move);
        int i = 0;
        for (State st : dist) {
            Rational prob = dist.lookUp(st);
            Map<Vertex, Value> changes = st.getValuation().getAllValuations();
            String set = PRISMModuleDefinition.valuation2Change(move, changes);
            builder.append(prob.floatValue()).append(':').append(set);
            i++;

            if (i != dist.getSupport().size()) {
                builder.append(" + ");
            }
        }

        builder.append(';');
        return builder.toString();
    }

    private static int value2Int(Value value) {
        if (value == Value.TRUE) {
            return 1;
        } else if (value == Value.FALSE) {
            return -1;
        } else {
            return 0;
        }
    }


    public static void module(PrintWriter writer, String name, Game game, ADD add) {
        writer.println(startModule(name));
        writer.println("turn : [0..1] init 0;");

        for (Vertex vertex : add) {
            writer.println(PRISMModuleDefinition.vertex2Variable(vertex, game.initialState().getValuation()));
        }

        for (State state : game) {
            for (Move move : state.getAvailableMoves()) {
                writer.println(PRISMModuleDefinition.dist2Transition(state, move, add));
            }
        }
        writer.println(endModule());
    }
}
