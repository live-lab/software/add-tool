package addtool.add2prism;

import java.io.PrintWriter;

public final class PRISMStateDefinition {

    private PRISMStateDefinition() {
    }

    private static String stateDefinition(String name, int lb, int ub, int init) {
        return name + " : [" + lb + ".." + ub + "] init " + init + ';';
    }

    public static void state(PrintWriter writer, String name, int lb, int ub, int init) {
        writer.println(stateDefinition(name, lb, ub, init));
    }
}
