package addtool.add2prism;


import addtool.add.model.*;

import java.util.*;
import java.util.stream.Collectors;

// If the top-event is an And we partition the BasicEvents into the ones of the respective disjoint subtrees of the ADD
public final class SubtreePartitioner {

    public static List<SubtreeComponent> partition(ADD add){
        return (new SubtreePartitioner(add)).getPartitions();
    }

    private final List<SubtreeComponent> partitions;
    // Contains the Set of roots for each BasicEvent
    private final Map<BasicEvent, Set<Integer>> parents = new HashMap<>();

    private SubtreePartitioner(ADD add){
        Vertex top = add.getTop();
        // Number of predecessor of top elements = number of roots
        int n = top.getPredecessors().size();
        if(!(top instanceof And)){
            throw new IllegalArgumentException("Only call SubtreePartitioner with add which has AND on top.");
        }
        add.getBasicEventsAsBEs().forEach(v -> parents.put(v, new HashSet<>()));
        // For each successor note which BasicEvent is in its subtree. May overlap!
        for(int i = 0; i< n; i++){
            List<Vertex> workset = new ArrayList<>();
            workset.add(top.getPredecessors().get(i));
            while(!workset.isEmpty()){
                Vertex cur = workset.remove(0);
                if(cur instanceof BasicEvent){
                    parents.get(cur).add(i);
                }
                else if (cur instanceof UnaryOp){
                    workset.addAll(cur.getPredecessors());
                }
                else if (cur instanceof NaryOp){
                    workset.addAll(cur.getPredecessors());
                }
            }
        }

        // reverse the map.
        // Contains the reverse of parents, so now every set of roots that appeared will be mapped to the BEs that
        // are part of _all_ subtrees started from the roots
        Map<Set<Integer>, Set<BasicEvent>> subtreeBEMap = parents.entrySet()
                .stream()
                .collect(Collectors.groupingBy(Map.Entry::getValue,
                        Collectors.mapping(Map.Entry::getKey, Collectors.toSet())));

        // Turn the Roots and the Set of Basic Events into Subtrees.
        List<Subtree> subtrees = new LinkedList<>();
        for(Map.Entry<Set<Integer>, Set<BasicEvent>> subtreeBEs : subtreeBEMap.entrySet()){
            Set<Integer> roots = subtreeBEs.getKey();
            Set<BasicEvent> bes = subtreeBEs.getValue();
            subtrees.add(new Subtree(roots, bes));
        }

        // Now we need to connect the subtrees which overlap (Like roots {1}, roots {2} and roots {1,2})
        // Into components.
        List<SubtreeComponent> subtreeComponents = new LinkedList<>();
        while(!subtrees.isEmpty()){
            // The subtree has not yet been added to any component.
            // So we take this as the starter for the next component.
            Subtree curSubtree = subtrees.remove(0);
            SubtreeComponent curComponent = new SubtreeComponent(curSubtree);
            subtreeComponents.add(curComponent);
            // We want to get all other subtrees that overlap with this subtree.
            // We add each to the current component that does.
            // We start with all subtrees, which have not yet been added to any components.
            // We do not need to check any prior subtrees, because overlapping is symmetric,
            // thus no prior subtree can overlap with the current component.
            List<Subtree> componentCandidates = new LinkedList<>(subtrees);
            while(!componentCandidates.isEmpty()){
                Subtree curCandidate = componentCandidates.remove(0);
                if(curComponent.addIfContained(curCandidate)){
                    // We found the component for the candidate.
                    subtrees.remove(curCandidate);
                    // Fill the candidates with all subtrees which are in no components yet
                    // Already checked candidates can now overlap.
                    componentCandidates = new LinkedList<>(subtrees);
                }
            }
        }
        partitions = subtreeComponents;

    }

    private List<SubtreeComponent> getPartitions() {
        return partitions;
    }
}
