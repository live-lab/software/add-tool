package addtool.add2prism;

import addtool.add.model.BasicEvent;
import addtool.add.model.Player;
import addtool.helpers.Pair;
import addtool.helpers.PollSet;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

// All subtrees of a component overlap with one another.
class SubtreeComponent {

    private final Set<Subtree> subtrees;
    // The edges, which represent, that two subtrees have common nodes.
    private final Map<Subtree, List<Subtree>> adjacencyList;

    protected SubtreeComponent(Subtree subtreeStart){
        this.subtrees = new HashSet<>();
        this.subtrees.add(subtreeStart);
        this.adjacencyList = new HashMap<>();
        this.adjacencyList.put(subtreeStart, new LinkedList<>());
    }

    private SubtreeComponent(Set<Subtree> subtrees, Map<Subtree, List<Subtree>> adjacencyList){
        this.subtrees = subtrees;
        this.adjacencyList = adjacencyList;
    }

    //new Subtree is contained in current component if it overlaps with at least one current subtree.
    protected boolean addIfContained(Subtree newSubtree){
        boolean success = false;
        for(Subtree subtree : subtrees) {
            Set<Integer> intersection = subtree.getRoots();
            intersection.retainAll(subtree.getRoots());
            if(!intersection.isEmpty()){
                success = true;
                adjacencyList.getOrDefault(newSubtree, new LinkedList<>()).add(subtree);
                adjacencyList.get(subtree).add(newSubtree);
            }
        }
        return success;
    }

    protected boolean onlyAttacker(){
        return subtrees.stream().allMatch(Subtree::onlyAttacker);
    }



    private Set<SubtreeComponent> copySplit(Subtree newSubtree, Set<Subtree> newSubtrees){
        if(newSubtree.isEmpty()){
            // Split the current component if possible.
            // Check if newSubtrees are still connected, otherwise split into more components.
            // Return Set of split components.

            List<Set<Subtree>> subtreePartitions = split(newSubtrees);
            return subtreePartitions.stream()
                    .map(set -> new SubtreeComponent(set, constrainAdjacencyList(set)))
                    .collect(Collectors.toSet());
        }
        else{
            newSubtrees.add(newSubtree);
            HashSet<SubtreeComponent> set = new HashSet<>();
            set.add(new SubtreeComponent(newSubtrees, adjacencyList));
            return set;
        }
    }

    private List<Set<Subtree>> split(Set<Subtree> newSubtrees) {
        List<Set<Subtree>> returnSet = new LinkedList<>();
        while(!newSubtrees.isEmpty()){
            Subtree current = newSubtrees.stream().findFirst().get();
            newSubtrees.remove(current);
            Set<Subtree> curComponent = dfs(current, newSubtrees);
            returnSet.add(curComponent);
        }
        return returnSet;
    }

    private Set<Subtree> dfs(Subtree root, Set<Subtree> subtrees) {
        Set<Subtree> returnSet = new HashSet<>();
        returnSet.add(root);
        List<Subtree> workSet = new LinkedList<>();
        workSet.add(root);
        while(!workSet.isEmpty()){
            Subtree current = workSet.remove(0);
            for(Subtree neighbor : adjacencyList.get(current)){
                if(!returnSet.contains(neighbor)){
                    workSet.add(neighbor);
                    returnSet.add(neighbor);
                }
            }
        }
        return returnSet;
    }

    private Map<Subtree, List<Subtree>> constrainAdjacencyList(Set<Subtree> retain) {
        HashMap<Subtree, List<Subtree>> newAdjacencyList = new HashMap<>();
        for(Map.Entry<Subtree, List<Subtree>> entry : adjacencyList.entrySet()){
            Subtree subtree = entry.getKey();
            // Only keep List, if I should retain current Subtree
            if(retain.contains(subtree)){
                List<Subtree> neighbors = new LinkedList<>(entry.getValue());
                // Keep only neighbors in 'retain'
                neighbors.retainAll(retain);
                newAdjacencyList.put(subtree, neighbors);
            }
        }
        return newAdjacencyList;
    }

    // Poll from subtree, then split the results.
    private Stream<Pair<Set<SubtreeComponent>, Set<? extends BasicEvent>>> pollThenSplit
            (Subtree subtree, Set<Subtree> remaining, int n, Player player){
        Stream<Pair<Subtree,Set<? extends BasicEvent>>> newSubtrees = subtree.poll(n, player);
        return newSubtrees.map(s -> new Pair<>(copySplit(s.getFirst(), remaining), s.getSecond()));
    }

    public Stream<Pair<Set<SubtreeComponent>, Set<? extends BasicEvent>>> poll(int n, Player player) {
        return StreamSupport.stream(new PollSet<>(subtrees).spliterator(), false)
                .flatMap(pair -> pollThenSplit(pair.getFirst(), pair.getSecond(), n, player));
    }

    /*
    protected boolean isEmpty(){
        return subtrees.stream().allMatch(Subtree::isEmpty);
    }*/
}
