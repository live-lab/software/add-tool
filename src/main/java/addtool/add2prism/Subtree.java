package addtool.add2prism;

import addtool.add.model.BasicEvent;
import addtool.add.model.BasicEventPlayer;
import addtool.add.model.BasicEventTime;
import addtool.add.model.Player;
import addtool.helpers.Pair;
import addtool.helpers.PowerSet;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Subtree {
    // The set of roots, this subtree is rooted in. Can be >1 because the dag can overlap after the children.
    // A root is the direct predecessor of the top node.
    private final Set<Integer> roots = new HashSet<>();
    // The set of basicEvents, which are rooted in all the roots.
    private final Set<BasicEventPlayer> basicEventsAttacker = new HashSet<>();
    private final Set<BasicEventPlayer> basicEventsDefender = new HashSet<>();
    private final Set<BasicEventTime> basicEventsTime = new HashSet<>();

    private Subtree(Subtree subtree){
        this.roots.addAll(subtree.roots);
        this.basicEventsAttacker.addAll(subtree.basicEventsAttacker);
        this.basicEventsDefender.addAll(subtree.basicEventsDefender);
        this.basicEventsTime.addAll(subtree.basicEventsTime);
    }

    public Subtree(int root, Set<BasicEvent> basicEvents){
        this.roots.add(root);
        fillBasicEvents(basicEvents);
    }

    private void fillBasicEvents(Set<BasicEvent> basicEvents) {
        for(BasicEvent be : basicEvents){
            if(be instanceof BasicEventPlayer && ((BasicEventPlayer) be).isAttacker()){
                basicEventsAttacker.add((BasicEventPlayer) be);
            }
            else if(be instanceof BasicEventPlayer && ((BasicEventPlayer) be).isDefender()){
                basicEventsDefender.add((BasicEventPlayer) be);
            }
            else if(be instanceof BasicEventTime){
                basicEventsTime.add((BasicEventTime) be);
            }
            else{
                throw new RuntimeException("BasicEvent must be either Player or Time");
            }
        }
    }

    public Subtree(Set<Integer> roots, Set<BasicEvent> basicEvents){
        this.roots.addAll(roots);
        fillBasicEvents(basicEvents);
    }

    public boolean onlyAttacker(){
        return this.basicEventsDefender.isEmpty() && this.basicEventsTime.isEmpty();
    }

    public boolean isEmpty(){
        return this.basicEventsDefender.isEmpty() && this.basicEventsTime.isEmpty()
                && this.basicEventsAttacker.isEmpty();
    }

    private Subtree copyRemove(Player player, Set<BasicEventPlayer> toRemove){
        Subtree copy = new Subtree(this);
        switch (player){
            case Attacker -> copy.basicEventsAttacker.removeAll(toRemove);
            case Defender -> copy.basicEventsDefender.removeAll(toRemove);
        }
        return copy;
    }

    public Set<Integer> getRoots() {
        return new HashSet<>(this.roots);
    }

    public Stream<Pair<Subtree, Set<? extends BasicEvent>>> poll(int n, Player player) {
        PowerSet<BasicEventPlayer> it = switch (player) {
            case Attacker -> new PowerSet<>(basicEventsAttacker, 1, n);
            case Defender -> new PowerSet<>(basicEventsDefender, n);
        };
        return StreamSupport.stream(it.spliterator(), false)
                .map(set -> new Pair<>(copyRemove(player, set), set));
    }
}
