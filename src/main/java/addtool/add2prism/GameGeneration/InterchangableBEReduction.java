package addtool.add2prism.GameGeneration;

import addtool.add.model.*;
import addtool.semantics.Move;
import addtool.semantics.State;

import java.util.Collection;
import java.util.stream.Collectors;

public final class InterchangableBEReduction implements AvailableMoveImplementation {

    DefaultAvailableMoves defaultImpl = new DefaultAvailableMoves();

    @Override
    public Collection<Move> availableMoves(State state){
        Collection<BasicEventPlayer> bes = state.getValuation().available(state.player);

        Collection<Move> allMoves = defaultImpl.availableMoves(state);

        // Filter out, if directly below And/Or, the And/Or only contains BEs which themselves have only one predecessor
        // and one of the BEs has a lower ID than the current be.
        return allMoves.stream().filter(m -> m.getMove().stream().noneMatch(be ->
                        be.getSuccessors().size() == 1 &&
                                be.getSuccessors().size() == 1 && (be.getSuccessors().get(0) instanceof And ||
                                be.getSuccessors().get(0) instanceof Or)
                                && be.getSuccessors().get(0).getPredecessors().stream()
                                .anyMatch(v -> v instanceof BasicEventPlayer
                                        && bes.contains(v) && !m.contains(v)
                                        && ((BasicEventPlayer) v).getSuccessProbability().equals(be.getSuccessProbability())
                                        && v.getId().compareTo(be.getId()) < 0)))
                .collect(Collectors.toList());
    }

/*
    @Override
    public Iterable<Move> availableMoves(State state){
        if(state.player == Player.Defender){
            return defaultImpl.availableMoves(state);
        }
        // All BEs that are available to the current player.
        Stream<BasicEvent> bes = state.getValuation().available(state.player).stream();

        // All BEs
        Stream<BasicEventPlayer> allBes = state.getValuation().remainingBasicEvents();
        // Only apply reduction if all remaining events belong to the current player.
        if(allBes.anyMatch(be -> be.getPlayer() != Player.Attacker)){
            return bes.map(be -> new Move(Collections.singleton(be), state.getADD(), state.player))
                    .collect(Collectors.toList());
        }

        // Filter out, if directly below AND, the AND only contains BEs which themselves have only one predecessor
        // and one of the BEs has a lower ID than the current be.
        return bes.filter(be -> !(be.getSuccessors().size() == 1 && be.getSuccessors().get(0) instanceof And &&
                        be.getSuccessors().get(0).getPredecessors().stream()
                                .allMatch(v -> v instanceof BasicEvent && v.getSuccessors().size() == 1) &&
                        be.getSuccessors().get(0).getPredecessors().stream()
                                .anyMatch(v -> v instanceof BasicEvent && v.getId().compareTo(be.getId()) < 0)))
                .map(be -> new Move(Collections.singleton(be), state.getADD(), state.player))
                .collect(Collectors.toList());
    }
*/
    /*
    Recursive AmpleSet Definition. Will be used later.

    private List<Move> ampleSet(State state){
        // All Basic Events that are available.
        Stream<BasicEvent> bes = ampleSet(state.getValuation().getInitialVertex());
        return bes.map(be -> new Move(Collections.singleton(be), add, state.getPlayer()))
                .collect(Collectors.toList());
    }

    private Stream<BasicEvent> ampleSet(Vertex v){
        if(v instanceof BasicEvent){
            return Stream.of((BasicEvent) v);
        }
        else if(v instanceof And){
            if(v.getPredecessors().stream().allMatch(ver -> ver instanceof BasicEvent && v.getSuccessors().size()==1)){

            }
        }

        return v.getPredecessors().stream().flatMap(this::ampleSet);
    }
    */

}
