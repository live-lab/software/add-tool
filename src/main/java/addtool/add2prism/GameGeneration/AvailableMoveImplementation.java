package addtool.add2prism.GameGeneration;

import addtool.semantics.Move;
import addtool.semantics.State;

import java.util.Collection;

@FunctionalInterface
public interface AvailableMoveImplementation {
    Collection<Move> availableMoves(State state);
}
