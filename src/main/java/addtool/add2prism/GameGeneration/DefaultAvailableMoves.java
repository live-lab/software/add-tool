package addtool.add2prism.GameGeneration;

import addtool.add.model.BasicEventPlayer;
import addtool.add.model.Player;
import addtool.helpers.PowerSet;
import addtool.semantics.Move;
import addtool.semantics.State;
import com.google.common.collect.Streams;
import org.jscience.mathematics.number.Rational;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static addtool.add2prism.MainDot2Semantics.*;

public class DefaultAvailableMoves implements AvailableMoveImplementation{

    /**
     * returns the available moves from a given state
     *
     * @param state the state whose available moves are to be found
     * @return an iterable over the available moves
     */
    @Override
    public Collection<Move> availableMoves(State state) {
        Set<BasicEventPlayer> allBEs = state.getValuation().available(state.player);
        return availableMoves(state, allBEs);
    }

    public Collection<Move> availableMoves(State state, Collection<BasicEventPlayer> bes) {
        int min = state.player==Player.Attacker?1:0;
        Rational costBound = state.player==Player.Attacker?costBoundAttacker:costBoundDefender;
        PowerSet<BasicEventPlayer> availableMoves = new PowerSet<>(bes, min, numberAttacker);
        Stream<Move> ava = Streams.stream((Iterable<Set<BasicEventPlayer>>) availableMoves)
                .map(x -> new Move(x, state.getADD(), state.player));
        if(!costBound.isNegative()){
            ava = ava.filter(m -> !state.getValuation().getCost().plus(Rational.valueOf(m.getCost(), 1)).isLargerThan(costBound));
        }
        return ava.collect(Collectors.toList());
    }
}
