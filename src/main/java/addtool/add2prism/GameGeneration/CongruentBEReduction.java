package addtool.add2prism.GameGeneration;

import addtool.add.model.*;
import addtool.semantics.Move;
import addtool.semantics.State;
import org.jscience.mathematics.number.Rational;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class CongruentBEReduction implements AvailableMoveImplementation {

    DefaultAvailableMoves defaultImpl = new DefaultAvailableMoves();

    @Override
    public Collection<Move> availableMoves(State state){
        Collection<BasicEventPlayer> bes = state.getValuation().available(state.player);

        Collection<Move> allMoves = defaultImpl.availableMoves(state);

        // Filter out, if any be is (==keep if non are) directly below AND/OR, the AND/OR contains a BE that is lower in the
        // relation that is not in move (also has only single Successor).
        return allMoves.stream().filter(m -> m.getMove().stream().noneMatch(be ->
                        be.getSuccessors().size() == 1 && be instanceof BasicEventPlayer &&
                                existsSmallerPreNotInMove(be.getSuccessors().get(0), (BasicEventPlayer) be, m, bes)))
                .collect(Collectors.toList());
    }

    // Returns -1 if the Gate is purely bad for the defender, 1 if the gate is purely good and 0 if mixed
    private int getParity(Vertex v){
        if(v instanceof Nat || v instanceof NotTrue|| v instanceof SAnd || v instanceof SOr || v instanceof  Trigger)
            return 0;
        if(v.getSuccessors().isEmpty()) return 1;
        List<Integer> successorValues = v.getSuccessors().stream().map(this::getParity).collect(Collectors.toList());
        if(successorValues.stream().allMatch(x -> x>0)){
            if(v instanceof Not){
                return -1;
            }
            else{
                return 1;
            }
        }
        if(successorValues.stream().allMatch(x -> x<0)){
            if(v instanceof Not){
                return 1;
            }
            else{
                return -1;
            }
        }
        return 0;
    }

    private boolean existsSmallerPreNotInMove(Vertex parent, BasicEventPlayer be, Move m, Collection<BasicEventPlayer> bes) {
        if((parent instanceof And || parent instanceof Or)) {
            for(Vertex v : parent.getPredecessors()){
                if(v instanceof BasicEventPlayer && be.getSuccessors().size() == 1 && !be.equals(v)
                        && ((BasicEventPlayer) v).getPlayer() == be.getPlayer() && !m.contains(v) && bes.contains(v)){
                    BasicEventPlayer be2 = (BasicEventPlayer) v;
                    Rational prob1 = be.getSuccessProbability();
                    Rational prob2 = be2.getSuccessProbability();
                    Rational cost1 = be.getCost();
                    Rational cost2 = be2.getCost();
                    int parity = getParity(parent);
                    if(parity > 0 && be.getPlayer() == Player.Attacker
                            || parity < 0 && be.getPlayer() == Player.Defender){
                        // Get highest prob (tie -> lowestCost (tie -> lowest lexicographic))
                        if(prob2.isLessThan(prob1) ||
                                (prob2.equals(prob1) &&
                                    (cost2.isLessThan(cost1) ||
                                        cost2.equals(cost1) && be2.getId().compareTo(be.getId()) < 0))){
                            return true;
                        }
                    }
                    else if(parity < 0 && be.getPlayer() == Player.Attacker
                            || parity > 0 && be.getPlayer() == Player.Defender){
                        // Get lowest prob (tie -> lowestCost (tie -> lowest lexicographic))
                        if(prob1.isLessThan(prob2) ||
                                (prob2.equals(prob1) &&
                                        (cost2.isLessThan(cost1) ||
                                                cost2.equals(cost1) && be2.getId().compareTo(be.getId()) < 0))){
                            return true;
                        }
                    }
                    else{
                        if(prob2.equals(prob1) &&
                                (cost2.isLessThan(cost1) ||
                                        (cost2.equals(cost1) && be2.getId().compareTo(be.getId()) < 0))){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

}
