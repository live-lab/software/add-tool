/**
 * Exports ADDs to Prism
 * See ADD2Prism.translateGame
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 16.09.15
 */
package addtool.add2prism;