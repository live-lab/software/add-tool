package addtool.add2prism;

import addtool.add.io.ADDExport;
import addtool.add.model.ADD;
import addtool.add.model.BasicEvent;
import addtool.add.model.BasicEventPlayer;
import addtool.add.model.Vertex;
import addtool.helpers.TranslationTable;
import addtool.semantics.Game;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is used save ADDs as PRISM-Files.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 17.10.15
 * @since 17.10.15
 */
public final class ADD2Prism implements ADDExport {
    public static final String INDENT = "    ";

    public ADD2Prism() {
    }

    public static void translateGame(ADD add, Game game, PrintWriter writer) {
        writer.println("smg");
        writer.println();
        Set<BasicEvent> attMoves = new HashSet<>();
        Set<BasicEvent> defMoves = new HashSet<>();

        for (Vertex vertex : add) {
            if (vertex instanceof BasicEventPlayer) {
                if (((BasicEventPlayer) vertex).isAttacker()) {
                    attMoves.add((BasicEvent) vertex);
                } else {
                    defMoves.add((BasicEvent) vertex);
                }
            }
        }

        if (!attMoves.isEmpty()) {
            PRISMPlayerDefinition.player(writer, "attacker", attMoves, false);
            writer.println();
            PRISMPlayerDefinition.player(writer, "defender", defMoves, true);
            writer.println();
        }
        PRISMModuleDefinition.module(writer, "ADD", game, add);
        writer.println();
        writer.println("label \"success\" = " + PRISMModuleDefinition.vertex2VariableName(add.getTopAtt()) + "=1;");
        writer.println("label \"fail\" = " + PRISMModuleDefinition.vertex2VariableName(add.getTopAtt()) + "=-1;");

        writer.println();
        writer.println("rewards \"costAttacker\"");
        for (BasicEvent b : attMoves) {
            writer.println(PRISMPlayerDefinition.getActionName(b) + "true : " + b.getCost() + ';');
        }
        writer.println("endrewards");
    }

    @Override
    public String getName() {
        return TranslationTable.PRISM;
    }

    @Override
    public String getFileExtension() {
        return ".prism";
    }

    @Override
    public void exportModel(ADD model, File f) throws IOException {
        PrintWriter writer=new PrintWriter(f, StandardCharsets.UTF_8);
        Game game = MainDot2Semantics.generateSemantics(model);
        ADD2Prism.translateGame(model, game, writer);
        writer.flush();
        writer.close();
    }
}
