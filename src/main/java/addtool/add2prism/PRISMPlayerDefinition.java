package addtool.add2prism;

import addtool.add.model.BasicEvent;

import java.io.PrintWriter;
import java.util.Set;

public final class PRISMPlayerDefinition {

    private PRISMPlayerDefinition() {
    }

    private static String startPlayer(String name) {
        return "player " + name;
    }

    private static String endPlayer() {
        return "endplayer";
    }

    public static String getActionName(BasicEvent b) {
        return "[a" + b.getId() + "]";
    }

    public static String getDefaultName() {
        return "[default]";
    }

    private static String actionsPlayer(Set<BasicEvent> moves, boolean defender) {
        StringBuilder names = new StringBuilder();
        int i = 0;
        for (BasicEvent b : moves) {
            names.append(PRISMPlayerDefinition.getActionName(b));
            i++;
            if (i != moves.size() || defender) {
                names.append(", ");
            }
        }
        if (defender) {
            names.append(PRISMPlayerDefinition.getDefaultName());
        }
        return names.toString();
    }

    // mods contains both the names of the modules AND the actions a player controls
    public static void player(PrintWriter writer, String name, Set<BasicEvent> moves, boolean defender) {
        writer.println(startPlayer(name));
        writer.println(ADD2Prism.INDENT + actionsPlayer(moves, defender));
        writer.println(endPlayer());
    }
}
