package addtool.add2prism;

import addtool.add.model.ADD;
import addtool.add2prism.GameGeneration.AvailableMoveImplementation;
import addtool.add2prism.GameGeneration.DefaultAvailableMoves;
import addtool.analysisonadd.ProbabilityHeuristic;
import addtool.analysisongame.ProbabilityVisitor;
import addtool.dot2add.*;
import addtool.semantics.*;
import org.jscience.mathematics.number.Rational;

import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public final class MainDot2Semantics {

    public static final int numberAttacker = 1;
    public static final int numberDefender = 1;
    public static final Rational costBoundAttacker = Rational.valueOf(-1, 1);
    public static final Rational costBoundDefender = Rational.valueOf(-1, 1);

    private AvailableMoveImplementation ava = new DefaultAvailableMoves();

    private MainDot2Semantics() {
        throw new AssertionError("Instantiating utility class MainDot2Semantics");
    }

    public static void main(String... args) throws IOException, NoValueSpecifiedException, WrongValueForLabelOperatorException, WrongStructureADDException, WrongValueForSinkException {
        if(args.length>=1){
            run(args[0]);
        }
        else{
            System.err.println("One argument expected, none given.");
        }
    }

    private static void run(String arg) throws IOException, WrongValueForLabelOperatorException, NoValueSpecifiedException, WrongStructureADDException, WrongValueForSinkException{
        FileReader in = new FileReader(arg);
        ADD add = ParserDOT.parseADD(in);
        ProbabilityHeuristic heuristic = new ProbabilityHeuristic();
        add.accept(heuristic);
        Game semantics = generateSemantics(add);
        ProbabilityVisitor probVis = new ProbabilityVisitor(semantics);
        semantics.accept(probVis);
    }


    public static Game generateSemantics(ADD add, AvailableMoveImplementation ava) {
        Game semantics = new Game(add);
        State initialState = semantics.initialState();

        if (initialState.getValuation().getValue(add.getTop()) == Value.TRUE) {
            return semantics;
        }

        // LIFO Queue of all discovered and unprocessed states.
        LinkedList<State> queue = new LinkedList<>();
        queue.add(initialState);
        initialState.setDiscovered();

        int i = 0;
        int queueSize = 1;
        int gameSize = 1;

        // compute all necessary states and transitions
        while (!queue.isEmpty()) {
            queueSize--;
            i++;
            // get next valuation out of the queue and compute available moves for the current player
            State state = queue.removeLast();

            if(i % 10000 == 0) {
                System.out.println("Yey, we got up to " + i + ", queue-size is "
                        + queueSize + ", game-size is " + gameSize);
            }

            // compute all moves (depends on the number of events the players can attempts simultaneously)
            Iterable<Move> availableMoves = ava.availableMoves(state);

            // compute the resulting distribution for each move
            for (Move move : availableMoves) {

                Distribution<State> dist = semantics.createSuccessorStates(state, move);
                state.addSuccessor(move, dist);

                for (State newState : dist) {
                    if(newState.isNew()){
                        gameSize++;
                        newState.setDiscovered();
                        if(newState.rootValue() == Value.UNDECIDED) {
                            queueSize++;
                            queue.add(newState);
                        }
                    }
                }
            }
        }
        System.out.println("Iterations: " + i);
        return semantics;
    }

    public static Game generateSemantics(ADD add) {
        return generateSemantics(add, new DefaultAvailableMoves());
    }

}
