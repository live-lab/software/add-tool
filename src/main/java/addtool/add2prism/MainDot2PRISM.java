package addtool.add2prism;

import addtool.add.model.ADD;
import addtool.dot2add.ParserDOT;
import addtool.helpers.ExceptionHandler;
import addtool.semantics.Game;

import java.io.FileReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

/**
 * starting point to translate dot files specifying ADDs syntactically to modest representing ADD semantics.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 15.03.15
 */
public final class MainDot2PRISM {

    private MainDot2PRISM() {
    }

    public static void main(String... args) throws Exception {
        if(args.length>=2){
            convert2prism(args[0],args[1]);
        }else if(args.length>=1){
            convert2prism(args[0]);
        }else{
            System.err.println("Insufficient arguments. Must be at least one file.");
        }
    }

    public static void convert2prism(String infile)throws Exception {
        convert2prism(infile,infile+".prism");
    }

    public static void convert2prism(String infile,String outfile) throws addtool.dot2add.WrongValueForLabelOperatorException, addtool.dot2add.NoValueSpecifiedException, addtool.dot2add.WrongStructureADDException, addtool.dot2add.WrongValueForSinkException, java.io.IOException {
        try {
            long startTime = System.currentTimeMillis();


            FileReader in = new FileReader(infile);
            ADD add = ParserDOT.parseADD(in);
            // translation to game
            Game game = MainDot2Semantics.generateSemantics(add);

            long middleTime = System.currentTimeMillis();
            PrintWriter writer = new PrintWriter(outfile, StandardCharsets.UTF_8);
            //game to PRISM input
            ADD2Prism.translateGame(add, game, writer);
            writer.flush();
            writer.close();

            long stopTime = System.currentTimeMillis();
            long generationTime = middleTime - startTime;
            long writeOut = stopTime - middleTime;
            System.out.println("generation: " + generationTime);
            System.out.println("writeout: " + writeOut);
        } catch (java.io.FileNotFoundException e) {
            ExceptionHandler.logException(e);
        }
    }
}
