package addtool.helpers.preferences;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for the new Preference API
 */
public abstract class Preference implements Comparable{
    private final String key;
    private final String title;
    private final String group;
    protected List<ActionListener> listeners;
    private boolean hidden;

    /**
     * Generates a new Preference
     * @param key the unique key for saving and finding it
     * @param title the displayed title or a key for a resource file
     * @param group the group string for organisation in the edit dialog
     */
    public Preference(String key,String title, String group){
        this.key=key;
        this.title=title;
        this.group=group;
        this.hidden=false;
        listeners=new ArrayList<>();
    }

    /**
     * Returns key
     * @return the key
     */
    public String getKey(){
        return key;
    }

    /**
     * Returns human-readable title
     * @return the title of the preference
     */
    public String getTitle(){
        return title;
    }

    /**
     * Returns the preference group code
     * @return the group path
     */
    public String getGroup(){
        return group;
    }

    /**
     * Returns the value of the preference
     * @return an object depending on the preference subclass
     */
    public abstract Object getValue();

    /**
     * Returns the current value as a string
     * @return a string representing the current value
     */
    public abstract String getValueString();

    /**
     * Updates the value from a string
     * @param s the string to be parsed in the preference value
     */
    public abstract void setValue(String s);

    /**
     * Returns a component for editing the preference value
     * @return a jcomponent
     */
    public abstract JComponent getEditComponent();

    /**
     * Reads values from edit components
     */
    public void setValueFromComponent(){
        activateListeners();
    }
    protected void activateListeners(){
        ActionEvent ae=new ActionEvent(this,0,"value_changed");
        listeners.forEach(l->l.actionPerformed(ae));
    }

    /**
     * Adds an action listener. It will be activated when the preference value is changed
     * @param al an action listener
     */
    public void addActionListener(ActionListener al){
        listeners.add(al);
    }

    /**
     * Remove an actionlistener
     * @param al the action listener
     */
    public void removeActionListener(ActionListener al){
        listeners.remove(al);
    }

    /**
     * Removes all action listeners
     */
    public void removeAllListeners(){
        listeners.clear();
    }

    /**
     *
     * @return A MenuItem to edit the preference
     */

    public abstract JComponent getMenuItem();

    public String toString(){
        return String.format("KEY: %s, TITLE: %s, GROUP: %s, VALUE: %s, TYPE: %s",
                getKey(),getTitle(),getGroup(),getValueString(),getClass().getSimpleName());
    }
    @Override
    public int compareTo(@NotNull Object o) {
        if(o instanceof Preference){
            return ((Preference) o).getKey().compareTo(this.getKey());
        }
        return -1;
    }

    /**
     * Checks if the preference is hidden. i.e. not displayed in the edit dialog
     * @return true if it will not be displayed in the edit dialog
     */
    public boolean isHidden(){
        return hidden;
    }

    /**
     * Sets the preference hidden in the edit dialog
     * @param show true if it is hidden and should not be displayed in the edit dialog
     */
    public void setVisible(boolean show){
        this.hidden=!show;
    }
}
