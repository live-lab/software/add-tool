package addtool.helpers.preferences;

import addtool.helpers.Constants;
import addtool.scripts.Main;
import java.io.File;
import java.io.FileFilter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class to handle Preference pre settings
 */
public class PreferenceProfile {
    /**
     * File extension of profiles
     */
    public static final String PROFILE_ENDING="_profile.xml";
    static List<PreferenceProfile> defaultProfiles=new ArrayList<>();
    static {
        defaultProfiles.add(new PreferenceProfile("beginner","beginner.xml"));
        defaultProfiles.add(new PreferenceProfile("advanced","advanced.xml"));
        defaultProfiles.add(new PreferenceProfile("expert","expert.xml"));
        //load profiles from plugins
        File[] profiles = new File(Constants.PLUGIN_FOLDER).
                listFiles(pathname -> pathname.getAbsolutePath().endsWith(PROFILE_ENDING));
        if(profiles!=null) {
            for (File f : profiles) {
                String title = f.getName().substring(0, f.getName().length() - PROFILE_ENDING.length());
                defaultProfiles.add(new PreferenceProfile(title, f));
            }
        }
    }

    private Map<String, String> preferences;

    /**
     * Returns the default shipped profiles
     * @return a list of preference profiles
     */
    public static List<PreferenceProfile> getProfiles(){
        return defaultProfiles.subList(0,defaultProfiles.size());
    }

    private String title;

    /**
     * Creates an empty profile with the given title as name
     * @param title the name of the profile
     */
    public PreferenceProfile (String title){
        this.title=title;
        preferences=Map.of();
    }
    /**
     * Creates an empty profile with the given title as name and loads preferences from file
     * @param title the name of the profile
     * @param resourcePath A path to a file to read the profile from
     */
    public PreferenceProfile(String title,String resourcePath) {
        this(title);
        InputStream in = PreferenceProfile.class.getResourceAsStream(
                Constants.getResourceFolder()+ resourcePath);
        if(in!=null){
            preferences=PreferenceManager.readPreferences(in).
                    values().stream().collect(Collectors.toMap(v->v.getKey(),(v)->v.getValueString()));
        }else{
            System.err.println(resourcePath+" was not found.");
        }
    }
    /**
     * Creates an empty profile with the given title as name and loads preferences from file
     * @param title the name of the profile
     * @param from A file to read the profile from
     */
    public PreferenceProfile(String title,File from){
        this(title);
        if(from.exists()){
            preferences=PreferenceManager.readPreferences(from).
                    values().stream().collect(Collectors.toMap(v->v.getKey(),(v)->v.getValueString()));
        }
    }

    /**
     * Get title of preference profile
     * @return the title
     */
    public String getTitle(){
        return title;
    }

    /**
     * updates the preference map to values from the current profile
     * @param oldValues the map to update
     */
    public void loadTo(Map<String,Preference> oldValues){
        oldValues.forEach((k,p)->{
            if(preferences.containsKey(p.getKey())){
                p.setValue(preferences.get(p.getKey()));
                p.activateListeners();
            }
        });
    }
}
