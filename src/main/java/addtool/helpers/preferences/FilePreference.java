package addtool.helpers.preferences;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;
/**
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * A preference for files
 * Will be stored as path
 * Allows editing via a c{@link JFileChooser}
 */
public class FilePreference extends Preference{
    File value,tmp;
    JButton component;
    ActionListener actionListener;
    FileFilter filter;
    /**
     * The maximum amount of chars displayed in the edit button
     */
    public static final int MAX_LENGTH=20;
    JMenu menu;
    /**
     * Generates a new Preference
     * @param key the unique key for saving and finding it
     * @param title the displayed title or a key for a resource file
     * @param group the group string for organisation in the edit dialog
     * @param value the initial value as path to file
     */
    public FilePreference(String key,String title, String group, String value){
        super(key,title,group);
        this.value= new File(value);
        this.tmp=this.value;
        component=new JButton();
        actionListener=e->{
            JFileChooser jfc=new JFileChooser(this.tmp.getParentFile());
            jfc.setFileFilter(filter);
            int result=jfc.showDialog(component,getResource("select_file"));
            if(result==JFileChooser.APPROVE_OPTION){
                this.tmp=jfc.getSelectedFile();
                component.setText(getLabel(tmp));
            }
        };
        component.addActionListener(actionListener);
        component.setToolTipText(getTooltip(getTitle()));
        menu=new JMenu(getResource(getTitle()));
        menu.addActionListener(actionListener);
        filter=new FileFilter() {
            @Override
            public boolean accept(File f) {
                return true;
            }

            @Override
            public String getDescription() {
                return getResource("all_files");
            }
        };
    }
    private String getLabel(File path){
        if(filter.accept(tmp)){
            String str=path.getAbsolutePath();
            if(str.length()>MAX_LENGTH){
                return "..."+str.substring(str.length()-MAX_LENGTH);
            }
            return str;
        }else{
            return getResource("select_path");
        }
    }

    /**
     * Sets a filenamefilter for file selection
     * @param filter a new filter
     */
    public void setFileNameFilter(FileFilter filter){
        this.filter=filter;
    }
    @Override
    public Object getValue() {
        return new File(value.getAbsolutePath());
    }

    @Override
    public String getValueString() {
        return value.getAbsolutePath();
    }

    @Override
    public void setValue(String s) {
        this.value=new File(s);
        this.tmp=value;
        component.setText(getLabel(tmp));
        menu.setText(getLabel(tmp));
    }

    @Override
    public JComponent getEditComponent() {
        component.setText(getLabel(tmp));
        return component;
    }

    @Override
    public void setValueFromComponent() {
        value=tmp;
        super.setValueFromComponent();
    }

    @Override
    public JMenuItem getMenuItem() {
        return menu;
    }

    /**
     * Start a search for a file complying to the file name filter 
     * @see FilePreference#setFileNameFilter(FileFilter) 
     */
    public void tryFindFile(){
        //Only do this if value invalid
        if((filter.accept(value)&&value.isFile())||value.getAbsolutePath().contains("NOTFOUND")){
            //No search as the file is either valid or a frustran search was done
            return;
        }
        ExecutorService runner= Executors.newSingleThreadExecutor();
        runner.submit(()->{
            long millis=System.currentTimeMillis();
            File[] systemRoots = File.listRoots();
            FileSystemView fsv = FileSystemView.getFileSystemView();
            for (File root: systemRoots) {
                try {
                    Recurse r = new Recurse();
                    Files.walkFileTree(root.toPath(), r);
                    if((filter.accept(value)&&value.isFile())){
                        //No further search if found
                        return;
                    }
                    //Optional<Path> first = Files.walk(root.toPath()).filter(p->!p.startsWith("$")&&!p.startsWith("."))
                    //        .peek(System.out::println).filter(p -> filter.accept(p.toFile())&&p.toFile().isFile()).findFirst();

                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            //Set Value so search is not done every time
            setValue("NOTFOUND");

        });
        runner.shutdown();
    }
    class Recurse implements FileVisitor<Path> {

        private long filesCount;
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            //This is where I need my logic
            if(file.toFile().isFile()&&filter.accept(file.toFile())){
                if(!(filter.accept(value)&&value.isFile())){
                    //do not override if user set a value on his own
                    setValue(file.toFile().getAbsolutePath());
                }
                return FileVisitResult.TERMINATE;
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
            // This is important to note. Test this behaviour
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            return FileVisitResult.CONTINUE;
        }

        public long getFilesCount() {
            return filesCount;
        }
    }
}
