package addtool.helpers.preferences;

import javax.swing.*;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

/**
 * A radiobutton preference allowing the selection of one out of n options.
 */
public class RadioPreference extends StringPreference{
    private String [] options;
    private JComboBox<String> component;
    private JMenu menu;
    ButtonGroup btgroup;

    /**
     * Generates a new Preference
     * @param key the unique key for saving and finding it
     * @param title the displayed title or a key for a resource file
     * @param group the group string for organisation in the edit dialog
     * @param value the initial value as string
     * @param options the available options
     */
    public RadioPreference(String key, String title, String group, String value, String ... options) {
        super(key, title, group, value);
        this.options=options;
        component=new JComboBox(rewriteInLocale(this.options));
        component.setToolTipText(getTooltip(getTitle()));
        setSelected();
        menu=new JMenu(getResource(title));
        menu.setToolTipText(getTooltip(getTitle()));
        btgroup = new ButtonGroup();
        buildMenu();
    }

    /**
     * Updates the available options
     * @param options the new set of options
     */
    public void setOptions(String ... options){
        this.options=options;
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>( rewriteInLocale(this.options) );
        component.setModel( model );
        setSelected();
        buildMenu();
    }
    private void setSelected(){
        for(int i=0;i<options.length;i++){
            if(this.options[i].equals(getValueString())){
                component.setSelectedIndex(i);
                break;
            }
        }
    }

    private String[] rewriteInLocale(String []values){
        String[] output=new String[values.length];
        for(int i=0;i<values.length;i++){
            output[i]=getResource(values[i]);
        }
        return output;
    }
    private void buildMenu(){
        menu.removeAll();
        JMenuItem selected=null;
        for(String s:options) {
            JRadioButtonMenuItem menuItemTD = new JRadioButtonMenuItem(getResource(s));
            menuItemTD.addActionListener(e -> setValueFromComponent());
            menuItemTD.setToolTipText(getTooltip(s));
            if(getValue().equals(s)){
                selected=menuItemTD;
            }
            btgroup.add(menuItemTD);
            menu.add(menuItemTD);
        }
        if(selected!=null){
            selected.setSelected(true);
        }
    }
    @Override
    public JComponent getEditComponent(){
        setSelected();
        return component;
    }
    public void setValueFromComponent(){
        if(!getValue().equals(options[component.getSelectedIndex()])){
            setValue(options[component.getSelectedIndex()]);
            for(int i=0;i< options.length;i++){
                ((JRadioButtonMenuItem)menu.getMenuComponent(i)).setSelected(i==component.getSelectedIndex());
            }
        }
        //Read value from radio button
        int optionRadio=-1;
        for(int i=0;i< options.length;i++){
            if(((JRadioButtonMenuItem)menu.getMenuComponent(i)).isSelected()){
                optionRadio=i;
                component.setSelectedIndex(i);
                break;
            }
        }
        if(optionRadio!=-1&&!getValue().equals(options[optionRadio])){
            setValue(options[optionRadio]);
        }
        super.setValueFromComponent();
    }
    public JMenu getMenuItem(){
        buildMenu();
        return menu;
    }
}
