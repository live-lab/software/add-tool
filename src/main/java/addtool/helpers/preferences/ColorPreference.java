package addtool.helpers.preferences;

import com.mxgraph.util.mxUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

/**
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * A preference for color values
 * Will be stored as hex values
 * Allows editing via a c{@link JColorChooser}
 */
public class ColorPreference extends Preference{
    Color value;
    JButton component;
    ActionListener actionListener;
    /**
     * Generates a new Preference
     * @param key the unique key for saving and finding it
     * @param title the displayed title or a key for a resource file
     * @param group the group string for organisation in the edit dialog
     * @param value the initial value as hex string
     */
    public ColorPreference(String key,String title, String group, String value){
        super(key,title,group);
        this.value= mxUtils.parseColor(value);
        component=new JButton(" ");
        component.setBackground(this.value);
        component.setOpaque(true);
        actionListener=e->{
            component.setBackground(JColorChooser.showDialog(component,
                    getResource("choose_color"), component.getBackground()));
        };
        component.addActionListener(actionListener);
        component.setToolTipText(getTooltip(getTitle()));
    }

    @Override
    public Object getValue() {
        return new Color(value.getRGB());
    }

    @Override
    public String getValueString() {
        return mxUtils.hexString(value);
    }

    @Override
    public void setValue(String s) {
        this.value= mxUtils.parseColor(s);
    }

    @Override
    public JComponent getEditComponent() {
        component.setBackground(value);
        return component;
    }

    @Override
    public void setValueFromComponent() {
        value=component.getBackground();
        super.setValueFromComponent();
    }

    @Override
    public JMenuItem getMenuItem() {
        JMenu menu=new JMenu(getResource(getTitle()));
        menu.addActionListener(actionListener);
        return menu;
    }
}
