package addtool.helpers.preferences;

import addtool.helpers.ExceptionHandler;
import addtool.helpers.TriConsumer;
import com.google.common.collect.Ordering;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static addtool.helpers.Update.basedir;
import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;
/**
 * A class to manage preferences in the new API
 * Provides a singleton with {@link PreferenceManager#getPreferenceManager()}
 */
@SuppressWarnings("LogException")
public final class PreferenceManager {
    private static PreferenceManager singleton=new PreferenceManager();
    private static final String PATH_TO_PREFERENCES="config.xml";
    private static final String SEPARATOR="/";
    private static final String GROUP="group";
    private static final String VALUE="value";
    private static final String TITLE="title";
    private static final String TYPE="type";

    private static final GridBagConstraints left=new GridBagConstraints();
    private static final GridBagConstraints right=new GridBagConstraints();
    private static final GridBagConstraints fillBottom=new GridBagConstraints();
    static {
        left.gridx=0;
        left.gridwidth=1;
        left.weighty=0.1;
        left.weightx=0.2;
        left.ipadx=5;
        left.fill=GridBagConstraints.HORIZONTAL;

        right.gridx=1;
        right.gridwidth=3;
        right.weighty=0.1;
        right.weightx=0.8;
        right.ipadx=5;
        right.fill=GridBagConstraints.HORIZONTAL;

        fillBottom.gridx=0;
        fillBottom.gridwidth=left.gridwidth+right.gridwidth;
        fillBottom.weightx=0.9;
        fillBottom.fill=GridBagConstraints.HORIZONTAL;
        fillBottom.weighty=0.9;

        singleton = new PreferenceManager();
    }

    private final HashMap<String, Preference> preferences=new HashMap();

    /**
     * Returns the singleton instance for managing preferences
     * @return the preference manager
     */
    public static PreferenceManager getPreferenceManager(){
        return singleton;
    }
    private PreferenceManager(){
        readPreferences();
    }

    /**
     * Adds preferences to the manager
     * @param preference preferences to add. Will override value if another is already stored in the manager
     */
    public void putPreferences(Preference ... preference){
        for(Preference p:preference) {
            if(preferences.containsKey(p.getKey())){
                //This sets the value of the newly pushed preference to the old value
                p.setValue(preferences.get(p.getKey()).getValueString());
                preferences.replace(p.getKey(),p);
            }else{
                preferences.put(p.getKey(), p);
            }
            p.addActionListener(e->{
                writePreferences();}
            );
        }
    }

    /**
     * Searches for Preference with the given key
     * @param key the key to search for
     * @return the preference or null if none was found with this key
     */
    public Preference getPreference(String key){
        return preferences.getOrDefault(key,null);
    }

    /**
     * Returns all preferences for a given group string. Will include substring.
     * e.g. a preference with security/encryption will be returned on entering security
     * @param groupPrefix the prefix
     * @return The preferences with this prefix
     */
    public List<Preference> getPreferences(String groupPrefix){
        return preferences.values().stream().
                filter(p->p.getGroup().
                startsWith(groupPrefix)).
                collect(Collectors.toList());
    }

    /**
     * Opens the edit dialog for preferences
     * @param parent a parent window
     */
    public void openEditDialog(Component parent){
        openEditDialog(parent,"");
    }

    /**
     * Opens the edit dialog for preferences for a subset of all preferences
     * @param prefix the prefix for the preferences to show
     * @param parent a parent window
     */
    public void openEditDialog(Component parent,String prefix){
        openEditDialog(parent,prefix,(int)prefix.chars().filter(c->c==SEPARATOR.charAt(0)).count());
    }
    /**
     * Opens the edit dialog for preferences for a subset of all preferences
     * @param prefix the prefix for the preferences to show
     * @param parent a parent window
     * @param depth the depth in which to search. Usually the amount of separators in the prefix
     */
    public void openEditDialog(Component parent, String prefix, int depth){
        openEditDialog(parent,prefix,depth,e->{});
    }
    /**
     * Opens the edit dialog for preferences for a subset of all preferences
     * @param prefix the prefix for the preferences to show
     * @param parent a parent window
     * @param depth the depth in which to search. Usually the amount of separators in the prefix
     * @param onClose An action listener that will be executed on saving the preferences
     */
    public void openEditDialog(Component parent, String prefix, int depth, ActionListener onClose){
        JDialog dia=new JDialog(parent==null?null:SwingUtilities.getWindowAncestor(parent));
        dia.setTitle(getResource("preferences_title"));
        List<String> groups=getGroups(prefix,depth);
        dia.setLayout(new BorderLayout());
        dia.add(addPanelForGroup(groups,prefix,depth),BorderLayout.CENTER);
        if(parent instanceof Window){
            dia.setIconImages(((Window) parent).getIconImages());
        }

        JButton run=new JButton(getResource("save"));
        JRootPane rootPane = SwingUtilities.getRootPane(dia);
        rootPane.setDefaultButton(run);
        dia.add(run,BorderLayout.PAGE_END);
        run.addActionListener(e->{
            //Only update preferences that were displayed
            groups.stream().map(this::getByGroup).flatMap(Collection::stream).forEach(Preference::setValueFromComponent);
            writePreferences();
            try{
                onClose.actionPerformed(e);
            }catch (Exception ex){
                ExceptionHandler.logException(ex);
            }
            dia.setAlwaysOnTop(false);
            dia.setVisible(false);
            //dia.setModal(false);
            dia.dispose();
        });
        dia.setAlwaysOnTop(true);
        dia.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dia.setVisible(true);
        dia.setMinimumSize(new Dimension(600,400));
        dia.pack();
        dia.setLocationRelativeTo(null);
        //This does not work with file or color preferences. Some issue with modality arises
        //dia.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        //dia.setModal(true);
    }
    private JComponent addPanelForGroup(List<String> groups,String prefix, int depth){
        JPanel panel=new JPanel();
        panel.setLayout(new GridBagLayout());
        //Search for Elements on this level
        List<Preference> inGroup=getByGroup(prefix);
        for(Preference p:inGroup){
            if(p.isHidden()){
                continue;
            }
            JLabel label=new JLabel(getResource(p.getTitle()));
            label.setToolTipText(getTooltip(p.getTitle()));
            panel.add(label,left);
            panel.add(p.getEditComponent(),right);
        }
        //next layer
        if(groups.size()>1||(groups.size()==1&&!groups.get(0).isEmpty())){//There need to be subcategories
            JTabbedPane tabbedPane=new JTabbedPane();
            for(String s:groups){
                if(s.isEmpty())continue;
                String newPrefix= prefix.isEmpty() ?s:prefix+SEPARATOR+s;
                List<String> nextGroups=getGroups(newPrefix,depth+1);
                tabbedPane.addTab(getResource(s),null,addPanelForGroup(nextGroups,newPrefix,depth+1),getTooltip(s));
            }
            panel.add(tabbedPane,fillBottom);
        }
        return panel;
    }
    private List<Preference> getByGroup(String group){
        List<Preference> items= preferences.values().stream().filter(p->p.getGroup().equals(group)).collect(Collectors.toList());
        return Ordering.natural().sortedCopy(items);
    }

    private List<String> getGroups(String prefix,int depth){
        List<String> items= preferences.values().stream().map(Preference::getGroup)
                .filter(s->{
                    if(prefix.isEmpty())return true;
                    return s.startsWith(prefix+SEPARATOR);
                })
                .map(s->{
                    String [] strings=s.split(SEPARATOR);
                    if(strings.length>depth){
                        return strings[depth];
                    }
                    return null;
                }).filter(Objects::nonNull).distinct().collect(Collectors.toList());
        return Ordering.natural().sortedCopy(items);
    }

    /**
     * Writes all current preferences to the config file
     */
    public void writePreferences(){
        TriConsumer<String,Preference,Element> writer=(String s,Preference p,Element node)->{
            node.setAttribute(TITLE,p.getTitle());
            node.setAttribute(VALUE,p.getValueString());
            node.setAttribute(TYPE,p.getClass().getSimpleName());
            node.setAttribute(GROUP,p.getGroup());
        };
        writeXML(new File(basedir,PATH_TO_PREFERENCES),preferences,writer,"UserPreferences");
    }

    /**
     * A helper method to write a map to xml format
     * @param target target file
     * @param content the content map
     * @param writer A function to write an entry to a xml element
     * @param rootName the name for the root node
     * @param <K> class of the key
     * @param <V> class of the value
     */
    public static <K,V> void writeXML(File target, Map<K,V> content, TriConsumer<K,V,Element> writer, String rootName){
        try {
            // create DocumentBuilder
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = null;
            docBuilder = docFactory.newDocumentBuilder();

            //create document
            Document document = docBuilder.newDocument();
            Element root=document.createElement(rootName);
            document.appendChild(root);
            content.forEach((k,v)->{
                Element node=document.createElement(k.toString());
                writer.accept(k,v,node);
                root.appendChild(node);
            });
            //bring document to shape and stream to file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            // pretty print XML
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(target);
            transformer.transform(source, result);
        } catch (Exception e) {
            ExceptionHandler.logException(e);
        }
    }

    /**
     * Reads preferences from file
     */
    public void readPreferences(){
        putPreferences(readPreferences(new File(basedir,PATH_TO_PREFERENCES)).values().toArray(Preference[]::new));
    }
    /**
     * Reads preferences from a file
     * @param xmlFile a file to read from
     * @return The preferences saved in the file
     */
    static Map<String, Preference> readPreferences(File xmlFile){
        //File xmlFile = new File(PATH_TO_PREFERENCES);
        if(!xmlFile.exists()||xmlFile.length()<10){
            return Map.of();
        }
        return readPreferences(()-> {
            try {
                return prepareDocumentBuilder().parse(xmlFile);
            } catch (Exception e) {
                ExceptionHandler.logException(e);
            }
            return null;
        });
    }

    /**
     * Helper method to simplify getting a document builder
     * @return the document builder
     * @throws ParserConfigurationException
     */
    public static DocumentBuilder prepareDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        return dBuilder;
    }
    /**
     * Reads preferences from a document
     * @param documentFunction a supplier for the document
     * @return The preferences saved in the document
     */
    static Map<String, Preference> readPreferences(Supplier<Document> documentFunction){
        Map<String, Preference> loaded=new HashMap<>();
        try {
            Document doc = documentFunction.get();
            readDoc(doc,n->{
                Preference p=getPreferenceFromNode(n);
                if(p!=null){
                    loaded.put(p.getKey(),p);
                }
            });
        } catch (Exception e) {
            ExceptionHandler.logException(e);
        }
        return loaded;
    }
    /**
     * Reads preferences from an input stream
     * @param in an inputstream for the document
     * @return The preferences saved in the document
     */
    static Map<String, Preference> readPreferences(InputStream in){
        return readPreferences(()-> {
            try {
                return prepareDocumentBuilder().parse(in);
            } catch (Exception e) {
                ExceptionHandler.logException(e);
            }
            return null;
        });
    }

    /**
     * Reads a document
     * @param doc the document
     * @param eatNode the consumer for the contained nodes
     */
    public static void readDoc(Document doc,  Consumer<Node> eatNode) {
        doc.getDocumentElement().normalize();

        Node root = doc.getDocumentElement();
        NodeList nodes = root.getChildNodes();
        for(int i=0;i<nodes.getLength();i++){
            eatNode.accept(nodes.item(i));
        }
    }
    private static Preference getPreferenceFromNode(Node node){
        String key=node.getNodeName();
        try {
            String title = node.getAttributes().getNamedItem(TITLE).getNodeValue();
            String value = node.getAttributes().getNamedItem(VALUE).getNodeValue();
            String group = node.getAttributes().getNamedItem(GROUP).getNodeValue();
            String type = node.getAttributes().getNamedItem(TYPE).getNodeValue();
            Preference p;
            switch (type){
                case "StringPreference":
                    p=new StringPreference(key,title,group,value);
                    break;
                case "IntPreference":
                    p=new IntPreference(key,title,group,value);
                    break;
                case "FilePreference":
                    p=new FilePreference(key,title,group,value);
                    break;
                case "ColorPreference":
                    p=new ColorPreference(key,title,group,value);
                    break;
                case "BooleanPreference":
                    p=new BooleanPreference(key,title,group,value.equals("TRUE"));
                    break;
                case "RadioPreference":
                    p=new RadioPreference(key,title,group,value);
                    break;
                default:
                    throw new UnsupportedOperationException("Unsupported Preference Type");
            }
            return p;
        }catch (NullPointerException ex){
            return null;
        }
    }

    /**
     * Loads a preference profile to the preference manager
     * @param pp the preference profile to load
     */
    public void loadProfile(PreferenceProfile pp){
        if(pp==null)return;
        pp.loadTo(preferences);
        writePreferences();
    }

}
