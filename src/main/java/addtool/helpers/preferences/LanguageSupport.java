package addtool.helpers.preferences;

import addtool.helpers.Constants;
import addtool.helpers.ExceptionHandler;
import info.debatty.java.stringsimilarity.Levenshtein;
import org.jscience.mathematics.function.Variable;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Helper Class to handle internationalization
 */
public final class LanguageSupport {
    private static final List<Locale> SHIPPED=List.of(new Locale("en","UK"),new Locale("de","DE"));
    private static Locale currentLanguage=SHIPPED.get(0);
    private static URLClassLoader loader = null;
    private static final String bundleName="ui_strings";
    private static final String propertyEnding=".properties";
    /**
     * Preference key for language setting
     */
    public static final String PREF_KEY="language";
    private static ResourceBundle languageBundle= ResourceBundle.getBundle(bundleName, currentLanguage);
    private static final Map<String,String> languageKeys;
    static {
        updateBundle();
        Set<Locale> languages=getLanguages();
        languageKeys=languages.stream().collect(Collectors.toMap(
                        LanguageSupport::getDisplayName, Locale::toString)
                );
        String[] keys=languageKeys.keySet().toArray(String[]::new);
        RadioPreference languageProperty=new RadioPreference(PREF_KEY,PREF_KEY,
                "",getDisplayName(currentLanguage),keys);
        PreferenceManager.getPreferenceManager().putPreferences(languageProperty);
        languageProperty.addActionListener(e-> loadLanguage(languageProperty.getValueString()));
        loadLanguage(languageProperty.getValueString());
    }

    private LanguageSupport() {
    }

    private static String getDisplayName(Locale l){
        return l.getDisplayLanguage()+
                (l.getDisplayCountry().isEmpty()?"":String.format("(%s)",l.getDisplayCountry()));
    }

    /**
     * Tries to find exact match or best match
     * @param s
     * @return
     */
    private static Locale getFromDisplayName(String s){
        if(languageKeys.containsKey(s)){
            return codeToLocale(s);
        }
        double lowestDistance=Double.MAX_VALUE;
        Locale best=SHIPPED.get(0);
        Levenshtein levenshtein=new Levenshtein();
        for(Locale l:Locale.getAvailableLocales()){
            String display=getDisplayName(l);
            double distance=levenshtein.distance(display,s);
            if(distance<lowestDistance){
                lowestDistance=distance;
                best=l;
            }
        }
        return best;
    }
    private static Locale codeToLocale(String s){
        String[] codes=languageKeys.get(s).split("_");
        if(codes.length==1){
             return new Locale(codes[0]);
        }else{
            return new Locale(codes[0],codes[1]);
        }
    }

    /**
     * Updates the language to a given code
     * @param code a language code
     */
    public static void loadLanguage(String code){
        currentLanguage=getFromDisplayName(code);
        updateBundle();
    }
    private static void updateBundle(){
        //noinspection LogException
        try {
            File file = new File(LanguageSupport.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            file=new File(file.getParentFile(),Constants.PLUGIN_FOLDER);//Folder of jar file
            URL[] urls = {file.toURI().toURL()};
            loader = new URLClassLoader(urls);
            if(SHIPPED.contains(currentLanguage)){
                languageBundle = ResourceBundle.getBundle(bundleName, currentLanguage);
            }else{
                languageBundle = ResourceBundle.getBundle(bundleName, currentLanguage,loader);
            }
            if(currentLanguage!=null){
                //Change some Swing/Java defaults to the new value
                UIManager.put("OptionPane.cancelButtonText", getResource("cancel"));
                UIManager.put("OptionPane.noButtonText", getResource("no"));
                UIManager.put("OptionPane.okButtonText", getResource("ok"));
                UIManager.put("OptionPane.yesButtonText", getResource("yes"));
                Locale.setDefault(currentLanguage);
                JOptionPane.setDefaultLocale(currentLanguage);
            }
        } catch (URISyntaxException | IOException e) {
            ExceptionHandler.logException(e);
        }
    }

    /**
     * Returns the string stored in the properties for the passed key. If none is found returns the key
     * @param key the key
     * @return the value or the key if no value is found
     */
    public static String getResource(String key){
        if(languageBundle.containsKey(key)){
            return languageBundle.getString(key);
        }else{
            //System.err.println("Warning: Resource-Key missing "+key);
            return key;
        }
    }

    /**
     * searches for tooltip under the key_tooltip if none is found the returned String is empty
     * @param key A key to the resource
     * @return The tooltip for the given key
     */
    public static String getTooltip(String key){
        String realKey=key+"_tooltip";
        String value=getResource(realKey);
        if(value.equals(realKey)){
            //System.err.println("Warning: Resource-Key missing "+realKey);
            return "";
        }
        return value;
    }

    /**
     * List all available locales
     * @return a set of locales
     */
    public static Set<Locale> getLanguages(){
        Set<Locale> output=new HashSet<>();
        List<String> codes=new ArrayList<>();
        for(Locale l:Locale.getAvailableLocales()){
            //Try to find country locale
            String path=bundleName+ '_' +l.getLanguage()+
                    (l.getCountry().isEmpty()?"":('_' +l.getCountry()))+
                    (l.getDisplayVariant().isEmpty()?"":('_' +l.getDisplayVariant()))
                    +propertyEnding;
            if(loader.findResource(path)!=null&&!codes.contains(path)){
                output.add(l);
                codes.add(path);
            }
        }
        //Add default language
        SHIPPED.forEach(output::add);
        return output;
    }

    /**
     * Will search a file path for a given locale. will also use the properties' hierarchy. e.g. test_de_De is not available so test_de is used
     * filename will be built with prefix_language-code.extension
     * @param prefix the prefix
     * @param extension the extension
     * @return the path to the localised file
     */
    public static String getLocaleFile(String prefix,String extension){
        String path=prefix+ '_' +currentLanguage.getLanguage()+
                (currentLanguage.getCountry().isEmpty()?"":('_' +currentLanguage.getCountry()))+
                (currentLanguage.getDisplayVariant().isEmpty()?"":('_' +currentLanguage.getDisplayVariant()))+
                extension;
        if(new File(path).exists())return path;
        path=prefix+ '_' +currentLanguage.getLanguage()+
                (currentLanguage.getCountry().isEmpty()?"":('_' +currentLanguage.getCountry()))+
                extension;
        if(new File(path).exists())return path;
        path=prefix+ '_' +currentLanguage.getLanguage()+
                extension;
        if(new File(path).exists())return path;
        return prefix+extension;
    }

}
