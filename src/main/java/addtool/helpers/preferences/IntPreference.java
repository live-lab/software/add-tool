package addtool.helpers.preferences;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

public class IntPreference extends Preference{
    int value;
    JTextField component;
    JTextField MenuComponent;
    private int lower=Integer.MIN_VALUE,upper=Integer.MAX_VALUE;
    public IntPreference(String key,String title, String group, String value){
        super(key,title,group);
        this.value=Integer.parseInt(value);
        component=new JTextField(value);
        MenuComponent=new JTextField(value);
        MenuComponent.setBorder(BorderFactory.createTitledBorder(getResource(getTitle())));
        component.setToolTipText(getTooltip(getTitle()));
        MenuComponent.setToolTipText(getTooltip(getTitle()));
        DocumentListener valueChecker=new DocumentListener() {
            long lastCheck=System.currentTimeMillis();
            public void changedUpdate(DocumentEvent e) {
                check(e);
            }
            public void removeUpdate(DocumentEvent e) {
                check(e);
            }
            public void insertUpdate(DocumentEvent e) {
                check(e);
            }

            public void check(DocumentEvent e) {
                JTextField source;
                if(component.getDocument().equals(e.getDocument())){
                    source=component;
                }else{
                    source=MenuComponent;
                }
                try{
                    int current=Integer.parseInt(source.getText());
                    if(current<=upper&&current>=lower){
                        source.setBackground(Color.white);
                        SwingUtilities.invokeLater(()->source.setToolTipText(getTooltip(getTitle())));
                    }else{
                        source.setBackground(Color.red);
                        SwingUtilities.invokeLater(()->source.setToolTipText(String.format(getResource("ValueNotInRange"),lower,upper)));

                    }
                }catch (NumberFormatException ex){
                    source.setToolTipText(String.format(getResource("ValueNotAnInt"),lower,upper));
                    source.setBackground(Color.red);
                }
            }
        };
        component.getDocument().addDocumentListener(valueChecker);
        MenuComponent.getDocument().addDocumentListener(valueChecker);
    }
    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public String getValueString() {
        return String.valueOf(value);
    }

    @Override
    public void setValue(String s) {
        this.value=Integer.parseInt(s);
        if(value<lower){
            value=lower;
        }else if(value>upper){
            value=upper;
        }
        component.setText(String.valueOf(value));
        MenuComponent.setText(String.valueOf(value));
        super.activateListeners();//Activate listeners
    }
    public void setBounds(int lower,int upper){
        if(lower>upper){
            throw new UnsupportedOperationException("Lower bound has to be smaller than the upper bound!");
        }
        this.lower=lower;
        this.upper=upper;
        if(value<this.lower){
            setValue(String.valueOf(lower));
        }else if(value>this.upper){
            setValue(String.valueOf(upper));
        }
    }
    public void increment(){
        increment(1);
    }
    public void increment(int by){
        setValue(String.valueOf(value+by));
    }
    public void decrement(){
        decrement(1);
    }
    public void decrement(int by){
        setValue(String.valueOf(value-by));
    }

    @Override
    public JComponent getEditComponent() {
        return component;
    }

    @Override
    public void setValueFromComponent() {
        if(!getValue().equals(component.getText())){
            setValue(component.getText());
        }
        if(!getValue().equals(MenuComponent.getText())){
            setValue(MenuComponent.getText());
        }
        super.setValueFromComponent();
    }

    @Override
    public JComponent getMenuItem() {
        return MenuComponent;
    }
}
