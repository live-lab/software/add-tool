package addtool.helpers.preferences;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

/**
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * A preference for boolean values
 */
public class BooleanPreference extends Preference{
    private boolean value;
    JCheckBox component;
    JCheckBoxMenuItem menuItem;

    /**
     * Generates a new Preference
     * @param key the unique key for saving and finding it
     * @param title the displayed title or a key for a resource file
     * @param group the group string for organisation in the edit dialog
     * @param value the initial value
     */
    public BooleanPreference(String key, String title, String group,boolean value) {
        super(key, title, group);
        this.value=value;
        component=new JCheckBox(" ");
        component.setSelected(value);
        menuItem=new JCheckBoxMenuItem(getResource(getTitle()));
        menuItem.addActionListener(e-> setValueFromComponent());
        menuItem.setSelected(value);
        component.setToolTipText(getTooltip(getTitle()));
        menuItem.setToolTipText(getTooltip(getTitle()));
    }

    @Override
    public Boolean getValue() {
        return value;
    }

    @Override
    public String getValueString() {
        return value?"TRUE":"FALSE";
    }

    @Override
    public void setValue(String s) {
        setValue(s.equals("TRUE"));
    }
    public void setValue(boolean b) {
        value=b;
        menuItem.setSelected(value);
        component.setSelected(value);
    }

    @Override
    public JComponent getEditComponent() {
        return component;
    }
    @Override
    public void setValueFromComponent() {
        if(value!=component.isSelected()){
            setValue(component.isSelected());
        }
        if(value!=menuItem.getState()){
            setValue(menuItem.getState());
        }
        super.setValueFromComponent();
    }

    @Override
    public JComponent getMenuItem() {
        return menuItem;
    }

}
