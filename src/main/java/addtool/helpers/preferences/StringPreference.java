package addtool.helpers.preferences;

import javax.swing.*;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

public class StringPreference extends Preference{
    private String value;
    private JTextField component;
    JTextField MenuComponent;
    public StringPreference(String key,String title, String group, String value){
        super(key,title,group);
        this.value=value;
        component=new JTextField(value);
        MenuComponent=new JTextField(value);
        MenuComponent.setBorder(BorderFactory.createTitledBorder(getResource(getTitle())));
        component.setToolTipText(getTooltip(getTitle()));
        MenuComponent.setToolTipText(getTooltip(getTitle()));
    }
    @Override
    public Object getValue() {
        return getValueString();
    }

    @Override
    public String getValueString() {
        return value;
    }

    @Override
    public void setValue(String s) {
        this.value=s;
        component.setText(String.valueOf(value));
        MenuComponent.setText(String.valueOf(value));
    }

    @Override
    public JComponent getEditComponent() {
        return component;
    }

    @Override
    public void setValueFromComponent() {
        if(!getValue().equals(component.getText())){
            setValue(component.getText());
        }
        if(!getValue().equals(MenuComponent.getText())){
            setValue(MenuComponent.getText());
        }
        super.setValueFromComponent();
    }

    @Override
    public JComponent getMenuItem() {
        return MenuComponent;
    }
}
