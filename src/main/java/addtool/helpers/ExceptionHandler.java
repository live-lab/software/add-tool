package addtool.helpers;

import java.io.*;
import java.util.Arrays;

import static addtool.helpers.Update.basedir;

/**
 * A handler to catch uncaught exceptions to a log-file and to log exceptions.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 07.10.2020
 */
@SuppressWarnings("LogException")
public class ExceptionHandler implements Thread.UncaughtExceptionHandler{
    static final String errorFile=new File(basedir,"Errors.txt").getAbsolutePath();
    static final String warningFile=new File(basedir,"Warnings.txt").getAbsolutePath();
    static boolean nthCall=false;

    private static void writeThrowable(Throwable throwable,String file,String ...additional){
        try {
            FileWriter fw = new FileWriter(file,nthCall);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw=new PrintWriter(bw);
            Arrays.stream(additional).forEach(pw::println);
            pw.println(throwable.getClass());
            pw.println(throwable.getMessage());
            Arrays.stream(throwable.getStackTrace()).forEach(pw::println);
            if(throwable.getCause()!=null){
                pw.println(throwable.getCause().getClass()+"|"+throwable.getCause().getMessage());
                Arrays.stream(throwable.getCause().getStackTrace()).forEach(pw::println);
            }
            pw.close();
            nthCall=true;
        }catch (IOException e) {
            ExceptionHandler.logException(e);
        }
    }
    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        writeThrowable(throwable,warningFile);
    }

    /**
     * Logs a throwable to the error file 
     * Also writes an info to the error stream
     * @param throwable the throwable to write
     * @param additional additional information
     * @see ExceptionHandler#writeThrowable(Throwable, String, String...)
     */
    public static void logException(Throwable throwable,String ...additional){
        writeThrowable(throwable,errorFile,additional);
        System.err.println(String.format("An Error occurred. Check %s for details.",errorFile));
    }
}
