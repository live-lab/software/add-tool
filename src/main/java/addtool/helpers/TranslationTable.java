package addtool.helpers;

import java.util.List;

/**
 * helpers.CheckingConstants for writing and parsing DOT-files
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 10/14/15
 */
public final class TranslationTable {

    // names for operators
    public static final List<String> TRIGGER = CaseStringList.ofCSL("TRIGGER", "trigger", "Tr", "tr", "TR", "Trig", "TRIG", "trig", "Trigger");
    public static final List<String> RESET = CaseStringList.ofCSL("RESET", "reset", "re", "RE", "Reset");
    public static final List<String> AND = CaseStringList.ofCSL("And", "AND", "and");
    public static final List<String> OR = CaseStringList.ofCSL("OR", "or", "Or");
    public static final List<String> SAND = CaseStringList.ofCSL("SAND", "Sand", "SAnd", "sand");
    public static final List<String> SOR = CaseStringList.ofCSL("SOR", "Sor", "SOr", "sor");
    public static final List<String> IF = CaseStringList.ofCSL("IF", "if", "If");
    public static final List<String> NOT = CaseStringList.ofCSL("NEG", "Neg", "neg", "NOT", "Not", "not");
    public static final List<String> NOT_TRUE = CaseStringList.ofCSL("NT", "Nottrue", "NotTrue");
    public static final List<String> NAT = CaseStringList.ofCSL("NAT", "nat", "Nat", "NotAttempted", "notattempted", "Notattempted");
    public static final List<String> IVR = CaseStringList.ofCSL("IVR", "ivr", "Ivr", "SWAP", "SWP", "Swap", "swap", "swp");
    public static final List<String> CM = CaseStringList.ofCSL("CM", "cm", "countermeasure", "Countermeasure", "CounterMeasure", "COUNTERMEASURE");
    public static final List<String> COST = CaseStringList.ofCSL("COST", "cost", "Cost");
    public static final List<String> BASIC_EVENT = CaseStringList.ofCSL("BE", "BasicEvent", "be","Basicevent","basicevent");
    public static final List<String> BASIC_EVENT_ATTACKER = CaseStringList.ofCSL("BEAttacker","BEA", "BasicEventAttacker", "bea","BasiceventAttacker");
    public static final List<String> BASIC_EVENT_DEFENDER = CaseStringList.ofCSL("BEDefender","BED", "BasicEventDefender", "bed","BasiceventDefender");
    public static final List<String> BASIC_EVENT_TIME = CaseStringList.ofCSL("BETime","BET", "BasicEventTime", "bet","BasiceventTime");

    // names for players
    public static final List<String> ATTACKER = CaseStringList.ofCSL("Attacker", "ATTACKER", "A", "attacker");
    public static final List<String> DEFENDER = CaseStringList.ofCSL("Defender", "DEFENDER", "D", "defender", "d");


    // names for comparison operators
    public static final List<String> LEQ = CaseStringList.ofCSL("<=", "LEQ", "Leq", "leq", "<=");
    public static final List<String> LE = CaseStringList.ofCSL("<", "LE", "Le", "le", "<");
    public static final List<String> GEQ = CaseStringList.ofCSL(">=", "GEQ", "Geq", "geq", ">=");
    public static final List<String> GE = CaseStringList.ofCSL(">", "GE", "Ge", "ge", ">");
    public static final List<String> NEQ = CaseStringList.ofCSL("!=", "NEQ", "Neq", "neq", "<>", "!=");
    public static final List<String> EQ = CaseStringList.ofCSL("==", "EQ", "eq", "Eq", "==", "EQUAL", "equal", "Equal");


    // names for labels in DOT files
    public static final String COST_EXECUTION = "cost";
    public static final String COST_UNCERTAINTY = "cost_uncertainty";
    public static final String COST_PROBABILITYDELTA = "cost_probabilitydelta";
    public static final String DELAY_EXECUTION = "delay";
    public static final String DELAY_UNCERTAINTY = "delay_uncertainty";
    public static final String DELAY_PROBABILITYDELTA = "delay_probabilitydelta";
    public static final String COST_DELAY = "costDelaying"; //currently not in use
    public static final String NAME = "name";
    public static final String DISTRIBUTION = "distribution";
    public static final String GATE = "operator";
    public static final String PROBABILITY = "probability";
    public static final String UNCERTAINTY = "uncertainty";
    public static final String PROBABILITYDELTA = "probabilitydelta";
    public static final String CANBEROOT = "canberoot";
    public static final String XPOS = "xpos";
    public static final String YPOS = "ypos";
    public static final String COMMENT = "comment";
    public static final String BOUND = "bound";
    public static final String COMPARISON = "comparison";
    public static final String FIRST = "first";
    public static final String SECOND = "second";
    public static final String TYPE = "special";
    public static final String STYLE = "style";
    public static final String COLOR = "color";
    public static final String SINK = "sink";
    public static final String ORDER = "order";

    //File types
    public static final String XML = "XML";
    public static final String ADTOOL = "ADTool";
    public static final String UPPAAL = "UPPAAL";
    public static final String PRISM = "PRISM";
    public static final String DOT = "DOT";
    public static final String MODEST = "MODEST";

    public static final String XML_EXTENSION=".xml";
    public static final String JSON_EXTENSION=".json";
    public static final String DOT_EXTENSION=".dot";
    public static final String DUMP_EXTENSION=".dump";
    private TranslationTable() {
    }
}
