package addtool.helpers;

import com.google.common.collect.ImmutableList;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import static addtool.helpers.Update.basedir;

/**
 * Mixed interface for constant helper data or methods.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 06.07.2020
 */
public interface Constants {
    /**
     * Starting text for a comment line inside a dot
     */
    String COMMENT ="#";


    /**
     * Checks if the tool is running from a jar file
     * @return true if it is executed as jar
     */
    static boolean runningFromJar(){
        return Constants.class.getResource("Constants.class").toString().startsWith("jar"); //NON-NLS
    }

    /**
     * Determine file extension of a path
     * @param filename path to a file
     * @return the extension separated by the last dot
     */
    static Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
    /**
     * Determine file name of a path
     * @param filename path to a file
     * @return the name without extension and parent files
     */
    static Optional<String> getNameByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(0,filename.lastIndexOf(".") ));
    }

    /**
     * Path to the Resource Folder root
     * @return path
     */
    static String getResourceFolder(){
        return "/";
    }

    @Deprecated
    static void renderDot2png(File dot, File png){
        renderDot2png(dot,png,false);
    }
    @Deprecated
    static void renderDot2png(File dot, File png,boolean open){
        renderDot2png(dot.getAbsolutePath(),png.getAbsolutePath(),open);
    }
    @Deprecated
    static void renderDot2png(String path2dot,String path2png,boolean open){
        new Thread(()->{
            //noinspection LogException
            try {
                String command=String.format("dot -Tpng %s -o %s",path2dot,path2png); //NON-NLS
                Process p = Runtime.getRuntime().exec(command);
                p.waitFor();
                if(open) Desktop.getDesktop().open(new File(path2png));
            } catch (InterruptedException|IOException e) {
                ExceptionHandler.logException(e);
            }
        }).start();
    }
    /**
     * Returns all possible Subsets of a String Sequence with no attention to order.
     * @param ids List of Strings to build subsets of (size=n)
     * @return 2^n subsets (the empty set is included)
     */
    static List<List<String>> getSubsets(List<String> ids)
    {
        int n = ids.size();

        List<List<String>> subsets=new ArrayList<>();
        // Run a loop for printing all 2^n
        // subsets one by one
        for (int i = 0; i < (1<<n); i++)
        {
            //System.out.print("{ ");
            List<String> option=new ArrayList<>();
            // Print current subset
            for (int j = 0; j < n; j++) {

                // (1<<j) is a number with jth bit 1
                // so when we 'and' them with the
                // subset number we get which numbers
                // are present in the subset and which
                // are not
                if ((i & (1 << j)) > 0)
                    option.add(ids.get(j));
                //System.out.print(set[j] + " ");
            }
            subsets.add(option);
            //System.out.println("}");
        }
        return subsets;
    }

    /**
     * Returns all Permutations of all simple subsets of the List Ids.
     * Internally calls {@link #getSubsets(List)} and {@link #getPermutations(List)} in that order
     * @param ids List of Strings to build permutations with length up to (size=n)
     * @return List of all possible permutations
     */
    static List<List<String>> getPermutationsFromIDs(List<String> ids)
    {
        return getPermutations(getSubsets(ids));
    }
    /**
     * Returns all Permutations of a list of subsets given.
     * @param sets List of Sets to build permutations of
     * @return List of all possible permutations
     */
    static List<List<String>> getPermutations(List<List<String>> sets)
    {
        return sets.stream().map(Constants::generatePerm).flatMap(List::stream).collect(Collectors.toList());
    }
    /**
     * Returns all Permutations of a list.
     * @param original Original List to build permutations of
     * @return List of all possible permutations
     * @param <E> any type of object
     */
    static <E> List<List<E>> generatePerm(List<E> original) {
        //Thanks to Stackoverflow DaveFar
        if (original.isEmpty()) {
            List<List<E>> result = new ArrayList<>();
            result.add(new ArrayList<>());
            return result;
        }
        E firstElement = original.remove(0);
        List<List<E>> returnValue = new ArrayList<>();
        List<List<E>> permutations = generatePerm(original);
        for (List<E> smallerPermutation : permutations) {
            for (int index=0; index <= smallerPermutation.size(); index++) {
                List<E> temp = new ArrayList<>(smallerPermutation);
                temp.add(index, firstElement);
                returnValue.add(temp);
            }
        }
        return returnValue;
    }
    /**
     * Returns the given Number of Objects randomly from a list.
     * @param original Original List
     * @param amount the amount of random objects
     * @param rand a random number generator
     * @return List with given length
     */
    static <E> List<E> getRandomObjects(List<E> original,int amount,Random rand) {
        //Thanks to Stackoverflow DaveFar
        if (original.isEmpty()||amount==0) {
            return new ArrayList<>();
        }
        List<E> result = new ArrayList<>();
        int maxRepetitions=1000;
        int wdh=0;
        do{

            E el = original.get(rand.nextInt(original.size()));
            if(!result.contains(el))result.add(el);
            wdh++;
        }while(result.size()<amount&&wdh<maxRepetitions);

        return result;
    }

    /**
     * Converts a list of arguments in the style abc=xyz in a Map with key abc, value xyz.
     * @param args A list of arguments from commandline
     * @return The map of the argument array
     */
    static Map<String,String> convertArgs(String[] args){
        return convertArgs(args,0);
    }
    /**
     * Converts a list of arguments in the style abc=xyz in a Map with key abc, value xyz.
     * @param args A list of arguments from commandline
     * @param skip The number of entries to skip in the beginning
     * @return The map of the argument array
     */
    static Map<String,String> convertArgs(String[] args,int skip){
        return Arrays.stream(args).skip(skip).collect(Collectors.toMap(s->s.split("=")[0],s -> s.split("=")[1]));
    }


    /**
     * Searches for all Classes in the Classpath implementing a specific subclass
     * @param toFind A class or interface to find
     * @param <T> The Type
     * @param prefix The package name to look in.
     * @return A set of implementing/extending classes
     */
    static <T> Set<Class<? extends T>> findAllMatchingTypes(String prefix,Class<T> toFind) {
        String realPackage=prefix;
        if(prefix==null|| prefix.isEmpty()){
            //Replace empty prefix with parent package
            String packageName=Constants.class.getPackageName();
            realPackage=packageName.substring(0,packageName.lastIndexOf('.'));
        }
        Reflections reflections = new Reflections(realPackage, new TypeAnnotationsScanner(),
                new SubTypesScanner(), new MethodAnnotationsScanner() );//
        Set<Class<? extends T>> classes = reflections.getSubTypesOf(toFind);
        plugins.stream().filter(toFind::isAssignableFrom).forEach(classes::add);
        return classes;
    }

    /**
     * A list of plugin classes found in the designated folder
     */
    @SuppressWarnings("StaticCollection")
    List<Class> plugins= ImmutableList.copyOf(loadPlugins());
    /**
     * plugin folder
     */
    String PLUGIN_FOLDER="plugins";

    /**
     * loads plugins from the relevant folder.
     * is done on definition of plugins list
     * @return a list of classes
     */
    static List<Class> loadPlugins(){
        File folder=new File(PLUGIN_FOLDER);
        if(!folder.exists()||!folder.isDirectory()){
            /*Logger logger = LoggerFactory.getLogger(Constants.class);
            logger.warn("Loading plugins failed. plugins-folder does not exist or is not a directory.");*/
            return List.of();
        }
        List<Class> loaded=new ArrayList<>();
        File[] files=folder.listFiles(f-> f.getAbsolutePath().endsWith(".jar"));
        for(File f:files){
            //No user feedback. If plugin does not load a developer can check it in the log.
            //noinspection LogException
            try {
                //System.out.println("loading " + f.getAbsolutePath());
                JarFile jarFile = new JarFile(f.getAbsolutePath());
                Enumeration<JarEntry> e = jarFile.entries();

                URL[] urls = {new URL("jar:file:" + f.getAbsolutePath() + "!/")};
                URLClassLoader cl = URLClassLoader.newInstance(urls);

                while (e.hasMoreElements()) {
                    JarEntry je = e.nextElement();
                    if (je.isDirectory() || !je.getName().endsWith(".class")) {
                        continue;
                    }
                    // -6 because of .class
                    String className = je.getName().substring(0, je.getName().length() - 6);
                    className = className.replace('/', '.');
                    //noinspection LogException
                    try {
                        Class c = cl.loadClass(className);
                        loaded.add(c);
                        //System.out.println("loaded " + c.getSimpleName());
                    }catch (ClassNotFoundException ex){
                        ExceptionHandler.logException(ex);
                    }
                }
            }catch(IOException e){
                ExceptionHandler.logException(e);
            }
        }
        return loaded;
    }

    /**
     * Unpacks a folder or specific files from the jar file
     * target folder is the code root folder
     * @param prefix the prefix of the folder or path to a file
     * @throws IOException If file access failes
     * @throws URISyntaxException On file path problems
     */
    static void unpackFromJar(String prefix) throws IOException, URISyntaxException {
        File jarFile=new File(Constants.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
        java.util.jar.JarFile jar = new java.util.jar.JarFile(jarFile);

        java.util.Enumeration enumEntries = jar.entries();
        while (enumEntries.hasMoreElements()) {
            java.util.jar.JarEntry file = (java.util.jar.JarEntry) enumEntries.nextElement();
            if(!file.toString().startsWith(prefix)){
                continue;
            }
            java.io.File f = new java.io.File(basedir,file.getName());
            if (file.isDirectory()) { // if its a directory, create it
                f.mkdir();
                continue;
            }
            java.io.InputStream is = jar.getInputStream(file); // get the input stream
            java.io.FileOutputStream fos = new java.io.FileOutputStream(f);
            while (is.available() > 0) {  // write contents of 'is' to 'fos'
                fos.write(is.read());
            }
            fos.close();
            is.close();
        }
        jar.close();
    }

    /**
     * If the OS is Windows
     * @return true if running on windows
     */
    static boolean isWindows()
    {
        return  System.getProperty("os.name").startsWith("Windows");
    }
}
