/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package addtool.helpers;

import javax.xml.bind.DatatypeConverter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Helper class for automatic Updates
 * @author Florian Dorfhuber
 */
public final class Update {
    private Update() {
    }

    enum RequestMethod {
        GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
    }

    /**
     * The directory the tool is saved in
     */
    public static final File basedir;

    static {
        File f=new File("");
        //noinspection LogException
        try {
            f = new File(Update.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());

        } catch (URISyntaxException e) {
            ExceptionHandler.logException(e);
        }
        basedir=f.getParentFile();
    }

    private static final String latestURL="https://gitlab.com/live-lab/software/add-tool/-/raw/main/release/latest.jar";
    private static final String latestSHA="https://gitlab.com/live-lab/software/add-tool/-/raw/main/release/latest_checksum.txt";
    private static final String updaterURL="https://gitlab.com/live-lab/software/add-tool/-/raw/main/release/updater.jar";
    private static final String updaterSHA="https://gitlab.com/live-lab/software/add-tool/-/raw/main/release/updater_checksum.txt";
    static File home = null;
    static File updater = null;
    static File tmp = null;
    private static boolean isUpdateReady=false;
    private static boolean isUpdateAvailable=false;
    private static final List<ActionListener> listeners=new ArrayList<>();

    /**
     * Checks if an update is ready
     * @return true if an update is downloaded an ready to be used
     */
    public static boolean isUpdateReady(){
        return isUpdateReady;
    }

    /**
     * Checks if an update is available
     * @return true if an update is ready to be downloaded
     */
    public static boolean isUpdateAvailable(){
        return isUpdateAvailable;
    }

    /**
     * Checks for updates
     * @return true if the checksums do not match
     */
    public static boolean doUpdatecheck(){
        //noinspection LogException
        try {
            if(!isrunningfromJar()){
                System.err.println("not running from jar");
                return false;
            }
            String[] ergb=getSiteForURL(new URL(latestSHA));
            String checksum=getSHA(home);
            return !ergb[0].equalsIgnoreCase(checksum);
        } catch (URISyntaxException | IOException | NoSuchAlgorithmException ex) {
            ExceptionHandler.logException(ex);
        }
        return false;
    }

    /**
     * Does update check in another thread
     * @see Update#doUpdatecheck()
     */
    public static void asyncDoUpdatecheck(){
        Executor checker=Executors.newSingleThreadExecutor();
        checker.execute(()->{
            boolean available=doUpdatecheck();
            if(available){
                isUpdateAvailable=true;
                listeners.forEach(a-> a.actionPerformed(new ActionEvent(a,0,"updateAvailable")));
            }
        });
    }
    private static void setHome(File f){
        home=f;
        updater=new File(basedir,"updater.jar");
        tmp=new File(basedir,"tmp.jar");
    }

    /**
     * Adds a listener to be activated on state changes
     * @param a the listener
     */
    public static void addListener(ActionListener a){
        listeners.add(a);
    }
    /**
     * remove a listener
     * @param a the listener
     */
    public static void removeListener(ActionListener a){
        listeners.remove(a);
    }

    /**
     * Returns the lines of a webpage at a given URL
     * Will use GET Method
     * @param url the URL
     * @return the string contents
     */
    static String [] getSiteForURL(URL url){
        return getSiteForURL(url,null,RequestMethod.GET);
    }
    /**
     * Returns the lines of a webpage at a given URL
     * Will use GET Method
     * @param url the URL
     * @param b optional content used for POST, null if nothing to send
     * @param met The request method
     * @return the string contents
     */
    static String [] getSiteForURL(URL url,byte [] b,RequestMethod met){
        try
        {
            HttpURLConnection httpUrlConnection = getConnection(url,met);
            if(b!=null&&b.length!=0){
                OutputStream os=httpUrlConnection.getOutputStream();
                os.write(b);
                os.close();
            }
            BufferedReader br=new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream()));

            String str;
            ArrayList<String> cont=new ArrayList<>();
            br.lines().filter(s-> !s.isEmpty()).forEach(cont::add);
            String [] ar=new String[cont.size()];
            return cont.toArray(ar);
        }
        catch(Exception e)
        {
            ExceptionHandler.logException(e);
        }
        return null;
    }

    private static boolean getFileoverPHP(String url,String local,RequestMethod met) throws IOException {
        HttpURLConnection httpUrlConnection = getConnection(url,met);
        ReadableByteChannel in = Channels.newChannel(httpUrlConnection.getInputStream());
        FileOutputStream fos = new FileOutputStream(local);
        FileChannel channel = fos.getChannel();
        channel.transferFrom(in, 0, Long.MAX_VALUE);
        channel.close();
        fos.close();
        return true;
    }

    /**
     * Prepare a string to be url ready
     * @param s the string
     * @return the same string with escaped special characters
     */
    static String prepareurlstring(String s){
        return s.replaceAll(java.util.regex.Pattern.quote("\\"), "/").replaceAll("ä", "%C3%A4").replaceAll("Ä", "%C3%84")
                .replaceAll("Ö", "%C3%96").replaceAll("ö", "%C3%B6").replaceAll("Ü", "%C3%9C").replaceAll("ü", "%C3%BC")
                .replaceAll("ß", "%C3%9F").replaceAll("ẞ", "%E1%BA%9E");
    }
    private static HttpURLConnection getConnection(String url,RequestMethod met) throws IOException{
        return getConnection(new URL(prepareurlstring(url)),met);
    }
    private static HttpURLConnection getConnection(URL url,RequestMethod met) throws IOException{
        HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
        httpUrlConnection.setDoOutput(true);
        httpUrlConnection.setRequestMethod(met.toString());
        String encoding = "";
        httpUrlConnection.setRequestProperty  ("Authorization", "Basic " + encoding);
        return httpUrlConnection;
    }

    /**
     * Checks if file is running from a jar. Will update the home directory as side effect
     * @return true if it is running from a jar
     * @throws URISyntaxException
     */
    public static boolean isrunningfromJar() throws URISyntaxException{
        setHome(new File(Update.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()));
        return home.getAbsolutePath().endsWith(".jar");
    }

    /**
     * Generates SHA1 code from a file
     * @param path the target file
     * @return the SHA1 checksum
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public static String getSHA(String path) throws IOException, NoSuchAlgorithmException{
        return getSHA(new File(path));
    }/**
     * Generates SHA1 code from a file
     * @param path the target file
     * @return the SHA1 checksum
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public static String getSHA(File path) throws IOException, NoSuchAlgorithmException{
        FileInputStream in=new FileInputStream(path);
        byte [] b=new byte[in.available()];
        in.read(b);
        in.close();

        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] thedigest = md.digest(b);
        return DatatypeConverter.printHexBinary(thedigest).toUpperCase();
    }
    private static void getUpdater() throws IOException, NoSuchAlgorithmException {
        if(updater.exists()){
            if(validateFile(updaterSHA,updater))return;
        }
        //Download updater
        getFileoverPHP(updaterURL,updater.getAbsolutePath(),RequestMethod.GET);
    }
    private static boolean validateFile(String checksumURL,File f) throws IOException, NoSuchAlgorithmException {
        String checkSum=getSiteForURL(new URL(checksumURL))[0];
        String sha=getSHA(f);
        return sha.equalsIgnoreCase(checkSum);
    }

    /**
     * Downloads update and updater and notifies listeners on success
     */
    public static void preloadUpdate()  {
        if(home==null){
            doUpdatecheck();
        }
        Executor runner=Executors.newSingleThreadExecutor();
        runner.execute(()->{
            try {
                //check for updater and download
                getUpdater();
                getFileoverPHP(latestURL,tmp.getAbsolutePath(),RequestMethod.GET);
                if(validateFile(latestSHA,tmp)){
                    isUpdateReady=true;
                    listeners.forEach(a->a.actionPerformed(new ActionEvent(a,0,"updateReady")));
                }else{
                    ExceptionHandler.logException(new RuntimeException("Checksum not valid. Update failed."));
                    listeners.forEach(a->a.actionPerformed(new ActionEvent(a,0,"updateFailed")));
                }
            } catch (IOException|NoSuchAlgorithmException e) {
                ExceptionHandler.logException(e);
            }
        });
    }

    /**
     * Performs the update and informs listeners on failure
     */
    public static void performUpdateFireEvent() {
        performUpdateFireEvent(true);
    }
    /**
     * Performs the update and informs listeners on failure
     * @param doStart if the tool should be restarted after completion
     */
    public static void performUpdateFireEvent(boolean doStart) {
        try{
            performUpdate(doStart);
        }catch (IOException ex){
            ExceptionHandler.logException(ex);
            listeners.forEach(a->a.actionPerformed(new ActionEvent(a,0,"updateFailed")));
        }
    }
    /**
     * Performs the update and informs listeners on failure
     */
    public static void performUpdate() throws IOException {
        performUpdate(true);
    }
    /**
     * Performs the update and informs listeners on failure
     * @param doStart if the tool should be restarted after completion
     */
    public static void performUpdate(boolean doStart) throws IOException {
        if(isUpdateReady){
            Process exec = Runtime.getRuntime().exec(String.format("java -jar \"%s\" \"%s\" \"%s\"%s",
                    updater.getAbsolutePath(), tmp.getAbsolutePath(), home.getAbsolutePath(),
                    (doStart?"":" nostart")));
            System.exit(0);
        }else{
            System.err.println("Update not ready");
        }
    }
}

