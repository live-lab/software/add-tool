package addtool.helpers;

import addtool.helpers.preferences.FilePreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.RadioPreference;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.*;
import java.util.Arrays;
import java.util.Base64;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static java.nio.file.StandardOpenOption.CREATE;

/**
 * Helper class to handle en-/decryption of files
 * Encryption will mark files with starting @@ or ** for asymmetric or symmetric encryption
 */
public final class Crypter {
    private Crypter() {
    }

    /**
     * Preference Key for the private key path
     */
    public static final String PREF_KEY_PRIVATE="privateKey";
    /**
     * Preference Key for the public key path
     */
    public static final String PREF_KEY_PUBLIC="publicKey";
    /**
     * Preference Key for the default encryption mode
     */
    public static final String PREF_KEY_MODE="encryptMode";
    /**
     * Preference Group for related settings
     */
    public static final String GROUP="security";

    static {
        FilePreference privateKEY=new FilePreference(PREF_KEY_PRIVATE,PREF_KEY_PRIVATE,GROUP,"server.key");
        FilePreference publicKEY=new FilePreference(PREF_KEY_PUBLIC,PREF_KEY_PUBLIC,GROUP,"server.cert");
        RadioPreference encryptMode=new RadioPreference(PREF_KEY_MODE,PREF_KEY_MODE,GROUP,"encrypt_none","encrypt_none","private_key","public_key","password_encrypt");
        PreferenceManager.getPreferenceManager().putPreferences(publicKEY,privateKEY,encryptMode);
    }

    /**
     * Checks if the file at a path is encrypted
     * @param p the target path
     * @return true if it is marked as encrypted no mater which method is used
     * @throws IOException on input problems
     */
    public static boolean isEncrypted(Path p) throws IOException {
        return isEncrypted(Files.readAllBytes(p));
    }
    /**
     * Checks if content is marked as encrypted
     * @param b content
     * @return true if it is marked as encrypted no mater which method is used
     */
    public static boolean isEncrypted(byte[] b){
        return isEncryptedAsymmetric(b)||isEncryptedSymmetric(b);
    }
    /**
     * Checks if content is marked as encrypted
     * @param b content
     * @return true if it is marked as encrypted symmetrically
     */
    public static boolean isEncryptedSymmetric(byte[] b){
        return b[0]=='*'&&b[1]=='*';
    }
    /**
     * Checks if content is marked as encrypted
     * @param b content
     * @return true if it is marked as encrypted asymmetrically
     */
    public static boolean isEncryptedAsymmetric(byte[] b){
        return b[0]=='@'&&b[1]=='@';
    }

    /**
     * Encrypts (asymmetrically) a target file using a stored key
     * @param p Target path
     * @param publicKey true if the stored public key should be used, false for the private key
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     * @throws CertificateException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws CryptoException
     */
    public static void encryptWrite(Path p, boolean publicKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException, CertificateException, InvalidKeySpecException, InvalidAlgorithmParameterException, CryptoException {
        encryptWrite(p,getKey(publicKey));
    }
    /**
     * Encrypts (asymmetrically) a target file using a passed key
     * @param p Target path
     * @param key encryption key
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     * @throws CertificateException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws CryptoException
     */
    public static void encryptWrite(Path p, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException {
        Files.write(p,encrypt(Files.readAllBytes(p),key),CREATE);
    }

    /**
     * Encrypts (asymmetrically) a target file using a stored key
     * @param p Target path
     * @param publicKey true if the stored public key should be used, false for the private key
     * @return the encrypted content
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     * @throws CertificateException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws CryptoException
     */
    public static byte[] encrypt(Path p,boolean publicKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException, CertificateException, InvalidKeySpecException, InvalidAlgorithmParameterException, CryptoException {
        return encrypt(p,getKey(publicKey));
    }
    /**
     * Encrypts (asymmetrically) a target file using a passed key
     * @param p Target path
     * @param key encryption key
     * @return the encrypted content
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     * @throws CertificateException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws CryptoException
     */
    public static byte[] encrypt(Path p, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException {
        return encrypt(Files.readAllBytes(p),key);
    }
    /**
     * Decrypts (asymmetrically) a target file using a stored key
     * @param p Target path
     * @param publicKey true if the stored public key should be used, false for the private key
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     * @throws CertificateException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws CryptoException
     */

    public static void decryptWrite(Path p, boolean publicKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException, CertificateException, InvalidKeySpecException, InvalidAlgorithmParameterException, CryptoException {
        decryptWrite(p,getKey(publicKey));
    }
    /**
     * Decrypts (asymmetrically) a target file using a passed key
     * @param p Target path
     * @param key decryption key
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     * @throws CertificateException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws CryptoException
     */
    public static void decryptWrite(Path p, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException {
        Files.write(p,decrypt(Files.readAllBytes(p),key),CREATE);
    }
    /**
     * Decrypts (asymmetrically) a target file using a stored key
     * @param p Target path
     * @param publicKey true if the stored public key should be used, false for the private key
     * @return the decrypted content
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     * @throws CertificateException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws CryptoException
     */
    public static byte[] decrypt(Path p, boolean publicKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException, CertificateException, InvalidKeySpecException, InvalidAlgorithmParameterException, CryptoException {
        return decrypt(p,getKey(publicKey));
    }

    /**
     * Decrypts (asymmetrically) a target file using a passed key
     * @param p Target path
     * @param key decryption key
     * @return the decrypted content
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     * @throws CertificateException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws CryptoException
     */
    public static byte[] decrypt(Path p, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException {
        return decrypt(Files.readAllBytes(p),key);
    }

    /**
     * Encrypts a byte [] and returns the encrypted version
     * @param bytes the message
     * @param key the encryption key
     * @return the encrypted content
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static byte[] encrypt(byte[] bytes, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        KeyGenerator generator = KeyGenerator.getInstance("AES");
        generator.init(128); // The AES key size in number of bits
        SecretKey secKey = generator.generateKey();
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.ENCRYPT_MODE, secKey);
        byte[] byteCipherText = aesCipher.doFinal(bytes);

        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.PUBLIC_KEY, key);
        byte[] encryptedKey = cipher.doFinal(secKey.getEncoded());

        byte[] marker=("@@"+encryptedKey.length+"@@").getBytes(StandardCharsets.UTF_8);
        byte[] result=new byte[byteCipherText.length+encryptedKey.length+marker.length];

        System.arraycopy(marker,0,result,0,marker.length);
        System.arraycopy(encryptedKey,0,result,marker.length,encryptedKey.length);
        System.arraycopy(byteCipherText,0,result,marker.length+encryptedKey.length,byteCipherText.length);
        return result;
    }
    /**
     * Decrypts a byte [] and returns the decrypted version
     * @param bytes the message
     * @param key the decryption key
     * @return the decrypted content
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static byte[] decrypt(byte[] bytes, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        if(!isEncrypted(bytes)){
            throw new UnsupportedOperationException("File not in expected format");
        }
        String lengthString="";
        int keyStart=0;
        for(int i=2;i<bytes.length-1;i++){
            if(bytes[i]=='@'&&bytes[i+1]=='@'){
                keyStart=i+2;
                break;
            }
            lengthString+=(char)bytes[i];
        }
        int keyLength=Integer.parseInt(lengthString);
        byte[] encryptedKey=new byte[keyLength];
        System.arraycopy(bytes,keyStart,encryptedKey,0,keyLength);
        byte[] message=new byte[bytes.length-keyLength-keyStart];
        System.arraycopy(bytes,keyStart+keyLength,message,0,message.length);

        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.PRIVATE_KEY, key);
        byte[] decryptedKey = cipher.doFinal(encryptedKey);

        SecretKey originalKey = new SecretKeySpec(decryptedKey , 0, decryptedKey .length, "AES");
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.DECRYPT_MODE, originalKey);

        return  aesCipher.doFinal(message);
    }

    /**
     * Returns key depending on the preferences provided by the class.
     * @param publicKey if the public or the private key from the preference is wished
     * @return An encryption key
     * @throws CertificateException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws CryptoException
     */
    public static Key getKey(boolean publicKey) throws CertificateException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidKeyException, CryptoException {
        String file;
        if(publicKey){
            file=PreferenceManager.getPreferenceManager().getPreference(PREF_KEY_PUBLIC).getValueString();
        }else{
            file=PreferenceManager.getPreferenceManager().getPreference(PREF_KEY_PRIVATE).getValueString();
        }
        return getKey(new File(file),publicKey);
    }

    /**
     * Returns key depending on the preferences provided by the class.
     * @param file the target file of a certificate or private key
     * @return An encryption key
     * @throws CertificateException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws CryptoException
     */
    public static Key getKey(File file, boolean publicKey) throws IOException, CertificateException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidKeyException, CryptoException {
        if(publicKey){
            return getCertificate(file);
        }else{
            return readPrivateKey(file);
        }
    }
    private static PublicKey getCertificate(File file) throws IOException, CertificateException {
        FileInputStream fis = new FileInputStream(file);
        CertificateFactory cf = CertificateFactory.getInstance("X509");
        X509Certificate crt = (X509Certificate) cf.generateCertificate(fis);
        fis.close();
        return crt.getPublicKey();
    }
    private static PrivateKey readPrivateKey(File file) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, CryptoException {
            return readPrivateKey(file,null);
    }

    /**
     * Reads encrypted private key from file
     * @param file Target file
     * @param password the password used for the private key, if null a password dialog will be opened
     * @return the private key
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws CryptoException
     */
    public static PrivateKey readPrivateKey(File file,char[] password) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, CryptoException {
        byte[] fileBytes=Files.readAllBytes(file.toPath());
        String key = new String(fileBytes, Charset.defaultCharset());
        if(key.contains("-----BEGIN ENCRYPTED PRIVATE KEY-----")){
            return fileToPrivateKey(file,password==null?openPasswordWindow(null):password);
        }else{
            String privateKeyPEM = key
                    .replace("-----BEGIN PRIVATE KEY-----", "")
                    .replaceAll(System.lineSeparator(), "")
                    .replace("-----END PRIVATE KEY-----", "");

            byte[] encoded = Base64.getDecoder().decode(privateKeyPEM);

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");

            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
            return keyFactory.generatePrivate(keySpec);

        }

    }
    private static PrivateKey fileToPrivateKey(File f, char[] password)
            throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, CryptoException {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        // Using bcpkix-jdk14-1.48
        PEMParser pemParser = new PEMParser(new FileReader(f));
        Object object = pemParser.readObject();

        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
        KeyPair kp;
        if (object instanceof PEMEncryptedKeyPair)
        {
            // Encrypted key - we will use provided password
            PEMEncryptedKeyPair ckp = (PEMEncryptedKeyPair) object;
            PEMDecryptorProvider decProv = new JcePEMDecryptorProviderBuilder().build(password);
            kp = converter.getKeyPair(ckp.decryptKeyPair(decProv));
        }else if(object instanceof PKCS8EncryptedPrivateKeyInfo){
            PKCS8EncryptedPrivateKeyInfo epki = (PKCS8EncryptedPrivateKeyInfo)object;
            // decrypt and create PrivateKey object from ASN.1 structure
            try {
                InputDecryptorProvider decProv = new JceOpenSSLPKCS8DecryptorProviderBuilder()
                        .setProvider("BC")
                        .build(password);
                PrivateKeyInfo privateKeyInfo = epki.decryptPrivateKeyInfo(decProv);
                return new JcaPEMKeyConverter().getPrivateKey(privateKeyInfo);
            } catch (Exception ex) {
                throw new CryptoException("NoLoadPkcs8PrivateKey.exception.message", ex);
            }
        }
        else
        {
            // Unencrypted key - no password needed
            PEMKeyPair ukp = (PEMKeyPair) object;
            kp = converter.getKeyPair(ukp);
        }

        // RSA
        KeyFactory keyFac = KeyFactory.getInstance("RSA");
        RSAPrivateCrtKeySpec privateKey = keyFac.getKeySpec(kp.getPrivate(), RSAPrivateCrtKeySpec.class);

        return kp.getPrivate();
    }

    /**
     * Generates a symmetric key from a given password
     * PLEASE use rather the char[] version of this method. Strings are hard to delete from memory
     * @param password the password.
     * @return the generated key
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static SecretKey getSymmetricKeyFromPassword(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return getSymmetricKeyFromPassword(password.toCharArray());
    }
    /**
     * Generates a symmetric key from a given password
     * @param password the password
     * @return the generated key
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static SecretKey getSymmetricKeyFromPassword(char[] password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        String salt="X3Yu1X8fmc";//This is not the best solution, but given the password should be enough to decrypt the file it has to be fixed
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(password, salt.getBytes(), 65536, 256);
        SecretKey secret = new SecretKeySpec(factory.generateSecret(spec)
                .getEncoded(), "AES");
        return secret;
    }

    /**
     * Crypt a file symmetrically, mode depends on parameters
     * @param algorithm the algorithm to use
     * @param text the text
     * @param mode either 2=Decrypt, 1=Encrypt
     * @param key the key to use for crypting
     * @return the text after crypting
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static byte[] cryptSymmetric(String algorithm, byte[] text,int mode, SecretKey key) throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException {
        if(mode==Cipher.DECRYPT_MODE){
            //remove encryption marker
            byte[] neu=new byte[text.length-2];
            System.arraycopy(text,2,neu,0,neu.length);
            text=neu;
        }
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(mode, key, new IvParameterSpec( new byte[16]));
        byte[] done= cipher.doFinal(text);
        if(mode==Cipher.ENCRYPT_MODE){
            //Add encryption marker
            byte[] neu=new byte[done.length+2];
            neu[0]='*';neu[1]='*';
            System.arraycopy(done,0,neu,2,done.length);
            done=neu;
        }
        return done;
    }

    /**
     * Opens a password dialog and returns the input password
     * @param parent a parent window
     * @return the password entered
     */
    public static char[] openPasswordWindow(Component parent){
        return openPasswordWindow(parent,false);
    }
    /**
     * Opens a password dialog and returns the input password
     * @param parent a parent window
     * @param retype if true the user has to repeat the entered password, if they do not match the dialog will be opened again until match or abort.
     * @return the password entered
     */
    public static char[] openPasswordWindow(Component parent,boolean retype){
        return openPasswordWindow(parent,retype,false);
    }
    private static char[] openPasswordWindow(Component parent,boolean retype,boolean noMatch){
        JPasswordField passwordField = new JPasswordField(10);
        passwordField.setEchoChar('*');
        JPasswordField retypePasswordField= new JPasswordField(10);
        retypePasswordField.setEchoChar('*');
        JPanel pan=new JPanel();
        pan.setLayout(new GridLayout(0,1));

        pan.add(new JLabel(getResource("enter_password")));
        pan.add(passwordField);
        if(retype){
            pan.add(new JLabel(getResource("retype_password")));
            pan.add(retypePasswordField);
            if(noMatch){
                JLabel noMatchLabel=new JLabel(getResource("no_match"));
                noMatchLabel.setForeground(Color.red);
                pan.add(noMatchLabel);
            }
        }
        int result=JOptionPane.showConfirmDialog(
                parent,
                pan,
                getResource("enter_password"),
                JOptionPane.OK_CANCEL_OPTION);
        if(result==JOptionPane.CANCEL_OPTION){
            return null;
        }
        if(!Arrays.equals(passwordField.getPassword(),retypePasswordField.getPassword())){
            return openPasswordWindow(parent,retype,true);
        }
        return passwordField.getPassword();
    }

    /**
     * Encrypts or decrypts a file based on the preferences for mode, and keys
     * @param source The sourcefile
     * @param target the target file to write tos
     * @param mode either 2=Decrypt, 1=Encrypt
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws CertificateException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws CryptoException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static void cipherByPreference(File source,File target, int mode) throws InvalidAlgorithmParameterException, NoSuchPaddingException, CertificateException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, CryptoException, IllegalBlockSizeException, BadPaddingException {
        cipherByPreference(source,target,mode,null);
    }
    /**
     * Encrypts or decrypts a file based on the preferences for mode, and keys
     * @param source The sourcefile
     * @param target the target file to write tos
     * @param mode either 2=Decrypt, 1=Encrypt
     * @param parent A Window as parent in case a password enter dialog is needed
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws CertificateException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws CryptoException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static void cipherByPreference(File source,File target, int mode,Component parent) throws InvalidAlgorithmParameterException, NoSuchPaddingException, CertificateException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, CryptoException, IllegalBlockSizeException, BadPaddingException {
        byte[] content=Files.readAllBytes(source.toPath());
        byte[] output=cipherByPreference(content,mode,parent);
        Files.write(target.toPath(),output,CREATE);
    }
    /**
     * Encrypts or decrypts a message based on the preferences for mode, and keys
     * @param b the message
     * @return the message after the operation
     * @param mode either 2=Decrypt, 1=Encrypt
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws CertificateException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws CryptoException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static byte[] cipherByPreference(byte[] b, int mode) throws InvalidAlgorithmParameterException, NoSuchPaddingException, CertificateException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, CryptoException, IllegalBlockSizeException, BadPaddingException {
        return cipherByPreference(b,mode,null);
    }
    /**
     * Encrypts or decrypts a message based on the preferences for mode, and keys
     * @param b the message
     * @return the message after the operation
     * @param mode either 2=Decrypt, 1=Encrypt
     * @param parent A Window as parent in case a password enter dialog is needed
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws CertificateException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws CryptoException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static byte[] cipherByPreference(byte[] b, int mode,Component parent) throws InvalidAlgorithmParameterException, NoSuchPaddingException, CertificateException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, CryptoException, IllegalBlockSizeException, BadPaddingException {
        String selectedMode=PreferenceManager.getPreferenceManager().getPreference(PREF_KEY_MODE).getValueString();

        switch (selectedMode) {
            case "private_key", "public_key" -> {
                if (mode == Cipher.ENCRYPT_MODE) {
                    return encrypt(b, getKey(selectedMode.startsWith("public")));
                } else {
                    return decrypt(b, getKey(selectedMode.startsWith("public")));
                }
            }
            case "password_encrypt" -> {
                return cryptSymmetric("AES/CBC/PKCS5Padding", b, mode, getSymmetricKeyFromPassword(openPasswordWindow(parent, true)));
            }
            default -> {
                return b;
            }
        }
    }
}
