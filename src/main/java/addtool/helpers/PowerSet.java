package addtool.helpers;

import java.util.*;

/**
 * Just Maps the Partitionset onto the first component.
 * @param <E> Type of contained in the Set, over which the PowerSet is formed.
 *
 * Internal working: just like powerset, but return both the negative and the positive bits as seperate sets.
 * @author Alexander Taepper alexander.taepper@tum.de
 * @since 01.10.2021
 */
public class PowerSet<E> implements Iterator<Set<E>>,Iterable<Set<E>>{

    private final PartitionSet<E> partitionSet;

    public PowerSet(Collection<E> set, int min_elements, int up_to){
        this.partitionSet = new PartitionSet<>(set, min_elements, up_to);
    }

    public PowerSet(Collection<E> set, int up_to){
        this(set, 0, up_to);
    }

    public PowerSet(Collection<E> set) {
        this(set, set.size());
    }

    @Override
    public boolean hasNext() {
        return this.partitionSet.hasNext();
    }

    @Override
    public Set<E> next() {
        return this.partitionSet.nextOnlyFirst();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not Supported to remove from PowerSet!");
    }

    @Override
    public Iterator<Set<E>> iterator() {
        return this;
    }

}

