package addtool.helpers;

import java.awt.*;
import java.util.Arrays;

public class Threat {
    private String title;
    private STRIDE stride;
    private boolean isOpen;
    private Severity severity;
    private String description;
    private String mitigations;

    public Threat(){
        title="new Threat";
        stride=STRIDE.SPOOFING;
        isOpen=false;
        severity=Severity.MEDIUM;
        description="";
        mitigations="";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public STRIDE getStride() {
        return stride;
    }

    public void setStride(STRIDE stride) {
        this.stride = stride;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMitigations() {
        return mitigations;
    }

    public void setMitigations(String mitigations) {
        this.mitigations = mitigations;
    }

    public enum Severity{
        HIGH(Color.RED),MEDIUM(Color.YELLOW),LOW(Color.GREEN);
        private final Color color;
        Severity(Color c){
            color=c;
        }
        public Color getColor(){
            return color;
        }
        public Severity next(){
            return switch (this) {
                case HIGH -> MEDIUM;
                case MEDIUM -> LOW;
                case LOW -> HIGH;
            };
        }
        public static Severity valueOfInsensitive(String s){
            return Arrays.stream(values()).
                    filter(sev->sev.toString().equalsIgnoreCase(s)).
                    findFirst().
                    orElseThrow(()->new IllegalArgumentException(s+" not a valid Severity"));
        }
        public String getName(){
            String total=this.toString();
            return total.substring(0,1).toUpperCase()+total.substring(1).toLowerCase();
        }
    }
    public String toString(){
        return String.format("(%s,%s,%s)",title,stride.toString(),severity.toString());
    }
}
