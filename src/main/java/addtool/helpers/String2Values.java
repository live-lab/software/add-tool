package addtool.helpers;

import addtool.add.model.ADD;
import addtool.add.model.Vertex;
import addtool.add.operators.Comparison;
import addtool.dot2add.RationalFromString;
import org.jscience.mathematics.number.Rational;

import java.util.ArrayList;
import java.util.List;

/**
 * methods to parse the string representation of values.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 10/15/15
 */
public final class String2Values {


    private String2Values() {
    }

    /**
     * will return the operator represented by the input parameter operatorCost if it is a valid comparison operator,
     * otherwise, null will be returned.
     *
     * @param operatorCost a String representing a comparison operator
     * @return a comparison operator
     */
    public static Comparison[] getComparison(String operatorCost) {
        // split spring at every comma
        String[] operators = operatorCost.split(",");
        Comparison[] operatorObjects = new Comparison[operators.length];

        // first trim the single strings and then find out which operator is described
        for (int i = 0; i < operators.length; i++) {
            operators[i] = operators[i].trim();

            if (TranslationTable.LEQ.contains(operatorCost)) {
                operatorObjects[i] = Comparison.LEQ;
            } else if (TranslationTable.LE.contains(operatorCost)) {
                operatorObjects[i] = Comparison.LE;
            } else if (TranslationTable.GEQ.contains(operatorCost)) {
                operatorObjects[i] = Comparison.GEQ;
            } else if (TranslationTable.GE.contains(operatorCost)) {
                operatorObjects[i] = Comparison.GE;
            } else if (TranslationTable.NEQ.contains(operatorCost)) {
                operatorObjects[i] = Comparison.NEQ;
            } else if (TranslationTable.EQ.contains(operatorCost)) {
                operatorObjects[i] = Comparison.EQUAL;
            }

        }

        return operatorObjects;
    }

    public static Rational[] parseCosts(String cost) {

        //split string at commas
        String[] costs = cost.split(",");
        Rational[] finalCosts = new Rational[costs.length];

        // first trim string, then compute Rational from that string and put in array
        for (int i = 0; i < costs.length; i++) {
            costs[i] = costs[i].trim();
            finalCosts[i] = RationalFromString.getRationalFromString(costs[i]);
        }

        return finalCosts;
    }

    public static Rational parseCost(String cost) {
        return RationalFromString.getRationalFromString(cost);
    }

    public static List<Vertex> getVertexList(String s, ADD add) {
        ArrayList<Vertex> list = new ArrayList<>();

        if (s.isEmpty()) {
            return list;
        }

        // split String in different BEs
        String[] sec = s.split(",");

        // get ids of split strings
        for (String single : sec) {
            // trim whitespace
            single = single.trim();

            // compute id (always first)
            int id = 0;
            for (Character c : single.toCharArray()) {
                if (Character.isDigit(c)) {
                    id = Integer.parseInt(String.valueOf(c)) + 10 * id;
                } else {
                    break;
                }
            }

            // get respective BE and add to toReset
            list.add(add.getVertex2Id(String.valueOf(id)));
        } // end of outer for

        return list;
    }

}
