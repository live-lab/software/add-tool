package addtool.helpers;

import addtool.add.model.*;

public final class Properties {
    private Properties() {
    }

    public static boolean isADT(ADD add) {
        for (Vertex vertex : add) {
            if (vertex instanceof Not ||vertex instanceof Nat || vertex instanceof SOr
                    || vertex instanceof Reset || vertex instanceof Trigger
                    || vertex instanceof Cost || vertex instanceof BasicEventTime
                    || vertex instanceof If || vertex instanceof Ivr
                    || vertex instanceof SAnd) {
                return false;
            }
        }

        return true;
    }

    public static boolean isSAT(ADD add) {
        for (Vertex vertex : add) {
            if (vertex instanceof BasicEventPlayer) {
                if (((BasicEventPlayer) vertex).getPlayer() == Player.Defender) {
                    return false;
                }
            } else if (vertex instanceof Not ||vertex instanceof Nat || vertex instanceof SOr
                    || vertex instanceof Reset || vertex instanceof Trigger
                    || vertex instanceof Cost || vertex instanceof BasicEventTime
                    || vertex instanceof If || vertex instanceof Ivr) {
                return false;
            }
        }

        return true;
    }
}
