package addtool.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * A not case-sensitive extension of {@link ArrayList}.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 17.02.2021
 */
public class CaseStringList extends ArrayList<String> {
    public CaseStringList(){
        super();
    }
    public CaseStringList(Collection<String> elements){
        super(elements);
    }
    @Override
    public boolean contains(Object o) {
        if(o instanceof String){
            String s=(String)o;
            return this.stream().anyMatch(e->e.equalsIgnoreCase(s));
        }
        return super.contains(o);
    }
    public static CaseStringList ofCSL(String ... elements){
        return new CaseStringList(Arrays.asList(elements));
    }

    @Override
    public CaseStringList clone() {
        return (CaseStringList) super.clone();
    }
}
