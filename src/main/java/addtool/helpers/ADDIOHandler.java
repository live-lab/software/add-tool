package addtool.helpers;

import addtool.add.io.ADDExport;
import addtool.add.model.ADD;
import addtool.dot2add.ParserDOT;
import addtool.helpers.preferences.Preference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.RadioPreference;
import addtool.nui.GraphTransformer;
import addtool.nui.mxWrapper.nGraphComponent;
import addtool.xml2add.ParserXML;

import javax.crypto.Cipher;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.io.*;
import java.util.Arrays;
import java.util.function.Predicate;

import static addtool.helpers.preferences.LanguageSupport.getResource;


/**
 * Easy access class for I/O-needs of ADD.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 21.04.2021
 */
public final class ADDIOHandler {
    private static File lastPath=new File(System.getProperty("user.dir"));
    private ADDIOHandler() {
    }

    /**
     * Opens a {@link JFileChooser} for all files in open-mode.
     * @return The {@link File} chosen by the user, null if the operation was aborted
     */
    public static File chooseFile(){
        return chooseFile(lastPath,f->true,null,false);
    }

    /**
     * Opens a {@link JFileChooser} for all files in open-mode.
     * @param filter A Predicate for filtering the
     * @param parent A parent {@link Component} for the FileChooser
     * @param save if true the Dialog will be opened in a mode to save files, otherwise to open.
     * @return The {@link File} chosen by the user, null if the operation was aborted
     */
    public static File chooseFile(Predicate<File> filter, Component parent, boolean save){
        return chooseFile(lastPath,filter,parent,save);
    }
    /**
     * Opens a {@link JFileChooser} for all files in open-mode.
     * @param filter A Predicate for filtering the
     * @param parent A parent {@link Component} for the FileChooser
     * @param name The name displayed in the file chooser filter selection
     * @param save if true the Dialog will be opened in a mode to save files, otherwise to open.
     * @return The {@link File} chosen by the user, null if the operation was aborted
     */
    public static File chooseFile(Predicate<File> filter, String name,Component parent, boolean save){
        return chooseFile(lastPath,filter,name,parent,save);
    }
    /**
     * Opens a {@link JFileChooser} for all files in open-mode.
     * @param par The parent file to start in
     * @param filter A Predicate for filtering the
     * @param parent A parent {@link Component} for the FileChooser
     * @param save if true the Dialog will be opened in a mode to save files, otherwise to open.
     * @return The {@link File} chosen by the user, null if the operation was aborted
     */
    public static File chooseFile(File par, Predicate<File> filter,Component parent,boolean save){
        return chooseFile(par,filter,"",parent,save);
    }
    /**
     * Opens a {@link JFileChooser} for all files in open-mode.
     * @param par The parent file to start in
     * @param name The name displayed in the file chooser filter selection
     * @param filter A Predicate for filtering the
     * @param parent A parent {@link Component} for the FileChooser
     * @param save if true the Dialog will be opened in a mode to save files, otherwise to open.
     * @return The {@link File} chosen by the user, null if the operation was aborted
     */
    public static File chooseFile(File par, Predicate<File> filter,String name,Component parent,boolean save){
        JFileChooser jfc=new JFileChooser(par);
        if(filter!=null){
            jfc.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return filter.test(file);
                }

                @Override
                public String getDescription() {
                    return name;
                }
            });
            jfc.setAcceptAllFileFilterUsed(false);
        }
        addCrypterPanel(jfc);
        int state;
        if(save){
            state=jfc.showSaveDialog(parent);
        }else{
            state=jfc.showOpenDialog(parent);
        }
        if(state==JFileChooser.APPROVE_OPTION){
            File selected=jfc.getSelectedFile();
            if(selected.isDirectory()){
                lastPath=selected;
            }else{
                lastPath=selected.getParentFile();
            }
            return selected;
        }else{
            return null;
        }
    }

    /**
     * Opens a {@link JFileChooser} in save mode to export file. Will determine the format by the selected filter.
     * @param parent Parent component for dialog
     * @param adg the model to export
     * @return true on success
     */
    public static boolean exportFile(Component parent, ADD adg){
       return exportFile(lastPath,parent,adg);
    }
    private static Preference addCrypterPanel(JFileChooser jfc){
        RadioPreference encryption= (RadioPreference) PreferenceManager.getPreferenceManager().getPreference(Crypter.PREF_KEY_MODE);
        JPanel pan=new JPanel();
        pan.setLayout(new GridLayout(0,1));
        pan.add(new JLabel(getResource("encryption_info")));
        pan.add(encryption.getEditComponent());
        jfc.setAccessory(pan);
        return encryption;
    }
    /**
     * Opens a File Chooser with all types as selectable Filter
     * @param par Parent directory, not necessary
     * @param parent the parent component for the file chooser
     * @param adg the Graph to export
     * @return If the operation was successful
     */
    public static boolean exportFile(File par, Component parent, ADD adg){
        JFileChooser jfc=new JFileChooser(par);
        Arrays.stream(ADDExport.getOptions()).sequential().map(ADDExport::getMethodByName)
                .map(e->new FileNameExtensionFilter(e.getName(),e.getFileExtension()))
                .forEach(jfc::addChoosableFileFilter);
        if(parent instanceof nGraphComponent){
            jfc.addChoosableFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.getAbsolutePath().endsWith(".png")||f.isDirectory();
                }

                @Override
                public String getDescription() {
                    return "PNG";
                }
            });
        }
        Preference encryption=addCrypterPanel(jfc);

        //Only allow the generated filters
        jfc.setAcceptAllFileFilterUsed(false);
        int state=jfc.showSaveDialog(parent);
        if(state==JFileChooser.APPROVE_OPTION){
            encryption.setValueFromComponent();
            File selected=jfc.getSelectedFile();
            if(selected.isDirectory()){
                lastPath=selected;
            }else{
                lastPath=selected.getParentFile();
            }
            try{
                if(jfc.getFileFilter().getDescription().equals("PNG")){
                    if(!GraphTransformer.savePng((nGraphComponent) parent,jfc.getSelectedFile())){
                        return false;
                    }
                    Crypter.cipherByPreference(jfc.getSelectedFile(),jfc.getSelectedFile(), Cipher.ENCRYPT_MODE,parent);
                    return true;
                }
                ADDExport export=ADDExport.getMethodByName(jfc.getFileFilter().getDescription());
                if(!selected.getAbsolutePath().endsWith(export.getFileExtension())){
                    selected=new File(selected.getAbsolutePath()+export.getFileExtension());
                }
                export.exportModel(adg,selected);
                Crypter.cipherByPreference(selected,selected, Cipher.ENCRYPT_MODE,parent);
            } catch (Exception e) {
                //noinspection LogException
                ExceptionHandler.logException(e);
                return false;
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * Opens a Dialog to select a supported File format.
     * @return The selected format
     */
    public static String selectFormat(){
        return selectFormat(null);
    }
    /**
     * Opens a Dialog to select a supported File format.
     * @param parent A parent component the dialog should clip on
     * @return The selected format
     */
    public static String selectFormat(Component parent){
        String[] formats=ADDExport.getOptions();//{MODEST,UPPAAL,PRISM,XML,DOT};
        int format_id= JOptionPane.showOptionDialog(parent,getResource("choose_format_message"),getResource("choose_format_title"),
                JOptionPane.DEFAULT_OPTION,JOptionPane.QUESTION_MESSAGE,null,formats,formats[0]);
        if(format_id==-1){
            return null;
        }
        return formats[format_id];
    }

    /**
     * Exports an ADD to file in a given format.
     * @param format The specified format {@link ADDIOHandler#selectFormat()}
     * @param target The Target File
     * @param adg The ADD to save
     * @return true if the process was successful. Exceptions will be handled by the {@link ExceptionHandler#logException(Throwable, String...)}
     */
    public static boolean export(String format, File target, ADD adg) {
        if(format==null||target==null||adg==null){
            return false;
        }
        try{
            ADDExport.getMethodByName(format).exportModel(adg,target);
        } catch (Exception e) {
            //noinspection LogException
            ExceptionHandler.logException(e);
            return false;
        }
        return true;
    }

    /**
     * Opens a Model from file. Will simply try the import formats until is succeeds.
     * @param path target path to load from
     * @throws FileNotFoundException if path is non existant
     * @return true on success
     */
    public static ADD importADD(String path) throws FileNotFoundException {
        File tar=new File(path);
        if(!tar.exists()){
            throw new FileNotFoundException(tar.getAbsolutePath());
        }
        ADD model=null;
        //As both readings are very fast we just check for both types to input
        // So we do not need to rely on the right file-ending
        //noinspection LogException
        try{
            if(tar.getAbsolutePath().endsWith(".dot")){
                FileReader in = new FileReader(tar);
                model = ParserDOT.parseADD(in);
            }
        }catch(Exception ex){
            ExceptionHandler.logException(ex);
        }

        if(model==null&&tar.getAbsolutePath().endsWith(".xml")){
            model = ParserXML.parse(tar.getAbsolutePath());
        }
        if(model!=null)model.validateConnections();
        return model;
    }
    /**
     * Opens a Model from file. Will simply try the import formats until is succeeds.
     * Masks FileNotFoundException
     * @param path target path to load from
     * @return true on success
     * @see ADDIOHandler#importADD(String)
     */
    public static ADD importADDMasked(String path){
        try{
            return importADD(path);
        }catch (FileNotFoundException ex){
            System.err.printf("File %s does not exist. Please check spelling.%n",path);
        }
        return null;
    }
}
