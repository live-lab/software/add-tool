package addtool.helpers;

import addtool.add.model.Cost;
import addtool.add.model.Vertex;
import addtool.add.operators.Comparison;
import addtool.add2modest.ModestCodeDeclaration;
import org.jscience.mathematics.number.Rational;

import java.util.Iterator;
import java.util.List;

/**
 * collection of method to get the string representation of the values stored at vertices.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 10/15/15
 */
public final class Values2String {

    private Values2String() {
    }

    public static String string2Costs(Rational[] costs) {
        StringBuilder tmp = new StringBuilder();

        if (costs == null) {
            return tmp.toString();
        }

        for (Rational cost : costs) {
            if (!tmp.toString().isEmpty()) {
                tmp.append(", ");
            }
            tmp.append(cost.doubleValue());
        }

        return tmp.toString();
    }

    public static String string2Costs(Rational cost) {
        return String.valueOf(cost.doubleValue());
    }
    public static String Rational2string(Rational cost,int pos) {
        String str= String.valueOf(cost.doubleValue());
        if(str.length()>pos&&pos!=-1){
            str=str.substring(0,pos);
        }
        return str;
    }

    public static String string2Comparisons(Comparison[] comparisons) {
        StringBuilder tmp = new StringBuilder();

        if (comparisons == null) {
            return tmp.toString();
        }

        for (Comparison c : comparisons) {
            if (!tmp.toString().isEmpty()) {
                tmp.append(", ");
            }
            tmp.append(c);
        }

        return tmp.toString();
    }

    public static String getList(List<Vertex> list) {
        // initialize successors
        StringBuilder textForSuccessors = new StringBuilder();
        Iterator<Vertex> it = list.iterator();

        while (it.hasNext()) {
            Vertex ver = it.next();
            textForSuccessors.append(getVertexName(ver));

            if (it.hasNext()) {
                textForSuccessors.append(", ");
            }
        }
        return textForSuccessors.toString();
    }

    public static String getIdList(List<Vertex> list) {
        // initialize successors
        StringBuilder textForPredecessors = new StringBuilder();
        Iterator<Vertex> it = list.iterator();

        while (it.hasNext()) {
            Vertex ver = it.next();
            textForPredecessors.append(ver.getId());

            if (it.hasNext()) {
                textForPredecessors.append(',');
            }
        }
        return textForPredecessors.toString();
    }

    public static String getVertexName(Vertex v) {
        if (v == null) {
            throw new IllegalArgumentException("Vertex cannot be null!");
        }
        return v.getId() + ' ' + v.getOperatorName() + (((v.getName()) != null && !(v.getName().isEmpty())) ? " (" + v.getName() + ')' : "");
    }

    public static String costConstraint(Cost cost) {
        StringBuilder tmp = new StringBuilder();
        int i = 0;
        for (Comparison c : cost.getOp()) {
            if (!tmp.toString().isEmpty()) {
                tmp.append(" && ");
            }

            tmp.append(ModestCodeDeclaration.getVariableCost(i)).append(c).append(cost.getBound()[i].doubleValue());
            i++;
        }
        return tmp.toString();
    }

    public static int id2Int(String id) {
        return Integer.parseInt(id);
    }
    public static int id2Int(String id,int def) {
        try{
            return Integer.parseInt(id);
        }catch(NumberFormatException ex){
            return def;
        }
    }
}
