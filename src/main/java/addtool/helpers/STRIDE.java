package addtool.helpers;

import java.util.Arrays;
import java.util.List;

public enum STRIDE {
    SPOOFING("Spoofing"),TAMPERING("Tampering"),REPUDIATION("Repudiation"),
    INFORMATION_DISCLOSURE("Information disclosure"),DENIAL_OF_SERVICE("Denial of service"),
    ELEVATION_OF_PRIVILEGE("Elevation of privilege");
    private final String clearName;
    STRIDE(String clearName){
        this.clearName=clearName;
    }
    public String getName(){
        return clearName;
    }
    public static STRIDE read(String s){
        try{
            return valueOf(s);
        }catch (IllegalArgumentException ex){
            return Arrays.stream(values()).filter(st->st.getName().equals(s)).findFirst().orElseThrow(()->ex);
        }
    }
    public STRIDE next(){
        List<STRIDE> values=Arrays.asList(values());
        int index=values.indexOf(this)+1;
        if(index>=values.size()){
            index=0;
        }
        return values.get(index);
    }
}
