package addtool.helpers;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Class that constructs all different ways to poll from a set. That is extract one element from the set and
 * return both the removed element and the remaining set. Useful to iterate over every eay of polling from a set.
 *
 *
 * @author Alexander Taepper alexander.taepper@tum.de
 * @since 01.10.2021
 */
public class PollSet<E> implements Iterator<Pair<E,Set<E>>>,Iterable<Pair<E,Set<E>>>{

    private final PartitionSet<E> partitionSet;

    public PollSet(Collection<E> set){
        this.partitionSet = new PartitionSet<>(set, 1, 1);
    }

    @Override
    public Iterator<Pair<E, Set<E>>> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return partitionSet.hasNext();
    }

    @Override
    public Pair<E, Set<E>> next() {
        Pair<Set<E>,Set<E>> next = partitionSet.next();
        return new Pair<>(next.getFirst().iterator().next(), next.getSecond());
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("No remove from PollSet");
    }
}
