package addtool.helpers;

import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * Class that constructs all different ways to Partition a set.
 *
 * Internal working: just like PowerSet, but return both the negative and the positive bits as separate sets.
 * Negative and positive bits refer to the 0s and 1s of a counter, that just counts up.
 * Every configuration of 0s and 1s are reached once the counter overflows.
 * Additional constraints in the form of minimum and maximum partition size of the respectively first returned
 * partition are possible.
 *
 * @author Alexander Taepper alexander.taepper@tum.de
 * @since 01.10.2021
 */
public class PartitionSet<E> implements Iterator<Pair<Set<E>,Set<E>>>,Iterable<Pair<Set<E>,Set<E>>>{
    // put into array so every element has well defined position.
    protected final E[] arr;
    // binary array indicating, which elements are returned next.
    // array is a simple counter, counting up.
    protected final BitSet bset;
    // Current amount of elements that would be returned.
    private int bset_ones = 0;
    private final int up_to;
    private final int min_elements;

    /**
     * Constructs a PartitionSet.
     * @param set The set for which to construct the partitions.
     * @param min_elements min elements that have to be in the first partition
     * @param up_to max elements that can be in the first partition.
     */
    @SuppressWarnings("unchecked")
    public PartitionSet(@NotNull Collection<E> set, int min_elements, int up_to){
        arr = (E[]) set.toArray();
        // One longer. To catch the overflow, which corresponds to finished counter incrementation.
        bset = new BitSet(arr.length + 1);
        this.up_to = up_to;
        this.min_elements = min_elements;
        incrementUntilValid();
    }

    public PartitionSet(Collection<E> set, int up_to){
        this(set, 0, up_to);
    }

    public PartitionSet(Collection<E> set) {
        this(set, set.size());
    }

    @Override
    public boolean hasNext() {
        return !overflow();
    }

    private boolean overflow(){
        return this.bset.get(arr.length);
    }

    @Override
    public Pair<Set<E>,Set<E>> next() {
        if(bset.get(arr.length)){
            throw new NoSuchElementException("Last Element of PowerSet was already reached.");
        }
        Set<E> returnSet1 = new HashSet<>();
        Set<E> returnSet2 = new HashSet<>();
        for(int i = 0; i < arr.length; i++) {
            if(bset.get(i)) {
                returnSet1.add(arr[i]);
            }
            else if(!bset.get(i)) {
                returnSet2.add(arr[i]);
            }
        }
        incrementCounter();
        incrementUntilValid();
        return new Pair<>(returnSet1, returnSet2);
    }

    /**
     * Just like next, but faster because it only adds to one set and returns the respective set.
     * @return the first Partition of the next() method
     */
    protected Set<E> nextOnlyFirst() {
        if(bset.get(arr.length)){
            throw new NoSuchElementException("Last Element of PowerSet was already reached.");
        }
        Set<E> returnSet1 = new HashSet<>();
        for(int i = 0; i < arr.length; i++) {
            if(bset.get(i)) {
                returnSet1.add(arr[i]);
            }
        }
        incrementCounter();
        incrementUntilValid();
        return returnSet1;
    }

    // Increment counter by one until overflow or valid position is reached.
    private void incrementUntilValid(){
        while(hasNext() && (bset_ones < min_elements)) {
            incrementCounter();
        }
    }

    // increment bset
    private void incrementCounter() {
        // for loop represents carrying the carry bit over up to the position "arr.length-1"
        // Overflow bit will be set manually if no increment was successfully set.
        for(int i = 0; i < arr.length; i++) {
            // Carry over.
            if(bset.get(i)) {
                bset.clear(i);
                bset_ones -= 1;
            }
            // First 0 reached. Setting this bit to one, corresponds to incrementing entire counter by 1
            // Only incrementing if bset_ones < upto corresponds to skipping all positions, which have too many 1s set.
            // -> Performance gain.
            else if (bset_ones < up_to){
                bset.set(i);
                bset_ones += 1;
                return;
            }
        }
        // If no valid position could be found before, set the overflow flag.
        bset.set(arr.length);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not Supported to remove from powerset!");
    }

    @Override
    public Iterator<Pair<Set<E>,Set<E>>> iterator() {
        return this;
    }

}

