package addtool.generation;

import addtool.add.model.*;
import addtool.analysisonadd.ADDVisitor;

import java.util.*;

/**
 * A visitor to copy an ADD.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 15.11.2018
 */
public class CopyVisitor implements ADDVisitor {

    private final HashMap<String, Vertex> newVertices = new HashMap<>();
    private final Set<Vertex> noSuccessors = new HashSet<>();
    private final String prefix;

    public CopyVisitor(String prefix) {
        this.prefix = prefix;
    }

    public Map<String, Vertex> getNewVertices() {
        return new HashMap<>(newVertices);
    }

    public void visit(Vertex vertex) {
        Vertex copy;
        if (vertex instanceof BasicEventPlayer) {
            copy = new BasicEventPlayer(vertex.getName(), ((BasicEventPlayer) vertex).getSuccessProbability(), ((BasicEventPlayer) vertex).getCost(), ((BasicEventPlayer) vertex).getPlayer(), prefix + vertex.getId());
        } else if (vertex instanceof BasicEventTime) {
            copy = new BasicEventTime(vertex.getName(), ((BasicEventTime) vertex).getSuccessProbability(), ((BasicEventTime) vertex).getCost(), ((BasicEventTime) vertex).getDistribution(), prefix + vertex.getId());
        } else {
            switch (vertex.getClass().getSimpleName()) {
                case "And" -> {
                    copy = new And(vertex.getName(), prefix + vertex.getId());
                    setUpNary((NaryOp) copy, (NaryOp) vertex);
                }
                    /*((And) copy).setPredecessor01(newVertices.get(prefix + ((And) vertex).getPredecessor01().getId()));
                    ((And) copy).setPredecessor02(newVertices.get(prefix + ((And) vertex).getPredecessor02().getId()));
                    newVertices.get(prefix + ((And) vertex).getPredecessor01().getId()).addSuccessor(copy);
                    newVertices.get(prefix + ((And) vertex).getPredecessor02().getId()).addSuccessor(copy);*/
                case "Or" -> {
                    copy = new Or(vertex.getName(), prefix + vertex.getId());
                    setUpNary((NaryOp) copy, (NaryOp) vertex);
                }
                    /*((Or) copy).setPredecessor01(newVertices.get(prefix + ((Or) vertex).getPredecessor01().getId()));
                    ((Or) copy).setPredecessor02(newVertices.get(prefix + ((Or) vertex).getPredecessor02().getId()));
                    newVertices.get(prefix + ((Or) vertex).getPredecessor01().getId()).addSuccessor(copy);
                    newVertices.get(prefix + ((Or) vertex).getPredecessor02().getId()).addSuccessor(copy);*/
                case "Trigger" -> {
                    copy = new Trigger(vertex.getName(), prefix + vertex.getId());
                    copy.addPredecessor(newVertices.get(prefix + ((UnaryOp) vertex).getPredecessor().getId()));
                    //((Trigger) copy).setPredecessor(newVertices.get(prefix + ((Trigger) vertex).getPredecessor().getId()));
                    for (BasicEvent b : ((Trigger) vertex).getToTrigger()) {
                        ((Trigger) copy).addToTrigger((BasicEvent) newVertices.get(prefix + b.getId()));
                    }
                }
                //Adding successor unnecessary as new API works both ways
                //newVertices.get(prefix + ((UnaryOp) vertex).getPredecessor().getId()).addSuccessor(copy);
                case "SAnd" -> {
                    copy = new SAnd(vertex.getName(), prefix + vertex.getId());
                    setUpNary((NaryOp) copy, (NaryOp) vertex);
                }
                    /*((SAnd) copy).setPredecessor01(newVertices.get(prefix + ((SAnd) vertex).getPredecessor01().getId()));
                    ((SAnd) copy).setPredecessor02(newVertices.get(prefix + ((SAnd) vertex).getPredecessor02().getId()));
                    newVertices.get(prefix + ((SAnd) vertex).getPredecessor01().getId()).addSuccessor(copy);
                    newVertices.get(prefix + ((SAnd) vertex).getPredecessor02().getId()).addSuccessor(copy);*/
                case "SOr" -> {
                    copy = new SOr(vertex.getName(), prefix + vertex.getId());
                    setUpNary((NaryOp) copy, (NaryOp) vertex);
                }
                    /*((SOr) copy).setPredecessor01(newVertices.get(prefix + ((SOr) vertex).getPredecessor01().getId()));
                    ((SOr) copy).setPredecessor02(newVertices.get(prefix + ((SOr) vertex).getPredecessor02().getId()));
                    newVertices.get(prefix + ((SOr) vertex).getPredecessor01().getId()).addSuccessor(copy);
                    newVertices.get(prefix + ((SOr) vertex).getPredecessor02().getId()).addSuccessor(copy);*/
                case "Not" -> {
                    copy = new Not(vertex.getName(), prefix + vertex.getId());
                    setUpUnary((UnaryOp) copy, (UnaryOp) vertex);
                }
                    /*((Not) copy).setPredecessor(newVertices.get(prefix + ((Not) vertex).getPredecessor().getId()));
                    newVertices.get(prefix + ((Not) vertex).getPredecessor().getId()).addSuccessor(copy);*/
                case "Nat" -> {
                    copy = new Nat(vertex.getName(), prefix + vertex.getId());
                    setUpUnary((UnaryOp) copy, (UnaryOp) vertex);
                }
                    /*((Nat) copy).setPredecessor(newVertices.get(prefix + ((Nat) vertex).getPredecessor().getId()));
                    newVertices.get(prefix + ((Nat) vertex).getPredecessor().getId()).addSuccessor(copy);*/
                case "Ivr" -> {
                    copy = new Ivr(vertex.getName(), prefix + vertex.getId());
                    setUpUnary((UnaryOp) copy, (UnaryOp) vertex);
                }
                    /*((Ivr) copy).setPredecessor(newVertices.get(prefix + ((Ivr) vertex).getPredecessor().getId()));
                    newVertices.get(prefix + ((Ivr) vertex).getPredecessor().getId()).addSuccessor(copy);*/
                default -> throw new UnsupportedOperationException("The operator " + vertex.getOperatorName() + " is not supported yet");
            }
        }
        if (vertex.isSinkAttacker()) {
            copy.setSinkAttacker(true);
        } else if (vertex.isSinkDefender()) {
            copy.setSinkDefender(true);
        }
        newVertices.put(copy.getId(), copy);
    }
    private void setUpNary(NaryOp copy,NaryOp original){
        //copy = new And(vertex.getName(), prefix + vertex.getId());
        copy.addPredecessor(newVertices.get(prefix + original.getPredecessor01().getId()));
        copy.addPredecessor(newVertices.get(prefix + original.getPredecessor02().getId()));
        newVertices.get(prefix + original.getPredecessor01().getId()).addSuccessor(copy);
        newVertices.get(prefix + original.getPredecessor02().getId()).addSuccessor(copy);
    }
    private void setUpUnary(UnaryOp copy,UnaryOp original){
        copy.setPredecessor(newVertices.get(prefix + original.getPredecessor().getId()));
        newVertices.get(prefix + original.getPredecessor().getId()).addSuccessor(copy);
    }

    public ADD getADD() {
        return new ADD(newVertices, noSuccessors, false);
    }

    @Override
    public void visit(BasicEventPlayer basicEvent) {
        visit((Vertex) basicEvent);
    }

    @Override
    public void visit(BasicEventTime basicEvent) {
        visit((Vertex) basicEvent);
    }

    @Override
    public void visit(And and) {
        visit((Vertex) and);
    }

    @Override
    public void visit(Or or) {

        visit((Vertex) or);
    }

    @Override
    public void visit(SAnd sAnd) {

        visit((Vertex) sAnd);
    }

    @Override
    public void visit(SOr sOr) {

        visit((Vertex) sOr);
    }

    @Override
    public void visit(If ifGate) {

        visit((Vertex) ifGate);
    }

    @Override
    public void visit(Not not) {

        visit((Vertex) not);
    }

    @Override
    public void visit(NotTrue not) {
        visit((Vertex) not);
    }

    @Override
    public void visit(Nat nat) {

        visit((Vertex) nat);
    }

    @Override
    public void visit(Ivr ivr) {

        visit((Vertex) ivr);
    }

    @Override
    public void visit(Trigger trigger) {

        visit((Vertex) trigger);
    }

    @Override
    public void visit(Reset reset) {

        visit((Vertex) reset);
    }

    @Override
    public void visit(Cost vertex) {

        visit((Vertex) vertex);
    }

    @Override
    public void visit(Countermeasure vertex) {

        visit((Vertex) vertex);
    }
}
