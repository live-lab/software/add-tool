package addtool.modelchecking;

import addtool.helpers.TranslationTable;
import addtool.helpers.preferences.FilePreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.StringPreference;

import javax.swing.filechooser.FileFilter;
import java.io.File;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.TranslationTable.ADTOOL;
import static addtool.modelchecking.CheckingConstants.*;

/**
 * ModelCheckerAdapter for the ADTool
 * Allows setting a tool Path and a domain
 * @author Florian Dorfhuber
 * @see ModelCheckerAdapter
 */
public class ADToolAdapter extends ModelCheckerAdapter{
    /**
     * The tag used in the settings to write the custom domain names to.
     * Will be automatically replaced on execution with the name of the domain.
     */
    public static final String TAG_DOMAIN="[domain]";
    static {
        String group=PREF_GROUP+ADTOOL;
        FilePreference Path=new FilePreference(KEY_ADTOOL_PATH,PATH,group,getResource(SELECT_PATH));
        StringPreference Args=new StringPreference(KEY_ADTOOL_MC_ARGS,PARAMS,group,ADTOOL_MC_ARGS);

        PreferenceManager.getPreferenceManager().putPreferences(Path,Args);

        Path.setFileNameFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory()||(f.getAbsolutePath().endsWith(".jar")&&f.getName().contains("ADTool"));
            }

            @Override
            public String getDescription() {
                return ADTOOL;
            }
        });
        //Try to automatically find the checker
        //Path.tryFindFile();
    }

    /**
     * This is for reference. To be found by the Reflections API you have to include a constructor without arguments.
     */
    public ADToolAdapter(){

    }
    @Override
    public String getName() {
        return ADTOOL;
    }

    @Override
    public boolean isReady() {
        return new File(PreferenceManager.getPreferenceManager().getPreference(KEY_ADTOOL_PATH).getValueString()).exists();
    }

    @Override
    public void findConnectorInBackground() {

    }


    @Override
    public String prepareCommand() {
        String command = PreferenceManager.getPreferenceManager().getPreference(KEY_ADTOOL_MC_ARGS).getValueString();
        command=command.replace(TAG_TOOL,
                wrapPath(PreferenceManager.getPreferenceManager().getPreference(KEY_ADTOOL_PATH).getValueString()));
        command = command.replace(TAG_DOMAIN, "a");//All domains NON-NLS NON-NLS
        return command;
    }

    @Override
    public String getEnding() {
        return ".xml";
    }

    @Override
    public String getFormat() {
        return TranslationTable.XML;
    }
}
