package addtool.modelchecking;

import addtool.helpers.ExceptionHandler;
import addtool.helpers.preferences.FilePreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.StringPreference;

import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.TranslationTable.PRISM;
import static addtool.modelchecking.CheckingConstants.*;

/**
 * Class to handle connection to PRISM-Games model checker
 * @see ModelCheckerAdapter
 */
public class PRISMAdapter extends ModelCheckerAdapter{
    /**
     * Placeholder for prism properties
     */
    public static final String TAG_PROPERTIES="[props]";
    static {
        String group=PREF_GROUP+PRISM;
        FilePreference Path=new FilePreference(KEY_PRISM_PATH,PATH,group,getResource(SELECT_PATH));
        StringPreference Args=new StringPreference(KEY_PRISM_MC_ARGS,PARAMS,group,PRISM_MC_ARGS);
        StringPreference Props=new StringPreference(KEY_PRISM_PROPS,PROPERTIES,group,PRISM_PROPS);

        PreferenceManager.getPreferenceManager().putPreferences(Path,Args,Props);


        Path.setFileNameFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory()||f.getAbsolutePath().endsWith("prism")||f.getAbsolutePath().endsWith("prism.bat");
            }

            @Override
            public String getDescription() {
                return PRISM;
            }
        });
    }
    final File propertyTMP =new File("propertyTMP.props");

    /**
     * Empty constructor to ensure search finds this method.
     */
    public PRISMAdapter(){

    }
    @Override
    public String getName() {
        return PRISM;
    }


    @Override
    public String prepareCommand() {
        String command = PreferenceManager.getPreferenceManager().getPreference(KEY_PRISM_MC_ARGS).getValueString();
        //PROPS
        try {
            Files.writeString(propertyTMP.toPath(),
                    PreferenceManager.getPreferenceManager().getPreference(KEY_PRISM_PROPS).getValueString(),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            ExceptionHandler.logException(e);
        }
        command = command.replace(TAG_PROPERTIES, propertyTMP.getAbsolutePath());
        command=command.replace(TAG_TOOL,
                wrapPath(PreferenceManager.getPreferenceManager().getPreference(KEY_PRISM_PATH).getValueString()));
        return command;
    }

    @Override
    public String getEnding() {
        return ".prism";
    }

    @Override
    public String getFormat() {
        return PRISM;
    }
    @Override
    public boolean isReady() {
        return new File(PreferenceManager.getPreferenceManager().getPreference(KEY_PRISM_PATH).getValueString()).exists();
    }

    @Override
    public void findConnectorInBackground() {
        ((FilePreference)PreferenceManager.getPreferenceManager().getPreference(KEY_PRISM_PATH)).tryFindFile();
    }
}
