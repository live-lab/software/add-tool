package addtool.modelchecking;

import addtool.add.io.ADDExport;
import addtool.add.model.ADD;
import addtool.helpers.Constants;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Predicate;

/**
 * An interface for adapters to Model-checker
 * Main focus is the execute method
 * Should use a not already used Name
 * Consider a reverse domain style with a suiting key in the language bundle
 * @see CheckerAdapter#execute(ADD, PrintStream)
 * @author Florian Dorfhuber
 */
public interface CheckerAdapter {
    /**
     * A name by which the method can be found. Should not collide with others. Is also displayed in the tabs
     * @return The NAme of the method
     */
    String getName();

    /**
     * Runs the Method on the model and writes the results to the stream
     * @param model The model to analyse
     * @param writeTo A Stream to write to
     */
    void execute(ADD model, PrintStream writeTo);


    /**
     * Get selected ADDExport
     * Will only find classes that have an accessible constructor with no arguments
     * @param checkName Name of the Type {@link CheckerAdapter#getName()}
     * @return an object if there exists a corresponding class, null otherwise
     */
    static CheckerAdapter getMethodByParam(Predicate<CheckerAdapter> checkName){
        Set<Class<? extends CheckerAdapter>> classes = Constants.findAllMatchingTypes("",CheckerAdapter.class);
        return classes.stream().map(c->{try{
                    return c.getConstructor().newInstance();
                } catch (InvocationTargetException |InstantiationException|IllegalAccessException|NoSuchMethodException e) {
                    return null;
                }}).filter(o-> Objects.nonNull(o) && checkName.test(o)).
                findFirst().orElse(null);
    }

    /**
     * Enter the Name and ADDImport Implementation.
     * @param name A name from {@link ADDExport#getName()}
     * @return An instance of a corresponding Object
     */
    static CheckerAdapter getMethodByName(String name){
        return getMethodByParam(a->a.getName().equals(name));
    }

    /**
     * Lists all Classes implementing the Interface in the classpath.
     * Will only find classes that have an accessible constructor with no arguments
     * @see CheckerAdapter#getName()
     * @return An Array of all Class names
     */
    static String[] getOptions(){
        Set<Class<? extends CheckerAdapter>> classes = Constants.findAllMatchingTypes("",CheckerAdapter.class);
        return classes.stream().map(c->{try{
            return c.getConstructor().newInstance().getName();
        } catch (InvocationTargetException |InstantiationException|IllegalAccessException|NoSuchMethodException e) {
            return null;
        }}).filter(Objects::nonNull).toArray(String[]::new);
    }

    /**
     * Check if the adapter is configured and ready to run
     * @return true if the adapter can be used
     */
    boolean isReady();

    /**
     * Here your adapter can (not must) implement a search for a suitable binary
     */
    void findConnectorInBackground();

}
