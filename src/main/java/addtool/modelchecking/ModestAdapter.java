package addtool.modelchecking;

import addtool.helpers.TranslationTable;
import addtool.helpers.preferences.FilePreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.StringPreference;

import javax.swing.filechooser.FileFilter;
import java.io.File;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.TranslationTable.*;
import static addtool.modelchecking.CheckingConstants.*;

/**
 * Connects the tool directly to modest
 * Handles export command building
 * @see ModelCheckerAdapter
 */
public class ModestAdapter extends ModelCheckerAdapter{
    static {
        String group=PREF_GROUP+MODEST;
        FilePreference Path=new FilePreference(KEY_MODEST_PATH,PATH,group,getResource(SELECT_PATH));
        StringPreference Args=new StringPreference(KEY_MODEST_MC_ARGS,PARAMS,group,MODEST_MC_ARGS);

        PreferenceManager.getPreferenceManager().putPreferences(Path,Args);


        Path.setFileNameFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory()||f.getAbsolutePath().endsWith("modest")||f.getAbsolutePath().endsWith("modest.exe");
            }

            @Override
            public String getDescription() {
                return MODEST;
            }
        });
    }

    /**
     * empty constructor, just to be sure it stays here.
     */
    public ModestAdapter(){

    }
    @Override
    public String getName() {
        return MODEST;
    }


    @Override
    public String prepareCommand() {
        String command = PreferenceManager.getPreferenceManager().getPreference(KEY_MODEST_MC_ARGS).getValueString();
        command=command.replace(TAG_TOOL,
                wrapPath(PreferenceManager.getPreferenceManager().getPreference(KEY_MODEST_PATH).getValueString()));
        return command;
    }

    @Override
    public String getEnding() {
        return ".modest";
    }

    @Override
    public String getFormat() {
        return TranslationTable.MODEST;
    }

    @Override
    public boolean isReady() {
        return new File(PreferenceManager.getPreferenceManager().getPreference(KEY_MODEST_PATH).getValueString()).exists();
    }

    @Override
    public void findConnectorInBackground() {
        ((FilePreference)PreferenceManager.getPreferenceManager().getPreference(KEY_MODEST_PATH)).tryFindFile();
    }
}
