package addtool.modelchecking;

import addtool.helpers.TranslationTable;
import addtool.helpers.preferences.FilePreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.StringPreference;

import javax.swing.filechooser.FileFilter;
import java.io.File;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.TranslationTable.UPPAAL;
import static addtool.modelchecking.CheckingConstants.*;

/**
 * An Adapter implementation for UPPAAL verifyta
 */
public class UPPAALAdapter extends ModelCheckerAdapter{
    static {
        String group=PREF_GROUP+UPPAAL;
        FilePreference Path=new FilePreference(KEY_UPPAAL_PATH,PATH,group,getResource(SELECT_PATH));
        StringPreference Args=new StringPreference(KEY_UPPAAL_MC_ARGS,PARAMS,group,UPPAAL_MC_ARGS);
        PreferenceManager.getPreferenceManager().putPreferences(Path,Args);

        Path.setFileNameFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory()||f.getAbsolutePath().endsWith("verifyta")||f.getAbsolutePath().endsWith("verifyta.exe");
            }

            @Override
            public String getDescription() {
                return UPPAAL;
            }
        });
    }

    /**
     * empty constructor to ensure it is found.
     */
    public UPPAALAdapter(){

    }
    @Override
    public String getName() {
        return UPPAAL;
    }


    @Override
    public String prepareCommand() {
        String command = PreferenceManager.getPreferenceManager().getPreference(KEY_UPPAAL_MC_ARGS).getValueString();
        command=command.replace(TAG_TOOL,
                wrapPath(PreferenceManager.getPreferenceManager().getPreference(KEY_UPPAAL_PATH).getValueString()));
        return command;
    }

    @Override
    public String getEnding() {
        return ".uppaal.xml";
    }

    @Override
    public String getFormat() {
        return TranslationTable.UPPAAL;
    }

    @Override
    public boolean isReady() {
        return new File(PreferenceManager.getPreferenceManager().getPreference(KEY_UPPAAL_PATH).getValueString()).exists();
    }
    @Override
    public void findConnectorInBackground() {
        ((FilePreference)PreferenceManager.getPreferenceManager().getPreference(KEY_UPPAAL_PATH)).tryFindFile();
    }
}
