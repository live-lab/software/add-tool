package addtool.modelchecking;

import addtool.add.model.ADD;
import addtool.helpers.ADDIOHandler;
import addtool.helpers.ExceptionHandler;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Example implementation of the CheckerAdapter
 * Basis for the shipped four Adapters
 * @see CheckerAdapter
 */
public abstract class ModelCheckerAdapter implements CheckerAdapter{
    /**
     * Text key for parameters inserted for the model checker
     */
    public static final String PARAMS="params";

    /**
     * Text key for path to model checker
     */
    public static final String PATH="path";

    /**
     * Text key for additional properties for the MC
     */
    public static final String PROPERTIES="properties";

    /**
     * Text key for path selection
     */
    public static final String SELECT_PATH="select_path";

    /**
     * Placeholder for the path to the tool used in params preference
     */
    public static final String TAG_TOOL="[tool]";
    /**
     * Placeholder for the temporary file that is used as input to the model checker
     */
    public static final String TAG_INPUT="[input]";
    /**
     * Placeholder for the temporary file that is used as output of the model checker
     */
    public static final String TAG_OUTPUT="[output]";

    /**
     * Group prefix for Preferences
     */
    public static final String PREF_GROUP="checker/";

    /**
     * An empty constructor.
     * Is here as a reminder that the CheckerAdapter implementation needs an empty constructor to be found.
     */
    public ModelCheckerAdapter(){
    }

    private final File inputTMP =new File("inputTMP");
    private final File outputTMP =new File("outputTMP");

    private boolean active=false;

    /**
     * Wraps tools in double quotes
     * @param path the path to be wrapped
     * @return the wrapped path
     */
    public static String wrapPath(String path){
        return '"' +path+ '"';
    }
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void execute(ADD model, PrintStream writeTo) {
        if(active){
            System.err.println("Checker is busy instantiate a new Object or try again later.");
        }
        active=true;
        String ending=getEnding();
        File realInputTMP=new File(inputTMP.getAbsolutePath()+ending);
        File realOutputTMP=new File(outputTMP.getAbsolutePath()+ending);
        String command=prepareCommand()
                .replace(TAG_OUTPUT,realOutputTMP.getAbsolutePath())
                .replace(TAG_INPUT,realInputTMP.getAbsolutePath());
        ExecutorService exe = Executors.newSingleThreadExecutor();
        //Export File
        Future<Boolean> exportResult= exe.submit(()->{
            //Export file
            try{
                if(!ADDIOHandler.export(getFormat(),realInputTMP,model)){
                    writeTo.println(getResource("export_error"));
                    return Boolean.FALSE;
                }
            }catch (RuntimeException e){
                writeTo.println(getResource("export_error"));
                return Boolean.FALSE;
            }
            writeTo.println(getResource("export_done")+".\n++++++++++++++++++++++++++++++++++++++++++++++++++");
            return Boolean.TRUE;
        });
        //Run Checker
        Future<Boolean> checkerResult= exe.submit(()->{
            if(!exportResult.get()){
                active=false;
                writeTo.println("Done");
                writeTo.flush();
                return Boolean.FALSE;
            }
            //noinspection ProhibitedExceptionCaught
            try{
                //System.out.println(command);

                String OS = System.getProperty("os.name").toLowerCase();
                Process p;
                if((OS.contains("nix") || OS.contains("nux") || OS.contains("aix"))){
                    //Fixes a problem on linux where direct starting was not possible
                    p=Runtime.getRuntime().exec(new String[]{"bash", "-l", "-c", command});
                }else{
                    p=Runtime.getRuntime().exec(command);
                }

                InputStream in=p.getInputStream();
                InputStream err=p.getErrorStream();
                while(p.isAlive()||in.available()>0||err.available()>0){
                    boolean foundSomething=false;
                    //System.out.println("Running");
                    if(in.available()>0){
                        String str=new String(in.readAllBytes(), StandardCharsets.UTF_8);
                        writeTo.print(str);
                        foundSomething=true;
                    }
                    if(err.available()>0){
                        String str=new String(err.readAllBytes(),StandardCharsets.UTF_8);
                        writeTo.print(str);
                        foundSomething=true;
                    }
                    if(!foundSomething){
                        //noinspection BusyWait
                        Thread.sleep(100);
                    }
                }
                if(realOutputTMP.exists()){
                    writeTo.println("\n++++++++++++++++++++++++++++++++++++++++++++++++++");
                    String outfile= Files.readString(realOutputTMP.toPath());
                    writeTo.println(outfile);
                }
            } catch (NullPointerException|IOException | InterruptedException e) {
                ExceptionHandler.logException(e);
            } finally {
                realInputTMP.delete();
                realOutputTMP.delete();
                writeTo.println("Done");
                writeTo.flush();
            }
            active=false;
            return Boolean.TRUE;
        });
    }

    /**
     * Abstract method for sub-classes to do last adjustments for the command
     * @return the prepared command.
     */
    public abstract String prepareCommand();

    /**
     * File extension for the output file
     * @return A String with the file extension lead by a dot '.'
     */
    public abstract String getEnding();

    /**
     * The Format Name by which an Exporter is searched for
     * @return A displayable name for the format
     */
    public abstract String getFormat();

}
