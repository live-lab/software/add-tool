/**
 * A package that allows direct model-checking in other tools and output to the UI
 * @see addtool.modelchecking.CheckerAdapter As interface to implement in plugins
 * @see addtool.nui.panels.ModelCheckerPanel As swing component to handle and display the results.
 */
package addtool.modelchecking;