package addtool.modelchecking;

/**
 * Constants needed for model checker integration.
 * These are now used as default values for preferences
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public interface CheckingConstants {
    /**
     * Model checker connection.
     */
    String UPPAAL_MC_ARGS="[tool] -V [input]";//tool, output, input NON-NLS //-l [output]
    String ADTOOL_MC_ARGS="java -jar [tool] -d [domain] -x [output] -i [input]";//tool, domain output, input NON-NLS
    String PRISM_MC_ARGS="[tool]  [input] [props] -javamaxmem 4g";//tool, domain output, input NON-NLS
    String PRISM_PROPS=//"// What is the maximum success probability of an attack?\n" + //NON-NLS Linebreaks are not working in xml
            "<<attacker>> Pmax=? [ F \"success\" ]\n" + //NON-NLS
            "<<attacker>> Pmax=? [ F \"fail\" ]"; //NON-NLS
    String MODEST_MC_ARGS="[tool] simulate -R Uniform -S ASAP [input]";//tool, domain output, input NON-NLS
    String KEY_UPPAAL_MC_ARGS="UPPAAL_MC_ARGS"; //NON-NLS
    String KEY_UPPAAL_PATH="UPPAAL_PATH"; //NON-NLS
    String KEY_ADTOOL_MC_ARGS="ADTOOL_MC_ARGS"; //NON-NLS
    String KEY_ADTOOL_PATH="ADTOOL_PATH"; //NON-NLS
    String KEY_PRISM_MC_ARGS="PRISM_MC_ARGS"; //NON-NLS
    String KEY_PRISM_PATH="PRISM_PATH"; //NON-NLS
    String KEY_PRISM_PROPS="PRISM_PROPS"; //NON-NLS
    String KEY_MODEST_MC_ARGS="MODEST_PROPS"; //NON-NLS
    String KEY_MODEST_PATH="MODEST_PATH"; //NON-NLS
}
