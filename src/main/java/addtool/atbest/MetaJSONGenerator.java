package addtool.atbest;

import addtool.add.model.Vertex;
import addtool.helpers.ExceptionHandler;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import addtool.add.model.ADD;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Generates meta-data in JSON format for upload in ATBEST.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public final class MetaJSONGenerator {
    private MetaJSONGenerator() {
    }

    public static String createJSONString(ADD model){
        return createJSON(model).toJSONString();
    }
    public static void writeJSONString(ADD model, File file){
        JSONObject jsonObject=createJSON(model);
        try {
            FileWriter fileWriter = new FileWriter(file.getAbsolutePath());
            fileWriter.write(jsonObject.toJSONString());
            fileWriter.close();
        } catch (IOException e) {
            ExceptionHandler.logException(e);
        }
    }
    public static JSONObject createJSON(ADD model){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("NumVertices",model.getId2Vertex().size());
        jsonObject.put("NumOperators",model.getOperators().size());
        jsonObject.put("NumBasicEvents",model.getBasicEvents().size());
        jsonObject.put("Description", String.join("<br>", model.getComments()));

        JSONArray gates=new JSONArray();
        Set<String> operators=model.getOperators().stream().map(Vertex::getOperatorName).collect(Collectors.toSet());
        gates.addAll(operators);

        jsonObject.put("Gates",gates);

        return jsonObject;
    }
}
