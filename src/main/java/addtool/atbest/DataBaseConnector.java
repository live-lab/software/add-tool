package addtool.atbest;

import addtool.add.model.ADD;
import addtool.add2dot.ADD2Dot;
import addtool.helpers.ExceptionHandler;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Utility interface for uploading models and ordering tokens.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public interface DataBaseConnector {
    /**
     * HTTP Request Methods.
     */
    enum RequestMethod {
        GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
    }
    File TMP=new File("tmp.dot");
    File TOKEN_FILE =new File("token.txt");
    //TODO test and switch to new Weblink
    String PROTOCOL ="https://"; //NON-NLS
    String DOMAIN ="www.model.in.tum.de/~kraemerj/upload";
    String FOLDER ="/get_token.php";
    String CHARSET="UTF-8";
    //static String protocol="https://",domain="postman-echo.com",folder="/post";
    static boolean publishAdd(ADD add){
        try {
            ADD2Dot.writeADD2DotFile(new PrintWriter(TMP),add);
            return publishAdd(TMP);
        } catch (Exception e) {
            ExceptionHandler.logException(e);
            return false;
        }
    }
    static boolean publishAdd(File addFile){
        if(addFile==null||!addFile.exists()){
            addFile= TMP;
        }
        if(TOKEN_FILE.exists()){
            try {
                HashMap<String,String> attr=new HashMap<>();
                attr.put("token",Files.readString(TOKEN_FILE.toPath(),StandardCharsets.UTF_8));
                return publishAdd(addFile,attr).contains("Upload complete");
            } catch (Exception e) {
                ExceptionHandler.logException(e);
                return false;
            }
        }else{
            //Show Dialog for missing Credentials
            return showDialogAndPublishAdd(addFile);
        }
    }

    static String publishAdd(File addFile,Map<String,String> attr){
        //noinspection LogException
        try {
            return uploadFile(Files.readAllBytes(addFile.toPath()),attr);
        } catch (Exception ex) {
            ExceptionHandler.logException(ex);
        }
        return null;
    }

    /**
     * Publishes Add to Database whilst retrieving a token. Token will be saved and displayed
     * @param add Model to submit
     * @param firstname Firstname of user
     * @param lastname Lastname of user
     * @param email Email of user
     * @param affiliation Affiliation
     * @return Boolean if submission was successful
     */
    static boolean publishAdd(ADD add, String firstname,String lastname,String email,String affiliation){
        try {
            ADD2Dot.writeADD2DotFile(new PrintWriter(TMP),add);
            return publishAdd(TMP,firstname,lastname,email,affiliation);
        } catch (Exception e) {
            ExceptionHandler.logException(e);
            return false;
        }
    }

    static boolean publishAdd(File addFile, String firstname,String lastname,String email,String affiliation){
        if(addFile==null||!addFile.exists()){
            addFile= TMP;
        }
        HashMap<String,String> attr=new HashMap<>();
        attr.put("fname",firstname);
        attr.put("lname",lastname);
        attr.put("mail",email);
        attr.put("affil",affiliation);
        attr.put("api","TRUE");//NEEDED to get Token directly
        String response=publishAdd(addFile,attr);
        //noinspection LogException
        try {
            String token=response.split("<br>")[0];
            Files.writeString(TOKEN_FILE.toPath(),token);
            System.out.println(token);

        } catch (Exception e) {
            ExceptionHandler.logException(e);
        }
        return response.contains(getResource("upload_complete"));
    }
    static boolean showDialogAndPublishAdd(File addFile){
        AtomicInteger state=new AtomicInteger();
        JDialog dia=new JDialog();
        dia.setTitle(getResource("enter_credentials"));
        JPanel pan=new JPanel();
        pan.setLayout(new GridLayout(0,2));
        pan.add(new JLabel(" "+getResource("firstname")+": "));
        JTextField fileNameTextField=new JTextField();
        pan.add(fileNameTextField);
        pan.add(new JLabel(" "+getResource("lastname")+": "));
        JTextField lastNameTextField=new JTextField();
        pan.add(lastNameTextField);
        pan.add(new JLabel(" "+getResource("email")+": "));
        JTextField mailTextField=new JTextField();
        pan.add(mailTextField);
        pan.add(new JLabel(" "+getResource("affiliation")+": "));
        JTextField affiliationTextField=new JTextField();
        pan.add(affiliationTextField);
        JButton cancel=new JButton(getResource("cancel"));
        cancel.addActionListener(e->{
            dia.setVisible(false);state.set(-5);
        });
        JButton sub=new JButton(getResource("submit"));
        sub.addActionListener(e->{
            dia.setVisible(false);state.set(5);
        });
        sub.setEnabled(false);
        pan.add(cancel);
        pan.add(sub);

        mailTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                update(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                update(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                update(e);
            }
            public void update(DocumentEvent e){
                if(Pattern.matches("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}\\b",mailTextField.getText())){
                    mailTextField.setBackground(Color.white);
                    sub.setEnabled(true);
                }else{
                    mailTextField.setBackground(Color.red);
                    sub.setEnabled(false);
                }
            }
        });

        dia.add(pan);
        //dia.setUndecorated(true);
        dia.pack();
        dia.setModal(true);
        dia.setVisible(true);
        if(state.intValue()!=5){
            return false;//If cancel was selected
        }

        String firstname=fileNameTextField.getText();
        String lastname=lastNameTextField.getText();
        String email=mailTextField.getText();
        String affiliation=affiliationTextField.getText();

        return publishAdd(addFile,firstname,lastname,email,affiliation);
    }
    static String getSiteForURL(URL url, Map<String,String> params, byte[] b, RequestMethod met){
        String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.

        StringBuilder out= new StringBuilder();
        //noinspection LogException
        try {
            String CRLF = "\r\n"; // Line separator required by multipart/form-data.
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            OutputStream output = connection.getOutputStream();
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, CHARSET), true);

            // Send normal param.
            params.forEach((k,v)->{
                writer.append("--").append(boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"")
                        .append(k)
                        .append(String.valueOf('"'))
                        .append(CRLF);
                writer.append("Content-Type: text/plain; charset=").append(CHARSET).append(CRLF);
                writer.append(CRLF).append(v).append(CRLF).flush();
            });

            // Send file.
            writer.append("--").append(boundary).append(CRLF);
            writer.append("Content-Disposition: form-data; name=\"files[]\"; filename=\"UserADD")
                    .append(String.valueOf(System.currentTimeMillis()))
                    .append(".dot\"")
                    .append(CRLF);
            writer.append("Content-Type: application/msword-template; charset=")
                    .append(CHARSET)
                    .append(CRLF); // Text file itself must be saved in this charset!
            writer.append(CRLF).flush();
            output.write(b);
            //Files.copy(textFile.toPath(), output);
            output.flush(); // Important before continuing with writer!
            writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.

            // End of multipart/form-data.
            writer.append("--").append(boundary).append("--").append(CRLF).flush();
// Request is lazily fired whenever you need to obtain information about response.
            int responseCode = ((HttpURLConnection) connection).getResponseCode();
            //System.out.println(responseCode); // Should be 200
            InputStream in=connection.getInputStream();
            while(in.available()>0){
                out.append(new String(in.readAllBytes(), StandardCharsets.UTF_8));
            }
        }catch (Exception ex){
            ExceptionHandler.logException(ex);
        }
        return out.toString();

    }
    private static HttpURLConnection getConnection(URL url,RequestMethod met) throws IOException{
        HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
        httpUrlConnection.setDoOutput(true);
        httpUrlConnection.setRequestMethod(met.toString());
        //httpUrlConnection.setRequestProperty  ("Authorization", "Basic " + encoding);
        return httpUrlConnection;
    }

    static String prepareURLString(String s){
        return s.replaceAll(java.util.regex.Pattern.quote("\\"), "/")
                .replaceAll("ä", "%C3%A4").replaceAll("Ä", "%C3%84")
                .replaceAll("Ö", "%C3%96").replaceAll("ö", "%C3%B6")
                .replaceAll("Ü", "%C3%9C").replaceAll("ü", "%C3%BC")
                .replaceAll("ß", "%C3%9F").replaceAll("ẞ", "%E1%BA%9E");
    }
    static String uploadFile(byte[] b, Map<String,String> attr){
        //noinspection LogException
        try {
            URL url=new URL(prepareURLString(PROTOCOL + DOMAIN + FOLDER));
            return getSiteForURL(url,attr,b,RequestMethod.POST);
        } catch (MalformedURLException ex) {
            ExceptionHandler.logException(ex);
        }
        return null;
    }
}
