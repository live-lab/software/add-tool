package addtool.scripts;


import addtool.add.model.ADD;
import addtool.add2dot.ADD2Dot;
import addtool.feedback.FeedbackTuple;
import addtool.helpers.ADDIOHandler;
import addtool.helpers.Constants;
import addtool.helpers.ExceptionHandler;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.PreferenceProfile;
import addtool.nui.EasySplash;
import addtool.nui.GraphFrame;
import addtool.nui.Tutorial;
import addtool.nui.UIHelper;

import javax.swing.*;

import static addtool.helpers.Constants.getNameByStringHandling;
import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.nui.GraphFrame.EXAMPLES;
import static javax.swing.JFrame.MAXIMIZED_BOTH;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Main method to start {@link GraphFrame}.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public final class MainEditor {
    private MainEditor() {
    }
    private static GraphFrame frame=null;
    private static EasySplash splashScreen=null;

    /**
     * Main method will start the editor and load settings
     * @param args
     */
    public static void main(String [] args) {
        splashScreen=new EasySplash();
        if(frame==null||!frame.isDisplayable()){
            setupEditor();
        }
        loadFiles(args);
        splashScreen.dispose();

        listenInput();
    }
    private static void listenInput(){
        ExecutorService listener= Executors.newSingleThreadExecutor();
        listener.submit(()->{
            String command="";
            while(true){
                if(System.in.available()>0){
                    byte[] b=new byte[System.in.available()];
                    System.in.read(b);
                    command+=new String(b);
                    String[] split = command.split("\" \"");
                    loadFiles(split);
                    command="";
                }
                Thread.sleep(100);
            }
        });
        listener.shutdown();
    }

    /**
     * Start the editor new. Will also launch a splashscreen if one is present
     */
    public static void setupEditor(){
        if(frame==null){
            //Load UI settings
            UIHelper.updateLookAndFeel();
            UIHelper.updateUIFont();
        }
        if(splashScreen!=null){
            splashScreen.setProgress(5);
        }
        //Unpack examples if necessary
        if(!new File(EXAMPLES).exists()){
            try {
                Constants.unpackFromJar(EXAMPLES);
            } catch (IOException|URISyntaxException e) {
                ExceptionHandler.logException(e);
            }
        }
        if(splashScreen!=null){
            splashScreen.setProgress(15);
        }
        //See if first start dialog has to be shown.
        PreferenceProfile preferenceProfile=Tutorial.checkSetup();
        if(splashScreen!=null){
            splashScreen.setProgress(25);
        }

        boolean exitOnClose=true;
        if(frame!=null){
            exitOnClose=frame.isExitOnClose();
        }

        if(splashScreen!=null){
            splashScreen.setProgress(35);
        }
        //Run Window
        frame=new GraphFrame(getResource("editor_title"));
        frame.setExtendedState(MAXIMIZED_BOTH);
        frame.setMinimumSize(new Dimension(400,400));
        frame.setExitOnClose(exitOnClose);

        if(splashScreen!=null){
            splashScreen.setProgress(75);
        }

        if(preferenceProfile!=null){
            PreferenceManager.getPreferenceManager().loadProfile(preferenceProfile);
        }

        if(splashScreen!=null){
            splashScreen.setProgress(85);
        }

        frame.setVisible(true);
        frame.startTutorial();

        if(splashScreen!=null){
            splashScreen.setProgress(100);
        }
    }

    /**
     * Updates editor exit on close setting
     * @param exitOnClose true if closing the editor should stop the jvm
     */
    public static void setExitOnClose(boolean exitOnClose){
        if(frame!=null){
            frame.setExitOnClose(exitOnClose);
        }
    }

    /**
     * Loads a bunch of files to the editor
     * @param files the file array also supporting an args array
     */
    public static void loadFiles(String [] files){
        loadFiles(Arrays.stream(files).map(s->s.trim().replaceAll("\"",""))
                .map(File::new).toArray(File[]::new));
    }

    /**
     * Loads a bunch of files
     * @param files the file array
     */
    public static void loadFiles(File  [] files){
        if(frame==null||!frame.isDisplayable()){
            setupEditor();
        }
        for(File f:files){
            if(f.exists()){
                boolean doStep2=true;
                if(f.getAbsolutePath().endsWith(".dot")||f.getAbsolutePath().endsWith(".xml")||f.getAbsolutePath().endsWith(".json")){
                    //Try directly loading the model. If that fails still start loading new file.
                    doStep2=!frame.loadAction(f);
                }
                if(doStep2){
                    //Load new ADD with the file name and dot extension
                    String fileName=getNameByStringHandling(f.getName()).orElse(f.getName())+".dot";
                    int nonce=1;
                    if(new File(f.getParentFile(),fileName).exists()){
                        String newFileName="";
                        do{
                            nonce++;
                            newFileName=nonce+fileName;
                        }while (new File(f.getParentFile(),newFileName).exists());
                        fileName=newFileName;
                    }
                    ADD empty=new ADD(Map.of(), Set.of(),true);
                    File target=new File(f.getParentFile(),fileName);
                    try {
                        //Make sure file is touched to see for the user and the tool
                        new ADD2Dot().exportModel(empty,target);
                    } catch (IOException e) {
                        ExceptionHandler.logException(e);
                    }
                    frame.loadADD(empty,
                            target.getAbsolutePath(),
                            JOptionPane.NO_OPTION);
                }
            }
        }
    }
}
