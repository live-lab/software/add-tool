package addtool.scripts;

import addtool.add.model.ADD;
import addtool.add2modest.MainDot2Modest;
import addtool.add2prism.MainDot2PRISM;
import addtool.add2uppaal.MainDot2Uppaal;
import addtool.add2xml.MainDot2XML;
import addtool.dot2add.ParserDOT;
import addtool.helpers.ExceptionHandler;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Script to compare conversion times to the various formats.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 28.09.2020
 */
public final class MainCheckPerformance {
        public static final int N=50;

    private MainCheckPerformance() {
    }

    public static void main(String[] args) throws IOException {
                PrintWriter pw=new PrintWriter(Path.of("test","Casestudy","Performance","stat.csv").toString());
                pw.println("Type;Nodes;mu;var");
                Files.walk(Path.of("test","Casestudy","Performance")).sorted().filter(p->p.toString().toLowerCase().endsWith("dot")).forEach(p->{
                        try {
                            analyseModel(MainDot2XML::main,"XML",p,".xml",pw);
                            analyseModel(MainDot2Uppaal::main,"UPPAAL",p,"_uppaal.xml",pw);
                            analyseModel(MainDot2PRISM::main,"PRISM",p,".prism",pw);
                            analyseModel(MainDot2Modest::main,"MODEST",p,".modest",pw);
                        } catch (Exception e) {
                            ExceptionHandler.logException(e);
                        }
                });
                pw.close();
        }
        public static void analyseModel(BiConsumer_ex<String,String> method,String name,Path in,String ending,PrintWriter pw) throws Exception {
            System.out.println(in+" "+name);
            long x=0;
            long x_2=0;
            for(int i=0;i<N;i++){
                long mil=System.currentTimeMillis();
                method.accept(in.toString(), in +ending);
                mil=System.currentTimeMillis()-mil;
                x+=mil;
                x_2+=mil*mil;
            }
            FileReader inRead = new FileReader(in.toFile());
            ADD add = ParserDOT.parseADD(inRead);
            double mu=((double)x)/N;
            double var=((double)x_2)/N-mu*mu;
            System.out.printf("Model to %s with %d nodes, has mean: %f, var %f%n",name,add.getId2Vertex().size(),(mu),(var));
            pw.println(String.format("%s;%d;%f;%f",name,add.getId2Vertex().size(),(mu),(var)));
            pw.flush();
        }
        @FunctionalInterface
        interface BiConsumer_ex<T, U> {
            void accept (T var1, U var2)throws Exception;
        }
}
