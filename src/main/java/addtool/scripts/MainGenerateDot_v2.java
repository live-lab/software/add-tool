package addtool.scripts;

import addtool.add.model.*;
import addtool.add2dot.ADD2Dot;
import addtool.dot2add.WrongStructureADDException;
import addtool.helpers.Constants;
import addtool.helpers.ExceptionHandler;
import org.jscience.mathematics.number.Rational;
import addtool.timeseries.PACTuple;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.*;
/**
 * Class to generate random ADD up to a certain size or amount.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 21.07.2020
 */
public final class MainGenerateDot_v2 {

    private static final Random random = new Random();
    /**
     * Maximum number for costs.
     */
    private static int max = 100;
    /**
     * Number of BEs. Total Number of ADDs = number².
     */
    private static int number = 40;
    /**
     * ratio of cross-linking. Recommended <=0.1.
     */
    private static double adv_join_rate=0.0;//,no_successor_rate=0.05;
    /**
     * Number of Maximum Predecessors.
     */
    private static int max_predecessor =2;
    /**
     * Number of Maximum Successors.
     */
    private static int max_suc=1;
    /**
     * Usable Operators.
     */
    private static final int AND_OR=2;
    private static final int SAND_OR=3;
    private static final int SAND_SOR=4;
    private static final int NAT_NOT=6;
    private static final int AND_OR_NOT=11;
    private static final int ALL_BUT_NOT=12;
    /**
     * BE Types.
     */
    private static final int BE_TIME=1;
    private static final int BE_ATTACKER=2;
    private static final int BE_BOTH_PLAYER=3;
    private static final int BE_TIME_ATTACK=4;
    private static final int BE_ALL=5;
    /**
     * actual operator set.
     */
    private static int bound=AND_OR;//Which Trees to generate
    /**
     * Actual BE set.
     */
    private static int bes=BE_ATTACKER;//Which Trees to generate
    /**
     * Max number of nodes before breakup.
     */
    private static int maxNumber =10;
    private static int minNumber=8;//Which Trees to generate

    private MainGenerateDot_v2() {
    }

    @SuppressWarnings("ObjectEquality")
    public static void main(String... args) throws IOException {
        //args = args.length==0?new String[]{Path.of("test","Gen","_").toString()}:args;
        String target=args.length==0?Path.of("data","Learning","small","_").toString():args[1];
        Map<String,String> options=Constants.convertArgs(args,2);
        if(options.containsKey("advjoin")){
            adv_join_rate=Double.parseDouble(options.get("advjoin"));
            options.remove("advjoin");
        }
        if(options.containsKey("ops")){
            bound=Integer.parseInt(options.get("ops"));
            options.remove("ops");
        }
        if(options.containsKey("maxpred")){
            max_predecessor =Integer.parseInt(options.get("maxpred"));
            options.remove("maxpred");
        }
        if(options.containsKey("maxsuc")){
            max_suc=Integer.parseInt(options.get("maxsuc"));
            options.remove("maxsuc");
        }
        if(options.containsKey("maxcost")){
            max=Integer.parseInt(options.get("maxcost"));
            options.remove("maxcost");
        }
        if(options.containsKey("numb")){
            number=Integer.parseInt(options.get("numb"));
            options.remove("numb");
        }
        if(options.containsKey("maxnumb")){
            maxNumber =Integer.parseInt(options.get("maxnumb"));
            options.remove("maxnumb");
        }
        if(options.containsKey("minnumb")){
            minNumber =Integer.parseInt(options.get("minnumb"));
            options.remove("minnumb");
        }
        if(options.containsKey("be")){
            bes=Integer.parseInt(options.get("be"));
            options.remove("be");
        }
        if(!options.isEmpty()){
            System.err.println("Unused arguments: "+options);
        }

        ArrayList<ArrayList<ADD>> adds = new ArrayList<>();
        int id = 0;

        ArrayList<ADD> addsA = new ArrayList<>();

        //First build Basic Events
        for (int i = 0; i < number; i++) {
            int state=random.nextInt(6);
            BasicEvent event;
            PACTuple prob= getRandomValues(1);
            PACTuple cost= getRandomValues(max);
            PACTuple delay= getRandomValues(max);
            PACTuple prob100 = new PACTuple(Rational.ONE, Rational.ZERO, Rational.ZERO);

            if(bes==BE_TIME||(bes==BE_TIME_ATTACK&&state%2==0)||(bes==BE_ALL&&state%3==0)){
                event =new BasicEventTime(String.valueOf(id),prob100/*prob*/,cost,delay, String.valueOf(id),"Uniform(0,1)");
            }else if(bes==BE_ATTACKER||
                        (bes==BE_TIME_ATTACK&&state%2==1)||
                        (bes==BE_BOTH_PLAYER&&state%2==1)||
                        (bes==BE_ALL&&state%3==1)){
                event= new BasicEventPlayer(String.valueOf(id), prob100/*prob*/,cost,delay, String.valueOf(id), Player.Attacker);
            }else{
                event= new BasicEventPlayer(String.valueOf(id), prob100 /*prob*/,cost,delay, String.valueOf(id), Player.Defender);
            }

            HashMap<String, Vertex> string2Vertex = new HashMap<>();
            string2Vertex.put(String.valueOf(id), event);
            Set<Vertex> vertices = new HashSet<>();
            vertices.add(event);

            //if (i % 2 == 0) {
                event.setSinkAttacker(true);
                ADD add = new ADD(string2Vertex, vertices, true);
                if(minNumber<=1){
                    PrintWriter writer = new PrintWriter(
                            target + "file-" + id + '_' +string2Vertex.size()+ ".dot",
                            StandardCharsets.UTF_8);
                    ADD2Dot.writeADD2DotFile(writer, add);
                    writer.close();
                }
                add.makeIDsConsistent();
                addsA.add(add);
            /*} else {
                event.setSinkAttacker(true);
                ADD add = new ADD(string2Vertex, vertices, true);
                add.makeIDsConsistent();
                addsD.add(add);
            }*/
            id++;
        }

        adds.add(0, addsA);

        //Build trees with increasing depth
        MAIN:for (int i = 1; i < number; i++) {
            adds.add(i, new ArrayList<>());

            ADD write2File = new ADD(new HashMap<>(), new HashSet<>(), false);
            for (int j = 1; j < number; j++) {
                try {
                    int operator;
                    if(bound==AND_OR_NOT){
                        operator=random.nextInt(3);
                        if(operator==2)operator=4;//map 2 to mean NOT for And-Or-Not-trees
                    }else if(bound==ALL_BUT_NOT){
                        operator=random.nextInt(5);
                        if(operator==4)operator=5;//MAP Not to Nat as Not ist not allowed
                    }else{
                        operator = random.nextInt(bound);
                    }
                    Vertex vertex = int2Vertex(operator, id);

                    if (vertex instanceof UnaryOp) {
                        // get ADD of depth j-1
                        ADD add = adds.get(i - 1).get(random.nextInt(adds.get(i - 1).size()));

                        Vertex addTo;

                        if (add.getNoSuccessor().size() == 1) {
                            write2File = new ADDBuilder(ADDBuilder.copyADD(add)).simpleJoin((UnaryOp) vertex).get();
                        } else {
                            List<Vertex> vs = Constants.getRandomObjects(
                                                    new ArrayList<>(add.getNoSuccessor()),
                                                    1,
                                                    random);
                            if (vs.isEmpty()) {
                            //System.out.println(add + "|" + add.getNoSuccessor());
                            //add.getOperators().forEach(v -> System.out.println("++" + v + "|" + v.getSuccessors()));
                                addTo = add.getNoSuccessor().iterator().next();
                            } else {
                                addTo = vs.get(0);
                            }
                            //System.out.println("Unary* "+addTo+"|"+vertex);
                            write2File = new ADDBuilder(ADDBuilder.copyADD(add))
                                            .simpleJoin((UnaryOp) vertex, addTo)
                                            .get();
                        }
                        write2File.makeIDsConsistent();
                        write2File.validateConnections();
                        adds.get(i).add(write2File);
                    } else if (vertex instanceof NaryOp) {
                        // get two ADDs, one of depth j-1 and copy them
                        ArrayList<ADD> choose = adds.get(random.nextInt(i));

                        ADD chooseADD01 = choose.get(random.nextInt((choose.size()) - 1));
                        ADD chooseADD02;
                        do {
                            chooseADD02 = adds.get(i - 1).get(random.nextInt((choose.size() - 1)));
                        } while (chooseADD01 == chooseADD02&&chooseADD02!=null);
                        chooseADD01.makeIDsConsistent();
                        chooseADD02.makeIDsConsistent();

                        // add ADD to ArrayList
                        //write2File = new ADD(id2vertex01, noSuccessors, false);

                        if ( adv_join_rate==0) {
                            //System.out.println("SIMPLE");
                            write2File=new ADDBuilder(ADDBuilder.copyADD(chooseADD01))
                                            .simpleJoin((NaryOp)vertex,chooseADD02)
                                            .get();
                            /*if(vertex.getPredecessors().stream().distinct().count()<2){
                                System.out.println("ERROR");
                            }
                            if(write2File.getId2Vertex().values().stream()
                                        .filter(v->v.getSuccessors().size()==0).count()>1){
                                System.out.println("ERROR");
                                System.out.println("Me");
                                ADDBuilder.printModel(write2File,System.out);


                                System.out.println("M1");
                                ADDBuilder.printModel(chooseADD01,System.out);

                                System.out.println("M2");
                                ADDBuilder.printModel(chooseADD02,System.out);

                                System.exit(-5);
                            }*/
                        }else if (random.nextDouble() >= adv_join_rate) {
                            //System.out.println("ADV");
                            ADD cad = ADDBuilder.copyADD(chooseADD01);

                            List<Vertex> predecessor_01 = Constants.getRandomObjects(
                                                                    new ArrayList<>(cad.getNoSuccessor()),
                                                            1,
                                                                    random);
                            List<Vertex> predecessor_02 = Constants.getRandomObjects(
                                                                    new ArrayList<>(chooseADD02.getNoSuccessor()),
                                                            1,
                                                                    random);

                            write2File = new ADDBuilder(cad)
                                    .join((NaryOp) vertex, chooseADD02, predecessor_01, predecessor_02, List.of(), List.of()).get();
                        } else {
                            //System.out.println("SIMPLE with ADV");
                            ADD cad = ADDBuilder.copyADD(chooseADD01);
                            int numberPredecessorsFrom1 = 1 + random.nextInt(max_predecessor - 2);
                            int numberPredecessorsFrom2 = 1 + random.nextInt(max_predecessor - numberPredecessorsFrom1);
                            if (numberPredecessorsFrom1 >= cad.getId2Vertex().size()) {
                                numberPredecessorsFrom1 = 1 + (cad.getId2Vertex().size() / 2);
                            }
                            if (numberPredecessorsFrom2 >= chooseADD02.getId2Vertex().size()) {
                                numberPredecessorsFrom2 = 1 + (chooseADD02.getId2Vertex().size() / 2);
                            }
                            //System.out.println(numberPredecessorsFrom1+"|"+numberPredecessorsFrom2);

                            List<Vertex> predecessor_01 = Constants.getRandomObjects(
                                                        new ArrayList<>(cad.getId2Vertex().values()),
                                                        numberPredecessorsFrom1,
                                                        random);
                            List<Vertex> predecessor_02 = Constants.getRandomObjects(
                                                        new ArrayList<>(chooseADD02.getId2Vertex().values()),
                                                        numberPredecessorsFrom2,
                                                        random);
                            //System.out.println(predecessor_01+"|"+predecessor_02);

                            int numberSuccessorsFrom1 = random.nextInt(max_suc - 2);
                            int numberSuccessorsFrom2 = random.nextInt(max_suc - numberSuccessorsFrom1);
                            if (numberSuccessorsFrom1 >= cad.getId2Vertex().size()) {
                                numberSuccessorsFrom1 = (cad.getId2Vertex().size() / 2);
                            }
                            if (numberSuccessorsFrom2 >= chooseADD02.getId2Vertex().size()) {
                                numberSuccessorsFrom2 = (chooseADD02.getId2Vertex().size() / 2);
                            }
                            //System.out.println(numberSuccessorsFrom1+"|"+numberSuccessorsFrom2);

                            List<Vertex> suc_01 = Constants.getRandomObjects(
                                                        new ArrayList<>(cad.getOperators()),
                                                        numberSuccessorsFrom1,
                                                        random);
                            List<Vertex> suc_02 = Constants.getRandomObjects(
                                                        new ArrayList<>(chooseADD02.getOperators()),
                                                        numberSuccessorsFrom2,
                                                        random);
                            //System.out.println(suc_01+"|"+suc_02);
                            //System.out.println("END");
                            write2File = new ADDBuilder(cad)
                                    .join((NaryOp) vertex, chooseADD02, predecessor_01, predecessor_02, suc_01, suc_02)
                                    .get();
                        }
                        write2File.makeIDsConsistent();
                        write2File.validateConnections();
                        adds.get(i).add(write2File);
                    }
                    if(write2File!=null&&!write2File.getNoSuccessor().isEmpty()&&write2File.getId2Vertex().size()>=minNumber){
                        PrintWriter writer = new PrintWriter(
                                target + "file-" + id + '_' +write2File.getId2Vertex().size() +".dot",
                                StandardCharsets.UTF_8);

                        ADD2Dot.writeADD2DotFile(writer, write2File);
                        if(write2File.getId2Vertex().size()>= maxNumber) {
                            break MAIN;
                        }
                        id++;
                    }
                } catch (WrongStructureADDException|IOException e) {
                    ExceptionHandler.logException(e);
                }
            }
        }
    }
    private static PACTuple getRandomValues(int max){
        Rational value;
        if(max==1){
            int divisor = random.nextInt(100);
            divisor++;
            int dividend = random.nextInt(divisor);
            value=Rational.valueOf(dividend, divisor);
        }else{
            value=Rational.valueOf(random.nextInt(max),1);
        }

        int divisor = random.nextInt(Integer.MAX_VALUE-100);
        divisor+=10;
        int dividend = random.nextInt(divisor /10);//max 0.1
        Rational unc=Rational.valueOf(dividend,divisor);
        Rational delta=Rational.valueOf(5,100);

        return new PACTuple(value,unc,delta);
    }

    private static Vertex int2Vertex(int i, int id) {
        return switch (i) {
            case 0 -> new And(String.valueOf(id), String.valueOf(id));
            case 1 -> new Or(String.valueOf(id), String.valueOf(id));
            case 2 -> new SAnd(String.valueOf(id), String.valueOf(id));
            case 3 -> new SOr(String.valueOf(id), String.valueOf(id));
            case 4 -> new Not(String.valueOf(id), String.valueOf(id));
            case 5 -> new Nat(String.valueOf(id), String.valueOf(id));
            default -> throw new IllegalArgumentException("Something wrong with generation of vertices!");
        };
    }
}
