package addtool.scripts;

import addtool.add.model.ADD;
import addtool.add.model.Vertex;
import addtool.dot2add.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Initial sequence generation to only return valid sequences.
 * Now replaced with more robust method
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 17.09.2020
 * @see addtool.learning.LearnHelper#generateSequences(ADD, int, int, Random)
 */
@Deprecated
public final class MainGenerateSequences {
    private static final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private MainGenerateSequences() {
    }

    public static void main (String... args) throws IOException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForLabelOperatorException, WrongValueForSinkException {
        FileReader in = new FileReader("test/ADDs/_file-335.dot");
        ADD add = ParserDOT.parseADD(in);

        int maxLength=add.getBasicEvents().size()*2;
        int seed=12345;
        int skip=0;
        int limit=100000;


        Random rand=new Random(seed);
        Supplier<String> generator=()->{
            int count=maxLength;
            StringBuilder builder = new StringBuilder();
            while (count-- != 0) {
                int character = rand.nextInt(ALPHA_NUMERIC_STRING.length());
                builder.append(ALPHA_NUMERIC_STRING.charAt(character));
            }
            return builder.toString();
        };
        //Write Metadata of modell
        PrintWriter pw1=new PrintWriter("test_meta.csv");
        pw1.println("ID;OP;P1;P2;P3;P4");
        add.getOperators().stream().map(v-> v.getId()+ ';' +v.getOperatorName()+ ';' +v.getPredecessors().stream().map(Vertex::getId).collect(Collectors.joining(";"))).forEach(pw1::println);
        pw1.close();

        PrintWriter pw=new PrintWriter("test.csv");
        pw.println(IntStream.range(0,maxLength).mapToObj(String::valueOf).collect(Collectors.joining(";")));
        //TODO this version does not work perfectly in respect to unique sequences.
        Stream.generate(generator).distinct().skip(skip).limit(limit).forEach(seq->{
            List<String> ids= new ArrayList<>(add.getBasicEventIds());
            List<String> end_seq=new ArrayList<>();
            for(int i=0;i<seq.length();i++){
                int pos=ALPHA_NUMERIC_STRING.indexOf(seq.charAt(i))%(ids.size()+1);
                if(pos==ids.size())continue;
                end_seq.add(ids.get(pos));
                boolean endTurn=false;
                if(!add.isValidSequence(end_seq)){
                    end_seq.remove(end_seq.size()-1);
                    endTurn=true;
                }
                if(add.postCheckSequence()||endTurn){
                    break;
                }
            }
            int dif=maxLength-end_seq.size();
            for(int i=0;i<dif;i++){
                end_seq.add("");
            }
            if(dif==maxLength)return;
            pw.println(String.join(";", end_seq));
        });


        pw.close();
    }
}
