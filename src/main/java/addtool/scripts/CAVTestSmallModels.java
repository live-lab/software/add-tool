package addtool.scripts;

import addtool.helpers.ExceptionHandler;
import addtool.learning.LearnADD;

import java.util.ArrayList;
import java.util.List;

/**
 * Generates data for genetic algorithm evaluation.
 * Trying to exactly reproduce small models by learning
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 09.07.2021
 */
public final class CAVTestSmallModels {

    private CAVTestSmallModels() {
    }

    public static void main(String... args){
        List<String> params= new ArrayList<>(List.of("-l","data/Learning/small","data/Learning/metasmall",
                "seed=123","suppresswarnings=1","train=0","meta=1","epochs=120","pool=15",
                "width=140","actions=1","traces=10000","er=1"
        ));
        runArgs(params);
        //Showcase
        /*args.remove(2);
        args.remove(1);
        args.add(1,"data/Learning/showcase_small_c19_1.dot");
        args.add(2,"data/Learning/metashowcase");
        runArgs(args);*/
    }

    public static void runArgs(List<String> args){
        System.out.println(String.join(" ", args));
        //noinspection LogException
        try {
            LearnADD.main(args.toArray(new String[]{}));
        } catch (Exception e) {
            ExceptionHandler.logException(e);
        }
    }
    /*public void testModelSize(){
        compareFolders("data/Learning/compare","data/Learning/metabetter");
        compareFolders("data/Learning/sequential","data/Learning/metasequential");
    }
    private void compareFolders(String folder1,String folder2){
        try (Stream<Path> pathStream = Files.walk(Path.of(folder1))) {
            List<Rational> vals=pathStream.map(p->{
                if(Files.isDirectory(p))return null;
                try {
                    ADD par= ParserDOT.parseADD(new FileReader(p.toFile()));
                    int size=par.getId2Vertex().size();


                    try (Stream<Path> pathStream2 = Files.walk(Path.of(folder2))) {
                        List<Rational> val=pathStream2.filter(p2->p2.toString().
                                endsWith(p.getFileName().toString())).
                                map(p2-> {
                            try {
                                return ParserDOT.parseADD(new FileReader(p2.toFile()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return null;
                        }).filter(Objects::nonNull).map(a->{
                            return Rational.valueOf(a.getId2Vertex().size(),size);
                        }).collect(Collectors.toList());

                        Rational res=Rational.ZERO;
                        for(Rational r:val){
                            res=res.plus(r);
                        }
                        if(val.size()==0){
                            System.err.println("ERROR on "+p.getFileName());
                            return null;
                        }
                        res=res.divide(Rational.valueOf(val.size(),1));
                        return res;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }).filter(Objects::nonNull).collect(Collectors.toList());
            Rational res=Rational.ZERO;
            System.err.println(vals.size());
            for(Rational r:vals){
                res=res.plus(r);
            }
            res=res.divide(Rational.valueOf(vals.size(),1));
            System.out.println(res.doubleValue());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
    //public void testexample(){
    //    List<String> args=new ArrayList(List.of("-l","data/Learning/vm_examples/exampleat.dot","data/Learning/vm_examples",
    //            "seed=123","suppresswarnings=0","train=0.8","traces=1000","meta=0","epochs=60",
    //            "width=40","actions=3","ops=1","ordered=0","success=80","mutation=0.8","weight=0.5","suffix=_res_"
    //            ));
    //    runArgs(args);
        /*List<String> args2=new ArrayList(List.of("-l","data/Learning/vm_examples/exampleat.dot","data/Learning/vm_examples",
                "seed=123","suppresswarnings=0","train=0.8","traces=1000","meta=0","epochs=60",
                "width=40","actions=3","ops=1","ordered=0","success=80","mutation=0.8","weight=0.5","suffix=_res_"
        ));
        runArgs(args2);*/
    //}
}
