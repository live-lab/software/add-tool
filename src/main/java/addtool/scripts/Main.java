package addtool.scripts;

import addtool.add.model.ADD;
import addtool.add.model.BasicEvent;
import addtool.add2dot.ADD2Dot;
import addtool.add2dot.MainDOT2CSV;
import addtool.add2modest.MainDot2Modest;
import addtool.add2prism.MainDot2PRISM;
import addtool.add2prism.MainDot2Semantics;
import addtool.add2uppaal.MainDot2Uppaal;
import addtool.add2uppaal.NADD2Uppaal;
import addtool.add2xml.MainDot2XML;
import addtool.analysisonadd.PrintType;
import addtool.analysisonadd.PrintableVisitor;
import addtool.feedback.*;
import addtool.timeseries.*;
import com.github.signaflo.timeseries.TimeSeries;
import com.google.common.collect.ImmutableList;
import addtool.helpers.Constants;
import addtool.learning.LearnADD;
import addtool.xml2add.MainXML2Dot;
import addtool.helpers.ExceptionHandler;

import java.io.*;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.DoubleConsumer;
import java.util.stream.Collectors;

import static addtool.analysisonadd.PrintableVisitor.getVisitorWithModel;
import static addtool.helpers.ADDIOHandler.importADDMasked;
import static addtool.helpers.TranslationTable.*;

/**
 * This class is used for commandline use of the tool.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 15.07.2020
 */
public final class Main {
    private Main() {
    }

    //It is the CLI Main Class. No point in being specific.
    @SuppressWarnings({"ProhibitedExceptionDeclared", "OverlyBroadThrowsClause"})
    public static void main(String [] args) throws Exception {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        if(args.length==0){
            System.out.println("Starting editor. If you wanted something else, check add-tool -h for help.");
            MainEditor.main(args);
            return;
        }
        switch(args[0]){
            case "-e":
                //long first=System.currentTimeMillis();
                MainEditor.main(args);
                //first=System.currentTimeMillis()-first;
                //System.out.println("Process finished in "+first+" ms");
                break;
            case "-c":
                //long first=System.currentTimeMillis();
                handleConvert(args);
                //first=System.currentTimeMillis()-first;
                //System.out.println("Process finished in "+first+" ms");
                break;
            case "-g":
                if(args.length<2){
                    System.out.println("Target folder needed. Check add-tool -h for help.");
                }else{
                    MainGenerateDot_v2.main(args);
                }
                break;
            case "-a"://ANALYSIS
                if(args.length<2){
                    System.out.println("Target file needed. Check add-tool -h for help.");
                }else{
                    calculateProps(args);
                }
                break;
            case "-f":
                handleFeedback(args);
                break;
            case "-p":
                handleProb(args);
                break;
            case "-l":
                LearnADD.main(args);
                break;
            case "-le":
                LearnADD.evaluate(args);
                break;
            case "-lv":
                LearnADD.crossValidate(args);
                break;
            case "-h":
                InputStream in = Main.class.getResourceAsStream(
                        Constants.getResourceFolder()+ "cl_help");
                if(in==null){
                    System.out.println("Could not locate help file. Please contact us for help.");
                    return;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                reader.lines().forEach(System.out::println);
                break;
            default:
                System.out.println(String.format("Option %s not found. Check add-tool -h for help.",args[0]));
                break;
        }
    }

    private static void handleProb(String[] args) {
        File modelFile=new File(args[1]);
        File data=new File(args[2]);
        if(!modelFile.exists()){
            System.err.println("File: "+modelFile.getAbsolutePath()+" not found. Please check spelling.");
            return;
        }
        if(!data.exists()){
            System.err.println("File: "+data.getAbsolutePath()+" not found. Please check spelling.");
            return;
        }
        ADD model=importADDMasked(modelFile.getAbsolutePath());
        if(model==null){
            //Error message is produced by masking function.
            return;
        }
        model.validateConnections();
        Map<String, String> argumentMap = Constants.convertArgs(args, 3);
        //Prepare values
        TimeSeriesAnalysis modelType;
        DoubleConsumer setProbability;
        if(argumentMap.containsKey("model")&&argumentMap.get("model").equals("gauss")){
            modelType= new GAUSSIAN();
            setProbability=((GAUSSIAN)modelType)::setAlpha;
        }else{
            modelType= new ARIMA();
            setProbability=((ARIMA)modelType)::setDelta;
        }
        String type="prob";
        if(argumentMap.containsKey("type")){
            type=argumentMap.get("type");
        }
        String dividend=null;
        if(argumentMap.containsKey("divid")){
            dividend=argumentMap.get("divid");
        }
        String divisor=null;
        if(argumentMap.containsKey("divis")){
            divisor=argumentMap.get("divis");
        }

        double delta=0.05;
        if(argumentMap.containsKey("delta")){
            try {
                delta = Double.parseDouble(argumentMap.get("delta"));
            }catch (NumberFormatException ex){
                System.err.println("Argument delta: "+argumentMap.get("delta")+" is not readable. Using 0.05 instead.");
            }
        }

        long timeout=180;
        if(argumentMap.containsKey("timeout")){
            try {
                timeout = Long.parseLong(argumentMap.get("timeout"));
            }catch (NumberFormatException ex){
                System.err.println("Argument timeout: "+argumentMap.get("timeout")+" is not readable. Using 180 seconds instead.");
            }
        }

        String id=null;
        if(argumentMap.containsKey("id")){
            id=argumentMap.get("id");
        }
        try(Scanner s=new Scanner(System.in)){
            while(!model.getBasicEventIds().contains(id)&&(!"q".equals(id))){
                System.err.println("ID: "+id+" not included in model.");
                System.err.println("Enter q to exit or use one of "+model.getBasicEventIds());
                id=s.next();
            }
        }
        if("q".equals(id))return;

        TimeSeries ts;
        try {

            List<String> cols= CSVHandler.getColumns(data.getAbsolutePath());
            if(cols.size()==1){
                ts=CSVHandler.getTimeSeriesFor(data.getAbsolutePath());
            }else if(cols.contains(dividend)){
                if(cols.contains(divisor)){
                    ts=CSVHandler.getTimeSeriesProbabilityFor(data.getAbsolutePath(),dividend,divisor);
                }else{
                    ts=CSVHandler.getTimeSeriesFor(data.getAbsolutePath(),dividend);
                }
            }else{
                System.err.println("Column "+dividend+" is invalid. Use one of "+cols+" instead and try again.");
                return;
            }
        } catch (IOException e) {
            ExceptionHandler.logException(e);
            return;
        }
        if(ts==null)return;
        CalcPAC cp =new CalcPAC(modelType);
        //sets alpha or delta depending on model type
        setProbability.accept(delta);
        cp.registerProgressStream(System.out);
        PACTuple res=null;
        Future<PACTuple> fpt=cp.run(ts);
        try {
            res=fpt.get(timeout,TimeUnit.SECONDS);
        } catch (InterruptedException|ExecutionException e) {
            ExceptionHandler.logException(e);
        } catch (TimeoutException e) {
            System.err.println("Operation reached timeout.");
            return;
        }
        if(res==null){
            System.out.println("PAC result was null. Check logs or try again.");
            return;
        }
        System.out.println(res);
        BasicEvent be=(BasicEvent)model.getId2Vertex().get(id);
        switch (type) {
            case "prob" -> {
                //System.out.println("Prob");
                be.setSuccessProbability(res.getValue());
                be.setUncertainty(res.getUncertainty());
                be.setProbabilityDelta(res.getDelta());
            }
            case "cost" -> be.setCost(res.copy());
            case "delay" -> be.setDelay(res.copy());
            default -> System.out.println("Unknown Type: " + type + " use prob, cost or delay.");
        }
        try {
            PrintWriter pw=new PrintWriter(modelFile.getAbsolutePath().toLowerCase().endsWith("dot")?modelFile.getAbsolutePath():modelFile.getAbsolutePath()+".dot");
            ADD2Dot.writeADD2DotFile(pw,model);
        } catch (FileNotFoundException e) {
            return;
        }
        System.out.println("finished");
    }

    private static void calculateProps(String[] args) {
        String type=args[1].toLowerCase();
        String file=args[2];
        String arguments=Arrays.stream(args).skip(3).collect(Collectors.joining(";")).toLowerCase();
        ADD model=importADDMasked(file);
        if(model==null)return;
        model.validateConnections();
        boolean pac=arguments.contains("pac");
        boolean recur=!arguments.contains("root");
        PrintType printType;
        if(pac && recur){
            printType= PrintType.PAC_RECURRENT;
        }else if(pac){
            printType= PrintType.PAC;
        }else if(recur){
            printType= PrintType.RECURRENT;
        }else{
            printType= PrintType.PLAIN;
        }

        long runtime;
        PrintableVisitor pv=PrintableVisitor.getMethodByShortName(type);
        if(pv==null){
            System.err.println("Unknown option: " + type + " Check add-tool -h for help.");
            return;
        }
        pv=getVisitorWithModel(pv,model);//Get concrete object for this specific model
        runtime = System.currentTimeMillis();
        System.out.println(pv.print(model,printType));
        runtime = System.currentTimeMillis() - runtime;
        /*switch (type) {
            case "prob" -> {
                runtime = System.currentTimeMillis();
                if (!pac) {
                    //Still in there as the runtime was better. It needed to be compared to the new algorithm.
                    //noinspection deprecation
                    ADDBuilder.printModelOld(model, System.out, false, recur);
                } else {
                    ADDBuilder.printModel(model, System.out, printType);
                }
                runtime = System.currentTimeMillis() - runtime;
            }
            case "cost" -> {
                runtime = System.currentTimeMillis();
                ADDBuilder.printModelCost(model, System.out, printType);
                runtime = System.currentTimeMillis() - runtime;
            }
            default -> {
                System.err.println("Unknown option: " + type + " Check add-tool -h for help.");
                return;
            }
        }*/
        //Save runtime
        if(arguments.contains("srt")){
            try {
                File f=new File("stat.csv");
                boolean exist=f.exists();
                PrintWriter pw=new PrintWriter(new FileOutputStream(f,true ));
                if(!exist)pw.println("Action,Diagram-size,exetime");
                pw.println(type+(pac?"-pac":"")+(!recur?"-root,":",")+model.getId2Vertex().size()+','+runtime);
                pw.close();
            } catch (FileNotFoundException e) {
                ExceptionHandler.logException(e);
            }
        }
    }
    public static void handleFeedback(String... args) {
        if(args.length<3){
            System.out.println("Type and File needed. Check add-tool -h for help.");
        }else{
            String type=args[1].toUpperCase();
            ADD model=importADDMasked(args[2]);

            if(model==null){
                System.out.printf("Could not parse model at %s. Model has to be either in  xml or dot format!",args[2]);
                return;
            }

            model.validateConnections();
            Collection<FeedbackTuple> results;
            switch (type) {
                case MODEST -> results = new FeedbackModest().getFeedback(model);
                case UPPAAL -> results = new FeedbackUPPAAL().getFeedback(model);
                case XML -> results = new FeedbackXML().getFeedback(model);
                case PRISM -> results = new FeedbackPRISM().getFeedback(model);
                default -> {System.out.printf("%s is no valid format. Check add-tool -h for help.", type);return;}
            }

            if(results.isEmpty())System.out.println("No problems");
            List<FeedbackTuple>res=new ArrayList<>(results);
            Collections.sort(res);//To have Messages of same type next to each other
            for(FeedbackTuple ft:res){
                String mask="\t[%s]\t%s at Vertex id %s";
                System.out.printf(mask, ft.getType(), ft.getText(), ft.getV1().getId() + (ft.getV2() == null ? "" : ", " + ft.getV2().getId()));

            }
        }
    }

    public static final String DOT2XML="dot2xml";
    public static final String DOT2PRISM="dot2prism";
    public static final String DOT2MODEST="dot2modest";
    public static final String DOT2SEMANTICS="dot2semantics";
    public static final String DOT2CSV="dot2csv";
    public static final String XML2DOT="xml2dot";
    public static final String DOT2UPPAAL="dot2uppaal";
    public static final String AUTO="auto";
    public static void handleConvert(String ... args)  {
        String mode;
        boolean modeFirst=false;
        List<String >modes= ImmutableList.of(DOT2XML,DOT2PRISM,DOT2MODEST,DOT2SEMANTICS,DOT2CSV,XML2DOT,DOT2UPPAAL,AUTO);
        if(modes.contains(args[1].toLowerCase())){
            mode=args[1].toLowerCase();modeFirst=true;
        }
        else mode=AUTO;
        String infile;
        String outfile;
        if(!modeFirst &&args.length>=3){
            infile=args[1];
            outfile=args[2];
        }else if(modeFirst&&args.length>=4){
            infile=args[2];
            outfile=args[3];
        }else{
            throw new UnsupportedOperationException("No suitable amount of args. Check add-tool -h for instructions.");
        }
        boolean srt=false;
        String tmpUppaalArg=null;
        long timeout=3000L;
        for(String s:args){
            if(s.equalsIgnoreCase("srt"))srt=true;
            if(s.toLowerCase().startsWith("timeout")){
                String[]splits=s.split("=");
                if(splits.length==2){
                    timeout=Long.parseLong(splits[1]);
                }
            }
            if(NADD2Uppaal.QueryState.of(s)!=null){
                tmpUppaalArg=s;
            }
        }
        String uppaalArg=tmpUppaalArg;
        File in=new File(infile);File out;
        if(new File(outfile).getParentFile()==null){//if-else needed to keep out effectively final
            out=Path.of(System.getProperty("user.dir"),outfile).toFile();
        }else{
            out=new File(outfile);
        }
        if(!in.exists()||!out.getParentFile().exists()){
            System.out.println("At least one file was not found. Please check spelling.");
            return;
        }

        if(in.isDirectory()^out.isDirectory()){
            System.out.println("For bulk mode both files have to be valid directories.");
            return;
        }
        if(in.isDirectory()&&out.isDirectory()){
            //TODO make check more generic
            if(!args[1].startsWith("dot")&&!args[1].startsWith("xml")){
                System.out.println("Bulk mode does not work with auto typing. See add-tool.jar -h for further instructions.");
            }
            String[] endings=args[1].split("2");
            String endIn=endings[0];
            String endOut=endings[1];
            Arrays.stream(Objects.requireNonNull(in.listFiles())).filter(f->f.getName().endsWith(endIn)).forEach(f->{
                try {
                    handleConvert(args[0],args[1],f.getAbsolutePath(), Path.of(out.getAbsolutePath(), f.getName().split("\\.")[0]+'.'+endOut).toString());
                } catch (RuntimeException e) {
                    ExceptionHandler.logException(e);
                }
            });
            return;
        }
        ExecutorService es= Executors.newSingleThreadExecutor();
        Future<AtomicLong> ft=es.submit(()->{
            AtomicLong time=new AtomicLong();
            try {
                time.set(System.currentTimeMillis());
                switch(mode){
                    case DOT2XML:
                        MainDot2XML.main(infile,outfile);
                        break;
                    case DOT2PRISM:
                        MainDot2PRISM.main(infile,outfile);
                        break;
                    case DOT2MODEST:
                        MainDot2Modest.main(infile,outfile);
                        break;
                    case DOT2SEMANTICS:
                        MainDot2Semantics.main(infile,outfile);
                        break;
                    case DOT2CSV:
                        MainDOT2CSV.main(infile,outfile);
                        break;
                    case XML2DOT:
                        MainXML2Dot.main(infile,outfile);
                        break;
                    case DOT2UPPAAL:
                        if(uppaalArg!=null){
                            MainDot2Uppaal.main(infile,outfile,uppaalArg);
                        }else{
                            MainDot2Uppaal.main(infile,outfile);
                        }
                        break;
                    case AUTO: default:
                        String [] splits= args[1].split("\\.");
                        if(splits.length<2&&!mode.equals(AUTO)){
                            System.out.println("No valid Filetype detected. Please check your spelling or see add-tool -h for help.");
                            return null;
                        }
                        //Autodetect mode by file endings
                        String [] InputSplits= infile.split("\\.");
                        if(InputSplits.length<2){
                            System.out.println("No valid Filetype detected. Please check your spelling or see add-tool -h for help.");
                            return null;
                        }
                        String option=InputSplits[InputSplits.length-1];
                        String [] OutputSplits= outfile.split("\\.");
                        if(OutputSplits.length<2){
                            System.out.println("No valid Filetype detected. Please check your spelling or see add-tool -h for help.");
                            return null;
                        }
                        option+='2'+OutputSplits[OutputSplits.length-1];
                        System.out.println("Auto conversion mode uses: "+option);
                        handleConvert(args[0],option,infile,outfile);
                        break;
                }
                time.set(System.currentTimeMillis()-time.get());
            } catch (Exception e) {
                time.set(-1);
                ExceptionHandler.logException(e);
            }
            return time;
        });
        es.shutdown();
        try {
            AtomicLong al=ft.get(timeout, TimeUnit.MILLISECONDS);
            if(srt&&al!=null){
                File f=new File("stat_c.csv");
                boolean exist=f.exists();
                PrintWriter pw=new PrintWriter(new FileOutputStream(f,true ));
                if(!exist)pw.println("Action,infile,Diagram-size,exetime");
                int size= Objects.requireNonNull(importADDMasked(infile)).getId2Vertex().size();
                pw.println(String.join(" ", args) +','+infile+','+size+','+al.get());
                pw.close();
            }
        }  catch (TimeoutException e) {
            System.out.println("TIMEOUT");
            System.exit(0);
        } catch(Exception ex){
            ExceptionHandler.logException(ex);
        }
    }
}
