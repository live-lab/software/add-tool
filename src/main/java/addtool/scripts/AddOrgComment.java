package addtool.scripts;

import addtool.helpers.ExceptionHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Adds Comment for authorship to existing dot files.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public final class AddOrgComment {
    private AddOrgComment() {
    }

    public static void main(String ... args) throws IOException {
        Path path=Path.of("artifact_examples");
        if(args.length>1){
            path=Path.of(args[1]);
        }
        Files.walk(path).forEach(p->{
            //Suppressed as it is only a script.
            //noinspection LogException
            try {
                if(Files.isRegularFile(p)){
                    String s=Files.readString(p, StandardCharsets.UTF_8);
                    s="#@author: Chair for Foundations of Software Reliability and Theoretical Computer Science Technical University of Munich\n"+s;
                    Files.writeString(p,s,StandardCharsets.UTF_8);
                }
            } catch (IOException e) {
                ExceptionHandler.logException(e);
            }

        });
    }
}
