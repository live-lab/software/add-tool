package addtool.scripts;

import addtool.add.model.ADD;
import addtool.add.model.Vertex;
import addtool.add2dot.ADD2Dot;
import addtool.dot2add.WrongStructureADDException;
import addtool.learning.*;
import org.jscience.mathematics.number.Rational;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import static addtool.learning.LearnHelper.getBeFromTraces;

/**
 * Script for learning the KDD CUP 1999 dataset.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 11.08.2021
 * @see LearnADD
 */
public final class LearnKDD {
    static final Locale locale= java.util.Locale.US;

    private LearnKDD() {
    }

    public static void main(String... args) throws IOException, TimeoutException, WrongStructureADDException, ExecutionException, InterruptedException {

        for(double w=0.1;w<=0.9;w+=0.1){
            for(double q=0.1;q<=0.9;q+=0.1){
                runWithSuffix(String.format(locale,"%.1f",q),String.format(locale,"%.1f",w));
            }
        }
    }
    public static void runWithSuffix(String suffix,String weight) throws IOException, WrongStructureADDException, TimeoutException, ExecutionException, InterruptedException {
        DataSet train= TraceIO.readBinTraces(new File("data/Learning/KDD_DATA/training"+suffix+".trace"));
        DataSet test=TraceIO.readBinTraces(new File("data/Learning/KDD_DATA/test"+suffix+".trace"));

        Set<Vertex> be=getBeFromTraces(train,new File("data/Learning/labels.txt"));

        LearnerConstraints lc=new LearnerConstraints();
        lc.setOps(1);

        if(false){
            LearnFuzzyADD fuzzyLearner=new LearnFuzzyADD(lc);
            Future<FuzzyADD> futureFuzzy=fuzzyLearner.runModelWOnly(be,train,test);
            FuzzyADD fuzzy=futureFuzzy.get();
            Rational Sens=LearnHelper.getSensitivity(fuzzy::evaluateSequence,test);
            Rational Spec=LearnHelper.getSpecificity(fuzzy::evaluateSequence,test);

            System.out.println("+++Sens: "+Sens.doubleValue()+"|Spec: "+Spec.doubleValue());
        }else{
            lc.setSeed(123);
            lc.setW1(weight);
            lc.setEPOCHS(100);
            lc.setPOOL_SIZE(30);
            lc.setEPOCH_WIDTH(60);
            lc.setNUM_ACTIONS(7);
            lc.setMUTATION_RATE(0.8);
            LearnADD learner=new LearnADD(lc);
            System.out.println(lc.buildSingleArgs());
            ADD add=learner.runModel(be,train,test,false);

            Rational Sens=LearnHelper.getSensitivity(add::evaluateSequence,test);
            Rational FP=LearnHelper.getFalsePositives(add::evaluateSequence,test);
            //Rational Spec=LearnHelper.getSpecificity(add::evaluateSequence,test);
            Path writeTO= Paths.get("data/Learning/KDD/result_ROC.csv");
            Files.writeString(writeTO,
                    String.format(locale,"%s;%s;%.6f;%.6f\n",
                            suffix,
                            weight,
                            Sens.doubleValue(),
                            FP.doubleValue())
                    , StandardOpenOption.CREATE,StandardOpenOption.APPEND);
            System.out.println("+++"+"q_"+suffix+"w_"+weight+" Sens: "+Sens.doubleValue()+"|FP: "+FP.doubleValue());

            //ADD add=learner.runFile(new File("data/Learning/training.trace"),new File("data/Learning/KDD"),false);
            ADD2Dot.writeADD2DotFile(new PrintWriter("data/Learning/KDD/result"+"q_"+suffix+"w_"+weight+".dot"),add);
        }
    }

}
