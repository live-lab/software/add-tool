package addtool.scripts;

import addtool.add.model.ADD;
import addtool.dot2add.*;
import addtool.helpers.Pair;
import addtool.learning.DataSet;
import addtool.learning.LearnADD;
import addtool.learning.LearnHelper;
import addtool.learning.LearnerConstraints;
import org.jscience.mathematics.number.Rational;

import java.io.*;
import java.util.Random;
import java.util.concurrent.TimeoutException;

/**
 * Class for crossvalidation in Learning
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 06.07.2021
 * @see LearnADD
 */
public final class LearningCrossValidation
{
    private LearningCrossValidation() {
    }

    public static void main(String ... args) throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException, TimeoutException {
        File outfile=new File("data/Learning/meta");
        int k=10;

        LearnerConstraints lc=new LearnerConstraints();
        lc.setEPOCHS(60);
        lc.setNUM_ACTIONS(3);
        lc.setEPOCH_WIDTH(40);
        lc.setSeed(123);
        lc.setTRAIN_SIZE(1000);
        //lc.setTimeout(5);
        File[] fs=new File("data/Learning/compare/").listFiles();
        if(fs==null)return;
        Rational totalVar=Rational.ZERO;

        PrintWriter pw=new PrintWriter(new File(outfile,"crossValidation.csv"));
        pw.println("file;k;mean;var");

        for(File f:fs){
            //Create Data
            Random random=new Random(lc.getSeed());
            ADD model= ParserDOT.parseADD(new FileReader(f.getAbsolutePath()));

            DataSet ref= LearnHelper.generateSequences(model,0,lc.getTRAIN_SIZE(),random);
            DataSet train=new DataSet();
            DataSet test=new DataSet();
            LearnHelper.splitDataSet(ref,train,test,random,lc.getTRAIN_RATE());

            LearnADD learner=new LearnADD(lc);

            Pair<Rational,Rational> results=learner.validateModel(model.getBasicEvents(),train,k);
            totalVar=totalVar.plus(results.getSecond());
            learner.resetTimeout();
            pw.println(String.format("%s;%d;%.3f;%.3f",f.getAbsolutePath(),k,
                    results.getFirst().doubleValue(),
                    results.getSecond().doubleValue()));
        }
        System.out.println("Mean Variance: "+totalVar.divide(Rational.valueOf(fs.length,1)).doubleValue());
        pw.close();
    }
}
