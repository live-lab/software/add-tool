package addtool.scripts;

import java.util.ArrayList;
import java.util.List;

import static addtool.scripts.CAVTestSmallModels.runArgs;
/**
 * Generates data for genetic algorithm evaluation.
 * Like {@link CAVTestLowSuccess} on sequential ADT
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 09.07.2021
 */
public final class CAVTestSequential {
    private CAVTestSequential() {
    }

    public static void main(String ... arguments){
        int[] successOptions= {5,10,20,40};
        double[] weights= {0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9};
        String currentSuccess="success=";
        String currentWeight="weight=0.3";
        String currentSuffix="";

        List<String> args=new ArrayList<>(List.of("-l","data/Learning/sequential","data/Learning/metasequential",
                "seed=123","suppresswarnings=0","train=0.8","traces=1000","meta=0","epochs=60",
                "width=40","actions=3","ops=4","ordered=0",currentSuccess,"mutation=0.8",currentWeight
                ,"er=1"));
        for (int success : successOptions) {
            args.remove(currentSuccess);

            currentSuccess = "success=" + success;

            args.add(currentSuccess);

            for (double weight : weights) {
                args.remove(currentWeight);
                args.remove(currentSuffix);

                currentSuffix = "suffix=_n" + success + "_w" + ((int) (weight * 10));
                currentWeight = "weight=" + weight;

                args.add(currentWeight);
                args.add(currentSuffix);

                System.out.println(String.join(" ", args));
                runArgs(args);

            }
        }

    }
}
