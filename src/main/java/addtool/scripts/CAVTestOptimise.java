package addtool.scripts;

import addtool.learning.LearnADD;
import org.jscience.mathematics.number.Rational;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Generates data for genetic algorithm evaluation.
 * Line-search over the various parameters
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 09.07.2021
 */
public final class CAVTestOptimise {
    private CAVTestOptimise() {
    }

    public static void main(String ... arguments) throws FileNotFoundException {
        //int[] succs=new int[]{5,10,20,40,80,160,320,500};
        //double[] weights=new double[]{0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9};
        int success=80;
        double weight=0.5;
        String paramPrefix="mutation";
        String currentParam="";
        double [] params={0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1};
        String currentSuccess="success="+success;
        String currentWeight="weight="+weight;
        String currentSuffix;
        List<String> args=new ArrayList<>(List.of("-l","data/Learning/compare","data/Learning/metaoptimize",
                "seed=123","suppresswarnings=0","train=0.8","traces=1000","meta=0","ordered=0",
                "width=40","epochs=60","actions=3",currentSuccess,currentWeight
                ,"er=1"));
        //width=40, epochs=60, actions=3, mutation=0.8
        //"width=40",
        FileOutputStream out=new FileOutputStream("data/Learning/timevalues.csv",true);
        PrintWriter printer=new PrintWriter(out,true);

        Map<Double, Rational> values=new HashMap<>();
        currentSuffix="suffix="+ '_' +paramPrefix;
        args.add(currentSuffix);
        for (double param : params) {
            args.remove(currentParam);
            currentParam = paramPrefix + '=' + param;
            args.add(currentParam);
            long time = System.currentTimeMillis();
            System.out.println(String.join(" ", args));
            //runArgs(args);
            printer.println(paramPrefix + ';' + param + ';' + (System.currentTimeMillis() - time));
            Rational last = LearnADD.getLastFitness();
            values.put(param, last);

        }
        System.out.println("Optimized "+paramPrefix);
        values.forEach((k,v)-> System.out.println(k+"->"+v.doubleValue()));
        Rational best=Rational.ZERO;
        Double bestValue=-1.0;
        for(Map.Entry<Double,Rational> e:values.entrySet()){
            if(e.getValue().isGreaterThan(best)){
                bestValue=e.getKey();
                best=e.getValue();
            }
        }
        System.out.println("Best: "+bestValue+" with "+best.doubleValue());
    }
}
