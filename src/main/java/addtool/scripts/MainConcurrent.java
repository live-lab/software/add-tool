package addtool.scripts;

import addtool.add.model.ADD;
import addtool.add.model.ADDBuilder;
import addtool.dot2add.*;
import addtool.helpers.ExceptionHandler;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * script to concurrently check for valid traces and their label.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 15.07.2020
 */
public final class MainConcurrent {
    private MainConcurrent() {
    }

    public static void main(String[] args) throws WrongStructureADDException, IOException, WrongValueForLabelOperatorException, NoValueSpecifiedException, WrongValueForSinkException {
        checkSequences();
    }

    /***
     * Concurrently checks a model (s. first line of method for path)
     * @throws IOException Thrown if problems on Input/output occur
     * @throws WrongStructureADDException If the model in the file is not specified correctly
     * @throws NoValueSpecifiedException  If the model in the file is not specified correctly
     * @throws WrongValueForLabelOperatorException If the model in the file is not specified correctly
     * @throws WrongValueForSinkException If the model in the file is not specified correctly,
     * @see ParserDOT#parseADD(FileReader)
     */
    public static void checkSequences() throws IOException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForLabelOperatorException, WrongValueForSinkException {
        String file="result";
        FileReader in = new FileReader(Path.of("test","ADDs",file+".dot").toString());
        ADD model = ParserDOT.parseADD(in);
        in.close();
        model.validateConnections();
        //ADD model=add;
        int threads=model.getBasicEvents().size(); int max=1000000;
        System.out.printf("Checking model with %d-BE and a max of %d results%n",threads,max);
        PrintWriter stat=new PrintWriter(Path.of("test","ADDs","stats.csv").toString());
        stat.println("name;checked;success");
        List<Runnable> runs=new ArrayList<>();
        CopyOnWriteArrayList<List<String>> res=new CopyOnWriteArrayList<>();
        model.getBasicEvents().forEach(b->{
            ArrayList<String> par=new ArrayList<>();par.add(b.getId());
            runs.add(()->{
                    //Copy trees
                    ADD mod=ADDBuilder.copyADD(model);
                    if(mod.isValidSequence(par)){
                        res.add(par);
                    }
                    new ADDBuilder(mod).getAllValidSequences(par,20,res,max);
                });
        });
        ExecutorService runner= Executors.newFixedThreadPool(threads);
        ExecutorService checker=Executors.newSingleThreadExecutor();
        checker.submit(()->{
            int old=0;
            int noChange=0;
            while(res.size()<max&&noChange<50){//
                //Wait
                if(res.size()==old){
                    noChange++;
                }else{
                    old=res.size();noChange=0;

                    List<List<String>> ram = new ArrayList<>(res);
                    int suc=getNumSuccesses(model,ram);
                    stat.println("file;"+ram.size()+ ';' +suc);

                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        ExceptionHandler.logException(e);
                    }
                }
                System.out.println(res.size()+" Size "+(res.size()<max));
            }
            System.out.println("Killing at "+res.size());
            System.out.println(res);
            runner.shutdown();//Executor is just closed for submission. Threads will not be terminated
            stat.close();
            //Evaluate Results
            AtomicInteger success=new AtomicInteger();
            AtomicInteger maxLength=new AtomicInteger();
            res.forEach(s -> {
                String[] seq = new String[s.size()];
                s.toArray(seq);
                boolean su = model.evaluateSequence(seq);
                if (su) {
                    success.incrementAndGet();
                    if(s.size()>maxLength.get()){
                        maxLength.set(s.size());
                    }
                }
            });
            System.out.printf("Of %d attacks %d where successful. That's %d percent; with max length of %d%n", res.size(), success.get(), 100 * success.get() / res.size(),maxLength.get());

            checker.shutdown();
        });
        runs.forEach(runner::submit);
    }
    public static int getNumSuccesses(ADD model,List<List<String>> sequences){
        AtomicInteger success=new AtomicInteger();
        sequences.forEach(s -> {
            String[] seq = new String[s.size()];
            s.toArray(seq);
            boolean su = model.evaluateSequence(seq);
            if (su) {
                success.incrementAndGet();
            }
        });
        return success.get();
    }
}
