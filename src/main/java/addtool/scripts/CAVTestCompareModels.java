package addtool.scripts;

import java.util.List;

import static addtool.scripts.CAVTestSmallModels.runArgs;

/**
 * Generates data for genetic algorithm evaluation.
 * Comparison of the learning with other methods.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 09.07.2021
 */
public final class CAVTestCompareModels {
    private CAVTestCompareModels() {
    }

    public static void main(String ... arguments){
        List<String>args= List.of("-l","data/Learning/compare","data/Learning/metacompare",
                "seed=123","suppresswarnings=0","train=0.8","meta=1","epochs=60",
                "width=40","actions=3","ordered=1","successrate=0.5","mutation=0.8",
                "ops=3"
        );
        runArgs(args);
    }
}
