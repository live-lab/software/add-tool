/**
 * All separate scripts formally used as tool entry points.
 * Contains the overall Main-class Main.java
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 21.09.2021
 */
package addtool.scripts;