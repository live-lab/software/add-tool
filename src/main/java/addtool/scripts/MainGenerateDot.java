package addtool.scripts;

import addtool.add.model.*;
import addtool.add2dot.ADD2Dot;
import addtool.dot2add.WrongStructureADDException;
import org.jscience.mathematics.number.Rational;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.*;

/**
 * Class to generate random ADD up to a certain size or amount.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 14.11.2018
 * @see MainGenerateDot_v2
 */
@Deprecated
public final class MainGenerateDot {

    private static final Random random = new Random();
    private static final int max = 100;
    private static final int number = 40;

    private MainGenerateDot() {
    }

    public static void main(String... args) throws IOException, WrongStructureADDException {
        args = args.length==0?new String[]{Path.of("data", "Gen").toString()}:args;
        ArrayList<ArrayList<ADD>> adds = new ArrayList<>();
        int id = 0;


        ArrayList<ADD> addsA = new ArrayList<>();
        ArrayList<ADD> addsD = new ArrayList<>();

        //First build Basic Events
        for (int i = 0; i < number; i++) {
            int divisor = random.nextInt(max);
            divisor++;
            int dividend = random.nextInt(divisor);
            int cost = random.nextInt(MainGenerateDot.max);
            Player player;

            if (i % 2 == 0) {
                player = Player.Attacker;
            } else {
                player = Player.Defender;
            }

            BasicEvent event = new BasicEventPlayer(String.valueOf(id), Rational.valueOf(dividend, divisor), Rational.valueOf(cost, 1), player, String.valueOf(i));

            HashMap<String, Vertex> string2Vertex = new HashMap<>();
            string2Vertex.put(String.valueOf(id), event);
            Set<Vertex> vertices = new HashSet<>();
            vertices.add(event);

            if (i % 2 == 0) {
                event.setSinkAttacker(true);
                ADD add = new ADD(string2Vertex, vertices, true);
                PrintWriter writer = new PrintWriter(args[0] + "file-" + id + ".dot", StandardCharsets.UTF_8);
                ADD2Dot.writeADD2DotFile(writer, add);
                writer.close();
                addsA.add(add);
            } else {
                event.setSinkAttacker(true);
                ADD add = new ADD(string2Vertex, vertices, true);
                addsD.add(add);
            }
            id++;
        }

        addsA.addAll(addsD);
        adds.add(0, addsA);

        //Build trees with increasing depth
        for (int i = 1; i < number; i++) {
            adds.add(i, new ArrayList<>());

            ADD write2File = new ADD(new HashMap<>(), new HashSet<>(), false);
            for (int j = 1; j < number; j++) {

                int operator = random.nextInt(6);
                Vertex vertex = int2Vertex(operator, id);

                if (vertex instanceof UnaryOp) {
                    // get ADD of depth j-1
                    ADD add = adds.get(i - 1).get(random.nextInt(adds.get(i - 1).size()));

                    // copy ADD
                    /*CopyVisitor copy = new CopyVisitor("");
                    add.accept(copy);
                    ADD deepCopy = copy.getADD();

                    // change top
                    Vertex top = deepCopy.getTop();
                    top.setSinkAttacker(false);
                    top.setSinkDefender(false);

                    // add predecessor to current sink
                    ((UnaryOp) vertex).setPredecessor(top);
                    top.addSuccessor(vertex);

                    // mark current vertex as sink
                    vertex.setSinkAttacker(true);

                    // generate ADD
                    HashMap<String, Vertex> id2vertex = (HashMap<String, Vertex>) deepCopy.getId2Vertex().clone();
                    id2vertex.put("" + id, vertex);
                    Set<Vertex> noSuccessors = new HashSet<>();
                    noSuccessors.add(vertex);

                    // add ADD to ArrayList
                    write2File = new ADD(id2vertex, noSuccessors, false);*/
                    write2File=new ADDBuilder(ADDBuilder.copyADD(add)).simpleJoin((UnaryOp)vertex).get();
                    write2File.makeIDsConsistent();
                    adds.get(i).add(write2File);
                } else if (vertex instanceof NaryOp) {
                    // get two ADDs, one of depth j-1 and copy them
                    ArrayList<ADD> choose = adds.get(random.nextInt(i));

                    ADD chooseADD01 = choose.get(random.nextInt((choose.size()) - 1));
                    ADD chooseADD02;
                    //noinspection ObjectEquality
                    do {
                        chooseADD02 = adds.get(i - 1).get(random.nextInt((choose.size() - 1)));
                    } while (chooseADD01 == chooseADD02);

                    /*CopyVisitor copy01 = new CopyVisitor("a");
                    CopyVisitor copy02 = new CopyVisitor("b");
                    chooseADD01.accept(copy01);
                    ADD deepCopy01 = copy01.getADD();

                    chooseADD02.accept(copy02);
                    ADD deepCopy02 = copy02.getADD();

                    // remove former sinks and add successors.
                    Vertex v01 = deepCopy01.getTop();
                    v01.setSinkAttacker(false);
                    v01.setSinkDefender(false);

                    Vertex v02 = deepCopy02.getTop();
                    v02.setSinkAttacker(false);
                    v02.setSinkDefender(false);

                    //set predecessors
                    ((NaryOp) vertex).setPredecessor01(v01);
                    ((NaryOp) vertex).setPredecessor02(v02);
                    v01.addSuccessor(vertex);
                    v02.addSuccessor(vertex);

                    // mark current sink as sink
                    vertex.setSinkAttacker(true);

                    // generate ADD
                    HashMap<String, Vertex> id2vertex01 = (HashMap<String, Vertex>) deepCopy01.getId2Vertex().clone();
                    HashMap<String, Vertex> id2vertex02 = (HashMap<String, Vertex>) deepCopy02.getId2Vertex().clone();

                    id2vertex01.putAll(id2vertex02);
                    id2vertex01.put("" + id, vertex);
                    Set<Vertex> noSuccessors = new HashSet<>();
                    noSuccessors.add(vertex);*/

                    // add ADD to ArrayList
                    //write2File = new ADD(id2vertex01, noSuccessors, false);
                    write2File = new ADDBuilder(ADDBuilder.copyADD(chooseADD01)).simpleJoin((NaryOp)vertex,chooseADD02).get();
                    write2File.makeIDsConsistent();
                    adds.get(i).add(write2File);
                }

                PrintWriter writer = new PrintWriter(args[0] + "file-" + id + ".dot", StandardCharsets.UTF_8);
                ADD2Dot.writeADD2DotFile(writer, write2File);

                id++;
            }


        }
    }

    private static Vertex int2Vertex(int i, int id) {
        return switch (i) {
            case 0 -> new And(String.valueOf(id), String.valueOf(id));
            case 1 -> new Or(String.valueOf(id), String.valueOf(id));
            case 2 -> new SAnd(String.valueOf(id), String.valueOf(id));
            case 3 -> new SOr(String.valueOf(id), String.valueOf(id));
            case 4 -> new Not(String.valueOf(id), String.valueOf(id));
            case 5 -> new Nat(String.valueOf(id), String.valueOf(id));
            default -> throw new IllegalArgumentException("Something wrong with generation of vertices!");
        };
    }
}
