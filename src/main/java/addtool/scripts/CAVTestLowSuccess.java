package addtool.scripts;

import java.util.ArrayList;
import java.util.List;

import static addtool.scripts.CAVTestSmallModels.runArgs;

/**
 * Generates data for genetic algorithm evaluation.
 * Evaluation with a small proportion of successful attacks.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 09.07.2021
 */
public final class CAVTestLowSuccess {
    private CAVTestLowSuccess() {
    }

    public static void main(String ... arguments){
        int[] succs= {5,10,20,40,80,160,320,500};
        double[] weights= {0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9};
        String currentSuccess="success=";
        String currentWeight="weight=0.3";
        String currentSuffix="";
        List<String> args=new ArrayList<>(List.of("-l","data/Learning/compare","data/Learning/metabetter",
                "seed=123","suppresswarnings=0","train=0.8","traces=1000","meta=0","epochs=60",
                "width=40","actions=3","ordered=0",currentSuccess,"mutation=0.8",currentWeight
                ,"er=1"));
        for (int success : succs) {
            args.remove(currentSuccess);

            currentSuccess = "success=" + success;

            args.add(currentSuccess);

            for (double weight : weights) {
                args.remove(currentWeight);
                args.remove(currentSuffix);

                currentWeight = "weight=" + weight;
                currentSuffix = "suffix=_n" + success + "_w" + ((int) (weight * 10));

                args.add(currentSuffix);
                args.add(currentWeight);

                System.out.println(String.join(" ", args));
                runArgs(args);

            }
        }
    }
}
