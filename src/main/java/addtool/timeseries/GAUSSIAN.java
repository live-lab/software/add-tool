package addtool.timeseries;

import com.github.signaflo.timeseries.TimeSeries;
import org.apache.commons.math3.distribution.NormalDistribution;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.DoubleConsumer;

/**
 * Calculates gaussian estimates for i.i.d. samples
 * @author "Florian Dorfhuber florian.dorfhuber@in.tum.de"
 */
public class GAUSSIAN implements TimeSeriesAnalysis {
    /**
     * Default alpha for analysis 0.05 =5%
     */
    public static final double DEFAULT_ALPHA=0.05;
    private double alpha=DEFAULT_ALPHA;
    private ExecutorService checker=null;
    @Override
    public void terminate() {
        if(checker!=null)checker.shutdownNow();
    }

    @Override
    public void addListener(DoubleConsumer progress) {

    }

    @Override
    public String getName() {
        return "GAUSSIAN";
    }

    /**
     * Update alpha
     * @param alpha a new alpha probability
     */
    public void setAlpha(double alpha){
        this.alpha=alpha;
    }
    @Override
    public Future<PACTuple> apply(TimeSeries timeSeries){
        double q=new NormalDistribution().inverseCumulativeProbability(1-alpha/2);

        checker= Executors.newSingleThreadExecutor();
        Future<PACTuple> future=checker.submit(()-> new PACTuple(timeSeries.mean(),q * timeSeries.stdDeviation()/Math.sqrt(timeSeries.size()),alpha));
        checker.shutdown();
        return future;
    }
}
