/**
 * Includes statistical Analysis for time series data.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
package addtool.timeseries;