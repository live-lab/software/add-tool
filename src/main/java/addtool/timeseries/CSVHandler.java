package addtool.timeseries;

import com.github.signaflo.timeseries.TimeSeries;
import com.github.signaflo.timeseries.Ts;
import addtool.helpers.ADDIOHandler;
import addtool.helpers.ExceptionHandler;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Imports CSV Data for later analysis.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public final class CSVHandler {
    private static final String[] TS = {};
    private static final String NONE="NONE";
    private static CSVFormat lastFormat=CSVFormat.DEFAULT;

    private CSVHandler() {
    }

    /**
     * Returns an array of column names of a file.
     * @param file The Inputfile
     * @return Array with the column names
     * @throws IOException If interaction with file goes wrong.
     * @see CSVHandler#getColumns(String)
     */
    public static String[] getColumnArray(String file) throws IOException {
        return getColumns(getRecords(file)).toArray(TS);
    }

    /**
     * Returns a list of column names of a file.
     * @param file The Inputfile
     * @return List with the column names
     * @throws IOException If interaction with file goes wrong.
     * @see CSVHandler#getColumns(List)
     * @see CSVHandler#getRecords(String)
     */
    public static List<String> getColumns(String file) throws IOException {
        return getColumns(getRecords(file));
    }

    /**
     * Returns a list of column names out of a set of records.
     * @param records The Inputfile
     * @return List with the column names
     * @see CSVHandler#getRecords(String)
     * @see CSVRecord
     */
    private static List<String> getColumns(List<CSVRecord> records){
        List<String> cols=new ArrayList<>();
        for (String field : records.get(0)) {
            cols.add(field);
        }
        return cols;
    }
    private static TimeSeries convertColumn(List<CSVRecord> rows,int cid){
        return convertColumn(rows,cid,1);
    }
    private static int getColumnID(List<CSVRecord> records, String col){
        return getColumns(records).indexOf(col);
    }
    private static TimeSeries convertColumn(List<CSVRecord> rows,int cid,int skip){
        //noinspection LogException
        try {
            return Ts.newAnnualSeries(rows.stream().skip(skip).
                    map(r -> r.get(cid)).mapToDouble(Double::parseDouble).toArray());
        }catch (RuntimeException ex){
            if(!ex.getMessage().contains(lastFormat.getDelimiterString())){
                //Switch format
                if(ex.getMessage().contains(";")){
                    lastFormat=CSVFormat.newFormat(';');
                }else if(ex.getMessage().contains(",")){
                    lastFormat=CSVFormat.newFormat(',');
                }
            }
            ExceptionHandler.logException(ex);
        }
        return null;
    }
    /**
     * Returns a list of {@link CSVRecord} out of an input file.
     * @param file The Inputfile
     * @return List with records
     * @throws IOException If interaction with file goes wrong.
     */
    public static List<CSVRecord> getRecords(String file) throws IOException{
        Reader in = new FileReader(file);
        return lastFormat.parse(in).getRecords();
    }

    /**
     * Creates a time series of a given file.
     * Uses the first column if not specified differently
     * @param file the input file
     * @return TimeSeries Data
     * @throws IOException If interaction with file goes wrong.
     * @see CSVHandler#getTimeSeriesFor(String, String)
     */
    public static TimeSeries getTimeSeriesFor(String file) throws IOException {
        return getTimeSeriesFor(file,getColumns(file).get(0));
    }
    /**
     * Creates a time series of a given file.
     * Uses the given column by name.
     * @param file the input file
     * @param col The column name
     * @return TimeSeries Data
     * @throws IOException If interaction with file goes wrong.
     */
    public static TimeSeries getTimeSeriesFor(String file, String col) throws IOException {
        List<CSVRecord> recs=getRecords(file);
        return convertColumn(recs, getColumnID(recs,col));
    }
    /**
     * Creates a time series with probabilities of a given file.
     * Uses the first column as dividend and the second as divisor if not specified differently
     * @param file the input file
     * @return TimeSeries Data
     * @throws IOException If interaction with file goes wrong.
     * @see CSVHandler#getTimeSeriesProbabilityFor(String, String,String)
     */
    public static TimeSeries getTimeSeriesProbabilityFor(String file) throws IOException {
        List<String> columns = getColumns(file);
        return getTimeSeriesProbabilityFor(file,columns.get(0),columns.get(1));
    }
    /**
     * Creates a time series with probabilities of a given file.
     * Uses the first param as dividend and the second as divisor if not specified differently
     * @param file the input file
     * @param dividend Dividend
     * @param divisor Divisor
     * @return TimeSeries Data
     * @throws IOException If interaction with file goes wrong.
     */
    public static TimeSeries getTimeSeriesProbabilityFor(String file, String dividend, String divisor) throws IOException {
        List<CSVRecord> recs=getRecords(file);
        TimeSeries ts1=convertColumn(recs, getColumnID(recs,dividend));
        TimeSeries ts2=convertColumn(recs, getColumnID(recs,divisor));
        if(ts1==null||ts2==null)return null;
        double [] newValue=new double[ts1.size()];
        for(int i=0;i<newValue.length;i++){
            newValue[i]=ts1.at(i)/ts2.at(i);
        }
        return Ts.newAnnualSeries(newValue);
    }

    /**
     * Opens an import dialog for reading TimeSeries data.
     * @param parent A Window to orient on
     * @return The imported TimeSeries
     * @throws IOException if interaktion with file goes wrong
     * @see CSVHandler#getTimeSeriesFor(String, String) 
     * @see CSVHandler#getTimeSeriesProbabilityFor(String, String, String)
     */
    public static TimeSeries openImportDialog(Window parent) throws IOException {
        //STEP 1: select file
        File f= ADDIOHandler.chooseFile(fl->fl.isDirectory()||fl.getAbsolutePath().toLowerCase().endsWith(".csv"),parent,false);
        //STEP 2: analyse file for columns
        List<String> cols=getColumns(f.getAbsolutePath());
        if(cols.size()==1){
            return getTimeSeriesFor(f.getAbsolutePath());
        }
        JComboBox<String> firstColumn=new JComboBox<>(cols.toArray(TS));
        cols.add(0,NONE);
        JComboBox<String> secondColumn=new JComboBox<>(cols.toArray(TS));
        JComponent[] inputs = {new JLabel(getResource("value_or_dividend")),firstColumn,
                new JLabel(getResource("divisor_optional")),secondColumn};
        int result = JOptionPane.showConfirmDialog(parent, inputs, getResource("value_select_title"), JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            String dividend=(String)firstColumn.getSelectedItem();
            String divisor=(String)secondColumn.getSelectedItem();
            if(NONE.equals(divisor)){
                return getTimeSeriesFor(f.getAbsolutePath(),dividend);
            }else{
                return getTimeSeriesProbabilityFor(f.getAbsolutePath(),dividend,divisor);
            }
        } else {
            return null;
        }
    }
}
