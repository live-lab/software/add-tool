package addtool.timeseries;

import com.github.signaflo.timeseries.TimeSeries;
import addtool.helpers.Constants;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.function.DoubleConsumer;
import java.util.function.Function;

/**
 * Interface for TimeSeries Analysis.
 * All classes should provide a Constructor with no arguments needed to be found by {@link TimeSeriesAnalysis#getOptions()}.
 * Therefore, default values for all parameters should be provided, if it is unclear which algorithm is used.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public interface TimeSeriesAnalysis extends Function<TimeSeries, Future<PACTuple>> {
    /**
     * Terminate analysis immediately
     */
    void terminate();

    /**
     * Adds a progress listener to be updated on progress with the completion percentage
     * @param progress
     */
    void addListener(DoubleConsumer progress);

    /**
     * @return Name of the analysis method
     */
    String getName();

    /**
     * Get selected TimeSeriesAnalysis
     * Will only find classes that have an accessible constructor with no arguments
     * @param name Name of the Type {@link TimeSeriesAnalysis#getName()}
     * @return an object if there exists a corresponding class, null otherwise
     */
    static TimeSeriesAnalysis getMethodByName(String name){
        Set<Class<? extends TimeSeriesAnalysis>> classes = Constants.findAllMatchingTypes("",TimeSeriesAnalysis.class);
        return classes.stream().map(c->{try{
            return c.getConstructor().newInstance();
        } catch (InvocationTargetException|InstantiationException|IllegalAccessException|NoSuchMethodException e) {
            return null;
        }}).filter(o-> Objects.nonNull(o) && o.getName().equals(name)).findFirst().orElse(null);
    }

    /**
     * Lists all Classes implementing the Interface in the classpath.
     * Will only find classes that have an accessible constructor with no arguments
     * @see TimeSeriesAnalysis#getName()
     * @return An Array of all Class names
     */
    static String[] getOptions(){
        Set<Class<? extends TimeSeriesAnalysis>> classes = Constants.findAllMatchingTypes("",TimeSeriesAnalysis.class);
        return classes.stream().map(c->{try{
            return c.getConstructor().newInstance().getName();
        } catch (InvocationTargetException|InstantiationException|IllegalAccessException|NoSuchMethodException e) {
            return null;
        }}).filter(Objects::nonNull).toArray(String[]::new);
    }
}
