package addtool.timeseries;

import com.github.signaflo.timeseries.TimeSeries;
import com.github.signaflo.timeseries.forecast.Forecast;
import com.github.signaflo.timeseries.model.arima.Arima;
import com.github.signaflo.timeseries.model.arima.ArimaOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.DoubleConsumer;

/**
 * Implementation of ARIMA for time series analysis.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public class ARIMA implements TimeSeriesAnalysis{
    /**
     * Default delta probability. 0.05=5%
     */
    public static final double DEFAULT_DELTA=0.05;
    private int [] minimalParameter =new int[6];
    private double minimalAIC=Double.MAX_VALUE;
    private int counter=0;
    private boolean terminated=false;
    private double delta=DEFAULT_DELTA;
    final List<DoubleConsumer> progressListeners=new ArrayList<>();

    @Override
    public Future<PACTuple> apply(TimeSeries timeSeries){
        return calcPACForTimeSeries(timeSeries,1);
    }

    /**
     * Calculates the PAC-Values for this method
     * @param timeSeries the data
     * @param steps the amount of staps
     * @return a pactuple in a Future object.
     */
    public synchronized Future<PACTuple> calcPACForTimeSeries(TimeSeries timeSeries, int steps) {
        minimalAIC = Double.MAX_VALUE;
        counter = 0;

        int step = 27;
        ExecutorService worker= Executors.newCachedThreadPool();
        for (int i = 0; i < 729; i += step) {
            int pos = i;
            worker.submit(() -> runTask(timeSeries, pos, pos + step));
        }
        worker.shutdown();

        ExecutorService checker = Executors.newSingleThreadExecutor();
        Future<PACTuple> future = checker.submit(() -> {
            try {
                while (!worker.awaitTermination(5, TimeUnit.SECONDS)) {
                    double progressPercent = (counter * 100.0 / 729.0);
                    progressListeners.forEach(pl -> pl.accept(progressPercent));
                    //System.out.println("Progress: "+progressPercent+"%");
                    //Progress: $i %   \r
                }
                //System.out.println(counter);
                return analyse(timeSeries, steps, delta);
            } catch (InterruptedException ex) {
                worker.shutdownNow();

            }
            return null;
        });
        checker.shutdown();
        return future;
    }
    private Runnable runTask(TimeSeries timeSeries, int skip, int bound){
        int p=0;
        int d=0;
        int q=0;
        int P=0;
        int D=0;
        int Q=0;
        for(int i=0;i< bound&&i<729&&!terminated;i++){//729 ist 3 hoch 6
            if(i>=skip) {
                ArimaOrder modelOrder = ArimaOrder.order(p, d, q, P, D, Q);
                Arima model = Arima.model(timeSeries, modelOrder);
                updateParam(model.aic(), p, d, q, P, D, Q);
            }
            Q++;
            if(Q>2){
                Q=0;D++;
                if(D>2){
                    D=0;P++;
                    if(P>2){
                        P=0;q++;
                        if(q>2){
                            q=0;d++;
                            if(d>2){
                                d=0;p++;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    private synchronized void updateParam(double aic, int p, int d, int q, int P, int D, int Q){
        if(minimalAIC >aic){
            minimalAIC = aic;
            minimalParameter =new int[]{p,d,q,P,D,Q};
        }
        counter++;
    }
    private PACTuple analyse(TimeSeries timeSeries, int steps, double delta){
        ArimaOrder modelOrder = ArimaOrder.order(minimalParameter[0], minimalParameter[1], minimalParameter[2], minimalParameter[3], minimalParameter[4], minimalParameter[5]);
        Arima model = Arima.model(timeSeries, modelOrder);
        //System.out.println(model.aic()); // Get and display the model AIC
        //System.out.println(model.coefficients()); // Get and display the estimated coefficients
        //System.out.println(java.util.Arrays.toString(model.stdErrors()));
        Forecast forecast = model.forecast(steps,delta);
        //System.out.println(forecast);
        TimeSeries point=forecast.pointEstimates();
        double estimate=point.at(point.size()-1);
        TimeSeries lb=forecast.lowerPredictionInterval();
        double estimateLowerBound=lb.at(lb.size()-1);
        TimeSeries ub=forecast.upperPredictionInterval();
        double estimateUpperBound=ub.at(ub.size()-1);
        return new PACTuple(estimate,estimateLowerBound,estimateUpperBound,delta);
    }
    @Override
    public void terminate(){
        terminated=true;
    }
    @Override
    public void addListener(DoubleConsumer listener){
        progressListeners.add(listener);
    }
    @Override
    public String getName(){
        return "ARIMA";
    }

    /**
     * Update delta probability
     * @param delta the target probability
     */

    public void setDelta(double delta) {
        this.delta = delta;
    }
}
