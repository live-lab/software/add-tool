package addtool.timeseries;

import com.github.signaflo.timeseries.TimeSeries;

import javax.swing.*;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Adapter class for calculating PAC-Values by the options {@link TimeSeriesAnalysis}.
 * @author Florian Dorfhube florian.dorfhuber@in.tum.de
 */
public class CalcPAC {
    private final List<JProgressBar> progress=new ArrayList<>();
    private final List<PrintStream> progressStream =new ArrayList<>();
    final TimeSeriesAnalysis analysis;

    /**
     * Initializes the object with a dataset
     * @param analysis the analysis method
     */
    public CalcPAC(TimeSeriesAnalysis analysis){
        this.analysis=analysis;
        this.analysis.addListener(this::receiveProgress);
    }

    /**
     * Updates the progress value
     * @param progressDouble the new progress
     */
    public void receiveProgress(double progressDouble){
        progress.forEach(pb->pb.setValue(Double.valueOf(progressDouble).intValue()));
        progressStream.forEach(s->s.print(getResource("progress")+": "+progressDouble+" %   \\r")); //NON-NLS
    }

    /**
     * Adds a progressbar to the object. It will be updated on analysis progress
     * @param jProgressBar a progressbar
     */
    public void registerProgressBar(JProgressBar jProgressBar){
        if(jProgressBar!=null){
            progress.add(jProgressBar);
            jProgressBar.setMaximum(100);
            jProgressBar.setMinimum(0);
        }
    }

    /**
     * Adds a printstream to analysis. Progress will be output there
     * @param out the stream to write there
     */
    public void registerProgressStream(PrintStream out) {
        if(out==null)return;
        progressStream.add(out);
    }

    /**
     * Start the analysis with a given dataset
     * @param timeSeries the dataset
     * @return
     */
    public Future<PACTuple> run(TimeSeries timeSeries){
        return analysis.apply(timeSeries);
    }

    /**
     * Shuts all processes down and returns the currently best estimate.
     */
    public void shutdownNow(){
        analysis.terminate();
    }
}
