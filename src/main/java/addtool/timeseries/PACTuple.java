package addtool.timeseries;

import addtool.dot2add.RationalFromString;
import org.jscience.mathematics.number.Rational;

import java.util.Collection;
import java.util.function.BiFunction;

public class PACTuple implements java.io.Serializable{
    Rational delta;
    Rational value;
    Rational uncertainty;

    public static final PACTuple NIL_TUPLE=new PACTuple(-1,-1,-1);
    public static final PACTuple SURE_TUPLE=new PACTuple(1,0,0);

    public PACTuple (double forecast,double lb,double ub,double sig){
        this(RationalFromString.getRationalFromString(String.valueOf(forecast)),RationalFromString.getRationalFromString(String.valueOf(lb)),
                RationalFromString.getRationalFromString(String.valueOf(ub)),RationalFromString.getRationalFromString(String.valueOf(sig)));
    }
    public PACTuple (double forecast,double uc,double sig){
        this(RationalFromString.getRationalFromString(String.valueOf(forecast)),RationalFromString.getRationalFromString(String.valueOf(uc)),
                RationalFromString.getRationalFromString(String.valueOf(sig)));
    }
    public PACTuple (Rational forecast,Rational lb,Rational ub,Rational sig){
        this(forecast,ub.minus(forecast),sig);
    }
    public PACTuple (Rational forecast,Rational unc,Rational delta){
        this.delta=delta;
        this.value=forecast;
        this.uncertainty=unc;
    }
    public PACTuple setDelta(Rational delta) {
        return new PACTuple(this.value,this.uncertainty,delta);
    }

    public PACTuple setValue(Rational value) {
        return new PACTuple(value,this.uncertainty,this.delta);
    }

    public PACTuple setUncertainty(Rational uncertainty) {
        return new PACTuple(this.value,uncertainty,this.delta);
    }
    public Rational getDelta() {
        return delta;
    }

    public Rational getValue() {
        return value;
    }

    public Rational getUncertainty() {
        return uncertainty;
    }
    public String toString(){
        return String.format("Value: %f, Uncertainty %f, Delta %f",value.doubleValue(),uncertainty.doubleValue(),delta.doubleValue());
    }
    public PACTuple copy(){
        return new PACTuple(value,uncertainty,delta);
    }
}
