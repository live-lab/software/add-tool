package addtool.analysisonadd;

import addtool.add.model.*;
import addtool.add.operators.Comparison;
import org.jscience.mathematics.number.Rational;
import addtool.timeseries.PACTuple;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * implements simple Cost Heuristic.
 *
 * @author Julia Eisentraut, julia.kraemer@in.tum.de
 * @since 5.10.2020
 */
public class CostHeuristic implements PrintableVisitor {

    public static final String name="Cost";
    private final HashMap<Vertex, PACTuple> cost = new HashMap<>();
    private final HashMap<Vertex, PACTuple> costFailing = new HashMap<>();
    private boolean notSatisfied = false;
    private Vertex notSatisfiedConstraint = null;

    /**
     * maps all basic events to their cost.
     * @param costBE maps all basic events to their cost
     */
    public CostHeuristic(Map<BasicEvent, PACTuple> costBE) {
        cost.putAll(costBE);

        for (BasicEvent b : costBE.keySet()) {
            costFailing.put(b, new PACTuple(0, 0, 0));
        }
    }
    /**
     * maps all basic events to their cost.
     * @param model a model to start with
     */
    public CostHeuristic(ADD model) {
        this(model.getBasicEvents().stream()
                .map(v->(BasicEvent)v)
                .collect(Collectors.toMap(v->v, BasicEvent::getCosts)));
    }

    /**
     * Returns a map assigning each (visited) vertex its cost.
     * @return a map assigning each (visited) vertex its cost
     */
    public Map<Vertex, PACTuple> getCost() {
        return new HashMap<>(cost);
    }
    public Map<String, PACTuple> getCostById() {
        return cost.entrySet().stream().collect(Collectors.toMap(e->e.getKey().getId(), Map.Entry::getValue));
    }

    /**
     * Checks if Cost constraints are satisfied.
     * @return false if the constraint can be satisfied and true if there is a cost vertex
     * with a violated cost constraint
     */
    public boolean isNotSatisfied() {
        return notSatisfied;
    }

    /**
     * Returns the unsatisfied constraint.
     * @return a vertex labelled with COST, which cost constraint is violated
     */
    public Vertex getNotSatisfiedConstraint() {
        return notSatisfiedConstraint;
    }

    @Override
    public String toString() {
        return "COST:" + cost;
    }


    @Override
    public void visit(BasicEventPlayer basicEvent) {
        // do nothing since cost of basic events are already in map
    }

    @Override
    public void visit(BasicEventTime basicEvent) {
        // do nothing since cost of basic events are already in map
    }

    @Override
    public void visit(And and) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            visitAnd(and);
        }
    }

    @Override
    public void visit(SAnd sAnd) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            visitAnd(sAnd);
        }
    }

    private void visitAnd(NaryOp vertex){
        List<Vertex> predecessors = vertex.getPredecessors();

        // cost of this vertex is sum of the costs of all predecessors
        Rational sum = Rational.ZERO;
        Rational unc = Rational.ZERO;
        for (Vertex v : predecessors) {
            PACTuple pc = cost.get(v);
            Rational cv = pc.getValue();
            unc = unc.plus(pc.getUncertainty());
            if (cv != null)
                sum = sum.plus(cv);
        }
        Rational delta = PAC.calcProbabilityDelta(predecessors.stream().map(cost::get).collect(Collectors.toList()));
        PACTuple pt = new PACTuple(sum, unc, delta);
        cost.put(vertex, pt);

        //cost of failing of this vertex
        Rational min = Rational.valueOf(Long.MAX_VALUE,1);//costFailing.get(vertex.getPredecessor01()).getValue();
        for (Vertex v : predecessors) {
            PACTuple pc = costFailing.get(v);
            Rational current = pc.getValue();
            if (current != null && (min == null || current.isLessThan(min))) {
                min = current;
            }
        }
        Rational unc2 = PAC.calcUncertaintyForMin(predecessors.stream().map(costFailing::get).collect(Collectors.toList()));
        if (min == null) min = Rational.ZERO;

        PACTuple pt2 = new PACTuple(min, unc2, delta);
        costFailing.put(vertex, pt2);
    }

    @Override
    public void visit(Or or) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            visitOr(or);
        }
    }

    @Override
    public void visit(SOr sOr) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            visitOr(sOr);
        }
    }

    private void visitOr(NaryOp vertex) {
        List<Vertex> predecessors = vertex.getPredecessors();

        // cost of this vertex is min over the costs of all predecessors
        Rational min = Rational.valueOf(Long.MAX_VALUE,1);//cost.get(vertex.getPredecessor01()).getValue();
        for (Vertex v : predecessors) {
            PACTuple pc=cost.get(v);
            Rational current = pc.getValue();
            if (current != null && (min == null || current.isLessThan(min))) {
                min = current;
            }
        }
        Rational delta = PAC.calcProbabilityDelta(predecessors.stream().map(cost::get).collect(Collectors.toList()));
        Rational unc = PAC.calcUncertaintyForMin(predecessors.stream().map(cost::get).collect(Collectors.toList()));
        if (min == null) min = Rational.ZERO;

        PACTuple pt = new PACTuple(min, unc, delta);
        cost.put(vertex, pt);

        // cost of failing this vertex
        Rational sum = Rational.ZERO;
        Rational unc2 = Rational.ZERO;
        for (Vertex v : predecessors) {
            PACTuple pc = costFailing.get(v);
            Rational cv = pc.getValue();
            unc2 = unc2.plus(pc.getUncertainty());
            if (cv != null)
                sum = sum.plus(cv);
        }
        PACTuple pt2 = new PACTuple(sum, unc2, delta);
        costFailing.put(vertex, pt2);
    }

    @Override
    public void visit(If vertex) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else {
            throw new UnsupportedOperationException("CostHeuristic does not support ifGates.");
        }

    }

    @Override
    public void visit(Not vertex) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            ADDVisitor.propagateIdentity(vertex, cost, costFailing);
        }

    }

    @Override
    public void visit(NotTrue not) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            ADDVisitor.propagateIdentity(not, cost, costFailing);
        }
    }

    @Override
    public void visit(Nat vertex) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            ADDVisitor.propagateIdentity(vertex, cost, costFailing);
        }

    }

    @Override
    public void visit(Ivr vertex) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            ADDVisitor.propagateIdentity(vertex, cost, costFailing);
        }

    }

    @Override
    public void visit(Trigger vertex) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            ADDVisitor.propagateIdentity(vertex, cost, costFailing);
        }

    }

    @Override
    public void visit(Reset vertex) {

        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else {
            throw new UnsupportedOperationException("CostHeuristic does not support Reset.");
        }
    }

    @Override
    public void visit(Cost vertex) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else{
            Vertex predecessor = vertex.getPredecessor();
            if(vertex.getBound().length==0||vertex.getOp().length==0||vertex.getPredecessor()==null){
                return;
            }
            Rational bound = vertex.getBound()[0];
            Comparison comp = vertex.getOp()[0];

            if (Comparison.evaluate(comp, cost.get(predecessor).getValue(), bound)) {
                cost.put(vertex, cost.get(predecessor));
                costFailing.put(vertex, costFailing.get(predecessor));
            } else {
                notSatisfied = true;
                notSatisfiedConstraint = vertex;
            }
        }
    }

    @Override
    public void visit(Countermeasure vertex) {
        if (notSatisfied) {
            //skip since a cost constraint could not be satisfied
        } else {
            throw new UnsupportedOperationException("CostHeuristic does not support Countermeasure.");
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShortName() {
        return "cost";
    }

    @Override
    public String printVertex(Vertex v, PrintType printType) {
        return printVertex(v,printType,0);
    }
    public String printVertex(Vertex v, PrintType printType,int depth) {
        StringBuilder out=new StringBuilder();
        out.append("\t".repeat(depth));
        PACTuple pt=cost.get(v);
        if(pt==null){
            pt=PACTuple.NIL_TUPLE;
        }
        String os=String.format("%s %s: %s %s %s: %f",
                v.getId(),getResource("operator_short"),
                v.getOperatorName(),getResource("has"),
                getResource(getName()),pt.getValue().doubleValue());
        if(printType==PrintType.PAC||printType==PrintType.PAC_RECURRENT){
            os+=(String.format(", ε: %f, ẟ: %f",pt.getUncertainty().doubleValue(),pt.getDelta().doubleValue()));
        }
        out.append(os);
        out.append("\n");
        if(printType==PrintType.RECURRENT||printType==PrintType.PAC_RECURRENT){
            v.getPredecessors().forEach(k->out.append(printVertex(k,printType,depth+1)));
        }
        return out.toString();
    }
}
