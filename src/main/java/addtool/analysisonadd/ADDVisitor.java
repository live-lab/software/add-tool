package addtool.analysisonadd;

import addtool.add.model.*;

import java.util.Map;

/**
 * Visitor pattern for ADD analysis.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 06.11.2018
 */
public interface ADDVisitor {

    void visit(BasicEventPlayer basicEvent);

    void visit(BasicEventTime basicEvent);

    void visit(And and);

    void visit(Or or);

    void visit(SAnd sAnd);

    void visit(SOr sOr);

    void visit(If ifGate);

    void visit(Not not);

    void visit(NotTrue not);

    void visit(Nat nat);

    void visit(Ivr ivr);

    void visit(Trigger trigger);

    void visit(Reset reset);

    void visit(Cost vertex);

    void visit(Countermeasure vertex);

    /**
     * for unary vertices only, success and failing probability are the same as the inputs'.
     *
     * @param vertex for which to propagate the uncertainty
     */
    static <E> void propagateIdentity(UnaryOp vertex, Map<Vertex, E> success, Map<Vertex, E> failure) {
        Vertex predecessor = vertex.getPredecessor();
        success.put(vertex, success.get(predecessor));
        failure.put(vertex, failure.get(predecessor));
    }

}
