package addtool.analysisonadd;

import addtool.add.model.*;
import org.jscience.mathematics.number.Rational;
import addtool.timeseries.PACTuple;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * implements simple Delay Heuristic.
 *
 * @author Julia Eisentraut, julia.kraemer@in.tum.de
 * @since 5.10.2020
 */
public class DelayHeuristic implements PrintableVisitor {

    public static final String name="Delay";
    private final HashMap<Vertex, PACTuple> delay = new HashMap<>();
    private final HashMap<Vertex, PACTuple> delayFailing = new HashMap<>();

    /**
     * Heuristic for analysing delays.
     * @param delayBE maps all basic events to their cost.
     */
    public DelayHeuristic(Map<BasicEvent, PACTuple> delayBE) {
        delay.putAll(delayBE);

        for (BasicEvent b : delayBE.keySet()) {
            delayFailing.put(b, new PACTuple(0, 0, 0));
        }
    }
    /**
     * maps all basic events to their delay.
     * @param model a model to start with
     */
    public DelayHeuristic(ADD model) {
        this(model.getBasicEvents().stream()
                .map(v->(BasicEvent)v)
                .collect(Collectors.toMap(v->v, BasicEvent::getDelay)));
    }

    /**
     * Returns full map of delays.
     * @return a map assigning each (visited) vertex its delay.
     */
    public Map<Vertex, PACTuple> getDelay() {
        return new HashMap<>(delay);
    }
    public Map<String, PACTuple> getDelayById() {
        return delay.entrySet().stream().collect(Collectors.toMap(e->e.getKey().getId(), Map.Entry::getValue));
    }

    @Override
    public String toString() {
        return "DELAY:" + delay;
    }

    @Override
    public void visit(BasicEventPlayer basicEvent) {
        // do nothing since cost of basic events are already in map
    }

    @Override
    public void visit(BasicEventTime basicEvent) {
        // do nothing since cost of basic events are already in map
    }

    @Override
    public void visit(And and) {
        visitAnd(and);
    }

    @Override
    public void visit(SAnd sAnd) {
        visitAnd(sAnd);
    }

    private void visitAnd(NaryOp vertex){

        List<Vertex> predecessors = vertex.getPredecessors();

        // delay of this vertex is sum of the costs of all predecessors
        Rational max = Rational.ZERO;
        for (Vertex v : predecessors) {
            PACTuple pc = delay.get(v);

            if (max.isLessThan(pc.getValue())) {
                max = pc.getValue();
            }
        }
        Rational unc = PAC.calcUncertaintyForMax(predecessors.stream().map(delay::get).collect(Collectors.toList()));
        Rational delta = PAC.calcProbabilityDelta(vertex.getPredecessors().stream().map(delay::get).collect(Collectors.toList()));
        PACTuple pt = new PACTuple(max, unc, delta);
        delay.put(vertex, pt);

        // delay of failing this vertex
        Rational min = Rational.valueOf(Long.MAX_VALUE,1);//delay.get(vertex.getPredecessor01()).getValue();
        for (Vertex v : predecessors) {
            PACTuple pc = delayFailing.get(v);
            Rational current = pc.getValue();
            if (current != null && (min == null || current.isLessThan(min))) {
                min = current;
            }
        }
        Rational unc2 = PAC.calcUncertaintyForMin(predecessors.stream().map(delay::get).collect(Collectors.toList()));
        if (min == null) min = Rational.ZERO;

        PACTuple pt2 = new PACTuple(min, unc2, delta);
        delayFailing.put(vertex, pt2);
    }

    @Override
    public void visit(Or or) {
        visitOr(or);
    }

    @Override
    public void visit(SOr sOr) {
        visitOr(sOr);
    }

    private void visitOr(NaryOp vertex){
        List<Vertex> predecessors = vertex.getPredecessors();

        // cost of this vertex is min over the costs of all predecessors
        Rational min = Rational.valueOf(Long.MAX_VALUE,1);//delay.get(vertex.getPredecessor01()).getValue();
        for (Vertex v : predecessors) {
            PACTuple pc = delay.get(v);
            Rational current = pc.getValue();
            if (current != null && (min == null || current.isLessThan(min))) {
                min = current;
            }
        }
        Rational delta = PAC.calcProbabilityDelta(predecessors.stream().map(delay::get).collect(Collectors.toList()));
        Rational unc = PAC.calcUncertaintyForMin(predecessors.stream().map(delay::get).collect(Collectors.toList()));
        if (min == null) min = Rational.ZERO;

        PACTuple pt = new PACTuple(min, unc, delta);
        delay.put(vertex, pt);

        //delay of failing this vertex sum of delays of failing all vertices
        Rational max = Rational.ZERO;
        for (Vertex v : predecessors) {
            PACTuple pc = delayFailing.get(v);

            if (max.isLessThan(pc.getValue())) {
                max = pc.getValue();
            }
        }
        Rational unc2 = PAC.calcUncertaintyForMax(predecessors.stream().map(delay::get).collect(Collectors.toList()));
        PACTuple pt2 = new PACTuple(max, unc2, delta);
        delayFailing.put(vertex, pt);
    }

    @Override
    public void visit(If ifGate) {
        throw new UnsupportedOperationException("DelayHeuristic does not support ifGates.");
    }

    @Override
    public void visit(Not not) {
        ADDVisitor.propagateIdentity(not, delay, delayFailing);
    }

    @Override
    public void visit(NotTrue not) {
        ADDVisitor.propagateIdentity(not, delay, delayFailing);
    }

    @Override
    public void visit(Nat nat) {
        ADDVisitor.propagateIdentity(nat, delay, delayFailing);
    }

    @Override
    public void visit(Ivr ivr) {
        ADDVisitor.propagateIdentity(ivr, delay, delayFailing);
    }

    @Override
    public void visit(Trigger trigger) {
        ADDVisitor.propagateIdentity(trigger, delay, delayFailing);
    }

    @Override
    public void visit(Reset reset) {
        throw new UnsupportedOperationException("DelayHeuristic does not support Reset.");
    }

    @Override
    public void visit(Cost vertex) {
        ADDVisitor.propagateIdentity(vertex, delay, delayFailing);
    }

    @Override
    public void visit(Countermeasure vertex) {
        throw new UnsupportedOperationException("DelayHeuristic does not support Countermeasure.");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShortName() {
        return "delay";
    }

    @Override
    public String printVertex(Vertex v, PrintType printType) {
        return printVertex(v,printType,0);
    }


    public String printVertex(Vertex v, PrintType printType,int depth) {
        StringBuilder out=new StringBuilder();
        out.append("\t".repeat(depth));
        PACTuple pt=delay.get(v);
        if(pt==null){
            pt=PACTuple.NIL_TUPLE;
        }
        String os=String.format("%s %s: %s %s %s: %f",
                v.getId(),getResource("operator_short"),
                v.getOperatorName(),getResource("has"),
                getResource(getName()),pt.getValue().doubleValue());
        if(printType==PrintType.PAC||printType==PrintType.PAC_RECURRENT){
            os+=(String.format(", ε: %f, ẟ: %f",pt.getUncertainty().doubleValue(),pt.getDelta().doubleValue()));
        }
        out.append(os);
        out.append("\n");
        if(printType==PrintType.RECURRENT||printType==PrintType.PAC_RECURRENT){
            v.getPredecessors().forEach(k->out.append(printVertex(k,printType,depth+1)));
        }
        return out.toString();
    }
}
