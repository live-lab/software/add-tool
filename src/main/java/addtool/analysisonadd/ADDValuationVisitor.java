package addtool.analysisonadd;

import addtool.add.model.*;
import addtool.learning.Sequence;
import addtool.semantics.Valuation;
import addtool.semantics.Value;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

/**
 * A visitor for Valuation.
 * Should replace {@link ADD#evaluateSequence(Sequence)} but does not yet due to performance issues.
 * @author Alexander Taepper alexander.taepper@tum.de
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 06.11.2018
 */
public class ADDValuationVisitor implements ADDVisitor {


    private final Set<Vertex> toChange;
    private final Function<Vertex, Value> mapping;
    private final Valuation old;
    private final Valuation val;

    public ADDValuationVisitor(Valuation old, Set<BasicEvent> turnToTrue, Set<BasicEvent> attempted){
        // val contains the valuation for basic events
        this.old = old;
        this.val = old.copy();
        toChange = new HashSet<>(attempted);
        mapping = t -> (t instanceof BasicEvent && turnToTrue.contains(t))?Value.TRUE:Value.FALSE;
    }

    public ADDValuationVisitor(Valuation old) {
        this.old = old;
        this.val = old.copy();
        toChange = new HashSet<>();
        mapping = t -> Value.UNDECIDED;
    }

    public ADDValuationVisitor(Valuation old, Set<Vertex> toChange, Function<Vertex, Value> mapping) {
        this.old = old;
        this.val = old.copy();
        this.toChange = new HashSet<>(toChange);
        this.mapping = mapping;
    }

    public Valuation getVal() {
        return val;
    }

    @Override
    public void visit(BasicEventPlayer basicEvent) {
        if (toChange.contains(basicEvent)) {
            val.addValuation(basicEvent, mapping.apply(basicEvent));
            return;
        }
        val.addValuation(basicEvent, old.getValue(basicEvent));
    }

    @Override
    public void visit(BasicEventTime basicEvent) {
        if (toChange.contains(basicEvent)) {
            val.addValuation(basicEvent, mapping.apply(basicEvent));
            return;
        }
        val.addValuation(basicEvent, old.getValue(basicEvent));
    }

    @Override
    public void visit(And and) {
        if (toChange.contains(and)) {
            val.addValuation(and, mapping.apply(and));
            return;
        }
        val.addValuation(and,
                and.getPredecessors().stream()
                        .map(val::getValue)
                        .reduce(Value.TRUE, Value::and));
    }

    @Override
    public void visit(Or or) {
        if (toChange.contains(or)) {
            val.addValuation(or, mapping.apply(or));
            return;
        }
        val.addValuation(or,
                or.getPredecessors().stream()
                        .map(val::getValue)
                        .reduce(Value.FALSE, Value::or));
    }

    @Override
    public void visit(SAnd sAnd) {
        if (toChange.contains(sAnd)) {
            val.addValuation(sAnd, mapping.apply(sAnd));
            return;
        }
        // Works only for binary SANDs
        Vertex pred01 = sAnd.getPredecessor01();
        Vertex pred02 = sAnd.getPredecessor02();

        // if old valuation decided -> keep value.
        if (old.getValue(sAnd) != Value.UNDECIDED) {
            val.addValuation(sAnd, old.getValue(sAnd));
            return;
        }

        // if any preds are FALSE -> set false
        if (sAnd.getPredecessors().stream().anyMatch(x -> val.getValue(x) == Value.FALSE)) {
            val.addValuation(sAnd, Value.FALSE);
            return;
        }
        Value lastPredVal = Value.TRUE;
        int numberOfNewTrues = 0;
        for(Vertex predecessor : sAnd.getPredecessors()){
            Value predVal = val.getValue(predecessor);
            // check that only from the start an uninterrupted sequence of TRUEs are present.
            if(lastPredVal == Value.UNDECIDED && predVal == Value.TRUE){
                val.addValuation(sAnd, Value.FALSE);
                return;
            }
            if(predVal == Value.TRUE && old.getValue(predecessor) == Value.UNDECIDED){
                numberOfNewTrues += 1;
            }
            lastPredVal = predVal;
        }
        if(numberOfNewTrues > 1){
            val.addValuation(sAnd, Value.FALSE);
            return;
        }
        // Up until the end, only TRUEs -> success!!
        if(lastPredVal == Value.TRUE){
            val.addValuation(sAnd, Value.TRUE);
            return;
        }
        val.addValuation(sAnd, Value.UNDECIDED);

    }


    // SOR only for two predecessors!
    @Override
    public void visit(SOr sOr) {
        if(sOr.getPredecessors().size() > 2){
            System.err.println("SOR only supports 2 predecessors in ADDValuationVisitor!");
        }
        if (toChange.contains(sOr)) {
            val.addValuation(sOr, mapping.apply(sOr));
            return;
        }
        Vertex pred01 = sOr.getPredecessor01();
        Vertex pred02 = sOr.getPredecessor02();

        if (old.getValue(sOr) != Value.UNDECIDED) {
            val.addValuation(sOr, old.getValue(sOr));
        } else {
            // old valuation undecided
            if (val.getValue(pred01) == Value.FALSE && val.getValue(pred02) == Value.UNDECIDED) {
                val.addValuation(sOr, Value.UNDECIDED);
            } else if (val.getValue(pred01) == Value.FALSE && val.getValue(pred02) == Value.FALSE) {
                val.addValuation(sOr, Value.FALSE);
            } else if (val.getValue(pred01) == Value.FALSE && val.getValue(pred02) == Value.TRUE) {
                if (old.getValue(pred01) == val.getValue(pred01)) {
                    val.addValuation(sOr, Value.TRUE);
                } else {
                    val.addValuation(sOr, Value.FALSE);
                }
            } else if (val.getValue(pred01) == Value.UNDECIDED && val.getValue(pred02) == Value.UNDECIDED) {
                val.addValuation(sOr, Value.UNDECIDED);
            } else if (val.getValue(pred01) == Value.UNDECIDED && val.getValue(pred02) == Value.FALSE) {
                val.addValuation(sOr, Value.FALSE);
            } else if (val.getValue(pred01) == Value.UNDECIDED && val.getValue(pred02) == Value.TRUE) {
                val.addValuation(sOr, Value.FALSE);
            } else if (val.getValue(pred01) == Value.TRUE && val.getValue(pred02) == Value.TRUE) {
                val.addValuation(sOr, Value.FALSE);
            } else if (val.getValue(pred01) == Value.TRUE && val.getValue(pred02) == Value.FALSE) {
                val.addValuation(sOr, Value.FALSE);
            } else if (val.getValue(pred01) == Value.TRUE && val.getValue(pred02) == Value.UNDECIDED) {
                val.addValuation(sOr, Value.TRUE);
            }
            else{
                throw new IllegalStateException("Some internal error occured while processing SOR. " + sOr);
            }
        }
    }

    @Override
    public void visit(If ifGate) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void visit(Not not) {
        if (toChange.contains(not)) {
            val.addValuation(not, mapping.apply(not));
            return;
        }
        Vertex pred = not.getPredecessor();

        if (val.getValue(pred) == Value.TRUE) {
            val.addValuation(not, Value.FALSE);
        } else if (val.getValue(pred) == Value.FALSE) {
            val.addValuation(not, Value.TRUE);
        } else {
            val.addValuation(not, Value.UNDECIDED);
        }
    }

    @Override
    public void visit(NotTrue not) {
        if (toChange.contains(not)) {
            val.addValuation(not, mapping.apply(not));
            return;
        }
        Vertex pred = not.getPredecessor();

        if (val.getValue(pred) == Value.TRUE) {
            val.addValuation(not, Value.FALSE);
        } else if (val.getValue(pred) == Value.FALSE) {
            val.addValuation(not, Value.TRUE);
        } else {
            val.addValuation(not, Value.TRUE);
        }
    }

    @Override
    public void visit(Nat nat) {
        if (toChange.contains(nat)) {
            val.addValuation(nat, mapping.apply(nat));
            return;
        }
        Vertex pred = nat.getPredecessor();

        if (val.getValue(pred) == Value.FALSE||val.getValue(pred) == Value.TRUE) {
            val.addValuation(nat, Value.FALSE);
        } else {
            val.addValuation(nat, Value.TRUE);
        }
    }

    @Override
    public void visit(Ivr ivr) {
        if (toChange.contains(ivr)) {
            val.addValuation(ivr, mapping.apply(ivr));
            return;
        }
        Vertex pred = ivr.getPredecessor();

        if (val.getValue(pred) == Value.TRUE) {
            val.addValuation(ivr, Value.UNDECIDED);
        } else if (val.getValue(pred) == Value.FALSE) {
            val.addValuation(ivr, Value.FALSE);
        } else {
            val.addValuation(ivr, Value.TRUE);
        }
    }

    @Override
    public void visit(Trigger trigger) {
        if (toChange.contains(trigger)) {
            val.addValuation(trigger, mapping.apply(trigger));
            return;
        }
        Vertex pred = trigger.getPredecessor();
        val.addValuation(trigger, val.getValue(pred));

    }

    @Override
    public void visit(Reset reset) {
        if (toChange.contains(reset)) {
            val.addValuation(reset, mapping.apply(reset));
            return;
        }
        Vertex pred = reset.getPredecessor();
        val.addValuation(reset, val.getValue(pred));
    }

    @Override
    public void visit(Cost vertex) {
        throw new UnsupportedOperationException("ADDValuationVisitor does not support Cost.");
    }

    @Override
    public void visit(Countermeasure vertex) {
        throw new UnsupportedOperationException("ADDValuationVisitor does not support Countermeasure.");
    }
}

