package addtool.analysisonadd;

import addtool.add.model.*;
import org.jscience.mathematics.number.Rational;

import java.util.HashMap;

/**
 * implements Probability Heuristic from QEST 2019 "Expected Cost Analysis for Attack-Defense Trees".
 *
 * @author Julia Eisentraut, julia.kraemer@in.tum.de
 * @since 5.10.2020
 */
public class ProbabilityHeuristic implements ADDVisitor {

    private final HashMap<Vertex, Rational> probTrue = new HashMap<>();
    private final HashMap<Vertex, Rational> probFalse = new HashMap<>();
    private final HashMap<Vertex, Rational> probUndefined = new HashMap<>();

    /**
     * correct results for non-triggerable, BEs, AND, OR, NOT.
     * <p>
     * too optimistic evaluation for Triggerable BE, SAND and SOR since
     * probability of trigger is not taken into account for probability computation
     * of triggerable basic events and we assume that the players do not play events
     * in the wrong order
     * <p>
     * COST, COUNTERMEASURE not supported
     * <p>
     * just binary trees
     */


    @Override
    public String toString() {
        return "PROBABILITY:" + probTrue;
    }


    @Override
    public void visit(BasicEventPlayer basicEvent) {
        if (basicEvent.isAttacker()) {
            probTrue.put(basicEvent, basicEvent.getSuccessProbability());
            probFalse.put(basicEvent, (Rational.ONE.minus(basicEvent.getSuccessProbability())));
            probUndefined.put(basicEvent, Rational.ONE);
        }
        else{
            visitBE(basicEvent);
        }
    }

    @Override
    public void visit(BasicEventTime basicEvent) {
        visitBE(basicEvent);
    }

    private void visitBE(BasicEvent basicEvent) {
        probTrue.put(basicEvent, Rational.ZERO);
        probFalse.put(basicEvent, Rational.ZERO);
        probUndefined.put(basicEvent, Rational.ZERO);
    }

    @Override
    public void visit(And and) {
        visitAnd(and);
    }

    @Override
    public void visit(SAnd sAnd) {
        visitAnd(sAnd);
    }

    private void visitAnd(NaryOp and) {
        Rational probPred01T = probTrue.get((and).getPredecessor01());
        Rational probPred02T = probTrue.get((and).getPredecessor02());
        Rational probPred01F = probFalse.get((and).getPredecessor01());
        Rational probPred02F = probFalse.get((and).getPredecessor02());
        Rational probPred01U = probUndefined.get((and).getPredecessor01());
        Rational probPred02U = probUndefined.get((and).getPredecessor02());

        probTrue.put(and, probPred01T.times(probPred02T));
        probFalse.put(and, probPred01F.plus((Rational.ONE.minus(probPred01F)).times(probPred02F)));

        Rational comp = probPred01U.times(probPred02U);
        comp = comp.plus(probPred01T.times(probPred02U));
        comp = comp.plus(probPred02T.times(probPred01U));
        comp = (Rational.ONE.isLessThan(comp) ? Rational.ONE : comp);

        probUndefined.put(and, comp);
    }

    @Override
    public void visit(Or or) {
        visitOr(or);
    }

    @Override
    public void visit(SOr sOr) {
        visitOr(sOr);
    }

    private void visitOr(NaryOp naryOp) {
        Rational probPred01T = probTrue.get((naryOp).getPredecessor01());
        Rational probPred02T = probTrue.get((naryOp).getPredecessor02());
        Rational probPred01F = probFalse.get((naryOp).getPredecessor01());
        Rational probPred02F = probFalse.get((naryOp).getPredecessor02());
        Rational probPred01U = probUndefined.get((naryOp).getPredecessor01());
        Rational probPred02U = probUndefined.get((naryOp).getPredecessor02());

        probFalse.put(naryOp, probPred01F.times(probPred02F));
        probTrue.put(naryOp, probPred01T.plus((Rational.ONE.minus(probPred01T)).times(probPred02T)));

        Rational comp = probPred01U.times(probPred02U);
        comp = comp.plus(probPred01F.times(probPred02U));
        comp = comp.plus(probPred02F.times(probPred01U));
        comp = (Rational.ONE.isLessThan(comp) ? Rational.ONE : comp);

        probUndefined.put(naryOp, comp);
    }

    @Override
    public void visit(If ifGate) {
        // TODO
    }

    @Override
    public void visit(Not not) {
        Vertex pred = (not).getPredecessor();
        probTrue.put(not, probFalse.get(pred));
        probFalse.put(not, probTrue.get(pred));
        probUndefined.put(not, probUndefined.get(pred));
    }

    @Override
    public void visit(NotTrue not) {
        Vertex pred = (not).getPredecessor();
        probTrue.put(not, probFalse.get(pred).plus(probUndefined.get(pred)));
        probFalse.put(not, probTrue.get(pred));
        probUndefined.put(not, Rational.ZERO);
    }

    @Override
    public void visit(Nat nat) {
        Vertex pred = (nat).getPredecessor();
        probTrue.put(nat, probFalse.get(pred));
        probFalse.put(nat, probTrue.get(pred));
        probUndefined.put(nat, probUndefined.get(pred));
    }

    @Override
    public void visit(Ivr ivr) {
        // should be IVR at this point
        // ^old comment. As seen in TODOs in this file. That is not the case.
        Vertex pred = (ivr).getPredecessor();
        probTrue.put(ivr, probUndefined.get(pred));
        probFalse.put(ivr, probFalse.get(pred));
        probUndefined.put(ivr, probTrue.get(pred));
    }

    @Override
    public void visit(Trigger trigger) {
        Vertex pred = (trigger).getPredecessor();
        probTrue.put(trigger, probTrue.get(pred));
        probFalse.put(trigger, probFalse.get(pred));
        probUndefined.put(trigger, probUndefined.get(pred));
    }

    @Override
    public void visit(Reset reset) {
        // TODO
    }

    @Override
    public void visit(Cost vertex) {
        // TODO
    }

    @Override
    public void visit(Countermeasure vertex) {
        // TODO
    }
}
