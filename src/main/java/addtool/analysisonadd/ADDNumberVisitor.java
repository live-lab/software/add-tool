package addtool.analysisonadd;

import addtool.add.model.Vertex;

/**
 * Visitor extension to allow additional arguments on visitation
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 27.11.18
 */
public interface ADDNumberVisitor extends ADDVisitor {

    void visit(Vertex vertex, int numberNeg, int numberIvr);
}
