package addtool.analysisonadd;

/**
 * An enum for printing models. PAC means PAC-values are printed, while recurrent means a recursive call to children
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 23.09.2021
 */
public enum PrintType {
    /**
     * Prints only one vertex without PAC-Values
     */
    PLAIN,
    /**
     * Prints only one vertex with PAC-Values
     */
    PAC,
    /**
     * Prints values without pac, but from all child nodes recurrent from the root
     */
    RECURRENT,
    /**
     * Like PAC+RECURRENT
     */
    PAC_RECURRENT
}
