package addtool.analysisonadd;

import addtool.add.model.ADD;
import addtool.add.model.ADDBuilder;
import addtool.add.model.Vertex;
import addtool.helpers.Constants;
import addtool.helpers.ExceptionHandler;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

/**
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * Adapter to handle Analysis methods
 */
public interface PrintableVisitor extends ADDVisitor{
    /**
     * A simple name for the method
     * @return a name for the method, by default the simple class name
     */
    default String getName(){
        return getClass().getSimpleName();
    }/**
     * A simple name for the method
     * @return a name for the method, by default the first 4 letters of the name
     */
    default String getShortName(){
        return getName().toLowerCase().substring(0,4);
    }

    /**
     * A print method for a single vertex
     * @param v the vertex to print
     * @param printType the print type
     * @return a result string
     */
    String printVertex(Vertex v, PrintType printType);

    /**
     * Saves the print Type already before calculation to avoid calculation of unneeded values.
     * @param printType The type wished
     */
    default void setPrintType(PrintType printType){}
    /**
     * Starts printing the model with the root nodes
     * @param model a model to print
     * @param printType the print type
     * @return a result string
     */
    default String print(ADD model, PrintType printType){
        StringBuilder builder=new StringBuilder();
        this.setPrintType(printType);
        model.accept(this);
        model.getNoSuccessor().forEach(v->builder.append(printVertex(v,printType)).append('\n'));
        return builder.toString();
    }

    /**
     * Prints the model with a new instance of the given class.
     * This has to be called, as most visitors take an ADD in the constructor.
     * @param pv the printable visitor from which the new one will be created
     * @param model the model to print
     * @param printType the print type
     * @return the resulting string
     */
    static String print(PrintableVisitor pv,ADD model, PrintType printType)  {
        if(pv!=null) {
            try {
                return pv.getClass().getConstructor(ADD.class).newInstance(model).print(model,printType);
            } catch (InstantiationException|IllegalAccessException|InvocationTargetException|NoSuchMethodException e) {
                ExceptionHandler.logException(e);
            }
        }
        return "ERROR Visitor not Found";
    }
    static PrintableVisitor getVisitorWithModel(PrintableVisitor pv,ADD model)  {
        if(pv!=null) {
            try {
                return pv.getClass().getConstructor(ADD.class).newInstance(model);
            } catch (InstantiationException|IllegalAccessException|InvocationTargetException|NoSuchMethodException e) {
                ExceptionHandler.logException(e);
            }
        }
        return null;
    }
    /**
     * Get selected PrintableVisitor
     * Will only find classes that have an accessible constructor with an ADD as parameter
     * @param name Name of the Type {@link PrintableVisitor#getName()}
     * @param toName a function to map the name to another format.
     * @return an object if there exists a corresponding class, null otherwise
     * @see PrintableVisitor#getMethodByFullName(String)
     * @see PrintableVisitor#getMethodByShortName(String)
     */
    static PrintableVisitor getMethodByName(String name, Function<PrintableVisitor,String> toName){
        Set<Class<? extends PrintableVisitor>> classes = Constants.findAllMatchingTypes("",PrintableVisitor.class);
        ADD test= ADDBuilder.of();
        return classes.stream().map(c-> {
                    try {
                        return c.getConstructor(ADD.class).newInstance(test);
                    } catch (NoSuchMethodException|InvocationTargetException|InstantiationException|IllegalAccessException e) {
                        return null;
                    }
        }).filter(c-> Objects.nonNull(c) && toName.apply(c).equals(name)).findFirst().orElse(null);
    }

    /**
     * Returns an instance of a printable visitor by its full name
     * @param name the name of the method
     * @return a new instance
     */
    static PrintableVisitor getMethodByFullName(String name){
        return getMethodByName(name,PrintableVisitor::getName);
    }
    /**
     * Returns an instance of a printable visitor by its short name
     * @param name the name of the method
     * @return a new instance
     */
    static PrintableVisitor getMethodByShortName(String name){
        return getMethodByName(name,PrintableVisitor::getShortName);
    }

    /**
     * Lists all Classes implementing the Interface in the classpath.
     * Will only find classes that have an accessible constructor with an ADD as parameter
     * @see PrintableVisitor#getName()
     * @return An Array of all Class names
     */
    static String[] getOptions(){
        Set<Class<? extends PrintableVisitor>> classes = Constants.findAllMatchingTypes("",PrintableVisitor.class);
        ADD test= ADDBuilder.of();
        return classes.stream().map(c->{try{
            return c.getConstructor(ADD.class).newInstance(test).getName();
        } catch (InvocationTargetException|InstantiationException|IllegalAccessException|NoSuchMethodException e) {
            return null;
        }}).filter(Objects::nonNull).toArray(String[]::new);
    }
}
