/**
 * Package for analysis on add.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 16.09.15
 */
package addtool.analysisonadd;