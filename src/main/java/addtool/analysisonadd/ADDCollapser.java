package addtool.analysisonadd;

import addtool.add.model.*;
import addtool.semantics.Valuation;
import addtool.semantics.Value;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Collapses parts of an ADD that can be simplified.
 * @author Alexander Taepper alexander.taepper@tum.de
 * @since 20.01.2020
 */

public final class ADDCollapser {

    private ADDCollapser() {
    }

    public static ADD collapseAndOr(ADD original){
        ADD add = ADDBuilder.copyADD(original);
        for(Vertex v : add.getVertices()){
            if(v instanceof And || v instanceof Or){
                for(Vertex pre : v.getPredecessors()){
                    if(pre.getClass() == v.getClass()){
                        v.removePredecessor(pre);
                        ((NaryOp) v).addPredecessors(pre.getPredecessors());
                    }
                }
            }
        }
        return add;
    }

    public static ADD collapseUnaryGates(ADD original){
        ADD add = ADDBuilder.copyADD(original);
        for(Vertex higher : add.getVertices()){
            for(Vertex lower : add.getVertices()){
                // Don't simplify, if higher is same as lower or is directly above lower.
                if (higher.equals(lower) || lower.getSuccessors().contains(higher)) {
                    continue;
                }
                if(dependsOnlyOn(higher, lower)){
                    Valuation valuation = new Valuation(add);
                    valuation.getAllValuations().put(lower, Value.FALSE);
                    Value falseVal = valuation.next(lower, Direction.UP).getValue(higher);
                    valuation.getAllValuations().put(lower, Value.UNDECIDED);
                    Value undecVal = valuation.next(lower, Direction.UP).getValue(higher);
                    valuation.getAllValuations().put(lower, Value.TRUE);
                    Value trueVal = valuation.next(lower, Direction.UP).getValue(higher);


                    if(falseVal == Value.FALSE && undecVal == Value.UNDECIDED && trueVal == Value.TRUE){
                        lower.addSuccessors(higher.getSuccessors());
                        higher.clearSuccessors();
                    }
                    else {
                        UnaryOp gate;
                        if (falseVal == Value.TRUE && undecVal == Value.UNDECIDED && trueVal == Value.FALSE) {
                            gate = new Not("Not", String.valueOf(add.getMaxID() + 1));
                        } else if (falseVal == Value.FALSE && undecVal == Value.TRUE && trueVal == Value.FALSE) {
                            gate = new Nat("Nat", String.valueOf(add.getMaxID() + 1));
                        } else if (falseVal == Value.FALSE && undecVal == Value.TRUE && trueVal == Value.UNDECIDED) {
                            gate = new Ivr("Ivr", String.valueOf(add.getMaxID() + 1));
                        }
                        else if(falseVal == Value.TRUE && undecVal == Value.TRUE && trueVal == Value.FALSE){
                            gate = new NotTrue("NotTrue", String.valueOf(add.getMaxID() + 1));
                        } else {
                            gate = new UnaryLogicOp("Custom ULO", String.valueOf(add.getMaxID() + 1),
                                    trueVal, falseVal, undecVal);
                        }
                        gate.setPredecessor(lower);
                        if(higher.getSuccessors().isEmpty()){
                            add.setTopAtt(gate);
                        }
                        else{
                            gate.addSuccessors(higher.getSuccessors());
                            higher.clearSuccessors();
                        }
                    }
                }
            }
        }
        return add;
    }

    public static boolean dependsOnlyOn(Vertex higher, Vertex lower) {
        // Work set of Vertices the higher nodes value depends on.
        // If this ever reaches only
        List<Vertex> workList = new LinkedList<>();
        workList.add(higher);
        while(!workList.isEmpty()){
            Vertex current = workList.remove(0);
            if(current.equals(lower)){
                // Good, this can be dropped, because higher depends on lower.
                continue;
            }
            if(current instanceof BasicEvent){
                // Higher, depends on a BasicEvent that is not equals to lower
                return false;
            }
            // We look further down the ADD to see which BEs it depends on, or if it is only lower.
            workList.addAll(current.getPredecessors());
        }
        return true;
    }

    public static ADD pruneNoSuccessors(ADD original){
        ADD add = ADDBuilder.copyADD(original);
        // The working set of all nodes that currently have no successors.
        // Remove themselves from their predecessors successor and add their predecessors to the working set
        // if they have no successors left.
        List<Vertex> noSuccessors= add.getVertices().stream()
                .filter(v->v.getSuccessors().isEmpty())
                .filter(v->!(v.equals(add.getTopAtt()) || v.equals(add.getTop())))
                .collect(Collectors.toCollection(LinkedList::new));
        while(!noSuccessors.isEmpty()){
            Vertex cur = noSuccessors.remove(0);

            for(Vertex pre : cur.getPredecessors()){
                pre.getSuccessors().remove(cur);
                // No further check needed, because if they had successors before and have no more now,
                // They can not have been added to the working set and will not be able to be added to
                // the working set after this.
                if(pre.getSuccessors().isEmpty()){
                    noSuccessors.add(pre);
                }
            }
            add.getId2Vertex().remove(cur.getId());
        }
        return add;
    }
}
