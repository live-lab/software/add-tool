package addtool.analysisonadd;

/**
 * Allow trees/dags to be visited from the root to the leaves (DOWN) as well as from the leafs to the root (UP).
 * This enum specifies the direction.
 *
 * @author Alexander Taepper, alexander.taepper@tum.de
 * @since 30.09.2021
 */
public enum Direction {
    UP, DOWN
}
