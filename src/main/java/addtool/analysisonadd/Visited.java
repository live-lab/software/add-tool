package addtool.analysisonadd;

public interface Visited<T> {

    void accept(T visitor);

    void accept(T visitor, Direction direction);
}
