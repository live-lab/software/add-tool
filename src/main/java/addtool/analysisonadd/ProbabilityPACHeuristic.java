package addtool.analysisonadd;

import addtool.add.model.*;
import org.jscience.mathematics.number.Rational;
import addtool.timeseries.PACTuple;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * implements simple Probability Heuristic.
 *
 * @author Julia Eisentraut, julia.kraemer@in.tum.de
 * @since 5.10.2020
 */
public class ProbabilityPACHeuristic implements PrintableVisitor {
    public static final String name="Probability";
    private final HashMap<Vertex, PACTuple> probability = new HashMap<>();
    private final HashMap<Vertex, PACTuple> probabilityFailing = new HashMap<>();
    private boolean calculatePAC = true;

    /**
     * simple Probability Heuristic.
     * @param delayBE maps all basic events to their probability.
     */
    public ProbabilityPACHeuristic(Map<BasicEvent, PACTuple> delayBE) {
        probability.putAll(delayBE);

        for (BasicEvent b : delayBE.keySet()) {
            Rational unc = probability.get(b).getUncertainty();
            Rational delta = probability.get(b).getDelta();
            Rational failing = Rational.ONE.minus(probability.get(b).getValue());
            probabilityFailing.put(b, new PACTuple(failing, unc, delta));
        }
    }
    /**
     * maps all basic events to their probability.
     * @param model a model to start with
     */
    public ProbabilityPACHeuristic(ADD model) {
        this(model.getBasicEvents().stream()
                .map(v->(BasicEvent)v)
                .collect(Collectors.toMap(v->v, BasicEvent::getSuccessProbabilityTuple)));
    }


    @Override
    public void visit(BasicEventPlayer vertex) {
        // do nothing since cost of basic events are already in map
    }

    @Override
    public void visit(BasicEventTime vertex) {
        // do nothing since cost of basic events are already in map
    }

    @Override
    public void visit(And and) {
        visitAnd(and);
    }

    @Override
    public void visit(SAnd and) {
        visitAnd(and);
    }

    private void visitAnd(Vertex and){
        List<Vertex> predecessors = and.getPredecessors();
        List<PACTuple> predecessorValuation = predecessors.stream().map(probability::get).collect(Collectors.toList());

        Rational tmpVal,tmpUnc,delta;
        if(calculatePAC){
            // prob of this vertex is multiplication the success probabilities of all predecessors
            delta = PAC.calcProbabilityDelta(predecessorValuation);

            tmpUnc=predecessorValuation.get(0).getUncertainty();
            tmpVal=predecessorValuation.get(0).getValue();
            for(int i=1;i<predecessorValuation.size();i++){
                tmpUnc=PAC.calcAndUncertainty(new PACTuple(tmpVal,tmpUnc,Rational.ZERO),predecessorValuation.get(i));
                tmpVal = tmpVal.times(predecessorValuation.get(i).getValue());
            }
        }else{
            tmpVal=PAC.multiplyAll(predecessorValuation);
            tmpUnc=Rational.ZERO;
            delta=Rational.ZERO;
        }

        PACTuple pt = new PACTuple(tmpVal, tmpUnc, delta);
        probability.put(and, pt);

        // prob of failing this vertex isvertex the probability of at least one predecessor failing
        PACTuple pt2 = new PACTuple(Rational.ONE.minus(tmpVal), tmpUnc, delta);
        probabilityFailing.put(and, pt2);
    }

    @Override
    public void visit(Or or) {
        visitOr(or);
    }

    @Override
    public void visit(SOr or) {
        visitOr(or);
    }

    private void visitOr(Vertex or){
        List<PACTuple> predecessorValuation = or.getPredecessors().stream().map(probability::get).collect(Collectors.toList());

        Rational tmpVal,tmpUnc,delta;
        if(calculatePAC){
            delta = PAC.calcProbabilityDelta(predecessorValuation);

            tmpUnc=predecessorValuation.get(0).getUncertainty();
            tmpVal=predecessorValuation.get(0).getValue();
            for(int i=1;i<predecessorValuation.size();i++){
                tmpUnc=PAC.calcOrUncertainty(new PACTuple(tmpVal,tmpUnc,Rational.ZERO),predecessorValuation.get(i));
                tmpVal = Rational.ONE.minus(
                        Rational.ONE.minus(tmpVal).times(
                                Rational.ONE.minus(predecessorValuation.get(i).getValue()))
                );
            }
        }else{
            tmpVal=Rational.ONE;
            for(PACTuple pt:predecessorValuation){
                tmpVal=tmpVal.times(Rational.ONE.minus(pt.getValue()));
            }
            tmpVal=Rational.ONE.minus(tmpVal);

            tmpUnc=Rational.ZERO;
            delta=Rational.ZERO;
        }

        PACTuple pt = new PACTuple(tmpVal, tmpUnc, delta);
        probability.put(or, pt);

        // prob of succeeding at this vertex is the probability of at least one predecessor succeeding
        PACTuple pt2 = new PACTuple(Rational.ONE.minus(tmpVal), tmpUnc, delta);
        probabilityFailing.put(or, pt2);
    }

    private void visitInverseUnary(UnaryOp gate){
        Vertex child=gate.getPredecessor();

        PACTuple childTupFailing = probabilityFailing.get(child);
        PACTuple pt = new PACTuple(Rational.ONE.minus(childTupFailing.getValue()),
                childTupFailing.getUncertainty(), childTupFailing.getDelta());
        probabilityFailing.put(gate, pt);

        PACTuple childTupSuccess = probability.get(child);
        // prob of succeeding at this vertex is the probability of at least one predecessor succeeding
        PACTuple pt2 = new PACTuple(Rational.ONE.minus(childTupSuccess.getValue()),
                childTupSuccess.getUncertainty(), childTupSuccess.getDelta());
        probability.put(gate, pt2);
    }
    @Override
    public void visit(If ifGate) {
        throw new UnsupportedOperationException("ProbabilityPACHeuristic not defined for If");
    }

    @Override
    public void visit(Not not) {
        visitInverseUnary(not);
    }

    @Override
    public void visit(NotTrue not) {
        visitInverseUnary(not);
    }

    @Override
    public void visit(Nat nat) {
        visitInverseUnary(nat);
    }

    @Override
    public void visit(Ivr ivr) {
        visitInverseUnary(ivr);
    }

    @Override
    public void visit(Trigger trigger) {
        ADDVisitor.propagateIdentity(trigger, probability, probabilityFailing);
    }

    @Override
    public void visit(Reset reset) {
        throw new UnsupportedOperationException("ProbabilityPACHeuristic not defined for Reset");
    }

    @Override
    public void visit(Cost vertex) {
        ADDVisitor.propagateIdentity(vertex, probability, probabilityFailing);
    }

    @Override
    public void visit(Countermeasure vertex) {
        throw new UnsupportedOperationException("ProbabilityPACHeuristic not defined for Countermeasure");
    }

    private void propagateIdentity(Vertex vertex) {
    }

    /**
     * Returns probability values.
     * @return a map assigning each (visited) vertex its cost.
     */
    public Map<Vertex, PACTuple> getProbability() {
        return new HashMap<>(probability);
    }

    public Map<String, PACTuple> getProbbyId() {
        return probability.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().getId(), Map.Entry::getValue));
    }

    @Override
    public String toString() {
        return "Probability:" + probability;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShortName() {
        return "prob";
    }

    @Override
    public String printVertex(Vertex v, PrintType printType) {
        return printVertex(v,printType,0);
    }

    @Override
    public void setPrintType(PrintType printType) {
        calculatePAC=(printType==PrintType.PAC_RECURRENT|| printType==PrintType.PAC);
    }

    public String printVertex(Vertex v, PrintType printType,int depth) {
        StringBuilder out=new StringBuilder();
        out.append("\t".repeat(depth));
        PACTuple pt=probability.get(v);
        if(pt==null)pt=PACTuple.NIL_TUPLE;
        String os=String.format("%s %s: %s %s %s: %f",
                v.getId(),getResource("operator_short"),
                v.getOperatorName(),getResource("has"),
                getResource(getName()),pt.getValue().doubleValue());

        if(printType==PrintType.PAC||printType==PrintType.PAC_RECURRENT){
            os+=(String.format(", ε: %f, ẟ: %f",pt.getUncertainty().doubleValue(),pt.getDelta().doubleValue()));
        }
        out.append(os);
        out.append("\n");
        if(printType==PrintType.RECURRENT||printType==PrintType.PAC_RECURRENT){
            v.getPredecessors().forEach(k->out.append(printVertex(k,printType,depth+1)));
        }
        return out.toString();
    }
}
