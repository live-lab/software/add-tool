package addtool.xml2add;

import addtool.add.model.ADD;
import addtool.add2dot.ADD2Dot;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * Main class to convert xml to dot.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 03.07.2020
 */
public final class MainXML2Dot {

    private MainXML2Dot() {
    }

    public static void main(String... args) throws IOException {
        String infile= Path.of("test","Casestudy","casestudy_falconx_v3.dot.xml").toString();
        if(args.length>=1){
            infile=args[0];
        }
        String outfile=infile+".dot";
        if(args.length>=2){
            outfile=args[1];
        }
        ADD add = ParserXML.parse(infile);
        add.validateConnections();

        PrintWriter writer = new PrintWriter(outfile, StandardCharsets.UTF_8);
        ADD2Dot.writeADD2DotFile(writer, add);
        // create DocumentBuilder
        /*DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        //create document
        Document document = docBuilder.newDocument();
        add2xml.Write2XML.write2XML(add, document);

        //bring document to shape and stream to file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File(args[0] + ".xml"));
        transformer.transform(source, result);*/
    }
}
