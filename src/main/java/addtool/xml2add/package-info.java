/**
 * This package transforms the xml-format to the ADD format.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 18.08.2020
 */
package addtool.xml2add;