package addtool.xml2add;

import addtool.add.model.BasicEvent;
import addtool.add.model.Vertex;
import addtool.dot2add.RationalFromString;
import org.jscience.mathematics.number.Rational;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.Map;

import static addtool.add2xml.Write2XML.*;
import static addtool.helpers.TranslationTable.COST_EXECUTION;
import static addtool.helpers.TranslationTable.PROBABILITY;

/**
 * Domain for XML export.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 07.10.2020
 */
public class Domain {
    enum Domain_type{
        PROBABILITY,COST,NOT_DETERMINED
    }
    private final String id;
    private Domain_type type;
    /**
     * This variable caches unused properties, as xml import would lose them before refining.
     */
    private final Map<String, Rational> cache;
    public Domain(Node data){
        id=data.getAttributes().getNamedItem("id").getTextContent();
        cache=new HashMap<>();

        type= Domain_type.NOT_DETERMINED;
        //Try to figure out the type
        if(id.equals(DOMAIN_COST)){
            type= Domain_type.COST;
        }else if(id.equals(DOMAIN_PROB)){
            type=Domain_type.PROBABILITY;
        }

        if(type==Domain_type.NOT_DETERMINED){
            NodeList nodes=data.getChildNodes();
            for(int i=0;i<nodes.getLength();i++){
                if(nodes.item(i) instanceof Element) {
                    Element e = (Element) nodes.item(i);
                    if (e.getTagName().equals("class")) {
                        if (e.getTextContent().equals(ADT_Prob)) {
                            type = Domain_type.PROBABILITY;
                            break;
                        } else if (e.getTextContent().equals(ADT_COST)) {
                            type = Domain_type.COST;
                            break;
                        }
                    }
                    if (e.getTagName().equals("description")) {
                        if (e.getTextContent().toLowerCase().contains(PROBABILITY)) {
                            type = Domain_type.PROBABILITY;
                            break;
                        } else if (e.getTextContent().toLowerCase().contains(COST_EXECUTION)) {
                            type = Domain_type.COST;
                            break;
                        }
                    }
                }
            }
        }
        if(type==Domain_type.NOT_DETERMINED){
            System.err.println("Could not determine the type of domain "+id);
        }
    }
    public String getId(){
        return id;
    }
    public String toString(){
        return id+ '|' + type;
    }
    public void handleVertex(Vertex v,String value){
        Rational valueRational=RationalFromString.getRationalFromString(value);
        if(!tryPutValue(v,valueRational)){
            //Save value for later after refining
            cache.put(v.getId(),valueRational);
        }
    }

    /**
     * Tries to get domain value from cache.
     * @param v the vertex to search for by id
     * @return true if a value was found and set, false otherwise
     */
    public boolean valueFromCache(Vertex v){
        if(cache.containsKey(v.getId())){
            tryPutValue(v,cache.get(v.getId()));
        }
        return false;
    }

    /**
     * Tries to put Value to the given vertex.
     * @param v The vertex to set the value to
     * @param value The domain value to set
     * @return true if Vertex is a BasicEvent, false otherwise
     */
    private boolean tryPutValue(Vertex v,Rational value){
        if(v instanceof BasicEvent){
            BasicEvent be=(BasicEvent)v;
            switch (type) {
                case COST -> be.setCost(value);
                case PROBABILITY -> be.setSuccessProbability(value);
            }
            return true;
        }
        return false;
    }
}
