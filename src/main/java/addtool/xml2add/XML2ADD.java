package addtool.xml2add;

import addtool.add.io.ADDImport;
import addtool.add.model.ADD;
import addtool.helpers.TranslationTable;

import java.io.File;

public class XML2ADD implements ADDImport
{
    @Override
    public String getName() {
        return TranslationTable.XML;
    }

    @Override
    public String getFileExtension() {
        return ".xml";
    }

    @Override
    public ADD importModel(File f) {
        ADD model= ParserXML.parse(f.getAbsolutePath());
        if(model!=null)model.validateConnections();
        return model;
    }
}
