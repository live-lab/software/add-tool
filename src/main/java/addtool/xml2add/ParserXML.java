package addtool.xml2add;

import addtool.add.model.*;
import addtool.helpers.ExceptionHandler;
import addtool.helpers.TranslationTable;
import org.jscience.mathematics.number.Rational;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;

public final class ParserXML {

    private static int id = 0;

    private ParserXML() {
    }

    public static ADD parse(String args) {

        // necessary elements to construct add
        HashMap<String, Vertex> id2Vertex = new HashMap<>();
        Map<String,Domain> domains=new HashMap<>();
        try {
            File xmlFile = new File(args);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);

            doc.getDocumentElement().normalize();

            Node root = doc.getDocumentElement();
            NodeList nodes = root.getChildNodes();

            for (int i = 0; i < nodes.getLength(); i++) {
                Node elem = nodes.item(i);
                if (elem.getNodeType() == Node.ELEMENT_NODE && ((Element) elem).getTagName().equals("node")) {
                    root = elem;
                }
                if (elem.getNodeType() == Node.ELEMENT_NODE && ((Element) elem).getTagName().equals("domain")) {
                    Domain d=new Domain(elem);
                    domains.put(d.getId(),d);
                }
            }
            id = 0;
            Vertex vertex = ParserXML.handleNode(root, id2Vertex, true,domains);
            vertex.setSinkAttacker(true);

        } catch (Exception e) {
            ExceptionHandler.logException(e);
        }
        ADD.validateConnections(id2Vertex);
        return reformModel(id2Vertex,domains);
    }

    /**
     * Changes Countermeasures to Not True operators and adds them to a higher AND.
     * @param id2Vertex the vertices of the model
     * @return a model with the stated changes
     */
    private static ADD reformModel(Map<String,Vertex>id2Vertex,Map<String,Domain> domains){
        Map<String,Vertex> work=new HashMap<>(id2Vertex);
        //iterating over id2vertex and modifying work to avoid exception
        id2Vertex.values().stream().filter(v->v.getSuccessors().isEmpty()).forEach(v->iterateCM(v,work, Player.Attacker,domains));
        return ADDBuilder.of(work.values().toArray(new Vertex[]{}));
    }

    /**
     * Iterates all Countermeasures recursively.
     * @param root The Vertex to start from
     * @param work The Map of vertices to change if needed
     * @param aktPlayer The current player on this plane
     * @param domains Data domains to use if a new BE gets introduced
     */
    private static void iterateCM(Vertex root,Map<String, Vertex> work,Player aktPlayer,Map<String,Domain> domains){
        Player nextPlayer;
        if(root instanceof Countermeasure){
            nextPlayer=aktPlayer==Player.Attacker?Player.Defender:Player.Attacker;
        }else{
            nextPlayer=aktPlayer;
        }
        root.getPredecessors().forEach(v->{
            if(!v.getId().equals(root.getId()))
                iterateCM(v,work,nextPlayer,domains);
        });
        if(root instanceof Countermeasure){
            Countermeasure cm=(Countermeasure)root;
            Vertex cmPredecessor=cm.getPredecessor();
            if(cmPredecessor==null)return;
            String id=ADDBuilder.getFreeID(work,new Random());
            Not nt=new Not(TranslationTable.NOT.get(0),id);
            work.put(id,nt);
            nt.addPredecessor(cmPredecessor);
            cmPredecessor.removeSuccessor(cm);

            List<Vertex> cmSuccessors=cm.getSuccessors();
            cmSuccessors.forEach(successor->{
                if(successor instanceof NaryOp){
                    NaryOp nary=(NaryOp) successor;
                    nary.removePredecessor(cm);
                    String id2=ADDBuilder.getFreeID(work,new Random());
                    And and=new And(id2,id2);
                    nary.getSuccessors().forEach(narySuccessor-> narySuccessor.replacePredecessor(nary,and));
                    and.addPredecessors(List.of(nary,nt));
                    work.put(id2,and);

                    //Switch OR without Predecessor to BE
                    if(nary instanceof Or&&nary.getPredecessors().isEmpty()){
                        BasicEvent be=generateBasicEvent(aktPlayer==Player.Attacker,nary.getName(),nary.getId(),Rational.ONE,Rational.ZERO);
                        work.remove(nary.getId(),nary);
                        work.put(be.getId(),be);
                        domains.values().forEach(d->d.valueFromCache(be));
                        nary.getSuccessors().forEach(narySuccessor-> narySuccessor.replacePredecessor(nary,be));
                    }

                }else{
                    System.err.println("Countermeasure under a UnaryOp. This should not happen. Skipping.");
                }
            });
            work.remove(cm.getId());
        }
    }

    private static Vertex handleNode(Node root, HashMap<String, Vertex> id2vertex, boolean attacker,Map<String,Domain>domains) {
        Vertex vertex = null;
        NodeList predecessors = root.getChildNodes();
        String label = ParserXML.getLabel(predecessors);

        // check whether its an element node
        if (root.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) root;
            NamedNodeMap attributes = element.getAttributes();

            boolean addCountermeasure = false;

            if (element.hasAttribute("switchRole")) {
                Node switchRole = attributes.getNamedItem("switchRole");
                if (switchRole.getTextContent().equals("yes")) {
                    attacker = !attacker;
                    addCountermeasure = true;
                }
            }
            //DEBUG!
            //addCountermeasure=true;

            // find out whether node has an refinement
            if (element.hasAttribute("refinement")) {
                Node refinement = attributes.getNamedItem("refinement");
                String text = refinement.getNodeValue();
                String idString=String.valueOf(id);
                // treat node differently depending on value of refinement
                if (ParserXML.isBasicEvent(root)) {
                    vertex = new BasicEventPlayer(label, Rational.ONE, Rational.ZERO, (attacker ? Player.Attacker : Player.Defender), idString);
                } else if (text.equals("conjunctive")) {
                    vertex = new And(label, idString);
                } else if (text.equals("disjunctive")) {
                    vertex = new Or(label, idString);
                } else if (text.equals("sequential")) {
                    vertex = new SAnd(label, idString);
                } else {
                    throw new IllegalArgumentException("Unknown refinement type!");
                }
            }
            // increase id whenever vertex is added
            id++;
            if(vertex!=null){
                id2vertex.put(vertex.getId(), vertex);
            }

            // deal with predecessors and add vertex to all lists
            int minus = 0;
            for (int i = 0; i < predecessors.getLength(); i++) {
                Node predecessorNode = predecessors.item(i);
                if (predecessorNode.getNodeType() == Node.ELEMENT_NODE &&
                        ((Element) predecessorNode).getTagName().equals("node")) {
                    Vertex predecessor = ParserXML.handleNode(predecessorNode, id2vertex, attacker,domains);
                    vertex.addPredecessor(predecessor);

                } else {
                    minus++;
                }
                if (predecessorNode.getNodeType() == Node.ELEMENT_NODE &&
                        ((Element) predecessorNode).getTagName().equals("parameter")) {
                    String did=((Element) predecessorNode).getAttribute("domainId");
                    Domain domain=domains.get(did);
                    if(domain!=null){
                        domain.handleVertex(vertex,predecessorNode.getTextContent());
                    }
                }
            }
            // check in the end whether we need to add a countermeasure vertex
            if (addCountermeasure) {
                // we split trees at countermeasures
                UnaryOp countermeasure = new Countermeasure(label, String.valueOf(id));
                id2vertex.put(countermeasure.getId(), countermeasure);
                countermeasure.setPredecessor(vertex);
                id++;

                // change vertex since we need to return the countermeasure vertex
                vertex = countermeasure;
            }
        }

        return vertex;
    }


    private static BasicEvent generateBasicEvent(boolean attacker, String name, String id, Rational
            probability, Rational cost) {
        if (attacker) {
            return new BasicEventPlayer(name, probability, cost, Player.Attacker, id);
        } else {
            return new BasicEventPlayer(name, probability, cost, Player.Defender, id);
        }
    }

    // basic events can only be recognized by checking whether other events follow
    private static boolean isBasicEvent(Node node) {
        NodeList successors = node.getChildNodes();
        for (int i = 0; i < successors.getLength(); i++) {
            Node n = successors.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) n;
                if (elem.getTagName().equals("node")) {
                    return false;
                }
            }
        }
        return true;
    }

    private static String getLabel(NodeList predecessors) {
        String label = String.valueOf(id);
        for (int i = 0; i < predecessors.getLength(); i++) {
            Node name = predecessors.item(i);
            if (name.getNodeType() == Node.ELEMENT_NODE) {
                //first successor is label of inner node
                Element elem = (Element) name;
                if (elem.getTagName().equals("label")) {
                    label = elem.getTextContent();
                }
            }
        }
        return label;
    }

}
