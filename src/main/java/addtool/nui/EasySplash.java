package addtool.nui;

import addtool.helpers.Constants;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Objects;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * A class to build a simple splash screen a utility to update progress during execution
 * @author Florian Dorfhuber
 */
public class EasySplash {
    private JFrame F;
    private JProgressBar prog=null;

    /**
     * The default constructor to prepare a splash screen
     * Will be displayed automatically
     * @param favicon the favicon in the corner. May be null
     * @param img The main  image displayed in the splash screen
     * @param c the background color of the progressbar
     * @param maxval the maximum value of the progressbar
     * @param orientation the orientation of the progressbar
     * @see JProgressBar#HORIZONTAL
     * @see JProgressBar#VERTICAL
     */
    public EasySplash(ImageIcon favicon, ImageIcon img,Color c,int maxval,int orientation){
        F=new JFrame();
        F.setUndecorated(true);
        F.setLayout(new BorderLayout());
        F.add(new JLabel(img),BorderLayout.CENTER);
        if(maxval>0){
            prog=new JProgressBar(orientation);
            prog.setMaximum(maxval);
            if(c!=null)prog.setBackground(c);
            F.add(prog,BorderLayout.PAGE_END);
        }
        F.pack();
        F.setVisible(true);
        F.setIconImage(favicon.getImage());
        Dimension size=Toolkit.getDefaultToolkit().getScreenSize();
        //F.setSize(new Dimension(800,600));
        F.setLocation((size.width-F.getSize().width)/2, (size.height-F.getSize().height)/2);
    }

    /**
     * The default constructor to prepare a splash screen
     * Will be displayed automatically
     * @param img The main  image displayed in the splash screen, also used as favicon
     * @param c the background color of the progressbar
     * @param maxval the maximum value of the progressbar
     * @param orientation the orientation of the progressbar
     * @see JProgressBar#HORIZONTAL
     * @see JProgressBar#VERTICAL
     */
    public EasySplash(ImageIcon img,Color c,int maxval,int orientation){
        this(img,img,c,maxval,orientation);
    }

    /**
     * The empty constructor for the splashscreen
     * Will be displayed automatically
     * Needs the constants class and language support and values for the keys "icon_logo", "icon_splash"
     * @see Constants#getResourceFolder()
     * @see addtool.helpers.preferences.LanguageSupport#getResource(String)
     * */
    public EasySplash(){
        this(new ImageIcon(
                Objects.requireNonNull(
                        EasySplash.class.getResource(Constants.getResourceFolder() +getResource("icon_logo"))
                )),
                new ImageIcon(
                        Objects.requireNonNull(
                                EasySplash.class.getResource(Constants.getResourceFolder() +getResource("icon_splash"))
                        )),
                null,100,JProgressBar.HORIZONTAL);
    }

    /**
     * Updates the progress in the Progressbar to the given Value
     * @param progress the current progress between 0-100 or 0 - maxval
     */
    public void setProgress(int progress){
        if(prog!=null){
            prog.setValue(progress);
        }
    }

    /**
     * Passes the method on to the JFrame
     * @param cop the default close operation
     * @see JFrame#setDefaultCloseOperation(int)
     */
    public void setCloseOperation(int cop){
        F.setDefaultCloseOperation(cop);
    }

    /**
     * @see JFrame#dispose()
     */
    public void dispose(){
        F.dispose();
    }

    /**
     * Returns the splashscreen Frame
     * @return The Frame displaying the splash screen
     */
    public JFrame getFrame(){
        return F;
    }

    /**
     * Adds additional text to the progressbar
     * This can be used to explain what is currently loading
     * @param text the text to be displayed
     */
    public void setText(String text) {
        prog.setStringPainted(true);
        prog.setString(text);
    }

    /**
     * Get the Progressbar used in the splashscreen
     * @return The progressbar
     */
    public JProgressBar getProgressBar() {
        prog.setStringPainted(true);
        return prog;
    }
}
