/**
 * A package for different Styles to render Graphs on the Editor.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
package addtool.nui.style;