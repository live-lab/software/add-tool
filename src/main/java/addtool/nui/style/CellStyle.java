package addtool.nui.style;

import addtool.add.model.Vertex;
import addtool.dfd.Entity;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxImageBundle;
import addtool.nui.mxWrapper.nGraph;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Objects;

/**
 * A template for custom cell styles to render vertices.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public abstract class CellStyle {
    /**
     * Method that is called on UI Update.
     * Here the rendering should be done.
     * @param g A graph that is updated
     * @param font The current UI Font
     * @param cell The cell in the graph that is rendered
     * @param v The data object that is represented by the cell
     */
    public abstract void renderCell(nGraph g, Font font, mxCell cell, Vertex v);
    /**
     * Method that is called on UI Update.
     * Here the rendering should be done.
     * @param g A graph that is updated
     * @param font The current UI Font
     * @param cell The cell in the graph that is rendered
     * @param e The data object that is represented by the cell
     */
    public abstract void renderCell(nGraph g, Font font, mxCell cell, Entity e);

    /**
     * Get the mxShape for a vertex
     * @param v The data object
     * @return A string containing the shape to be used in rendering
     */
    public abstract String getStyleFor(Vertex v);

    /**
     * A method to export a bundle of images for use in graph rendering
     * @return the imageBundle containing the icons used by the style
     */
    public abstract mxImageBundle getImageBundle();

    /**
     * A helper method to get a base64 String of data
     * Usually used in icons
     * @param in A stream for input data usually from resources
     * @return Base64 encoded data from the file
     * @throws IOException if reading the stream fails
     * @see CellStyle#getImageBundle()
     */
    public static String encodeFileToBase64Binary(InputStream in) throws IOException{
        synchronized (in){
            byte[] bytes = new byte[(int)in.available()];
            in.read(bytes);
            in.close();
            return "data:image/png;base64,"+new String(Base64.getEncoder().encode(bytes), StandardCharsets.UTF_8); //NON-NLS
        }
    }

    /**
     * A helper method to get a base64 String of data
     * Usually used in icons
     * @param s A path to the resource
     * @return Base64 encoded data from the file
     * @throws IOException if reading the stream fails
     * @see CellStyle#getImageBundle()
     */
    public static String encodeFileToBase64Binary(String s) throws IOException {
        return encodeFileToBase64Binary(Objects.requireNonNull(CellStyle.class.getResourceAsStream(s)));
    }
}
