package addtool.nui.style;

import addtool.add.model.BasicEvent;
import addtool.add.model.BasicEventPlayer;
import addtool.add.model.Vertex;
import addtool.dfd.Entity;
import addtool.helpers.preferences.BooleanPreference;
import addtool.helpers.preferences.ColorPreference;
import addtool.helpers.preferences.PreferenceManager;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxImageBundle;
import addtool.helpers.Constants;
import addtool.helpers.ExceptionHandler;
import addtool.nui.mxWrapper.nGraph;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * A simple style class for vertex rendering.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public class BasicStyle extends CellStyle{
    /**
     * The height of the operator icon.
     * Sadly this is currently hardcoded, as the mxLibrary does not return it that easy
     */
    private static final int iconHeight=20;
    /**
     * Preference Key if the ID should be displayed in a vertex
     */
    public static final String SHOW_ID="showID";
    /**
     * Preference Key if the operator name should be displayed in the vertex.
     * Is bypassed if the vertex would be empty otherwise
     */
    public static final String SHOW_OPERATOR="showOperator";
    /**
     * Preference Key if the operator icons should be used.
     */
    public static final String SHOW_ICON="showICON";
    /**
     * Preference Key for the background color of attacker vertices
     */
    public static final String COLOR_ATTACK="colorAttacker";
    /**
     * Preference Key for the background color of defender vertices
     */
    public static final String COLOR_DEFEND="colorDefender";
    /**
     * Preference Key for the background color of other vertices. Not Attacker/Defender
     */
    public static final String COLOR_VERTEX="colorVertex";
    /**
     * Preference Group Key for style related preferences
     */
    public static final String VERTEX_LAYOUT="layout/vertex";

    static {
        //Define layout preferences
        BooleanPreference showID=new BooleanPreference(SHOW_ID,SHOW_ID,VERTEX_LAYOUT,true);
        BooleanPreference showOP=new BooleanPreference(SHOW_OPERATOR,SHOW_OPERATOR,VERTEX_LAYOUT,true);
        BooleanPreference showICON=new BooleanPreference(SHOW_ICON,SHOW_ICON,VERTEX_LAYOUT,true);
        ColorPreference attacker=new ColorPreference(COLOR_ATTACK,COLOR_ATTACK,VERTEX_LAYOUT,"#AD372D");
        ColorPreference defender=new ColorPreference(COLOR_DEFEND,COLOR_DEFEND,VERTEX_LAYOUT,"#29CF71");
        ColorPreference vertex=new ColorPreference(COLOR_VERTEX,COLOR_VERTEX,VERTEX_LAYOUT,"#8585ad");

        PreferenceManager.getPreferenceManager().putPreferences(showID,showOP,showICON,attacker,defender,vertex);

    }

    /**
     * A (currently hardcoded) list of operators that have icons.
     */
    final List<String> operators=List.of("and","or","not","nottrue","sor","sand");
    @Override
    public void renderCell(nGraph g, Font font,mxCell cell, Vertex v) {
        String firstLine="";
        if((Boolean)PreferenceManager.getPreferenceManager().getPreference(SHOW_ID).getValue()){
            firstLine+=v.getId()+": ";
        }
        if((Boolean)PreferenceManager.getPreferenceManager().getPreference(SHOW_OPERATOR).getValue()){
            if(v instanceof BasicEventPlayer){
                //noinspection StringConcatenationMissingWhitespace
                firstLine+=("BE"+((BasicEventPlayer)v).getPlayer());
            }else{
                firstLine+=(v.getOperatorName());
            }
        }
        List<String> lines = new ArrayList<>();
        if(!firstLine.isEmpty()){
            lines.add(firstLine);
        }

        if(v instanceof BasicEvent||
                (!v.getOperatorName().equalsIgnoreCase(v.getName())&&!v.getId().equalsIgnoreCase(v.getName()))){
            lines.add(v.getName());
        }
        //Backup if label would be empty instead
        if(lines.isEmpty()){
            lines.add(v.getOperatorName());
        }
        lines= nGraph.preCut(lines);
        cell.setValue(String.join("\n", lines));

        Object[] toChange= {cell};
        if(v instanceof BasicEventPlayer){
            switch (((BasicEventPlayer) v).getPlayer()) {
                case Attacker -> g.setCellStyles(mxConstants.STYLE_FILLCOLOR,
                        PreferenceManager.getPreferenceManager().getPreference(COLOR_ATTACK).getValueString(),
                        toChange);
                case Defender -> g.setCellStyles(mxConstants.STYLE_FILLCOLOR,
                        PreferenceManager.getPreferenceManager().getPreference(COLOR_DEFEND).getValueString(),
                        toChange);
            }
        }else{
            //mxUtils.hexString(Color.lightGray)
            g.setCellStyles(mxConstants.STYLE_FILLCOLOR,
                    PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                    toChange);
        }
        int addHeight=0;
        if(!(v instanceof BasicEvent)&&operators.contains(v.getOperatorName().toLowerCase())&&
                (Boolean)PreferenceManager.getPreferenceManager().getPreference(SHOW_ICON).getValue()) {
            String op = v.getOperatorName().toLowerCase();
            g.setCellStyles(mxConstants.STYLE_IMAGE, op, toChange);
            g.setCellStyles(mxConstants.STYLE_IMAGE_ALIGN, mxConstants.ALIGN_CENTER, toChange);
            g.setCellStyles(mxConstants.STYLE_IMAGE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP, toChange);
            g.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_BOTTOM, toChange);
            addHeight=iconHeight;
        }else{
            g.setCellStyles(mxConstants.STYLE_IMAGE, null, toChange);
            g.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE, toChange);
        }
        g.changeShape(cell,getStyleFor(v));
        g.setCellStyles(mxConstants.STYLE_SHAPE,getStyleFor(v),toChange);
        //g.setCellStyles(mxConstants.STYLE_SHAPE,mxConstants.SHAPE_LABEL,toChange);

        g.setCellStyles(mxConstants.STYLE_FONTSIZE, String.valueOf(font.getSize()),toChange);

        g.setCellStyles(mxConstants.STYLE_FONTSIZE, String.valueOf(font.getSize()),toChange);
        int addWidth=0;
        g.updateLabel(cell,g.getView().getState(cell),lines,addHeight,addWidth);
    }

    @Override
    public void renderCell(nGraph g, Font font, mxCell cell, Entity e) {
        Object[] toChange= {cell};
        g.setCellStyles(mxConstants.STYLE_STROKECOLOR, e.getColor(),toChange);
        g.setCellStyles(mxConstants.STYLE_FONTSIZE, String.valueOf(font.getSize()),toChange);
        g.changeShape(cell,e.getShape());
        cell.setValue(e.getName());
    }

    @Override
    public String getStyleFor(Vertex v) {
        if(v instanceof BasicEvent){
            return mxConstants.SHAPE_ELLIPSE;
        }else{
            return mxConstants.SHAPE_LABEL;
        }
    }

    @Override
    public mxImageBundle getImageBundle() {
        mxImageBundle bundle=new mxImageBundle();
        operators.forEach(s->{
            //Can be suppressed as it only means having an icon less.
            //noinspection LogException
            try {
                bundle.putImage(s,
                        encodeFileToBase64Binary(
                                Constants.getResourceFolder()+"operators/"+s+".png")); //NON-NLS
            } catch (Exception e) {
                ExceptionHandler.logException(e);
            }
        });
        return bundle;
    }

}
