package addtool.nui.style;

import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.shape.mxRectangleShape;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxCellState;

import java.awt.*;

/**
 * An extension for the rectangular shape of mxGraphs
 * Used for the DFD Entity Store
 */
public class nStoreShape extends mxRectangleShape {
    /**
     * Shape string for this shaped used in generating a new cells
     */
    public static final String SHAPE_STORE="store_shape";
    @Override
    public void paintShape(mxGraphics2DCanvas canvas, mxCellState state)
    {
        Rectangle rect = state.getRectangle();
        int x = rect.x;
        int y = rect.y;
        int w = rect.width;
        int h = rect.height;
        if (configureGraphics(canvas, state, true))
        {
            canvas.fillShape(rect, hasShadow(canvas, state));
        }
        Graphics2D graphics2D=canvas.getGraphics();
        Stroke s=graphics2D.getStroke();
        graphics2D.setStroke(new BasicStroke(3));
        graphics2D.setColor(Color.decode((String)state.getStyle().get(mxConstants.STYLE_STROKECOLOR)));
        graphics2D.drawLine(x,y,x+w,y);
        graphics2D.drawLine(x,y+h,x+w,y+h);
        graphics2D.setStroke(s);
    }
}
