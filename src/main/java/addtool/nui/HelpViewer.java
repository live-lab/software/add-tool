package addtool.nui;


import addtool.helpers.Constants;
import addtool.helpers.ExceptionHandler;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static addtool.helpers.Constants.isWindows;
import static addtool.helpers.Update.basedir;
import static addtool.helpers.preferences.LanguageSupport.getLocaleFile;
import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Helper class to show help webpage
 */
public final class HelpViewer {
    private static final String HELP_FILE_PREFIX=new File(basedir,"help/doc").getAbsolutePath();
    private static final String HELP_FILE_EXTENSION=".html";
    private static final File HELP_FILE=new File(HELP_FILE_PREFIX+HELP_FILE_EXTENSION);
    static {
        if(!HELP_FILE.exists()){
            //noinspection LogException
            try {
                Constants.unpackFromJar("help");
            } catch (URISyntaxException|IOException e) {
                ExceptionHandler.logException(e);
            }
        }
    }

    private static WebView webView = null;

    private HelpViewer() {
    }

    /**
     * Opens the help window
     * Displays the webpage using javaFx.
     * If that fails it calls the standard browser in the OS
     */
    public static void showHelp() {
        if(!Constants.isWindows()){
            fallBack(null);
            return;
        }
        JFrame frame = new JFrame(getResource("help"));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize(Math.max(screenSize.width/2,Math.min(800,screenSize.width)), Math.max(screenSize.height*2/3,Math.min(900,screenSize.height)));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        JPanel panel = new JPanel();
        LayoutManager layout = new FlowLayout();
        panel.setLayout(layout);

        try{
            //this caused a nasty error in java fx. This can not be caught.
            com.sun.javafx.tk.Toolkit.getToolkit();
        }catch (RuntimeException ex){
            fallBack(frame);
        }

        JFXPanel jfxPanel = new JFXPanel();
        Platform.runLater(() -> {
            try{
                webView = new WebView();
                webView.setPrefSize(frame.getContentPane().getSize().width,frame.getContentPane().getSize().height);

                jfxPanel.setLocation(0,0);
                jfxPanel.setScene(new Scene(webView));
                webView.getEngine().load("file://"+getLocaleFile(HELP_FILE_PREFIX,HELP_FILE_EXTENSION));
            }catch (RuntimeException ex){
                fallBack(frame);
            }
        });

        panel.add(jfxPanel);
        Platform.setImplicitExit(false);
        frame.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                Platform.runLater(() -> {
                    try{
                        jfxPanel.setSize(frame.getContentPane().getSize());
                        jfxPanel.setLocation(0,0);
                        webView.setPrefSize(frame.getContentPane().getSize().width,frame.getContentPane().getSize().height);
                    }catch (RuntimeException ex){
                        fallBack(frame);
                    }
                });

            }

            @Override
            public void componentMoved(ComponentEvent e) {}

            @Override
            public void componentShown(ComponentEvent e) {}

            @Override
            public void componentHidden(ComponentEvent e) {}
        });



        frame.getContentPane().add(panel, BorderLayout.CENTER);
    }
    private static void fallBack(JFrame frame){
        if(frame!=null){
            frame.dispose();
        }
        try {
            Runtime.getRuntime().exec((isWindows()?"":"firefox ")+getLocaleFile(HELP_FILE_PREFIX,HELP_FILE_EXTENSION));
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,
                    String.format(getResource("help_load_failed"),getLocaleFile(HELP_FILE_PREFIX,HELP_FILE_EXTENSION)),
                    getResource("help_load_failed_title"),
                    JOptionPane.ERROR_MESSAGE);
        }
    }

}
