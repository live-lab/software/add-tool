package addtool.nui;

import addtool.dfd.Actor;
import addtool.dfd.DataFlow;
import addtool.dfd.Entity;
import addtool.dfd.Process;
import addtool.dfd.Store;
import addtool.helpers.Threat;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.nui.VertexEditPane.addStandardPanel;

/**
 * WIP Class to edit Endititys for DFD
 * @author Florian Dorfhuber
 * @see Entity
 * @see VertexEditPane
 */
public final class EntityEditPane {
    private EntityEditPane() {
    }

    /**
     * Opens an edit dialog for the passed entity. On Closing the data is saved and returned as new entity
     * @param entity the entity to edit
     * @param parent A parent window to snap in
     * @return the entity with the changed values or the passed entity if the user aborted.
     */
    public static Entity editEntity(Entity entity, Window parent){
        //0.2=x of panel is entity stuff 1-x is Threats
        JDialog dia=new JDialog(parent);
        dia.setTitle(getResource("entity_edit"));
        dia.setModal(true);
        if(parent!=null){
            dia.setIconImages(parent.getIconImages());
        }
        JPanel contentPane=new JPanel();
        contentPane.setLayout(new GridBagLayout());
        GridBagConstraints panelConstraints=new GridBagConstraints();
        dia.add(contentPane);

        JPanel entityPanel=new JPanel();
        entityPanel.setLayout(new GridLayout(0,2));
        panelConstraints.weightx=0.1;
        contentPane.add(entityPanel,panelConstraints);

        JPanel threatPanel=new JPanel();
        threatPanel.setLayout(new GridLayout(0,1));
        JScrollPane threatScrollPane=new JScrollPane(threatPanel);
        panelConstraints.gridwidth=4;
        panelConstraints.weightx=0.9;
        contentPane.add(threatScrollPane,panelConstraints);

        //Entity panel
        entityPanel.add(new JLabel(getResource("name")));
        JTextField nameField=new JTextField(entity.getName());
        entityPanel.add(nameField);
        JCheckBox outOfScopeBox=new JCheckBox(getResource("is_out_of_scope"));
        outOfScopeBox.setSelected(entity.isOutOfScope());
        entityPanel.add(outOfScopeBox);
        entityPanel.add(new JLabel());
        entityPanel.add(new JLabel(getResource("reason_out_of_scope")));
        JTextArea reasonOutOfScopeArea=new JTextArea(entity.getReasonOutOfScope());
        entityPanel.add(reasonOutOfScopeArea);

        JCheckBox authenticationBox=new JCheckBox(getResource("has_auth"));
        JCheckBox logBox=new JCheckBox(getResource("is_log"));
        JCheckBox credentialsBox=new JCheckBox(getResource("stores_credentials"));
        JCheckBox encryptedBox=new JCheckBox(getResource("is_encrypted"));
        JCheckBox signedBox=new JCheckBox(getResource("is_signed"));
        JCheckBox publicBox=new JCheckBox(getResource("is_public_network"));
        JTextField privilegeField=new JTextField();
        JTextField protocolField=new JTextField();
        if(entity instanceof Actor){
            authenticationBox.setSelected(((Actor) entity).isProvidesAuthentication());
            entityPanel.add(authenticationBox);
        }
        if(entity instanceof Process){
            privilegeField.setText(((Process) entity).getPrivilegeLevel());
            entityPanel.add(new JLabel(getResource("privilege_level")));
            entityPanel.add(privilegeField);
        }
        if(entity instanceof Store){
            logBox.setSelected(((Store) entity).isLog());
            entityPanel.add(logBox);
            credentialsBox.setSelected(((Store) entity).isStoresCredentials());
            entityPanel.add(credentialsBox);
            encryptedBox.setSelected(((Store) entity).isEncrypted());
            entityPanel.add(encryptedBox);
            signedBox.setSelected(((Store) entity).isSigned());
            entityPanel.add(signedBox);
        }
        if(entity instanceof DataFlow){
            protocolField.setText(((DataFlow) entity).getProtocol());
            entityPanel.add(new JLabel(getResource("protocol")));
            entityPanel.add(protocolField);
            encryptedBox.setSelected(((DataFlow) entity).isEncrypted());
            entityPanel.add(encryptedBox);
            publicBox.setSelected(((DataFlow) entity).isPublicNetwork());
            entityPanel.add(publicBox);
        }


        //threat panel

        for(Threat t:entity.getThreats()){
            threatPanel.add(new ThreatItem(t));
        }

        JButton addThreatButton=new JButton("+");
        addThreatButton.addActionListener(e->{
            Threat t=new Threat();
            ThreatItem ti=new ThreatItem(t);
            threatPanel.add(ti,threatPanel.getComponentCount()-1);
            threatPanel.updateUI();
            contentPane.updateUI();
        });
        threatPanel.add(addThreatButton);



        AtomicBoolean save=new AtomicBoolean(false);
        JButton saveButton=(JButton)UIHelper.buildToolButton("save","save",e->{
            save.set(true);
            dia.dispose();
        },false);
        JRootPane rootPane = SwingUtilities.getRootPane(dia);
        rootPane.setDefaultButton(saveButton);

        AbstractButton discard=UIHelper.buildToolButton("cancel","cancel",e->{
            save.set(false);
            dia.dispose();
        },false);
        addStandardPanel(contentPane,discard,saveButton,panelConstraints,panelConstraints);

        contentPane.updateUI();
        dia.setMinimumSize(new Dimension(400,300));
        dia.pack();
        dia.setVisible(true);
        //read values from dialog

        if(save.get()){
            //Write to entity
            entity.setName(nameField.getText());
            entity.setOutOfScope(outOfScopeBox.isSelected());
            entity.setReasonOutOfScope(reasonOutOfScopeArea.getText());

            if(entity instanceof Actor){
                ((Actor) entity).setProvidesAuthentication(authenticationBox.isSelected());
            }
            if(entity instanceof Process){
                ((Process) entity).setPrivilegeLevel(privilegeField.getText());
            }
            if(entity instanceof Store){
                ((Store) entity).setLog(logBox.isSelected());
                ((Store) entity).setStoresCredentials(credentialsBox.isSelected());
                ((Store) entity).setEncrypted(encryptedBox.isSelected());
                ((Store) entity).setSigned(signedBox.isSelected());
            }
            if(entity instanceof DataFlow){
                ((DataFlow) entity).setEncrypted(encryptedBox.isSelected());
                ((DataFlow) entity).setPublicNetwork(publicBox.isSelected());
                ((DataFlow) entity).setProtocol(protocolField.getText());
            }

            List<Threat> threats=new ArrayList<>();
            for(Component c:threatPanel.getComponents()){
                if(c instanceof ThreatItem){
                    threats.add(((ThreatItem) c).getThreat());
                }
            }
            entity.replaceThreats(threats);
        }
        return entity;
    }
    static class ThreatItem extends JPanel{
        Threat threat;
        final JLabel nameLabel;
        final JButton STRIDELabel;
        final JButton openButton;
        final JButton severityButton;
        final JButton editButton;
        final JButton removeButton;
        public ThreatItem(Threat t){
            threat=t;
            setLayout(new GridBagLayout());
            GridBagConstraints nameConstraints=new GridBagConstraints();
            nameConstraints.fill=GridBagConstraints.HORIZONTAL;
            nameConstraints.weightx=0.9;
            GridBagConstraints btConstraints=new GridBagConstraints();
            btConstraints.weightx=0.1;

            nameLabel=new JLabel();
            this.add(nameLabel,nameConstraints);

            STRIDELabel=new JButton();
            STRIDELabel.addActionListener(e->{
                threat.setStride(threat.getStride().next());
                SwingUtilities.invokeLater(this::updateUI);
            });
            this.add(STRIDELabel,btConstraints);

            //TODO add icons
            openButton=new JButton();
            openButton.addActionListener(e->{
                threat.setOpen(!threat.isOpen());
                SwingUtilities.invokeLater(this::updateUI);
            });
            this.add(openButton,btConstraints);

            //TODO add icons
            severityButton=new JButton("");
            severityButton.addActionListener(e->{
                    threat.setSeverity(threat.getSeverity().next());
                    SwingUtilities.invokeLater(this::updateUI);
            });
            this.add(severityButton,btConstraints);

            editButton=new JButton();
            UIHelper.updateIcon(getResource("button_edit"),editButton);
            editButton.addActionListener(e -> {
                threat=ThreatEditPane.editThreat(threat,SwingUtilities.getWindowAncestor(this));
                //TODO update preview
                SwingUtilities.invokeLater(this::updateUI);
            });
            this.add(editButton,btConstraints);

            removeButton=new JButton();
            UIHelper.updateIcon(getResource("button_delete"),removeButton);
            removeButton.addActionListener(e->{
                JComponent parent=(JComponent)getParent();
                parent.remove(ThreatItem.this);
                parent.updateUI();
            });
            this.add(removeButton,btConstraints);
            updateUI();
        }
        @Override
        public void updateUI(){
            if(nameLabel!=null){
                nameLabel.setText(threat.getTitle());
            }
            if(STRIDELabel!=null){
                STRIDELabel.setText(threat.getStride().toString().substring(0,1));
                STRIDELabel.setToolTipText(threat.getStride().toString());
            }
            if(openButton!=null){
                UIHelper.updateIcon(threat.isOpen()?
                        getResource("button_warning"):
                        getResource("button_ok"),openButton);
                //openButton.setText(threat.isOpen()?"X":"Y");
                openButton.setToolTipText(threat.isOpen()?getResource("open"):
                        getResource("mitigated"));
            }

            if(severityButton!=null){
                severityButton.setBackground(threat.getSeverity().getColor());
                severityButton.setToolTipText(threat.getSeverity().toString());
            }

            super.updateUI();
        }
        public Threat getThreat(){
            return threat;
        }
    }
}
