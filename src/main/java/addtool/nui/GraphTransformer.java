package addtool.nui;

import addtool.add.model.*;
import addtool.add2dot.ADD2Dot;
import addtool.analysisonadd.CostHeuristic;
import addtool.analysisonadd.DelayHeuristic;
import addtool.feedback.FeedbackTuple;
import addtool.nui.mxWrapper.nADDGraph;
import addtool.nui.mxWrapper.nGraph;
import addtool.nui.mxWrapper.nGraphComponent;
import addtool.nui.mxWrapper.nGraphModel;
import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.*;
import addtool.helpers.ExceptionHandler;
import addtool.timeseries.PACTuple;
import com.mxgraph.util.png.mxPngEncodeParam;
import com.mxgraph.util.png.mxPngImageEncoder;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Transforms {@link nGraph} from the UI to {@link ADD}.
 * Also provides functions to dump invalid graphs to a dump file and restore it.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public interface GraphTransformer {
    /**
     * Converts a graph in to a functional ADD
     * @param graph The graph containing a model
     * @return the ADD that could be built from the graph. null if no valid model could be extracted
     * @see ADD
     */
    static ADD graph2Model(nADDGraph graph){
        return graph2Model(graph,new ArrayList<>());
    }
    /**
     * Executes the conversion method on the UI Thread
     * @param graph The graph containing a model
     * @return the ADD that could be built from the graph. null if no valid model could be extracted
     * @see GraphTransformer#graph2Model(nADDGraph)
     */
    static ADD graph2ModelOnUI(nADDGraph graph){
        RunnableFuture<ADD> rf = new FutureTask<>(() -> graph2Model(graph));
        SwingUtilities.invokeLater(rf);
        //noinspection LogException
        try {
            return rf.get();
        } catch (InterruptedException|ExecutionException ex) {
            ExceptionHandler.logException(ex);
        }
        return null;
    }
    /**
     * Converts a graph in to a functional ADD
     * @param graph The graph containing a model
     * @param lines A list to store problems and output of the conversion process in.
     * @return the ADD that could be built from the graph. null if no valid model could be extracted
     * @see ADD
     */
    static ADD graph2Model(nADDGraph graph,List<FeedbackTuple> lines){
        Object[] cells=graph.getChildCells(graph.getDefaultParent());
        return graph2Model(graph,cells,graph.getAllEdges(cells),lines);
    }
    /**
     * Converts a graph in to a functional ADD
     * Allows conversion of parts of the graph
     * @param graph The graph containing a model
     * @param cells The cells from the graph that should be used
     * @param edges the edges that should be used
     * @param lines A list to store problems and output of the conversion process in.
     * @return the ADD that could be built from the graph. null if no valid model could be extracted
     * @see ADD
     */
    static ADD graph2Model(nADDGraph graph,Object[] cells,Object[] edges,List<FeedbackTuple> lines){
        AtomicBoolean fullOutput=new AtomicBoolean(false);
        if(graph.isInAction()){
            return null;
        }
        ADD add = null;
        try{
            graph.getModel().beginUpdate();
            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_start")));

            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_vertices")));
            graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, mxUtils.hexString(Color.BLACK),cells);

            HashMap<String, Vertex> id2vertex= new HashMap<>();
            Arrays.stream(cells).filter(c->c instanceof mxCell)
                    .map(c->graph.getVertex((mxCell)c,false))
                    .filter(Objects::nonNull)
                    .forEach(v-> {
                                if(id2vertex.containsKey(v.getId())){
                                    //lines.add("<span style=\"color:red\">ERROR:</span>");
                                    lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.ERROR,getVertexCode(v)
                                            +getResource("health_check_vertices_not_unique")));
                                    fullOutput.set(true);
                                }else{
                                    id2vertex.put(v.getId(),v);
                                    v.setSinkAttacker(false);
                                    v.setSinkDefender(false);
                                    //noinspection LogException
                                    try {
                                        mxCell cell = graph.getCell(v);
                                        v.setLocation(cell.getGeometry().getPoint());
                                    }catch(RuntimeException ex){
                                        ExceptionHandler.logException(ex);
                                    }
                                }
                            }
                    );
            //getting locations

            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_vertices_finished")));

            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_connections")));
            id2vertex.forEach((k,v)->{
                v.clearSuccessors();
                if(v instanceof NaryOp){
                    ((NaryOp)v).clearPredecessors();
                }
                if(v instanceof UnaryOp){
                    ((UnaryOp)v).clearPredecessor();
                }
                if(v instanceof BasicEvent){
                    ((BasicEvent)v).setTrigger_state(TriggerState.NOT_TRIGGER_ABLE);
                    ((BasicEvent)v).setResettable(false);
                }
                if(v instanceof Trigger){
                    ((Trigger)v).clearToTrigger();
                }
                if(v instanceof Reset){
                    ((Reset)v).clearToReset();
                }
            });
            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_connections_finished")));

            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_connections_build")));
            Arrays.stream(edges).distinct().map(e->(mxCell)e).forEach(e->{
                Vertex v1=graph.getVertex((mxCell)e.getSource(),false);
                Vertex v2=graph.getVertex((mxCell)e.getTarget(),false);
                boolean isDashed=graph.isDashed(e);
                if(v1==null||v2==null){
                    //lines.add("<span style=\"color:red\">ERROR:</span>");
                    lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.ERROR,
                            getResource("connection_invalid")));
                    fullOutput.set(true);
                    graph.setCellStyles(mxConstants.STYLE_STROKECOLOR,
                            mxUtils.hexString(Color.RED),
                            List.of(e).toArray());
                }else if(v2 instanceof BasicEvent&&!isDashed){
                    if(v1 instanceof Trigger){
                        ((Trigger)v1).addToTrigger((BasicEvent) v2);
                        ((BasicEvent)v2).setTrigger_state(TriggerState.TRIGGER_ABLE);
                    }else if(v1 instanceof Reset){
                        graph.setDashed(e,true);
                        ((Reset)v1).addToReset((BasicEvent) v2);
                        ((BasicEvent)v2).setResettable(true);
                    }else{
                        //lines.add("<span style=\"color:red\">ERROR:</span>");
                        lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.ERROR,
                                getVertexCode(v2)+getResource("basic_event_predecessor")));
                        graph.setCellStyles(mxConstants.STYLE_STROKECOLOR,
                                mxUtils.hexString(Color.RED),
                                List.of(e).toArray());
                        fullOutput.set(true);
                    }
                }else{
                    boolean added=false;
                    if(isDashed){
                        if(!(v1 instanceof Reset)&&!(v1 instanceof Trigger)){
                            graph.setDashed(e,false);
                        }else if(!(v2 instanceof BasicEvent)){
                            graph.setDashed(e,false);
                        } else if(v1 instanceof Reset){
                            added=true;
                            ((Reset)v1).addToReset((BasicEvent) v2);
                            ((BasicEvent)v2).setResettable(true);
                        } else if(v1 instanceof Trigger){//Leaving this explicit if later more conditions are added
                            added=true;
                            ((Trigger)v1).addToTrigger((BasicEvent) v2);
                            ((BasicEvent)v2).setTrigger_state(TriggerState.TRIGGER_ABLE);
                        }
                    }
                    if(!added){
                        if(v2 instanceof UnaryOp){
                            UnaryOp u2=(UnaryOp)v2;
                            if(u2.getPredecessor()!=null){
                                //lines.add("<span style=\"color:red\">ERROR:</span>");
                                lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.ERROR,getVertexCode(v2)+getResource("unary_op_predecessor")));
                                fullOutput.set(true);
                            }
                        }
                        v2.addPredecessor(v1);
                        //v1.addSuccessor(v2);
                    }
                }
            });
            //Add Sequential Markings
            graph.readPredecessorsForSequential();

            //Check Nary for Predecessors
            id2vertex.values().stream().filter(v->v instanceof NaryOp&&v.getPredecessors().size()<2).forEach(v->{
                lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.ERROR,
                        getVertexCode(v)+getResource("nary_op_predecessor")));
                fullOutput.set(true);

                graph.setCellStyles(mxConstants.STYLE_STROKECOLOR,
                        mxUtils.hexString(Color.RED),
                        List.of(graph.getCell(v)).toArray());
            });
            id2vertex.values().stream().filter(v->v instanceof UnaryOp&& v.getPredecessors().isEmpty()).forEach(v->{
                lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.ERROR,
                        getVertexCode(v)+getResource("unary_op_no_predecessor")));
                fullOutput.set(true);
                graph.setCellStyles(mxConstants.STYLE_STROKECOLOR,
                        mxUtils.hexString(Color.RED),
                        List.of(graph.getCell(v)).toArray());
            });

            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_connections_build_finished")));

            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,
                    getResource("health_check_search_root")));
            Set<Vertex> noSuccessors=id2vertex.values().stream()
                    .filter(v-> v.getSuccessors().isEmpty() &&v.canBeRoot())
                    .collect(Collectors.toSet());
            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,
                    getResource("health_check_search_root_finished")));

            noSuccessors.forEach(v->{
                //TODO determine better if x is sink attacker
                v.setSinkAttacker(true);
                Object stroke_color=graph.getCellStyle(graph.getCell(v)).get(mxConstants.STYLE_STROKECOLOR);
                if(!stroke_color.equals(mxUtils.hexString(Color.RED))&&!stroke_color.equals(mxUtils.hexString(Color.YELLOW))){
                    //Paint real endpoints iff not painted yet
                    graph.setCellStyles(mxConstants.STYLE_STROKECOLOR,
                            mxUtils.hexString(Color.BLUE),
                            List.of(graph.getCell(v)).toArray());
                }
            });

            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_building_add")));
            add=new ADD(id2vertex,noSuccessors,true);
            add.validateConnections();
            ADD2Dot.writeADD2DotFile(new PrintWriter("tmp.dot"),add);
            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_building_add_finished")));
            //Now save to tmp folder
            add.setComments(graph.getComments());

            //Nested try to avoid break up if only one domain fails
            //noinspection NestedTryStatement,LogException
            try {
                lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_pushing_costs")));
                Map<BasicEvent, PACTuple> map = add.getBasicEvents().stream()
                        .collect(Collectors.toMap(be -> (BasicEvent) be, be -> ((BasicEvent) be).getCosts()));
                CostHeuristic ch = new CostHeuristic(map);
                add.accept(ch);
                graph.setVertex2Cost(ch.getCost());
                lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_pushing_costs_finished")));
            }catch(RuntimeException ex){
                ExceptionHandler.logException(ex);
            }

            //noinspection NestedTryStatement,LogException
            try{
                lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_pushing_delays")));
                Map<BasicEvent, PACTuple> map=add.getBasicEvents().stream()
                        .collect(Collectors.toMap(be -> (BasicEvent)be, be -> ((BasicEvent) be).getDelay()));
                DelayHeuristic dh=new DelayHeuristic(map);
                add.accept(dh);
                graph.setVertex2Delay(dh.getDelay());
                lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_pushing_delays_finished")));
            }catch(RuntimeException ex){
                ExceptionHandler.logException(ex);
            }
            graph.updateCells();

            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_end")));
        }catch(FileNotFoundException|RuntimeException ex){
            lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.ERROR,ex.getClass().toString()));
            Arrays.stream(ex.getStackTrace()).
                    map(StackTraceElement::toString).
                    map(s->new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.ERROR,s)).
                    forEach(lines::add);
            fullOutput.set(true);
        }finally {
            if(graph.getModel() instanceof nGraphModel){
                ((nGraphModel)graph.getModel()).endUpdate(false);
            }else{
                graph.getModel().endUpdate();
            }
        }
        if(fullOutput.get()){
            return null;
        }
        //String text=lines.get(0)+" <span style=\"color:green\">ok</span>";
        lines.clear();
        lines.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.OK,getResource("health_check_end")));
        return ADDBuilder.copyADD(add);
    }

    /**
     * Converts a vertex into feedback code using id and name (if available)
     * @param v The vertex in question
     * @return A string representing the vertex
     */
    private static String getVertexCode(Vertex v){
        if(v.getName().isEmpty()){
            return String.format("ID: %s (%s) ",v.getId(),v.getOperatorName());
        }else{
            return String.format("ID: %s (%s) ",v.getId(),v.getName());
        }
    }

    /**
     * Exports a graph to the dump format
     * this is just writing the object itself serialized.
     * @param graph The graph to write
     * @param file Target file
     */
    static void graph2Dump(nGraph graph,String file){
        try {
            FileOutputStream fileOut =
                    new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(graph);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            ExceptionHandler.logException(i);
        }
    }

    /**
     * Read graph from dump file
     * @param file Target file
     * @return the graph stored in the file. Null on Errors
     */
    static nGraph dump2Graph(String file){
        //noinspection LogException
        try {
            FileInputStream fileIn =
                    new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Object o=in.readObject();
            in.close();
            fileIn.close();
            return (nGraph) o;
        } catch (IOException | ClassNotFoundException i) {
            ExceptionHandler.logException(i);
        }
        return null;
    }

    /**
     * Method for exporting to png. Built after saveXmlPng in com.mxgraph.examples.swing.editor.EditorActions
     * @param component The component containing the graph in question
     * @param target the target file
     * @return true on a success, false if an error occurred
     */
    static boolean savePng(nGraphComponent component,File target) {
        BufferedImage image = mxCellRenderer.createBufferedImage(component.getGraph(),
                null, 1, Color.WHITE, component.isAntiAlias(), null,
                component.getCanvas());
        // Creates the URL-encoded XML data
        mxCodec codec = new mxCodec();
        String xml = URLEncoder.encode(
                mxXmlUtils.getXml(codec.encode(component.getGraph().getModel())), StandardCharsets.UTF_8);
        mxPngEncodeParam param = mxPngEncodeParam
                .getDefaultEncodeParam(image);
        param.setCompressedText(new String[] { "mxGraphModel", xml });
        // Saves as a PNG file
        try(FileOutputStream outputStream = new FileOutputStream(target))
        {
            mxPngImageEncoder encoder = new mxPngImageEncoder(outputStream,
                    param);

            encoder.encode(image);
        } catch (IOException e) {
            ExceptionHandler.logException(e);
            return false;
        }
        return true;
    }
}
