package addtool.nui;

import addtool.helpers.Constants;
import addtool.helpers.ExceptionHandler;
import addtool.helpers.preferences.IntPreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.RadioPreference;
import com.formdev.flatlaf.FlatLightLaf;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;
import static addtool.nui.mxWrapper.nGraph.PREFS_GRAPH_FONT;

/**
 * Helper class for UI related static methods that are used in multiple locations
 */
public final class UIHelper {
    /**
     * Key for look and feel preference
     */
    public static final String PREFS_LOOK_AND_FEEL="look_feel";
    /**
     * Key for UI Font size 12-24
     */
    public static final String PREFS_UI_FONT="ui_font";

    static {
        //Setup Look and Feel Changes
        FlatLightLaf.installLafInfo();
        UIManager.LookAndFeelInfo[] lafInfo = UIManager.getInstalledLookAndFeels();
        String [] LookAndFeels=new String[lafInfo.length];
        for(int i=0;i<lafInfo.length;i++){
            LookAndFeels[i]=lafInfo[i].getClassName();
        }
        RadioPreference lookAndFeel=new RadioPreference(PREFS_LOOK_AND_FEEL,PREFS_LOOK_AND_FEEL,"View",
                "com.formdev.flatlaf.FlatLightLaf",LookAndFeels);
        //Setup Font changes
        RadioPreference fontSize=new RadioPreference(PREFS_UI_FONT,PREFS_UI_FONT,"View",
                "16","12","14","16","18","20","24");
        PreferenceManager.getPreferenceManager().putPreferences(lookAndFeel,fontSize);
        lookAndFeel.addActionListener(e->updateLookAndFeel());
    }

    private UIHelper() {
    }

    /**
     * Updates the UI Look and Feel to the value in the preference
     */
    public static void updateLookAndFeel(){
        String look= PreferenceManager.getPreferenceManager().getPreference(PREFS_LOOK_AND_FEEL).getValueString();
        if(UIManager.getLookAndFeel().getID().equals(look)){
            return;
        }
        try {
            UIManager.setLookAndFeel(look);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ExceptionHandler.logException(ex);
        }
    }
    /**
     * Constructs a button (not toggle) for a toolbar.
     * @param Icon A String for an icon-file in /src/images if file is not found, the string will be used as button-text
     * @param ToolTip A tooltip string
     * @param al The Action to perform on clicking
     * @return The button with above properties
     * @see UIHelper#buildToolButton(String, String, ActionListener, boolean)
     */
    public static AbstractButton buildToolButton(String Icon, String ToolTip, ActionListener al){
        return buildToolButton(Icon,ToolTip,al,false);
    }

    /**
     * Constructs a button for a toolbar.
     * @param Icon A String for an icon-file in /src/images if file is not found, the string will be used as button-text
     * @param ToolTip A tooltip string
     * @param al The Action to perform on clicking
     * @param toggle if true a {@link JToggleButton} will be returned, otherwise a {@link JButton}
     * @return The button with above properties
     */
    public static AbstractButton buildToolButton(String Icon, String ToolTip, ActionListener al,boolean toggle){
        AbstractButton bt;
        if(toggle){
            bt= new JToggleButton();
        } else{
            bt=new JButton();
        }
        updateIcon(getResource(Icon),bt);
        bt.setToolTipText(getTooltip(ToolTip));
        bt.addActionListener(al);
        return bt;
    }

    /**
     * Updates the icon of a button
     * if the icon is not found the value will icon string will be set as the text
     * @param Icon The string refers to a location in the resource folder for the icon
     * @param bt the button to change the icon
     */
    public static void updateIcon(String Icon, AbstractButton bt){
        //noinspection ProhibitedExceptionCaught
        try {
            bt.setIcon(new ImageIcon(
                    ImageIO.read(GraphFrame.class.getResource(
                            Constants.getResourceFolder()+Icon))));
        } catch (IOException | IllegalArgumentException|NullPointerException e) {
            bt.setText(Icon);
        }
    }

    /**
     * Changes the default UI Font to the given UI Font Resource
     * @param f Target Font
     */
    public static void setUIFont(javax.swing.plaf.FontUIResource f){
        Enumeration<Object> keys=UIManager.getLookAndFeelDefaults().keys();
        for (Iterator<Object> it = keys.asIterator(); it.hasNext(); ) {
            Object key = it.next();
            Object value=UIManager.get(key);
            if(value instanceof javax.swing.plaf.FontUIResource) {
                UIManager.getLookAndFeelDefaults().put(key,f);
            }
        }
    }

    /**
     * Changes the UI Font size by the given value. Negatives allowed
     * @param off the offset in Font size
     */
    public static void changeFontSize(int off){
        ((IntPreference)PreferenceManager.getPreferenceManager().getPreference(PREFS_GRAPH_FONT)).increment(off);
    }

    /**
     * Updates the UI Font via the Swing tools
     * @see UIHelper#getUIFont()
     */
    public static void updateUIFont(){
        Font f=getUIFont();
        UIManager.put("MenuBar.font", f);
        UIManager.put("MenuItem.font", f);
        setUIFont(new FontUIResource(f));
    }

    /**
     * Generates the UI Font from the preferences
     * @return the current Font based on preferences
     */
    public static Font getUIFont(){
        return new Font("Sans.serif",Font.PLAIN,
                Integer.parseInt(PreferenceManager.getPreferenceManager().getPreference("ui_font").getValueString()));
    }

    /**
     * Generates a default menuitem
     * @param name text key, will be used in {@link addtool.helpers.preferences.LanguageSupport#getResource(String)} and {@link addtool.helpers.preferences.LanguageSupport#getTooltip(String)}
     * @param mnemonic a mnemonic key
     * @param accelerator A keystroke code
     * @param actionListener an action listener
     * @return the item
     * All arguments except name can be null.
     */
    public static JMenuItem getMenuItem(String name,int mnemonic,KeyStroke accelerator,ActionListener actionListener){
        JMenuItem item=new JMenuItem(getResource(name));
        initializeMenuItem(item,name,mnemonic,accelerator,actionListener);
        return item;
    }
    /**
     * Generates a default menu
     * @param name text key, will be used in {@link addtool.helpers.preferences.LanguageSupport#getResource(String)} and {@link addtool.helpers.preferences.LanguageSupport#getTooltip(String)}
     * @param mnemonic a mnemonic key
     * @param accelerator A keystroke code
     * @param actionListener an action listener
     * @return the item
     * All arguments except name can be null.
     */
    public static JMenu getMenu(String name,int mnemonic,KeyStroke accelerator,ActionListener actionListener){
        JMenu item=new JMenu(getResource(name));
        initializeMenuItem(item,name,mnemonic,accelerator,actionListener);
        return item;
    }
    private static void initializeMenuItem(JMenuItem item, String name, int mnemonic, KeyStroke accelerator, ActionListener actionListener){
        item.setToolTipText(getTooltip(name));
        if(mnemonic!=-1){
            item.setMnemonic(mnemonic);
        }
        if(accelerator!=null){
            item.setAccelerator(accelerator);
        }
        if(actionListener!=null){
            item.addActionListener(actionListener);
        }
    }

    /**
     * Generates a button that appears like a JLabel
     * @param icon The label text or path to an icon in the resources
     * @param tooltip tooltip key as normal text may be an icon path
     * @param actionListener an actionlistener
     * @return the button
     * @see UIHelper#updateIcon(String, AbstractButton)
     */
    public static JButton getLabelLikeButton(String icon,String tooltip,ActionListener actionListener){
        JButton result=getLabelLikeButton(tooltip,actionListener);
        result.setText("");
        updateIcon(getResource(icon),result);
        return result;
    }
    /**
     * Generates a button that appears like a JLabel
     * @param name text key, will be used in {@link addtool.helpers.preferences.LanguageSupport#getResource(String)} and {@link addtool.helpers.preferences.LanguageSupport#getTooltip(String)}
     * @param actionListener an actionlistener
     * @return the button
     */
    public static JButton getLabelLikeButton(String name,ActionListener actionListener){
        JButton lb=new JButton(getResource(name));
        lb.setToolTipText(getTooltip(name));
        lb.setOpaque(false);
        lb.setFocusPainted(false);
        lb.setMargin(new Insets(0, 0, 0, 0));
        lb.setContentAreaFilled(false);
        lb.setBorderPainted(false);

        if(actionListener!=null){
            lb.addActionListener(actionListener);
        }
        return lb;
    }
}
