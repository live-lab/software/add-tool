package addtool.nui;

import addtool.add.io.ADDImport;
import addtool.add.model.ADD;
import addtool.add.model.BasicEvent;
import addtool.add.model.Vertex;
import addtool.add2dot.ADD2Dot;
import addtool.analysisonadd.CostHeuristic;
import addtool.analysisonadd.DelayHeuristic;
import addtool.analysisonadd.ProbabilityPACHeuristic;
import addtool.atbest.DataBaseConnector;
import addtool.atbest.MetaJSONGenerator;
import addtool.dfd.*;
import addtool.dfd.io.DFDExporter;
import addtool.dfd.io.DFDImporter;
import addtool.feedback.FeedbackGenerator;
import addtool.feedback.FeedbackTuple;
import addtool.helpers.*;
import addtool.helpers.preferences.*;
import addtool.modelchecking.CheckerAdapter;
import addtool.nui.mxWrapper.nADDGraph;
import addtool.nui.mxWrapper.nDFDGraph;
import addtool.nui.mxWrapper.nGraph;
import addtool.nui.panels.*;
import addtool.nui.style.nStoreShape;
import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.swing.util.mxGraphTransferable;
import com.mxgraph.swing.util.mxSwingConstants;
import com.mxgraph.util.mxPoint;
import org.eclipse.jgit.api.errors.GitAPIException;

import javax.crypto.Cipher;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.event.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static addtool.helpers.Update.basedir;
import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.TranslationTable.*;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;
import static addtool.nui.UIHelper.*;
import static addtool.nui.panels.AnalysisPanel.PREF_PAC;
import static addtool.nui.style.BasicStyle.*;
import static addtool.nui.style.nStoreShape.SHAPE_STORE;


/**
 * A type of JFrame to display ADD.
 *
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public class GraphFrame extends JFrame{
    /**
     * Preference Key for the Look and Feel
     */
    public static final String PREFS_LAF="LookandFeel";

    /**
     * Preference Key for the minimum Log Level i.e. the minimal level shown in the feedback area
     * @see FeedbackPanel
     */
    public static final String PREFS_LOG="log_level";
    /**
     * Preference Key for the ATBest author
     * This value will be added as a comment to your models
     */
    public static final String PREFS_AUTHOR="author";
    /**
     * Preference Key for the ATBest Token. This is needed for automatic upload
     */
    public static final String PREFS_TOKEN="token";
    /**
     * Preference Key for the timeout to run a single request inside a model checker
     */
    public static final String PREFS_TIMEOUT="runTimeout";
    /**
     * Preference Key to determine if checkers with warnings should be used in the run action
     * @see GraphFrame#runModel()
     * @see GraphFrame#runModel(ADD)
     */
    public static final String PREFS_DO_WARNINGS="doWarnings";
    /**
     * Preference Key to determine if only one chekcer or all available should be used in the run action
     * @see GraphFrame#runModel()
     * @see GraphFrame#runModel(ADD)
     */
    public static final String PREFS_DO_ONE="doOne";
    /**
     * The folder of the example models
     */
    public static final String EXAMPLES="examples";
    /**
     * A marker for minimal display size. Used in the minimal dimension.
     */
    public static final int MINIMAL_SIZE=100;
    /**
     * Minimal dimension of a panel inside the split panels.
     * Made by using Minimal_Size twice for width and once for height
     */
    public static final Dimension MINIMAL_DIMENSION=new Dimension(MINIMAL_SIZE*2,MINIMAL_SIZE);


    static {
        mxGraphics2DCanvas.putShape(SHAPE_STORE,new nStoreShape());
        //Use this to force load the Crypter Class
        Crypter.isEncrypted(new byte[3]);
    }

    /**
     * This Component displays all data from {@link nGraph#doHealthCheck()}.
     */
    private FeedbackPanel logger;
    /**
     * This Component displays model data like probabilities.
     */
    private AnalysisPanel data;
    /**
     * This Component displays model checker outputs.
     */
    private ModelCheckerPanel mcOutput;
    /**
     * THe left toolbar. Used for graph related actions
     */
    private JToolBar leftToolBar;
    /**
     * the top toolbar used for file actions and others
     */
    private JToolBar topToolBar;
    /**
     * The Menubar
     */
    private JMenuBar menuBar;
    /**
     * A Panel to add custom options to.
     */
    private JPanel customOptions;

    private final List<AbstractButton> ADDOptions=new ArrayList<>();
    private final List<AbstractButton> DFDOptions=new ArrayList<>();
    private final List<AbstractButton> GitOptions=new ArrayList<>();
    private GitConnection gitConnection=null;
    /**
     * The component to display the graph.
     */
    //private nGraphComponent graphComponent;
    private MultiGraphComponent graphComponent;
    /**
     * The type of Frame 1=Tabbed, 2=Slider.
     */
    private final int type;
    private static final int AUTO_SAVE_INTERVAL =15;
    private int autoSaveCount=0;
    private boolean isADD=true;

    private AbstractButton runButton;

    private boolean exitOnClose=true;

    /**
     * Creates a new object and calls two methods.
     * @see GraphFrame#setup()
     * @see GraphFrame#startUIUpdate()
     * @see JFrame#JFrame()
     */
    public GraphFrame(){
        super();
        type=1;
        setup();
        startUIUpdate();
    }

    /**
     * Creates a new object and calls two methods.
     * @param title Title of the Frame
     * @see GraphFrame#setup()
     * @see GraphFrame#startUIUpdate()
     * @see JFrame#JFrame(String)
     */
    public GraphFrame(String title){
        this(title,1);
    }

    /**
     * Creates a new object and calls two methods.
     * @param title Title of the Frame
     * @param type 1 = Tabbed, 2 = Slider
     * @see GraphFrame#setup()
     * @see GraphFrame#startUIUpdate()
     * @see JFrame#JFrame(String)
     * @see SliderMultiGraphComponent
     * @see TabbedMultiGraphComponent
     */
    public GraphFrame(String title, int type){
        super(title);
        this.type=type;
        setup();
        startUIUpdate();
    }
    private void buildMenuBar(){
        menuBar=new JMenuBar();
        JMenu fileMenu=UIHelper.getMenu("file",KeyEvent.VK_F,null,null);

        JMenuItem newItem=UIHelper.getMenuItem("new",KeyEvent.VK_N,KeyStroke.getKeyStroke("control N"),e->graphComponent.create(1));
        JMenuItem saveItem=UIHelper.getMenuItem("save",KeyEvent.VK_S,KeyStroke.getKeyStroke("control S"),e->saveAction());
        JMenuItem saveAsItem=UIHelper.getMenuItem("save_as",KeyEvent.VK_A,KeyStroke.getKeyStroke("control shift S"),e->saveAction(true));
        JMenuItem saveMetaItem=UIHelper.getMenuItem("save_meta",-1,null,e->saveMetaAction());
        JMenuItem openItem=UIHelper.getMenuItem("open_file",KeyEvent.VK_O,KeyStroke.getKeyStroke("control O"),e->loadAction());
        JMenuItem openExampleItem=UIHelper.getMenuItem("open_example_file",KeyEvent.VK_X,KeyStroke.getKeyStroke("control shift O"),e->loadExamples());
        JMenuItem exportItem=UIHelper.getMenuItem("export",KeyEvent.VK_E,KeyStroke.getKeyStroke("control E"),e->exportAction());

        fileMenu.add(newItem);
        fileMenu.add(saveItem);
        fileMenu.add(saveAsItem);
        //fileMenu.add(saveMetaItem);//Meta should not be used by standard user
        fileMenu.add(openItem);
        fileMenu.add(openExampleItem);
        fileMenu.add(exportItem);

        fileMenu.addSeparator();

        fileMenu.add(UIHelper.getMenuItem("settings",-1,null,
            e-> PreferenceManager.getPreferenceManager().openEditDialog(this,"",0,
                    l->SwingUtilities.invokeLater(()->SwingUtilities.updateComponentTreeUI(GraphFrame.this))
            )));

        JMenu defaultSettings=UIHelper.getMenu("defaultSettings",-1,null,null);
        for(PreferenceProfile preferenceProfile:PreferenceProfile.getProfiles()){
            defaultSettings.add(UIHelper.getMenuItem(preferenceProfile.getTitle(),-1,null,
                    e->{
                        PreferenceManager.getPreferenceManager().loadProfile(preferenceProfile);
                        SwingUtilities.invokeLater(()->
                                SwingUtilities.updateComponentTreeUI(GraphFrame.this));
                    }));
        }
        fileMenu.add(defaultSettings);

        menuBar.add(fileMenu);

        //EDIT Menu
        JMenu editMenu=UIHelper.getMenu("edit",KeyEvent.VK_E,null,null);

        editMenu.add(UIHelper.getMenuItem("run",KeyEvent.VK_R,KeyStroke.getKeyStroke("control R"),e->runModel()));

        editMenu.addSeparator();

        editMenu.add(UIHelper.getMenuItem("redo",KeyEvent.VK_Y,
                KeyStroke.getKeyStroke("control Y"),e->graphComponent.getGraphComponent().getUndoManager().redo()));
        editMenu.add(UIHelper.getMenuItem("undo",KeyEvent.VK_Z,
                KeyStroke.getKeyStroke("control Z"),e->graphComponent.getGraphComponent().getUndoManager().undo()));

        editMenu.addSeparator();

        editMenu.add(getMenuItem("cut",KeyEvent.VK_X,KeyStroke.getKeyStroke("control X")));
        editMenu.add(getMenuItem("copy",KeyEvent.VK_C,KeyStroke.getKeyStroke("control C")));
        editMenu.add(getMenuItem("paste",KeyEvent.VK_V,KeyStroke.getKeyStroke("control V")));

        menuBar.add(editMenu);


        //Replaces the old submenu
        StringPreference authorName=new StringPreference(PREFS_AUTHOR,PREFS_AUTHOR,"connections/atbest","");
        PreferenceManager.getPreferenceManager().putPreferences(authorName);
        StringPreference userName=new StringPreference(PREFS_TOKEN,PREFS_TOKEN,"connections/atbest","");
        PreferenceManager.getPreferenceManager().putPreferences(userName);

        JMenu viewMenu=UIHelper.getMenu("view",KeyEvent.VK_V,null,null);

        viewMenu.add(UIHelper.getMenuItem("sort_now",KeyEvent.VK_S,KeyStroke.getKeyStroke("control shift O"),
                e->graphComponent.getGraphComponent().sort(graphComponent.getGraph()::doHealthCheck)));

        JComponent sortOrderItem=PreferenceManager.getPreferenceManager().getPreference("sorting").getMenuItem();
        if(sortOrderItem instanceof JMenu){
            ((JMenu)sortOrderItem).setMnemonic(KeyEvent.VK_O);
        }
        viewMenu.add(sortOrderItem);

        //Menu for Log Level

        //Generate log level Strings
        FeedbackTuple.MESSAGE_TYPE[] logValues = FeedbackTuple.MESSAGE_TYPE.values();
        String [] levels=new String[FeedbackTuple.MESSAGE_TYPE.values().length];
        for (int i=0;i<logValues.length;i++){
            levels[i]=logValues[i].toString();
        }
        RadioPreference logLevel=new RadioPreference(PREFS_LOG,PREFS_LOG,"View",
                FeedbackTuple.MESSAGE_TYPE.INFORMATION.toString(),levels);
        PreferenceManager.getPreferenceManager().putPreferences(logLevel);
        logLevel.addActionListener(e->logger.setLogLevel(FeedbackTuple.MESSAGE_TYPE.valueOf(logLevel.getValueString())));
        viewMenu.add(logLevel.getMenuItem());

        JCheckBoxMenuItem gridItem=(JCheckBoxMenuItem)PreferenceManager.getPreferenceManager().getPreference("show_grid").getMenuItem();
        gridItem.setMnemonic(KeyEvent.VK_G);
        viewMenu.add(gridItem);
        menuBar.add(viewMenu);

        //Git Menu
        JMenu gitMenu=UIHelper.getMenu("git_menu",KeyEvent.VK_G,null,null);
        gitMenu.add(UIHelper.getMenuItem("git_pull",KeyEvent.VK_P,null,e->gitPull()));
        gitMenu.add(UIHelper.getMenuItem("git_commit",KeyEvent.VK_C,null,e->gitCommit()));
        gitMenu.add(UIHelper.getMenuItem("git_push",KeyEvent.VK_U,null,e->gitPush()));
        gitMenu.add(UIHelper.getMenuItem("git_push",KeyEvent.VK_U,null,e->gitPush()));
        gitMenu.add(UIHelper.getMenuItem("git_requirement",KeyEvent.VK_R,null,e->gitEditRequirement()));

        GitOptions.add(gitMenu);
        menuBar.add(gitMenu);


        //Help Menu
        JMenu help=UIHelper.getMenu("help_menu",KeyEvent.VK_H,null,null);
        help.add(UIHelper.getMenuItem("help",KeyEvent.VK_E,KeyStroke.getKeyStroke("F1"),e-> HelpViewer.showHelp()));
        help.add(UIHelper.getMenuItem("start_tour",-1,null,
                e-> Tutorial.restartTour(this,graphComponent,leftToolBar,logger,topToolBar,data,mcOutput,menuBar)));

        //Prepare Update logic
        JMenuItem updateItem=UIHelper.getMenuItem("update_check",KeyEvent.VK_U,null,a->{
            if(Update.isUpdateReady()){
                Update.performUpdateFireEvent();
            }else if(Update.isUpdateAvailable()){
                Update.preloadUpdate();
            }else {
                Update.asyncDoUpdatecheck();
            }
        });
        Update.addListener(a->{
                updateItem.setText(getResource(a.getActionCommand()));
                updateItem.setToolTipText(getTooltip(a.getActionCommand()));
        });
        Update.asyncDoUpdatecheck();
        help.add(updateItem);
        //End update logic

        menuBar.add(help);

        this.setJMenuBar(menuBar);
    }

    /**
     * If the closing operation is exit on close
     * @return true if the Frame causes the application to stop on closing
     */
    public boolean isExitOnClose() {
        return exitOnClose;
    }

    /**
     * Set if the frame ends the application on close.
     * Default is true. Should be changed if it is used inside another application.
     * @param exitOnClose true if the application should stop on closing the window
     */
    public void setExitOnClose(boolean exitOnClose) {
        this.exitOnClose = exitOnClose;
    }
    private ActionListener getUIAction(String name){
        return e-> {
                ActionMap uiActionMap = SwingUtilities.getUIActionMap(graphComponent.getGraphComponent());
                if(uiActionMap.get(name)==null){
                    graphComponent.getGraphComponent().updateUIActions();
                    uiActionMap = SwingUtilities.getUIActionMap(graphComponent.getGraphComponent());
                    if(uiActionMap.get(name)==null){
                        //Throw Error
                        JOptionPane.showMessageDialog(this,
                                getResource("error_ui_action"),
                                getResource("error_ui_action_title"),
                                JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                }
                uiActionMap.get(name).actionPerformed(
                        new ActionEvent(graphComponent.getGraphComponent(), 0, name)
                );
            };
    }
    private JMenuItem getMenuItem(String name,int mnemonic,KeyStroke accelerator){
        return UIHelper.getMenuItem(name,mnemonic,accelerator,getUIAction(name));
    }
    private void buildToolBar(){
        //BEGIN TOOLBAR
        topToolBar = new JToolBar();
        //tools.setLayout(new FlowLayout());
        topToolBar.add(buildToolButton("button_open","open_file",(e)-> loadAction()));
        topToolBar.add(buildToolButton("button_save","save_file",(e)-> saveAction()));

        topToolBar.addSeparator();
        IntPreference timeout=new IntPreference(PREFS_TIMEOUT,PREFS_TIMEOUT,"checker","60");
        timeout.setBounds(0,Integer.MAX_VALUE);
        BooleanPreference doWarnings=new BooleanPreference(PREFS_DO_WARNINGS,PREFS_DO_WARNINGS,"checker",true);
        BooleanPreference doOne=new BooleanPreference(PREFS_DO_ONE,PREFS_DO_ONE,"checker",false);
        PreferenceManager.getPreferenceManager().putPreferences(timeout,doWarnings,doOne);
        runButton=buildToolButton("button_run","run",(e)->runModel());
        topToolBar.add(runButton);
        topToolBar.addSeparator();
        topToolBar.add(buildToolButton("button_upload","upload_model",(e)->uploadAction((JButton)e.getSource())));

        topToolBar.addSeparator();

        IntPreference graphFont= (IntPreference) PreferenceManager.getPreferenceManager().getPreference(nGraph.PREFS_GRAPH_FONT);
        topToolBar.add(buildToolButton("button_font_small","font_small",
                (e)->graphFont.decrement(2),false));
        JLabel currentSize=new JLabel(graphFont.getValueString(),SwingConstants.CENTER);
        graphFont.addActionListener(e-> currentSize.setText(graphFont.getValueString()));
        topToolBar.add(currentSize);
        topToolBar.add(buildToolButton("button_font_large","font_large",
                (e)->graphFont.increment(2),false));

        topToolBar.addSeparator();

        topToolBar.add(buildToolButton("button_undo","undo",getUIAction("undo"),false));
        topToolBar.add(buildToolButton("button_redo","redo",getUIAction("redo"),false));

        topToolBar.add(buildToolButton("button_cut","cut",getUIAction("cut"),false));
        topToolBar.add(buildToolButton("button_copy","copy",getUIAction("copy"),false));
        topToolBar.add(buildToolButton("button_paste","paste",getUIAction("paste"),false));
        topToolBar.addSeparator();

        //Git Options:
        AbstractButton gitButton=buildToolButton("git_logo","git",
                (e)->PreferenceManager.getPreferenceManager().openEditDialog(this,"connections/git"),false);
        topToolBar.add(gitButton);
        AbstractButton pullButton=buildToolButton("button_pull","git_pull",(e)->gitPull(),false);
        topToolBar.add(pullButton);
        AbstractButton commitButton=buildToolButton("button_commit","git_commit",(e)-> gitCommit(),false);
        topToolBar.add(commitButton);
        AbstractButton pushButton=buildToolButton("button_push","git_push",(e)->gitPush(),false);
        topToolBar.add(pushButton);
        GitOptions.addAll(List.of(gitButton,pullButton,commitButton,pushButton));

        //Get Anchor for additional tabs:
        customOptions=new JPanel();
        customOptions.setLayout(new GridLayout(1,0));
        topToolBar.add(customOptions);

        topToolBar.addSeparator();


        //Do orientation Listening
        topToolBar.addComponentListener(new ComponentListener() {
            public void changeOrientation(){
                Component []components= topToolBar.getComponents();
                for(Component c:components){
                    if(c instanceof Container){
                        int row= topToolBar.getOrientation()==JToolBar.HORIZONTAL?1:0;
                        int column=row==0?1:0;
                        ((Container) c).setLayout(new GridLayout(row,column));
                    }
                }
            }
            @Override
            public void componentResized(ComponentEvent e) {
                changeOrientation();
            }

            @Override
            public void componentMoved(ComponentEvent e) {
                changeOrientation();
            }

            @Override
            public void componentShown(ComponentEvent e) {
                changeOrientation();
            }

            @Override
            public void componentHidden(ComponentEvent e) {
                changeOrientation();
            }
        });
        //END of Toolbar
        //new JScrollPane(tools)
        this.getContentPane().add(topToolBar, BorderLayout.PAGE_START);


        leftToolBar=new JToolBar();

        leftToolBar.add(buildToolButton("button_edit_vertex","edit_vertex",getUIAction("edit")));

        AbstractButton sortButton=buildToolButton("button_sort","sorting",
                (e)->graphComponent.getGraphComponent().sort(graphComponent.getGraph()::doHealthCheck));
        leftToolBar.add(sortButton);
        ADDOptions.add(sortButton);
        //tools.add(ADDOptions,constButton);

        leftToolBar.addSeparator();

        leftToolBar.add(createDnDVertexButton("And","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_and","button_and_vertex"));
        leftToolBar.add(createDnDVertexButton("SAnd","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_sand","button_sand_vertex"));

        leftToolBar.add(createDnDVertexButton("Or","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_or","button_or_vertex"));
        leftToolBar.add(createDnDVertexButton("SOr","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_sor","button_sor_vertex"));


        leftToolBar.add(createDnDVertexButton("Not","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_not","button_not_vertex"));
        leftToolBar.add(createDnDVertexButton("Nat","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_nat","button_nat_vertex"));
        leftToolBar.add(createDnDVertexButton("NOTTRUE","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_nottrue","button_nottrue_vertex"));


        leftToolBar.add(createDnDVertexButton("COST","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_cost","button_cost_vertex"));
        leftToolBar.add(createDnDVertexButton("TRIGGER","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_trigger","button_trigger_vertex"));
        leftToolBar.add(createDnDVertexButton("RESET","label",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_reset","button_reset_vertex"));


        leftToolBar.add(createDnDVertexButton("BEATTACKER","ellipse",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_ATTACK).getValueString(),
                "add_BEATTACKER","button_bea_vertex"));
        leftToolBar.add(createDnDVertexButton("BEDEFENDER","ellipse",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_DEFEND).getValueString(),
                "add_BEDEFENDER","button_bed_vertex"));
        leftToolBar.add(createDnDVertexButton("BETIME","ellipse",
                PreferenceManager.getPreferenceManager().getPreference(COLOR_VERTEX).getValueString(),
                "add_BETIME","button_bet_vertex"));


        leftToolBar.add(createDnDVertexButton("","straight","#000000",
                "add_edge","button_edge"));



        //DFD buttons
        AbstractButton processButton=buildToolButton("button_process",
                "process",(e)->graphComponent.getGraph().createEmptyVertex());
        DFDOptions.add(processButton);
        leftToolBar.add(processButton);

        AbstractButton storeButton=buildToolButton("button_store","store",
                (e)->{
                    if(graphComponent.getGraph() instanceof nDFDGraph){
                        ((nDFDGraph) graphComponent.getGraph()).insertVertex(new Store());
                    }
                });
        DFDOptions.add(storeButton);
        leftToolBar.add(storeButton);

        AbstractButton actorButton=buildToolButton("button_actor","actor",
                (e)->{
                    if(graphComponent.getGraph() instanceof nDFDGraph){
                        ((nDFDGraph) graphComponent.getGraph()).insertVertex(new Actor());
                    }
                });
        DFDOptions.add(actorButton);
        leftToolBar.add(actorButton);


        AbstractButton trustButton=buildToolButton("button_trust_boundary","trust_boundary",
                (e)->{
                    if(graphComponent.getGraph() instanceof nDFDGraph){
                        Line l=new trustBoundary();
                        l.addPoint(new Point(0,0));
                        l.addPoint(new Point(50,50));
                        l.addPoint(new Point(25,75));
                        l.addPoint(new Point(100,100));
                        ((nDFDGraph) graphComponent.getGraph()).insertVertex(l);
                    }
                });
        DFDOptions.add(trustButton);
        leftToolBar.add(trustButton);
        //TODO check for DFD

        leftToolBar.setOrientation(JToolBar.VERTICAL);
        this.getContentPane().add(leftToolBar, BorderLayout.LINE_START);
    }


    /**
     * true if a run analysis is currently running
     */
    private boolean runningAnalysis=false;
    /**
     * A list of with an hourglass animated buttons
     */
    private final List<AbstractButton> animated=new ArrayList<>();
    private void runModel() {
        runModel((ADD)graphComponent.getGraph().doHealthCheck());
    }
    private void runModel(ADD model) {
        if(runningAnalysis){
            return;
        }
        runningAnalysis=true;
        startAnimation(runButton);
        ExecutorService runner=Executors.newSingleThreadExecutor();
        runner.submit(()->{

            //Search available Checker connectors
            List<CheckerAdapter> available=new ArrayList<>();
            String[] options = CheckerAdapter.getOptions();
            for(String s:options){
                CheckerAdapter adapter=CheckerAdapter.getMethodByName(s);
                if(adapter.isReady()){
                    available.add(adapter);
                }
            }
            //get Best Checker
            List<CheckerAdapter> good=new ArrayList<>();
            List<CheckerAdapter> withWarning=new ArrayList<>();
            for(CheckerAdapter adapter:available){
                Collection<FeedbackTuple> feedback=FeedbackGenerator.getMethodByName(adapter.getName()).getFeedback(model);
                if(feedback.stream().noneMatch(t->t.getType()== FeedbackTuple.MESSAGE_TYPE.ERROR)){
                    if(feedback.stream().anyMatch(t->t.getType()== FeedbackTuple.MESSAGE_TYPE.WARNING)){
                        withWarning.add(adapter);
                    }else{
                        good.add(adapter);
                    }
                }
            }
            //Run Model in checker
            //Try in order if not successful
            boolean success=false;
            StringBuilder result=new StringBuilder("\nQuADTool: ");
            boolean shortOutput=model.getNoSuccessor().size()==1;//Do not write ID if only one root
            //Get ADTool Result:
            result.append(model.getNoSuccessor().stream().
                    map(v->v.getDisruptionProb().doubleValue()+(shortOutput?" ":(" [ID: "+v.getId()+"] "))).
                    collect(Collectors.joining()));
            if(!((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference(PREFS_DO_WARNINGS)).getValue()){
                //Do not use checkers with warnings in feedback
                withWarning.clear();
            }
            String checkerOutput = "";
            for(int i = 0; i<good.size()+withWarning.size(); i++){
                CheckerAdapter adapter=i<good.size()?good.get(i):withWarning.get(i-good.size());
                ByteArrayOutputStream bas = new ByteArrayOutputStream();
                String utf8 = StandardCharsets.UTF_8.name();
                //noinspection LogException
                try (PrintStream ps = new PrintStream(bas, true, utf8)) {
                    adapter.execute(model, ps);
                    int iteration=0;
                    int maxIteration=((Integer)PreferenceManager.getPreferenceManager()
                            .getPreference(PREFS_TIMEOUT).getValue())*10;//times ten as we iterate every 0.1 seconds
                    while (!bas.toString(utf8).contains("Done")&&iteration<maxIteration) {
                        //noinspection BusyWait
                        Thread.sleep(100);
                        iteration++;
                    }
                    checkerOutput= bas.toString(utf8);
                } catch (UnsupportedEncodingException|InterruptedException e) {
                    //Just log it and try next
                    ExceptionHandler.logException(e);
                }
                //Extract result
                String extract=extractProbability(checkerOutput);
                if(extract!=null){
                    result.append(String.format("\n%s: %s",adapter.getName(),extract));
                    if(((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference(PREFS_DO_ONE)).getValue()){
                        //Stop after one success
                        break;
                    }
                }
            }
            //Show Result
            if(result.length()==0){
                JOptionPane.showMessageDialog(this,getResource("no_checker_found"),
                        getResource("no_checker_found_title"),JOptionPane.ERROR_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(this,getResource("disruption_probability")+result,
                        getResource("checker_found_title"),JOptionPane.PLAIN_MESSAGE);
            }
        });
        runner.submit(()->stopAnimation(runButton,getResource("button_run")));
        //noinspection NestedAssignment
        runner.submit(()->runningAnalysis=false);
        runner.shutdown();
    }

    private void startAnimation(AbstractButton bt) {
        animated.add(bt);
        ExecutorService runner=Executors.newSingleThreadExecutor();
        runner.submit(()->{
            int state=0;
            while(animated.contains(bt)) {
                updateIcon(String.format("hourglass_%d.png",state),bt);
                state=(state+1)%5;
                try {
                    //noinspection BusyWait
                    Thread.sleep(state==0?300:100);
                } catch (InterruptedException ignored) {
                }
            }
        });
        runner.shutdown();
    }
    private void stopAnimation(AbstractButton bt,String icon) {
        animated.remove(bt);
        updateIcon(icon,bt);
    }

    /**
     * Automatically extracts the probability value from a model checker output.
     * It does not matter which checker.  Will probably only run smooth with MODEST, PRISM, ADTool, UPPAAL
     * @param text The output of the model checker
     * @return A string containing the probability of success/failure
     */
    public static String extractProbability(String text){
        List<String> startClauses=List.of(
                "<parameter domainId=\"ProbSucc1\" category=\"derived\">",
                "<parameter domainId=\"SandRealDomain1\" category=\"derived\">",
                "Property P_won\n    Estimated probability: ",
                "Result: ",
                "Pr(<> ...) in ["
        );
        List<String> endClauses=List.of(
            "</parameter>",
            "</parameter>",
            "\n    Runs used:",
            " (exact floating point)",
                "]\nwith confidence 0.95."
        );
        for(int i=0;i<startClauses.size();i++){
            if(text.contains(startClauses.get(i))&&text.contains(endClauses.get(i))){
                int beginIndex=text.indexOf(startClauses.get(i))+startClauses.get(i).length();
                int endIndex=text.substring(beginIndex).indexOf(endClauses.get(i))+beginIndex;
                if(beginIndex<endIndex){
                    String result=text.substring(beginIndex,endIndex);
                    if(result.contains(",")){//Uppaal returns confidence bound
                        String[] values=result.split(",");
                        double middle=(Double.parseDouble(values[0])+Double.parseDouble(values[1]))/2;
                        result= String.valueOf(middle);
                    }
                    return result;
                }
            }
        }
        return null;
    }

    /**
     * Generates all UI Components.
     */
    private void setup() {
        updateLookAndFeel();
        updateUIFont();

        PreferenceManager.getPreferenceManager().getPreference(PREFS_UI_FONT).addActionListener(e->{
            updateUIFont();
            SwingUtilities.invokeLater(()->
                    SwingUtilities.updateComponentTreeUI(GraphFrame.this));
            SwingUtilities.invokeLater(()->
                    SwingUtilities.updateComponentTreeUI(GraphFrame.this));
        });

        try {
            this.setIconImage(
                    ImageIO.read(
                            Objects.requireNonNull(
                                    GraphFrame.class.getResource(Constants.getResourceFolder() +getResource("icon_logo"))
                            )
                    )
            );
        } catch (IOException ignored) {
            //DO nothing it only results in no icon
        }

        graphComponent=type==1?new TabbedMultiGraphComponent():new SliderMultiGraphComponent();
        if(type==1){
            ((TabbedMultiGraphComponent)graphComponent).addTabCloseListener(e->checkClosing());
        }
        graphComponent.addListener(this::notifyPanels);
        graphComponent.setMinimumSize(MINIMAL_DIMENSION);
        //graphComponent.getGraphComponent().setGridStyle();
        renewGraph();

        buildMenuBar();

        //This is necessary to get all classes loaded and all preferences ready
        CheckerAdapter.getOptions();

        this.setLayout(new BorderLayout());
        buildToolBar();


        logger=new FeedbackPanel ();
        logger.setLogLevel(FeedbackTuple.MESSAGE_TYPE.valueOf(
                PreferenceManager.getPreferenceManager().getPreference(PREFS_LOG).getValueString()));
        logger.setMinimumSize(MINIMAL_DIMENSION);

        JSplitPane jsp=new JSplitPane(JSplitPane.VERTICAL_SPLIT,graphComponent,logger);
        jsp.setResizeWeight(1);//Will give the graph all space not used by the logger
        jsp.setOneTouchExpandable(true);

        data=new AnalysisPanel();

        data.addListenerTo(ProbabilityPACHeuristic.name,(s,pac)->{
            nGraph graph=graphComponent.getGraph();
            if(graph instanceof nADDGraph){
                Vertex v=((nADDGraph) graph).getVertexByID(s);
                if(!(v instanceof BasicEvent)){
                    return;
                }
                if(((BooleanPreference) PreferenceManager.getPreferenceManager().getPreference(PREF_PAC)).getValue()){
                    ((BasicEvent) v).setSuccessProbability(pac);
                }else{
                    ((BasicEvent) v).setSuccessProbability(pac.getValue());
                }
            }
        });
        data.addListenerTo(CostHeuristic.name,(s, pac)->{
            nGraph graph=graphComponent.getGraph();
            if(graph instanceof nADDGraph){
                Vertex v=((nADDGraph) graph).getVertexByID(s);
                if(!(v instanceof BasicEvent)){
                    return;
                }
                if(((BooleanPreference) PreferenceManager.getPreferenceManager().getPreference(PREF_PAC)).getValue()){
                    ((BasicEvent) v).setCost(pac);
                }else{
                    ((BasicEvent) v).setCost(((BasicEvent) v).getCosts().setValue(pac.getValue()));
                }
            }
        });
        data.addListenerTo(DelayHeuristic.name,(s, pac)->{
            nGraph graph=graphComponent.getGraph();
            if(graph instanceof nADDGraph){
                Vertex v=((nADDGraph) graph).getVertexByID(s);
                if(!(v instanceof BasicEvent)){
                    return;
                }
                //only override full PAC if full PAC was displayed
                if(((BooleanPreference) PreferenceManager.getPreferenceManager().getPreference(PREF_PAC)).getValue()){
                    ((BasicEvent) v).setDelay(pac);
                }else{
                    ((BasicEvent) v).setDelay(((BasicEvent) v).getDelay().setValue(pac.getValue()));
                }
            }
        });
        ETabbedPane dataPane=new ETabbedPane(ComponentOrientation.LEFT_TO_RIGHT,data);
        dataPane.addButton(buildToolButton("button_edit","edit_domain",e->data.switchEdit()), ETabbedPane.ButtonSide.TRAILING);
        dataPane.setMinimumSize(MINIMAL_DIMENSION);

        mcOutput=new ModelCheckerPanel();
        mcOutput.setMinimumSize(MINIMAL_DIMENSION);
        JSplitPane rightSplitPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,dataPane,mcOutput);
        rightSplitPane.setResizeWeight(1);
        rightSplitPane.setOneTouchExpandable(true);

        JSplitPane jSplitPane=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,jsp, rightSplitPane  );
        jSplitPane.setOneTouchExpandable(true);
        jSplitPane.setResizeWeight(0.8);

        this.getContentPane().add(jSplitPane, BorderLayout.CENTER);


        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we)
            {
                int PromptResult=JOptionPane.showConfirmDialog(GraphFrame.this,
                        getResource("exit_message"),getResource("exit_title"),JOptionPane.YES_NO_OPTION);
                if(PromptResult==JOptionPane.YES_OPTION)
                {
                    //Perform last update step if an update is preloaded and waiting
                    if(Update.isUpdateReady()){
                        //Has no consequence. User wants to close and should close
                        //noinspection LogException
                        try {
                            Update.performUpdate(false);
                        } catch (IOException e) {
                            ExceptionHandler.logException(e);
                        }
                    }
                    if(exitOnClose){
                        System.exit(0);
                    }else{
                        setVisible(false);
                        dispose();
                    }
                }
            }
        });
    }

    private void checkClosing() {
        if(graphComponent.getGraphComponent().isModified()){
            int state=JOptionPane.showConfirmDialog(this,getResource("discard_unsaved"),
                    getResource("discard_unsaved_title"),
                    JOptionPane.YES_NO_OPTION);
            if(state==JOptionPane.YES_OPTION){
                saveAction();
            }
        }
    }

    private void uploadAction(JButton bt) {
        Object model=graphComponent.getGraph().doHealthCheck();
        if(model instanceof ADD){
            if(DataBaseConnector.publishAdd((ADD)model)){
                bt.setOpaque(true);
                bt.setBackground(Color.GREEN);
                Executors.newScheduledThreadPool(1)
                        .schedule(()->{
                            bt.setBackground(Color.WHITE);
                            bt.setOpaque(false);
                        },1,TimeUnit.SECONDS);
            }else{
                JOptionPane.showMessageDialog(GraphFrame.this,
                        getResource("upload_error"),
                        getResource("upload_error_title"),JOptionPane.ERROR_MESSAGE);
            }
        }else{
            JOptionPane.showMessageDialog(GraphFrame.this,
                    getResource("export_error"),
                    getResource("export_error_title"),JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Opens a JOptionPane to select Filetype then calls {@link GraphFrame#export(String)}.
     */
    private void export() {
        export(ADDIOHandler.selectFormat(this));
    }

    /**
     * Exports a graph to a filetype.
     * @param format A file format written as in {@link TranslationTable}
     */
    private void export(String format) {
        export(format,null);
    }
    /**
     * Exports a graph to a filetype.
     * @param format A file format written as in {@link TranslationTable}
     */
    private void export(String format,File t) {
        Object adg=graphComponent.getGraph().doHealthCheck();
        if(adg==null){
            JOptionPane.showMessageDialog(GraphFrame.this,
                    getResource("export_error"),
                    getResource("export_error_title"),JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(!(adg instanceof ADD)){
            return;
        }
        ADD model=(ADD)adg;
        File target=t==null?ADDIOHandler.chooseFile(f->true,this,true):t;
        if(target==null){
            return;
        }
        if(ADDIOHandler.export(format,target,model)){
            JOptionPane.showMessageDialog(GraphFrame.this,
                    getResource("export_done"),getResource("export_done")
                    ,JOptionPane.INFORMATION_MESSAGE);
        }
        else{
            JOptionPane.showMessageDialog(GraphFrame.this,
                    getResource("export_error"),
                    getResource("export_error_title"),JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Resets graph and {@link GraphFrame#graphComponent}.
     */
    private void renewGraph(){
        //Do not use it kills the undo mechanic
        //graphComponent.getGraphComponent().setGraph(new nADDGraph());
    }

    /**
     * Triggers repeated health-check.
     * should only be called once in constructor
     */
    private void startUIUpdate(){
        var executorService= Executors.newScheduledThreadPool(1);
        executorService.scheduleWithFixedDelay(this::notifyPanels,1,2, TimeUnit.SECONDS);
    }
    protected void notifyPanels(){
        Object state=graphComponent.getGraph().doHealthCheck();

        checkGitConnection();

        if(state instanceof ADD){
            ADD model=(ADD)state;
            SwingUtilities.invokeLater(()-> data.notify(graphComponent.getGraph(),model));
            SwingUtilities.invokeLater(()-> mcOutput.notify(graphComponent.getGraph(),model));
            pushToTMP(model,graphComponent.getLabel());
            runButton.setEnabled(true);
        }else{
            runButton.setEnabled(false);
        }
        //Get feedback also if the formatting did not work
        if(state instanceof ADD|| state==null){
            SwingUtilities.invokeLater(()-> logger.notify(graphComponent.getGraph(),(ADD)state));
        }
        isADD=graphComponent.getGraph() instanceof nADDGraph;
        //TODO if DFD is activated be more precise in what to show depending on preferences
        //ADDOptions.forEach(a->a.setVisible(isADD));
        DFDOptions.forEach(a->a.setVisible(!isADD));
    }
    protected void saveAction(){
        saveAction(false);
    }
    protected void saveAction(boolean saveAs){
        //If you can save you can leave the moving be!
        graphComponent.getGraph().setInAction(false);
        Object state=graphComponent.getGraph().doHealthCheck();
        if(state==null){
            int res=JOptionPane.showConfirmDialog(GraphFrame.this,
                    getResource("export_error_dump"),
                    getResource("export_error_title"),JOptionPane.YES_NO_OPTION,JOptionPane.ERROR_MESSAGE);
            if(res==JOptionPane.NO_OPTION)return;
        }
        if(state instanceof DFD){
            DFD model=(DFD)state;
            File target=new File(graphComponent.getLabel());
            if(saveAs||!target.exists()){
                target=ADDIOHandler.chooseFile(f->f.isDirectory()||f.getAbsolutePath().endsWith(JSON_EXTENSION),GraphFrame.this,true);
            }
            DFDExporter.writeJSONString(model,target);
        }else if(state instanceof ADD||state==null){

            ADD model=(ADD)state;
            if(checkRequirementWithFeedback(model)){
                return;
            }

            File toSave=null;
            String lb=graphComponent.getLabel();
            if(!saveAs&&lb!=null){
                File f=new File(lb);
                if(f.exists()&&f.isFile()){
                    toSave=f;
                }
            }
            if(toSave!=null&&toSave.getAbsolutePath().toLowerCase().endsWith(XML_EXTENSION)){
                export(XML,toSave);
                checkAddToGit(toSave);
                return;
            }
            //this inspection is wrong
            //noinspection ConstantValue
            if(toSave==null||saveAs){
                toSave=ADDIOHandler.chooseFile(f->f.isDirectory()||f.getAbsolutePath().endsWith(DOT_EXTENSION),TranslationTable.DOT,
                        GraphFrame.this,true);
            }
            if(toSave!=null){
                if(model!=null){
                    try {
                        PrintWriter writer = new PrintWriter(toSave, StandardCharsets.UTF_8);
                        ADD2Dot.writeADD2DotFile(writer, model);
                        if(graphComponent instanceof TabbedMultiGraphComponent){
                            graphComponent.setLabel(toSave.getAbsolutePath());
                        }
                        checkGitConnection();
                        checkAddToGit(toSave);
                    } catch (IOException fileNotFoundException) {
                        ExceptionHandler.logException(fileNotFoundException);
                        JOptionPane.showMessageDialog(this,getResource("export_error"),
                                getResource("export_error_title"),JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                    GraphTransformer.graph2Dump(graphComponent.getGraph(),toSave.getAbsolutePath()+DUMP_EXTENSION);
                }
                if(graphComponent instanceof TabbedMultiGraphComponent){
                    graphComponent.getGraphComponent().setModified(false);
                }
            }
        }
    }

    private AbstractButton createDnDVertexButton(String vertexType,String shape,String color,String tooltip,String icon){
        String style=String.format("fillColor=%s;%s=%s;image=and;imageAlign=center;" +
                "imageVerticalAlign=top;verticalAlign=bottom;shape=%s;fontSize=20;",
                color,nADDGraph.VERTEX_ID,vertexType,shape);
        mxGeometry geometry=new mxGeometry(50,50,100,50);
        mxCell cell=new mxCell(vertexType,geometry,vertexType.isEmpty()?null:style);
        if(vertexType.isEmpty()){
            geometry.setTerminalPoint(new mxPoint(50, 100), true);
            geometry.setTerminalPoint(new mxPoint(150, 50), false);
            geometry.setRelative(true);
            cell.setEdge(true);
        }else{
            cell.setVertex(true);
        }
        mxGraphTransferable t = new mxGraphTransferable(new Object[] {cell },geometry);


        AbstractButton addButton=buildToolButton(icon,tooltip,
                (e)-> {
                    try {
                        graphComponent.getGraph().addCell(cell.clone());
                    } catch (CloneNotSupportedException ex) {
                        ExceptionHandler.logException(ex);
                        JOptionPane.showMessageDialog(this,getResource("add_vertex_error"),
                                getResource("add_vertex_error_title"),JOptionPane.ERROR_MESSAGE);
                    }
                });

        ADDOptions.add(addButton);
        DragSource dragSource = new DragSource();
        // Install the handler for dragging nodes into a graph
        DragGestureListener dragGestureListener = e->
                e.startDrag(null, mxSwingConstants.EMPTY_IMAGE, new Point(),
                        t, null);
        dragSource.createDefaultDragGestureRecognizer(addButton,
                DnDConstants.ACTION_COPY, dragGestureListener);

        //Link to preference
        if(!vertexType.isEmpty()){
            BooleanPreference showVertex=new BooleanPreference("show_"+vertexType.toUpperCase(),"show_"+vertexType.toUpperCase(),"layout/operators",true);
            PreferenceManager.getPreferenceManager().putPreferences(showVertex);
            addButton.setVisible(showVertex.getValue());
            showVertex.addActionListener(e->addButton.setVisible(showVertex.getValue()));
        }

        //End test dnd
        return addButton;
    }
    protected void saveMetaAction(){
        //If you can save you can leave the moving be!
        graphComponent.getGraph().setInAction(false);
        Object state=graphComponent.getGraph().doHealthCheck();
        if(state==null){
            int res=JOptionPane.showConfirmDialog(GraphFrame.this,
                    getResource("export_error_dump"),
                    getResource("export_error_title"),
                    JOptionPane.YES_NO_OPTION,JOptionPane.ERROR_MESSAGE);
            if(res==JOptionPane.NO_OPTION)return;
        }
        if(!(state instanceof ADD))return;
        ADD model=(ADD)state;

        File toSave=ADDIOHandler.chooseFile(f->f.isDirectory()||f.getAbsolutePath().endsWith(DOT_EXTENSION),
                    GraphFrame.this,true);
        if(toSave!=null){
            MetaJSONGenerator.writeJSONString(model,toSave);
        }
    }

    protected void loadAction(){
        File toLoad=ADDIOHandler.chooseFile(
                f->f.isDirectory()||f.getAbsolutePath().endsWith(DOT_EXTENSION)||
                        f.getAbsolutePath().endsWith(DUMP_EXTENSION)||
                        f.getAbsolutePath().endsWith(XML_EXTENSION)||
                        f.getAbsolutePath().endsWith(JSON_EXTENSION),
                "Models (.dot,.dump,.xml)",
                GraphFrame.this,false);
        loadAction(toLoad);
    }
    protected void loadExamples(){
        File toLoad=ADDIOHandler.chooseFile(
                new File(basedir,EXAMPLES),
                f->f.isDirectory()||f.getAbsolutePath().endsWith(DOT_EXTENSION)||
                        f.getAbsolutePath().endsWith(DUMP_EXTENSION)||
                        f.getAbsolutePath().endsWith(XML_EXTENSION)||
                        f.getAbsolutePath().endsWith(JSON_EXTENSION),
                getResource("load_filter"),
                GraphFrame.this,false);
        loadAction(toLoad);
    }

    /**
     * Loads the given File to a graph component.
     * If it is unclear if it should add a new tab there will be a dialog.
     * @param toLoad the file to load in the application
     * @return true if the file could be loaded
     */
    public boolean loadAction(File toLoad){
        if(toLoad!=null){
            File loadingFile=toLoad;
            boolean encrypted=false;
            try {
                String label=loadingFile.getAbsolutePath();
                //if encrypted decrypt in other location:
                if(Crypter.isEncrypted(loadingFile.toPath())){
                    encrypted=true;
                    File target=new File("process."+Constants.getExtensionByStringHandling(loadingFile.getAbsolutePath()).orElse("dot"));
                    Crypter.cipherByPreference(loadingFile,target,Cipher.DECRYPT_MODE,this);
                    loadingFile=target;
                }

                if(loadingFile.getAbsolutePath().endsWith(DUMP_EXTENSION)){
                    //LOAD DUMP
                    nGraph g=GraphTransformer.dump2Graph(loadingFile.getAbsolutePath());
                    if(encrypted){
                        loadingFile.deleteOnExit();
                    }
                    if(g!=null){
                        graphComponent.addGraph(g,label);
                        return true;
                    }
                }
                if(loadingFile.getAbsolutePath().endsWith(JSON_EXTENSION)){
                    //LOAD JSON
                    DFD dfd=DFDImporter.importDFD(loadingFile);
                    nDFDGraph dfdGraph=new nDFDGraph();
                    dfdGraph.load(dfd);
                    graphComponent.addGraph(dfdGraph,label);
                    if(encrypted){
                        loadingFile.deleteOnExit();
                    }
                    return true;
                }

                ADDImport importer=ADDImport.getMethodByExtension(loadingFile.getAbsolutePath());
                if(importer==null){
                    JOptionPane.showMessageDialog(this,
                            getResource("parse_error"),
                            getResource("parse_error_title"),
                            JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                ADD model = importer.importModel(loadingFile);
                if(model!=null){
                    GraphFrame.this.loadADD(model,label,-1);
                }
                if(encrypted){
                    loadingFile.deleteOnExit();
                }
                return true;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this,
                        getResource("input_error"),
                        getResource("input_error_title"),
                        JOptionPane.ERROR_MESSAGE);
                ExceptionHandler.logException(e);
            }finally {
                if(encrypted){
                    loadingFile.deleteOnExit();
                }
            }
        }
        return false;
    }
    private void exportAction() {
        if(isADD){
            boolean success=ADDIOHandler.exportFile(graphComponent.getGraphComponent(), (ADD) getCurrentModel());
            if(success){
                JOptionPane.showMessageDialog(GraphFrame.this,
                        getResource("export_done"),getResource("export_done")
                        ,JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(GraphFrame.this,
                        getResource("export_error"),
                        getResource("export_error_title"),JOptionPane.ERROR_MESSAGE);
            }
        }else{
            throw new UnsupportedOperationException("DFD Export not implemented");
        }
    }

    /**
     * Adds custom panels to the frame.
     * They are located in the top toolbar and will be contained in a titled border if the title is specified.
     * @param title The title to write in the border around the panels. If null no border will be created
     * @param comps the components to add to the panel. Will override existing components.
     */
    public void setCustomPanel(String title,Component ...comps){
        if(title!=null){
            customOptions.setBorder(BorderFactory.createTitledBorder(title));
        }
        customOptions.removeAll();
        for(Component c:comps){
            customOptions.add(c);
        }
        customOptions.invalidate();
    }

    private void pushToTMP(ADD add, String label) {
        if(autoSaveCount< AUTO_SAVE_INTERVAL){
            autoSaveCount++;
            return;
        }
        autoSaveCount=0;
        File tmp=new File(basedir,"tmp");
        tmp.mkdir();
        if(label==null){
            label="unknown.dot";
        }else if(!label.endsWith(DOT_EXTENSION)){
            label += DOT_EXTENSION;
        }
        File target=new File(label);
        String prefix=target.getName().substring(0,target.getName().length()-4)+ '_';

        //Removing old Files
        if(tmp.listFiles()!=null){
            Arrays.stream(tmp.listFiles((file, s) -> s.startsWith(prefix))).
                    sorted((f1, f2)->-f1.getAbsolutePath().
                            compareTo(f2.getAbsolutePath())).
                    skip(10).forEach(File::delete);
        }
        LocalDateTime now= LocalDateTime.now();
        String output=prefix+now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd-hh-mm-ss"))+(add==null?DUMP_EXTENSION:DOT_EXTENSION);

        if(add!=null){
            //No error message if autosave failes. It will be tried again.
            //noinspection LogException
            try {
                ADD2Dot.writeADD2DotFile(new PrintWriter(new File(tmp,output).getAbsolutePath()),add);
            } catch (FileNotFoundException e) {
                ExceptionHandler.logException(e);
            }
        }else{
            GraphTransformer.graph2Dump(graphComponent.getGraph(),new File(tmp,output).getAbsolutePath());
        }
    }

    /**
     * Returns the current model from the currently selected graphcomponent
     * @return Either a DFD or ADD or null
     */
    public Object getCurrentModel(){
        return graphComponent.getGraph().doHealthCheck();
    }

    /**
     * Imports an ADD to the UI.
     * @param add the ADD to import
     * @return Returns a boolean if only the passed model was used
     */
    public boolean loadADD(ADD add){
        return loadADD(add,null,-1);
    }

    /**
     * Imports an ADD to the UI.
     * state==-1: Display dialog for user choice
     * state==0 /CANCEL_OPTION: do not load if canvas is nonempty
     * state==1 /YES_OPTION: merge graphs
     * state==2 /NO_OPTION: open new tab
     * @param add the ADD to import
     * @param label A label for the tab, usually the file path
     * @param state Information if the graphs should be merged
     * @return Returns a boolean if only the passed model was used
     */
    public boolean loadADD(ADD add,String label,int state){
        //First check if label already exists
        if(label!=null&&graphComponent.getLabels().contains(label)){
            int index=graphComponent.getLabels().indexOf(label);
            graphComponent.display(index);
            return true;
        }

        nGraph graph=graphComponent.getGraph();
        if(!graph.isEmpty()){
            Object[] options = { getResource("merge"),getResource("new_tab"),getResource("cancel") };
            if(state==-1){
                state=JOptionPane.showOptionDialog(this,
                        getResource("merge_current"),
                        getResource("merge_current_title"),
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null, options,null);
            }
            if(state==JOptionPane.CANCEL_OPTION||state==JOptionPane.CLOSED_OPTION){
                return false;
            }
            else if(state==JOptionPane.YES_OPTION){
                renewGraph();
            }else if(state==JOptionPane.NO_OPTION){
                graphComponent.create(1);
            }
        }else{
            renewGraph();
        }
        if(label!=null&&!label.isEmpty()){
            graphComponent.setLabel(label);
        }
        graphComponent.getGraph().load(add);
        graphComponent.getGraphComponent().sortIfMissingLocations(graphComponent.getGraph()::doHealthCheck);
        notifyPanels();
        return true;
    }

    /**
     * Loads multiple ADDs
     * @param add a list of models
     * @see GraphFrame#loadModels(List)
     */
    public void loadADDs(List<ADD> add){
        loadModels(Arrays.asList(add.toArray()));
    }
    /**
     * Loads multiple ADDs
     * @param add a list of models
     * @param feedback A list of additional data for each model to be displayed with it
     * @param labels A list of labels for the models
     * @see GraphFrame#loadModels(List, List, List)
     */
    public void loadADDs(List<ADD> add, List<Collection<FeedbackTuple>>feedback, List<String> labels){
        loadModels(Arrays.asList(add.toArray()),feedback,labels);
    }
    /**
     * Loads multiple Models ADDs/DFDs
     * @param add a list of models
     */
    public void loadModels(List<Object> add){
        graphComponent.loadADDs(add);
        notifyPanels();
    }
    /**
     * Loads multiple ADDs/DFDs
     * @param add a list of models
     * @param feedback A list of additional data for each model to be displayed with it
     * @param labels A list of labels for the models
     */
    public void loadModels(List<Object> add, List<Collection<FeedbackTuple>>feedback, List<String> labels){
        graphComponent.loadADDs(add,feedback,labels);
        notifyPanels();
    }

    /**
     * Pass on label change to current graph
     * @param lb the new label for the current canvas
     */
    public void setLabel(String lb) {
        graphComponent.setLabel(lb);
    }

    //GIT Handling Methods
    private void gitCommit(){
        boolean success=gitConnection.commit(
                JOptionPane.showInputDialog(this,
                        getResource("commit_message_text"),
                        getResource("commit_message_title"),
                        JOptionPane.PLAIN_MESSAGE)
        );
        gitMessage(success);
    }
    private void gitPull(){
        gitMessage(gitConnection.pull());
    }

    private void gitEditRequirement() {
        try {
            File target=new File(gitConnection.getRoot().getAbsolutePath(),REQUIREMENT_FILE);
            Requirement.edit(this,target);
            if(!gitConnection.trackedInRepo(target))gitConnection.add(target);
        } catch (Exception ex) {
            ExceptionHandler.logException(ex);
            JOptionPane.showMessageDialog(this,getResource("change_requirement_error"),
                    getResource("change_requirement_error_title"),JOptionPane.ERROR_MESSAGE);
        }
    }
    private void gitPush(){
        gitMessage(gitConnection.push());
    }
    private void gitMessage(boolean success){
        if(success){
            JOptionPane.showMessageDialog(this,getResource("git_success"),
                    getResource("git_success_title"),JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(this,getResource("git_fail"),
                    getResource("git_fail_title"),JOptionPane.ERROR_MESSAGE);
        }
    }
    private void checkGitConnection(){
        //noinspection LogException
        try {
            File f=new File(graphComponent.getLabel());
            if(GitConnection.isInRepo(f)){
                gitConnection=GitConnection.getFromFile(f);
                gitConnection.setLoginOnFail(this,true);
                GitOptions.forEach(b->b.setVisible(true));
                return;
            }
        } catch (IOException e) {
            ExceptionHandler.logException(e);
        }
        GitOptions.forEach(b->b.setVisible(false));
        gitConnection=null;//If it failes just deactivate git
    }
    private void checkAddToGit(File toSave) {
        try {
            if(gitConnection!=null&&!gitConnection.trackedInRepo(toSave)){
                int result=JOptionPane.showConfirmDialog(this,
                        getResource("git_add_message"),
                        getResource("git_add_title"),
                        JOptionPane.YES_NO_OPTION);
                if(result==JOptionPane.YES_OPTION){
                    gitConnection.add(toSave);
                }
            }
        } catch (IOException|GitAPIException e) {
            ExceptionHandler.logException(e);
            JOptionPane.showMessageDialog(this,getResource("git_add_error"),
                    getResource("git_add_error_title"),JOptionPane.ERROR_MESSAGE);
        }
    }
    private static final String REQUIREMENT_FILE=".quadtool";
    private boolean checkRequirement(ADD model){
        if(gitConnection!=null){
            File reqFile=new File(gitConnection.getRoot().getAbsolutePath(),REQUIREMENT_FILE);
            if(reqFile.exists()){
                //Error can be suppressed as default is true
                //noinspection LogException
                try {
                    return Requirement.checkRequirements(reqFile,model);
                } catch (Exception e) {
                    ExceptionHandler.logException(e);
                }
            }
        }
        return true;
    }

    /**
     * Checks the requirements from the repo. If not met shows a Dialog with feedback.
     * @param model A model to check the requirements on
     * @return a boolean if the action should be aborted.
     */
    private boolean checkRequirementWithFeedback(ADD model){
        if(!checkRequirement(model)){
            File reqFile=new File(gitConnection.getRoot().getAbsolutePath(),REQUIREMENT_FILE);
            //noinspection LogException
            try {
                int result=JOptionPane.showConfirmDialog(this,
                        Requirement.getFeedback(reqFile,model)+getResource("requirement_not_met_suffix"),
                        getResource("requirement_not_met_title"),JOptionPane.YES_NO_OPTION);
                return result==JOptionPane.NO_OPTION;
            } catch (Exception e) {
                ExceptionHandler.logException(e);
            }
        }
        return false;
    }

    /**
     * Starts tutorial tour
     */
    public void startTutorial() {
        Tutorial.startTour(this,graphComponent,leftToolBar,logger,topToolBar,data,mcOutput,menuBar);
    }
}
