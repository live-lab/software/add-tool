package addtool.nui;

import addtool.add.model.*;
import addtool.add.operators.Comparison;
import addtool.helpers.TranslationTable;
import addtool.helpers.preferences.BooleanPreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.nui.panels.AnalysisPanel;
import addtool.nui.panels.HidableTabbedPanel;
import com.github.signaflo.timeseries.TimeSeries;
import addtool.dot2add.RationalFromString;
import addtool.helpers.ExceptionHandler;
import org.jscience.mathematics.number.Rational;
import addtool.timeseries.CSVHandler;
import addtool.timeseries.CalcPAC;
import addtool.timeseries.PACTuple;
import addtool.timeseries.TimeSeriesAnalysis;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

/**
 * An edit dialog for vertices
 * @see Vertex
 */
public final class VertexEditPane {
    private static final Vertex[] VERTICES = {};
    /**
     * Preferences Key to show advanced options
     */
    public static final String PREF_ADVANCED_OPTIONS="vertex_advanced_options";

    private VertexEditPane() {
    }
    static int row=0;
    static final int BORDER=5;
    /**
     * List of supported operators
     * may be replaced in later versions
     */
    public static final String[] operators = { "AND","SAND", "OR","SOR", "NOT","NOTTRUE", "NAT", "COST","TRIGGER","RESET" , "BEPLAYER" , "BETIME"};
    static {
        BooleanPreference showAdvancedOptions=new BooleanPreference(PREF_ADVANCED_OPTIONS,PREF_ADVANCED_OPTIONS,"layout/vertex",false);
        PreferenceManager.getPreferenceManager().putPreferences(showAdvancedOptions);
    }

    /**
     * A quick method to change a vertex operator e.g. from a popupMenu
     * @param old the vertex to change
     * @param operator the new operator key. Use values from the TranslationTable
     * @return the new operator
     */
    public static Vertex changeType(Vertex old,String operator){
        if(Arrays.asList(operators).contains(operator)||
                TranslationTable.BASIC_EVENT_ATTACKER.contains(operator)||
                TranslationTable.BASIC_EVENT_DEFENDER.contains(operator)){
            if(operator.equals("BEPLAYER")){
                operator=TranslationTable.BASIC_EVENT_ATTACKER.get(0);
            }
            Vertex neu=ADDBuilder.buildOperatorForKey(operator,old.getId(),old.getName());
            if(neu.getClass().equals(old.getClass())){
                if(!(neu instanceof BasicEventPlayer&&
                        !((BasicEventPlayer) neu).getPlayer().equals(((BasicEventPlayer) old).getPlayer()))){
                    //Writing values to the same class is pointless
                    return old;
                }
            }
            if(neu instanceof NaryOp){
                old.getPredecessors().forEach(p->p.replaceSuccessor(old,neu));
            }
            if(neu instanceof UnaryOp){
                old.getPredecessors().stream().limit(1).forEach(p->p.replaceSuccessor(old,neu));
            }
            if(neu instanceof BasicEvent&&old instanceof BasicEvent){
                //Write Probabilities if applicable
                BasicEvent oldEvent=(BasicEvent) old;
                BasicEvent neuEvent=(BasicEvent) neu;
                neuEvent.setSuccessProbability(oldEvent.getSuccessProbability());
                neuEvent.setProbabilityDelta(oldEvent.getProbabilityDelta());
                neuEvent.setUncertainty(oldEvent.getUncertainty());
                neuEvent.setCost(oldEvent.getCost());
                neuEvent.setDelay(oldEvent.getDelay());
            }
            old.getSuccessors().forEach(s->s.replacePredecessor(old,neu));
            return neu;
            //NOTE: Writing a clause for Cost is not necessary. Only one of the operators is of type cost. So nothing is transferred.
        }
        return old;
    }
    static void addStandardPanel(JPanel main,JComponent leftComponent,JComponent rightComponent,
                                           GridBagConstraints leftConstraints, GridBagConstraints rightConstraints){

        addStandardPanel(main,leftComponent,leftConstraints);
        row--;
        addStandardPanel(main,rightComponent,rightConstraints);
    }
    private static void addStandardPanel(JPanel main,JComponent component,GridBagConstraints constraints){
        Color source;//The background color
        if(row%2==0){
            source=UIManager.getDefaults().getColor("Label.background");
        }else{
            source=UIManager.getDefaults().getColor("ToolBar.light");
        }

        if(component instanceof JLabel){
            ((JLabel) component).setHorizontalAlignment(SwingConstants.RIGHT);
            component.setBorder(BorderFactory.createLineBorder(source,BORDER));
            component.setBackground(source);
            component.setOpaque(true);
        }
        if(component instanceof JTextField){
            component.setBorder(BorderFactory.createLineBorder(source,BORDER));
        }
        if(component instanceof JCheckBox||component instanceof JTabbedPane||component instanceof BoundPanel){
            component.setBackground(source);
            component.setOpaque(true);
        }
        main.add(component,constraints);
        row++;
    }

    /**
     * Starts the editing dialog
     * @param v the vertex to start with
     * @param parent a parent window
     * @return the vertex post editing, or the original vertex if the operation was aborted
     */
    public static Vertex editVertex(Vertex v, Window parent){
        return editVertex(v,parent,null);
    }
    /**
     * Starts the editing dialog
     * @param parent a parent window
     * @return the vertex post editing, or the original vertex if the operation was aborted
     */
    public static Vertex editVertex(Window parent){
        return editVertex(new And("And","1"),parent);
    }
    /**
     * Starts the editing dialog
     * @param v the vertex to start with
     * @param parent a parent window
     * @param relativeLocation an offset location for display
     * @return the vertex post editing, or the original vertex if the operation was aborted
     */
    public static Vertex editVertex(Vertex v, Window parent,Point relativeLocation){
        JDialog dia=new JDialog(parent);
        if(parent!=null){
            dia.setIconImages(parent.getIconImages());
        }
        JPanel pan=new JPanel();
        pan.setLayout(new GridBagLayout());
        JComboBox<String> jcb=new JComboBox<>(operators);
        jcb.setSelectedItem(v==null?"AND":v.getOperatorName().toUpperCase());
        JLabel previousName=new JLabel(jcb.getSelectedItem().toString());
        JTextField nameTextField=new JTextField(v==null?"":v.getName());
        JTextField idTextField=new JTextField(v==null?"":v.getId());
        JCheckBox rootCheckBox=new JCheckBox(getResource("can_be_root"));
        rootCheckBox.setToolTipText(getTooltip("can_be_root"));
        rootCheckBox.setSelected(v == null || v.canBeRoot());
        GridBagConstraints left=new GridBagConstraints();
        left.gridwidth=3;
        left.gridy=-1;
        left.gridx=1;
        left.fill=GridBagConstraints.BOTH;
        GridBagConstraints right=new GridBagConstraints();
        right.gridwidth=6;
        right.gridy=-1;
        right.fill=GridBagConstraints.HORIZONTAL;
        right.gridx=left.gridwidth+1;
        GridBagConstraints all=new GridBagConstraints();
        all.gridwidth=left.gridwidth+right.gridwidth;
        all.gridy=-1;
        all.fill=GridBagConstraints.HORIZONTAL;
        all.gridx=1;
        addStandardPanel(pan,new JLabel(getResource("vertex_type")),jcb,left,right);
        addStandardPanel(pan,new JLabel(getResource("name")),nameTextField,left,right);

        if(((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference(PREF_ADVANCED_OPTIONS)).getValue()) {
            addStandardPanel(pan, new JLabel(getResource("id")), idTextField, left, right);
            addStandardPanel(pan, new JLabel(""), rootCheckBox, left, right);
        }
        //BE player
        JComboBox<Player> playerJComboBox=new JComboBox<>(Player.values());

        if((v instanceof BasicEventPlayer))playerJComboBox.setSelectedItem(((BasicEventPlayer)v).getPlayer());

        JLabel playerLabel=new JLabel(getResource("player"));
        addStandardPanel(pan,playerLabel,playerJComboBox,left,right);

        //BE Time
        JTextField distributionTextField=new JTextField(v instanceof BasicEventTime ?((BasicEventTime)v).getDistribution():"");
        if(!(v instanceof BasicEventTime))distributionTextField.setEditable(false);
        JLabel distributionLabel=new JLabel(getResource("distribution"));
        row--;//reduce as player and distribution do never show up together
        addStandardPanel(pan,distributionLabel,distributionTextField,left,right);

        //Predecessor fields
        //Predecessors
        JDragDropList predecessorDragAndDropList=new JDragDropList(v==null?VERTICES:v.getPredecessors().toArray(VERTICES));
        //if(!(v instanceof BasicEventTime))distributionTextField.setEditable(false);
        JLabel predecessorLabel=new JLabel(getResource("predecessors"));


        //BE Details
        PACPanel pacProb;
        PACPanel pacCost;
        PACPanel pacDelay;
        HidableTabbedPanel tabbedPane = new HidableTabbedPanel();
        if(v instanceof BasicEvent){
            BasicEvent be=(BasicEvent)v;
            pacProb=new PACPanel(dia,getResource("probability"),be.getSuccessProbabilityTuple());
            pacCost=new PACPanel(dia,getResource("cost"),be.getCosts());
            pacDelay=new PACPanel(dia,getResource("delay"),be.getDelay());
            predecessorDragAndDropList.setVisible(false);
            predecessorLabel.setVisible(false);
        }else{

            pacProb=new PACPanel(dia,getResource("probability"));
            pacCost=new PACPanel(dia,getResource("cost"));
            pacDelay=new PACPanel(dia,getResource("delay"));
            tabbedPane.setVisible(false);
        }
        if(!(v instanceof BasicEventTime)){
            distributionLabel.setVisible(false);
            distributionTextField.setVisible(false);
        }
        if(!(v instanceof BasicEventPlayer)){
            playerJComboBox.setVisible(false);
            playerLabel.setVisible(false);
        }
        tabbedPane.addTab(getResource("probability"),  pacProb);
        tabbedPane.addTab(getResource("cost"),  pacCost);
        tabbedPane.addTab(getResource("delay"),  pacDelay);

        //Read preferences on showing the domains:
        tabbedPane.setVisibilityAt(0,((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference("show_Probability")).getValue());
        tabbedPane.setVisibilityAt(1,((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference("show_Cost")).getValue());
        tabbedPane.setVisibilityAt(2,((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference("show_Delay")).getValue());

        //pan.add(tabbedPane,all);
        addStandardPanel(pan,tabbedPane,all);

        //Add Predecessor Panels
        //row--;
        addStandardPanel(pan,predecessorLabel,predecessorDragAndDropList,left,right);
        predecessorDragAndDropList.setEnabled(v instanceof NaryOp && ((NaryOp) v).isSequential());

        /*
         * Cost Operator fields
         */
        BoundHolderPanel CostPanel;
        if(v instanceof Cost){
            Cost c=(Cost)v;
            CostPanel=new BoundHolderPanel(dia,c.getOp(),c.getBound());
        }else{
            CostPanel=new BoundHolderPanel(dia);
            CostPanel.setVisible(false);
        }
        CostPanel.setBorder(BorderFactory.createTitledBorder("Boundaries"));
        pan.add(CostPanel,all);

        jcb.addActionListener(e->{
            Object selected=jcb.getSelectedItem();
            if(selected==null)return;
            String selection=selected.toString();
            if(selection.startsWith("BE")){
                tabbedPane.setVisible(true);
                predecessorDragAndDropList.setVisible(false);
                predecessorLabel.setVisible(false);
            }else{
                tabbedPane.setVisible(false);
                predecessorDragAndDropList.setVisible(true);
                predecessorLabel.setVisible(true);
            }
            playerJComboBox.setVisible(false);
            playerLabel.setVisible(false);

            //Change default Name to new operator where applicable
            if(nameTextField.getText().equalsIgnoreCase(previousName.getText())){
                nameTextField.setText(selection);
            }
            previousName.setText(selection);

            distributionLabel.setVisible(false);
            distributionTextField.setVisible(false);
            predecessorDragAndDropList.setVisible(false);
            if(selection.equals("BEPLAYER")){
                playerJComboBox.setVisible(true);
                playerLabel.setVisible(true);
            }
            if(selection.equals("BETIME")){
                distributionTextField.setVisible(true);
                distributionTextField.setEditable(true);
                distributionLabel.setVisible(true);
            }
            if(selection.equals("SAND")||jcb.getSelectedItem().toString().equals("SOR")){
                predecessorDragAndDropList.setEnabled(true);
            }

            rootCheckBox.setSelected(
                    !selection.equals("TRIGGER") &&
                            !jcb.getSelectedItem().toString().equals("RESET")
            );

            CostPanel.setVisible(selection.equalsIgnoreCase("COST"));

            dia.pack();
        });
        AtomicBoolean save=new AtomicBoolean(false);
        JButton saveButton=(JButton)UIHelper.buildToolButton("save","save",e->{
            save.set(true);
            dia.dispose();
        },false);
        JRootPane rootPane = SwingUtilities.getRootPane(dia);
        rootPane.setDefaultButton(saveButton);

        AbstractButton discard=UIHelper.buildToolButton("cancel","cancel",e->{
            save.set(false);
            dia.dispose();
        },false);

        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new GridLayout(1,0));
        buttonPanel.add(discard);
        buttonPanel.add(saveButton);
        pan.add(buttonPanel,all);

        dia.add(pan);
        dia.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dia.setModal(true);
        dia.setTitle(getResource("edit_vertex"));
        dia.pack();
        if(relativeLocation!=null){
            Dimension size=dia.getSize();
            Point location=relativeLocation;
            if(parent!=null){
                if(location.x+size.width>parent.getWidth()){
                    location.x=parent.getWidth()-size.width;
                }
                if(location.y+size.height>parent.getHeight()){
                    location.y=parent.getHeight()-size.height;
                }
            }
            dia.setLocation(location);
        }
        dia.setVisible(true);

        if(save.get()){
            //Read results
            Object selected=jcb.getSelectedItem();
            if(selected==null)return null;
            String selection=selected.toString();
            Vertex newVertex;
            if(selection.equalsIgnoreCase("BETIME")){
                newVertex=new BasicEventTime(nameTextField.getText(),
                        pacProb.getValues(),
                        pacCost.getValues(),
                        pacDelay.getValues(),
                        idTextField.getText(),
                        distributionTextField.getText());
            }else if(selection.equalsIgnoreCase("BEPLAYER")){
                newVertex=new BasicEventPlayer(nameTextField.getText(),
                        pacProb.getValues(),
                        pacCost.getValues(),
                        pacDelay.getValues(),
                        idTextField.getText(),
                        (Player)playerJComboBox.getSelectedItem());
            }else if(selection.equalsIgnoreCase("COST")) {
                newVertex=new Cost(nameTextField.getText(),
                        CostPanel.getBound(),
                        CostPanel.getComparison(),
                        idTextField.getText());
            }else{
                List<Vertex> predecessors = v==null?new ArrayList<>():v.getPredecessors();
                newVertex=ADDBuilder.buildOperatorForKey(selection,idTextField.getText(),nameTextField.getText());
                if(newVertex instanceof UnaryOp&& !predecessors.isEmpty())((UnaryOp)newVertex).setPredecessor(predecessors.get(0));
                if(newVertex instanceof NaryOp){
                    NaryOp nary=(NaryOp)newVertex;
                    if(nary.isSequential()){
                        nary.addPredecessors(Arrays.asList(predecessorDragAndDropList.getOrderedPredecessors()));
                    }else{
                        nary.addPredecessors(predecessors);
                    }
                }
            }
            newVertex.setCanBeRoot(rootCheckBox.isSelected());

            return newVertex;
        }else{
            return v;
        }


    }
    static final class PACPanel extends JPanel{
        private final JTextField probabilityTextField;
        private final JTextField uncertaintyTextField;
        private final JTextField deltaTextField;
        private final AbstractButton ab;
        public PACPanel(Window par,String value){
            this(par,value,new PACTuple(Rational.ONE,Rational.ZERO,Rational.ONE));
        }
        public PACPanel(Window par,String value,PACTuple initialValue){
            super();
            GridBagConstraints left=new GridBagConstraints();
            left.gridwidth=3;
            left.gridy=-1;
            left.gridx=1;
            left.fill=GridBagConstraints.HORIZONTAL;
            left.weightx=0.2;
            GridBagConstraints right=new GridBagConstraints();
            right.gridwidth=6;
            right.gridy=-1;
            right.fill=GridBagConstraints.HORIZONTAL;
            right.gridx=left.gridwidth+1;
            right.weightx=0.8;

            this.setLayout(new GridBagLayout());
            probabilityTextField =new JTextField(initialValue ==null?
                    "":
                    String.valueOf(initialValue.getValue().doubleValue()));
            probabilityTextField.setToolTipText(value);
            uncertaintyTextField =new JTextField(initialValue ==null?
                    "":
                    String.valueOf(initialValue.getUncertainty().doubleValue()));
            uncertaintyTextField.setToolTipText(getResource("uncertainty"));
            deltaTextField =new JTextField(initialValue ==null?
                    "":
                    String.valueOf(initialValue.getDelta().doubleValue()));
            deltaTextField.setToolTipText(getResource("prob_delta"));
            left.gridwidth--;
            this.add(new JLabel(value),left);
            GridBagConstraints inlineButtonConstraints=new GridBagConstraints();
            inlineButtonConstraints.gridwidth=1;
            inlineButtonConstraints.gridx=left.gridwidth+1;
            ab= UIHelper.buildToolButton("button_input","import",
                    e-> Executors.newSingleThreadExecutor().submit(()->calcPAC(par,(AbstractButton)e.getSource())));
            this.add(ab,inlineButtonConstraints);
            this.add(probabilityTextField,right);
            left.gridwidth++;
            if(((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference(AnalysisPanel.PREF_PAC)).getValue()){
                this.add(new JLabel(getResource("uncertainty")),left);
                this.add(uncertaintyTextField,right);
                this.add(new JLabel(getResource("prob_delta")),left);
                this.add(deltaTextField,right);
            }
        }

        public PACTuple getValues(){
            return new PACTuple(RationalFromString.getRationalFromStringLocale(probabilityTextField.getText()),
                    RationalFromString.getRationalFromStringLocale(uncertaintyTextField.getText()),
                    RationalFromString.getRationalFromStringLocale(deltaTextField.getText()));
        }
        public void setEditable(boolean edit){
            ab.setEnabled(edit);
            probabilityTextField.setEditable(edit);
            uncertaintyTextField.setEditable(edit);
            deltaTextField.setEditable(edit);
        }

        private void calcPAC(Window dia, AbstractButton caller){
            caller.setEnabled(false);
            //Find algorithm
            String [] options= TimeSeriesAnalysis.getOptions();
            int state=JOptionPane.showOptionDialog(dia,
                    getResource("import_models"),
                    getResource("import_models_title"),
                    JOptionPane.DEFAULT_OPTION,JOptionPane.QUESTION_MESSAGE,null,options,options[0]);
            if(state<0||state>options.length){
                caller.setEnabled(true);
                return;
            }
            TimeSeriesAnalysis algorithm=TimeSeriesAnalysis.getMethodByName(options[state]);


            CalcPAC cp=new CalcPAC(algorithm);
            JDialog jd=new JDialog(dia);
            jd.setTitle(getResource("progress"));
            jd.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent windowEvent) {
                    cp.shutdownNow();
                    jd.dispose();
                }

                @Override
                public void windowClosed(WindowEvent windowEvent) {
                    cp.shutdownNow();
                    jd.dispose();
                }
            });
            PACTuple pt=null;
            Future<PACTuple> res=null;
            try {
                TimeSeries data=CSVHandler.openImportDialog(dia);
                if(data==null){
                    JOptionPane.showMessageDialog(dia,
                            getResource("error_import_message"),
                            getResource("error_import_title"),JOptionPane.ERROR_MESSAGE);
                    caller.setEnabled(true);
                    return;
                }
                res = cp.run(data);
                JProgressBar jp=new JProgressBar();
                cp.registerProgressBar(jp);
                jd.add(jp);
                jd.pack();
                jd.setVisible(true);
                pt=res.get(10000, TimeUnit.SECONDS);
            } catch (InterruptedException|ExecutionException|IOException e) {
                ExceptionHandler.logException(e);
            }  catch (TimeoutException timeoutException) {
                cp.shutdownNow();
                try {
                    pt=res.get(30,TimeUnit.SECONDS);
                } catch (InterruptedException|ExecutionException|TimeoutException Exception) {
                    ExceptionHandler.logException(Exception);
                }
            }
            jd.dispose();
            caller.setEnabled(true);
            if(pt!=null) {
                probabilityTextField.setText(String.valueOf(pt.getValue().doubleValue()));
                uncertaintyTextField.setText(String.valueOf(pt.getUncertainty().doubleValue()));
                deltaTextField.setText(String.valueOf(pt.getDelta().doubleValue()));
            }
        }
    }
    static final class BoundHolderPanel extends JPanel{
        final Window par;
        public BoundHolderPanel(Window dia){
            par=dia;
            this.setLayout(new GridLayout(0,1));
            JButton add=new JButton("+");
            this.add(add);
            add.addActionListener(e-> BoundHolderPanel.this.add(new BoundPanel(BoundHolderPanel.this)));
        }
        public BoundHolderPanel(Window dia,Comparison[] comps,Rational[] bounds){
            this(dia);
            for(int i=0;i<comps.length;i++){
                add(new BoundPanel(this,bounds[i],comps[i]));
            }
        }
        public Comparison[] getComparison(){
            return getChildren().map(BoundPanel::getComparison).toArray(Comparison[]::new);
        }
        public Rational[] getBound(){
            return getChildren().map(BoundPanel::getBound).toArray(Rational[]::new);
        }
        private Stream<BoundPanel> getChildren(){
            return Arrays.stream(this.getComponents()).filter(c-> c instanceof BoundPanel).map(c->(BoundPanel) c);
        }
        @Override
        public void remove(Component comp) {
            super.remove(comp);
            updateUI();
            par.pack();
        }
        @Override
        public Component add(Component comp) {
            add(comp,getComponents().length-1);
            updateUI();
            par.pack();
            return comp;
        }
    }
    static final class BoundPanel extends JPanel{
        private final JComboBox<Comparison> ops;
        private final JTextField boundTextField;

        public BoundPanel(JComponent parent){
            this(parent,Rational.ONE,Comparison.EQUAL);
        }

        public BoundPanel(JComponent parent,Rational bound,Comparison value){

            Rational usedBound = bound;
            ops=new JComboBox<>(Comparison.values());
            if(value!=null)ops.setSelectedItem(value);
            if(usedBound ==null) usedBound =Rational.ONE;
            boundTextField =new JTextField(String.valueOf(usedBound.doubleValue()));

            JButton delete = new JButton("x");
            delete.addActionListener(e-> parent.remove(BoundPanel.this));

            this.setLayout(new GridBagLayout());
            GridBagConstraints c=new GridBagConstraints();
            c.weightx=0.1;
            c.fill=GridBagConstraints.HORIZONTAL;
            this.add(ops,c);
            c.weightx=0.9;
            this.add(boundTextField,c);
            c.weightx=0.1;
            this.add(delete,c);
        }
        public Comparison getComparison(){
            return (Comparison)ops.getSelectedItem();
        }
        public Rational getBound(){
            return RationalFromString.getRationalFromString(boundTextField.getText());
        }
    }
}
