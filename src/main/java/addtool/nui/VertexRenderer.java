package addtool.nui;

import addtool.add.model.Vertex;

import javax.swing.*;
import java.awt.*;

class VertexRenderer extends JLabel implements ListCellRenderer<Vertex> {
    public static final int MAX_STRING_LENGTH = 60;

    @Override
    public Component getListCellRendererComponent(JList<? extends Vertex> list, Vertex vertex, int index,
                                                  boolean isSelected, boolean cellHasFocus) {

        String code = vertex.toString();
        if (code.length() > MAX_STRING_LENGTH) {
            code = code.substring(0, MAX_STRING_LENGTH) + "[...]";
        }
        setText(code);

        return this;
    }
}
