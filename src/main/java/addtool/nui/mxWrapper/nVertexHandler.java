package addtool.nui.mxWrapper;

import com.mxgraph.swing.handler.mxVertexHandler;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;

import java.awt.event.MouseEvent;

/**
 * Overriding drag and drop to detect the {@link nGraph} being in use.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @see mxVertexHandler
 */
public class nVertexHandler extends mxVertexHandler {
    /**
     * Creates a new VertexHandler, based on superclass constructor
     * @param mxGraphComponent the component to handle vertices for
     * @param mxCellState the cell state
     */
    public nVertexHandler(mxGraphComponent mxGraphComponent, mxCellState mxCellState) {
        super(mxGraphComponent, mxCellState);
    }

    @Override
    public void  mouseDragged(MouseEvent e)
    {
        mxGraph graph=this.graphComponent.getGraph();
        if(graph instanceof nGraph){
            ((nGraph)graph).setInAction(true);
        }
        super.mouseDragged(e);
    }
    @Override
    public void mouseReleased(MouseEvent e)
    {
        super.mouseReleased(e);
        mxGraph graph=this.graphComponent.getGraph();
        if(graph instanceof nGraph){
            ((nGraph)graph).setInAction(false);
        }
    }
}
