package addtool.nui.mxWrapper;

import com.mxgraph.examples.swing.editor.EditorKeyboardHandler;
import com.mxgraph.swing.handler.mxKeyboardHandler;
import com.mxgraph.swing.mxGraphComponent;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Adds custom Keyboard interactions to {@link nGraphComponent}
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @see mxKeyboardHandler
 */
public class nKeyboardHandler extends EditorKeyboardHandler {
    private final ActionListener onEnter;
    private final mxGraphComponent component;

    /**
     * Creates new handler
     * @param mxGraphComponent the parent component
     * @param onEnter An actionListener when the enter key is hit.
     */
    public nKeyboardHandler(mxGraphComponent mxGraphComponent,ActionListener onEnter) {
        super(mxGraphComponent);
        this.onEnter = onEnter;
        this.component=mxGraphComponent;
    }

    @Override
    protected InputMap getInputMap(int var1) {
        InputMap map=super.getInputMap(var1);
        if(map!=null&&var1 == JComponent.WHEN_FOCUSED){
            map.put(KeyStroke.getKeyStroke("ENTER"), "edit");
            map.remove(KeyStroke.getKeyStroke("control S"));
            map.remove(KeyStroke.getKeyStroke("control O"));
            map.remove(KeyStroke.getKeyStroke("control N"));
        }
        return map;
    }
    @Override
    protected ActionMap createActionMap() {
        ActionMap var1 = super.createActionMap();
        var1.put("edit",new FoldAction());
        var1.put("undo", new HistoryAction(true));
        var1.put("redo", new HistoryAction(false));
        var1.remove("save");
        var1.remove("open");
        return var1;
    }
    @Override
    protected void installKeyboardActions(mxGraphComponent graphComponent){
        super.installKeyboardActions(graphComponent);
    }
    class FoldAction extends AbstractAction {
        static final String action = "edit";

        public FoldAction() {
            super(action);
        }

        @Override
        public void actionPerformed(ActionEvent var1) {
            onEnter.actionPerformed(var1);
        }

        @Override
        public FoldAction clone() throws CloneNotSupportedException {
            return (FoldAction) super.clone();
        }
    }
    @SuppressWarnings("ThrowInsideCatchBlockWhichIgnoresCaughtException")
    class HistoryAction extends AbstractAction
    {
        /**
         *
         */
        protected boolean undo;

        /**
         *
         */
        public HistoryAction(boolean undo)
        {
            this.undo = undo;
        }

        /**
         *
         */
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Automatically searches for the right component in the view structure.
            //Somehow swing does not acknowledge this correctly
            for(Component c:component.getParent().getComponents()){
                if (c instanceof nGraphComponent && c.hasFocus())
                {
                    if (undo)
                    {

                        ((nGraphComponent) c).getUndoManager().undo();
                    }
                    else
                    {
                        ((nGraphComponent) c).getUndoManager().redo();
                    }
                }
            }
        }

        @Override
        public HistoryAction clone() {
            try {
                HistoryAction clone = (HistoryAction) super.clone();
                clone.undo=this.undo;
                return clone;
            } catch (CloneNotSupportedException e) {
                throw new AssertionError();
            }
        }
    }
}
