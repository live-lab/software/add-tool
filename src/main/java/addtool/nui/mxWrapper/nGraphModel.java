package addtool.nui.mxWrapper;

import com.mxgraph.model.mxGraphModel;

/**
 * A wrapper class for the mxGraphModel
 * handles not tracking changes
 * @see mxGraphModel
 */
public class nGraphModel extends mxGraphModel {
    /**
     * empty constructor passing on to the super class constructor
     */
    public nGraphModel(){
        super();
    }

    private  boolean isTracking=true;

    /**
     * wraps the end update function in not trackign booleans if the param es set to false.
     * @param trackNext if false the last update is not tracked. Thus not adding to the undo manager
     */
    public void endUpdate(boolean trackNext){
        if(!trackNext){
            isTracking=false;
        }
        super.endUpdate();
        isTracking=true;
    }

    /**
     * If the model is currently tracked
     * @return true if changes are tracked, false otherwise
     */
    public boolean isTracking(){
        return isTracking;
    }
}
