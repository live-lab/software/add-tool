package addtool.nui.mxWrapper;

import addtool.nui.ContainerMenu;
import addtool.nui.DoubleClickListener;
import addtool.nui.panels.TabbedMultiGraphComponent;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.mxGraphLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.swing.handler.*;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.util.*;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxEdgeStyle;
import org.apache.commons.math3.util.Precision;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * A class to handle special needs of ADT in Graphs.
 * @see mxGraphComponent
 * @see nGraph
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public class nGraphComponent extends mxGraphComponent {
    DoubleClickListener doubleClickListener =null;
    final nKeyboardHandler keyboardHandler;
    final mxRubberband rubberBand;
    int sorting=JLabel.NORTH;//Currently NORTH,SOUTH,WEST allowed

    protected final mxUndoManager undoManager= new mxUndoManager();

    /**
     * Creates a new graph component
     * sets up the undo manager, change listener, handlers etc
     * @param graph the graph to contain
     */
    public nGraphComponent(nGraph graph){
        super(graph);
        setToolTips(true);
        keyboardHandler=new nKeyboardHandler(this,e-> editCell(getGraph().getSelectionCell()));
        rubberBand = new mxRubberband(this);

        graph.setResetViewOnRootChange(false);

        graph.getModel().addListener(mxEvent.CHANGE, changeTracker);
        // Adds the command history to the model and view
        graph.getModel().addListener(mxEvent.UNDO, undoHandler);
        graph.getView().addListener(mxEvent.UNDO, undoHandler);

        // Keeps the selection in sync with the command history
        mxEventSource.mxIEventListener undoHandler = (source, evt) -> {
            List<mxUndoableEdit.mxUndoableChange> changes = ((mxUndoableEdit) evt
                    .getProperty("edit")).getChanges();
            getGraph().setSelectionCells(getGraph()
                    .getSelectionCellsForChanges(changes));
        };

        undoManager.addListener(mxEvent.UNDO, undoHandler);
        undoManager.addListener(mxEvent.REDO, undoHandler);
        //Change accessible text on mouse change
        MouseMotionListener mousy=new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {}
            @Override
            public void mouseMoved(MouseEvent e) {
                mxCell cell=(mxCell) getCellAt(e.getX(),e.getY());
                if(cell!=null){
                    getAccessibleContext().setAccessibleName(getGraph().getLabel(cell));
                }else{
                    getAccessibleContext().setAccessibleName("");
                }
            }
        };
        this.getGraphControl().addMouseMotionListener(mousy);
    }
    private void editCell(Object cell){
        if(!(cell instanceof mxCell))return;
        if(getGraph().isSwimlane(cell)){
            graph.foldCells(!graph.isCellCollapsed(cell), false,
                    new Object[] { cell });
            return;
        }
        mxCell mxCell=(mxCell)cell;
        getGraph().editCell(mxCell,this);
        refresh();
    }
    @Override
    protected void installDoubleClickHandler(){
        doubleClickListener =new DoubleClickListener() {
            @Override
            public void one(MouseEvent e) {
                if(SwingUtilities.isRightMouseButton(e)){
                    mxCell cell=(mxCell)getCellAt(e.getX(),e.getY());
                    if(cell!=null){
                        ContainerMenu.openPopupMenu(cell,nGraphComponent.this);
                    }
                }
            }

            @Override
            public void two(MouseEvent e) {
                editCell( getCellAt(e.getX(),e.getY()));
            }
        };
        getGraphControl().addMouseListener(doubleClickListener);
    }
    @Override
    protected mxConnectionHandler createConnectionHandler()
    {
        return new nConnectionHandler(this);
    }
    @Override
    public nGraph getGraph(){
        return (nGraph)graph;
    }

    /**
     * Triggers Sorting of the displayed graph. Default is HierarchicalLayout SOUTH
     * @param after A runnable that can be executed after sorting
     */
    public void sort(Runnable after) {
        switch (sorting){
            case JLabel.SOUTH->sortBottomUp(after);
            case JLabel.NORTH->sortTopDown(after);
            default->sortDefault(after,sorting);
        }
    }

    /**
     * Reinstalls the keyboardactions
     */
    public void updateUIActions(){
        keyboardHandler.installKeyboardActions(this);
    }
    /**
     * Triggers Sorting of the displayed add. Default is HierarchicalLayout SOUTH
     * Is only called if more than one Vertex is set at the origin.
     * @param after a runnable that is called after sorting.
     */
    public void sortIfMissingLocations(Runnable after) {
        Object[] objects=getGraph().getChildCells(getGraph().getDefaultParent());
        boolean alreadyOrigin=false;
        for(Object o:objects){
            mxCell cell1=(mxCell)o;
            mxGeometry geo=cell1.getGeometry();
            if(Precision.equals(geo.getX(),0,0.005)&&Precision.equals(geo.getY(),0,0.005)){
                if(alreadyOrigin){
                    sort(after);
                    return;
                }
                alreadyOrigin=true;
            }
        }
        SwingUtilities.invokeLater(after);
    }

    /**
     * Sets automatic sorting direction
     * @param sorting an integer representing the sorting
     */
    public void setSorting(int sorting){
        this.sorting=sorting;
    }/**
     * Triggers Sorting of the displayed graph.
     * bottom up means deepest basic event at the bottom root at top
     * @param after A runnable that can be executed after sorting
     */
    public void sortBottomUp(Runnable after){
        sortDefault(after,
                JLabel.SOUTH,
                ()->getGraph().translateCells());
    }
    /**
     * Triggers Sorting of the displayed graph.
     * bottom up means deepest basic event at the top root at bottom
     * @param after A runnable that can be executed after sorting
     */
    public void sortTopDown(Runnable after){
        sortDefault(after,
                JLabel.NORTH);
    }
    /**
     * Triggers Sorting of the displayed add.
     * bottom up means deepest basic event at the bottom root at top
     * @param after A runnable that can be executed after sorting
     * @param orientation the sorting direction
     * @param todoPreEnd tasks to be executed before update is ended
     */
    public void sortDefault(Runnable after,int orientation,Runnable ... todoPreEnd){
        nGraph graph=getGraph();
        Object cell = graph.getDefaultParent();
        mxGraphLayout layout=new mxHierarchicalLayout(graph,orientation);
        graph.getModel().beginUpdate();
        try
        {
            layout.execute(cell);
        }
        finally
        {
            mxMorphing morph = new mxMorphing(this, 1,
                    1.2, 20);

            morph.addListener(mxEvent.DONE, (sender, evt) ->{
                Arrays.stream(todoPreEnd).forEach(Runnable::run);
                graph.getModel().endUpdate();
                graph.updateCells();
                SwingUtilities.invokeLater(after);
            } );

            morph.startAnimation();
        }
    }

    /**
     * Returns cell Handler for moving objects.
     * @param state
     *            Cell state for which a handler should be created.
     * @return Returns the handler to be used for the given cell state.
     */
    @Override
    public mxCellHandler createHandler(mxCellState state)
    {
        if (graph.getModel().isVertex(state.getCell()))
        {
            //return new mxVertexHandler(this, state);
            return new nVertexHandler(this, state);
        }
        if (graph.getModel().isEdge(state.getCell()))
        {
            mxEdgeStyle.mxEdgeStyleFunction style = graph.getView().getEdgeStyle(state,
                    null, null, null);

            if (graph.isLoop(state) || Objects.equals(style, mxEdgeStyle.ElbowConnector)
                    || Objects.equals(style, mxEdgeStyle.SideToSide)
                    || Objects.equals(style, mxEdgeStyle.TopToBottom))
            {
                return new mxElbowEdgeHandler(this, state);
            }

            //return new mxEdgeHandler(this, state);
            return new nEdgeHandler(this, state);
        }
        return new mxCellHandler(this, state);
    }
    protected final mxEventSource.mxIEventListener undoHandler = (source, evt)->{
        //This allows not tracking Changes made in a health-check or any other activity that should not be reverted
        if(!(graph.getModel() instanceof nGraphModel)||((nGraphModel) graph.getModel()).isTracking() ){
            undoManager.undoableEditHappened((mxUndoableEdit) evt
                    .getProperty("edit"));
        }
    };
    protected final mxEventSource.mxIEventListener changeTracker = (source, evt) -> setModified(true);

    protected boolean modified = false;

    /**
     * Sets the modified boolean to a value.
     * true represents the graph being modified after last save.
     * @param modified true if changes were made
     */
    public void setModified(boolean modified)
    {
        if(!((nGraphModel)getGraph().getModel()).isTracking()){
            return;
        }
        boolean oldValue = this.modified;
        this.modified = modified;

        firePropertyChange("modified", oldValue, modified);

        if (oldValue != modified)
        {
            if(getParent() instanceof JTabbedPane&&getParent().getParent() instanceof TabbedMultiGraphComponent){
                ((TabbedMultiGraphComponent) getParent().getParent()).setChanged(modified);
            }
        }
    }
    /**
     * Returns if the graph was modified
     * true represents the graph being modified after last save.
     * @return  true if changes were made
     */
    public boolean isModified(){
        return modified;
    }

    /**
     * Get current undo manager
     * @return the undomanager
     */
    public mxUndoManager getUndoManager()
    {
        return undoManager;
    }

    /**
     * Creates a new graphhandler
     * @return the graphhandler
     */
    @Override
    protected mxGraphHandler createGraphHandler()
    {
        return new nGraphHandler(this);
    }
}
