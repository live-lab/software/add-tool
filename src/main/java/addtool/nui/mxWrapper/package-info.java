/**
 * Package contains classes that wrap the mxGraph functionality to add/remove from it
 */
package addtool.nui.mxWrapper;