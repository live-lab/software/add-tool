package addtool.nui.mxWrapper;

import com.mxgraph.swing.handler.mxConnectionHandler;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

import java.awt.event.MouseEvent;

/**
 * Overriding drag and drop to detect the {@link nGraph} being in use.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @see mxConnectionHandler
 */
public class nConnectionHandler extends mxConnectionHandler {
    /**
     * creates new connection handler based on parent class
     * @param mxGraphComponent the parent graph component
     */
    public nConnectionHandler(mxGraphComponent mxGraphComponent) {
        super(mxGraphComponent);
    }
    @Override
    public void  mouseDragged(MouseEvent e)
    {
        mxGraph graph=this.graphComponent.getGraph();
        if(graph instanceof nGraph&&connectPreview.isActive()){
            ((nGraph)graph).setInAction(true);
        }
        super.mouseDragged(e);
    }
    @Override
    public void mouseReleased(MouseEvent e)
    {
        super.mouseReleased(e);
        mxGraph graph=this.graphComponent.getGraph();
        if(graph instanceof nGraph){
            ((nGraph)graph).setInAction(false);
        }
    }
}
