package addtool.nui.mxWrapper;

import addtool.dfd.*;
import addtool.dfd.Process;
import addtool.feedback.FeedbackTuple;
import addtool.nui.EntityEditPane;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.util.mxPoint;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * nGraph child class for DFD
 * @see DFD
 * @see nGraph
 */
public class nDFDGraph extends nGraph{
    final Map<mxCell, Entity> cell2Entity;

    /**
     * Creates new object
     */
    public nDFDGraph(){
        cell2Entity=new HashMap<>();
    }
    @Override
    public nGraph copyGraph() {
        nDFDGraph output=new nDFDGraph();
        output.loadCell2Entity(cell2Entity.values());
        return output;
    }

    @Override
    public void editCell(mxCell cell, nGraphComponent parent) {
        Entity neu= EntityEditPane.editEntity(cell2Entity.get(cell), SwingUtilities.getWindowAncestor(parent));
        cell2Entity.replace(cell,neu);
        style.renderCell(this,getFont(),cell,neu);
    }

    /**
     * Returns Entity for a given ID
     * @param ID the id
     * @return the entity
     */
    public Entity getByID(String ID){
        return cell2Entity.values().stream().filter(e->e.getID().equals(ID)).findFirst().orElse(null);
    }

    /**
     * Get Cell for given entity
     * @param e the entity
     * @return the cell or null if no matching cell is present
     */
    public mxCell getCell(Entity e){
        return  cell2Entity.entrySet().stream().
                filter(entry->entry.getValue().equals(e)).
                map(Map.Entry::getKey).
                findFirst().orElse(null);
    }

    @Override
    public mxCell createEmptyVertex() {
        return insertVertex(new Process());
    }

    /**
     * Generates new cell based on an entity
     * @param op the relevant entity
     * @return the cell based on it.
     */
    public mxCell insertVertex(Entity op){
        getModel().beginUpdate();
        mxCell v1;
        try
        {
            Rectangle r=op.getBoundaries();
            if(op instanceof Line){
                Line line=(Line)op;
                List<mxPoint> mxPoints=line.getPoints().stream().map(p->new mxPoint(p.x,p.y)).collect(Collectors.toList());

                System.out.println(line.getPoints().toString()+ '|' +mxPoints+ '|' +op.getClass());
                mxGeometry geometry = new mxGeometry(0, 0, line.getWidth(),line.getHeigth());

                geometry.setPoints(mxPoints);
                if(mxPoints.size()>1){
                    geometry.setTerminalPoint(mxPoints.get(0), true);
                    geometry.setTerminalPoint(mxPoints.get(mxPoints.size()-1), false);
                }
                //geometry.setRelative(true);

                v1 = new mxCell(op.getName(), geometry,op.getShape());//"edgeStyle=elbowEdgeStyle;elbow=horizontal;orthogonal=0;"
                v1.setEdge(true);

                if(line.hasSource()){
                    Entity source=getByID(line.getSource());
                    if(source!=null){
                        System.out.println("Setting Source");
                        v1.setSource(getCell(source));
                    }
                }
                if(line.hasTarget()){
                    Entity target=getByID(line.getTarget());
                    if(target!=null){
                        System.out.println("Setting Target");
                        v1.setTarget(getCell(target));
                    }
                }
                this.addCell(v1,this.getDefaultParent());

            }else{
                v1=(mxCell)super.insertVertex(this.getDefaultParent(),null,op.getName(),r.x,r.y,r.width,r.height,"shape="+op.getShape()+ ';');
            }
            cell2Entity.put(v1,op);
            style.renderCell(this,getFont(),v1,op);
        }
        finally
        {
            getModel().endUpdate();
        }
        return v1;
    }

    @Override
    public void updateCells() {
        //TODO implement
    }

    @Override
    public Object doHealthCheck() {
        return doHealthCheck(new ArrayList<>());
    }

    @Override
    public Object doHealthCheck(ArrayList<FeedbackTuple> output) {
        DFD dfd=new DFD();
        //TODO Meaningful export
        Object[] cells=this.getChildCells(this.getDefaultParent());
        for(Object cell:cells){
            if(cell instanceof mxCell){
                mxCell mxCell=(com.mxgraph.model.mxCell) cell;
                if(!cell2Entity.containsKey(cell)){
                    if(mxCell.isEdge()){
                        DataFlow flow=new DataFlow();
                        cell2Entity.put(mxCell,flow);
                    }
                }
                Entity e=cell2Entity.get(mxCell);
                //Update graphical data from
                mxGeometry geometry=mxCell.getGeometry();
                e.setBoundaries(geometry.getRectangle());
                if(e instanceof Line){
                    Line l=(Line)e;
                    List <Point> points=new ArrayList<>();
                    if(geometry.getPoints()!=null){
                        points.addAll(geometry.getPoints().stream().map(mxPoint::getPoint).collect(Collectors.toList()));
                    }
                    if(geometry.getSourcePoint()!=null){
                        points.add(0,geometry.getSourcePoint().getPoint());
                    }
                    if(geometry.getTargetPoint()!=null){
                        points.add(geometry.getTargetPoint().getPoint());
                    }
                    l.replacePoints(points);
                    mxCell source = (com.mxgraph.model.mxCell) mxCell.getSource();
                    if(source !=null){
                        l.setSource(cell2Entity.get(source).getID());
                    }
                    mxCell target = (com.mxgraph.model.mxCell) mxCell.getTarget();
                    if(target !=null){
                        l.setTarget(cell2Entity.get(target).getID());
                    }

                }
                dfd.addEntity(e);
            }
        }
        updateCells();
        return dfd;
    }

    @Override
    public boolean isEmpty() {
        return cell2Entity.isEmpty();
    }

    @Override
    public void load(Object model) {
        if(model instanceof DFD){
            //Add not lines first to allow finding starting points of line
            ((DFD) model).getEntities().stream().filter(e->!(e instanceof Line)).forEach(this::insertVertex);
            ((DFD) model).getEntities().stream().filter(e->(e instanceof Line)).forEach(this::insertVertex);
        }
    }
    private void loadCell2Entity(Collection<Entity> entities){
        entities.forEach(this::insertVertex);
        this.updateCells();
    }
}
