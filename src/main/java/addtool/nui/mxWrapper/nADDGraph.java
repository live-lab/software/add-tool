package addtool.nui.mxWrapper;

import addtool.add.model.*;
import addtool.feedback.FeedbackGenerator;
import addtool.feedback.FeedbackTuple;
import addtool.helpers.preferences.BooleanPreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.nui.VertexEditPane;
import addtool.timeseries.PACTuple;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.nui.GraphTransformer.graph2Model;
import static addtool.nui.panels.AnalysisPanel.PREF_PAC;

/**
 * extension of the mxGraph classes specifically for ADDs
 * @see nGraph
 * @see mxGraph
 * @see ADD
 */
public class nADDGraph extends nGraph{
    /**
     * key for the vertex id which is stored in the style of the cells.
     */
    public static final String VERTEX_ID="vertexId";
    final Map<mxCell, Vertex> cell2Vertex=new HashMap<>();
    transient Map<String, PACTuple> id2Cost=new HashMap<>();
    transient Map<String, PACTuple> id2Delay=new HashMap<>();

    /**
     * Sets the cost map after analysis
     * Is needed for tooltips.
     * @param id2Cost a cost map for the vertices in the graph
     */
    public void setId2Cost(Map<String,PACTuple> id2Cost){
        this.id2Cost=new HashMap<>(id2Cost);
    }

    /**
     * Sets the cost map after analysis
     * Is needed for tooltips.
     * @param vertex2Cost a cost map for the vertices in the graph
     */
    public void setVertex2Cost(Map<Vertex, PACTuple> vertex2Cost){
        setId2Cost(vertex2Cost.entrySet().stream().
                map(e->Map.entry(e.getKey(),e.getValue()==null?PACTuple.NIL_TUPLE:e.getValue())).
                collect(Collectors.toMap(e->e.getKey().getId(), Map.Entry::getValue)));
    }
    /**
     * Sets the delay map after analysis
     * Is needed for tooltips.
     * @param delay a delay map for the vertices in the graph
     */
    public void setVertex2Delay(Map<Vertex, PACTuple> delay) {
        setId2Delay(delay.entrySet().stream().
                map(e->Map.entry(e.getKey(),e.getValue()==null?PACTuple.NIL_TUPLE:e.getValue())).
                collect(Collectors.toMap(e->e.getKey().getId(), Map.Entry::getValue)));
    }
    /**
     * Sets the delay map after analysis
     * Is needed for tooltips.
     * @param id2Delay a delay map for the vertices in the graph
     */
    public void setId2Delay(Map<String,PACTuple> id2Delay){
        this.id2Delay=new HashMap<>(id2Delay);
    }

    /**
     * Invokes an editing process for a given vertex
     * @param cell the cell to edit
     */
    @Override
    public void editCell(mxCell cell, nGraphComponent parent){
        if(cell.isVertex()){
            Vertex vp = getVertex(cell,true);
            mxGeometry geometry = cell.getGeometry();
            Point relative=new Point(Double.valueOf(geometry.getX()+geometry.getWidth()).intValue(),
                    Double.valueOf(geometry.getY()).intValue());
            SwingUtilities.convertPointToScreen(relative,parent);
            Vertex v = VertexEditPane.editVertex(vp, SwingUtilities.getWindowAncestor(parent),relative);
            if(v!=null){
                put(cell, v);

                setCellName(v, cell);
                updateNaryEdges(cell,v);
            }
        }
    }

    @Override
    public String getToolTipForCell(Object cell)
    {
        if(!(cell instanceof mxCell))return null;
        Vertex v=getVertex((mxCell)cell,true);
        if(v==null)return null;
        final String spacer="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        final String pacMask="%s: %f<br>%s%s: %f<br>%s%s: %f<br>";
        final String generalMask="%s: %f<br>";
        boolean showPac=((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference(PREF_PAC)).getValue();
        String out="<html>";
        if(((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference("show_Probability")).getValue()) {
            try {
                if(showPac){
                    out += String.format(pacMask,
                            getResource("disrupt_probability"), v.getDisruptionProb().doubleValue(),
                            spacer, getResource("uncertainty"), v.getUncertainty().doubleValue(),
                            spacer, getResource("prob_delta"), v.getProbabilityDelta().doubleValue());
                }else{
                    out += String.format(generalMask,
                            getResource("disrupt_probability"), v.getDisruptionProb().doubleValue());
                }
            } catch (RuntimeException ex) {
                //No logging. Will simply output "no active data"
                //ExceptionHandler.logException(ex);
            }
        }
        if(id2Cost!=null&&id2Cost.get(v.getId())!=null&&
                ((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference("show_Cost")).getValue()){
            PACTuple cost=id2Cost.get(v.getId());

            if(showPac){
                out+=String.format(pacMask,
                        getResource("cost"),cost.getValue().doubleValue(),
                        spacer,getResource("uncertainty"),cost.getUncertainty().doubleValue(),
                        spacer,getResource("prob_delta"),cost.getDelta().doubleValue());
            }else{
                out += String.format(generalMask,
                        getResource("cost"),cost.getValue().doubleValue());
            }
        }
        if(id2Delay!=null&&id2Delay.get(v.getId())!=null&&
            ((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference("show_Delay")).getValue()){
            PACTuple delay=id2Delay.get(v.getId());
            if(showPac){
                out+=String.format(pacMask,
                        getResource("delay"),delay.getValue().doubleValue(),
                        spacer,getResource("uncertainty"),delay.getUncertainty().doubleValue(),
                        spacer,getResource("prob_delta"),delay.getDelta().doubleValue());
            }else{
                out+=String.format(generalMask,
                        getResource("delay"),delay.getValue().doubleValue());
            }
        }
        if(out.length()<10){
            //If no data is active
            out+=getResource("no_domain_active");
        }
        return out+"</html>";
    }

    @Override
    public mxCell createEmptyVertex(){
        return insertVertex(new And("And","0"), //The Vertex ID will be overwritten by the graph
                200,20,nGraph.VERTEX_WIDTH,nGraph.VERTEX_HEIGHT);
    }

    /**
     * Adds a new mxCell based on a passed vertex
     * Note: The size will be changed on next rendering
     * @param op The vertex that should be graphically represented
     * @param x X position
     * @param y Y Position
     * @param width Width of the vertex
     * @param height Height of the vertex
     * @return the new cell
     */
    public mxCell insertVertex(Vertex op, int x, int y, int width,int height){
        getModel().beginUpdate();
        mxCell v1;
        try
        {
            v1=(mxCell)super.insertVertex(this.getDefaultParent(),null,op.getOperatorName(),x,y,width,height);
            op.setID(v1.getId());
            setCellName(op,v1);
            put(v1,op);
        }
        finally
        {
            getModel().endUpdate();
        }
        return v1;
    }

    /**
     * Returns Vertex linked to a given cell. If there is no connection yet, the connection ist build.
     * This can happen on copy&paste actions
     * @param cell A cell from the nADDGraph
     * @param readPredecessors if true the predecessor connections are read {@link nADDGraph#readPredecessors(mxCell, Vertex)}
     * @return the vertex stored in the map or a new one if none existed until then
     */
    public Vertex getVertex(mxCell cell,boolean readPredecessors){
        Vertex v=cell2Vertex.get(cell);
        if(v!=null&&readPredecessors){
            readPredecessors(cell,v);
        }
        if(cell!=null&&cell.isVertex()&&v==null){
            v=generateVertexFor(cell);
        }
        return v;
    }

    /**
     * Returns a vertex from the graph for a given ID
     * @param id the id in question
     * @return the vertex if found, null otherwise
     */
    public Vertex getVertexByID(String id){
        return cell2Vertex.values().stream().filter(v->v.getId().equals(id)).findFirst().orElse(null);
    }

    /**
     * iterates active cells and returns the next available id
     * @return the next unused id.
     */
    public String getNextID(){
        return String.valueOf(cell2Vertex.values().stream().mapToInt(v->{
            try{
                return Integer.parseInt(v.getId());
            }catch (NumberFormatException ex){
                return 0;
            }
        }).max().orElse(0)+1);
    }

    /**
     * gets a corresponding vertex to a cell.
     * If there exists none yet, it is generated.
     * The information is either from a vertex id or type in the style string.
     * can also copy vertices from other currently active graphs.
     * @param cell the cell to find a vertex for
     * @return the vertex mapped to the cell.
     */
    public Vertex generateVertexFor(mxCell cell){
        if(cell2Vertex.containsKey(cell)){
            return cell2Vertex.get(cell);
        }
        //Extract source ID
        String id=(String) getCellStyle(cell).get(VERTEX_ID);
        Vertex original=null;
        if(id.contains("@")){//Also search other graphs
            int graphCode=Integer.parseInt(id.split("@")[1]);
            id=id.split("@")[0];
            nGraph byHash=nGraph.getByHash(graphCode);
            if(byHash instanceof nADDGraph){
                original=((nADDGraph) byHash).getVertexByID(id);
            }
        }else {
            original = getVertexByID(id);
        }
        //Find original vertex
        Vertex copied;
        if(original==null){
            copied=ADDBuilder.buildOperatorForKey(id,cell.getId(),id);
            copied.setName(copied.getOperatorName());
        }else{
            copied=original.copy();
        }
        copied.setID(getNextID());
        setCellStyles(VERTEX_ID,copied.getId()+ '@' +this.hashCode(),new Object[]{cell});
        put(cell,copied);
        setCellName(copied,cell);
        readPredecessors(cell,copied);
        return copied;
    }

    /**
     * Gets cell for a given vertex
     * @param v the vertex to find a cell for
     * @return the cell for the vertex, null if none exists
     */
    public mxCell getCell(Vertex v){
        return cell2Vertex.entrySet().stream().filter(e->e.getValue().equals(v)).map(Map.Entry::getKey).findFirst().orElse(null);
    }

    /**
     * Gets cell for a given vertex using its ID number
     * @param v the vertex to find a cell for
     * @return the cell for the vertex, null if none exists
     */
    public mxCell getCellById(Vertex v){
        return getCellById(v.getId());
    }
    /**
     * Gets cell for a given vertex using its ID number
     * @param id the vertex id to find a cell for
     * @return the cell for the vertex, null if none exists
     */
    public mxCell getCellById(String id){
        if(id==null){
            return null;
        }
        return cell2Vertex.entrySet().stream().
                filter(e->e.getValue().getId().equals(id)).
                map(Map.Entry::getKey).
                findFirst().orElse(null);
    }

    /**
     * Update or add connection between cell and vertex
     * Will also write vertex id to cell style
     * @param cell the cell 
     * @param v the vertex
     * @return the last vertex mapped to the cell if one was in the map
     */
    public Vertex put(mxCell cell,Vertex v){
        setCellStyles(VERTEX_ID,v.getId()+ '@' +this.hashCode(),new Object[]{cell});
        return cell2Vertex.put(cell,v);
    }
    @Override
    public boolean isEmpty(){
        //There may be still remains in the cell2Vertex map, but we care about actually visible cells
        return getChildCells(getDefaultParent()).length==0;
    }

    private void readPredecessors(mxCell cell,@NotNull Vertex v){
        if(v instanceof NaryOp)((NaryOp)v).clearPredecessors();
        if(v instanceof UnaryOp)((UnaryOp)v).clearPredecessor();
        Arrays.stream(getEdges(cell))
                .filter(Objects::nonNull)
                .map(c -> (mxCell) c)
                .filter(c -> c.getTarget().equals(cell))
                .sorted(nGraph.cellOrder).forEach(c -> {
                    mxICell source = c.getSource();
                    if (source instanceof mxCell) {
                        v.addPredecessor(cell2Vertex.get(source));
                    }
                });
    }

    /**
     * Reads the predecessor order of sequential operators
     * @see nADDGraph#readPredecessors(mxCell, Vertex) 
     */
    public void readPredecessorsForSequential(){
        cell2Vertex.entrySet().stream().
                filter(e->(e.getValue() instanceof NaryOp&&((NaryOp)e.getValue()).isSequential())).
                forEach(e-> readPredecessors(e.getKey(),e.getValue()));
    }

    /**
     * This method creates the nice little numbers on the edges of sequential operators.
     * @param cell Cell in Graph
     * @param v Vertex
     */
    public void updateNaryEdges(mxCell cell,Vertex v){
        //graph.setLabelsVisible(true);
        java.util.List<Vertex> predecessors = v.getPredecessors();
        for(int i=0;i<predecessors.size();i++){
            int ind=i;//to make it effective final
            Vertex pre=predecessors.get(i);
            cell2Vertex.entrySet().stream().
                    filter(e->e.getValue().equals(pre)).
                    forEach(e->
                            Arrays.stream(getEdges(cell)).
                                    map(c->(mxCell)c).
                                    filter(c->c.getTarget().equals(cell)&&c.getSource().equals(e.getKey())).
                                    forEach(c->{
                                        if(v instanceof NaryOp && ((NaryOp)v).isSequential()){
                                            setCellStyles(mxConstants.STYLE_NOEDGESTYLE, "0", java.util.List.of(c).toArray());
                                            setCellStyles(mxConstants.STYLE_NOLABEL, "0", java.util.List.of(c).toArray());
                                            setCellStyles(mxConstants.STYLE_LABEL_BACKGROUNDCOLOR,
                                                    mxUtils.hexString(Color.WHITE),
                                                    List.of(c).toArray());
                                            c.setValue(String.valueOf(ind + 1));
                                        }else{
                                            c.setValue("");
                                        }
                                    })
                    );
        }
    }

    /**
     * inserts a new vertex for a vertex
     * @param root usually the default root of the graph
     * @param v a vertex 
     * @return the new cell
     * @see nGraph#getDefaultParent()
     * @see nADDGraph#insertVertex(Vertex, int, int, int, int)
     */
    public mxCell insertVertex(Object root,Vertex v){
        getModel().beginUpdate();
        mxCell v1 = (mxCell)insertVertex(root,
                v.getId(),
                v.getOperatorName(),
                v.getLocation().x,
                v.getLocation().y,
                VERTEX_WIDTH,
                VERTEX_HEIGHT,
                style.getStyleFor(v));
        setCellName(v,v1);
        put(v1,v);
        getModel().endUpdate();
        return v1;
    }

    /**
     * Renders the cell new based on the vertex
     * @param v the vertex to render the cell by
     * @param cell the cell to render
     */
    public void setCellName(Vertex v, mxCell cell){
        style.renderCell(this,getFont(),cell,v);
    }
    @Override
    public void load(Object model) {
        if(model instanceof ADD){
            ADD add=(ADD)model;
            load(add.getId2Vertex().values(),add.getComments());
        }
    }

    /**
     * Imports a bunch of vertices and creates their eddges.
     * Also allows comments, which will be displayed in the feedback area
     * @param vertices the vertices to import
     * @param comments comments to save in the graph
     */
    public void load(Collection<Vertex> vertices, Collection<String> comments){
        this.comments.addAll(comments);
        //graph.removeCells();
        getModel().beginUpdate();
        //Set Vertices
        Object root=getDefaultParent();
        vertices.forEach(v->insertVertex(root,v));
        //Set Edges
        cell2Vertex.forEach((c,v)->{
            List<Vertex> successors=v.getSuccessors();
            if(v instanceof Reset){
                successors.addAll(((Reset)v).getToReset());
            }
            if(v instanceof Trigger){
                successors.addAll(((Trigger)v).getToTrigger());
            }
            cell2Vertex.forEach((c2,v2)->{
                if(successors.contains(v2)) {
                    insertEdge(root, null, "", c, c2);//,"edgeStyle=elbowEdgeStyle;elbow=horizontal;orthogonal=0;"+ "entryX=0;entryY=0;entryPerimeter=1;")
                }
            });
        });
        //Add Sequential Markings
        cell2Vertex.entrySet().stream().
                filter(e->(e.getValue() instanceof NaryOp&&((NaryOp)e.getValue()).isSequential())).
                forEach(e-> updateNaryEdges(e.getKey(),e.getValue()));
        getModel().endUpdate();
        this.updateCells();
    }
    @Override
    public ADD doHealthCheck(){
        return doHealthCheck(new ArrayList<>());
    }
    @Override
    public ADD doHealthCheck(ArrayList<FeedbackTuple> output){
        return graph2Model(this,output);
    }
    /**
     * Analyses current model and displays problems in the UI.
     * @param model A model generated by {@link nGraph#doHealthCheck()}
     * @param feedbackGenerator a feedback generator to generate feedback
     * @return A list of Strings to display in some way
     */
    public List<FeedbackTuple> colorFeedback(ADD model, FeedbackGenerator feedbackGenerator){
        Collection<FeedbackTuple> messages;
        if(feedbackGenerator!=null) {
            messages= Stream.of(feedbackGenerator.getFeedback(model),additionalFeedback).
                    flatMap(Collection::stream).
                    collect(Collectors.toList());
        }else {
            messages=additionalFeedback;
        }
        List<FeedbackTuple> lines=new ArrayList<>();
        for(FeedbackTuple f:messages){
            lines.add(f);
            mxCell c1=getCellById(f.getV1());
            if(c1==null){
                continue;
            }
            if(f.isEdge()){
                mxCell c2=getCellById(f.getV2());
                Object[] connections=Arrays.stream(getAllEdges(new mxCell[]{c1,c2}))
                        .map(o->(mxCell)o)
                        .filter(c->c.isEdge()&&c.getSource().equals(c1)&&c.getTarget().equals(c2))
                        .toArray();
                setCellStyles(mxConstants.STYLE_STROKECOLOR,
                        mxUtils.hexString(f.getType().getColor()),connections);
            }else{
                setCellStyles(mxConstants.STYLE_STROKECOLOR,
                        mxUtils.hexString(f.getType().getColor()),new mxCell[]{c1});
            }
        }
        return lines;
    }
    @Override
    public void updateCells(){
        cell2Vertex.forEach((key, value) -> setCellName(value, key));
    }

    /**
     * Exports add for a subsection of the model based in the given root
     * @param target the root of the add part to consider
     * @return the add or null if invalid
     */
    public ADD transformComponent(mxCell target) {
        List<mxICell> edges=new ArrayList<>();
        List<mxICell> cells=new ArrayList<>();
        for(int i=0;i<target.getChildCount();i++){
            mxICell c=target.getChildAt(i);
            if(c.isEdge()){
                edges.add(c);
            }else{
                cells.add(c);
            }
        }
        return graph2Model(this,cells.toArray(Object[]::new),edges.toArray(Object[]::new),new ArrayList<>());
    }
    @Override
    public nADDGraph copyGraph(){
        nADDGraph output=new nADDGraph();
        output.load(cell2Vertex.values(),getComments());
        return output;
    }

}
