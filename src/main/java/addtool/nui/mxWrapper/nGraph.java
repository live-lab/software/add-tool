package addtool.nui.mxWrapper;

import addtool.helpers.preferences.IntPreference;
import addtool.helpers.preferences.PreferenceManager;
import com.mxgraph.analysis.mxAnalysisGraph;
import com.mxgraph.analysis.mxGraphStructure;
import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;
import addtool.feedback.FeedbackTuple;
import addtool.helpers.Constants;
import addtool.nui.style.BasicStyle;
import addtool.nui.style.CellStyle;
import org.w3c.dom.Document;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.regex.Pattern;


/**
 * Custom graph to represent a model in a {@link nGraphComponent}.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @see mxGraph
 */
public abstract class nGraph extends mxGraph  implements java.io.Serializable{
    /**
     * Minimal width of a vertex in px
     */
    public static final int VERTEX_WIDTH =80;
    /**
     * Minimal height of a vertex in px
     */
    public static final int VERTEX_HEIGHT =40;

    /**
     * Padding in px
     */
    public static final int PAD=5;

    /**
     * Maximum line width in chars inside a cell label
     */
    public static final int MAX_LINE_LENGTH =15;
    /**
     * Deserialized graphs may not work. This method writes values to new one
     * @param graph The source nGraph
     * @return the new nGraph
     */
    public static nGraph copyGraph(nGraph graph){
        return graph.copyGraph();
    }
    private transient Font font;
    private static final int MIN_FONT =12;
    private static final int MAX_FONT =72;
    /**
     * Preference Key for Textsize in Cell labels. Min 12, Max 72;
     */
    public static final String PREFS_GRAPH_FONT="vertexFontSize";
    static {
        IntPreference graphFontSize=new IntPreference(PREFS_GRAPH_FONT,PREFS_GRAPH_FONT,"layout",String.valueOf(MIN_FONT+4));
        graphFontSize.setBounds(MIN_FONT,MAX_FONT);
        PreferenceManager.getPreferenceManager().putPreferences(graphFontSize);
    }
    protected transient Collection<FeedbackTuple> additionalFeedback;
    private transient boolean inAction;
    protected final List<String> comments;
    protected final transient CellStyle style=new BasicStyle();
    @SuppressWarnings("StaticCollection")
    private static final List<nGraph> instances=new ArrayList<>();
    protected static nGraph getByHash(int hashCode){
        return instances.stream().filter(g->g.hashCode()==hashCode).findFirst().orElse(null);
    }

    /**
     * generate a new graph. Prepares preference listening etc.
     */
    public nGraph(){
        super(new nGraphModel());//
        PreferenceManager.getPreferenceManager().getPreference(PREFS_GRAPH_FONT).addActionListener(e-> changeFontSize());
        instances.add(this);
        comments=new ArrayList<>();
        font=new Font("Verdana", Font.PLAIN,
                (Integer)PreferenceManager.getPreferenceManager().getPreference(PREFS_GRAPH_FONT).getValue());

        mxCodec codec = new mxCodec();
        Document doc = mxUtils.loadDocument(Objects.requireNonNull(nGraph.class
                .getResource(Constants.getResourceFolder() + "style.xml")).toString());

        if (doc != null)
        {
            codec.decode(doc.getDocumentElement(),
                    getStylesheet());
            refresh();
        }
        additionalFeedback=new ArrayList<>();
        inAction=false;
        this.setCellsSelectable(true);
        addImageBundle(style.getImageBundle());
        setCellsResizable(false);
    }

    /**
     * Returns additional comments, if any were set, for the graph
     * @return a list of strings passed on to the graph
     */
    public List<String> getComments() {
        return new ArrayList<>(comments);
    }
    /**
     * Order for sequential Predecessors.
     */
    public static final Comparator<mxCell> cellOrder = (cell, t1) -> {
        int state = 0;
        if (cell.getValue() == null || cell.getValue().equals("")) {
            state = 99;
        }
        if (t1.getValue() == null || t1.getValue().equals("")) {
            if (state == 99) state = -1;
            else state = -99;
        }
        if (state == 0){
            //state = Integer.parseInt(cell.getValue().toString()) - Integer.parseInt(t1.getValue().toString());
            state=cell.getValue().toString().compareTo(t1.getValue().toString());
        }
        return state;
    };

    /**
     * Returns a Point with the lowest x and y value below zero in this graph.
     * @return the lowest Point in all Positions
     */
    public mxPoint getMinPoint(){
        double miny=0;
        double minx=0;
        Object[] cells=this.getChildCells(this.getDefaultParent());
        for(Object o:cells){
            mxCell cell1=(mxCell)o;
            mxGeometry geo=cell1.getGeometry();
            if(miny>geo.getY())miny=geo.getY();
            if(minx>geo.getX())minx=geo.getX();
        }

        return new mxPoint(minx,miny);
    }

    /**
     * get the right lower border point of the cells displayed
     * @return a point with the highest x,y coordinates used in the graph
     */
    public mxPoint getMaxPoint(){
        double miny=0;
        double minx=0;
        Object[] cells=this.getChildCells(this.getDefaultParent());
        for(Object o:cells){
            mxCell cell1=(mxCell)o;
            mxGeometry geo=cell1.getGeometry();
            if(miny<geo.getY()+geo.getHeight())miny=geo.getY()+geo.getHeight();
            if(minx<geo.getX()+geo.getWidth())minx=geo.getX()+geo.getWidth();
        }

        return new mxPoint(minx,miny);
    }

    /**
     * Generates a copy of the graph
     * @return the copied graph
     */
    public abstract nGraph copyGraph();
    /**
     * Invokes an editing process for a given vertex
     * @param cell the cell to edit
     * @param parent the parent graph component for the dialog
     */
    public abstract void editCell(mxCell cell, nGraphComponent parent);

    /**
     * Translates the Graph by a Point. Positive Values translate to the right or down, negative the other way.
     * @param min The smallest Point which will serve as new origin
     */
    public void translateCells(mxPoint min) {
        Object[] cells=this.getChildCells(this.getDefaultParent());
        for(Object o:cells){
            this.translateCell(o,-min.getX(),-min.getY());
        }
    }
    /**
     * Translates the Graph by the minimum Point. Positive Values translate to the right or down, negative the other way.
     */
    public void translateCells() {
        translateCells(getMinPoint());
    }

    /**
     * Adds an empty vertex with standard data
     * @return a new cell
     */
    public abstract mxCell createEmptyVertex();

    /**
     * Sets the feedback displayed additionally to normal feedback
     * Can be used for static data
     * @param toSet a collection of tuples
     * @see FeedbackTuple
     */
    public void setAdditionalFeedback(Collection<FeedbackTuple> toSet){
        additionalFeedback=new ArrayList<>(toSet);
    }

    /**
     * Returns id a given edge is drawn dashed
     * @param mxCell the edge in question
     * @return true if it is a dashed line
     */
    public boolean isDashed(mxCell mxCell){
        String val=(String)getCellStyle(mxCell).get(mxConstants.STYLE_DASHED);
        if(val==null){
            toggleCellStyle(mxConstants.STYLE_DASHED,true,mxCell);
            return false;
        }
        return val.equals("1");
    }

    /**
     * makes an edge dashed or not
     * @param mxCell the edge to change
     * @param dashed true if it should be dashed, false if it should be solid
     */
    public void setDashed(mxCell mxCell,boolean dashed){
        boolean isDashed=isDashed(mxCell);
        if (dashed != isDashed) {
            toggleCellStyle(mxConstants.STYLE_DASHED, true, mxCell);
        }
    }

    /**
     * Converts a list of strings into a line wrapped list of strings based on maximum line width.
     * @param l the lines to display
     * @return the new lines to display
     */
    public static List<String> preCut(List<String> l){
        List<String> separators=List.of("_"," ","-","+");
        List<String> lines=new ArrayList<>();

        for(String s:l){
            if(s ==null){
                continue;
            }
            if(s.length()> MAX_LINE_LENGTH){
                boolean set=false;
                for(String sp:separators){
                   if(s.contains(sp)){
                       String[] splits=s.split(Pattern.quote(sp));
                       StringBuilder put= new StringBuilder();
                       for(String split:splits){
                           if(put.length()+split.length()> MAX_LINE_LENGTH){
                               lines.add(put.toString());
                               put = new StringBuilder(split);
                           }else{
                               put.append((put.length() == 0) ? "" : sp).append(split);
                           }
                           if(put.length()>= MAX_LINE_LENGTH){
                              lines.add(put.toString());
                              put = new StringBuilder();
                           }
                       }
                       if(put.length()>0){
                           lines.add(put.toString());
                       }
                       set=true;
                       break;
                   }
                }
                if(!set){
                    lines.add(s);
                }
            }else{
                lines.add(s);
            }
        }
        return lines;
    }

    /**
     * Updates label for a cell
     * @param cell the cell to update
     * @param state the cell state for the cell
     * @param lines the lines to display
     */
    public void updateLabel(mxCell cell,mxCellState state,List<String> lines){
        updateLabel(cell,state,lines,0,0);
    }
    /**
     * Updates label for a cell
     * @param cell the cell to update
     * @param state the cell state for the cell
     * @param lines the lines to display
     * @param addHeight padding in height
     * @param addWidth padding in width
     */
    public void updateLabel(mxCell cell,mxCellState state,List<String> lines,int addHeight,int addWidth)
    {
        int pad=PAD *  10;
        if(state!=null) {
            String shape = (String) state.getStyle().get(mxConstants.STYLE_SHAPE);
            if(mxConstants.SHAPE_ELLIPSE.equals(shape)){
                lines.add(0,"");
                lines.add("");
                pad = PAD * 8;
            }
        }
        FontMetrics fm=mxUtils.getFontMetrics(font);

        int maxWidth=lines.stream().mapToInt(fm::stringWidth).max().orElse(VERTEX_WIDTH)+pad;
        int maxHeight=lines.size()*fm.getHeight()+PAD*3;

        mxGeometry geom=cell.getGeometry();
        geom.setWidth(Math.max(VERTEX_WIDTH,maxWidth+addWidth));
        geom.setHeight(Math.max(VERTEX_HEIGHT,maxHeight+addHeight));
        cell.setGeometry(geom);
    }

    /**
     * Returns current Font Size in pt
     * @return the font size in pt
     */
    public int getFontSize(){
        return font.getSize();
    }

    /**
     * Returns current Font
     * @return the currently used font
     */
    public Font getFont(){
        return font;
    }
    private void changeFontSize(){
        getModel().beginUpdate();
        int size=(Integer)PreferenceManager.getPreferenceManager().getPreference(PREFS_GRAPH_FONT).getValue();
        font=new Font(font.getFontName(), font.isBold()?Font.BOLD:Font.PLAIN, size);
        updateCells();
        getModel().endUpdate();
        refresh();
    }

    /**
     * Updates the whole UI of the cells.
     */
    public abstract void updateCells();
    /**
     * This method checks the displayed diagram for formal errors and compiles it to an ADD.
     * Usage in Save-Action and the recurrent health-check
     * @return an ADD if result is ok, null otherwise
     */
    public abstract Object doHealthCheck();
    /**
     * This method checks the displayed diagram for formal errors and compiles it to an ADD.
     * Usage in Save-Action and the recurrent health-check
     * @param output an optional list to add resulting feedback in healthcheck
     * @return an ADD if result is ok, null otherwise
     */
    public abstract Object doHealthCheck(ArrayList<FeedbackTuple> output);

    /**
     * Checks if the graph is currently modified by the user
     * @return true if the user is currently changing something in the graph
     */
    public boolean isInAction() {
        return inAction;
    }

    /**
     * Sets if the graph is currently modified
     * @param inAction true if the graph is modified and should not be updated by other routines
     */
    public void setInAction(boolean inAction){
        this.inAction=inAction;
    }

    /**
     * Updates the shape of a cell
     * @param cell the cell to change shape
     * @param shape a string representing the shape
     */

    public void changeShape(mxCell cell, String shape){
        //Warning suppressed as library needs a Hashtable
        @SuppressWarnings("UseOfObsoleteCollectionType") Hashtable<Object,mxCellState> states=getView().getStates();
        mxCellState state=states.get(cell);
        if(state==null){
            state= getView().createState(cell);
        }

        Map<String,Object> style=state.getStyle();

        style.replace(mxConstants.STYLE_SHAPE,shape);

        state.setStyle(style);
        states.replace(cell,state);
        getView().setStates(states);
    }

    /**
     * Searches for a component or sub tree based in a given root cell.
     * This is done by removing the edges from the root ans checking if two separate graphs result
     * @param cell the root cell
     * @return an array with the content of the component, null if no component is found or there are more connecting edges
     */
    public mxCell[] checkForCollapsing(mxCell cell){
        //Get Number of Components of g at the beginning.
        Object root=getDefaultParent();
        Object[] allCells=getChildCells(root);
        Object[] allEdges=getAllEdges(allCells);
        mxAnalysisGraph analysis=new mxAnalysisGraph();
        analysis.setGraph(this);
        Object[][] oldComponents=mxGraphStructure.getGraphComponents(analysis);
        int oldComponentCount=oldComponents.length;
        //Remove all edges from the graph, that start in cell
        Object[] edges=this.getEdges(cell);
        List<mxCell> removed=new ArrayList<>();
        for(Object e:edges){
            mxCell c=(mxCell)e;
            if(c.getSource().equals(cell)){
                removed.add(c);
                this.removeCells(new Object[]{c});
            }
        }

        Object[][] newComponents=mxGraphStructure.getGraphComponents(analysis);

        //Add the removed edges again
        removed.forEach(this::addCell);

        //Check for correct component
        if (newComponents.length > oldComponentCount) {
            for (Object[] component : newComponents) {
                if (Arrays.asList(component).contains(cell)) {
                    return Arrays.copyOf(component, component.length, mxCell[].class);
                }
            }
        }
        return null;
    }

    /**
     * Creates a Container cell to house a graph component
     * @param component the cells that should be contained in the swimlane
     * @return the Container cell
     */
    public mxCell prepareContainerForComponent(mxCell[] component){
        double minX=Double.MAX_VALUE;
        double minY=Double.MAX_VALUE;
        double maxX=-Double.MAX_VALUE;
        double maxY=-Double.MAX_VALUE;
        for(mxCell cell:component){
            mxGeometry geo=cell.getGeometry();
            minX=Math.min(minX,geo.getX());
            maxX=Math.max(maxX,geo.getX()+geo.getWidth());
            minY=Math.min(minY,geo.getY());
            maxY=Math.max(maxY,geo.getY()+geo.getHeight());
        }
        mxCell cell = new mxCell("Container", new mxGeometry(minX-PAD*2, minY-PAD*4, maxX-minX+PAD*4, maxY-minY+PAD*10),
                mxConstants.SHAPE_SWIMLANE);
        cell.setVertex(true);
        addCell(cell);
        return cell;
    }

    /**
     * Adds cells to a container
     * @param container the parent container
     * @param component the cells to add
     */
    public void addToContainer(mxCell container,mxCell[] component){
        mxGeometry geo=container.getGeometry();
        for(mxCell c:component){
            container.insert(c);
            translateCell(c,-geo.getX()+PAD*2,-geo.getY()+PAD*4);
        }
        Object [] edges=getAllEdges(component);
        for(Object o:edges){
            mxCell edge=(mxCell)o;
            if(edge.getSource().getParent().equals(container)&&
                    edge.getTarget().getParent().equals(container)){
                container.insert(edge);
                translateCell(edge,-geo.getX()+PAD*2,-geo.getY()+PAD*4);
            }
        }
    }

    /**
     * Removes the children from a certain container
     * @param container the container to empty
     */
    public void removeFromContainer(mxCell container){
        mxCell root=(mxCell)getDefaultParent();
        //List<mxICell> content=new ArrayList<>();
        mxGeometry geo=container.getGeometry();
        while (container.getChildCount()>0){
            mxICell child= container.getChildAt(0);
            if(child!=null){
                root.insert(child);
                //content.add(child);
                translateCell(child, geo.getX() -PAD*2,geo.getY()-PAD*4);
            }
        }

        removeCells(new Object[]{container});
    }

    /**
     * Checks if there are active cells in the graph
     * @return true if no cells are displayed
     */
    public abstract boolean isEmpty();

    /**
     * Loads a given model
     * depending on the subclass it has to be of a certain class
     * @param model the model to load
     */
    public abstract void load(Object model);

    /**
     * removes the graph from the list of active instances
     * this should be done for memory reasons
     * tracking active instances is important to allow cross copying
     */
    public void destroy() {
        instances.remove(this);
    }
}
