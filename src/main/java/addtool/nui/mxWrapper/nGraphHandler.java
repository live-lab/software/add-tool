package addtool.nui.mxWrapper;

import com.mxgraph.swing.handler.mxGraphHandler;
import com.mxgraph.swing.mxGraphComponent;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.MouseEvent;

/**
 * A wrapper for the GraphHandler
 * currently does nothing new. Used for debugging
 * @see mxGraphHandler
 */
public class nGraphHandler extends mxGraphHandler {
    /**
     * Creates a new handler
     * @param graphComponent the related graph component
     */
    public nGraphHandler(mxGraphComponent graphComponent) {
        super(graphComponent);
    }
    @Override
    public void drop(DropTargetDropEvent e)
    {
        super.drop(e);
    }
    @Override
    public void dragEnter(DropTargetDragEvent e)
    {
        super.dragEnter(e);
    }
    @Override
    public void mouseReleased(MouseEvent e)
    {
        super.mouseReleased(e);

    }
}
