package addtool.nui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *  Class to provide DoubleClickActions to User.
 * @author Florian Dorfhuber
 */
public abstract class DoubleClickListener
  implements MouseListener
{
  private final AtomicInteger clickCount;
  /**
   * Long to set maximum time between two clicks to recognise them as  a DoubleClick.
   */
  static final long wait = 300L;
  final ThreadPoolExecutor runner;
  
    /**
     *  Creates a DoubleClickListener.
     */
  public DoubleClickListener() {
    clickCount = new AtomicInteger(0);
    runner = new ThreadPoolExecutor(1, 1,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>());
  }
  
  @Override
  public void mouseClicked(MouseEvent e) {
      clickEvent(e.getClickCount(), e);
  }
  



  @Override
  public void mousePressed(MouseEvent e) {}
  


  @Override
  public void mouseReleased(MouseEvent e) {}
  


  @Override
  public void mouseEntered(MouseEvent e) {}
  


  @Override
  public void mouseExited(MouseEvent e) {}
  

  /**
   * Method called by click on Component.
   * @param eCont Click count of component
   * @param e MouseEvent called
   */
  private void clickEvent(int eCont, MouseEvent e)
  {
      //Save biggest amount of Clicks
    if (clickCount.get() < eCont) {
      clickCount.set(eCont);
    }
    //Start Thread just once
    if (clickCount.get() > 1){
        clickCount.set(2);

        if(runner.getActiveCount()>0){
          return;
        }
    }

    runner.submit(() -> {
      try {
        //Waits max time between clicks
        Thread.sleep(wait);
      } catch (InterruptedException ex) {
        clickCount.set(0);
      }
      //Call Method depending on click number
      int count=clickCount.getAndSet(0);
      if (count == 1) {
        one(e);
      } else if (count == 2) {
        two(e);
      }
    });
  }
  
    /**
     *  Method which is called when just one click is performed.
     * @param paramMouseEvent Event that is triggered by User
     */
    public abstract void one(MouseEvent paramMouseEvent);
  
    /**
     * Method which is called when double click is performed.
     * @param paramMouseEvent Event that is triggered by User
     */
    public abstract void two(MouseEvent paramMouseEvent);
}
