package addtool.nui;

import addtool.helpers.STRIDE;
import addtool.helpers.Threat;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.nui.VertexEditPane.addStandardPanel;

public final class ThreatEditPane {

    private ThreatEditPane() {
    }

    public static Threat editThreat(Threat threat, Window parent) {
        //0.2=x of panel is entity stuff 1-x is Threats
        JDialog dia = new JDialog(parent);
        dia.setTitle(getResource("edit_threat"));
        dia.setModal(true);
        if(parent!=null){
            dia.setIconImages(parent.getIconImages());
        }
        JPanel content=new JPanel();
        content.setLayout(new GridBagLayout());
        GridBagConstraints c=new GridBagConstraints();
        c.gridx=0;c.gridy=GridBagConstraints.RELATIVE;
        c.weightx=0.9;c.fill=GridBagConstraints.HORIZONTAL;

        content.add(new JLabel(getResource("name")),c);
        JTextField titleField=new JTextField(threat.getTitle());
        content.add(titleField,c);

        content.add(new JLabel(getResource("stride_type")),c);
        JComboBox<STRIDE> strideComboBox=new JComboBox<>(STRIDE.values());
        strideComboBox.setSelectedItem(threat.getStride());
        content.add(strideComboBox,c);

        JPanel radios=new JPanel();
        radios.setLayout(new GridLayout(1,0));

        radios.add(new JLabel(getResource("status")));
        JPanel statusPanel=new JPanel();
        statusPanel.setLayout(new GridLayout(0,1));
        JRadioButton openButton=new JRadioButton(getResource("open"));
        statusPanel.add(openButton);
        JRadioButton mitigatedButton=new JRadioButton(getResource("mitigated"));
        statusPanel.add(mitigatedButton);
        ButtonGroup statusGroup=new ButtonGroup();
        statusGroup.add(openButton);
        statusGroup.add(mitigatedButton);
        if(threat.isOpen()){
            openButton.setSelected(true);
        }else{
            mitigatedButton.setSelected(true);
        }
        radios.add(statusPanel);

        radios.add(new JLabel(getResource("severity")));
        JPanel severityPanel=new JPanel();
        severityPanel.setLayout(new GridLayout(0,1));
        JRadioButton highButton=new JRadioButton(Threat.Severity.HIGH.toString());
        severityPanel.add(highButton);
        JRadioButton mediumButton=new JRadioButton(Threat.Severity.MEDIUM.toString());
        severityPanel.add(mediumButton);
        JRadioButton lowButton=new JRadioButton(Threat.Severity.LOW.toString());
        severityPanel.add(lowButton);
        ButtonGroup severityGroup=new ButtonGroup();
        severityGroup.add(highButton);
        severityGroup.add(mediumButton);
        severityGroup.add(lowButton);
        switch (threat.getSeverity()){
            case HIGH -> highButton.setSelected(true);
            case MEDIUM -> mediumButton.setSelected(true);
            case LOW -> lowButton.setSelected(true);
        }
        radios.add(severityPanel);

        content.add(radios,c);

        content.add(new JLabel(getResource("description")),c);
        JTextArea descriptionArea=new JTextArea();
        descriptionArea.setText(threat.getDescription());
        content.add(descriptionArea,c);

        content.add(new JLabel(getResource("mitigations")),c);
        JTextArea mitigationArea=new JTextArea();
        mitigationArea.setText(threat.getMitigations());
        content.add(mitigationArea,c);


        AtomicBoolean save=new AtomicBoolean(false);
        JButton saveButton=(JButton)UIHelper.buildToolButton("save","save",e->{
            save.set(true);
            dia.dispose();
        },false);
        JRootPane rootPane = SwingUtilities.getRootPane(dia);
        rootPane.setDefaultButton(saveButton);

        AbstractButton discard=UIHelper.buildToolButton("cancel","cancel",e->{
            save.set(false);
            dia.dispose();
        },false);
        addStandardPanel(content,discard,saveButton,c,c);
        dia.add(content);
        dia.setMinimumSize(new Dimension(400,300));
        dia.pack();
        dia.setVisible(true);
        if(save.get()){
            //Read values
            threat.setTitle(titleField.getText());
            threat.setStride((STRIDE)strideComboBox.getSelectedItem());
            threat.setOpen(openButton.isSelected());
            threat.setMitigations(mitigationArea.getText());
            threat.setDescription(descriptionArea.getText());
            threat.setSeverity(highButton.isSelected()?
                    Threat.Severity.HIGH:
                    (mediumButton.isSelected()?
                            Threat.Severity.MEDIUM:
                            Threat.Severity.LOW));
        }
        return threat;
    }
}
