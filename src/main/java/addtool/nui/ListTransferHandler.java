package addtool.nui;

import addtool.add.model.Vertex;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;

/**
 * Creates Drag & Drop support for reordering vertex predecessors.
 * A {@link TransferHandler} for enabling D&D support for {@link JDragDropList}
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public class ListTransferHandler extends TransferHandler {
    //private Vertex[] selected = null;

    @Override
    public boolean canImport(TransferSupport info) {
        return info.isDataFlavorSupported(VertexTransferable.Vertex_DATA_FLAVOR);
    }

    @Override
    protected Transferable createTransferable(JComponent c) {
        if (!(c instanceof JList)) {
            return null;
        }
        JList<Vertex> list = (JList<Vertex>) c;
        int[] indices = list.getSelectedIndices();
        return Arrays.stream(indices)
                .mapToObj(list.getModel()::getElementAt)
                .map(VertexTransferable::new)
                .findAny().orElse(null);
    }

    @Override
    public int getSourceActions(JComponent c) {
        return TransferHandler.MOVE;
    }

    @Override
    public boolean importData(TransferSupport info) {
        if (!info.isDrop()) {
            return false;
        }

        DefaultListModel<Vertex> listModel =(DefaultListModel<Vertex>) ((JList<Vertex>) info.getComponent()).getModel();
        int index = ((JList.DropLocation) info.getDropLocation()).getIndex();

        Vertex data;
        try {
            data = (Vertex) info.getTransferable().getTransferData(VertexTransferable.Vertex_DATA_FLAVOR);
        } catch (UnsupportedFlavorException|IOException e) {
            return false;
        }

        Vertex[] values = {data};

        for (Vertex val : values) {
            if (listModel.indexOf(val) < index) {
                index--;
            }
            listModel.removeElement(val);
            listModel.add(index, val);
        }
        return true;
    }

    /**
     * Handles vertex drag-and-drop in JList
     * @see Vertex
     * @see Transferable
     */
    public static class VertexTransferable implements Transferable {
        /**
         * Vertex Dataflavor
         */
        public static final DataFlavor Vertex_DATA_FLAVOR = new DataFlavor(Vertex.class, "add_tool/Vertex");
        private final Vertex vertex;

        /**
         * Create new Transferable based on the vertex
         * @param vertex the vertex to transfer
         */
        public VertexTransferable(Vertex vertex) {
            this.vertex = vertex;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            return new DataFlavor[]{Vertex_DATA_FLAVOR};
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return flavor.equals(Vertex_DATA_FLAVOR);
        }

        @Override
        public Object getTransferData(DataFlavor flavor) {
            return vertex;
        }
    }
}
