package addtool.nui.panels;

import addtool.helpers.Constants;
import addtool.nui.UIHelper;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Component to optionally display a label with an icon and an animation
 * Animations are stored in gif format
 * All are referenced by their path
 */
public class IconLabel extends JPanel {
    private final JButton left;
    private final JLabel right;

    private JLabel animation;
    private JLabel noAnimation;
    private final JPanel wrapper;

    /**
     * Creates a new IconLabel with an Icon and the text
     * @param pathToIcon the path for the icon
     * @param label the label text. Must be the final text. There will be no call of getResources
     */
    public IconLabel(String pathToIcon,String label){
        this(pathToIcon,label,"");
    }
    /**
     * Creates a new IconLabel with an Icon and the text
     * @param pathToIcon the path for the icon
     * @param label the label text. Must be the final text. There will be no call of getResources
     * @param pathToAnimation A path to an animation gif
     */
    public IconLabel(String pathToIcon,String label,String pathToAnimation){
        this.setLayout(new BorderLayout());
        left=UIHelper.getLabelLikeButton("",null);
        UIHelper.updateIcon(pathToIcon,left);

        this.add(left,BorderLayout.LINE_START);
        right=new JLabel(label);
        this.add(right,BorderLayout.CENTER);

        wrapper=new JPanel();
        try {
            URL animationUrl = IconLabel.class.getResource(pathToAnimation);
            URL defaultUrl=IconLabel.class.getResource(Constants.getResourceFolder()+getResource("button_run_big"));
            if(animationUrl!=null&&defaultUrl!=null){
                ImageIcon icon = new ImageIcon(animationUrl);
                animation = new JLabel(icon);
                BufferedImage empty=new BufferedImage(icon.getIconWidth(),icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);

                Graphics2D g2d = empty.createGraphics();
                g2d.setPaint(Color.BLACK);
                BufferedImage defaultImage= ImageIO.read(defaultUrl);
                g2d.drawImage(defaultImage,
                        (empty.getWidth()-defaultImage.getWidth())/2,
                        (empty.getHeight()-defaultImage.getHeight())/2,
                        this);
                g2d.dispose();
                noAnimation=new JLabel(new ImageIcon(empty ));
                wrapper.setBorder(BorderFactory.createLoweredBevelBorder());
            }else{
                animation=new JLabel();
                noAnimation=new JLabel();
            }
        } catch (IllegalArgumentException | IOException e) {
            animation=new JLabel();
            noAnimation=new JLabel();
        }
        wrapper.add(noAnimation);
        this.add(wrapper,BorderLayout.LINE_END);
        //animation.setVisible(false);
        wrapper.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                showAnimation();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                hideAnimation();
            }
        });
        this.setBorder(BorderFactory.createLoweredBevelBorder());
    }

    /**
     * Update the icon on the left side.
     * @param path path to the new icon
     */
    public void setIcon(String path){
        UIHelper.updateIcon(path,left);
    }
    /**
     * Update the icon on the left side.
     * @param icon the new icon
     */
    public void setIcon(Icon icon){
        left.setIcon(icon);
    }
    /**
     * Update the text in the middle/right
     * @param text final text.
     */
    public void setText(String text){
        right.setText(text);
    }
    /**
     * Update the tooltip Text in the middle/right
     * @param text tooltip text for the middle
     */
    @Override
    public void setToolTipText(String text){
        right.setToolTipText(text);
    }

    /**
     * Hides animation and displays the play button again
     */
    public void hideAnimation(){
        wrapper.remove(animation);
        wrapper.add(noAnimation);
        updateUI();
    }
    /**
     * Show animation in the right panel if one was set.
     */
    public void showAnimation(){
        wrapper.add(animation);
        wrapper.remove(noAnimation);
        updateUI();
    }
}
