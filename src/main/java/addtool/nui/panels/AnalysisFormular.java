package addtool.nui.panels;

import addtool.add.model.ADD;
import addtool.analysisonadd.PrintType;
import addtool.analysisonadd.PrintableVisitor;
import addtool.dot2add.RationalFromString;
import addtool.helpers.preferences.BooleanPreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.learning.TrinaryFunction;
import addtool.nui.DoubleClickListener;
import addtool.timeseries.PACTuple;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.nui.panels.AnalysisPanel.PREF_PAC;

/**
 * This is used to show Strings generated from ADDs.
 * It also allows for input by entering values only for Basic Events
 * Basic Events are detected by a BE in the textline
 * Extends HTMLFormular
 * @author Florian Dorfhuber
 * @see HTMLFormular
 */
public class AnalysisFormular extends HTMLFormular{
    /**
     * Helper method to replace \t in a string with html safe spaces
     * @param s input text
     * @return text with safe spaces instead of \t
     */
    public static String tabToSpace(String s){
        return s.replaceAll("\t","&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
    }

    /**
     * Helper method to change non html Tags with html tags
     * Replaces \t with five save spaces, \n with linebreak and wraps in html tag
     * @param data A String to transform. Preferably not already html
     * @return The data with html ready tags
     */
    public static String transformOutputToHtml(String data){
        return data.lines().map(AnalysisFormular::tabToSpace)
                .collect(Collectors.joining("<br>","<html>","</html>"));
    }

    /**
     * Helper method to transform input Data to a html form
     * Will add text input fields to all lines containing BE and of a specific pattern
     * Then will add a submit/cancel button.
     * @param data A string generated probably by an ADD
     * @return The String converted to html and with form
     * @see AnalysisFormular#transformLine(String)
     */
    public static String transformOutputToForm(String data){
        return data.lines().map(s->{
                    if(s.contains("BE")){
                        return transformLine(s);
                    }
                    return s;
                })//style="font-size: 40px;"
                .map(AnalysisFormular::tabToSpace)
                .collect(Collectors.joining("<br>","<html><form>","<br>" +
                        String.format("<input type='reset' value='%s'><input type='submit' value='%s'></form></html>",
                                getResource("cancel"),getResource("submit"))));
    }

    /**
     * Transforms a single line into a form element.
     * Expects the line to contain BE and be of pattern
     * ID *: Operator * Value: [value]
     * optional followed by:
     * , delta: [value], epsilon: [value]
     * @param s a line of text
     * @return the line as html form element
     * @see AnalysisFormular#transformOutputToForm(String) 
     */
    private static String transformLine(String s) {
        try{
            String outputString=s;
            String id=outputString.substring(0,outputString.indexOf(' ')).trim();
            String[] blocks=outputString.split(":");

            int prePos=outputString.length();
            for(int i=blocks.length-2;i>=1;i--){
                String domain=blocks[i].substring(blocks[i].lastIndexOf(' ')+1);
                String value=blocks[i+1].substring(blocks[i+1].indexOf(' ')+1,
                        i==blocks.length-2?
                                blocks[i+1].length():
                                blocks[i+1].lastIndexOf(','));
                int valPos=prePos-value.length();
                outputString=outputString.substring(0,valPos)+
                        String.format("<input type=\"text\" id=\"%s\" name=\"%s\" value=\"%s\" size=\"%d\">",id+ '_' +domain,id+ '_' +domain,value,5)
                        +outputString.substring(valPos+value.length());
                prePos-=blocks[i].length()+1;
            }
            return outputString;
        }catch (RuntimeException ex){
            return s;
        }
    }

    /**
     * A list of listeners to inform of changes in the form.
     * These Functions will be called with the id of the vertex and the new PACTuple for the specific value
     */
    private final List<BiConsumer<String,PACTuple>> valueListener;
    /**
     * Printing method for the ADD. Will be used on notification of change.
     * Will not be called if editing is active to prevent overriding.
     */
    final TrinaryFunction<PrintableVisitor,ADD,PrintType,String> generator;
    /**
     * The analysis method used on ADD
     * @see addtool.analysisonadd.CostHeuristic
     * @see addtool.analysisonadd.DelayHeuristic
     * @see addtool.analysisonadd.ProbabilityHeuristic
     */
    final PrintableVisitor visitor;

    /**
     * The domain name to act like a key if there are changes needed on the panels
     */
    final String domainName;
    /**
     * If the panel is in editing mode. Meaning a user can fill in values via the html form.
     */
    private boolean editMode;
    /**
     * The current ADD. Can be null if the current canvas model is invalid
     */
    private @Nullable ADD current;

    /**
     * The standard constructor.
     * @param domainName A key to identify the different panels
     * @param generator A string output method
     * @param visitor A domain analysis method to display with the generator and a ADD
     */
    public AnalysisFormular(String domainName,TrinaryFunction<PrintableVisitor,ADD,PrintType,String> generator,PrintableVisitor visitor){
        this.generator=generator;
        this.domainName=domainName;
        this.visitor=visitor;
        this.editMode=false;
        valueListener=new ArrayList<>();

        putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
        setEditable(false);

        addPropertyChangeListener("data", evt -> {
            Map<String, String> result = getResult();
            Map<String, PACTuple> forEvent=new HashMap<>();
            //Read value to add:
            result.forEach((k,v)-> {
                String[] splits = k.split("_");
                String id = splits[0];
                if (!forEvent.containsKey(id)) {
                    forEvent.put(id, PACTuple.SURE_TUPLE);
                }
                String value = splits[1];
                switch (value){
                    case "ε"-> forEvent.replace(id,forEvent.get(id).setUncertainty(RationalFromString.getRationalFromStringLocale(v)));
                    case "ẟ"-> forEvent.replace(id,forEvent.get(id).setDelta(RationalFromString.getRationalFromStringLocale(v)));
                    default -> forEvent.replace(id,forEvent.get(id).setValue(RationalFromString.getRationalFromStringLocale(v)));
                }
            });
            editMode=false;//Allows acceptance of new data
            update(current);//Reset view
            valueListener.forEach(forEvent::forEach);
        });
        addMouseListener(new DoubleClickListener() {
            @Override
            public void one(MouseEvent paramMouseEvent) {
            }

            @Override
            public void two(MouseEvent paramMouseEvent) {
                edit();
            }
        });
    }

    /**
     *
     * @return the domain name
     * @see AnalysisFormular#domainName
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Starts the edit mode.
     * @see AnalysisFormular#switchEdit()
     */
    public void edit(){
        if(!editMode&&current!=null){
            setText(transformOutputToForm(generator.apply(visitor,current, getPrintType())));
            editMode=true;
        }
    }

    /**
     * Switches the edit mode depending on the current state
     * @see AnalysisFormular#edit()
     * @see AnalysisFormular#resetForm()
     */
    public void switchEdit(){
        if(!editMode&&current!=null){
            edit();
        }else{
            resetForm();
        }
    }

    /**
     * Adds a new listener to be notified on input data saving
     * @param listener A listener
     */
    public void addValueListener(BiConsumer<String,PACTuple> listener){
        valueListener.add(listener);
    }

    /**
     * Removes a listener
     * @param listener the listener to be removed
     * @return true if it was found and removed
     * @see List#remove(Object)
     */
    public boolean removeListener(BiConsumer<String,PACTuple> listener){
        return valueListener.remove(listener);
    }

    /**
     * Switches editing back to non edit mode.
     * Looses all input data not saved
     * Saveing is done with the save button
     */
    @Override
    protected void resetForm() {
        super.resetForm();
        editMode=false;
        update(current);
    }

    /**
     * Updates the displayed values to the new model.
     * Will not do anything if the Panel is in editing mode.
     * @param model A valid ADD to be printed by the specified method
     */
    public void update(ADD model){
        if(!editMode){
            if(generator!=null&&model!=null){
                setText(transformOutputToHtml(generator.apply(visitor,model,getPrintType())));
                current=model;
            }else{
                current=null;
                setText("");
            }
        }
    }

    /**
     * Returns the current PrintType setting in the tool.
     * This is read from a boolean preference "usePAC".
     * It is boolean as only RECURRENT and PAC_RECURRENT are an option
     * @return the Printtype to be used
     * @see PrintType
     */
    public static PrintType getPrintType(){
        return (((BooleanPreference) PreferenceManager.getPreferenceManager().getPreference(PREF_PAC)).getValue()?
                        PrintType.PAC_RECURRENT:PrintType.RECURRENT);
    }

}
