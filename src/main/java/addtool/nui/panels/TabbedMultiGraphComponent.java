package addtool.nui.panels;

import addtool.add.model.ADD;
import addtool.nui.UIHelper;
import addtool.nui.mxWrapper.nADDGraph;
import com.mxgraph.swing.mxGraphComponent;
import addtool.feedback.FeedbackTuple;
import addtool.nui.mxWrapper.nGraph;
import addtool.nui.mxWrapper.nGraphComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * This class handles multiple {@link nGraphComponent} objects and makes it possible to switch between them.
 * Layout is organised in tabs
 */
public class TabbedMultiGraphComponent extends MultiGraphComponent implements TabParent {
    final AbstractButton neu;
    private final List<ActionListener> onTabClose=new ArrayList<>();

    /**
     * Creates a new object with a single tab with empty canvas and an add button
     */
    public TabbedMultiGraphComponent(){
        super();

        cont=new JTabbedPane();
        ((JTabbedPane)cont).addChangeListener(e -> {
            JTabbedPane pane = (JTabbedPane) e.getSource();
            if(pane.getSelectedComponent() instanceof mxGraphComponent){
                current=contentPanels.get(pane.getSelectedIndex());
            }else if(current!=null){
                int i=pane.indexOfComponent(current);
                if(i!=-1)pane.setSelectedComponent(current);
            }
            triggerListeners();
        });
        this.add(cont,BorderLayout.CENTER);
        neu=UIHelper.getLabelLikeButton("button_add","new",e-> create(1,true));
        //this.setBackground(Color.gray);
        create(1);
        ((JTabbedPane) cont).addTab(getResource("do_not_click"),new JLabel());
        ((JTabbedPane) cont).setTabComponentAt(1,neu);
    }

    @Override
    public void setLabel(String lb) {
        int pos=((JTabbedPane) cont).getSelectedIndex();
        labels.remove(pos);
        labels.add(pos,lb);
        revalidateTitles();
    }

    /**
     * Changes the status to modified for the current tab. Meaning adding a star to the tab label and notifying the graphcomponent
     * @param changed true if the model was changed since last save action, false if not
     */
    public void setChanged(boolean changed){
        int pos=((JTabbedPane) cont).getSelectedIndex();
        ((TabComponent)((JTabbedPane) cont).getTabComponentAt(pos)).setChanged(changed);
        current.setModified(changed);
    }
    @Override
    public String getLabel(){
        int pos=((JTabbedPane) cont).getSelectedIndex();
        if(pos<labels.size()){
            return labels.get(pos);
        }
        return null;
    }

    @Override
    public void setLabels(List<String> labels){
        if(labels!=null&&labels.size()==contentPanels.size()) {
            this.labels = new ArrayList<>(labels);
            revalidateTitles();
        }
    }

    /**
     * Adds a new listener for tab closing events
     * @param l the listener
     */
    public void addTabCloseListener(ActionListener l){
        onTabClose.add(l);
    }
    /**
     * Removes a listener for tab closing events
     * @param l the listener
     */
    public void removeTabCloseListener(ActionListener l){
        onTabClose.remove(l);
    }

    @Override
    public void removeTab(int pos){
        if(pos<contentPanels.size()&&pos>=0){
            onTabClose.forEach(l->l.actionPerformed(new ActionEvent(this,0,"closed")));
            int cur=((JTabbedPane)cont).getSelectedIndex();
            contentPanels.get(pos).getGraph().destroy();
            contentPanels.remove(pos);

            ((JTabbedPane)cont).removeTabAt(pos);

            if(labels.size()>pos){
                labels.remove(pos);
            }

            if(cur==pos&& !contentPanels.isEmpty()){
                //System.out.println(pos+"|"+cur);
                cur=(cur==0?0:(cur-1));
                ((JTabbedPane)cont).setSelectedIndex(cur);
                current=contentPanels.get(cur);
            }
        }
        if(contentPanels.isEmpty()){
            create(1);
        }
    }

    @Override
    public void removeTab(Component comp) {
        removeTab(((JTabbedPane)cont).indexOfTabComponent(comp));
    }
    @Override
    public void setSelectedIndex(int pos){
        ((JTabbedPane)cont).setSelectedIndex(pos);
    }

    @Override
    public void setSelected(Component comp) {
        setSelectedIndex(((JTabbedPane)cont).indexOfTabComponent(comp));
    }

    private void revalidateTitles(){
        for(int i=0;i<labels.size()&&i<((JTabbedPane)cont).getTabCount();i++){
            if(((JTabbedPane)cont).getTabComponentAt(i) instanceof TabComponent){
                TabComponent tb=(TabComponent)((JTabbedPane)cont).getTabComponentAt(i);
                if(tb!=null){
                    tb.setTitle(labels.get(i));
                    tb.setToolTipText(labels.get(i));
                }
            }
        }
    }

    /**
     * Creates a certain amount of Panels.
     * @param size The amount of panels to generate
     */
    @Override
    public void create(int size) {
        create(size,false);
    }
    private void create(int size,boolean askType){
        for(int i=0;i<size;i++){
            int k=1;
            String pre="new";
            String lb;
            while (labels.contains(lb=pre+k)){
                k++;
            }
            labels.add(lb);
            nGraph graph;
            /*if(askType){
                String[] options={"ADD","DFD"};
                int result=0;
                OptionPane.showOptionDialog(SwingUtilities.getWindowAncestor(this),
                        "Which type of graph do you want",
                        "Type selection",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[0]);
                switch (result){
                    case 1: graph=new nDFDGraph(); break;
                    case 0: default: graph=new nADDGraph();break;
                }
                graph=new nADDGraph();
            }else{*/
                graph=new nADDGraph();
            //}
            addGraph(graph,lb);
        }
        ((JTabbedPane)cont).setSelectedIndex(contentPanels.size()-1);
    }

    @Override
    public void addGraph(nGraph graph, String label) {
        nGraphComponent drawer=new nGraphComponent(nGraph.copyGraph(graph));
        drawer.setGridVisible(this.isGridShown());
        drawer.getViewport().setOpaque(true);
        drawer.getViewport().setBackground(Color.WHITE);
        drawer.setSorting(this.getSorting());
        //drawer.setBackground(Color.BLUE);
        contentPanels.add(drawer);
        int pos=contentPanels.size()-1;
        labels.add(label);
        ((JTabbedPane)cont).insertTab(label,null,drawer,null,pos);
        ((JTabbedPane) cont).setTabComponentAt(pos,new TabComponent(label,this));

    }

    @Override
    public void display(int index) {
        if(index>=0&&index<((JTabbedPane)cont).getTabCount()){
            ((JTabbedPane)cont).setSelectedIndex(index);
        }
    }


    /**
     * Loads all models of a group.
     * This will erase all current tabs
     * @param models A collection of models.
     */
    @Override
    public void loadADDs(List <Object> models){
        cont.removeAll();
        contentPanels.clear();

        create(models.size());
        for(int i=0;i<models.size();i++){
            if(models.get(i) instanceof ADD){
                ADD model=(ADD)models.get(i);
                model.validateConnections();
                contentPanels.get(i).getGraph().load(model);
                contentPanels.get(i).sort(()->{});
            }
        }
    }
    /**
     * Loads all models of a group.
     * This will erase all current tabs
     * @param models A collection of models.
     * @param additional A list of additional FeedbackTuples to indicate changes in the model
     */
    @Override
    public void loadADDs(List <Object> models, List<Collection<FeedbackTuple>> additional, List<String> labels){
        loadADDs(models);
        if(additional!=null&&models.size()==additional.size()){
            for(int i=0;i<models.size();i++){
                contentPanels.get(i).getGraph().setAdditionalFeedback(additional.get(i));
            }
        }
        //Set Labels takes care of the size checking
        setLabels(labels);
    }

}
