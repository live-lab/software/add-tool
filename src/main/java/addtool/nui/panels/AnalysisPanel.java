package addtool.nui.panels;

import addtool.add.model.ADD;
import addtool.analysisonadd.PrintableVisitor;
import addtool.helpers.preferences.BooleanPreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.nui.mxWrapper.nGraph;
import addtool.timeseries.PACTuple;

import javax.swing.*;
import java.util.List;
import java.util.ArrayList;
import java.util.function.BiConsumer;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

/**
 * The Panel to output ADD-Data on.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public class AnalysisPanel extends HidableTabbedPanel implements Commandable{
    /**
     * A key for Preferences grouped under view/analysis (displayed strings may vary due to localisation)
     */
    public static final String PREF_GROUP="View/analysis";
    /**
     * A prefix for preferences on supported domains. Will be put in front of the name of the method.
     * The resulting preference defines if the method is displayed.
     * @see PrintableVisitor#getName()
     */
    public static final String PREF_PREFIX="show_";
    /**
     * A Preference if PAC (uncertain) values should be displayed.
     * @see AnalysisFormular#getPrintType()
     */
    public static final String PREF_PAC="usePAC";

    /*
     * Prepares preferences and pushes them to the Preference Manager
     */
    static {
        //Create Domain Preferences
        String[] options= PrintableVisitor.getOptions();
        for(String name:options){
            BooleanPreference showAnalysis=new BooleanPreference(PREF_PREFIX+name,PREF_PREFIX+name,PREF_GROUP,true);
            PreferenceManager.getPreferenceManager().putPreferences(showAnalysis);
        }
        BooleanPreference showPAC=new BooleanPreference(PREF_PAC,PREF_PAC,PREF_GROUP,false);
        PreferenceManager.getPreferenceManager().putPreferences(showPAC);
    }

    /**
     * The last used ADD if it was valid
     * @see AnalysisPanel#notify(nGraph, ADD)
     */
    private ADD last=null;

    /**
     * The last used Graph
     * @see AnalysisPanel#notify(nGraph, ADD)
     */
    //private nGraph graph = null;
    /**
     * A list of available analysis tabs
     * @see AnalysisFormular
     */
    final List <AnalysisFormular> analysisFormulars;

    /**
     * Creates a new Analysis Panel
     * Will automatically use the PrintableVisitor Interface to get available Analysis Options.
     * Will create tabs for every method and display them depending on the preferences.
     * @see PrintableVisitor
     * @see HidableTabbedPanel
     */
    public AnalysisPanel(){
        super();
        analysisFormulars =new ArrayList<>();
        String[] options= PrintableVisitor.getOptions();
        for(String name:options){
            PrintableVisitor pv=PrintableVisitor.getMethodByFullName(name);
            AnalysisFormular PropArea= new AnalysisFormular(pv.getName(),(printableVisitor,model,p)->PrintableVisitor.print(printableVisitor,model,p),pv);
            analysisFormulars.add(PropArea);
            JScrollPane panel=new JScrollPane(PropArea);
            addTab(getResource(pv.getName()),null,
                    panel,
                    getTooltip(pv.getName()));
            int index=this.getTabCount()-1;
            BooleanPreference preference= (BooleanPreference) PreferenceManager.getPreferenceManager().getPreference(PREF_PREFIX+pv.getName());
            setVisibilityAt(index,preference.getValue());
            this.setVisible(anyVisible());
            preference.addActionListener(e->{
                this.setVisibilityAt(index,preference.getValue());
                this.setVisible(anyVisible());
            });
        }
        invalidate();
        setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
        addChangeListener(e->this.updateUI());
        setSelectedIndexToVisible();
    }
    @Override
    public void notify(nGraph graph, ADD model) {
        last=model;
        //this.graph=graph;
        updateUI();
    }

    @Override
    public void updateUI(){
        if(last!=null&&this.getSelectedIndex()!=-1){
            analysisFormulars.get(this.getSelectedIndex()).update(last);
        }
    }

    /**
     * Passes a listener on to a formular
     * @param name the key of the formular
     * @param listener the listener to pass on to the formular
     */
    public void addListenerTo(String name, BiConsumer<String, PACTuple> listener){
        analysisFormulars.forEach(a->{
            if(a.getDomainName().equals(name)){
                a.addValueListener(listener);
            }
        });
    }

    /**
     * Switches the editing mode in the currently selected tab.
     * @see AnalysisFormular#switchEdit()
     */
    public void switchEdit() {
        analysisFormulars.get(getSelectedIndex()).switchEdit();
    }
}
