package addtool.nui.panels;

import addtool.add.model.ADD;
import addtool.feedback.FeedbackGenerator;
import addtool.feedback.FeedbackTuple;
import addtool.helpers.preferences.BooleanPreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.nui.GraphFrame;
import addtool.nui.GraphTransformer;
import addtool.nui.mxWrapper.nADDGraph;
import addtool.nui.mxWrapper.nGraph;
import com.mxgraph.util.mxUtils;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

/**
 * Panel to show feedback on.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public class FeedbackPanel extends HidableTabbedPanel implements Commandable{
    /**
     * Preference group for feedback related Preferences
     */
    public static final String PREF_GROUP="View/feedback";
    /**
     * A Prefix for Preferences related to hiding/showing a specific feedback option
     */
    public static final String PREF_PREFIX="show_";
    /**
     * Key for the general feedback
     */
    public static final String NAME_STRUCTURE="structure";
    static {
        //Create Domain Preferences
        String[] options= FeedbackGenerator.getOptions();
        for(String name:options){
            BooleanPreference showAnalysis=new BooleanPreference(PREF_PREFIX+name,PREF_PREFIX+name,PREF_GROUP,true);
            PreferenceManager.getPreferenceManager().putPreferences(showAnalysis);
        }
    }

    /**
     * The last displayed model
     */
    private ADD last=null;
    /**
     * the last sent graph
     */
    private nADDGraph graph;
    /**
     * All tabs for feedback
     */
    final List<FeedbackArea> feedbackAreas;

    /**
     * Create new object and automatically initalise tabs
     * @see FeedbackGenerator#getOptions()
     * @see FeedbackGenerator#getEmpty()
     */
    public FeedbackPanel(){
        super();
        graph=new nADDGraph();
        feedbackAreas =new ArrayList<>();
        FeedbackArea zeroFeedBack=new FeedbackArea(FeedbackGenerator.getEmpty());
        feedbackAreas.add(zeroFeedBack);
        addTab(getResource(NAME_STRUCTURE),null, new JScrollPane(zeroFeedBack),getTooltip(NAME_STRUCTURE));

        String [] options=FeedbackGenerator.getOptions();
        for(String feedbackType:options){
            FeedbackGenerator generator=FeedbackGenerator.getMethodByName(feedbackType);
            FeedbackArea feedBackArea=new FeedbackArea(generator);
            feedbackAreas.add(feedBackArea);
            addTab(generator.getName(),generator.getIcon(), new JScrollPane(feedBackArea),generator.getName());
            int index=this.getTabCount()-1;
            BooleanPreference preference= (BooleanPreference) PreferenceManager.getPreferenceManager().getPreference(PREF_PREFIX+generator.getName());
            setVisibilityAt(index,preference.getValue());
            preference.addActionListener(e-> this.setVisibilityAt(index,preference.getValue()));
        }
        this.setSelectedIndex(0);
        //setMinimumSize(new Dimension(500,500));
        setSize(new Dimension(200,200));
        invalidate();
        setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
        addChangeListener(e->SwingUtilities.invokeLater(this::updateUI));
    }

    /**
     * Changes the message level displayed in the Panels
     * @param level the last level to be displayed. Lower levels will be omitted
     * @see FeedbackTuple.MESSAGE_TYPE
     */
    public void setLogLevel(FeedbackTuple.MESSAGE_TYPE level){
        feedbackAreas.forEach(f->f.setLogLevel(level));
    }
    @Override
    public void notify(nGraph graph, ADD model) {
        last=model;
        if(graph!=null){
            this.graph=(nADDGraph) graph;
        }
        updateUI();
    }
    @Override
    public void updateUI(){
        if(graph!=null){
            List<FeedbackTuple> basic =new ArrayList<>();
            GraphTransformer.graph2Model(graph,basic);
            feedbackAreas.get(this.getSelectedIndex()).update(graph,last,basic);
        }
        this.invalidate();
    }

    static class FeedbackArea extends JTextPane{
        final FeedbackGenerator generator;
        /**
         * Sets the level of which log messages have to be at least to be shown on update
         */
        FeedbackTuple.MESSAGE_TYPE logLevel;
        public FeedbackArea(FeedbackGenerator gen){
            this(gen, FeedbackTuple.MESSAGE_TYPE.OK);
        }
        public FeedbackArea(FeedbackGenerator gen, FeedbackTuple.MESSAGE_TYPE logLevel){
            generator=gen;
            this.logLevel=logLevel;
            setEditorKit( new javax.swing.text.html.HTMLEditorKit());
            setText("<html>"+getResource("health_check_start")+"</html>"); //NON-NLS
            setMinimumSize(GraphFrame.MINIMAL_DIMENSION);
            setSize(GraphFrame.MINIMAL_DIMENSION);
            putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
        }
        public void setLogLevel(FeedbackTuple.MESSAGE_TYPE logLevel){
            this.logLevel=logLevel;
        }
        public void update(nGraph graph, ADD model, List<FeedbackTuple> basic){
            List<FeedbackTuple> total=new ArrayList<>(basic);
            if(generator!=null&&model!=null&&graph instanceof nADDGraph){
                List<FeedbackTuple> feedback=((nADDGraph)graph).colorFeedback(model,generator);
                total.addAll(feedback);
            }

            this.setText(total.stream()
                    .filter(tuple->tuple.getType().ordinal()<=logLevel.ordinal())
                    .map(tuple->String.format("<span style=\"color:%s\">%s</span> %s", //NON-NLS
                            mxUtils.hexString(tuple.getType().getColor()),
                            tuple.getType().toString(),
                            tuple.getText()))
                    .distinct()
                    .collect(Collectors.joining("<br>",
                            "<html>",
                            "</html>")));

            this.updateUI();
        }
    }
}
