package addtool.nui.panels;

import addtool.add.model.ADD;
import addtool.nui.mxWrapper.nGraph;

/**
 * Listener for changes in the current UI graph.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
@FunctionalInterface
public interface Commandable {
    /**
     * Method is called when there a changes at the subject.
     * @param graph the current graph
     * @param model the ADD that could be generated from the graph data
     */
    void notify(nGraph graph, ADD model);

}
