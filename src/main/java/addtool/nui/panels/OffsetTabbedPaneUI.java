package addtool.nui.panels;
import java.awt.*;
import java.util.function.IntPredicate;

import javax.swing.plaf.basic.BasicTabbedPaneUI;
/**
 * Derived from
 * <a href="https://www.codeproject.com/Tips/814000/Enhanced-JTabbedPane">kjburns1980</a>
 */
public class OffsetTabbedPaneUI extends BasicTabbedPaneUI {
    private int leadingOffset = 0;
    private int minHeight = 0;
    private int trailingOffset=0;
    private final IntPredicate isVisibleAt;

    /**
     * Extension of the normal UI
     * The passed predicate returns if a tab is displayed or not.
     * This is intended to be used with hidable tabbed panels
     * @param isVisibleAt A function taking an index and returning if this index is visible
     * @see HidableTabbedPanel
     */
    public OffsetTabbedPaneUI(IntPredicate isVisibleAt) {
        super();
        this.isVisibleAt=isVisibleAt;
    }

    /* (non-Javadoc)
     * @see javax.swing.plaf.basic.BasicTabbedPaneUI#calculateTabHeight(int, int, int)
     */
    @Override
    protected int calculateTabHeight(int tabPlacement, int tabIndex,
                                     int fontHeight) {
        return Math.max(super.calculateTabHeight(tabPlacement, tabIndex, fontHeight), this.minHeight);
    }

    /* (non-Javadoc)
     * @see javax.swing.plaf.basic.BasicTabbedPaneUI#getTabAreaInsets(int)
     */
    @Override
    protected Insets getTabAreaInsets(int tabPlacement) {
        // ignores tab placement for now
        return new Insets(0, this.leadingOffset, 0, this.trailingOffset);
    }

    /**
     * @param offset the offset to set
     */
    public void setLeadingOffset(int offset) {
        this.leadingOffset = offset;
    }

    /**
     * @param minHeight the minHeight to set
     */
    public void setMinHeight(int minHeight) {
        this.minHeight = minHeight;
    }

    /**
     * The offset for the trailing button group to the normal tabs
     * @param offset an integer denoting the padding in px
     */
    public void setTrailingOffset(int offset) {
        this.trailingOffset = offset;
    }

    @Override
    protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics) {
        if(isVisibleAt.test(tabIndex)){
            return super.calculateTabWidth(tabPlacement, tabIndex, metrics);
        }
        return 0;
    }
}