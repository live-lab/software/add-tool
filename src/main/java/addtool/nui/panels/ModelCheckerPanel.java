package addtool.nui.panels;

import addtool.add.model.ADD;
import addtool.helpers.preferences.PreferenceManager;
import addtool.modelchecking.CheckerAdapter;
import addtool.modelchecking.ModelCheckerAdapter;
import addtool.nui.GraphTransformer;
import addtool.nui.UIHelper;
import addtool.nui.mxWrapper.nADDGraph;
import addtool.nui.mxWrapper.nGraph;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Objects;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Tab to hold output of model-checking.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public class ModelCheckerPanel  extends JTabbedPane implements Commandable, TabParent {
    final AbstractButton neu;
    nADDGraph current;

    /**
     * Creates a new model checker panel
     */
    public ModelCheckerPanel() {
        super();
        current=new nADDGraph();

        neu=UIHelper.getLabelLikeButton("button_add","start_checking_wizard",e-> openWizard());

        addTab(getResource("do_not_click"), UIHelper.getLabelLikeButton("start_checking_wizard",e->openWizard()));
        setTabComponentAt(0,neu);
    }
    @Override
    public void notify(nGraph graph, ADD model) {
        if(graph instanceof nADDGraph){
            current=(nADDGraph)graph;
        }
    }
    protected void create(CheckerAdapter adapter, ADD model){
        int pos=this.getComponentCount()-2;
        WriteToTextArea area=new WriteToTextArea();
        area.append(getResource("starting")+"...\n");
        area.setEditable(false);
        JScrollPane scrollPane=new JScrollPane(area);
        insertTab(adapter.getName(),null,scrollPane,null,pos);
        setTabComponentAt(pos,new TabComponent(adapter.getName(),this));
        setSelectedIndex(pos);
        adapter.execute(model,area.getConnector());
    }

    @Override
    public void removeTab(int index) {
        removeTabAt(index);
    }

    @Override
    public void setSelected(Component comp) {
        setSelectedIndex(indexOfTabComponent(comp));
    }


    @Override
    public void removeTab(Component comp) {
        removeTab(indexOfTabComponent(comp));
    }
    private void openWizard(){
        if(current==null)return;
        JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);

        ADD model= GraphTransformer.graph2Model(current);
        if(model==null){
            JOptionPane.showMessageDialog(topFrame,getResource("export_error"),getResource("export_error_title"),JOptionPane.ERROR_MESSAGE);
            return;
        }
        //This is necessary to get all classes loaded
        CheckerAdapter.getOptions();
        ActionListener onClose=e->{
            String []options=CheckerAdapter.getOptions();
            PreferenceManager.getPreferenceManager().getPreferences(ModelCheckerAdapter.PREF_GROUP).stream()
                    .filter(p -> p.getEditComponent().isShowing())
                    .map(p -> {
                        for (String option : options) {
                            if (p.getKey().toUpperCase().contains(option.toUpperCase())) {
                                return option;
                            }
                        }
                        return null;
                    }).filter(Objects::nonNull)
                    .map(CheckerAdapter::getMethodByName)
                    .findFirst().ifPresent(lastUsed -> create(lastUsed, model));
        };

        PreferenceManager.getPreferenceManager().openEditDialog(topFrame, ModelCheckerAdapter.PREF_GROUP.substring(0, ModelCheckerAdapter.PREF_GROUP.length()-1),1,onClose);
    }
}
