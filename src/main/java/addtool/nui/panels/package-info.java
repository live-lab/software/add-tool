/**
 * This Package defines Panels to be used in the Editor.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
package addtool.nui.panels;