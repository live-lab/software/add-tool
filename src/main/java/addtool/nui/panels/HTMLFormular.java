package addtool.nui.panels;


import java.awt.event.ActionEvent;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.text.html.*;

/**
 * HTMLFormular.java
 * Derived from:
 * <a href="https://wiki.byte-welt.net/wiki/HTML-Formular_in_Swing-Benutzerinterface">Byte-Welt</a>
 */
public class HTMLFormular extends JEditorPane {

    private final String encoding;
    private Map<String, String> resultMap = null;

    /**
     * Creates a new object with encoding UTF-8
     * @see HTMLFormular#HTMLFormular(String)
     */
    public HTMLFormular() {
        this("UTF-8");
    }

    /**
     * Creates a new form component.
     * @param encoding the input encoding. Usually UTF-8
     */
    public HTMLFormular(String encoding) {
        this.encoding = encoding;
        setEditable(false);
        setEditorKit(new HTMLEditorKit() {

            @Override
            public ViewFactory getViewFactory() {
                return new HTMLEditorKit.HTMLFactory() {

                    @Override
                    public View create(Element elem) {
                        Object o = elem.getAttributes().getAttribute(StyleConstants.NameAttribute);
                        if (o instanceof HTML.Tag) {
                            HTML.Tag kind = (HTML.Tag) o;
                            if (kind.equals(HTML.Tag.INPUT)) {
                                return new FormView(elem) {

                                    @Override
                                    protected void submitData(String data) {
                                        try {
                                            setData(data);
                                        } catch (UnsupportedEncodingException ex) {
                                            Logger.getLogger(HTMLFormular.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    @Override
                                    public void actionPerformed(ActionEvent evt) {
                                        super.actionPerformed(evt);
                                        Element element = getElement();
                                        StringBuilder dataBuffer = new StringBuilder();
                                        HTMLDocument doc = (HTMLDocument)getDocument();
                                        AttributeSet attr = element.getAttributes();

                                        String type = (String) attr.getAttribute(HTML.Attribute.TYPE);
                                        if (type.equals("reset")) {
                                            resetForm();
                                        }
                                    }
                                };
                            }
                        }
                        return super.create(elem);
                    }
                };
            }
        });
    }
    protected void resetForm(){

    }

    private void setData(String data) throws UnsupportedEncodingException {
        resultMap = new HashMap<>(16);
        StringTokenizer st = new StringTokenizer(data, "&");
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            String key = URLDecoder.decode(token.substring(0, token.indexOf('=')), encoding);
            String value = URLDecoder.decode(token.substring(token.indexOf('=') + 1), encoding);
            resultMap.put(key, value);
        }
        firePropertyChange("data", null, data);
    }

    /**
     * Returns a map with the html results
     * Keys are the field ids, values the values in the input
     * @return the result map
     */
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public Map<String, String> getResult() {
        return resultMap;
    }
}