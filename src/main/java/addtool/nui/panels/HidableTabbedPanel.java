package addtool.nui.panels;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * An extension to JTabbedPanes allowing to hide a tab
 * This is done by calculating a zero width of hidden tabs.
 * Will also change selection if the selected tab is hidden.
 * Do not change the UI, otherwise it will not work properly
 */
public class HidableTabbedPanel extends JTabbedPane {
    final List<Boolean> tabVisibility;

    /**
     * Creates a new Hidable Tabbed Panel. No tabs yet included.
     */
    public HidableTabbedPanel(){
        super();
        tabVisibility=new ArrayList<>();
        this.setUI(new com.formdev.flatlaf.ui.FlatTabbedPaneUI() {
            @Override
            protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics) {
                if(tabVisibility.get(tabIndex)){
                    return super.calculateTabWidth(tabPlacement, tabIndex, metrics);
                }
                return 0;
            }
        });
    }

    /**
     * Check if the tab at a position is visible
     * @param index the index
     * @return true if visible, false if not
     */
    public boolean isVisibleAt(int index){
        return tabVisibility.get(index);
    }
    /**
     * Sets a tabs visibility
     * @param index the index
     * @param value the new value. True if it should be visible
     */
    public void setVisibilityAt(int index,boolean value){
        tabVisibility.set(index,value);
        if(!value&&index==getSelectedIndex()){
            setSelectedIndexToVisible();
        }
    }
    /**
     * Updates selection to a visible tab.
     * Is automatically done after visibility changes
     */
    public void setSelectedIndexToVisible(){
        for(int i=0;i<getTabCount();i++){
            if(tabVisibility.get(i)){
                setSelectedIndex(i);
                break;
            }
        }
    }
    @Override
    public void insertTab(String title, Icon icon, Component component, String tip, int index) {
        super.insertTab(title,icon,component,tip,index);
        tabVisibility.add(index,true);
    }
    @Override
    public void removeTabAt(int index) {
        super.removeTabAt(index);
        tabVisibility.remove(index);
    }

    @Override
    public void removeAll() {
        super.removeAll();
        tabVisibility.clear();
    }

    /**
     * Converts tab title into index
     * Also checks for resourced names
     * @param name the title of a tab.
     * @return the index of the tab, -1 if no matching tab was found.
     * @see addtool.helpers.preferences.LanguageSupport#getResource(String)
     */
    public int getTabIndexByName(String name){
        int tabCount = this.getTabCount();
        for (int i=0; i < tabCount; i++)
        {
            String tabTitle = this.getTitleAt(i);
            if (tabTitle.equals(name)) return i;
            if (tabTitle.equals(getResource(name))) return i;
        }
        return -1;
    }

    /**
     * Checks if any tab is visible.
     * @return true if at least one tab is shown.
     */
    public boolean anyVisible(){
        return tabVisibility.stream().anyMatch(b->b);
    }
}
