package addtool.nui.panels;

import javax.swing.*;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * A standard TextArea that can be fed via its own PrintStream.
 * Can be used for logging etc.
 */
public class WriteToTextArea extends JTextArea {
    private final PrintStream connector;

    /**
     * Creates a new object and sets up the connector
     */
    public WriteToTextArea(){
        connector=new PrintStream(new TAOutputStream(),true);
    }

    /**
     * Returns the connected print stream
     * Any input will be displayed in the panel
     * @return a print stream
     */
    public PrintStream getConnector(){
        return connector;
    }

    class TAOutputStream extends OutputStream {
        private final List<Byte> buffer=new ArrayList<>();
        @Override
        public void write(int i) {
            buffer.add((byte)i);
        }
        @Override
        public void flush() {
            byte[] buff=new byte[buffer.size()];
            for(int i=0;i<buff.length;i++){
                buff[i]=buffer.get(i);
            }
            buffer.clear();
            String str=new String(buff, StandardCharsets.UTF_8);
            SwingUtilities.invokeLater(()-> WriteToTextArea.this.append(str));
        }
        @Override
        public void close() {
        }
    }
}
