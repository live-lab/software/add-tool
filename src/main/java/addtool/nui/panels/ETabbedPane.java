package addtool.nui.panels;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.Serial;

import javax.swing.*;

/**
 * Derived from
 * <a href="https://www.codeproject.com/Tips/814000/Enhanced-JTabbedPane">kjburns1980</a>
 */
public class ETabbedPane extends JPanel {
    /**
     * The side where a new button is added.
     * Leading means before the Tabs
     * Trailing is after the tabs
     */
    public enum ButtonSide {
        /**
         * Button will be displayed left of the tabs if flow is left to right
         */
        LEADING,
        /**
         * Button will be displayed after the tabs  if flow is left to right
         */
        TRAILING
    }
    /**
     *
     */
    @Serial
    private static final long serialVersionUID = -2090869893562246860L;
    private final HidableTabbedPanel tabs;
    private final JToolBar leadingButtons;
    private final JToolBar trailingButtons;
    //private JPanel dropDown;
    private final OffsetTabbedPaneUI tabUI;

    /**
     * Creates a new ETabbedPane
     * @param orient a flow direction
     * @param panel The Tabbed Panel to be displayed between the buttons
     * @see ComponentOrientation#LEFT_TO_RIGHT
     */
    public ETabbedPane(ComponentOrientation orient,HidableTabbedPanel panel) {
        SpringLayout sl = new SpringLayout();
        this.setLayout(sl);

        this.leadingButtons = new JToolBar();
        this.leadingButtons.setBorderPainted(false);
        this.leadingButtons.setFloatable(false);
        this.leadingButtons.setOpaque(false);
        this.leadingButtons.setComponentOrientation(orient);
        this.leadingButtons.setLayout(new FlowLayout(FlowLayout.LEADING, 1, 1));
        sl.putConstraint(SpringLayout.NORTH, this.leadingButtons, 0, SpringLayout.NORTH, this);
        String side = ((orient == ComponentOrientation.RIGHT_TO_LEFT) ? SpringLayout.EAST : SpringLayout.WEST);
        sl.putConstraint(side, this.leadingButtons, 0, side, this);
        this.add(leadingButtons);

        this.trailingButtons = new JToolBar();
        this.trailingButtons.setBorderPainted(false);
        this.trailingButtons.setFloatable(false);
        this.trailingButtons.setOpaque(false);
        this.trailingButtons.setComponentOrientation(orient);
        this.trailingButtons.setLayout(new FlowLayout(FlowLayout.LEADING, 1, 1));
        sl.putConstraint(SpringLayout.NORTH, this.trailingButtons, 0, SpringLayout.NORTH, this);
        side = ((orient == ComponentOrientation.RIGHT_TO_LEFT) ? SpringLayout.WEST : SpringLayout.EAST);
        sl.putConstraint(side, this.trailingButtons, 0, side, this);
        this.add(trailingButtons);

        this.tabs = panel;
        this.tabs.setComponentOrientation(orient);
        sl.putConstraint(SpringLayout.NORTH, this.tabs, 0, SpringLayout.NORTH, this);
        sl.putConstraint(SpringLayout.SOUTH, this.tabs, 0, SpringLayout.SOUTH, this);
        sl.putConstraint(SpringLayout.WEST, this.tabs, 0, SpringLayout.WEST, this);
        sl.putConstraint(SpringLayout.EAST, this.tabs, 0, SpringLayout.EAST, this);
        tabUI = new OffsetTabbedPaneUI(tabs::isVisibleAt);
        this.tabs.setUI(tabUI);
        this.add(tabs);

        this.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                if (tabs.getTabCount() > 0) {
                    tabs.getSelectedComponent().requestFocusInWindow();
                }
            }
        });
    }

    /**
     * @return the tabs
     */
    public JTabbedPane getJTabbedPane() {
        return tabs;
    }

    /**
     * Adds a button at the specified size
     * @param button The Button to add
     * @param side a side to add it to
     * @see ButtonSide
     */
    public void addButton(AbstractButton button, ButtonSide side) {
        button.setBorderPainted(false);
        button.setFocusable(false);
        button.setMargin(new Insets(1, 1, 1, 1));
        ((side == ButtonSide.LEADING) ? this.leadingButtons : this.trailingButtons).add(button);
        this.tabUI.setMinHeight(
                Math.max(this.leadingButtons.getPreferredSize().height,
                        this.trailingButtons.getPreferredSize().height));
        this.tabUI.setLeadingOffset(this.leadingButtons.getPreferredSize().width);
        this.tabUI.setTrailingOffset(this.trailingButtons.getPreferredSize().width);
        this.validate();
    }
}