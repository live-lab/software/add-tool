package addtool.nui.panels;

import addtool.add.model.ADD;
import addtool.feedback.FeedbackTuple;
import addtool.nui.mxWrapper.nADDGraph;
import addtool.nui.mxWrapper.nGraph;
import addtool.nui.mxWrapper.nGraphComponent;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This class handles multiple {@link nGraphComponent} objects and makes it possible to switch between them.
 */
public class SliderMultiGraphComponent extends MultiGraphComponent {
    final CardLayout layout;
    final JSlider selection;
    final JLabel label;

    /**
     * Creates a new object with only one model and a slider
     */
    public SliderMultiGraphComponent(){
        super();

        layout=new CardLayout();
        cont.setLayout(layout);

        selection=new JSlider();
        selection.setMinimum(1);
        selection.setMaximum(1);
        selection.setVisible(false);
        selection.setPaintLabels(true);
        selection.addChangeListener(e->{
            updateSelection(((JSlider)e.getSource()).getValue());
            triggerListeners();
        });
        this.add(selection,BorderLayout.PAGE_END);

        label=new JLabel("No Text yet");
        label.setVisible(false);
        this.add(label,BorderLayout.PAGE_START);
        //this.setBackground(Color.gray);
        create(1);
    }

    /**
     * creates a new component with a certain ampunt of empty canvas
     * @param size the amount of pages
     */
    public SliderMultiGraphComponent(int size){
        this();
        create(size-1);//one was already built by default

        updateSlider();
    }

    @Override
    public void setLabel(String lb) {
        int pos=labels.indexOf(label.getText());
        labels.remove(pos);
        labels.add(pos,lb);
        label.setText(lb);
    }
    @Override
    public String getLabel(){
        return null;
    }

    @Override
    public void setLabels(List<String> labels){
        if(labels!=null&&labels.size()==contentPanels.size()){
            this.labels=new ArrayList<>(labels);
            label.setVisible(true);
            label.setText(labels.get(contentPanels.indexOf(current)));
        }else{
            this.labels.clear();
            label.setVisible(false);
        }
    }
    /**
     * Creates a certain amount of Panels.
     * @param size The amount of panels to generate
     */
    @Override
    public void create(int size){
        for(int i=0;i<size;i++){
            addGraph(new nADDGraph(),"");
        }
        if(size>0){
            current= contentPanels.get(0);
            layout.first(cont);
        }
    }

    @Override
    public void addGraph(nGraph graph, String label) {
        nGraphComponent drawer=new nGraphComponent(nGraph.copyGraph(graph));
        drawer.setGridVisible(this.isGridShown());
        drawer.getViewport().setOpaque(true);
        drawer.getViewport().setBackground(Color.WHITE);
        drawer.setSorting(this.getSorting());
        //drawer.setBackground(Color.BLUE);
        contentPanels.add(drawer);
        cont.add(drawer,String.valueOf(contentPanels.size()));
    }

    @Override
    public void display(int index) {
        updateSelection(index);
    }

    /**
     * Updates Slider and its invariants on size change.
     */
    private void updateSlider(){
        selection.setMaximum(contentPanels.size());
        selection.setVisible(contentPanels.size() > 1);
    }
    /**
     * Updates Layout and invariants.
     */
    private void updateSelection(int pos){
        layout.show(cont,String.valueOf(pos));
        current= contentPanels.get(pos-1);
        if(!labels.isEmpty()){
            label.setText(labels.get(pos-1));
        }
    }

    /**
     * Loads all models of a group.
     * This will erase all current tabs
     * @param models A collection of models.
     */
    @Override
    public void loadADDs(List <Object> models){
        cont.removeAll();
        contentPanels.clear();

        create(models.size());
        for(int i=0;i<models.size();i++){
            if(models.get(i) instanceof ADD){
                ADD model=(ADD) models.get(i);
                model.validateConnections();
                contentPanels.get(i).getGraph().load(model);
                contentPanels.get(i).sort(()->{});
            }
        }
        updateSlider();
    }
    /**
     * Loads all models of a group.
     * This will erase all current tabs
     * @param models A collection of models.
     * @param additional A list of additional FeedbackTuples to indicate changes in the model
     */
    @Override
    public void loadADDs(List <Object> models, List<Collection<FeedbackTuple>> additional, List<String> labels){
        loadADDs(models);
        if(additional!=null&&models.size()==additional.size()){
            for(int i=0;i<models.size();i++){
                contentPanels.get(i).getGraph().setAdditionalFeedback(additional.get(i));
            }
        }
        //Set Labels takes care of the size checking
        setLabels(labels);
    }
}
