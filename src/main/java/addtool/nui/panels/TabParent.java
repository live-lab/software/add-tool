package addtool.nui.panels;

import java.awt.*;

/**
 * An interface for connections between TabComponents und tabbed panels
 * @see TabComponent
 */
public interface TabParent {
    /**
     * Remove a tab at a certain location
     * @param index the index of the tab
     */
    void removeTab(int index);
    /**
     * Remove a tab with a certain tab component
     * @param comp the tab component to remove
     */
    void removeTab(Component comp);

    /**
     * Set selected tab to certain index
     * @param index the target index
     */
    void setSelectedIndex(int index);
    /**
     * Set selected tab to tab with certain tab component
     * @param comp the target component
     */
    void setSelected(Component comp);
}
