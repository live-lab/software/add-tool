package addtool.nui.panels;

import addtool.feedback.FeedbackTuple;
import addtool.helpers.preferences.BooleanPreference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.RadioPreference;
import addtool.nui.mxWrapper.nADDGraph;
import addtool.nui.mxWrapper.nGraph;
import addtool.nui.mxWrapper.nGraphComponent;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.Map.entry;

/**
 * Components that can in some way hold multiple {@link nGraphComponent}.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public abstract class MultiGraphComponent extends JPanel {
    protected final java.util.List<Runnable> listeners;
    protected JComponent cont;
    protected final java.util.List<nGraphComponent> contentPanels;
    protected java.util.List<String> labels;
    protected nGraphComponent current;
    protected final Map<String, Integer> keyToSorting= Map.ofEntries(
        entry("bottom_up", JLabel.SOUTH),
        entry("top_down", JLabel.NORTH),
            entry("left_right",JLabel.WEST)
    );
    final RadioPreference sorting;
    final BooleanPreference showGrid;

    /**
     * Creates a new Multigraph Component.
     * Handles settings connection for children.
     */
    public MultiGraphComponent(){
        this.setLayout(new BorderLayout());
        current=new nGraphComponent(new nADDGraph());
        cont=new JPanel();
        this.add(cont,BorderLayout.CENTER);
        //this.add(new JTextArea(),BorderLayout.LINE_START);

        contentPanels =new ArrayList<>();

        labels=new ArrayList<>();
        listeners=new ArrayList<>();
        //create(1);
        setBorder(BorderFactory.createLoweredBevelBorder());


        //Define Preferences
        sorting=new RadioPreference("sorting","sorting","View","top_down",
                keyToSorting.keySet().toArray(String[]::new));
        PreferenceManager.getPreferenceManager().putPreferences(sorting);
        sorting.addActionListener(e->setSorting(keyToSorting.get(sorting.getValueString())));
        setSorting(keyToSorting.get(sorting.getValueString()));

        showGrid=new BooleanPreference("show_grid","show_grid","View",true);
        PreferenceManager.getPreferenceManager().putPreferences(showGrid);
        showGrid.addActionListener(e-> setGridPainted(showGrid.getValue()));
        setGridPainted(showGrid.getValue());
    }

    /**
     * Changes label of the currently selected component
     * @param lb the new label
     */
    public abstract void setLabel(String lb);

    /**
     * Returns label
     * @return label of currently selected component
     */
    public abstract String getLabel();

    /**
     * Get all labels of the component
     * @return A list with all labels
     */
    public List<String> getLabels(){
        return new ArrayList<>(labels);
    }

    /**
     * Update all labels
     * Usually should have the same size as the panels of the component
     * @param labels a list of new labels
     */
    public abstract void setLabels(java.util.List<String> labels);
    /**
     * Creates a certain amount of Panels.
     * @param size The amount of panels to generate
     */
    public abstract void create(int size);

    /**
     * Returns currently displayed graph
     * @return the graph used in the selected graph component
     */
    public nGraph getGraph(){
        return current.getGraph();
    }

    /**
     * Adds a new graph as new component
     * @param graph the new graph
     */
    public void addGraph(nGraph graph){
        addGraph(graph,"");
    }

    /**
     * Adds a new graph as new component
     * @param graph the new graph
     * @param label the label used in the new component.
     */
    public abstract void addGraph(nGraph graph,String label);

    /**
     * Get currently selected graph component
     * @return the selected graph component
     */
    public nGraphComponent getGraphComponent(){
        return current;
    }

    /**
     * revalidates the current graph
     * not used anymore as tool runs stable and this method creates new issues
     */
    @Deprecated
    public void resetGraphComponent(){
        int pos= contentPanels.indexOf(current);
        cont.remove(current);

        nGraphComponent drawer=new nGraphComponent(current.getGraph());
        cont.add(drawer,String.valueOf(pos));
        cont.revalidate();

        contentPanels.replaceAll(gc->{
            if(gc.equals(current)){
                return drawer;
            }else{
                return gc;
            }
        });
        current=drawer;
    }

    /**
     * Set selection to tab of given index
     * @param index the index of the tab to be displayed
     */
    public abstract void display (int index);
    /**
     * Loads all models of a group.
     * This will erase all current tabs
     * @param models A collection of models.
     */
    public abstract void loadADDs(java.util.List<Object> models);
    /**
     * Loads all models of a group.
     * This will erase all current tabs
     * @param models A collection of models.
     * @param additional A list of additional FeedbackTuples to indicate changes in the model
     * @param labels a list of labels for the models
     */
    public abstract void loadADDs(java.util.List<Object> models, java.util.List<Collection<FeedbackTuple>> additional, List<String> labels);

    /**
     * Adds a change listener.
     * The listener will be triggered by the {@link MultiGraphComponent#triggerListeners()} method. Called on change of displayed component
     * @param listener a new listener
     */
    public void addListener(Runnable listener){
        listeners.add(listener);
    }

    /**
     * Activates change listeners if the displayed object changes
     */
    public void triggerListeners(){
        listeners.forEach(Runnable::run);
    }

    /**
     * Changes sortign for all child components
     * @param sorting Sorting direction
     * @see MultiGraphComponent#keyToSorting
     */
    public void setSorting(int sorting){
        contentPanels.forEach(c->c.setSorting(sorting));
    }

    /**
     * Returns current standard sorting.
     * @return the integer representing the sorting
     * @see MultiGraphComponent#keyToSorting
     */
    public int getSorting(){
        return keyToSorting.get(sorting.getValueString());
    }

    /**
     * Changes grid painting for all children
     * @param showGrid if true the grid is painted
     */
    public void setGridPainted(boolean showGrid){
        contentPanels.forEach(c->c.setGridVisible(showGrid));
        this.repaint();
    }

    /**
     * If the grid is painted
     * @return true if the grid is painted
     */
    public boolean isGridShown(){
        return showGrid.getValue();
    }

}
