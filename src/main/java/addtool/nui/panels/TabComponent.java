package addtool.nui.panels;

import addtool.nui.UIHelper;

import javax.swing.*;
import java.awt.*;

/**
 * A tab component representing the tab item in a Tabbed Panel
 * Features a label with automatic truncating, a star as change marker, a close button
 */
public class TabComponent extends JPanel {
    static final int MAX_LENGTH=15;
    final AbstractButton close;
    final JButton lb;
    String title;
    final TabParent parent;
    /**
     * A boolean if the underlying tab was changed recently
     */
    private boolean changed;

    /**
     * Creates a new tab component with a title
     * @param title the title
     * @param parent the parent object, this is needed to inform the parent of the closing operation
     * @see TabParent
     */
    public TabComponent(String title,TabParent parent){
        super();
        this.parent=parent;
        setLayout(new GridBagLayout());
        GridBagConstraints gbc=new GridBagConstraints();
        gbc.fill=GridBagConstraints.HORIZONTAL;
        gbc.weightx=0.9;
        this.title=title;
        this.changed=false;
        this.setToolTipText(this.title);

        lb= UIHelper.getLabelLikeButton(cutTitle(),e->parent.setSelected(this));
        lb.setToolTipText(this.title);

        setOpaque(false);
        add(lb,gbc);
        gbc.weightx=0.1;
        close=UIHelper.buildToolButton("button_close","close",e->parent.removeTab(this));
        add(close,gbc);
        this.setOpaque(false);
    }

    /**
     * Changes the title text
     * The tooltip will be updated to the un-truncated version
     * @param title a text
     */
    public void setTitle(String title){
        this.title=title;
        updateTitle();
    }

    /**
     * Switches the change marker
     * @param changed true if the change marker should be displayed
     */
    public void setChanged(boolean changed){
        this.changed=changed;
        updateTitle();
    }

    private String cutTitle(){
        String prefix=changed?"* ":"";
        return prefix+(this.title.length()>MAX_LENGTH?
                "..."+ this.title.substring(this.title.length()-MAX_LENGTH+3):
                this.title);
    }

    /**
     * Updates title (truncated) and sets tooltip to un-truncated text
     */
    public void updateTitle(){
        lb.setText(cutTitle());
        lb.setToolTipText(this.title);
        this.setToolTipText(this.title);
    }
}