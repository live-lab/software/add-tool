package addtool.nui;

import addtool.add.model.*;
import addtool.helpers.Constants;
import addtool.helpers.preferences.*;
import addtool.modelchecking.CheckerAdapter;
import addtool.nui.mxWrapper.nGraphComponent;
import addtool.nui.panels.IconLabel;
import addtool.nui.panels.MultiGraphComponent;
import addtool.nui.style.BasicStyle;
import com.mxgraph.util.mxPoint;
import org.jscience.mathematics.number.Rational;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

/**
 * Helper class to handle first startup and tutorial
 */
public final class Tutorial {
    /**
     * Key for the preference to show startup window
     */
    public static final String PREF_FIRST="tuto_first_start";
    /**
     * Key for the preference to show tour on start up
     */
    public static final String PREF_TOUR="tuto_show_tour";
    static {
        BooleanPreference firstStart=new BooleanPreference(PREF_FIRST,PREF_FIRST,"",true);
        firstStart.setVisible(false);//Hide it in Options
        BooleanPreference showTour=new BooleanPreference(PREF_TOUR,PREF_TOUR,"",true);
        showTour.setVisible(false);//Hide it in Options
        PreferenceManager.getPreferenceManager().putPreferences(firstStart,showTour);
    }

    private Tutorial() {
    }
    private static PreferenceProfile selected=null;
    /**
     * Opens a quick start guide to set essential settings
     * @return A selected PreferenceProfile. At first startup the preferences are not loaded yet. So this has to be done after loading the dialog.
     */
    public static PreferenceProfile checkSetup(){
        //Things that should be set on first run:
        //Language, Expert level, Search for MCs
        if(((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference(PREF_FIRST)).getValue()){
            JDialog dia=new JDialog();
            dia.setTitle(getResource("setup_title"));
            dia.setLayout(new BorderLayout());

            try {
                ImageIcon logo=new ImageIcon(
                        ImageIO.read(
                                Objects.requireNonNull(
                                        GraphFrame.class.getResource(Constants.getResourceFolder() + "quadtool_logo_rund.png")
                                )
                        )
                );
                JLabel iconLabel=new JLabel(new ImageIcon(logo.getImage().getScaledInstance(128,128,0)));
                dia.add(iconLabel,BorderLayout.LINE_START);
                dia.setIconImage(logo.getImage());
            } catch (IOException ignored) {
                //Do nothing. If it goes wrong there is just no icon
            }

            JPanel content=new JPanel();
            content.setLayout(new GridLayout(0,1));

            JLabel welcome=new JLabel(getResource("welcome_message"));
            content.add(welcome);

            //Language
            JPanel languagePanel=new JPanel();
            languagePanel.setLayout(new GridLayout(1,0));
            JLabel languageLb=new JLabel(getResource("language"));
            languageLb.setToolTipText(getTooltip("language"));
            languagePanel.add(languageLb);
            Preference languagePref= PreferenceManager.getPreferenceManager().getPreference(LanguageSupport.PREF_KEY);
            languagePanel.add(languagePref.getEditComponent());
            content.add(languagePanel);

            //Expert Level
            List<PreferenceProfile> options=PreferenceProfile.getProfiles();
            String[] stringOptions=options.stream().map(p->getResource(p.getTitle())).toArray(String[]::new);
            JComboBox<String> profileOption=new JComboBox<>(stringOptions);
            JLabel expertLb=new JLabel(getResource("defaultSettings"));
            expertLb.setToolTipText(getTooltip("defaultSettings"));
            JPanel expertPanel=new JPanel();
            expertPanel.setLayout(new GridLayout(1,0));
            expertPanel.add(expertLb);
            expertPanel.add(profileOption);
            content.add(expertPanel);
            content.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

            //Search MCs
            JCheckBox searchMCs=new JCheckBox(getResource("setup_search_MC"));
            searchMCs.setToolTipText(getTooltip("setup_search_MC"));
            content.add(searchMCs);

            dia.add(content,BorderLayout.CENTER);

            JButton run=new JButton(getResource("save"));
            dia.add(run,BorderLayout.PAGE_END);
            run.addActionListener(e->{
                selected=options.stream().
                        filter(p->getResource(p.getTitle()).equals(profileOption.getSelectedItem())).
                        findFirst().orElse(null);

                languagePref.setValueFromComponent();

                if(searchMCs.isSelected()){
                    String[] checkerOptions = CheckerAdapter.getOptions();
                    for(String s:checkerOptions){
                        CheckerAdapter adapter=CheckerAdapter.getMethodByName(s);
                        adapter.findConnectorInBackground();
                    }
                }
                dia.setAlwaysOnTop(false);
                dia.setVisible(false);
                dia.setModal(false);
                dia.dispose();
            });
            dia.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    //Now deactivate as the setup is only done once
                    ((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference(PREF_FIRST)).setValue(false);
                    PreferenceManager.getPreferenceManager().writePreferences();
                }
            });
            dia.setAlwaysOnTop(true);
            dia.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dia.pack();
            dia.setLocationRelativeTo(null);
            dia.setModal(true);
            dia.setVisible(true);

            return selected;
        }
        return null;
    }

    /**
     * Starts an in-App Tour over the most important features
     * will set the preference to true for the moment
     * @param frame The main Graph Frame
     * @param content The Canvas component
     * @param leftToolbar the left toolbar
     * @param FeedbackPanel Feedback Panel
     * @param topToolbar Top toolbar
     * @param analysisPanel Analysis Panel
     * @param checkerPanel Model Checker Panel
     * @param menuBar The Menu bar
     */
    public static void restartTour(GraphFrame frame, MultiGraphComponent content,JToolBar leftToolbar,JTabbedPane FeedbackPanel,JToolBar topToolbar,JTabbedPane analysisPanel,JTabbedPane checkerPanel,JMenuBar menuBar){
        PreferenceManager.getPreferenceManager().getPreference(PREF_TOUR).setValue("TRUE");
        startTour(frame,content,leftToolbar,FeedbackPanel,topToolbar,analysisPanel,checkerPanel,menuBar,0);
    }
    /**
     * Starts an in-App Tour over the most important features
     * @param frame The main Graph Frame
     * @param content The Canvas component
     * @param leftToolbar the left toolbar
     * @param FeedbackPanel Feedback Panel
     * @param topToolbar Top toolbar
     * @param analysisPanel Analysis Panel
     * @param checkerPanel Model Checker Panel
     * @param menuBar The Menu bar
     */
    public static void startTour(GraphFrame frame, MultiGraphComponent content,JToolBar leftToolbar,JTabbedPane FeedbackPanel,JToolBar topToolbar,JTabbedPane analysisPanel,JTabbedPane checkerPanel,JMenuBar menuBar){
        startTour(frame,content,leftToolbar,FeedbackPanel,topToolbar,analysisPanel,checkerPanel,menuBar,0);
    }
    private static void startTour(GraphFrame frame, MultiGraphComponent content,JToolBar leftToolbar,JTabbedPane FeedbackPanel,JToolBar topToolbar,JTabbedPane analysisPanel,JTabbedPane checkerPanel,JMenuBar menuBar, int step){
        /*
        Tour steps:
            1: Load example model
            2: Explain Canvas
                2.1: Node types
                2.2: Edit options (enter/double click, delete, copy/cut/paste)
            3: Left Menu Bar
                3.1 Edit
                3.2 Sorting
                3.3 Drag Nodes
            4: Feedback
            5: Top ToolBar
                5.1 Open/Save
                5.2 Run
                5.3 Upload
                5.4 Font
                5.5 Operations (maybe skip for 2.2)
                5.6 GIT
            6: Analysis Panel
            7: Model checker Panel
            8: Menu Bar
         */
        if(((BooleanPreference)PreferenceManager.getPreferenceManager().getPreference(PREF_TOUR)).getValue()){
            ActionListener onClose=e->startTour(frame,content,leftToolbar,FeedbackPanel,topToolbar,analysisPanel,checkerPanel,menuBar,step+1);
            switch (step){
                case 0->{
                    boolean loadExample=content.getGraphComponent().getGraph().isEmpty();
                    if(loadExample){
                        frame.loadADD(getExample());//Load example
                    }
                    explainCanvas(frame,content,loadExample,onClose);
                }
                case 1-> explainLeftToolBar(frame,leftToolbar,onClose);
                case 2-> explainFeedback(frame,FeedbackPanel,onClose);
                case 3-> explainTopToolBar(frame,topToolbar,onClose);
                case 4-> explainAnalysis(frame,analysisPanel,onClose);
                case 5-> explainChecker(frame,checkerPanel,onClose);
                case 6-> explainMenuBar(frame,menuBar,onClose);
                default-> skipTutorial();
            }
        }
    }

    private static ActionListener normalNext = null;
    private static ActionListener endNext = null;
    private static JDialog preparePopup(Component parent, Point offset,ActionListener onClose,String title,Component ... pages){
        Point p=parent.getLocation();
        SwingUtilities.convertPointToScreen(p,parent);
        p.x+=offset.x;
        p.y+=offset.y;

        JPanel content=new JPanel();
        content.setLayout(new BorderLayout());

        AtomicInteger pageIndex=new AtomicInteger(0);
        JPanel cardPanel=new JPanel();

        CardLayout layout=new CardLayout();
        cardPanel.setLayout(layout);
        for(int i=0;i<pages.length;i++){
            cardPanel.add(String.valueOf(i),pages[i]);
        }
        content.add(cardPanel,BorderLayout.CENTER);

        JPanel bottom=new JPanel();
        bottom.setLayout(new GridLayout(1,0));
        JButton previous=new JButton(getResource("tutorial_previous"));
        previous.setToolTipText(getTooltip("tutorial_previous"));
        previous.setEnabled(false);

        JButton next=new JButton(getResource("tutorial_next"));
        next.setToolTipText(getTooltip("tutorial_next"));

        bottom.add(previous);
        bottom.add(next);

        previous.addActionListener(e->{
            if(pageIndex.get()>=pages.length-1){
                next.setText(getResource("tutorial_next"));
                next.removeActionListener(endNext);
                next.addActionListener(normalNext);
            }
            layout.show(cardPanel,String.valueOf(pageIndex.decrementAndGet()));
            if(pageIndex.get()==0){
                previous.setEnabled(false);
            }
        });
        normalNext=e->{
            layout.show(cardPanel,String.valueOf(pageIndex.incrementAndGet()));
            if(pageIndex.get()>=pages.length-1){
                next.setText(getResource("tutorial_next_end_step"));
                next.removeActionListener(normalNext);
                next.addActionListener(endNext);
            }
            previous.setEnabled(true);
        };
        if(pages.length>1){
            content.add(bottom,BorderLayout.PAGE_END);
            next.addActionListener(normalNext);
        }else{
            content.add(next,BorderLayout.PAGE_END);
            next.setText(getResource("tutorial_next_end_step"));
        }
        content.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JFrame parentWindow=(JFrame) SwingUtilities.getWindowAncestor(parent);
        JDialog popUp=new JDialog(parentWindow);
        popUp.setTitle(getResource(title));
        //popUp.setUndecorated(true);
        popUp.setModal(false);
        popUp.add(content);
        popUp.pack();
        popUp.setResizable(false);
        popUp.setVisible(true);

        if(parentWindow!=null){
            if(p.x+popUp.getWidth()>parentWindow.getContentPane().getWidth()){
                p.x-=(p.x+popUp.getWidth())-parentWindow.getContentPane().getWidth();
            }
            if(p.y+popUp.getHeight()>parentWindow.getContentPane().getHeight()){
                p.y-=(p.y+popUp.getHeight())-parentWindow.getContentPane().getHeight();
            }
        }
        popUp.setLocation(p);

        popUp.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                skipTutorial();popUp.dispose();
            }
        });
        //Do Action Listeners
        endNext=e->{
            popUp.dispose();
          onClose.actionPerformed(e);
        };
        if(pages.length<=1){
            next.addActionListener(endNext);
        }
        return popUp;
    }
    private static void skipTutorial(){
        PreferenceManager.getPreferenceManager().getPreference(PREF_TOUR).setValue("FALSE");
        PreferenceManager.getPreferenceManager().writePreferences();
    }

    /**
     * Generates a small example model
     * @return A small ADD
     */
    public static ADD getExample(){
        And and=new And(getResource("tutorial_target"),"1");
        BasicEventPlayer event1=new BasicEventPlayer(getResource("tutorial_event_1"), Rational.valueOf(4,5),Rational.ONE, Player.Attacker,"2");
        BasicEventPlayer event2=new BasicEventPlayer(getResource("tutorial_event_2"), Rational.valueOf(2,5),Rational.ONE, Player.Attacker,"3");
        and.addPredecessor(event1);
        and.addPredecessor(event2);
        Map<String, Vertex> id2vertex=new HashMap<>();
        id2vertex.put(and.getId(),and);
        id2vertex.put(event1.getId(),event1);
        id2vertex.put(event2.getId(),event2);
        return new ADD(id2vertex, Set.of(and),true);
    }
    private static void explainCanvas(GraphFrame frame,MultiGraphComponent content,boolean loadExample,ActionListener onClose){
        JLabel page1=new JLabel(
                String.format(getResource("tutorial_canvas_page_1"),
                    PreferenceManager.getPreferenceManager().getPreference(BasicStyle.COLOR_ATTACK).getValueString(),
                    PreferenceManager.getPreferenceManager().getPreference(BasicStyle.COLOR_DEFEND).getValueString(),
                    PreferenceManager.getPreferenceManager().getPreference(BasicStyle.COLOR_VERTEX).getValueString()
                ));
        page1.setBorder(BorderFactory.createLoweredBevelBorder());
        IconLabel page2=new IconLabel("",getResource("tutorial_canvas_page_2"));
        page2.setBorder(BorderFactory.createLoweredBevelBorder());
        IconLabel page3=new IconLabel("",getResource("tutorial_canvas_page_3"));//,"/videos/move_edge.gif"
        page3.setBorder(BorderFactory.createLoweredBevelBorder());
        Component[] pages;
        if(loadExample){//Only show page 2 (which explains the example model) if it was actually loaded
            pages= new Component[]{page1, page2, page3};
        }else{
            pages= new Component[]{page1, page3};
        }
        nGraphComponent component=content.getGraphComponent();
        mxPoint border=component.getGraph().getMaxPoint();
        preparePopup(content,
                new Point((int)border.getX(),
                ((int)border.getY()/2)),
                onClose,"tutorial_canvas_title",
                pages);
    }

    private static void explainLeftToolBar(GraphFrame frame, JToolBar toolbar, ActionListener onClose) {
        IconLabel page1_1=new IconLabel(getResource("button_edit_vertex"),getResource("tutorial_leftBar_page_1_1"),"/videos/edit_button.gif");
        IconLabel page1_2=new IconLabel(getResource("button_sort"),getResource("tutorial_leftBar_page_1_2"),"/videos/sort_button.gif");
        JPanel page1=new JPanel();
        page1.setLayout(new GridLayout(0,1));
        page1.add(page1_1);
        page1.add(page1_2);

        IconLabel page2_1=new IconLabel(getResource("button_and_vertex"),getResource("tutorial_leftBar_page_2_1"),"/videos/add_button.gif");
        IconLabel page2_2=new IconLabel(getResource("button_bea_vertex"),getResource("tutorial_leftBar_page_2_2"),"/videos/drag_add_button.gif");
        IconLabel page2_3=new IconLabel(getResource("button_edge"),getResource("tutorial_leftBar_page_2_3"),"/videos/move_edge.gif");
        JPanel page2=new JPanel();
        page2.setLayout(new GridLayout(0,1));
        page2.add(page2_1);
        page2.add(page2_2);
        page2.add(page2_3);

        preparePopup(toolbar,
                new Point(toolbar.getWidth()+10,toolbar.getHeight()/6),
                onClose,"tutorial_leftBar_title",page1,page2);
    }
    private static void explainFeedback(GraphFrame frame, JTabbedPane feedback, ActionListener onClose) {
        IconLabel page1=new IconLabel("",getResource("tutorial_feedback_page_1"),"/videos/structure_error.gif");
        page1.setBorder(BorderFactory.createLoweredBevelBorder());
        preparePopup(feedback.getSelectedComponent(),
                new Point(feedback.getWidth()/2,-100),
                onClose,"tutorial_feedback_title",page1);
    }
    private static void explainAnalysis(GraphFrame frame, JTabbedPane analysis, ActionListener onClose) {
        IconLabel page1=new IconLabel("",getResource("tutorial_analysis_page_1"),"/videos/edit_domain.gif");
        page1.setBorder(BorderFactory.createLoweredBevelBorder());
        preparePopup(analysis.getSelectedComponent(),
                new Point(10,200),
                onClose,"tutorial_analysis_title",page1);
    }

    private static void explainTopToolBar(GraphFrame frame, JToolBar toolbar, ActionListener onClose) {
        IconLabel page1_1=new IconLabel(getResource("button_open"),getResource("tutorial_topBar_page_1_1"));
        IconLabel page1_2=new IconLabel(getResource("button_save"),getResource("tutorial_topBar_page_1_2"));
        JPanel page1=new JPanel();
        page1.setLayout(new GridLayout(0,1));
        page1.add(page1_1);
        page1.add(page1_2);

        IconLabel page2_1=new IconLabel(getResource("button_run"),getResource("tutorial_topBar_page_2_1"));
        IconLabel page2_2=new IconLabel(getResource("button_upload"),getResource("tutorial_topBar_page_2_2"));
        JPanel page2=new JPanel();
        page2.setLayout(new GridLayout(0,1));
        page2.add(page2_1);
        page2.add(page2_2);

        IconLabel page3_1=new IconLabel(getResource("button_font_large"),getResource("tutorial_topBar_page_3_1"));
        IconLabel page3_2=new IconLabel(getResource("button_undo"),getResource("tutorial_topBar_page_3_2"));
        IconLabel page3_3=new IconLabel(getResource("button_redo"),getResource("tutorial_topBar_page_3_3"));
        JPanel page3=new JPanel();
        page3.setLayout(new GridLayout(0,1));
        page3.add(page3_1);
        page3.add(page3_2);
        page3.add(page3_3);


        IconLabel page4_1=new IconLabel(getResource("button_copy"),getResource("tutorial_topBar_page_4_1"));
        IconLabel page4_2=new IconLabel(getResource("button_cut"),getResource("tutorial_topBar_page_4_2"));
        IconLabel page4_3=new IconLabel(getResource("button_paste"),getResource("tutorial_topBar_page_4_3"));
        JPanel page4=new JPanel();
        page4.setLayout(new GridLayout(0,1));
        page4.add(page4_1);
        page4.add(page4_2);
        page4.add(page4_3);

        IconLabel page5_1=new IconLabel(getResource("git_logo"),getResource("tutorial_topBar_page_5_1"));
        IconLabel page5_2=new IconLabel(getResource("button_pull"),getResource("tutorial_topBar_page_5_2"));
        IconLabel page5_3=new IconLabel(getResource("button_commit"),getResource("tutorial_topBar_page_5_3"));
        IconLabel page5_4=new IconLabel(getResource("button_push"),getResource("tutorial_topBar_page_5_4"));
        JPanel page5=new JPanel();
        page5.setLayout(new GridLayout(0,1));
        page5.add(page5_1);
        page5.add(page5_2);
        page5.add(page5_3);
        page5.add(page5_4);

        preparePopup(toolbar,
                new Point(toolbar.getWidth()/5,toolbar.getHeight()+10),
                onClose,"tutorial_topBar_title",page1,page2,page3,page4,page5);
    }

    private static void explainChecker(GraphFrame frame, JTabbedPane checkerPanel, ActionListener onClose) {
        IconLabel page1=new IconLabel("",getResource("tutorial_checker_page_1"),"/videos/model_checker.gif");
        page1.setBorder(BorderFactory.createLoweredBevelBorder());
        preparePopup(checkerPanel.getSelectedComponent(),
                new Point(10,-200),
                onClose,"tutorial_checker_title",page1);

    }

    private static void explainMenuBar(GraphFrame frame, JMenuBar menuBar, ActionListener onClose) {
        JLabel page1=new JLabel(getResource("tutorial_MenuBar_page_1"));
        page1.setBorder(BorderFactory.createLoweredBevelBorder());
        preparePopup(menuBar,
                new Point(menuBar.getWidth()/4,30),
                onClose,"tutorial_MenuBar_title",page1);
    }
}
