package addtool.nui;

import addtool.add.model.ADD;
import addtool.add.model.Vertex;
import addtool.helpers.preferences.BooleanPreference;
import addtool.helpers.preferences.Preference;
import addtool.helpers.preferences.PreferenceManager;
import addtool.nui.mxWrapper.nADDGraph;
import addtool.nui.mxWrapper.nGraph;
import addtool.nui.mxWrapper.nGraphComponent;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import addtool.helpers.ADDIOHandler;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static addtool.helpers.preferences.LanguageSupport.getResource;
import static addtool.helpers.preferences.LanguageSupport.getTooltip;

/**
 * Popup menu for right-click handling.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public final class ContainerMenu {
    private static final int MAX_CONTEXT_LENGTH = 6;

    private ContainerMenu() {
    }

    /**
     * Context option to open popup menu on right click on vertex
     * @param target the cell that was targeted by the secondary mous click
     * @param graphComponent the parent component
     */
    public static void openPopupMenu(mxCell target, nGraphComponent graphComponent){
        if(graphComponent.getGraph().isSwimlane(target)){
            openPopupMenuContainer(target,graphComponent);
        }else{
            openPopupMenuCell(target,graphComponent);
        }
    }

    private static void openPopupMenuCell(mxCell target,nGraphComponent graphComponent) {
        JPopupMenu menu=getBlankMenu(target,graphComponent);
        JMenuItem removeCell = UIHelper.getMenuItem("remove_cell",-1,null,
                e -> graphComponent.getGraph().removeCells(new Object[]{target})
        );
        menu.add(removeCell);

        if(target.isVertex()) {
            JMenuItem editVertex = new JMenuItem(getResource("edit_vertex"));
            editVertex.setToolTipText(getTooltip("edit_vertex"));
            editVertex.addActionListener(
                    e -> SwingUtilities.getUIActionMap(graphComponent).get("edit").actionPerformed(
                            new ActionEvent(graphComponent, 0, "edit"))
            );
            JMenuItem editLabel = new JMenuItem(getResource("edit_label"));
            editLabel.addActionListener(e -> {
                nGraph graph = graphComponent.getGraph();
                if (graph instanceof nADDGraph) {
                    Vertex v = ((nADDGraph) graph).getVertex(target, true);
                    if (v == null) {
                        return;
                    }
                    //String newLabel=JOptionPane.showInputDialog(graphComponent,getResource("enter_value"),v.getName());
                    String newLabel = (String) JOptionPane.showInputDialog(graphComponent,
                            getResource("enter_value"),
                            getResource("edit_label"),
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            null,
                            v.getName());
                    v.setName(newLabel);
                    graph.updateCells();
                }
            });
            menu.add(editVertex);
            menu.add(editLabel);

            if (graphComponent.getGraph() instanceof nADDGraph) {
                menu.addSeparator();
                menu.add(new JLabel(getResource("change_Operator")));
                nADDGraph graph = (nADDGraph) graphComponent.getGraph();
                Vertex old = graph.getVertex(target, true);
                ArrayList<String> options = new ArrayList<>(List.of(VertexEditPane.operators));
                options.remove("BEPLAYER");
                options.add("BEATTACKER");
                options.add("BEDEFENDER");
                int iteration = 0;
                for (String s : options) {
                    if (iteration >= MAX_CONTEXT_LENGTH) {
                        break;
                    }
                    String key = "show_" + s;
                    Preference preference = PreferenceManager.getPreferenceManager().getPreference(key);
                    if (preference instanceof BooleanPreference && ((BooleanPreference) preference).getValue()) {
                        JMenuItem change = UIHelper.getMenuItem(s, -1, null, e -> {
                            graph.put(target,
                                    VertexEditPane.changeType(old, s));
                            graph.updateCells();
                        });
                        change.setToolTipText(getTooltip(("add_" + s).toLowerCase()));
                        menu.add(change);
                        iteration++;
                    }
                }
            }
            /*
            //This allows packing a component into a swim lane. But it is still buggy with in/outgoing connections. So it won't be in the next release
            mxCell[] component=graphComponent.getGraph().checkForCollapsing(target);
            if(component!=null){
                JMenuItem it=new JMenuItem(getResource("container_pack"));
                it.addActionListener(e->{
                    mxCell container=graphComponent.getGraph().prepareContainerForComponent(component);
                    graphComponent.getGraph().addToContainer(container,component);
                });
                menu.add(it);
            }else{
                menu.add(new JMenuItem(getResource("component_not_found")));
            }*/
        }
        renderBlankMenu(menu,target,graphComponent);
    }

    private static void openPopupMenuContainer(mxCell target, nGraphComponent graphComponent) {
        JPopupMenu menu=getBlankMenu(target,graphComponent);

        JMenuItem unpackMenu=new JMenuItem(getResource("container_unpack"));
        unpackMenu.addActionListener(e-> graphComponent.getGraph().removeFromContainer(target));
        menu.add(unpackMenu);

        JMenuItem exportMenu=new JMenuItem(getResource("component_export"));
        exportMenu.addActionListener(e->{
            nGraph graph=graphComponent.getGraph();
            if(graph instanceof nADDGraph){
                ADD adg=((nADDGraph)graph).transformComponent(target);
                ADDIOHandler.export(ADDIOHandler.selectFormat(graphComponent),
                        ADDIOHandler.chooseFile(f->true,graphComponent,true),
                        adg);
            }
        });
        menu.add(exportMenu);

        renderBlankMenu(menu,target,graphComponent);
    }
    private static JPopupMenu getBlankMenu(mxCell target,nGraphComponent graphComponent){
        // build popup menu
        return new JPopupMenu();
    }
    private static void renderBlankMenu(JPopupMenu popup,mxCell target,nGraphComponent graphComponent){
        // build popup menu
        Point targetPoint;
        if(target.isVertex()){
            mxGeometry geo=target.getGeometry();
            targetPoint=new Point(Double.valueOf(geo.getX()+geo.getWidth()).intValue(),Double.valueOf(geo.getY()).intValue());
        }else{
            targetPoint=target.getGeometry().getTargetPoint().getPoint();
        }
        popup.show(graphComponent,targetPoint.x,targetPoint.y);
    }
}
