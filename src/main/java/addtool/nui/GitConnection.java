package addtool.nui;

import addtool.helpers.ExceptionHandler;
import addtool.helpers.preferences.PreferenceManager;
import addtool.helpers.preferences.StringPreference;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.dircache.DirCache;
import org.eclipse.jgit.dircache.DirCacheIterator;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.EmptyTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilterGroup;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.util.Collections;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Helper class to handle connection to git repositories
 * Does most things automatically
 * @author Florian Dorfhuber
 */
public final class GitConnection {
    /**
     * Preference Key for the username
     */
    public static final String PREF_NAME="gitName";
    /**
     * Preference Key for the password/token
     * NOTE: the data will only be permanently saved if changed in the settings.
     */
    public static final String PREF_PW="gitPassword";
    static {
        StringPreference gitName=new StringPreference(PREF_NAME,PREF_NAME,"connections/git","");
        StringPreference gitPW=new StringPreference(PREF_PW,PREF_PW,"connections/git","");
        PreferenceManager.getPreferenceManager().putPreferences(gitName,gitPW);
    }

    /**
     * Returns the subpath of a file relative to the repository root
     * Also changes separator from \\ to / if needed
     * i.e. Repository: C:\Programs\
     * File: C:\Programs\data\file.txt
     * Result: data/file.txt
     * @param target The File that should be added or something else
     * @param parent The root file of the repository
     * @return A string with the relative path
     */
    public static String getPatternFromParent(File target,File parent){
        if(target.getAbsolutePath().contains(parent.getAbsolutePath())){
            return (target.getAbsolutePath().substring(parent.getAbsolutePath().length()+1))
                    .replaceAll("\\\\","/");//+1 is for the separator
        }
        return target.getName();
    }

    /**
     * Checks if a given file is inside a repository (not necessarily tracked)
     * @param f The file
     * @return true if it is part of the file tree based in the repository root
     * @throws IOException If the file can not be accessed, or the repository data is faulty
     */
    public static boolean isInRepo(File f) throws IOException {
        if(true){
            return false;
        }
        if(f==null||f.getParentFile()==null){
            return false;
        }
        try{
            Repository repository = Git.open(f).getRepository();
            return true;
        }catch (RepositoryNotFoundException ex){
            return isInRepo(f.getParentFile());
        }
    }

    /**
     * Factory method for a Git connection. It is based on a file being inside the repository
     * @param f A file to start search
     * @return A GitConnection if the file is inside a Repository and a parent directory was found
     * @throws IOException If access problems occur
     */
    public static GitConnection getFromFile(File f) throws IOException {
        if(f==null||f.getParentFile()==null){//Workaround do not allow root of drive
            return null;
        }
        try{
            Git git=Git.open(f);
            Repository repository = git.getRepository();
            GitConnection connection= new GitConnection(f,git,repository);
            //Add credentials if preferences are set
            if(!PreferenceManager.getPreferenceManager().getPreference(PREF_NAME).getValueString().isEmpty()&&
                    !PreferenceManager.getPreferenceManager().getPreference(PREF_PW).getValueString().isEmpty()){
                connection.setCredentialsProvider(new UsernamePasswordCredentialsProvider(
                        PreferenceManager.getPreferenceManager().getPreference(PREF_NAME).getValueString(),
                        PreferenceManager.getPreferenceManager().getPreference(PREF_PW).getValueString()));
            }
            return connection;
        }catch (RepositoryNotFoundException ex){
            return getFromFile(f.getParentFile());
        }
    }
    public String toString(){
        return "Connection ["+repository+ '|' +parent+ ']';
    }

    private final Repository repository;
    private final Git git;
    private CredentialsProvider provider = null;
    private final File parent;
    private Component parentComponent=null;
    boolean loginOnFail=false;
    private GitConnection(File parent,Git git,Repository repository){
        this.repository=repository;
        this.git=git;
        this.parent=parent;
    }

    /**
     * Changes the credentials provider of the repo.
     * Usually it is not needed as user/password is standard. But can be used to implement certificate connection.
     * @param provider The new CredentialsProvider
     */
    public void setCredentialsProvider(CredentialsProvider provider){
        this.provider=provider;
    }


    /**
     * @author <a href="https://gist.github.com/adietish/3434748">adietish</a>
     * @param f The file that should be tracked
     * @return true if the file is already tracked
     * @throws IOException on access problems with the file tree
     */
    public boolean trackedInRepo(File f) throws IOException {
        ObjectId objectId = repository.resolve(Constants.HEAD);
        @Nullable RevTree tree;
        if (objectId != null)
            tree = new RevWalk(repository).parseTree(objectId);
        else
            tree = null;

        TreeWalk treeWalk = new TreeWalk(repository);
        treeWalk.setRecursive(true);
        if (tree != null)
            treeWalk.addTree(tree);
        else
            treeWalk.addTree(new EmptyTreeIterator());
        treeWalk.addTree(new DirCacheIterator(repository.readDirCache()));
        treeWalk.setFilter(PathFilterGroup.createFromStrings(Collections.singleton(
                Repository.stripWorkDir(repository.getWorkTree(), f))));
        return treeWalk.next();
    }

    /**
     * Adds a file to the repository
     * @param pattern a path relative to the repository root
     * @throws GitAPIException
     * @see GitConnection#add(File)
     * @see GitConnection#getPatternFromParent(File, File)
     */
    public void add(String pattern) throws GitAPIException {
        AddCommand add = git.add();
        add.addFilepattern(pattern);
        DirCache call = add.call();
    }

    /**
     * Adds a file to the repository
     * @param f the File to add
     * @throws GitAPIException
     * @see GitConnection#add(String)
     * @see GitConnection#getPatternFromParent(File, File)
     */
    public void add(File f) throws GitAPIException {
        add(getPatternFromParent(f,parent));
    }

    /**
     * Commit changes to the branch
     * @param message A string to be shown in the history
     * @return true if the operation was successful, false if an Exception was thrown
     */
    public boolean commit(String message){
        try {
            CommitCommand commit = git.commit();
            commit.setMessage(message);
            commit.setCredentialsProvider(provider);
            commit.call();
            return true;
        } catch (GitAPIException ex){
            ExceptionHandler.logException(ex);
            if(loginOnFail&&updateCredentialProvider()){//only shows dialog if activated
                return commit(message);
            }
        }
        return false;
    }

    /**
     * Pulls remote changes of the branch
     * May open a credentials dialog if connection failed
     * Depends on loginOnFail Setting
     * @return true on success, false on Errors
     */
    public boolean pull() {
        try {
            PullCommand pull = git.pull();
            pull.setCredentialsProvider(provider);
            pull.call();
            return true;
        } catch (GitAPIException ex){
            ExceptionHandler.logException(ex);
            if(loginOnFail&&updateCredentialProvider()){//only shows dialog if activated
                return pull();
            }
        }
        return false;
    }

    /**
     * Pushes Changes to remote branch
     * May ask for credentials on Error if loginOnFail is set.
     * @return tru on success, false if a problem exists
     */
    public boolean push() {
        try{
            PushCommand push = git.push();
            push.setCredentialsProvider(provider);
            Iterable<PushResult> call = push.call();
            return true;
        } catch (GitAPIException ex){
            ExceptionHandler.logException(ex);
            if(loginOnFail&&updateCredentialProvider()){//only shows dialog if activated
                return push();
            }
        }
        return false;
    }

    /**
     * Get repository root file
     * @return The root of the repository
     */
    public File getRoot(){
        return this.parent;
    }

    /**
     * Sets if a credentials dialog should be shown on failed login
     * The parent is needed for the later dialog
     * @param parentComponent The Component to determine the Window parent of the dialog
     * @param loginOnFail true if a login attempted via dialog should be started
     */
    public void setLoginOnFail(Component parentComponent,boolean loginOnFail){
        this.parentComponent=parentComponent;
        this.loginOnFail=loginOnFail;
    }

    /**
     * Sets if a credentials dialog should be shown on failed login
     * @param loginOnFail true if a login attempted via dialog should be started
     */
    public void setLoginOnFail(boolean loginOnFail) {
        setLoginOnFail(null,loginOnFail);
    }
    /**
     * @return true if the value was updated successfully
     */
    private boolean updateCredentialProvider(){
        JTextField username = new JTextField(PreferenceManager.getPreferenceManager().getPreference(PREF_NAME).getValueString());
        JTextField password = new JPasswordField(PreferenceManager.getPreferenceManager().getPreference(PREF_PW).getValueString());
        JLabel info=new JLabel();
        Object[] message = {
                getResource("username"), username,
                getResource("password"), password,
                getResource("git_not_saved"),info
        };

        int option = JOptionPane.showConfirmDialog(parentComponent, message, getResource("login"), JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            provider=new UsernamePasswordCredentialsProvider(username.getText(),password.getText());
            return true;
        } else {
            return false;
        }
    }

}
