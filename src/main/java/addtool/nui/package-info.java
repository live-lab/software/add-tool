/**
 * This Package holds the classes for the new ADD GUI.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
package addtool.nui;