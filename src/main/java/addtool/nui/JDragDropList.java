package addtool.nui;

import addtool.add.model.Vertex;

import javax.swing.*;
import java.util.Arrays;

/**
 * A List with Drag and Drop support for vertices.
 * The actual list is wrapped in a JPanel
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 */
public class JDragDropList extends JPanel  {
    /**
     * A JList with vertices.
     */
    private JList<Vertex> list;
    /**
     * An array with the original vertices for the list.
     */
    final Vertex [] predecessors;

    /**
     * sets up the list object with the given predecessors.
     * @param predecessors vertices to display in the list
     */
    public JDragDropList(Vertex... predecessors) {
        super();
        add(createList(predecessors));
        this.predecessors =Arrays.copyOf(predecessors, predecessors.length);
    }

    /**
     * initializes {@link JDragDropList#list}.
     * @param predecessors vertices to display in the list
     * @return A {@link JScrollPane} containing the List
     */
    private JScrollPane createList(Vertex ... predecessors) {
        DefaultListModel<Vertex> listModel = new DefaultListModel<>();

        for (Vertex v:predecessors) {
            if(v!=null) {
                listModel.addElement(v);
            }
        }

        list = new JList<>(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        JScrollPane scrollPane = new JScrollPane(list);
        list.setDragEnabled(true);
        list.setDropMode(DropMode.INSERT);
        list.setTransferHandler(new ListTransferHandler());
        list.setCellRenderer(new VertexRenderer());

        return scrollPane;
    }

    /**
     * Returns the vertices in the current display order.
     * @return the {@link JDragDropList#predecessors} in the current order
     */
    public Vertex[] getOrderedPredecessors(){
        Vertex[]out=new Vertex[predecessors.length];
        for(int i=0;i<list.getModel().getSize();i++){
            out[i]=list.getModel().getElementAt(i);
        }
        return out;
    }
    @Override
    public void setEnabled(boolean enabled){
        super.setEnabled(enabled);
        list.setEnabled(enabled);
    }
}
