package addtool.nui;

import addtool.add.model.ADD;
import addtool.add.model.BasicEvent;
import addtool.add.model.Vertex;
import addtool.analysisonadd.CostHeuristic;
import addtool.analysisonadd.DelayHeuristic;
import addtool.dot2add.RationalFromString;
import addtool.helpers.preferences.PreferenceManager;
import addtool.timeseries.PACTuple;
import org.jscience.mathematics.number.Rational;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Manages ADT Requirements on Git commit
 */
public class Requirement {
    /**
     * Key for minimum amount of nodes in a model
     */
    public static final String KEY_MIN_NODES="minNodes";
    /**
     * Key for maximum amount of nodes in a model
     */
    public static final String KEY_MAX_NODES="maxNodes";
    /**
     * Key for minimum success probability of model
     */
    public static final String KEY_MIN_PROB="minProb";
    /**
     * Key for maximum success probability of model
     */
    public static final String KEY_MAX_PROB="maxProb";
    /**
     * Key for minimum minimal cost for an attack
     */
    public static final String KEY_MIN_COST="minCost";
    /**
     * Key for maximum minimal cost for an attack
     */
    public static final String KEY_MAX_COST="maxCost";
    /**
     * Key for minimum minimal delay for an attack
     */
    public static final String KEY_MIN_DELAY="minDelay";
    /**
     * Key for maximum minimal delay for an attack
     */
    public static final String KEY_MAX_DELAY="maxDelay";

    /**
     * Key for the root node in the xml
     */
    public static final String KEY_VALUE="value";
    private static final Map<String, BiPredicate<ADD,Rational>> tests=new HashMap<>();
    static {
        tests.put(KEY_MIN_NODES,Requirement::checkMinNodes);
        tests.put(KEY_MAX_NODES,Requirement::checkMaxNodes);
        tests.put(KEY_MIN_PROB,Requirement::checkMinProbability);
        tests.put(KEY_MAX_PROB,Requirement::checkMaxProbability);
        tests.put(KEY_MIN_COST,Requirement::checkMinCost);
        tests.put(KEY_MAX_COST,Requirement::checkMaxCost);
        tests.put(KEY_MIN_DELAY,Requirement::checkMinDelay);
        tests.put(KEY_MAX_DELAY,Requirement::checkMaxDelay);
    }

    /**
     * Checks the requirements contained in a file on a certain ADD
     * @param f the file with the requirements
     * @param model the model to check
     * @return true if all requirements are met.
     * @throws ParserConfigurationException If parsing of the xml failed
     * @throws IOException if the file was not accessible
     * @throws SAXException If the xml in the file is invalid
     * @see Requirement#fromFile(File)
     * @see Requirement#checkRequirements(ADD) 
     */
    public static boolean checkRequirements(File f, ADD model) throws ParserConfigurationException, IOException, SAXException {
        return fromFile(f).checkRequirements(model);

    }

    /**
     * Gets feedback on requirements in a file
     * @param f the file with the requirements
     * @param model the model to check
     * @return A string with feedback, it is empty if the requirements are met.
     * @throws ParserConfigurationException If parsing of the xml failed
     * @throws IOException if the file was not accessible
     * @throws SAXException If the xml in the file is invalid
     * @see Requirement#fromFile(File)
     * @see Requirement#getFeedback(ADD)
     */
    public static String getFeedback(File f, ADD model) throws ParserConfigurationException, IOException, SAXException {
        return fromFile(f).getFeedback(model);

    }

    /**
     * Opens an edit dialog for requirements
     * @param parent the parent window for the dialog
     * @param target the file to modify
     * @throws ParserConfigurationException If parsing of the xml failed
     * @throws IOException if the file was not accessible
     * @throws SAXException If the xml in the file is invalid
     * @see Requirement#fromFile(File)
     * @see Requirement#openEditDialog(Component, File) 
     */
    public static void edit(Component parent,File target) throws ParserConfigurationException, IOException, SAXException {
        fromFile(target).openEditDialog(parent,target);
    }

    /**
     * 
     * @param f Factory method for requirements based on a file
     * @return the requirement object contained in the file or an empty one if the file is non-existent
     * @throws ParserConfigurationException If parsing of the xml failed
     * @throws IOException if the file was not accessible
     * @throws SAXException If the xml in the file is invalid
     */
    public static Requirement fromFile(File f) throws ParserConfigurationException, IOException, SAXException {
        if(!f.exists()){
            return new Requirement();
        }
        Map<String,Rational> values=new HashMap<>();
        Consumer<Node> consumer= node -> {
            if(!node.hasAttributes())return;
            String key=node.getNodeName();
            Node named=node.getAttributes().getNamedItem(KEY_VALUE);
            if(named!=null){
                Rational value = RationalFromString.getRationalFromString(named.getNodeValue());
                values.put(key,value);
            }
        };
        PreferenceManager.readDoc(PreferenceManager.prepareDocumentBuilder().parse(f),consumer);
        return new Requirement(values);
    }

    private Map<String,Rational> bounds;

    /**
     * Creates new Requirement object with no restrictions
     */
    public Requirement(){
        this(new HashMap<>());
    }
    /**
     * Creates new Requirement object with the given restrictions
     * @param values a map with keys and their bounds
     */
    public Requirement(Map<String, Rational> values){
        bounds=new HashMap<>(values);
    }
    /**
     * Opens an edit dialog for requirements
     * @param parent the parent window for the dialog
     * @param f the file to modify
     */
    public void openEditDialog(Component parent, File f){
        Map<String, JTextField> values=tests.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                e->new JTextField((bounds.containsKey(e.getKey())?String.valueOf(bounds.get(e.getKey()).doubleValue()):""))));
        List<String> keys=values.keySet().stream().sorted().collect(Collectors.toList());
        List<Object> elements=new ArrayList<>();
        for(String key:keys){
            elements.add(getResource(key));
            elements.add(values.get(key));
        }
        Object[] message = elements.toArray();
        int option = JOptionPane.showConfirmDialog(parent, message, getResource("requirement_edit_title"), JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            this.bounds=values.entrySet().stream().
                    filter(e->!e.getValue().getText().isEmpty()).
                    collect(Collectors.toMap(Map.Entry::getKey, e->RationalFromString.getRationalFromString(e.getValue().getText())));
            save(f);
        }
    }

    /**
     * saves requirements to a given file
     * @param f the target file
     */
    public void save(File f){
        PreferenceManager.writeXML(f,bounds,
                (key,bound,node)-> node.setAttribute(KEY_VALUE,String.valueOf(bound.doubleValue())),"bounds");
    }

    /**
     * Checks the bounds on a given model
     * @param model the model to check
     * @return true if all requirements are met
     */
    public boolean checkRequirements(ADD model){
        return bounds.entrySet().stream().allMatch(e->tests.get(e.getKey()).test(model,e.getValue()));
    }
    /**
     * Checks the bounds on a given model and returns an error string
     * @param model the model to check
     * @return an empty string if all requirements are met, or a text with lines for every issue
     * this is language settign dependend 
     * @see addtool.helpers.preferences.LanguageSupport#getResource(String)
     */
    public String getFeedback(ADD model){
        return bounds.entrySet().stream().map(e->{
            if(tests.get(e.getKey()).test(model,e.getValue())){
                return "";
            }else{
                return String.format(getResource("requirement_mask"),
                        getResource(e.getKey()+"_feedback"),
                        e.getValue().doubleValue());
            }
        }).filter(s->!s.isEmpty()).collect(Collectors.joining("\n"));
    }

    private static boolean checkMinNodes(ADD model, Rational min){
        return model.getId2Vertex().size()>=min.intValue();
    }
    private static boolean checkMaxNodes(ADD model, Rational max){
        return model.getId2Vertex().size()<=max.intValue();
    }
    private static boolean checkMinDelay(ADD model, Rational min){
        return checkDelay(model,(v,dh)->dh.getDelay().get(v).getValue().compareTo(min)>=0);
    }
    private static boolean checkMaxDelay(ADD model, Rational max){
        return checkDelay(model,(v,dh)->dh.getDelay().get(v).getValue().compareTo(max)<=0);
    }
    private static boolean checkMinCost(ADD model, Rational min){
        return checkCost(model,(v,dh)->dh.getCost().get(v).getValue().compareTo(min)>=0);
    }
    private static boolean checkMaxCost(ADD model, Rational max){
        return checkCost(model,(v,dh)->dh.getCost().get(v).getValue().compareTo(max)<=0);
    }
    private static boolean checkMinProbability(ADD model, Rational min){
        return checkProbability(model,v->v.getDisruptionProb().compareTo(min)>=0);
    }
    private static boolean checkMaxProbability(ADD model, Rational max){
        return checkProbability(model,v->v.getDisruptionProb().compareTo(max)<=0);
    }

    private static boolean checkCost(ADD model, BiPredicate<Vertex,CostHeuristic> tester){
        Map<BasicEvent, PACTuple> map= model.getBasicEvents().stream()
                .map(v->(BasicEvent)v)
                .collect(Collectors.toMap(v->v, BasicEvent::getCosts));
        CostHeuristic ch=new CostHeuristic(map);
        model.accept(ch);
        return model.getNoSuccessor().stream().allMatch(v->tester.test(v,ch));
    }
    private static boolean checkDelay(ADD model, BiPredicate<Vertex,DelayHeuristic> tester){
        Map<BasicEvent, PACTuple> map= model.getBasicEvents().stream()
                .map(v->(BasicEvent)v)
                .collect(Collectors.toMap(v->v, BasicEvent::getDelay));
        DelayHeuristic ch=new DelayHeuristic(map);
        model.accept(ch);
        return model.getNoSuccessor().stream().allMatch(v->tester.test(v,ch));
    }
    private static boolean checkProbability(ADD model, Predicate<Vertex> tester){
        return model.getNoSuccessor().stream().allMatch(tester);
    }
}
