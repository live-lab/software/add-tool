@startuml

title __LEARNING's Class Diagram__\n

  namespace  {
    namespace ddtool.learning {
      class addtool.learning.DataSet {
          - data : Map<Sequence, Values>
          - size : int
          + clear()
          + compute()
          + computeIfAbsent()
          + computeIfPresent()
          + merge()
          + put()
          + put()
          + put()
          + putAgain()
          + putAll()
          + putIfAbsent()
          + remove()
          + remove()
          + replace()
          + replace()
          + replaceAll()
          + size()
          - updateSize()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      class addtool.learning.DataSet.Values {
          - weight : int
          + Values()
          + Values()
          + Values()
          + Values()
          + Values()
          + equals()
          + getBooleanLabel()
          + getLabel()
          + getWeight()
          + hashCode()
          - setWeight()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      class addtool.learning.FuzzyADD {
          ~ children : CopyOnWriteArrayList<ADD>
          + FuzzyADD()
          + addADD()
          + evaluateSequence()
          + evaluateSequenceLikely()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      class addtool.learning.LearnADD {
          - EPSILON : int
          - MAX_EPSILON : int
          - breakUp : boolean
          - changes : Map<ADD, Collection<FeedbackTuple>>
          - curr : File
          - isActive : boolean
          {static} - lastFitness : Rational
          - pool : Map<ADD, Rational>
          - rand : Random
          - tar : File
          - totalSize : Rational
          - totalWeight : Rational
          - watchTimeout : ScheduledExecutorService
          - weights : Map<Operation, Integer>
          + LearnADD()
          + clear()
          {static} + crossValidate()
          + deriveParams()
          {static} + evaluate()
          {static} + evaluate()
          + getBest()
          + getCachedFitness()
          + getChanges()
          {static} + getLastFitness()
          + getMomentFromPool()
          + getRand()
          + getWeights()
          + getWorst()
          {static} + main()
          + putFeedBack()
          + replaceIfBetter()
          + resetTimeout()
          + runFile()
          + runGeneration()
          + runGenerationZero()
          + runModel()
          + setWeights()
          + validateModel()
          + validateModel()
          # readTracesFromDot()
          # readTracesFromDot()
          - addModelToCurrent()
          - addModelToCurrent()
          - addModelToCurrent()
          - addModelToCurrent()
          - canContinue()
          - cleanMaps()
          - decreaseEpsilon()
          {static} - getProgressBar()
          {static} - getProgressBar()
          - increaseEpsilon()
          - initTimeout()
          - isActive()
          - openEditor()
          - resetWeights()
          - runModel()
          - writeEpochFitness()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      enum Operation {
        ADD_UNARY
        REMOVE_UNARY
        SWITCH_BE
        SWITCH_OP
        SWITCH_ORDER
        op
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      class addtool.learning.LearnFuzzyADD {
          ~ batchSize : int
          {static} ~ batches : int
          + LearnFuzzyADD()
          {static} + main()
          + runModel()
          + runModelWOnly()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      interface addtool.learning.LearnHelper {
          {static} + compareModel()
          {static} + generateRandomTree()
          {static} + generateSequences()
          {static} + generateSequences()
          {static} + generateSequences()
          {static} + getBatches()
          {static} + getBatches()
          {static} + getBeFromTraces()
          {static} + getBeFromTraces()
          {static} + getBeFromTraces()
          {static} + getFalsePositives()
          {static} + getLabelsFromFile()
          {static} + getNumFailures()
          {static} + getNumSuccesses()
          {static} + getPartitions()
          {static} + getSensitivity()
          {static} + getSpecificity()
          {static} + joinDataSet()
          {static} + splitDataSet()
          {static} + splitDataSet()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      class addtool.learning.LearnerConstraints {
          {static} + ACTIONS_KEY : String
          {static} + EPOCHS_KEY : String
          {static} + EPSILON_KEY : String
          {static} + ER_KEY : String
          {static} + META_KEY : String
          {static} + MUTATION_KEY : String
          {static} + OPERATORS_KEY : String
          {static} + ORDERED_KEY : String
          {static} + OUTPUT_TRACES_KEY : String
          {static} + POOL_SIZE_KEY : String
          {static} + SEED_KEY : String
          {static} + SHOW_EDITOR_KEY : String
          {static} + SUCCESS_KEY : String
          {static} + SUCCESS_RATE_KEY : String
          {static} + SUFFIX_KEY : String
          {static} + SUPPRESS_WARNINGS_KEY : String
          {static} + TIMEOUT_KEY : String
          {static} + TRACES_KEY : String
          {static} + TRAIN_KEY : String
          {static} + WEIGHT_KEY : String
          {static} + WIDTH_KEY : String
          - EPOCHS : int
          - EPOCH_WIDTH : int
          - EPSILON : int
          - MAX_EPSILON : int
          - MUTATION_RATE : double
          - NUM_ACTIONS : int
          - POOL_SIZE : int
          - TRAIN_RATE : double
          - TRAIN_SIZE : int
          - epochResultOut : boolean
          - fitnessSuffix : String
          - metaOutput : boolean
          - ops : List<Vertex>
          - ordered : boolean
          - outputTraces : boolean
          - seed : int
          - showEditor : boolean
          - success : int
          - successRate : double
          - suppressWarnings : boolean
          - timeout : int
          - w1 : Rational
          + LearnerConstraints()
          + buildArgs()
          + buildSingleArgs()
          + clone()
          + digestArgs()
          + digestArgs()
          + digestArgs()
          + getEPOCHS()
          + getEPOCH_WIDTH()
          + getEPSILON()
          + getFitnessSuffix()
          + getMAX_EPSILON()
          + getMUTATION_RATE()
          + getNUM_ACTIONS()
          + getOps()
          + getOpsCode()
          + getPOOL_SIZE()
          + getSeed()
          + getSuccess()
          + getSuccessRate()
          + getTRAIN_RATE()
          + getTRAIN_SIZE()
          + getTimeout()
          + getW1()
          + isEpochResultOut()
          + isMetaOutput()
          + isOrdered()
          + isOutputTraces()
          + isShowEditor()
          + isSuppressWarnings()
          + setEPOCHS()
          + setEPOCH_WIDTH()
          + setEPSILON()
          + setEpochResultOut()
          + setFitnessSuffix()
          + setMUTATION_RATE()
          + setMetaOutput()
          + setNUM_ACTIONS()
          + setOps()
          + setOps()
          + setOrdered()
          + setOutputTraces()
          + setPOOL_SIZE()
          + setSeed()
          + setShowEditor()
          + setSuccess()
          + setSuccessRate()
          + setSuppressWarnings()
          + setTRAIN_RATE()
          + setTRAIN_SIZE()
          + setTimeout()
          + setW1()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      class addtool.learning.Sequence {
          - events : List<String>
          - ordered : boolean
          + Sequence()
          + contains()
          + equals()
          + evaluateOnADD()
          + getEvents()
          + hashCode()
          + isOrdered()
          + setOrdered()
          + size()
          + toString()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      class addtool.learning.Sequence2Csv {
          {static} + sep : char
          {static} + write2File()
          - Sequence2Csv()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      class addtool.learning.TraceIO {
          {static} + delimiter : String
          {static} + readBinTraces()
          {static} + readBinTraces()
          {static} + writeBinTraces()
          - TraceIO()
      }
    }
  }
  

  namespace  {
    namespace ddtool.learning {
      interface addtool.learning.TrinaryFunction {
          {abstract} + apply()
      }
    }
  }
  

  addtool.learning.DataSet -up-|> java.util.HashMap
  addtool.learning.DataSet +-down- addtool.learning.DataSet.Values
  addtool.learning.DataSet.Values o-- addtool.semantics.Value : label
  addtool.learning.LearnADD o-- addtool.learning.LearnerConstraints : learnerConstraints
  addtool.learning.LearnADD +-down- addtool.learning.LearnADD.Operation
  addtool.learning.LearnFuzzyADD o-- addtool.learning.LearnerConstraints : learnerConstraints
  addtool.learning.LearnerConstraints .up.|> java.lang.Cloneable


right footer


PlantUML diagram generated by SketchIt! (https://bitbucket.org/pmesmeur/sketch.it)
For more information about this tool, please contact philippe.mesmeur@gmail.com
endfooter

@enduml
