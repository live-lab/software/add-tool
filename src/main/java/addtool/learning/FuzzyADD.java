package addtool.learning;

import addtool.add.model.ADD;

import java.util.concurrent.CopyOnWriteArrayList;
//TODO handle ADD extend

/**
 * Extension of {@link ADD} holding multiple ADD in one.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 28.06.2021
 */
public class FuzzyADD {
    final CopyOnWriteArrayList<ADD> children;
    public FuzzyADD(){
        children =new CopyOnWriteArrayList<>();
    }
    public void addADD(ADD add){
        children.add(add);
    }
    public double evaluateSequenceLikely(Sequence s){
        return children.stream().mapToInt(a-> s.evaluateOnADD(a)?1:0).average().orElse(-1);
    }
    public boolean evaluateSequence(Sequence s){
        return evaluateSequenceLikely(s)>0.5;
    }
}
