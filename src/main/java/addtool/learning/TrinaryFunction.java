package addtool.learning;

@FunctionalInterface
public interface TrinaryFunction<T,U,V, R>  {
    R apply(T var1,U var2,V var3);
}
