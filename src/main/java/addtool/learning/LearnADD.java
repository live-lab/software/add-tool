package addtool.learning;

import addtool.add2dot.ADD2Dot;
import addtool.add.model.*;
import addtool.dot2add.*;
import addtool.feedback.FeedbackTuple;
import addtool.helpers.ExceptionHandler;
import addtool.helpers.Pair;
import addtool.nui.GraphFrame;
import addtool.nui.UIHelper;
import org.jscience.mathematics.number.Rational;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import static addtool.learning.LearnHelper.*;

/**
 * Class to learn {@link ADD} from Data using a genetic algorithm.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 4.11.20
 */
public class LearnADD {
    private static Rational lastFitness =Rational.ZERO;
    public static Rational getLastFitness(){
        return lastFitness;
    }

    /**
     * The possible operations to perform on an ADD.
     */
    enum Operation{
        SWITCH_OP(ADDBuilder::switchRandomOperator),
        SWITCH_BE((ab,rand,ops)->ab.switchBasicEvent(rand)),
        SWITCH_ORDER((ab,rand,ops)->ab.shufflePredecessorsInSequential(rand)),
        ADD_UNARY(ADDBuilder::insertUnary),
        REMOVE_UNARY((ab,rand,ops)->ab.removeUnary(rand));
        private final TrinaryFunction<ADDBuilder,Random,List<Vertex>,ADDBuilder> op;
        Operation(){
            this.op=(m,r,o)->m;
        }
        Operation(TrinaryFunction<ADDBuilder,Random,List<Vertex>,ADDBuilder> op){
            this.op=op;
        }
        public ADDBuilder apply(ADDBuilder model, Random rand,List<Vertex> ops){
            return op.apply(model,rand,ops);
        }
    }

    private Map<Operation,Integer> weights;
    private final Map<ADD,Rational>pool;
    private Map<ADD,Collection<FeedbackTuple>> changes;
    final LearnerConstraints learnerConstraints;
    private Random rand;
    private int EPSILON = 0;
    private int MAX_EPSILON = 0;
    private Rational totalSize;
    private Rational totalWeight;
    private boolean isActive;
    private boolean breakUp;
    private ScheduledExecutorService watchTimeout;

    public LearnADD(LearnerConstraints lc){
        pool=new HashMap<>();
        changes=new HashMap<>();
        weights= Arrays.stream(Operation.values()).
                collect(Collectors.toMap(o->o,o->1));
        learnerConstraints= lc.clone();
        rand=new Random(learnerConstraints.getSeed());
        totalSize =Rational.ZERO;
        totalWeight =Rational.ZERO;
        isActive=true;
        breakUp=false;
        watchTimeout=null;
    }

    public Rational getCachedFitness(ADD add){
        return pool.get(add);
    }
    public ADD getBest(){
        return getMomentFromPool(Rational::isLargerThan,Rational.ZERO);
    }
    public ADD getWorst(){
        return getMomentFromPool(Rational::isLessThan,Rational.ONE);
    }

    public Map<LearnADD.Operation, Integer> getWeights() {
        return new HashMap<>(weights);
    }

    public void setWeights(EnumMap<LearnADD.Operation, Integer> weights) {
        this.weights = new EnumMap<>(weights);
    }

    public Map<ADD, Collection<FeedbackTuple>> getChanges() {
        return new HashMap<>(changes);
    }

    public Random getRand() {
        return rand;
    }

    private void increaseEpsilon(){
        EPSILON+=2;
        if(EPSILON>MAX_EPSILON)EPSILON=MAX_EPSILON;
    }
    private void decreaseEpsilon(){
        EPSILON-=2;
        if(EPSILON<=MAX_EPSILON/3)EPSILON=MAX_EPSILON/3;
    }

    /**
     * Adds listening for Timeout.
     */
    private void initTimeout(){
        if(watchTimeout==null){
            watchTimeout=Executors.newScheduledThreadPool(2);
            watchTimeout.schedule(()->{isActive=false;},learnerConstraints.getTimeout(),TimeUnit.MINUTES);
            watchTimeout.schedule(()->{isActive=false;breakUp=true;},(int)(learnerConstraints.getTimeout()*1.2),TimeUnit.MINUTES);
        }
    }
    /**
     * Whether the Learner accepts new Tasks.
     * @return true if it accepts new tasks
     */
    private boolean isActive(){
        return isActive;
    }

    /**
     * Whether the Learner should break up after the current generation.
     * @return true if the next generation can be started
     */
    private boolean canContinue(){
        return isActive&&!breakUp;
    }
    public void resetTimeout(){
        isActive=true;
        breakUp=false;
        if(watchTimeout!=null){
            watchTimeout.shutdownNow();
            watchTimeout=null;
        }
    }

    /**
     * Returns a model being better on a given constraint.
     * @param compare The predicate to evaluate the Rationals
     * @param start The initial value for comparison
     * @return The model having the best score on the constraint
     */
    public ADD getMomentFromPool(BiPredicate<Rational,Rational> compare, Rational start){
        Rational extreme=start;
        ADD extremeADD=null;
        for(Map.Entry<ADD,Rational> e:pool.entrySet()){
            if(compare.test(e.getValue(),extreme)){
                extremeADD=e.getKey();
                extreme=e.getValue();
            }
        }
        return extremeADD;
    }
    public boolean replaceIfBetter(ADD add,Rational val){
        if(add==null||val==null)return false;
        if(pool.size()<learnerConstraints.getPOOL_SIZE()){
            pool.put(add,val);
            return true;
        }
        if(pool.containsKey(add))return false;
        //Do not add same result twice
        if(pool.values().stream().anyMatch(r->r.equals(val)))return false;
        ADD worstADD=getWorst();
        Rational worst=pool.get(worstADD);
        if((worst!=null&&worstADD!=null)&&val.isGreaterThan(worst)){
            pool.remove(worstADD);
            pool.put(add,val);
            return true;
        }
        return false;
    }
    public void putFeedBack(ADD add,List<FeedbackTuple> feedback){
        changes.put(add,feedback);
    }
    private void cleanMaps(){
        changes=changes.entrySet().stream().
                filter(e->pool.containsKey(e.getKey())).
                collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    /**
     * Runs a learning generation.
     * Results are stored in the {@link LearnADD#pool}
     * @param add0 The best model of the previous generation
     * @param ref The reference table with sequences and values (learning data)
     * @throws WrongStructureADDException If a generated tree has invalid structure
     */
    public void runGeneration(ADD add0,DataSet ref) throws WrongStructureADDException {
        Map<ADD,Rational> currentGen=new HashMap<>();
        Map<ADD,Collection<FeedbackTuple>> currentChange=new HashMap<>();

        for(int i=0;i<learnerConstraints.getEPOCH_WIDTH();i++){
            if(rand.nextDouble()<learnerConstraints.getMUTATION_RATE()||pool.size()<2){
                List<ADD> keysAsArray = new ArrayList<>(pool.keySet());
                ADD modify=keysAsArray.get(rand.nextInt(keysAsArray.size()));
                ADDBuilder builder=new ADDBuilder(modify);
                Map<Operation,Integer> nWeights= new EnumMap<>(Operation.class);
                int wdh=learnerConstraints.getNUM_ACTIONS();//rand.nextInt(NUM_ACTIONS);
                for(int j=0;j<wdh;j++) {
                    int max=weights.values().stream().mapToInt(v->v).sum();
                    int pos=rand.nextInt(max);

                    Operation exe=Operation.SWITCH_BE;
                    for(Map.Entry<Operation,Integer> o:weights.entrySet()){
                        pos-=o.getValue();
                        if(pos<=0){
                            exe=o.getKey();
                            break;
                        }
                    }

                    builder=exe.apply(builder,rand,learnerConstraints.getOps());

                    if(nWeights.containsKey(exe)){
                        nWeights.compute(exe,(k,v)->v==null?1:v+1);
                    }else{
                        nWeights.put(exe,1);
                    }
                }
                addModelToCurrent(builder,ref,currentGen,currentChange,builder.getLogger());
            }else{
                Set<ADD> adds=pool.keySet();
                int i1=rand.nextInt(adds.size());
                int i2;
                do{
                    i2=rand.nextInt(adds.size());
                }while(i2!=i1);
                ADDBuilder builder1=null;
                ADDBuilder builder2=null;
                int j=0;
                for(ADD add : adds)
                {
                    if (j == i1){
                        builder1=new ADDBuilder(add);
                    }
                    if(j==i2){
                        builder2=new ADDBuilder(add);
                    }
                    j++;
                }
                if(ADDBuilder.canCrossOver(builder1,builder2)){

                    ADDBuilder.crossOver(builder1,builder2,rand);
                    if(builder1 != null){
                        addModelToCurrent(builder1,ref,currentGen,currentChange,"Crossover");
                        addModelToCurrent(builder2,ref,currentGen,currentChange,"Crossover");
                    }
                }

            }
        }
        //Add epsilon models
        for(int i=0;i<EPSILON;i++){
            ADD neu=generateRandomTree(add0.getBasicEvents(),rand,learnerConstraints.getOps());
            addModelToCurrent(neu,ref,currentGen,currentChange,"Random Tree was used");
        }
        /*
         * Select fittest
         */
        currentGen.forEach((add,acc)->{
            if(replaceIfBetter(add,acc)){
                changes.put(add,currentChange.get(add));
            }
        });
    }
    private void addModelToCurrent(ADDBuilder builder,DataSet ref,Map<ADD,Rational> currentGen,Map<ADD,Collection<FeedbackTuple>> currentChange,String message){
        addModelToCurrent(builder.get(),ref,currentGen,currentChange,message);
    }
    private void addModelToCurrent(ADD model,DataSet ref,Map<ADD,Rational> currentGen,Map<ADD,Collection<FeedbackTuple>> currentChange,String message) {
        addModelToCurrent(model,ref,currentGen,currentChange,List.of(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.INFORMATION,message)));
    }
    private void addModelToCurrent(ADDBuilder builder,DataSet ref,Map<ADD,Rational> currentGen,Map<ADD,Collection<FeedbackTuple>> currentChange,Collection<FeedbackTuple> changes) {
        addModelToCurrent(builder.get(),ref,currentGen,currentChange,changes);
    }

    /**
     * Helper method for storing Data in the fields of a current generation.
     * @param model the model to store
     * @param ref the trace-set to evaluate the model on
     * @param currentGen the pre-pool for the current generation
     * @param currentChange  the list of changes for the current generation
     * @param changes the changes associated with the model to store
     */
    private void addModelToCurrent(ADD model,DataSet ref,Map<ADD,Rational> currentGen,Map<ADD,Collection<FeedbackTuple>> currentChange,Collection<FeedbackTuple> changes){
        Rational nco=compareModel(s->s.evaluateOnADD(model),ref,learnerConstraints.getW1());
        currentGen.put(model,nco);
        currentChange.put(model,changes);
    }
    /**
     * Runs generation zero of the algorithm.
     * The best are saved to the {@link LearnADD#pool}
     * @param bes A Set of possible actions
     * @param ref The training data (Map of Sequences and boolean if they evaluated to true)
     * @throws WrongStructureADDException if a model is invalid
     */
    public void runGenerationZero(Set<Vertex> bes,DataSet ref)
            throws WrongStructureADDException {
        for(int i=0;i<learnerConstraints.getEPOCH_WIDTH();i++){
            ADD mod=generateRandomTree(bes,rand,learnerConstraints.getOps());
            Rational nco=compareModel(s->s.evaluateOnADD(mod),ref,learnerConstraints.getW1());
            this.replaceIfBetter(mod,nco);
        }
    }

    public static void main(String...args) throws IOException,
            WrongStructureADDException, NoValueSpecifiedException,
            WrongValueForLabelOperatorException, WrongValueForSinkException, TimeoutException {
            File infile=new File("data/Learning/examples");
            if(args.length>1)infile=new File(args[1]);
            File outfile=new File(args.length>2?args[2]:"data/Learning/meta");
            LearnerConstraints lc=new LearnerConstraints();
            lc.digestArgs(args);
            LearnADD learner=new LearnADD(lc);

            File[] files;
            if(infile.isDirectory()){
                FilenameFilter fnf= (file, s) -> s.endsWith(".dot")||s.endsWith(".trace");
                files=infile.listFiles(fnf);
            }else{
                files=new File[]{infile};
            }
            if(files==null){
                System.err.println("No valid files found. Aborting.");
                return;
            }

            long startTime=System.currentTimeMillis();
            //System.out.println(List.of(files));
            for(File f:files){
                ADD end=learner.runFile(f,outfile,!infile.isDirectory()&&lc.isShowEditor());
                //Output model
                if(end!=null){
                    File target=new File(outfile, learner.learnerConstraints.getFitnessSuffix()+f.getName());
                    ADD2Dot.writeADD2DotFile(new PrintWriter(target, StandardCharsets.UTF_8),end);
                }
            }
    }

    /**
     * Outputs the performance of a given model on a given Dataset.
     * @param args a String array having the input File in place 1, datafile in 2 outputfile in 3.
     */
    public static void evaluate(String... args) throws WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, IOException, WrongValueForSinkException {
        if(args.length<4){
            System.err.println("Inappropriate parameter amount for LearnADD evaluate");
            return;
        }
        File inFile=new File(args[1]);
        File dataFile=new File(args[2]);
        File outputFile=new File(args[3]);
        String identifier=args.length<5?"":args[4];
        evaluate(inFile,dataFile,outputFile,identifier);
    }
    public static void crossValidate(String... args) throws WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, IOException, WrongValueForSinkException, TimeoutException {
        if(args.length<3){
            System.err.println("Inappropriate parameter amount for LearnADD evaluate");
            return;
        }
        File inFile=new File(args[1]);
        File outfile=new File(args[2]);


        LearnerConstraints lc=new LearnerConstraints();
        lc.digestArgs(args);

        /*lc.setEPOCHS(50);
        lc.setSeed(123);
        lc.setTRAIN_SIZE(1000);*/

        //lc.setTimeout(5);
        File[] fs;
        if(inFile.isDirectory()){
            fs=inFile.listFiles();
        }else{
            fs=new File[]{inFile};
        }
        Rational totalVar=Rational.ZERO;
        Rational totalVarEx2=Rational.ZERO;
        for(File f:fs){
            //Create Data
            Random random=new Random(lc.getSeed());
            ADD model= ParserDOT.parseADD(new FileReader(f.getAbsolutePath()));

            DataSet ref= generateSequences(model,0,lc.getTRAIN_SIZE(),random);
            DataSet train=new DataSet();
            DataSet test=new DataSet();
            splitDataSet(ref,train,test,random,lc.getTRAIN_RATE());

            LearnADD learner=new LearnADD(lc);

            Pair<Rational,Rational> results=learner.validateModel(model.getBasicEvents(),train,10);
            totalVar=totalVar.plus(results.getSecond());
            totalVarEx2=totalVarEx2.plus(results.getSecond().times(results.getSecond()));
            learner.resetTimeout();
        }
        totalVar=totalVar.divide(Rational.valueOf(fs.length,1));
        totalVarEx2=totalVarEx2.divide(Rational.valueOf(fs.length,1));
        Rational VAR=totalVarEx2.minus(totalVar.times(totalVar));
        System.out.printf("Mean cross validation Variance %.3f ± %.3f (μ ± σ)%n",
                totalVar.doubleValue(),
                Math.sqrt(VAR.doubleValue()));
    }
    public static void evaluate(File inFile,File dataFile,File outFile,String identifier) throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        ADD model=ParserDOT.parseADD(new FileReader(inFile));
        DataSet dataSet=TraceIO.readBinTraces(dataFile,true);

        Rational Sens= getSensitivity(model::evaluateSequence,dataSet);
        Rational Spec= getSpecificity(model::evaluateSequence,dataSet);
        //Rational Spec=LearnHelper.getSpecificity(add::evaluateSequence,test);
        System.out.printf(Locale.US, "Sensitivity %.6f; Specificity %.6f; Identifier %s%n",
                Sens.doubleValue(),
                Spec.doubleValue(),
                identifier);
        Path writeTO= outFile.toPath();
        Files.writeString(writeTO,
                String.format(java.util.Locale.US,"%s;%.6f;%.6f\n",
                        identifier,
                        Sens.doubleValue(),
                        Spec.doubleValue())
                , StandardOpenOption.CREATE,StandardOpenOption.APPEND);
    }
    private void resetWeights(){
        Map<Operation,Integer> nWeights=new EnumMap<>(Operation.class);
        boolean sequential=learnerConstraints.getOps().stream().anyMatch(v->v instanceof SAnd ||v instanceof SOr);
        boolean unary=learnerConstraints.getOps().stream().anyMatch(v-> v instanceof UnaryOp);

        weights.forEach((key, value) -> {
            if ((key.equals(Operation.ADD_UNARY) ||
                    key.equals(Operation.REMOVE_UNARY)) && !unary) {
                nWeights.put(key, 0);
            } else if (key.equals(Operation.SWITCH_ORDER) && !sequential) {
                nWeights.put(key, 0);
            } else {
                nWeights.put(key, 1);
            }
        });
        weights=nWeights;
    }

    /**
     * empties pool and changes.
     * reads transient params of constraints
     */
    public void deriveParams() {
        pool.clear();
        resetWeights();
        changes.clear();
        rand=new Random(learnerConstraints.getSeed());
        EPSILON=learnerConstraints.getEPSILON();
        MAX_EPSILON=learnerConstraints.getMAX_EPSILON();
    }
    public void clear(){
        deriveParams();
        totalSize =Rational.ZERO;
        totalWeight =Rational.ZERO;
    }
    private transient File curr = null;
    private transient File tar = null;
    public ADD runFile(File f,File target,boolean openEditor) throws IOException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForLabelOperatorException, WrongValueForSinkException, TimeoutException {//LearnADD learner=new LearnADD();
        curr=f;
        tar=target;
        DataSet ref;
        // Import traces
        if(f.getAbsolutePath().endsWith(".dot")){
            ref=readTracesFromDot(f);
        }else if(f.getAbsolutePath().endsWith(".trace")){
            ref=TraceIO.readBinTraces(f);
        }else{
            System.err.println("unsuitable Filetype");
            return null;
        }
        if(!learnerConstraints.isSuppressWarnings()&&ref.size()<learnerConstraints.getTRAIN_SIZE()){
            System.err.println("WARNING: Less traces than ordered");
            System.err.println("Model: "+f.getAbsolutePath());
            System.err.println("Only "+ref.size()+" of "+learnerConstraints.getTRAIN_SIZE());
            System.err.println("True ones: "+getNumSuccesses(ref));
            System.err.println("+++++++++++++++++++++++");
        }

        //Prepare BEs and traces
        Set<Vertex> be=getBeFromTraces(ref);
        DataSet train=new DataSet();
        DataSet test=new DataSet();
        splitDataSet(ref,train,test,this.getRand(),learnerConstraints.getTRAIN_RATE());
        if(learnerConstraints.isMetaOutput()) {
            Set<String> ids=be.stream().map(Vertex::getId).collect(Collectors.toSet());
            Sequence2Csv.write2File(
                    Path.of(target.getAbsolutePath(), "test" + f.getName() +learnerConstraints.getFitnessSuffix()+  ".csv").toFile()
                    , test, ids);
            Sequence2Csv.write2File(
                    Path.of(target.getAbsolutePath(), "train" + f.getName() +learnerConstraints.getFitnessSuffix()+ ".csv").toFile(),
                    train, ids);
        }
        if(learnerConstraints.isOutputTraces()){
            TraceIO.writeBinTraces(new File(target.getAbsolutePath(), "test_" + f.getName() +learnerConstraints.getFitnessSuffix()+  ".trace"),test);
            TraceIO.writeBinTraces(new File(target.getAbsolutePath(), "train_" + f.getName() +learnerConstraints.getFitnessSuffix()+  ".trace"),train);
            TraceIO.writeBinTraces(new File(target.getAbsolutePath(), "whole_" + f.getName() +learnerConstraints.getFitnessSuffix()+  ".trace"),ref);
        }
        System.out.println("Generated: "+ref.size()+" Train: "+train.size()+" Test: "+test.size()+" Successes: "+getNumSuccesses(ref));
        //The heart of the model generation
        ADD start=runModel(be,train,test,openEditor);
        Rational val=compareModel(s->s.evaluateOnADD(start),test,learnerConstraints.getW1());

        //Outputting for analysis
        totalWeight = totalWeight.plus(val);
        totalSize = totalSize.plus(Rational.ONE);
        lastFitness =totalWeight.divide(totalSize);
        //System.out.println("Current weighted accuracy over all models: "+ lastFitness.doubleValue());

        File res=new File(target.getAbsolutePath(),"fitness"+learnerConstraints.getFitnessSuffix()+".csv");
        boolean neu=!res.exists();
        PrintWriter pw = new PrintWriter(new FileOutputStream(
                res,
                true ));
        if(neu){
            pw.println("Name;Fitness;Sensitivity;Specificity;w1;width;epochs;mutation;actions");
        }
        Rational sens=getSensitivity(s->s.evaluateOnADD(start),test);
        Rational spec=getSpecificity(s->s.evaluateOnADD(start),test);
        pw.println( f.getName()+ ';' +val.doubleValue()+ ';' +sens.doubleValue()+ ';' +spec.doubleValue()+ ';' +
                learnerConstraints.getW1().doubleValue()+ ';' +learnerConstraints.getEPOCH_WIDTH()+ ';' +
                learnerConstraints.getEPOCHS()+ ';' +learnerConstraints.getMUTATION_RATE()+ ';' +
                learnerConstraints.getNUM_ACTIONS());
        pw.close();
        return start;
    }
    protected DataSet readTracesFromDot(File f) throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        return readTracesFromDot(f,0);
    }
    protected DataSet readTracesFromDot(File f,int skip) throws IOException, WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, WrongValueForSinkException {
        ADD model= ParserDOT.parseADD(new FileReader(f.getAbsolutePath()));
        model.validateConnections();

        if(model.getBasicEvents().size()<=1)return null;

        DataSet ref;
        if(learnerConstraints.getSuccessRate()>=0&&learnerConstraints.getSuccessRate()<=1){
            ref=generateSequences(model,skip,skip+learnerConstraints.getTRAIN_SIZE(),
                    learnerConstraints.getSuccessRate(),learnerConstraints.isOrdered(),rand);
        }else{
            ref=generateSequences(model,skip,skip+learnerConstraints.getTRAIN_SIZE(),
                    learnerConstraints.getSuccess(),learnerConstraints.isOrdered(),rand);
        }
        return ref;
    }
    public ADD runModel(Set<Vertex> be,DataSet train,DataSet test,boolean openEditor) throws FileNotFoundException, TimeoutException, WrongStructureADDException {
        deriveParams();
        if(isActive()&&learnerConstraints.getTimeout()>0){
            initTimeout();
        }
        if(!isActive()){
            throw new TimeoutException("Further submission impossible. Timeout Reached");
        }
        runGenerationZero(be,train);
        ADD best=getBest();

        Rational fitness=pool.get(best);
        ADD start;
        if(fitness.equals(Rational.ONE)){
            start=best;
            writeEpochFitness(start,0,test);
        }else{
            start=runModel(best,train,test,openEditor);
        }
        if(!test.isEmpty()){
            fitness=compareModel(s->s.evaluateOnADD(start),test,learnerConstraints.getW1());
            System.out.println("Test set Fitness after Execution: "+fitness.doubleValue());
        }
        return start;
    }

    /**
     * Runs a k-fold cross validation on a given dataset.
     * @param be A set of available basic events
     * @param data A dataset to cross validate on
     * @param k the number of folds
     * @return A Pair containing mean and variance of the crossvalidation process
     * @throws FileNotFoundException Used by the {@link LearnADD#runModel(ADD, DataSet, DataSet, boolean)} as it may write meta outputs
     * @throws WrongStructureADDException If the generated ADD are of invalid shape
     * @throws TimeoutException If the model can not be submitted to the learner anymore, as the timeout was reached
     */
    public Pair<Rational,Rational> validateModel(Set<Vertex> be, DataSet data, int k) throws FileNotFoundException, WrongStructureADDException, TimeoutException {
        return validateModel(be,data,k,false);
    }

    /**
     * Runs a k-fold cross validation on a given dataset.
     * @param be A set of available basic events
     * @param data A dataset to cross validate on
     * @param k the number of folds
     * @param silent set true if the result should be printed to System.out after execution
     * @return A Pair containing mean and variance of the crossvalidation process
     * @throws FileNotFoundException Used by the {@link LearnADD#runModel(ADD, DataSet, DataSet, boolean)} as it may write meta outputs
     * @throws WrongStructureADDException If the generated ADD are of invalid shape
     * @throws TimeoutException If the model can not be submitted to the learner anymore, as the timeout was reached
     */
    public Pair<Rational,Rational> validateModel(Set<Vertex> be, DataSet data, int k,boolean silent) throws FileNotFoundException, WrongStructureADDException, TimeoutException {
        deriveParams();
        if(isActive()&&learnerConstraints.getTimeout()>0){
            initTimeout();
        }
        if(!isActive()){
            throw new TimeoutException("Further submission impossible. Timeout Reached");
        }
        List<DataSet> partitions= getPartitions(data,k,rand);
        Rational EX=Rational.ZERO;
        Rational EX2=Rational.ZERO;
        for(int i=0;i<partitions.size();i++){
            List<DataSet> newPartitions=new ArrayList<>(partitions);
            DataSet eval=newPartitions.remove(i);
            DataSet train= joinDataSet(newPartitions);

            ADD result=runModel(be,train,new DataSet(),false);
            Rational evalScore= compareModel(s->s.evaluateOnADD(result),eval,learnerConstraints.getW1());
            EX=EX.plus(evalScore);
            EX2=EX2.plus(evalScore.times(evalScore));
        }
        EX=EX.divide(Rational.valueOf(k,1));
        EX2=EX2.divide(Rational.valueOf(k,1));
        Rational VAR=EX2.minus(EX.times(EX));
        if(!silent){
            System.out.printf("Cross validation %.3f ± %.3f (μ ± σ)%n", EX.doubleValue(),Math.sqrt(VAR.doubleValue()));
        }
        return new Pair<>(EX,VAR);
    }

    private ADD runModel(ADD start,DataSet train,DataSet test,boolean openEditor)throws FileNotFoundException,
            WrongStructureADDException{
        long start_time=System.currentTimeMillis();
        List<ADD> epochs=new ArrayList<>();
        ADD finalStart=start;
        Rational val2=compareModel(s->s.evaluateOnADD(finalStart),test,learnerConstraints.getW1());
        epochs.add(start);
        String label="Train set Fitness after Generation: "+pool.get(start).doubleValue();

        List<String > labels=new ArrayList<>();
        labels.add(label);
        //System.out.println(label);

        List<Collection<FeedbackTuple>> changes=new ArrayList<>();
        changes.add(new ArrayList<>());
        Rational last=Rational.ZERO;
        for(int i=1;i<=learnerConstraints.getEPOCHS();i++){
            System.out.print("Running Epoch: "+i+ '/' +learnerConstraints.getEPOCHS()+getProgressBar(i,learnerConstraints.getEPOCHS())+ '\r');
            runGeneration(start,train);
            start=getBest();
            epochs.add(start);
            changes.add(this.changes.get(start));
            ADD finalStartLoop=start;
            Rational val=compareModel(s->s.evaluateOnADD(finalStartLoop),test,learnerConstraints.getW1());
            String lb="Train set Fitness after Generation: "+pool.get(start).doubleValue();
            if(last.isLessThan(val)){
                decreaseEpsilon();
            }else{
                increaseEpsilon();
            }
            last=val;
            if(learnerConstraints.isEpochResultOut()){
                writeEpochFitness(start,i,test);
            }
            labels.add(lb);
            //System.out.println(lb);
            if(val.equals(Rational.ONE)){
                break;
            }
            if(!canContinue()){
                System.err.println("Timeout reached. Breaking up after current generation.");
                break;
            }
        }
        System.out.println();

        //System.out.println("Runtime: "+(System.currentTimeMillis()-start_time));

        if(openEditor) {
            openEditor(epochs,changes,labels,train,test);
        }
        return start;
    }
    private void writeEpochFitness(ADD start,int num,DataSet test) throws FileNotFoundException {
        File res=new File(tar.getAbsolutePath(),"fitness_full"+learnerConstraints.getFitnessSuffix()+".csv");
        boolean neu=!res.exists();
        PrintWriter pw = new PrintWriter(new FileOutputStream(
                res,
                true ));
        if(neu){
            pw.println("Name;Fitness;Sensitivity;Specificity;Epoch;w1;width;epochs;mutation;actions");
        }
        Rational sens=getSensitivity(s->s.evaluateOnADD(start),test);
        Rational spec=getSpecificity(s->s.evaluateOnADD(start),test);
        Rational fit=compareModel(s->s.evaluateOnADD(start),test,learnerConstraints.getW1());
        pw.println( curr.getName()+ ';' +fit.doubleValue()+ ';' +sens.doubleValue()+ ';' +spec.doubleValue()+ ';' +
                num+ ';' +learnerConstraints.getW1().doubleValue()+ ';' +learnerConstraints.getEPOCH_WIDTH()+ ';' +
                learnerConstraints.getEPOCHS()+ ';' +learnerConstraints.getMUTATION_RATE()+ ';' +
                learnerConstraints.getNUM_ACTIONS());
        pw.close();
    }
    private void openEditor(List<ADD> epochs,List<Collection<FeedbackTuple>> changes,List<String> labels,DataSet train,DataSet test){
        GraphFrame adf = new GraphFrame("ADD Editor",2);
        adf.setExtendedState(JFrame.MAXIMIZED_BOTH);
        adf.setMinimumSize(new Dimension(400, 400));
        adf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        adf.setCustomPanel("Learn",
                UIHelper.buildToolButton("baseline_play_circle_outline_black_18dp.png",
                        "Restart Learning from current model",
                        e -> {
                            Object model = adf.getCurrentModel();
                            if (model instanceof ADD) {
                                ADD add=(ADD)model;
                                adf.setVisible(false);
                                adf.dispose();
                                try {
                                    runModel(add, train,test,true);
                                } catch (Exception Exception) {
                                    ExceptionHandler.logException(Exception);
                                }
                            }
                        }),
                UIHelper.buildToolButton("baseline_refresh_black_18dp.png",
                        "Recalculate epoch probability",
                        e -> {
                            Object model = adf.getCurrentModel();
                            if (model instanceof ADD) {
                                ADD add=(ADD)model;
                                Rational val = compareModel(s->s.evaluateOnADD(add), test,learnerConstraints.getW1());
                                String lb = "Epoch result: " + val.doubleValue();
                                adf.setLabel(lb);
                            }
                        }));
        adf.setVisible(true);
        adf.loadADDs(epochs, changes, labels);
    }
    private static String getProgressBar(int current, int max){
        return getProgressBar(20,current,max);
    }
    private static String getProgressBar(int signs, int current, int max){
        int letters=current*signs/max;
        StringBuilder builder=new StringBuilder(signs+2);
        for(int i=0;i<signs;i++){
            if(i<letters){
                builder.append('=');
            }else{
                builder.append(' ');
            }
        }
        builder.append(']');
        builder.insert(0, '[');
        return builder.toString();
    }
}
