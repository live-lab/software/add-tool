package addtool.learning;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public final class Sequence2Csv {
    public static final char sep=';';

    private Sequence2Csv() {
    }

    public static void write2File(File target, DataSet ref, Set<String> ids) throws FileNotFoundException {
        if(target==null){
            throw new FileNotFoundException("Valid File must be submitted");
        }
        PrintWriter pw=new PrintWriter(target);
        pw.println("label"+sep+ String.join(sep + "", ids));
        for(Map.Entry<Sequence, DataSet.Values> e:ref.entrySet()){
            pw.print((e.getValue().getBooleanLabel()?"1":"0")+sep);
            pw.println(ids.stream().map(key->{
                if(e.getKey().contains(key)){
                    return "1";
                }else{
                    return "0";
                }
            }).collect(Collectors.joining(sep+"")));
            /*for(String key:ids){
                if(e.getKey().contains(key)){
                    pw.print("1"+sep);
                }else{
                    pw.print("0"+sep);
                }
            }
            pw.println();*/
        }
        pw.close();
    }
}
