package addtool.learning;

import addtool.semantics.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Class for labeled datasets of {@link Sequence} and {@link Values}.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 28.06.2021
 */
public class DataSet extends HashMap<Sequence, DataSet.Values> {
    private int size;
    private Map<Sequence,Values> data;

    @Override
    public int size() {
        return size;
    }

    @Override
    public Values put(Sequence key, Values value) {
        Values v= super.put(key, value);
        updateSize();
        return v;
    }
    public Values put(Sequence key, boolean Value) {
        return this.put(key,new Values(Value));
    }
    public Values put(Sequence key, boolean Value,int weight) {
        return this.put(key,new Values(Value,weight));
    }
    public boolean putAgain(Sequence key) {
        Values v= this.get(key);
        if(v!=null){
            replace(key,v.setWeight(v.getWeight()+1));
            updateSize();
            return true;
        }
        return false;
    }

    @Override
    public void putAll(Map<? extends Sequence, ? extends Values> m) {
        super.putAll(m);
        updateSize();
    }

    @Override
    public Values remove(Object key) {
        Values v=this.get(key);
        if(v.getWeight()>1){
            replace((Sequence) key,v.setWeight(v.getWeight()-1));
        }else{
            super.remove(key);
        }
        updateSize();
        return v;
    }

    @Override
    public void clear() {
        super.clear();
        updateSize();
    }

    @Override
    public Values putIfAbsent(Sequence key, Values value) {
        Values v= super.putIfAbsent(key, value);
        updateSize();
        return v;
    }

    @Override
    public boolean remove(Object key, Object value) {
        boolean res= super.remove(key, value);
        updateSize();
        return res;
    }

    @Override
    public boolean replace(Sequence key, Values oldValue, Values newValue) {
        boolean res= super.replace(key, oldValue, newValue);
        updateSize();
        return res;
    }

    @Override
    public Values replace(Sequence key, Values value) {
        Values v= super.replace(key, value);
        updateSize();
        return v;
    }

    @Override
    public Values computeIfAbsent(Sequence key, Function<? super Sequence, ? extends Values> mappingFunction) {
        Values v= super.computeIfAbsent(key, mappingFunction);
        updateSize();
        return v;
    }

    @Override
    public Values computeIfPresent(Sequence key, BiFunction<? super Sequence, ? super Values, ? extends Values> remappingFunction) {
        Values v= super.computeIfPresent(key, remappingFunction);
        updateSize();
        return v;
    }

    @Override
    public Values compute(Sequence key, BiFunction<? super Sequence, ? super Values, ? extends Values> remappingFunction) {
        Values v= super.compute(key, remappingFunction);
        updateSize();
        return v;
    }

    @Override
    public Values merge(Sequence key, Values value, BiFunction<? super Values, ? super Values, ? extends Values> remappingFunction) {
        Values v= super.merge(key, value, remappingFunction);
        updateSize();
        return v;
    }

    @Override
    public void replaceAll(BiFunction<? super Sequence, ? super Values, ? extends Values> function) {
        super.replaceAll(function);
        updateSize();
    }
    private void updateSize(){
        size=this.values().stream().mapToInt(Values::getWeight).sum();
    }

    static class Values{
        private final Value label;
        private final int weight;
        public Values(){
            this(Value.FALSE,1);
        }
        public Values(Value label){
            this(label,1);
        }
        public Values(boolean label){
            this(label?Value.TRUE:Value.FALSE,1);
        }
        public Values(boolean label,int weight){
            this(label?Value.TRUE:Value.FALSE, weight);
        }
        public Values(Value label,int weight){
            this.label=label;
            this.weight = weight;
        }
        public boolean getBooleanLabel(){
            return label==Value.TRUE;
        }
        public Value getLabel(){
            return label;
        }
        public int getWeight(){
            return weight;
        }
        private Values setWeight(int weight){
            return new Values(label, weight);
        }
        @Override
        public int hashCode(){
            return label.hashCode()+7* weight;
        }
        @Override
        public boolean equals(Object o){
            if(o instanceof Values){
                Values other=(Values) o;
                return other.label==this.label&&other.weight ==this.weight;
            }
            return false;
        }
    }
}
