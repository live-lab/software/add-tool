package addtool.learning;

import addtool.add.model.*;
import addtool.dot2add.RationalFromString;
import addtool.dot2add.WrongStructureADDException;
import org.apache.commons.math3.util.Precision;
import org.jscience.mathematics.number.Rational;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Helper methods for learning.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 28.06.2021
 */
public interface LearnHelper {
    static void splitDataSet(DataSet ref, DataSet train, DataSet test, Random rand, double rate){
        if(ref==null||rate>1){
            //System.err.println(ref);
            throw new UnsupportedOperationException("Can not split DataSet, as it is either empty or the rate is grater than one.");
        }
        splitDataSet(ref,train,test,rand,(int)(ref.size()*rate),true);
    }

    static long getNumSuccesses(DataSet ref){
        return ref.values().stream().filter(DataSet.Values::getBooleanLabel).mapToInt(DataSet.Values::getWeight).sum();
    }
    static long getNumFailures(DataSet ref){
        return ref.values().stream().filter(b->!b.getBooleanLabel()).mapToInt(DataSet.Values::getWeight).sum();
    }
    /**
     * Calculates model sensitivity.
     * @param otherModel The Model to check
     * @param ref The Set of traces to calculate sensitivity of
     * @return Sensitivity on the given set
     */
    static Rational getSensitivity(Predicate<Sequence> otherModel, DataSet ref){
        int correctSuccess=ref.entrySet().stream().filter(e->e.getValue().getBooleanLabel()).
                mapToInt(e->
                        otherModel.test(e.getKey())==e.getValue().getBooleanLabel()?
                            e.getValue().getWeight():
                            0).sum();
        int successes=(int)getNumSuccesses(ref);
        return successes==0?Rational.ZERO:Rational.valueOf(correctSuccess,successes);
    }
    static Rational getFalsePositives(Predicate<Sequence> otherModel, DataSet ref){
        return Rational.ONE.minus(getSpecificity(otherModel,ref));
    }

    /**
     * Calculates model specificity.
     * @param otherModel The model to check on
     * @param ref A DataSet to evaluate
     * @return The specificity as Rational
     */
    static Rational getSpecificity(Predicate<Sequence> otherModel, DataSet ref){
        int corrFail=ref.entrySet().stream().filter(e->!e.getValue().getBooleanLabel()).
                mapToInt(e->
                        otherModel.test(e.getKey())==e.getValue().getBooleanLabel()?
                                e.getValue().getWeight():
                                0).sum();
        int failures=ref.size()-(int)getNumSuccesses(ref);
        return failures==0?Rational.ZERO:Rational.valueOf(corrFail,failures);
    }
    /**
     * Compares the Model with the Sequence data of another model.
     * @param otherModel The model
     * @param ref A Map with sequence as key and boolean (if model evaluated to true) as Value
     * @return fitness of the model
     */
    static Rational compareModel(Predicate<Sequence> otherModel, DataSet ref,Rational w1){
        if(ref.isEmpty())return Rational.ZERO;
        if(Precision.equals(w1.doubleValue(),-1,0.005)){
            int corr=ref.entrySet().stream().
                    mapToInt(e->
                            otherModel.test(e.getKey())==e.getValue().getBooleanLabel()?
                                    e.getValue().getWeight():
                                    0).sum();
            return Rational.valueOf(corr,ref.size());
        }

        //if w1==-1 it jumps to faster solution earlier
        //Rational w1=Rational.valueOf(successes,successes+failures);
        Rational w2=Rational.ONE.minus(w1);
        Rational sensitivity=getSensitivity(otherModel,ref);
        Rational specificity=getSpecificity(otherModel, ref);
        //System.out.println("~~~"+sensitivity.doubleValue()+"|"+specificity.doubleValue());
        return w1.times(sensitivity).plus(w2.times(specificity));
    }
    static void splitDataSet(DataSet ref,DataSet train,DataSet test,Random rand,int trainSize,boolean preserveRatio){
        if(ref==null||train==null||test==null){
            throw new UnsupportedOperationException("DataSets can not be null.");
        }
        if(trainSize==ref.size()||trainSize==0){
            //Do not split if they do not want you to
            train.putAll(ref);
            test.putAll(ref);
            return;
        }

        int success=(int)getNumSuccesses(ref);
        AtomicInteger successTrain=new AtomicInteger((success*trainSize)/ref.size());
        AtomicInteger failTrain=new AtomicInteger(trainSize-successTrain.get());

        int testSize=ref.size()-trainSize;
        if(testSize<0||trainSize<0){
            System.err.println("Sizes <0");
            throw new UnsupportedOperationException("Can not split DataSet if train or test size is smaller than zero.");
        }
        ref.forEach((k,v)->{
            if(train.size()==trainSize){
                test.put(k,v);
            }else if(test.size()==testSize){
                train.put(k,v);
            }else if(rand.nextInt(ref.size())<trainSize&&
                    (!preserveRatio||
                            (v.getBooleanLabel()?successTrain.get()>0:
                                    failTrain.get()>0))){
                train.put(k,v);
                if(v.getBooleanLabel()){
                    successTrain.decrementAndGet();
                }else{
                    failTrain.decrementAndGet();
                }
            }else{
                test.put(k,v);
            }
        });
    }
    /**
     * Builds a Random Tree of a Set of BasicEvents.
     * @param BEs The Set of BEs
     * @return The ADD
     * @throws WrongStructureADDException if the Tree at connection has a wrong structure
     * @see WrongStructureADDException
     */
    static ADD generateRandomTree(Set<Vertex> BEs, Random rand, List<Vertex> ops) throws WrongStructureADDException {
        List<ADD> trees=BEs.stream()
                .map(Vertex::copy)
                .map(ADDBuilder::of).collect(Collectors.toList());
        List<String> usedIds=BEs.stream().map(Vertex::getId).collect(Collectors.toList());
        while(trees.size()>1){//Combine Parts until reaching End
            //Create new Operator:
            String id;
            do{
                id=String.valueOf(rand.nextInt(1000));
            }while(usedIds.contains(id));
            usedIds.add(id);
            Vertex op=ops.get(rand.nextInt(ops.size())).copy();
            op.setID(id);op.setName(id);

            ADD tree1=trees.get(rand.nextInt(trees.size()));
            trees.remove(tree1);
            if(op instanceof UnaryOp){
                if(tree1.getNoSuccessor().stream().noneMatch(v->v instanceof UnaryOp)&&
                        tree1.getId2Vertex().size()<=tree1.getBasicEvents().size()*2)
                    trees.add(new ADDBuilder(tree1).simpleJoin((UnaryOp)op).get());
                else
                    trees.add(tree1);
            }else{
                //Can not use simple join as we need constant BEIds
                ADD tree2=trees.get(rand.nextInt(trees.size()));
                trees.remove(tree2);

                Map<String,Vertex> id2Vertex=new HashMap<>(tree1.getId2Vertex());
                id2Vertex.putAll(tree2.getId2Vertex());
                //System.out.println(id2Vertex.size()+"|"+tree1.getId2Vertex().size()+"|"+tree2.getId2Vertex().size());

                Set<Vertex> noSuccessor=Set.of(op);

                NaryOp naryOp=(NaryOp)op;
                id2Vertex.put(naryOp.getId(),naryOp);
                naryOp.addPredecessors(tree1.getNoSuccessor());
                naryOp.addPredecessors(tree2.getNoSuccessor());

                ADD neu=new ADD(id2Vertex,noSuccessor,true);
                neu.validateConnections();

                trees.add(neu);
            }
        }
        ADD ret=trees.get(0);
        if(ret!=null){
            ret.validateConnections();
        }
        return ret;
    }

    /**
     * Generates a bunch of valid sequences and their Values.
     * @param add The Model to generate sequences off
     * @param skip How many sequences in the beginning should be skipped
     * @param limit A max amount of sequences
     * @return A map with the sequence as key and the value
     */
    static DataSet generateSequences(ADD add,int skip,int limit,Random rand){
        return generateSequences(add,skip,limit,-1,false,rand);
    }
    static DataSet generateSequences(ADD add,int skip,int limit,double successRate,boolean ordered,Random rand){
        //CAVE STACK OVERFLOW IF not guided to below method!
        return generateSequences(add,skip,limit,(int)(limit*successRate),ordered,rand);
    }
    static DataSet generateSequences(ADD add,int skip,int limit,int success,boolean ordered,Random rand){
        String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        int maxLength=add.getBasicEvents().size()*2;
        /* They are only used if success is not -1 */
        AtomicInteger remainingSuccess=new AtomicInteger(success);
        AtomicInteger remainingFailure=new AtomicInteger(success==-1?-1:limit-success);

        Supplier<String> generator=()->{
            StringBuilder builder = new StringBuilder();
            for(int i=0;i<maxLength;i++){
                int character = rand.nextInt(ALPHA_NUMERIC_STRING.length());
                builder.append(ALPHA_NUMERIC_STRING.charAt(character));
            }
            return builder.toString();
        };
        DataSet dataSet=new DataSet();
        Map<Sequence, DataSet.Values> map=
                Stream.generate(generator).distinct().skip(skip).limit(limit* 10L)
                .map(seq->{
                    List<String> ids= new ArrayList<>(add.getBasicEventIds());
                    List<String> end_seq=new ArrayList<>();
                    for(int i=0;i<seq.length();i++){
                        int pos=ALPHA_NUMERIC_STRING.indexOf(seq.charAt(i))%(ids.size()+1);
                        if(pos==ids.size()){
                            continue;
                        }
                        end_seq.add(ids.get(pos));
                        boolean endTurn=false;
                        if(!add.isValidSequence(end_seq)){
                            end_seq.remove(end_seq.size()-1);
                            endTurn=true;
                        }
                        if(add.postCheckSequence()||endTurn){
                            break;
                        }
                    }
                    Sequence sequence=new Sequence(end_seq);
                    sequence.setOrdered(ordered);
                    return sequence;
                })
                .distinct()
                .map(s->Map.entry(s,s.evaluateOnADD(add)))
                .filter(e->{
                    /* Here we take care of the bounds */
                    if(remainingSuccess.get()==-1)return true;
                    if(e.getValue()){
                        if(remainingSuccess.get()>0){
                            remainingSuccess.decrementAndGet();//watch for this side effect
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        if(remainingFailure.get()>0){
                            remainingFailure.decrementAndGet();//watch for this side effect
                            return true;
                        }else{
                            return false;
                        }
                    }
                })
                .limit(limit)
                .collect(Collectors.toMap(Map.Entry::getKey, m->new DataSet.Values(m.getValue())));
        dataSet.putAll(map);
        return dataSet;
    }
    static Set<Vertex> getBeFromTraces(DataSet ref) {
        return getBeFromTraces(ref,new HashMap<>());
    }
    static Set<Vertex> getBeFromTraces(DataSet ref, File labels) throws IOException {
        return getBeFromTraces(ref,getLabelsFromFile(labels));
    }
    static Set<Vertex> getBeFromTraces(DataSet ref,Map<String,String> idToName) {
        Set<String> ids=new HashSet<>();
        for(Sequence s:ref.keySet()){
            ids.addAll(s.getEvents());
        }
        return ids.stream().
                map(s->new BasicEventPlayer(idToName.getOrDefault(s,s),Rational.ONE,Rational.ZERO,Player.Attacker,s)).
                collect(Collectors.toSet());
    }
    static Map<String,String> getLabelsFromFile(File labels) throws IOException {
        return Files.readAllLines(labels.toPath()).stream()
                .map(line->line.split("="))
                .filter(args->args.length==2)
                .collect(Collectors.toMap(arg->arg[0],arg->arg[1]));
    }
    static List<DataSet> getBatches(DataSet ref,int amount,Random rand){
        //1-1/e
        return getBatches(ref, amount, rand, RationalFromString.getRationalFromString("0.6321205588"));
    }

    static List<DataSet> getBatches(DataSet ref,int amount,Random rand, Rational unique){
        List<DataSet> dataSets=new ArrayList<>();
        List<Sequence> traces=new ArrayList<>(ref.keySet());
        for(int i=0;i<amount;i++){
            DataSet data=new DataSet();
            int uniqueTraces=unique.times(ref.size()).intValue();
            //Add all unique traces
            do{
                Sequence trace=traces.get(rand.nextInt(ref.size()));
                if(!data.containsKey(trace)){
                    data.put(trace,ref.get(trace));
                }
            }while(data.size()<uniqueTraces);
            List<Sequence> setTraces=new ArrayList<>(data.keySet());
            int duplicateAmount=ref.size()-data.size();
            for(int j=0;j<duplicateAmount;j++){
                data.putAgain(setTraces.get(rand.nextInt(setTraces.size())));
            }
            assert data.size()==ref.size();
            dataSets.add(data);
        }
        return dataSets;
    }

    static List<DataSet> getPartitions(DataSet ref,int amount,Random rand){
        DataSet refCopy=new DataSet();
        refCopy.putAll(ref);
        List<DataSet> dataSets=new ArrayList<>();
        int targetSize= refCopy.size()/amount;
        int rest=refCopy.size()%amount;
        for(int i=0;i<amount;i++){
            int actualSize=targetSize;
            if(rest>0){
                actualSize++;
                rest--;
            }
            DataSet data=new DataSet();
            while(actualSize>data.size()){
                //this needs to be defined here in case of doubled sequences in a Dataset
                List<Sequence> content=new ArrayList<>(refCopy.keySet());
                Sequence s=content.get(rand.nextInt(content.size()));
                DataSet.Values v=refCopy.remove(s);
                data.put(s,new DataSet.Values(v.getLabel(),1));
            }
            assert data.size()==actualSize;
            dataSets.add(data);
        }
        return dataSets;
    }
    static DataSet joinDataSet(List<DataSet> dataSets){
        DataSet ref=new DataSet();
        for(DataSet data:dataSets){
            for(Map.Entry<Sequence,DataSet.Values> entry:data.entrySet()){
                if(ref.containsKey(entry.getKey())){
                    DataSet.Values v1=ref.get(entry.getKey());
                    DataSet.Values v2=entry.getValue();
                    assert v1.getLabel()==v2.getLabel();
                    ref.replace(entry.getKey(),new DataSet.Values(v1.getLabel(),v1.getWeight()+v2.getWeight()));
                }else{
                    ref.put(entry.getKey(),entry.getValue());
                }
            }
        }
        return ref;
    }
}
