package addtool.learning;

import addtool.add.model.ADD;
import addtool.add.model.Vertex;
import addtool.dot2add.*;
import addtool.helpers.ExceptionHandler;
import addtool.helpers.Pair;
import org.jscience.mathematics.number.Rational;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Changes Learning to learning a set of {@link ADD} in different processes and pooling their classifications.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 28.06.2021
 */
public class LearnFuzzyADD {
    static final int batches=10;
    int batchSize=1000;
    final LearnerConstraints learnerConstraints;
    public LearnFuzzyADD(LearnerConstraints lc){
        learnerConstraints=lc.clone();
    }
    public Future<FuzzyADD> runModel(Set<Vertex> basicEvents, DataSet train, DataSet test){
        Random rand=new Random(learnerConstraints.getSeed());
        ExecutorService exe= Executors.newSingleThreadExecutor();
        Future <FuzzyADD> result=exe.submit(()-> {
            FuzzyADD fuzzy = new FuzzyADD();
            ExecutorService worker = Executors.newCachedThreadPool();
            List<DataSet> batchList = LearnHelper.getBatches(train, batches, rand);//1-1/e
            for (double j = 0.1; j < 1.0; j += 0.2) {
                String w1 = String.valueOf(j);
                for (DataSet batch : batchList) {
                    worker.submit(() -> {
                        try {
                            LearnerConstraints lcNeu = learnerConstraints.clone();
                            lcNeu.setW1(w1);
                            LearnADD learnerNeu = new LearnADD(lcNeu);
                            ADD mod = learnerNeu.runModel(basicEvents, batch, test, false);
                            fuzzy.addADD(mod);
                        } catch (FileNotFoundException |TimeoutException| WrongStructureADDException e) {
                            ExceptionHandler.logException(e);
                        }
                    });
                }

            }
            worker.shutdown();
            //TODO this is not good
            worker.awaitTermination(10, TimeUnit.MINUTES);

            return fuzzy;
        });
        exe.shutdown();
        return result;
    }

    public Future<FuzzyADD> runModelWOnly(Set<Vertex> basicEvents, DataSet train, DataSet test){
        Random rand=new Random(learnerConstraints.getSeed());
        ExecutorService exe= Executors.newSingleThreadExecutor();
        Future <FuzzyADD> result=exe.submit(()-> {
            FuzzyADD fuzzy = new FuzzyADD();
            ExecutorService worker = Executors.newCachedThreadPool();
            for (double j = 0.1; j < 1.0; j += 0.2) {
                String w1 = String.valueOf(j);
                    worker.submit(() -> {
                        try {
                            LearnerConstraints lcNeu = learnerConstraints.clone();
                            lcNeu.setW1(w1);
                            LearnADD learnerNeu = new LearnADD(lcNeu);
                            ADD mod = learnerNeu.runModel(basicEvents, train, test, false);
                            fuzzy.addADD(mod);
                        } catch (FileNotFoundException |TimeoutException| WrongStructureADDException e) {
                            ExceptionHandler.logException(e);
                        }
                    });
            }
            worker.shutdown();
            //TODO this is not good
            worker.awaitTermination(10, TimeUnit.MINUTES);

            return fuzzy;
        });
        exe.shutdown();
        return result;
    }
    public static void main(String... args) throws WrongValueForLabelOperatorException, WrongStructureADDException, NoValueSpecifiedException, IOException, WrongValueForSinkException, TimeoutException {
        //File f=new File("data/Learning/compare/_file-43_35.dot");
        File outfile=new File("data/Learning/meta");

        LearnerConstraints lc=new LearnerConstraints();
        lc.setEPOCHS(50);
        lc.setSeed(123);
        lc.setTRAIN_SIZE(1000);
        //lc.setTimeout(5);
        File[] fs=new File("data/Learning/compare/").listFiles();
        Rational totalVar=Rational.ZERO;
        for(File f:fs){
            //Create Data
            Random random=new Random(lc.getSeed());
            ADD model= ParserDOT.parseADD(new FileReader(f.getAbsolutePath()));

            DataSet ref=LearnHelper.generateSequences(model,0,lc.getTRAIN_SIZE(),random);
            DataSet train=new DataSet();
            DataSet test=new DataSet();
            LearnHelper.splitDataSet(ref,train,test,random,lc.getTRAIN_RATE());

            LearnADD learner=new LearnADD(lc);

            Pair<Rational,Rational> results=learner.validateModel(model.getBasicEvents(),train,10);
            totalVar=totalVar.plus(results.getSecond());
            learner.resetTimeout();
        }
        System.out.println("Mean Variance: "+totalVar.divide(Rational.valueOf(fs.length,1)).doubleValue());
 /*       long timeConvPre=System.currentTimeMillis();
        //First compare main
        ADD add=learner.runModel(model.getBasicEvents(),train,new DataSet(),false);
        long timeConvPost=System.currentTimeMillis();


        LearnFuzzyADD learnfuzzy=new LearnFuzzyADD(lc);
        long timeNewPre=System.currentTimeMillis();
        Future<FuzzyADD> Ffuzzy=learnfuzzy.runModel(model.getBasicEvents(),train,test);
        FuzzyADD fuzzy=Ffuzzy.get();

        long timeNewPost=System.currentTimeMillis();
        System.out.println("+++New: "+(timeNewPost-timeNewPre)+"|Conventional: "+(timeConvPost-timeConvPre));

        Rational fit=LearnHelper.compareModel(fuzzy::evaluateSequence,test,lc.getW1());
        Rational fitold= LearnHelper.compareModel(add::evaluateSequence,test,lc.getW1());
        System.out.println("+++New: "+fit.doubleValue()+"|Conventional: "+fitold.doubleValue());*/
    }

}
