package addtool.learning;

import addtool.add.model.ADD;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Sequence {
    private final List<String> events;
    private boolean ordered;

    public Sequence(List<String> events){
        this.events=new ArrayList<>(events);
        ordered=false;
    }
    public boolean isOrdered() {
        return ordered;
    }
    public int size(){
        return events.size();
    }
    public boolean contains(String s){
        return events.contains(s);
    }
    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }
    public List<String> getEvents(){
        return new ArrayList<>(events);
    }

    /**
     * Returns if the ADD would return True on one of its roots for the given trace/sequence.
     * All BE are assumed to succeed with probability 1
     * Replaced the old evaluate in ADD using ADDValuationVisitor
     * @param add A model to check on.
     * @return true if one of the roots evaluated TRUE
     */
    public boolean evaluateOnADD(ADD add){
        return add.evaluateSequence(this);
        /* Still not efficient enough
        Valuation val = new Valuation(add);
        Map<String, Vertex> id2vertex=add.getId2Vertex();
        for(String id : events){
            val = val.attempt((BasicEvent) id2vertex.get(id));
        }
        return add.getNoSuccessor().stream().map(val::getValue).anyMatch(v->v== Value.TRUE);
        */
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sequence sequence = (Sequence) o;
        if(ordered != sequence.ordered)return false;
        if(events.size() != sequence.events.size())return false;
        //if(label != sequence.label)return false;
        if(ordered){
            /* Check if they are the same if reordered but do not change order! */
            List<String> l1=new ArrayList<>(events);
            List<String> l2=new ArrayList<>(sequence.events);
            Collections.sort(l1);
            Collections.sort(l2);
            return Objects.equals(l1,l2);
        }else{
            return Objects.equals(events, sequence.events);
        }
    }

    @Override
    public int hashCode() {
        if(ordered){
            List<String> l1=new ArrayList<>(events);
            Collections.sort(l1);
            return l1.hashCode();
        }else{
            return events.hashCode();
        }
    }
    @Override
    public String toString(){
        return events.toString();
    }
}
