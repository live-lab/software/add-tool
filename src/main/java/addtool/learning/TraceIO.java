package addtool.learning;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public final class TraceIO {
    public static final String delimiter=";";

    private TraceIO() {
    }

    public static DataSet readBinTraces(File f) throws IOException {
        return readBinTraces(f,true);
    }
    public static DataSet readBinTraces(File f,boolean allowDuplicates) throws IOException {
        DataSet output=new DataSet();
        BufferedReader br=new BufferedReader(new FileReader(f));
        String s;
        while((s=br.readLine())!=null){
            List<String> elements= new ArrayList<>(Arrays.asList(s.split(delimiter)));
            Boolean label=elements.remove(0).equals("1")?Boolean.TRUE:Boolean.FALSE;
            Sequence seq=new Sequence(elements);
            if(output.containsKey(seq)){
                if(allowDuplicates){
                    output.putAgain(seq);
                }
            }else{
                output.put(seq,new DataSet.Values(label));
            }
        }
        return output;
    }
    public static void writeBinTraces(File f,DataSet ref) throws FileNotFoundException {
        PrintWriter pw=new PrintWriter(f);
        for(Map.Entry<Sequence, DataSet.Values> e:ref.entrySet()){
            pw.print((e.getValue().getBooleanLabel()?"1":"0"));
            pw.println(e.getKey().getEvents().stream().collect(Collectors.joining(delimiter,delimiter,"")));
            pw.flush();
        }
        pw.close();
    }
}
