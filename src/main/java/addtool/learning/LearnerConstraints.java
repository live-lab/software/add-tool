package addtool.learning;

import addtool.add.model.*;
import addtool.dot2add.RationalFromString;
import addtool.helpers.Constants;
import org.jscience.mathematics.number.Rational;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Record class to provide arguments for {@link LearnADD}.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 28.06.2021
 */
public class LearnerConstraints implements Cloneable{
    public static final String SEED_KEY="seed";
    public static final String EPOCHS_KEY="epochs";
    public static final String POOL_SIZE_KEY="pool";
    public static final String WIDTH_KEY="width";
    public static final String ACTIONS_KEY="actions";
    public static final String MUTATION_KEY="mutation";
    public static final String EPSILON_KEY="epsilon";
    public static final String TRACES_KEY="traces";
    public static final String TRAIN_KEY="train";
    public static final String META_KEY="meta";
    public static final String ER_KEY="er";
    public static final String ORDERED_KEY="ordered";
    public static final String SUPPRESS_WARNINGS_KEY="suppresswarnings";
    public static final String OUTPUT_TRACES_KEY="outputtraces";
    public static final String SUCCESS_RATE_KEY="successrate";
    public static final String WEIGHT_KEY="weight";
    public static final String SUCCESS_KEY="success";
    public static final String OPERATORS_KEY="ops";
    public static final String TIMEOUT_KEY="timeout";
    public static final String SUFFIX_KEY="suffix";
    public static final String SHOW_EDITOR_KEY="showeditor";
    private int EPOCHS=50;
    private int EPOCH_WIDTH=30;
    private int EPSILON=5;
    private int MAX_EPSILON=15;
    private int NUM_ACTIONS=7;
    private int TRAIN_SIZE=1000;
    private int POOL_SIZE=20;
    private int timeout=-1;
    private double MUTATION_RATE=0.5;
    private double TRAIN_RATE=0.8;
    private Rational w1;
    private int success=-1;
    private double successRate=-1;
    private boolean ordered=false;
    private boolean metaOutput=false;
    private boolean suppressWarnings=false;
    private boolean epochResultOut=false;
    private boolean outputTraces =false;
    private boolean showEditor =true;
    private List<Vertex> ops= Stream.of("And","Or")//,"SAnd","SOr","Not","Nat"
            .map(ADDBuilder::buildOperatorForKey)
            .collect(Collectors.toList());

    private String fitnessSuffix;
    private int seed=new Random().nextInt();
    public LearnerConstraints(){
        w1=Rational.valueOf(-1,1);
        fitnessSuffix="";
    }

    public boolean isEpochResultOut() {
        return epochResultOut;
    }

    public void setEpochResultOut(boolean epochResultOut) {
        this.epochResultOut = epochResultOut;
    }

    public void setFitnessSuffix(String fitnessSuffix) {
        this.fitnessSuffix = fitnessSuffix==null?"":fitnessSuffix;
    }

    public String getFitnessSuffix() {
        return fitnessSuffix;
    }
    public boolean isSuppressWarnings() {
        return suppressWarnings;
    }

    public void setSuppressWarnings(boolean suppressWarnings) {
        this.suppressWarnings = suppressWarnings;
    }


    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public boolean isShowEditor() {
        return showEditor;
    }

    public void setShowEditor(boolean showEditor) {
        this.showEditor = showEditor;
    }

    public boolean isOutputTraces() {
        return outputTraces;
    }

    public void setOutputTraces(boolean outputTraces) {
        this.outputTraces = outputTraces;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }
    public void setW1(String w1){
        this.w1= RationalFromString.getRationalFromString(w1);
    }
    public Rational getW1(){
        return w1;
    }

    public int getEPOCHS() {
        return EPOCHS;
    }

    public void setEPOCHS(int EPOCHS) {
        this.EPOCHS = EPOCHS;
    }

    public int getEPOCH_WIDTH() {
        return EPOCH_WIDTH;
    }

    public void setEPOCH_WIDTH(int EPOCH_WIDTH) {
        this.EPOCH_WIDTH = EPOCH_WIDTH;
    }

    public int getEPSILON() {
        return EPSILON;
    }
    public int getMAX_EPSILON() {
        return MAX_EPSILON;
    }

    public void setEPSILON(int EPSILON) {
        this.EPSILON = EPSILON;
        this.MAX_EPSILON=this.EPSILON*3;
    }

    public int getNUM_ACTIONS() {
        return NUM_ACTIONS;
    }

    public void setNUM_ACTIONS(int NUM_ACTIONS) {
        this.NUM_ACTIONS = NUM_ACTIONS;
    }

    public int getTRAIN_SIZE() {
        return TRAIN_SIZE;
    }

    public void setTRAIN_SIZE(int TRAIN_SIZE) {
        this.TRAIN_SIZE = TRAIN_SIZE;
    }

    public int getPOOL_SIZE() {
        return POOL_SIZE;
    }

    public void setPOOL_SIZE(int POOL_SIZE) {
        this.POOL_SIZE = POOL_SIZE;
    }

    public double getMUTATION_RATE() {
        return MUTATION_RATE;
    }

    public void setMUTATION_RATE(double MUTATION_RATE) {
        this.MUTATION_RATE = MUTATION_RATE;
    }
    public double getTRAIN_RATE() {
        return TRAIN_RATE;
    }

    public void setTRAIN_RATE(double TRAIN_RATE) {
        this.TRAIN_RATE = TRAIN_RATE;
    }

    public List<Vertex> getOps() {
        return new ArrayList<>(ops);
    }



    /**
     * Changes the allowed operators in the constraints.
     * @param code 1=AND, OR; 2=AND, OR, NOT; 3=AND, OR, SAND, SOR; 4=AND, OR, SAND, SOR, NAT; 5=AND, OR, NOT, SAND, SOR, NAT;
     */
    public void setOps(int code){
        List<String> opcodes=new ArrayList<>();
        opcodes.add("And");
        opcodes.add("Or");
        if(code==2||code==5){
            opcodes.add("Not");
        }
        if(code>=3){
            opcodes.add("SAnd");
            opcodes.add("SOr");
        }
        if(code>=4){
            opcodes.add("Nat");
        }

        ops=opcodes.stream()
                .map(ADDBuilder::buildOperatorForKey)
                .collect(Collectors.toList());
    }
    public int getOpsCode(){
        if(ops.stream().anyMatch(v->v instanceof Nat)){
            if(ops.stream().anyMatch(v->v instanceof Not)){
                return 5;
            }
            return 4;
        }
        if(ops.stream().anyMatch(v->v instanceof SAnd)){
            return 3;
        }
        if(ops.stream().anyMatch(v->v instanceof Not)){
            return 2;
        }
        return 1;
    }

    public void setOps(List<Vertex> ops) {
        this.ops = new ArrayList<>(ops);
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public double getSuccessRate() {
        return successRate;
    }

    public void setSuccessRate(double successRate) {
        this.successRate = successRate;
    }

    public boolean isOrdered() {
        return ordered;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }

    public boolean isMetaOutput() {
        return metaOutput;
    }

    public void setMetaOutput(boolean metaOutput) {
        this.metaOutput = metaOutput;
    }

    public void digestArgs(int skip,String... args){
        Map<String,String> options= Constants.convertArgs(args,skip);
        digestArgs(options);
    }
    public void digestArgs(String... args){
        digestArgs(3,args);
    }
    public void digestArgs(Map<String,String> options){
        if(options.containsKey(SEED_KEY)){
            setSeed(Integer.parseInt(options.get(SEED_KEY)));
            options.remove(SEED_KEY);
        }
        if(options.containsKey(EPOCHS_KEY)){
            setEPOCHS(Integer.parseInt(options.get(EPOCHS_KEY)));
            options.remove(EPOCHS_KEY);
        }
        if(options.containsKey(POOL_SIZE_KEY)){
            setPOOL_SIZE(Integer.parseInt(options.get(POOL_SIZE_KEY)));
            options.remove(POOL_SIZE_KEY);
        }
        if(options.containsKey(WIDTH_KEY)){
            setEPOCH_WIDTH(Integer.parseInt(options.get(WIDTH_KEY)));
            options.remove(WIDTH_KEY);
        }
        if(options.containsKey(ACTIONS_KEY)){
            setNUM_ACTIONS(Integer.parseInt(options.get(ACTIONS_KEY)));
            options.remove(ACTIONS_KEY);
        }
        if(options.containsKey(MUTATION_KEY)){
            setMUTATION_RATE(Double.parseDouble(options.get(MUTATION_KEY)));
            options.remove(MUTATION_KEY);
        }
        if(options.containsKey(EPSILON_KEY)){
            setEPSILON(Integer.parseInt(options.get(EPSILON_KEY)));
            options.remove(EPSILON_KEY);
        }
        if(options.containsKey(TRACES_KEY)){
            setTRAIN_SIZE(Integer.parseInt(options.get(TRACES_KEY)));
            options.remove(TRACES_KEY);
        }
        if(options.containsKey(TRAIN_KEY)){
            setTRAIN_RATE(Double.parseDouble(options.get(TRAIN_KEY)));
            options.remove(TRAIN_KEY);
        }
        if(options.containsKey(META_KEY)){
            setMetaOutput(options.get(META_KEY).equals("1"));
            options.remove(META_KEY);
        }
        if(options.containsKey(ER_KEY)){
            setEpochResultOut(options.get(ER_KEY).equals("1"));
            options.remove(ER_KEY);
        }
        if(options.containsKey(ORDERED_KEY)){
            setOrdered(options.get(ORDERED_KEY).equals("1"));
            options.remove(ORDERED_KEY);
        }
        if(options.containsKey(SUPPRESS_WARNINGS_KEY)){
            setSuppressWarnings(options.get(SUPPRESS_WARNINGS_KEY).equals("1"));
            options.remove(SUPPRESS_WARNINGS_KEY);
        }
        if(options.containsKey(OUTPUT_TRACES_KEY)){
            setOutputTraces(options.get(OUTPUT_TRACES_KEY).equals("1"));
            options.remove(OUTPUT_TRACES_KEY);
        }
        if(options.containsKey(SHOW_EDITOR_KEY)){
            setShowEditor(options.get(SHOW_EDITOR_KEY).equals("1"));
            options.remove(SHOW_EDITOR_KEY);
        }
        if(options.containsKey(SUCCESS_RATE_KEY)){
            setSuccessRate(Double.parseDouble(options.get(SUCCESS_RATE_KEY)));
            options.remove(SUCCESS_RATE_KEY);
        }
        if(options.containsKey(WEIGHT_KEY)){
            setW1(options.get(WEIGHT_KEY));
            options.remove(WEIGHT_KEY);
        }
        if(options.containsKey(SUCCESS_KEY)){
            setSuccess(Integer.parseInt(options.get(SUCCESS_KEY)));
            options.remove(SUCCESS_KEY);
        }
        if(options.containsKey(OPERATORS_KEY)){
            setOps(Integer.parseInt(options.get(OPERATORS_KEY)));
            options.remove(OPERATORS_KEY);
        }
        if(options.containsKey(TIMEOUT_KEY)){
            setTimeout(Integer.parseInt(options.get(TIMEOUT_KEY)));
            options.remove(TIMEOUT_KEY);
        }
        if(options.containsKey(SUFFIX_KEY)){
            setFitnessSuffix(options.get(SUFFIX_KEY));
            options.remove(SUFFIX_KEY);
        }
        if(!options.isEmpty()){
            System.err.println("Unused arguments: "+options);
        }
    }

    public String buildSingleArgs(){
        return String.join(" ", buildArgs());
    }
    public List<String> buildArgs(){
        List<String> output=new ArrayList<>();
        BiFunction <String,String,String> combiner=(k,v)->k+ '=' +v;

        output.add(combiner.apply(SEED_KEY,String.valueOf(getSeed())));
        output.add(combiner.apply(EPOCHS_KEY,String.valueOf(getEPOCHS())));
        output.add(combiner.apply(POOL_SIZE_KEY,String.valueOf(getPOOL_SIZE())));
        output.add(combiner.apply(WIDTH_KEY,String.valueOf(getEPOCH_WIDTH())));
        output.add(combiner.apply(ACTIONS_KEY,String.valueOf(getNUM_ACTIONS())));
        output.add(combiner.apply(MUTATION_KEY,String.valueOf(getMUTATION_RATE())));
        output.add(combiner.apply(EPSILON_KEY,String.valueOf(getEPSILON())));
        output.add(combiner.apply(TRACES_KEY,String.valueOf(getTRAIN_SIZE())));
        output.add(combiner.apply(TRAIN_KEY,String.valueOf(getTRAIN_RATE())));

        output.add(combiner.apply(META_KEY,isMetaOutput()?"1":"0"));
        output.add(combiner.apply(ER_KEY,isEpochResultOut()?"1":"0"));
        output.add(combiner.apply(ORDERED_KEY,isOrdered()?"1":"0"));
        output.add(combiner.apply(SUPPRESS_WARNINGS_KEY,isSuppressWarnings()?"1":"0"));
        output.add(combiner.apply(OUTPUT_TRACES_KEY,isOutputTraces()?"1":"0"));
        output.add(combiner.apply(SHOW_EDITOR_KEY,isShowEditor()?"1":"0"));

        output.add(combiner.apply(SUCCESS_RATE_KEY,String.valueOf(getSuccessRate())));
        output.add(combiner.apply(WEIGHT_KEY,String.valueOf(getW1().doubleValue())));
        output.add(combiner.apply(SUCCESS_KEY,String.valueOf(getSuccess())));
        output.add(combiner.apply(OPERATORS_KEY,String.valueOf(getOpsCode())));
        output.add(combiner.apply(TIMEOUT_KEY,String.valueOf(getTimeout())));
        output.add(combiner.apply(SUFFIX_KEY,String.valueOf(getFitnessSuffix())));

        return output;
    }
    @Override
    public LearnerConstraints clone() {
        try {
            return (LearnerConstraints)super.clone();
        } catch (CloneNotSupportedException e) {
            return new LearnerConstraints();
        }
    }
}
