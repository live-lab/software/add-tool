/**
 * A package for learning ADDs.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 18.08.2020
 */
package addtool.learning;