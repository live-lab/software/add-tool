package addtool.semantics.selfpruning;

interface SPADTVisitor {
    default void visit(AndV v){

    }
    default void visit(OrV v){

    }
    default void visit(SAndV v){

    }
    default void visit(SOrV v){

    }
    default void visit(NatV v){

    }
    default void visit(NotV v){

    }
    default void visit(NotTrueV v){

    }
    default void visit(BasicEventV v){

    }
    default void visit(ValueV v){

    }
    default void visit(TriggerV v){

    }
    default void visit1(AndV v){

    }
    default void visit1(OrV v){

    }
    default void visit1(SAndV v){

    }
    default void visit1(SOrV v){

    }
    default void visit1(NatV v){

    }
    default void visit1(NotV v){

    }
    default void visit1(NotTrueV v){

    }
    default void visit1(BasicEventV v){

    }
    default void visit1(ValueV v){

    }
    default void visit1(TriggerV v){

    }
}
