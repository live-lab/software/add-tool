package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.*;
import java.util.stream.Collectors;

public class NotV extends UnaryV {

    public NotV(VertexV pred){
        super(pred);
    }

    public NotV(Value v, VertexV pred){
        super(v, pred);
    }

    @Override
    public VertexV next(Map<Integer, Value> toChange) {
        VertexV newPred = predecessor.next(toChange);
        Value newVal = Value.not(newPred.val);
        if(newPred instanceof ValueV){
            return new ValueV(newVal);
        }
        if(newPred instanceof NotV){
            return ((NotV) newPred).predecessor;
        }
        if(newPred instanceof BasicEventV){
            // Just negate id. -> Negated ID will invert the probability
            return new BasicEventV(-((BasicEventV) newPred).id);
        }
        if(newPred instanceof OrV){
            List<VertexV> newPreds = ((OrV) newPred).predecessors.stream()
                    .map(x -> new NotV(Value.not(x.val), x).next(new HashMap<>())).collect(Collectors.toList());
            return new AndV(newVal, newPreds);
        }
        return new NotV(newVal, newPred);
    }

    @Override
    public List<String> toMultilineString() {
        List<String> list = new LinkedList<>();
        list.add("NOT " + val);
        list.addAll(predecessor.toMultilineString().stream().map(x -> "  " + x).collect(Collectors.toList()));
        return list;
    }

    @Override
    public void accept(SPADTVisitor visitor) {
        visitor.visit1(this);
        predecessor.accept(visitor);
        visitor.visit(this);
    }

    @Override
    public void calcHash() {
        hash = Objects.hash("NOT", val, predecessor);
    }
}