package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class NotTrueV extends UnaryV {

    public NotTrueV(VertexV pred){
        super(pred);
    }

    public NotTrueV(Value v, VertexV pred){
        super(v, pred);
    }

    @Override
    public VertexV next(Map<Integer, Value> toChange) {
        VertexV newPred = predecessor.next(toChange);
        Value newVal = newPred.val==Value.TRUE?Value.FALSE:Value.TRUE;
        if(newPred instanceof ValueV){
            return new ValueV(newVal);
        }
        return new NotTrueV(newVal, newPred);
    }

    @Override
    public List<String> toMultilineString() {
        List<String> list = new LinkedList<>();
        list.add("NOTTRUE " + val);
        list.addAll(predecessor.toMultilineString().stream().map(x -> "  " + x).collect(Collectors.toList()));
        return list;
    }

    @Override
    public void accept(SPADTVisitor visitor) {
        visitor.visit1(this);
        predecessor.accept(visitor);
        visitor.visit(this);
    }

    @Override
    public void calcHash() {
        hash = Objects.hash("NOTTRUE", val, predecessor);
    }
}