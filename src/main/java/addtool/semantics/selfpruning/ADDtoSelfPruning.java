package addtool.semantics.selfpruning;

import addtool.add.model.*;
import org.jscience.mathematics.number.Rational;

import java.util.*;
import java.util.stream.Collectors;

public final class ADDtoSelfPruning {

    private ADDtoSelfPruning() {
    }

    public static prADT convert(ADD add){
        VertexV root = ADDtoSelfPruning.convertRoot(add);
        Map<Integer, Player> players = ADDtoSelfPruning.getPlayers(add.getNoSuccessor(false));
        Map<Integer, Rational> probs = ADDtoSelfPruning.getProbs(add.getNoSuccessor(false));
        Map<Integer, Rational> costs = ADDtoSelfPruning.getCosts(add.getNoSuccessor(false));
        return new prADT(root, players, probs, costs);
    }


    public static VertexV convertRoot(ADD add){
        Map<Integer, Set<Trigger>> triggers = resolveTriggers(
                add.getId2Vertex().values().stream()
                        .filter(x -> x instanceof Trigger).map(x -> (Trigger) x).collect(Collectors.toSet()));
        return convert(add.getTop(), triggers).next(new HashMap<>());
    }

    public static VertexV convert(Vertex cur, Map<Integer, Set<Trigger>> triggers) {
        if(cur instanceof And){
            return new AndV(cur.getPredecessors().stream()
                    .map((Vertex v) -> convert(v, triggers)).collect(Collectors.toList()));
        }
        else if(cur instanceof Or){
            return new OrV(cur.getPredecessors().stream()
                    .map((Vertex v) -> convert(v, triggers)).collect(Collectors.toList()));
        }
        else if(cur instanceof BasicEventPlayer){
            int id = getID(cur);
            Set<Trigger> beTriggers = triggers.get(id);
            if(beTriggers == null || beTriggers.isEmpty()){
                return new BasicEventV(id);
            }
            else if (beTriggers.size() == 1){
                TriggerV newTrigger = new TriggerV(convert(beTriggers.iterator().next().getPredecessor(), triggers));
                return new BasicEventV(id, newTrigger);
            }
            else{
                /* beTriggers.size() > 1
                * Now convert to single trigger with OR below as described in Expected Costs paper by Julia
                */
                OrV orBelowTrigger = new OrV(beTriggers.stream().map(x -> convert(x.getPredecessor(), triggers))
                        .collect(Collectors.toList()));
                return new BasicEventV(id, new TriggerV(orBelowTrigger));
            }
        }
        else if(cur instanceof Nat){
            return new NatV(convert(((Nat) cur).getPredecessor(), triggers));
        }
        else if(cur instanceof Not){
            return new NotV(convert(((Not) cur).getPredecessor(), triggers));
        }
        else if(cur instanceof NotTrue){
            return new NotTrueV(convert(((NotTrue) cur).getPredecessor(), triggers));
        }
        else if(cur instanceof SAnd){
            return convertSAnd(cur.getPredecessors(), triggers);
        }
        else if(cur instanceof SOr){
            return convertSOr(cur.getPredecessors(), triggers);
        }
        // No check for instanceof Trigger, because Triggers are converted within the BasicEvent case!
        else{
            throw new IllegalArgumentException("Class not supported: " + cur.getClass());
        }
    }

    public static VertexV convertSAnd(List<Vertex> sandPredecessors, Map<Integer, Set<Trigger>> triggers) {
        if(sandPredecessors.size()<2){
            throw new IllegalArgumentException("SAnd must have at least two predecessors!");
        }
        else if(sandPredecessors.size()==2){
            VertexV pred1 = convert(sandPredecessors.get(0), triggers);
            VertexV pred2 = convert(sandPredecessors.get(1), triggers);
            return new SAndV(pred1, pred2);
        }
        else{
            VertexV pred2 = convert(sandPredecessors.remove(sandPredecessors.size()-1), triggers);
            VertexV pred1 = convertSAnd(sandPredecessors, triggers);
            return new SAndV(pred1, pred2);
        }
    }

    public static VertexV convertSOr(List<Vertex> sorPredecessors, Map<Integer, Set<Trigger>> triggers) {
        if(sorPredecessors.size()<2){
            throw new IllegalArgumentException("SOr must have at least two predecessors!");
        }
        else if(sorPredecessors.size()==2){
            VertexV pred1 = convert(sorPredecessors.get(0), triggers);
            VertexV pred2 = convert(sorPredecessors.get(1), triggers);
            return new SOrV(pred1, pred2);
        }
        else{
            VertexV pred2 = convert(sorPredecessors.remove(sorPredecessors.size()-1), triggers);
            VertexV pred1 = convertSOr(sorPredecessors, triggers);
            return new SOrV(pred1, pred2);
        }
    }

    public static Map<Integer, Set<Trigger>> resolveTriggers(Set<Trigger> allTriggers){
        Map<Integer, Set<Trigger>> map = new HashMap<>();
        for(Trigger t : allTriggers){
            for(BasicEvent be : t.getToTrigger()){
                int id = getID(be);
                if(!map.containsKey(id)){
                    map.put(id, new HashSet<>());
                }
                map.get(id).add(t);
            }
        }
        return map;
    }

    public static Map<Integer, Player> getPlayers(Collection<Vertex> noSuccessor) {
        Map<Integer, Player> map = new HashMap<>();
        Set<Vertex> alreadySeen = new HashSet<>(noSuccessor);
        List<Vertex> workset = new LinkedList<>(noSuccessor);
        while(!workset.isEmpty()){
            Vertex cur = workset.remove(0);
            if(cur instanceof BasicEventPlayer){
                BasicEventPlayer be = (BasicEventPlayer) cur;
                map.put(getID(cur), be.getPlayer());
            }
            for(Vertex pred : cur.getPredecessors()){
                if(!alreadySeen.contains(pred)){
                    alreadySeen.add(pred);
                    workset.add(pred);
                }
            }
        }
        return map;
    }

    private static int getID(Vertex v){
        return Math.abs(Integer.parseInt(v.getId())) + 1;
    }

    public static Map<Integer, Rational> getProbs(Collection<Vertex> noSuccessor) {
        Map<Integer, Rational> map = new HashMap<>();
        Set<Vertex> alreadySeen = new HashSet<>(noSuccessor);
        List<Vertex> workset = new LinkedList<>(noSuccessor);
        while(!workset.isEmpty()){
            Vertex cur = workset.remove(0);
            if(cur instanceof BasicEventPlayer){
                BasicEventPlayer be = (BasicEventPlayer) cur;
                map.put(getID(cur), be.getSuccessProbability());
            }
            for(Vertex pred : cur.getPredecessors()){
                if(!alreadySeen.contains(pred)){
                    alreadySeen.add(pred);
                    workset.add(pred);
                }
            }
        }
        return map;
    }

    public static Map<Integer, Rational> getCosts(Collection<Vertex> noSuccessor) {
        Map<Integer, Rational> map = new HashMap<>();
        Set<Vertex> alreadySeen = new HashSet<>(noSuccessor);
        List<Vertex> workset = new LinkedList<>(noSuccessor);
        while(!workset.isEmpty()){
            Vertex cur = workset.remove(0);
            if(cur instanceof BasicEventPlayer){
                BasicEventPlayer be = (BasicEventPlayer) cur;
                map.put(getID(cur), be.getCost());
            }
            for(Vertex pred : cur.getPredecessors()){
                if(!alreadySeen.contains(pred)){
                    alreadySeen.add(pred);
                    workset.add(pred);
                }
            }
        }
        return map;
    }
}
