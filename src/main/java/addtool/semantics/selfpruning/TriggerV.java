package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


public class TriggerV extends UnaryV {

    public TriggerV(VertexV pred){
        super(pred);
    }

    public TriggerV(Value v, VertexV pred){
        super(pred);
    }

    @Override
    public VertexV next(Map<Integer, Value> toChange) {
        VertexV newPred = predecessor.next(toChange);
        Value newVal = newPred.val;
        if(newPred instanceof ValueV){
            return new ValueV(newVal);
        }
        return new TriggerV(newVal, newPred);
    }

    @Override
    public List<String> toMultilineString() {
        List<String> list = new LinkedList<>();
        list.add("TR " + val);
        list.addAll(predecessor.toMultilineString().stream().map(x -> "  " + x).collect(Collectors.toList()));
        return list;
    }

    @Override
    public void accept(SPADTVisitor visitor) {
        visitor.visit1(this);
        predecessor.accept(visitor);
        visitor.visit(this);
    }

    @Override
    public void calcHash() {
        hash = Objects.hash("TR", val, predecessor);
    }
}