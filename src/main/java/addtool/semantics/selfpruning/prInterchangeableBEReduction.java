package addtool.semantics.selfpruning;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class prInterchangeableBEReduction implements prAvailableMoveImplementation{

    private final prADT adt;
    private final prDefaultAvailableMoves defaultImpl;

    public prInterchangeableBEReduction(prADT adt){
        this.adt = adt;
        this.defaultImpl = new prDefaultAvailableMoves(adt);
    }

    @Override
    public Collection<prMove> availableMoves(prState state) {
        Collection<prMove> allMoves = defaultImpl.availableMoves(state);

        return allMoves.stream().filter(m -> m.getBeIDs().stream().noneMatch(beID ->
                findSmallerPreNotInMoveCandidates(state.vertex, adt, beID, m).stream().anyMatch(beID2 ->
                        checkSmallerPreReallyInterchangeable(state.vertex, adt, beID, beID2)))).collect(Collectors.toList());
    }

    private Set<Integer> findSmallerPreNotInMoveCandidates(VertexV v, prADT adt, int beID, prMove m) {
        Set<Integer> res = new HashSet<>();
        v.accept(new SPADTVisitor() {
            @Override
            public void visit(AndV v) {
                localCheck(v.predecessors);
            }

            @Override
            public void visit(OrV v) {
                localCheck(v.predecessors);
            }

            private void localCheck(List<VertexV> predecessors){
                if(predecessors.stream().filter(x -> x instanceof BasicEventV)
                        .anyMatch(be -> ((BasicEventV) be).id == beID)){
                    res.addAll(
                            predecessors.stream()
                                    .filter(x -> (x instanceof BasicEventV))
                                    .map(x -> (BasicEventV) x)
                                    .filter(be2 -> adt.getPlayer(beID) == adt.getPlayer(be2.id)
                                            && be2.triggered()
                                            && isSmaller(be2.id, beID)
                                            && !m.getBeIDs().contains(be2.id))
                                    .map(be -> be.id)
                                    .collect(Collectors.toList()));
                }
            }

            private boolean isSmaller(int beID2, int beID1){
                return beID2 < beID
                        && adt.getProbability(beID).equals(adt.getProbability(beID2))
                        && adt.getCost(beID).equals(adt.getCost(beID2));
            }
        });
        return res;
    }

    static boolean checkSmallerPreReallyInterchangeable(VertexV v, prADT adt, int beID1, int beID2) {
        final Boolean[] res = {true};
        v.accept(new SPADTVisitor() {
            @Override
            public void visit(AndV v) {
                Set<Integer> predIDs = v.predecessors.stream()
                        .filter(x -> x instanceof BasicEventV)
                        .map(x -> ((BasicEventV) x).id)
                        .collect(Collectors.toSet());
                res[0] = res[0] && (predIDs.contains(beID1) == predIDs.contains(beID2));
            }

            @Override
            public void visit(OrV v) {
                Set<Integer> predIDs = v.predecessors.stream()
                        .filter(x -> x instanceof BasicEventV)
                        .map(x -> ((BasicEventV) x).id)
                        .collect(Collectors.toSet());
                res[0] = res[0] && (predIDs.contains(beID1) == predIDs.contains(beID2));
            }

            public void visit(NatV v){
                if(v.predecessor instanceof BasicEventV &&
                        (((BasicEventV) v.predecessor).id == beID1 || ((BasicEventV) v.predecessor).id == beID2)){
                    res[0] = false;
                }
            }

            public void visit(NotTrueV v){
                if(v.predecessor instanceof BasicEventV &&
                        (((BasicEventV) v.predecessor).id == beID1 || ((BasicEventV) v.predecessor).id == beID2)){
                    res[0] = false;
                }
            }

            public void visit(NotV v){
                if(v.predecessor instanceof BasicEventV &&
                        (((BasicEventV) v.predecessor).id == beID1 || ((BasicEventV) v.predecessor).id == beID2)){
                    res[0] = false;
                }
            }

            public void visit(TriggerV v){
                if(v.predecessor instanceof BasicEventV &&
                        (((BasicEventV) v.predecessor).id == beID1 || ((BasicEventV) v.predecessor).id == beID2)){
                    res[0] = false;
                }
            }
        });
        return res[0];
    }
}
