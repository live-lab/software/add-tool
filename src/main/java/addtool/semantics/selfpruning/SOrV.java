package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class SOrV extends VertexV {

    public final VertexV predecessor1;
    public final VertexV predecessor2;

    public SOrV(VertexV pred1, VertexV pred2){
        super();
        this.predecessor1 = pred1;
        this.predecessor2 = pred2;
    }

    public SOrV(Value v, VertexV pred1, VertexV pred2){
        super(v);
        this.predecessor1 = pred1;
        this.predecessor2 = pred2;
    }

    @Override
    public VertexV next(Map<Integer, Value> toChange) {
        VertexV newPred1 = predecessor1.next(toChange);
        VertexV newPred2 = predecessor2.next(toChange);
        switch (newPred1.val) {
            case TRUE -> {
                switch (newPred2.val){
                    case TRUE, FALSE -> {
                        return new ValueV(Value.FALSE);
                    }
                    case UNDECIDED -> {
                        return new ValueV(Value.TRUE);
                    }
                    default -> throw new IllegalStateException("Unexpected value: " + newPred2.val);
                }
            }
            case FALSE -> {
                switch (newPred2.val){
                    case TRUE -> {
                        if(predecessor1.val == Value.FALSE){
                            return new ValueV(Value.TRUE);
                        }
                        else{
                            return new ValueV(Value.FALSE);
                        }
                    }
                    case FALSE -> {
                        return new ValueV(Value.FALSE);
                    }
                    case UNDECIDED -> {
                        if(newPred1 instanceof ValueV) {
                            return newPred2;
                        }
                        else{
                            return new SOrV(newPred1, newPred2);
                        }
                    }
                    default -> throw new IllegalStateException("Unexpected value: " + newPred2.val);
                }
            }
            case UNDECIDED -> {
                switch (newPred2.val) {
                    case TRUE, FALSE -> {
                        return new ValueV(Value.FALSE);
                    }
                    case UNDECIDED -> {
                        return new SOrV(Value.UNDECIDED, newPred1, newPred2);
                    }
                    default -> throw new IllegalStateException("Unexpected value: " + newPred2.val);
                }
            }
            default -> throw new IllegalStateException("Unexpected value: " + newPred1.val);
        }
    }

    @Override
    public List<String> toMultilineString() {
        List<String> list = new LinkedList<>();
        list.add("SOR " + val);
        list.addAll(predecessor1.toMultilineString().stream().map(x -> "  " + x).collect(Collectors.toList()));
        list.addAll(predecessor2.toMultilineString().stream().map(x -> "  " + x).collect(Collectors.toList()));
        return list;
    }

    @Override
    public void accept(SPADTVisitor visitor) {
        visitor.visit1(this);
        predecessor1.accept(visitor);
        predecessor2.accept(visitor);
        visitor.visit(this);
    }

    @Override
    public void calcHash() {
        hash = Objects.hash("SOR", val, predecessor1, predecessor2);
    }
}