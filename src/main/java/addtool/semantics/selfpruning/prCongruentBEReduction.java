package addtool.semantics.selfpruning;

import addtool.add.model.Player;
import org.jscience.mathematics.number.Rational;

import java.util.*;
import java.util.stream.Collectors;

public class prCongruentBEReduction implements prAvailableMoveImplementation{

    private final prADT adt;
    private final prAvailableMoveImplementation impl;
    private prAvailableBEImplementation filterCandidates = null;

    public prCongruentBEReduction(prADT adt){
        this.adt = adt;
        this.impl = new prDefaultAvailableMoves(adt);
    }

    protected prCongruentBEReduction(prADT adt, prConfluence impl){
        this.adt = adt;
        this.impl = impl;
        this.filterCandidates = impl;
    }

    @Override
    public Collection<prMove> availableMoves(prState state) {
        Collection<prMove> allMoves = impl.availableMoves(state);

        Parities parities = new Parities();
        state.vertex.accept(parities);

        return allMoves.stream().filter(m -> m.getBeIDs().stream().noneMatch(beID ->
                        ((filterCandidates != null)?
                                findSmallerPreNotInMoveCandidates(state.vertex, adt, beID, m, parities).stream().filter(
                                        idCand -> filterCandidates.availableBEs(state).contains(idCand)
                                        )
                                :
                                findSmallerPreNotInMoveCandidates(state.vertex, adt, beID, m, parities).stream()
                                )
                        .anyMatch(beID2 ->
                        prInterchangeableBEReduction.checkSmallerPreReallyInterchangeable(state.vertex, adt, beID, beID2))))
                .collect(Collectors.toList());
    }

    private static class Parities implements SPADTVisitor {
        private final Map<Integer, Integer> parents = new HashMap<>();
        private final Map<Integer, Integer> parity = new HashMap<>();
        private final Map<Integer, Integer> beParity = new HashMap<>();

        public int getParity(Integer i){
            return beParity.get(i);
        }

        @Override
        public void visit1(AndV v) {
            v.predecessors.forEach(x -> parents.put(x.getUniqueID(), v.getUniqueID()));
            int vParity = parity.getOrDefault(parents.get(v.getUniqueID()), 1);
            if(parity.get(v.getUniqueID()) == null){
                parity.put(v.getUniqueID(), vParity);
            }
            else if(parity.get(v.getUniqueID()) != vParity) {
                parity.put(v.getUniqueID(), 0);
            }
        }

        @Override
        public void visit1(OrV v) {
            v.predecessors.forEach(x -> parents.put(x.getUniqueID(), v.getUniqueID()));
            int vParity = parity.getOrDefault(parents.get(v.getUniqueID()), 1);
            if(parity.get(v.getUniqueID()) == null){
                parity.put(v.getUniqueID(), vParity);
            }
            else if(parity.get(v.getUniqueID()) != vParity) {
                parity.put(v.getUniqueID(), 0);
            }
        }

        @Override
        public void visit1(SAndV v) {
            parents.put(v.predecessor1.getUniqueID(), v.getUniqueID());
            parents.put(v.predecessor2.getUniqueID(), v.getUniqueID());
            parity.put(v.getUniqueID(), 0);
        }

        @Override
        public void visit1(SOrV v) {
            parents.put(v.predecessor1.getUniqueID(), v.getUniqueID());
            parents.put(v.predecessor2.getUniqueID(), v.getUniqueID());
            parity.put(v.getUniqueID(), 0);
        }

        @Override
        public void visit1(NatV v) {
            parents.put(v.predecessor.getUniqueID(), v.getUniqueID());
            parity.put(v.getUniqueID(), 0);
        }

        @Override
        public void visit1(NotV v) {
            parents.put(v.predecessor.getUniqueID(), v.getUniqueID());
            int vParity = -parity.getOrDefault(parents.get(v.getUniqueID()), 1);
            if(parity.get(v.getUniqueID()) == null){
                parity.put(v.getUniqueID(), vParity);
            }
            else if(parity.get(v.getUniqueID()) != vParity) {
                parity.put(v.getUniqueID(), 0);
            }
        }

        @Override
        public void visit1(NotTrueV v) {
            parents.put(v.predecessor.getUniqueID(), v.getUniqueID());
            // TODO, can I change this to a -1 like Not?
            parity.put(v.getUniqueID(), 0);
        }

        @Override
        public void visit1(BasicEventV v) {
            v.trigger.ifPresent(x -> parents.put(x.getUniqueID(), v.getUniqueID()));
            int vParity = beParity.getOrDefault(parents.get(v.getUniqueID()), 1);
            if(beParity.get(v.id) == null){
                parity.put(v.getUniqueID(), vParity);
                beParity.put(v.id, vParity);
            }
            else if(beParity.get(v.id) != vParity) {
                parity.put(v.getUniqueID(), 0);
                beParity.put(v.id, 0);
            }
            parity.put(v.getUniqueID(), vParity);
        }

        @Override
        public void visit1(ValueV v) {
            parity.put(v.getUniqueID(), 0);
        }

        @Override
        public void visit1(TriggerV v) {
            parents.put(v.predecessor.getUniqueID(), v.getUniqueID());
            int vParity = parity.getOrDefault(parents.get(v.getUniqueID()), 1);
            if(parity.get(v.getUniqueID()) == null){
                parity.put(v.getUniqueID(), vParity);
            }
            else if(parity.get(v.getUniqueID()) != vParity) {
                parity.put(v.getUniqueID(), 0);
            }
        }
    }

    private Set<Integer> findSmallerPreNotInMoveCandidates(VertexV root, prADT adt, Integer absID, prMove m, Parities parities) {
        Set<Integer> res = new HashSet<>();
        root.accept(new SPADTVisitor() {
            @Override
            public void visit(AndV v) {
                localCheck(v.predecessors);
            }

            @Override
            public void visit(OrV v) {
                localCheck(v.predecessors);
            }

            private void localCheck(List<VertexV> predecessors){
                List<BasicEventV> besWithSameAbsID = predecessors.stream()
                        .filter(x -> x instanceof BasicEventV).map(x -> (BasicEventV) x)
                        .filter(be1 -> be1.absID() == absID).collect(Collectors.toList());
                // if no predecessor we can find no candidates, because the filter shall not be applied to this vertex,
                // if we have 2 BasicEvents, that match the absID we do not filter either
                if(besWithSameAbsID.size() == 1){
                    int be1ID = besWithSameAbsID.get(0).id;
                    res.addAll(
                            predecessors.stream()
                                .filter(x -> (x instanceof BasicEventV))
                                .map(x -> (BasicEventV) x)
                            .filter(be2 -> adt.getPlayer(be1ID) == adt.getPlayer(be2.id)
                                    && be2.triggered()
                                    && isSmaller(be2.id, be1ID)
                                    && !m.getBeIDs().contains(be2.id))
                            .map(be -> be.id)
                            .collect(Collectors.toList()));
                }
            }

            private boolean isSmaller(Integer be1Id, Integer be2Id){
                Rational prob1 = adt.getProbability(be1Id);
                Rational prob2 = adt.getProbability(be2Id);
                Rational cost1 = adt.getCost(be1Id);
                Rational cost2 = adt.getCost(be2Id);
                int parity = parities.getParity(be1Id); // == parity2, by previous check outside.
                Player player = adt.getPlayer(be1Id); // == player2, by previous check
                // Precompute lowest by equiv class
                boolean isSmallerByInterOrder = prob2.equals(prob1) &&
                        (cost2.isLessThan(cost1) || cost2.equals(cost1) && be2Id < be1Id);
                if(isSmallerByInterOrder){
                    return true;
                }
                if(parity > 0 && player == Player.Attacker
                        || parity < 0 && player == Player.Defender){
                    // Get highest prob
                    if(prob2.isLessThan(prob1)){
                        return true;
                    }
                }
                else if(parity < 0 && player == Player.Attacker
                        || parity > 0 && player == Player.Defender){
                    // Get lowest prob
                    if(prob1.isLessThan(prob2)){
                        return true;
                    }
                }
                return false;
            }
        });
        return res;
    }
}
