package addtool.semantics.selfpruning;

import java.util.Collection;

@FunctionalInterface
public interface prAvailableBEImplementation {
    Collection<Integer> availableBEs(prState state);
}
