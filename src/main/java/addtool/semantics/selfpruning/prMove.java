package addtool.semantics.selfpruning;

import addtool.helpers.PowerSet;
import addtool.semantics.Value;
import com.google.common.collect.Streams;
import org.jscience.mathematics.number.Rational;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class prMove {
    private final Set<Integer> beIDs;

    public prMove(Set<Integer> beIDs) {
        this.beIDs = beIDs;
    }

    public Set<Integer> getBeIDs(){
        return beIDs;
    }

    public Collection<Map<Integer, Value>> getPossibleValues(){
        Iterable<Set<Integer>> its = new PowerSet<>(beIDs);
        return Streams.stream(its).map(this::computeMapping).collect(Collectors.toList());
    }

    private Map<Integer, Value> computeMapping(Set<Integer> trueInts){
        Map<Integer, Value> map = new HashMap<>();
        for(Integer i : beIDs){
            map.put(i,trueInts.contains(i)? Value.TRUE : Value.FALSE);
        }
        return map;
    }

    public static Rational computeMappingProbability(Map<Integer, Value> mapping, prADT adt){
        Rational prob = Rational.ONE;
        for(Map.Entry<Integer, Value> e : mapping.entrySet()){
            if(e.getValue() == Value.TRUE){
                if(adt.getProbability(e.getKey()).isGreaterThan(Rational.ONE)){
                    System.out.println(adt.getProbability(e.getKey()));
                }
                prob = prob.times(adt.getProbability(e.getKey()));
            }
            else{
                prob = prob.times(Rational.ONE.minus(adt.getProbability(e.getKey())));
            }
        }
        if(prob.isGreaterThan(Rational.ONE)){
            System.out.println(prob);
        }
        return prob;
    }

    private static void assertFalse(boolean greaterThan) {
        throw new IllegalStateException("Prob > 1");
    }
}
