package addtool.semantics.selfpruning;

import addtool.add.model.Player;
import addtool.semantics.Distribution;
import addtool.semantics.Value;
import org.jscience.mathematics.number.Rational;

import java.util.HashMap;

public class prBellmanVisitor {

    private final prGame game;
    private final HashMap<prState, Rational> values = new HashMap<>();
    private final HashMap<prState, prMove> moves = new HashMap<>();

    public prBellmanVisitor(prGame game) {
        this.game = game;
    }

    // Calculates and returns the Probability of the root event.
    public Rational getRootVal(){
        this.game.accept(this);
        return this.getValue(this.game.initialState());
    }

    public void visit(prState state) {
        if (values.containsKey(state)) {
            // already treated this state
        } else if (state.rootValue() == Value.FALSE) {
            values.put(state, Rational.ZERO);
        } else if (state.rootValue() == Value.TRUE) {
            values.put(state, Rational.ONE);
        } else {
            if (state.player == Player.Attacker) {
                Rational maxProb = Rational.ZERO;
                prMove best = null;
                for (prMove move : state.getAvailableMoves()) {
                    Rational probMove = computeProb(state, move);
                    // vM >= max  is  !(vM < max) ; >= to avoid null move being used.
                    if (probMove.isLargerThan(maxProb) || probMove.equals(maxProb)) {
                        maxProb = probMove;
                        best = move;
                    }
                }
                values.put(state, maxProb);
                moves.put(state, best);
            } else {
                Rational minProb = Rational.ONE;
                prMove best = null;
                for (prMove move : state.getAvailableMoves()) {
                    Rational probMove = computeProb(state, move);
                    // vM <= min  is  ! vM > min ; <= to avoid null move being used.
                    if (probMove.isLessThan(minProb) || probMove.equals(minProb)) {
                        minProb = probMove;
                        best = move;
                    }
                }
                values.put(state, minProb);
                moves.put(state, best);
            }
        }
    }

    public prGame getGame() {
        return game;
    }


    private Rational computeProb(prState state, prMove move) {
        Rational probability = Rational.ZERO;
        Distribution<prState> dist = state.getSuccessor(move);

        for (prState succ : dist) {
            Rational valueSucc = values.get(succ);
            Rational probSucc = dist.lookUp(succ);

            // use Bellmann equation here
            probability = probability.plus(valueSucc.times(probSucc));
        }

        return probability;
    }

    @Override
    public String toString() {
        return "ProbabilityVisitor{" + values + '}';
    }

    public Rational getValue(prState state) {
        return values.get(state);
    }
}
