package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class NatV extends UnaryV {

    public NatV(VertexV succ){
        super(succ);
    }

    public NatV(Value v, VertexV succ){
        super(v, succ);
    }

    @Override
    public VertexV next(Map<Integer, Value> toChange) {
        VertexV newSucc = predecessor.next(toChange);
        Value newVal = Value.nat(newSucc.val);
        if(newSucc instanceof ValueV){
            return new ValueV(newVal);
        }
        return new NatV(newVal, newSucc);
    }

    @Override
    public List<String> toMultilineString() {
        List<String> list = new LinkedList<>();
        list.add("NAT " + val);
        list.addAll(predecessor.toMultilineString().stream().map(x -> "  " + x).collect(Collectors.toList()));
        return list;
    }

    @Override
    public void accept(SPADTVisitor visitor) {
        visitor.visit1(this);
        predecessor.accept(visitor);
        visitor.visit(this);
    }

    @Override
    public void calcHash() {
        hash = Objects.hash("NAT", val, predecessor);
    }
}