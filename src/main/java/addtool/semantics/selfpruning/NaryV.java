package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.List;

public abstract class NaryV extends VertexV {

    final List<VertexV> predecessors;

    protected NaryV(List<VertexV> preds) {
        super();
        this.predecessors = preds;
    }

    protected NaryV(Value v, List<VertexV> preds) {
        super(v);
        this.predecessors = preds;
    }
}
