package addtool.semantics.selfpruning;

import java.util.Collection;

@FunctionalInterface
public interface prAvailableMoveImplementation {
    Collection<prMove> availableMoves(prState state);
}
