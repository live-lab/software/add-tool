package addtool.semantics.selfpruning;

import addtool.add.model.Player;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class prConfluence implements prAvailableMoveImplementation, prAvailableBEImplementation{

    private final prADT adt;

    public prConfluence(prADT adt){
        this.adt = adt;
    }

    @Override
    public Collection<prMove> availableMoves(prState state) {
        if(state.player == Player.Defender){
            Collection<prMove> res = state.getAvailableBEs(Player.Defender, adt).stream()
                    .map(x -> new prMove(Collections.singleton(x))).collect(Collectors.toList());
            res.add(new prMove(Collections.emptySet()));
            return res;
        }
        return availableBEs(state).stream()
                .map(x -> new prMove(Collections.singleton(x))).collect(Collectors.toList());
    }

    @Override
    public Collection<Integer> availableBEs(prState state){
        Collection<Integer> availableBEs = state.getAvailableBEs(Player.Attacker, adt);
        // Now remove available BE groups if I am "And" and then Check for monotonicity groups, remove all monotonicity
        // groups that are Attacker only
        if(state.vertex instanceof AndV){
            List<Subtree> subgames = ((AndV) state.vertex).predecessors.stream()
                    .map(Subtree::new).collect(Collectors.toList());
            mergeOverlaps(subgames);
            // assert this merge also works!
            assertNoOverlaps(subgames);
            if(subgames.stream().allMatch(x -> x.onlyAttacker)) {
                // Take smallest subgame
                Optional<Subtree> sortedSets = subgames.stream()
                        .min(Comparator.comparingInt(x -> x.bes.size()));
                // If at least one vertex is present, we need to have at least one BE left, so no isPresent check needed
                availableBEs.retainAll(sortedSets.get().bes);
            }
            else {
                // merge all subgames where the defender is present
                mergeToOneDefender(subgames);
                // assert this merge also works!
                assertOneDefender(subgames);
                // Find the subgame if it will stay true indefinitey
                Optional<Subtree> onlyMixedGameStaysTrue = subgames.stream()
                        .filter(x -> !x.onlyAttacker && x.rootStaysTrue).findFirst();
                // If it does, only consider those and the attacker subgames, which are
                onlyMixedGameStaysTrue.ifPresent(subgame -> availableBEs.retainAll(subgame.bes));
            }
        }
        return availableBEs;
    }


    /*
    @Override
    public Collection<Integer> availableBEs(prState state){
        Collection<Integer> availableBEs = state.getAvailableBEs(Player.Attacker, adt);
        // Now remove available BE groups if I am "And" and then Check for monotonicity groups, remove all monotonicity
        // groups that are Attacker only
        if(state.vertex instanceof AndV){
            List<MonotonicityMap> monotonicityMaps = ((AndV) state.vertex).predecessors.stream()
                    .map(MonotonicityMap::new).collect(Collectors.toList());
            mergeOverlaps(monotonicityMaps);
            if(monotonicityMaps.stream().allMatch(x -> x.rootStaysTrue)){
                List<MonotonicityMap> sortedSets = monotonicityMaps.stream().filter(map -> map.onlyAttacker)
                        .sorted(Comparator.comparingInt(x -> -x.bes.size()))
                        .collect(Collectors.toList());
                for(MonotonicityMap map : sortedSets){
                    if(map.bes.size()<availableBEs.size()){
                        availableBEs.removeAll(map.bes);
                    }
                }
            }
            else {
                mergeSoOnlyOneMayNotStayTrue(monotonicityMaps);
                Optional<MonotonicityMap> toRemove = monotonicityMaps.stream()
                        .filter(x -> x.onlyAttacker && x.rootStaysTrue).findFirst();
                if(toRemove.isPresent() && toRemove.get().bes.size() < availableBEs.size()){
                    availableBEs.removeAll(toRemove.get().bes);
                }
            }
        }
        return availableBEs;
    }*/

    private final class Subtree implements SPADTVisitor{
        public boolean onlyAttacker = true;
        public Set<Integer> bes = new HashSet<>();
        public boolean rootStaysTrue;
        private final Map<VertexV, Boolean> willStayTrue = new HashMap<>();
        private final Map<VertexV, Boolean> willStayFalse = new HashMap<>();

        public Subtree(VertexV v){
            v.accept(this);
            this.rootStaysTrue = willStayTrue.get(v);
        }

        public void visit(AndV v){
            willStayTrue.put(v, v.predecessors.stream().allMatch(willStayTrue::get));
            willStayFalse.put(v, v.predecessors.stream().allMatch(willStayFalse::get));
        }

        public void visit(OrV v){
            willStayTrue.put(v, v.predecessors.stream().allMatch(willStayTrue::get));
            willStayFalse.put(v, v.predecessors.stream().allMatch(willStayFalse::get));
        }

        public void visit(SAndV v){
            willStayTrue.put(v, true);
            willStayFalse.put(v, true);
        }

        public void visit(SOrV v){
            willStayTrue.put(v, true);
            willStayFalse.put(v, true);
        }
        public void visit(NatV v){
            willStayTrue.put(v, false);
            willStayFalse.put(v, willStayTrue.get(v.predecessor) && willStayFalse.get(v.predecessor));
        }
        public void visit(NotV v){
            willStayTrue.put(v, willStayFalse.get(v.predecessor));
            willStayFalse.put(v, willStayTrue.get(v.predecessor));
        }
        public void visit(NotTrueV v){
            willStayTrue.put(v, false);
            willStayFalse.put(v, willStayTrue.get(v.predecessor));
        }
        public void visit(BasicEventV v){
            if(adt.getPlayer(v.absID()) == Player.Defender){
                this.onlyAttacker = false;
            }
            bes.add(v.absID());
            willStayTrue.put(v, true);
            willStayFalse.put(v, true);
        }
        public void visit(ValueV v){
            willStayTrue.put(v, true);
            willStayFalse.put(v, true);
        }
        public void visit(TriggerV v){
            willStayTrue.put(v, willStayTrue.get(v.predecessor));
            willStayFalse.put(v, willStayFalse.get(v.predecessor));
        }
        public void merge(Subtree map2) {
            this.rootStaysTrue &= map2.rootStaysTrue;
            this.onlyAttacker &= map2.onlyAttacker;
            this.bes.addAll(map2.bes);
        }
    }

    private static void mergeOverlaps(Collection<Subtree> subtrees){
        for(Subtree tree1 : subtrees){
            for(Subtree tree2 : subtrees){
                if(tree1 != tree2){
                    Collection<Integer> tmp = new HashSet<>(tree1.bes);
                    tmp.addAll(tree2.bes);
                    if(tmp.size() < tree1.bes.size() + tree2.bes.size()){
                        tree1.merge(tree2);
                        subtrees.remove(tree2);
                        mergeOverlaps(subtrees);
                        return;
                    }
                }
            }
        }
    }

    private static void mergeToOneDefender(List<Subtree> subtrees) {
        for(Subtree tree1 : subtrees){
            if(tree1.onlyAttacker) continue;
            for(Subtree tree2 : subtrees){
                if(tree1 != tree2){
                    if(!(tree2.onlyAttacker)){
                        tree1.merge(tree2);
                        subtrees.remove(tree2);
                        mergeToOneDefender(subtrees);
                        return;
                    }
                }
            }
        }
    }

    private void assertNoOverlaps(List<Subtree> subtrees) {
        HashSet<Integer> allIDs = new HashSet<>();
        for(Subtree s : subtrees){
            for(Integer i : s.bes) {
                if(allIDs.contains(i)) {
                    throw new IllegalStateException("Subtrees may not overlap!");
                }
                allIDs.add(i);
            }
        }
    }

    private void assertOneDefender(List<Subtree> subtrees) {
        int numDefender = 0;
        for(Subtree s : subtrees){
            if(!s.onlyAttacker){
                numDefender++;
            }
        }
        assertEquals(1, numDefender);
    }

}
