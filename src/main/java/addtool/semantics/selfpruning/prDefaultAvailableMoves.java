package addtool.semantics.selfpruning;

import addtool.add.model.Player;
import addtool.helpers.PowerSet;
import com.google.common.collect.Streams;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class prDefaultAvailableMoves implements prAvailableMoveImplementation {

    prADT adt;

    public prDefaultAvailableMoves(prADT adt){
        this.adt = adt;
    }

    @Override
    public List<prMove> availableMoves(prState state) {
        int minBEs = state.player == Player.Attacker ? 1 : 0;
        int maxBEs = state.player == Player.Attacker ? adt.maxAttacker : adt.maxDefender;
        Collection<Integer> besForPlayer = state.getAvailableBEs(state.player, adt);
        Iterable<Set<Integer>> avaBes = new PowerSet<>(besForPlayer, minBEs, maxBEs);
        return Streams.stream(avaBes).map(prMove::new).collect(Collectors.toList());
    }

}
