package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A Vertex whose Value cannot change anymore.
 */
public final class ValueV extends VertexV {

    public ValueV(Value v) {
        super(v);
    }

    @Override
    public VertexV next(Map<Integer, Value> toChange) {
        return this;
    }

    @Override
    public List<String> toMultilineString() {
        List<String> list = new LinkedList<>();
        list.add("VALUE " + val);
        return list;
    }


    @Override
    public void accept(SPADTVisitor visitor) {
        visitor.visit1(this);
        visitor.visit(this);
    }

    @Override
    public void calcHash() {
        hash = Objects.hash("VALUE", val);
    }
}
