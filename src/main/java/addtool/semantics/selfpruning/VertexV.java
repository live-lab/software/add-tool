package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.List;
import java.util.Map;

public abstract class VertexV {

    // protected List<ADDState> predecessors;
    public final Value val;

    private final int uniqueID;
    private static Integer idCounter = 1;
    protected Integer hash = null;

    public VertexV() {
        this(Value.UNDECIDED);
    }

    public int getUniqueID(){
        return uniqueID;
    }

    public VertexV(Value v) {
        val = v;
        uniqueID = idCounter;
        synchronized (idCounter){
            idCounter += 1;
        }
    }

    /**
     *
     * @param toChange the BEs that should be changed with their corresponding values to turn to
     * @return the new pruned ADDState
     */
    public abstract VertexV next(Map<Integer, Value> toChange);

    @Override
    public String toString() {
        return String.join("\n", toMultilineString());
    }

    public abstract List<String> toMultilineString();

    public abstract void accept(SPADTVisitor visitor);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VertexV vertexV = (VertexV) o;
        return hashCode() == vertexV.hashCode();
    }

    @Override
    public int hashCode(){
        if(hash == null){
            calcHash();
        }
        return hash;
    }

    abstract void calcHash();
}
