package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.*;
import java.util.stream.Collectors;

public class BasicEventV extends VertexV {

    public final int id;

    final Optional<VertexV> trigger;

    public BasicEventV(int id) {
        this.id = id;
        this.trigger = Optional.empty();
    }

    public BasicEventV(int id, VertexV trigger) {
        this.id = id;
        this.trigger = Optional.of(trigger);
    }

    public int absID(){
        return Math.abs(id);
    }

    @Override
    public VertexV next(Map<Integer, Value> toChange) {
        if(trigger.isPresent()) {
            VertexV newTrigger = trigger.get().next(toChange);

            if(newTrigger.val == Value.TRUE && toChange.containsKey(this.absID())) {
                Value val = toChange.get(this.absID());
                return new ValueV(id>0 ? val:Value.not(val));
            }
            else if (newTrigger instanceof ValueV) {
                if (newTrigger.val == Value.TRUE){
                    return new BasicEventV(this.id);
                }
                else{
                    return new ValueV(Value.UNDECIDED);
                }
            }
            else{
                return new BasicEventV(this.id, newTrigger);
            }
        }
        else {
            if (toChange.containsKey(this.absID())) {
                Value val = toChange.get(this.absID());
                return new ValueV(id > 0 ? val : Value.not(val));
            }
            else {
                return this;
            }
        }
    }

    public boolean triggered() {
        return this.trigger.isEmpty() || this.trigger.get().val == Value.TRUE;
    }

    @Override
    public List<String> toMultilineString() {
        List<String> list = new LinkedList<>();
        list.add("BE " + id);
        trigger.ifPresent(vertexV ->
                list.addAll(vertexV.toMultilineString().stream().map(x -> "  " + x).collect(Collectors.toList())));
        return list;
    }

    @Override
    public void accept(SPADTVisitor visitor) {
        visitor.visit1(this);
        trigger.ifPresent(tr -> tr.accept(visitor));
        visitor.visit(this);
    }

    @Override
    public void calcHash() {
        hash = Objects.hash("BE", id, trigger);
    }
}
