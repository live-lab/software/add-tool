package addtool.semantics.selfpruning;

import addtool.add.model.Player;
import addtool.analysisonadd.Direction;
import addtool.helpers.Pair;
import addtool.semantics.Distribution;
import addtool.semantics.Value;
import org.jscience.mathematics.number.Rational;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A class representing game semantics of self-pruning ADTs.
 * @author julia
 * @version 18/11/21
 * @since 18/11/21
 */

public class prGame {

    private final prADT adt;

    // Map used instead of Set, in order to find exact objects in it.
    // If you create new Objects which are equal to one already in a set (contains returns true)
    // they are practically immutable, which may be undesired.
    private final prState initial;
    private final Map<Pair<VertexV, Player>, prState> states;

    public prGame(prADT adt) {
        this.adt = adt;
        states = new HashMap<>();
        initial = prState.generateState(adt.root, Player.Defender, adt, states);
    }

    /**
     *
     * @param cur the State for which to build the successors
     * @param move the move which will be attempted. Distribution over possibilities
     * @return Distribution over ADDState the attempt of be can lead to
     */
    public Distribution<prState> buildSuccessors(prState cur, prMove move){
        Distribution<prState> dist = new Distribution<>();
        for (Map<Integer, Value> explicitMove : move.getPossibleValues()) {
            Rational probability = prMove.computeMappingProbability(explicitMove, adt);
            prState state = prState.generateState(cur.nextADT(explicitMove), cur.player, adt, states);
            dist.add(probability, state);
        }
        return dist;
    }

    public Rational calculateRootOptimalTup(){
        return new prBellmanVisitor(this).getRootVal();
    }

    public int getSize() {
        return states.keySet().size();
    }

    public prState initialState() {
        return initial;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        prGame game = (prGame) o;

        if (!Objects.equals(adt, game.adt)) return false;
        if (!states.equals(game.states)) return false;
        return Objects.equals(initial, game.initial);
    }

    @Override
    public int hashCode() {
        int result = adt != null ? adt.hashCode() : 0;
        result = 31 * result + states.hashCode();
        result = 31 * result + (initial != null ? initial.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return '(' + states.values().stream().map(prState::toString).collect(Collectors.joining("\n")) +
                ", " + initial + ')';
    }

    public void accept(prBellmanVisitor visitor) {
        initial.accept(visitor);
    }

    public void accept(prBellmanVisitor visitor, Direction direction) {
        if(direction == Direction.DOWN){
            initial.accept(visitor);
        }
        else {
            throw new UnsupportedOperationException("");
        }
    }
}
