package addtool.semantics.selfpruning;

import addtool.add.model.Player;
import addtool.analysisonadd.Direction;
import addtool.analysisonadd.Visited;
import addtool.helpers.Pair;
import addtool.semantics.Distribution;
import addtool.semantics.Value;

import java.util.*;
import java.util.stream.Collectors;

public final class prState implements Visited<prBellmanVisitor> {
    public final VertexV vertex;
    public final Player player;
    // availableBEs are being built on generation because we need to determine the player whose turn it is
    private final Collection<Integer> availableBEs;
    private final Map<prMove, Distribution<prState>> successors = new HashMap<>();
    private boolean discovered = false;

    private prState(VertexV vertex, Player lastPlayer, prADT adt) {
        this.vertex = vertex;
        this.availableBEs = availableBEs(vertex);
        if(lastPlayer == Player.Attacker && getAvailableBEs(Player.Defender, adt).isEmpty()){
            this.player = Player.Attacker;
        }
        else {
            this.player = Player.next(lastPlayer);
        }
    }

    public Collection<Integer> getAvailableBEs(){
        return availableBEs;
    }

    public Collection<Integer> getAvailableBEs(Player player, prADT adt){
        return availableBEs.stream().filter(beID -> adt.getPlayer(beID) == player).collect(Collectors.toSet());
    }


    /**
     * Generates a new state if it is not already in the states collection, otherwise returns the found state.
     * Also builds the availableBEs for the state.
     *
     * @param vertex the configuration of the current adt
     * @param player the player of the state we are coming from
     * @param states the player of the state we are coming from
     * @return the newly generated or found state
     */
    public static prState generateState(VertexV vertex, Player player, prADT adt, Map<Pair<VertexV, Player>, prState> states) {
        Pair<VertexV, Player> pair = new Pair<>(vertex, player);
        prState alreadyIn = states.get(pair);
        if (alreadyIn != null) {
            return alreadyIn;
        }
        else {
            prState state = new prState(vertex, player, adt);
            states.put(pair, state);
            return state;
        }
    }

    public Value rootValue() {
        return vertex.val;
    }

    public Set<prMove> getAvailableMoves() {
        return successors.keySet();
    }

    public Distribution<prState> getSuccessor(prMove move) {
        return successors.get(move);
    }

    public VertexV nextADT(Map<Integer, Value> toChange) {
        return vertex.next(toChange);
    }

    @Override
    public void accept(prBellmanVisitor visitor) {
        if(visitor.getValue(this) == null) {
            for (Distribution<prState> d : successors.values()) {
                d.getSupport().forEach(x -> x.accept(visitor));
            }

            visitor.visit(this);
        }
    }

    @Override
    public void accept(prBellmanVisitor visitor, Direction direction) {
        accept(visitor, Direction.DOWN);
    }

    public Player nextPlayer() {
        return Player.next(player);
    }

    public void setDiscovered() {
        this.discovered = true;
    }

    public boolean isNew() {
        return !this.discovered;
    }

    public void addSuccessor(prMove move, Distribution<prState> dist) {
        this.successors.put(move, dist);
    }

    private Set<Integer> availableBEs(VertexV vertex){
        if(vertex instanceof BasicEventV){
            BasicEventV be = (BasicEventV) vertex;
            if(be.trigger.isPresent()){
                Set<Integer> tmp = new HashSet<>(availableBEs(be.trigger.get()));
                if(be.trigger.get().val == Value.TRUE){
                    tmp.add(be.absID());
                }
                return tmp;
            }
            else{
                return Collections.singleton(be.absID());
            }
        }
        else if (vertex instanceof NaryV){
            return ((NaryV) vertex).predecessors.stream().map(this::availableBEs)
                    .flatMap(Set::stream).collect(Collectors.toSet());
        }
        else if (vertex instanceof UnaryV){
            return availableBEs(((UnaryV) vertex).predecessor);
        }
        else if (vertex instanceof SAndV){
            Set<Integer> tmp = new HashSet<>(availableBEs(((SAndV) vertex).predecessor1));
            tmp.addAll(availableBEs(((SAndV) vertex).predecessor2));
            return tmp;
        }
        else if (vertex instanceof SOrV){
            Set<Integer> tmp = new HashSet<>(availableBEs(((SOrV) vertex).predecessor1));
            tmp.addAll(availableBEs(((SOrV) vertex).predecessor2));
            return tmp;
        }
        else{
            return Collections.emptySet();
        }
    }

    @Override
    public boolean equals(Object o){
        if(o == null || !(o instanceof prState)) return false;
        prState otherState = (prState) o;
        return this.vertex.equals(otherState.vertex) && this.player==otherState.player;
    }

    @Override
    public int hashCode() {
        return Objects.hash(vertex, player);
    }
}
