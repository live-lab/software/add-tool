package addtool.semantics.selfpruning;

import addtool.semantics.Distribution;
import addtool.semantics.Value;

import java.util.LinkedList;

public class prGameGen {

    public static prGame gameGen(prADT adt, prAvailableMoveImplementation ava){
        prGame game = new prGame(adt);

        prState initialState = game.initialState();

        if (initialState.rootValue() == Value.TRUE) {
            return game;
        }

        // LIFO Queue of all discovered and unprocessed states.
        LinkedList<prState> queue = new LinkedList<>();
        queue.add(initialState);
        initialState.setDiscovered();

        int i = 0;
        int queueSize = 1;
        int gameSize = 1;

        // compute all necessary states and transitions
        while (!queue.isEmpty()) {
            queueSize--;
            i++;
            // get next valuation out of the queue and compute available moves for the current player
            prState state = queue.removeLast();
            // System.out.println(state.vertex);

            if(i % 10000 == 0) {
                System.out.println("Yey, we got up to " + i + ", queue-size is " + queueSize + ", gameSize is " + gameSize);
            }

            // compute all moves (depends on the number of events the players can attempts simultaneously)
            Iterable<prMove> availableMoves = ava.availableMoves(state);

            // compute the resulting distribution for each move
            for (prMove move : availableMoves) {

                Distribution<prState> dist = game.buildSuccessors(state, move);
                state.addSuccessor(move, dist);

                for (prState newState : dist) {
                    if(newState.isNew()){
                        gameSize++;
                        newState.setDiscovered();
                        if(newState.rootValue() == Value.UNDECIDED) {
                            queueSize++;
                            queue.add(newState);
                        }
                    }
                }
            }
        }
        System.out.println("Iterations: " + i);
        return game;
    }

}
