package addtool.semantics.selfpruning;

import addtool.semantics.Value;

public abstract class UnaryV extends VertexV {

    public final VertexV predecessor;

    public UnaryV(VertexV pred) {
        predecessor = pred;
    }

    public UnaryV(Value v, VertexV pred) {
        super(v);
        predecessor = pred;
    }
}
