package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.*;
import java.util.stream.Collectors;

public final class AndV extends NaryV {

    public AndV(List<VertexV> preds){
        super(preds);
    }

    public AndV(Value v, List<VertexV> preds){
        super(v, preds);
    }

    @Override
    public VertexV next(Map<Integer, Value> toChange) {
        List<VertexV> newPredecessors = new LinkedList<>();
        for(VertexV preds : predecessors){
            VertexV newPreds = preds.next(toChange);
            if(newPreds instanceof ValueV && newPreds.val == Value.FALSE){
                return newPreds;
            }
            else if(newPreds instanceof ValueV && newPreds.val == Value.TRUE){
                continue;
            }
            else if(newPreds instanceof AndV){
                newPredecessors.addAll(((AndV) newPreds).predecessors);
            }
            else{
                newPredecessors.add(newPreds);
            }
        }
        if(newPredecessors.isEmpty()){
            return new ValueV(Value.TRUE);
        }
        Value newVal = newPredecessors.stream().map(vert -> vert.val).reduce(Value.TRUE, Value::and);
        return new AndV(newVal, newPredecessors);
    }

    @Override
    public List<String> toMultilineString() {
        List<String> list = new LinkedList<>();
        list.add("AND " + val);
        list.addAll(predecessors.stream().flatMap(x -> x.toMultilineString().stream())
                .map(x -> "  " + x).collect(Collectors.toList()));
        return list;
    }

    @Override
    public void accept(SPADTVisitor visitor) {
        visitor.visit1(this);
        predecessors.forEach(x -> x.accept(visitor));
        visitor.visit(this);
    }

    @Override
    public void calcHash() {
        hash = Objects.hash("AND", val, predecessors);
    }
}
