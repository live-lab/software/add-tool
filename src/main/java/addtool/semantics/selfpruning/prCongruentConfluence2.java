package addtool.semantics.selfpruning;

import java.util.Collection;

public class prCongruentConfluence2 implements prAvailableMoveImplementation {

    private final prAvailableMoveImplementation impl;

    public prCongruentConfluence2(prADT adt){
        impl = new prCongruentBEReduction(adt, new prConfluence(adt));
    }

    @Override
    public Collection<prMove> availableMoves(prState state) {
        return impl.availableMoves(state);
    }
}
