package addtool.semantics.selfpruning;

import addtool.add.model.Player;
import org.jscience.mathematics.number.Rational;

import java.util.Map;

public class prADT {

    public final VertexV root;
    private final Map<Integer, Player> playerDef;
    private final Map<Integer, Rational> probabilities;
    private final Map<Integer, Rational> costs;
    public final int maxAttacker = 1;
    public final int maxDefender = 1;

    public prADT(VertexV root, Map<Integer, Player> playerDef, Map<Integer, Rational> probabilities, Map<Integer, Rational> costs) {
        this.root = root;
        this.playerDef = playerDef;
        this.probabilities = probabilities;
        this.costs = costs;
    }

    public Player getPlayer(Integer beID) {
        return playerDef.get(Math.abs(beID));
    }

    public Rational getProbability(Integer id) {
        if(id < 0){
            return Rational.ONE.minus(probabilities.get(-id));
        }
        else {
            return probabilities.get(id);
        }
    }

    public Rational getCost(Integer beID) {
        return costs.get(Math.abs(beID));
    }
}
