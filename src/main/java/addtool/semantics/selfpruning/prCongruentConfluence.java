package addtool.semantics.selfpruning;

import addtool.add.model.Player;
import org.jscience.mathematics.number.Rational;

import java.util.*;
import java.util.stream.Collectors;

public class prCongruentConfluence implements prAvailableMoveImplementation {

    private final prADT adt;
    private final prConfluence impl;


    // NOT USED: proCongruentConfluence2 WORKS
    public prCongruentConfluence(prADT adt){
        impl = new prConfluence(adt);
        this.adt = adt;
    }


    @Override
    public Collection<prMove> availableMoves(prState state) {
        Collection<Integer> bes = impl.availableBEs(state);

        Collection<prMove> allMoves = impl.availableMoves(state);

        ParityVisitor parities = new ParityVisitor();
        state.vertex.accept(parities);

        return allMoves.stream().filter(m -> m.getBeIDs().stream().noneMatch(beID ->
                                findSmallerPreNotInMoveCandidates(state.vertex, adt, beID, m, parities).stream()
                                .anyMatch(beID2 ->
                                        prInterchangeableBEReduction.checkSmallerPreReallyInterchangeable(state.vertex, adt, beID, beID2))))
                .collect(Collectors.toList());
    }

    private static class ParityVisitor implements SPADTVisitor {
        private final Map<VertexV, VertexV> parents = new HashMap<>();
        private final Map<VertexV, Integer> parity = new HashMap<>();
        private final Map<Integer, Integer> beParity = new HashMap<>();

        public int getParity(Integer i){
            return beParity.get(i);
        }

        @Override
        public void visit1(AndV v) {
            v.predecessors.forEach(x -> parents.put(x, v));
            int vParity = parity.getOrDefault(parents.get(v), 1);
            if(parity.get(v) == null){
                parity.put(v, vParity);
            }
            else if(parity.get(v) != vParity) {
                parity.put(v, 0);
            }
        }

        @Override
        public void visit1(OrV v) {
            v.predecessors.forEach(x -> parents.put(x, v));
            int vParity = parity.getOrDefault(parents.get(v), 1);
            if(parity.get(v) == null){
                parity.put(v, vParity);
            }
            else if(parity.get(v) != vParity) {
                parity.put(v, 0);
            }
        }

        @Override
        public void visit1(SAndV v) {
            parents.put(v.predecessor1, v);
            parents.put(v.predecessor2, v);
            parity.put(v, 0);
        }

        @Override
        public void visit1(SOrV v) {
            parents.put(v.predecessor1, v);
            parents.put(v.predecessor2, v);
            parity.put(v, 0);
        }

        @Override
        public void visit1(NatV v) {
            parents.put(v.predecessor, v);
            parity.put(v, 0);
        }

        @Override
        public void visit1(NotV v) {
            parents.put(v.predecessor, v);
            int vParity = -parity.getOrDefault(parents.get(v), 1);
            if(parity.get(v) == null){
                parity.put(v, vParity);
            }
            else if(parity.get(v) != vParity) {
                parity.put(v, 0);
            }
        }

        @Override
        public void visit1(NotTrueV v) {
            parents.put(v.predecessor, v);
            parity.put(v, 0);
        }

        @Override
        public void visit1(BasicEventV v) {
            v.trigger.ifPresent(x -> parents.put(x, v));
            int vParity = parity.getOrDefault(parents.get(v), 1);
            if(parity.get(v) == null){
                parity.put(v, vParity);
                beParity.put(v.id, vParity);
            }
            else if(parity.get(v) != vParity) {
                parity.put(v, 0);
                beParity.put(v.id, 0);
            }
        }

        @Override
        public void visit1(ValueV v) {
            parity.put(v, 0);
        }

        @Override
        public void visit1(TriggerV v) {
            parents.put(v.predecessor, v);
            int vParity = parity.getOrDefault(parents.get(v), 1);
            if(parity.get(v) == null){
                parity.put(v, vParity);
            }
            else if(parity.get(v) != vParity) {
                parity.put(v, 0);
            }
        }
    }

    private Set<Integer> findSmallerPreNotInMoveCandidates(VertexV v, prADT adt, int beID, prMove m, ParityVisitor parities) {
        Set<Integer> res = new HashSet<>();
        v.accept(new SPADTVisitor() {
            @Override
            public void visit(AndV v) {
                localCheck(v.predecessors);
            }

            @Override
            public void visit(OrV v) {
                localCheck(v.predecessors);
            }

            private void localCheck(List<VertexV> predecessors){
                if(predecessors.stream().filter(x -> x instanceof BasicEventV)
                        .anyMatch(be -> ((BasicEventV) be).id == beID)){
                    res.addAll(
                            predecessors.stream()
                                    .filter(x -> (x instanceof BasicEventV))
                                    .map(x -> (BasicEventV) x)
                                    .filter(be2 -> adt.getPlayer(beID) == adt.getPlayer(be2.id)
                                            && be2.triggered()
                                            && isSmaller(be2.id, beID)
                                            && !m.getBeIDs().contains(be2.id))
                                    .map(be -> be.id)
                                    .collect(Collectors.toList()));
                }
            }

            private boolean isSmaller(int beID2, int beID1){
                Rational prob1 = adt.getProbability(beID1);
                Rational prob2 = adt.getProbability(beID2);
                Rational cost1 = adt.getCost(beID1);
                Rational cost2 = adt.getCost(beID2);
                int parity = parities.getParity(beID1); // == parity2, by previous check outside.
                Player player = adt.getPlayer(beID1); // == player2, by previous check
                boolean smallerByLex = (cost2.isLessThan(cost1) ||
                        cost2.equals(cost1) && beID2 < beID1);
                if(prob2.equals(prob1) && smallerByLex){
                    return true;
                }
                if(parity > 0 && player == Player.Attacker
                        || parity < 0 && player == Player.Defender){
                    // Get highest prob (tie -> lowestCost (tie -> lowest lexicographic))
                    if(prob2.isLessThan(prob1)){
                        return true;
                    }
                }
                else if(parity < 0 && player == Player.Attacker
                        || parity > 0 && player == Player.Defender){
                    // Get lowest prob (tie -> lowestCost (tie -> lowest lexicographic))
                    if(prob1.isLessThan(prob2)){
                        return true;
                    }
                }
                return false;
            }
        });
        return res;
    }
}
