package addtool.semantics.selfpruning;

import addtool.semantics.Value;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public final class OrV extends NaryV {

    public OrV(List<VertexV> preds){
        super(preds);
    }

    public OrV(Value v, List<VertexV> preds){
        super(v, preds);
    }

    @Override
    public VertexV next(Map<Integer, Value> toChange) {
        List<VertexV> newPredecessors = new LinkedList<>();
        for(VertexV pred : predecessors){
            VertexV newPred = pred.next(toChange);
            if(newPred instanceof ValueV && newPred.val == Value.TRUE){
                return newPred;
            }
            else if(newPred instanceof ValueV && newPred.val == Value.FALSE){
                continue;
            }
            else if(newPred instanceof OrV){
                newPredecessors.addAll(((OrV) newPred).predecessors);
            }
            else{
                newPredecessors.add(newPred);
            }
        }
        if(newPredecessors.isEmpty()){
            return new ValueV(Value.FALSE);
        }
        Value newVal = newPredecessors.stream().map(vert -> vert.val).reduce(Value.FALSE, Value::or);
        return new OrV(newVal, newPredecessors);
    }

    @Override
    public List<String> toMultilineString() {
        List<String> list = new LinkedList<>();
        list.add("OR " + val);
        list.addAll(predecessors.stream().flatMap(x -> x.toMultilineString().stream())
                .map(x -> "  " + x).collect(Collectors.toList()));
        return list;
    }

    @Override
    public void accept(SPADTVisitor visitor) {
        visitor.visit1(this);
        predecessors.forEach(x -> x.accept(visitor));
        visitor.visit(this);
    }

    @Override
    public void calcHash() {
        hash = Objects.hash("OR", val, predecessors);
    }
}
