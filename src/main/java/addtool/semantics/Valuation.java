package addtool.semantics;

import addtool.add.model.*;
import addtool.analysisonadd.ADDValuationVisitor;
import addtool.analysisonadd.Direction;
import addtool.helpers.PowerSet;
import com.google.common.base.Functions;
import org.jetbrains.annotations.NotNull;
import org.jscience.mathematics.number.Rational;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Stores a Map of vertices and their values.
 * @author julia
 * @version 14/1/18
 * @see Vertex
 * @see Value
 */
public class Valuation implements Iterable<Vertex> {

    private final ADD add;
    private final Map<Vertex, Value> valuation;

    /**
     * Creates new valuation with an existing map.
     * @param valuation hashmap representing a valuation
     */
    private Valuation(ADD add, Map<Vertex, Value> valuation) {
        this.add = add;
        this.valuation = valuation;
    }

    /** Generates the start valuation for a given ADD
     *
     * @param add the add for which to compute the start valuation
     */
    public Valuation(ADD add){
        this.add = add;
        Map<Vertex, Value> undecValMap = add.getVertices().stream()
                .collect(Collectors.toMap(Function.identity(),
                        Functions.constant(Value.UNDECIDED)));
        Valuation undecVal = new Valuation(add, undecValMap);
        ADDValuationVisitor visitor = new ADDValuationVisitor(undecVal, new HashSet<>(), new HashSet<>());
        add.accept(visitor);
        this.valuation = visitor.getVal().getAllValuations();
    }

    public Value getValue(Vertex vertex) {
        Value v = valuation.get(vertex);
        return Objects.requireNonNullElse(v, Value.UNDECIDED);
    }

    public Map<Vertex, Value> getAllValuations() {
        return new HashMap<>(valuation);
    }

    public void addValuation(Vertex vertex, Value v) {
        if (v != Value.UNDECIDED) {
            valuation.put(vertex, v);
        }
        else {
            valuation.remove(vertex);
        }
    }

    public Valuation copy() {
        return new Valuation(add, new HashMap<>(valuation));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Valuation val1 = (Valuation) o;
        Map<Vertex, Value> valuation1 = val1.getAllValuations();

        // check whether every vertex in valuation is also in valuation and whether they have the same value
        for (Map.Entry<Vertex, Value> entry : valuation.entrySet()) {
            Value value = entry.getValue();
            if (!(value.equals(valuation1.get(entry.getKey())))) {
                return false;
            }
        }

        //check whether every vertex in valuation1 is also in valuation
        for (Vertex vertex : valuation1.keySet()) {
            if (!valuation.containsKey(vertex)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        return valuation.hashCode();
    }

    /**
     * computes the successor distribution of valuations for a given move attempted
     *
     * @param move the attempted basic events
     * @return list of the newly generated states and a distribution over valuations reachable
     */
    public Distribution<Valuation> nextValuation(Move move) {
        PowerSet<BasicEvent> turnToTruePowerset = new PowerSet<>(move.getMove());
        Distribution<Valuation> distribution = new Distribution<>();

        for (Set<BasicEvent> turnToTrue : turnToTruePowerset) {
            //compute valuation

            //TODO issue #44 probably goes here
            ADDValuationVisitor visitor = new ADDValuationVisitor(this, turnToTrue, move.getMove());
            add.accept(visitor);
            Valuation nextValuation = visitor.getVal();

            //compute occurrence probability for next valuation
            Rational probability = Rational.ONE;
            for (BasicEvent b : move) {
                if (turnToTrue.contains(b)) {
                    probability = probability.times(b.getSuccessProbability());
                } else {
                    probability = probability.times(Rational.ONE.minus(b.getSuccessProbability()));
                }
            }

            if (probability.isPositive()) {
                distribution.add(probability, nextValuation);
            }
        }
        return distribution;
    }

    /**
     * computes the set of available basic events in the set of vertices
     * in this valuation based on the current player
     *
     * @param player   indicates which player's turn it is
     * @return the set of available basic events for the given player
     */
    public Set<BasicEventPlayer> available(Player player) {

        Collection<Vertex> vertices = add.getVertices();

        // compute all successfully completed Trigger events
        Set<Trigger> completed = new HashSet<>();
        for (Vertex v : vertices) {
            if (v instanceof Trigger && valuation.get(v) == Value.TRUE) {
                completed.add((Trigger) v);
            }
        }

        // check for all uncompleted basic events
        Set<BasicEventPlayer> ava = new HashSet<>();
        for (Vertex v : vertices) {
            if ((v instanceof BasicEventPlayer) && (getValue(v) == Value.UNDECIDED)
                    && (((BasicEventPlayer) v).isPlayer(player))) {
                // available if undecided and non-triggerable
                if (!((BasicEventPlayer) v).isTriggerAble()) {
                    ava.add((BasicEventPlayer) v);
                } else {
                    // available if triggerable and in a set toTrigger of an completed trigger event
                    for (Trigger t : completed) {
                        List<BasicEvent> triggered = t.getToTrigger();
                        if (triggered.contains(v)) {
                            ava.add((BasicEventPlayer) v);
                        }
                    }
                }
            }
        }
        return ava;
    }

    public Rational getCost() {
        Rational cost = Rational.ZERO;
        for (Vertex vertex : this) {
            if (vertex instanceof BasicEvent) {
                cost = cost.plus(((BasicEvent) vertex).getCost());
            }
        }
        return cost;
    }

    public Map<BasicEvent, Value> getBasicEvents(){
        HashMap<BasicEvent, Value> res = new HashMap<>();
        for(Map.Entry<Vertex,Value> entry: this.valuation.entrySet()){
            if(entry.getKey() instanceof BasicEvent){
                res.put((BasicEvent) entry.getKey(), entry.getValue());
            }
        }
        return res;
    }

    @Override
    public Iterator<Vertex> iterator() {
        return valuation.keySet().iterator();
    }

    @Override
    public String toString() {
        return valuation.toString();
    }

    /** Returns the new Valuation if the ADD has _this_ valuation and be is set to val.
     *
     * @param be the basic event which should be updated to get the next valuation
     * @param val the value the basic event is changed to
     * @return the new valuation
     */
    public Valuation next(@NotNull BasicEvent be, Value val){
        ADDValuationVisitor visitor = new ADDValuationVisitor(this, Collections.singleton(be), v -> val);
        be.accept(visitor, Direction.UP);
        return visitor.getVal();
    }


    /** This is a rather niche function. Make sure this is the right function for your job.
     * Refreshes the current valuation without turning the value of a gate to a set value.
     * The add will be explored in the specified direction.
     *
     * @param vertex the vertex, the exploration should start from
     * @param direction the direction, the ADD should be explored in
     * @return the new valuation
     */
    public Valuation next(@NotNull Vertex vertex, Direction direction) {
        ADDValuationVisitor visitor = new ADDValuationVisitor(this);
        vertex.accept(visitor, direction);
        return visitor.getVal();
    }

    /**
     * Currently unused due to performing worse than Vertex::evaluate
     *
     * @param be the BasicEvent which should turn to the value which is sampled according to its prob.
     * @return the valuation the ADD turned to
     */
    public Valuation attempt(BasicEvent be){
        return next(be, be.attempt());
    }

    /**
     * See attempt, but do it for a sequence of BEs
     * @param bes the BEs which will be attempted in order.
     * @return the Valuation the attempts will end in
     */
    public Valuation attemptSequence(BasicEvent... bes){
        Valuation result = this;
        for(BasicEvent be : bes){
            result = result.next(be, be.attempt());
        }
        return result;
    }

    /**
     *
     * @return all BasicEvents in the ADD which were already attempted.
     */
    public List<BasicEvent> attemptedBasicEvents() {
        return this.getBasicEvents()
                .entrySet().stream()
                .filter(entry -> entry.getValue() != Value.UNDECIDED)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    /**
     *
     * @return The Basic Events which are remaining unattempted in the ADD
     */
    // TODO consider triggers
    public Stream<BasicEventPlayer> remainingBasicEvents() {
        return add.getBasicEventsAsBEs()
                .stream()
                .filter(v -> this.getValue(v) == Value.UNDECIDED);
    }

    public Vertex getRootVertex() {
        return this.add.getTop();
    }

    public Value getRootValue() {
        return getValue(getRootVertex());
    }

    public ADD getADD() {
        return add;
    }
}

