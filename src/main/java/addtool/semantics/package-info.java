/**
 * Classes needed for semantic specification of ADT.
 * @author Julia Eisentraut julia.eisentraut@in.tum.de
 * @since 14.01.2018
 */
package addtool.semantics;