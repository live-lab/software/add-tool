package addtool.semantics;

import addtool.add.model.*;
import org.jscience.mathematics.number.Rational;

import java.util.*;

public class Move implements Iterable<BasicEvent> {

    private final Set<BasicEvent> move;
    private final Map<BasicEvent, Set<Trigger>> triggersForEvent = new HashMap<>();
    private Player player;

    public Move(Set<? extends BasicEvent> move, ADD add, Player player) {
        this.move = Set.copyOf(move);
        this.player = player;

        for (BasicEvent b : move) {
            Set<Trigger> trigger = new HashSet<>();
            for (Vertex v : add) {
                if (v instanceof Trigger) {
                    if (((Trigger) v).getToTrigger().contains(b)) {
                        trigger.add((Trigger) v);
                    }
                }
            }
            triggersForEvent.put(b, trigger);
        }
    }

    public Set<Trigger> getTriggersForEvent(BasicEvent event) {
        return triggersForEvent.get(event);
    }

    public Set<BasicEvent> getMove() {
        return Set.copyOf(move);
    }
    public boolean isEmpty(){
        return move.isEmpty();
    }

    public boolean contains(Object o) {
        if(o instanceof BasicEvent){
            return move.contains((BasicEvent) o);
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Move move1 = (Move) o;

        if (move == null) {
            return move1.getMove() == null;
        }

        for (BasicEvent b : this.getMove()) {
            if (!move1.getMove().contains(b)) {
                return false;
            }
        }

        for (BasicEvent b : move1.getMove()) {
            if (!this.getMove().contains(b)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        return move != null ? move.hashCode() : 0;
    }

    @Override
    public Iterator<BasicEvent> iterator() {
        return move.iterator();
    }

    @Override
    public String toString() {
        return move.toString();
    }

    public String getName() {
        StringBuilder builder = new StringBuilder();
        builder.append('{');
        int i = 0;
        for (BasicEvent b : this) {
            builder.append(b.getName());
            if (i != move.size()) {
                builder.append(',');
            }
        }
        builder.append('}');
        return builder.toString();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getCost() {
        Rational cost = Rational.ZERO;
        for (BasicEvent b : move) {
            cost = cost.plus(b.getCost());
        }
        return cost.getDividend().intValue();
    }
}
