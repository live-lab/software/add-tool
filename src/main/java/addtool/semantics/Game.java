package addtool.semantics;

import addtool.add.model.ADD;
import addtool.add.model.Player;
import addtool.analysisonadd.Direction;
import addtool.analysisongame.CostVisitor;
import addtool.analysisongame.GameVisitor;
import addtool.analysisongame.ProbabilityVisitor;
import addtool.analysisonadd.Visited;
import org.jscience.mathematics.number.Rational;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A class representing game semantics.
 * @author julia
 * @version 14/1/18
 * @since 14/1/18
 */
public class Game implements Visited<GameVisitor>, Iterable<State> {
    // the ADD, which induces this game
    private final ADD original;
    // Map used instead of Set, in order to find exact objects in it.
    // If you create new Objects which are equal to one already in a set (contains returns true)
    // they are practically immutable, which may be undesired.
    private final Map<State, State> states = new HashMap<>();
    private final State initial;

    public Game(ADD add) {
        this.original = add;
        this.initial = new State(new Valuation(add), Player.Attacker);
        this.states.put(initial, initial);
    }

    public Rational calculateRootCost(){
        return new CostVisitor(this).getRootCost();
    }

    public Rational calculateRootProbability(){
        return new ProbabilityVisitor(this).getRootProb();
    }

    public int getSize() {
        return states.keySet().size();
    }

    public State initialState() {
        return initial;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        if (!Objects.equals(original, game.original)) return false;
        if (!states.equals(game.states)) return false;
        return Objects.equals(initial, game.initial);
    }

    @Override
    public int hashCode() {
        int result = original != null ? original.hashCode() : 0;
        result = 31 * result + states.hashCode();
        result = 31 * result + (initial != null ? initial.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return '(' + states.keySet().stream().map(State::toString).collect(Collectors.joining("\n")) +
                ", " + initial + ')';
    }

    @Override
    public void accept(GameVisitor visitor) {
        initial.accept(visitor);
    }

    @Override
    public void accept(GameVisitor visitor, Direction direction) {
        if(direction == Direction.DOWN){
            initial.accept(visitor);
        }
        else {
            throw new UnsupportedOperationException("");
        }
    }

    @Override
    public Iterator<State> iterator() {
        return states.keySet().iterator();
    }

    /**
     * Generates a new state if it is not already in the game, otherwise returns the found state.
     *
     * @param valuation the valuation of the state to generate
     * @param player the player of the state to generate
     * @return the newly generated or found state
     */
    private State generateState(Valuation valuation, Player player) {
        State state = new State(valuation, player);
        State alreadyIn = states.get(state);
        if (alreadyIn != null) {
            return alreadyIn;
        }
        else {
            this.states.put(state, state);
            return state;
        }
    }

    /**
     * Generates a distribution over the successor states of a state for a given move.
     *
     * @param state the state whose successors should be generated
     * @param move the move for which the successor
     * @return the distribution over the states
     */
    public Distribution<State> createSuccessorStates(State state, Move move) {
        Distribution<Valuation> nextValuations = state.getValuation().nextValuation(move);
        Distribution<State> ret = new Distribution<>();
        for(Valuation val : nextValuations){
            Player player = Player.next(state.player);
            State newState = generateState(val, player);
            ret.add(nextValuations.lookUp(val), newState);
        }
        return ret;
    }
}