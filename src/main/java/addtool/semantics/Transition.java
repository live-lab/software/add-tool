package addtool.semantics;

import java.util.Objects;

public class Transition {

    private Move move;
    private State start;
    private Distribution<State> end;

    public Transition(Move move, State start, Distribution<State> end) {
        this.move = move;
        this.start = start;
        this.end = end;
    }

    public Move getMove() {
        return move;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    public State getStart() {
        return start;
    }

    public void setStart(State start) {
        this.start = start;
    }

    public Distribution<State> getEnd() {
        return end;
    }

    public void setEnd(Distribution<State> end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transition that = (Transition) o;

        if (!Objects.equals(move, that.move)) return false;
        if (!Objects.equals(start, that.start)) return false;
        return Objects.equals(end, that.end);
    }

    @Override
    public int hashCode() {
        int result = move != null ? move.hashCode() : 0;
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "(" + start + ", " + move + ", " + end + ')';
    }
}
