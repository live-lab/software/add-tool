package addtool.semantics;

public enum Value {
    TRUE(1), FALSE(-1), UNDECIDED(0);
    private final int id;
    Value(int i){
        id=i;
    }
    public int getInt(){
        return id;
    }
    // general 3VL-Operations
    private static Value of(int i){
        return i > 0 ? TRUE : i < 0 ? FALSE : UNDECIDED;
    }
    public static Value and(Value a, Value b){
        return of(Math.min(a.getInt(), b.getInt()));
    }
    public static Value or(Value a, Value b){
        return of(Math.max(a.getInt(), b.getInt()));
    }
    public static Value not(Value a){
        return of(-a.getInt());
    }
    public static Value nat(Value a){
        return of(-Math.abs(a.getInt())*2+1);
    }
}
