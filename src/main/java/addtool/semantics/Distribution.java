package addtool.semantics;

import org.jscience.mathematics.number.Rational;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * This class implements a discrete probability distribution as a Collection.
 * @param <T> The Objects for the distribution
 * @author julia
 * @version 15/1/18
 * @since 15/1/18
 */
public class Distribution<T> implements Iterable<T> {

    // Distribution only contains nonZero probabilities
    private final HashMap<T, Rational> distribution = new HashMap<>();

    /**
     * Adds an item to the Collection.
     * @param prob The Probability assigned to the object
     * @param t The object for the corresponding probability
     */
    public void add(Rational prob, T t) {
        Collection<Rational> keys = distribution.values();

        if (!prob.isZero()) {
            Rational sum = Rational.ZERO;
            for (Rational d : keys) {
                sum = sum.plus(d);
            }

            if ((sum.plus(prob)).isGreaterThan(Rational.ONE)) {
                throw new IllegalArgumentException("Distribution assigns Probability greater than 1 to its Support!");
            }

            if (distribution.containsKey(t)) {
                Rational oldProb = distribution.get(t);
                distribution.put(t, oldProb.plus(prob));
            } else {
                distribution.put(t, prob);
            }
        }
    }

    /**
     * Returns the probability for a specific item to happen.
     * @param t An item to look the probability up
     * @return The probability assigned to the item
     */
    public Rational lookUp(T t) {
        return distribution.get(t);
    }

    // Returns True if the Distribution keySet is a subset of given parameter states
    public boolean allContained(Set<T> states) {
        for (T state : this) {
            if (!states.contains(state)) {
                return false;
            }
        }
        return true;
    }

    public Set<T> getSupport() {
        return distribution.keySet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Distribution<?> that = (Distribution<?>) o;
        return distribution.equals(that.distribution);
    }

    @Override
    public int hashCode() {
        return distribution.hashCode();
    }

    @Override
    public Iterator<T> iterator() {
        return distribution.keySet().iterator();
    }

    @Override
    public String toString() {
        return "[" + distribution + ']';
    }
}
