package addtool.semantics;

import addtool.add.model.ADD;
import addtool.add.model.BasicEvent;
import addtool.add.model.Player;
import addtool.analysisonadd.Direction;
import addtool.analysisonadd.Visited;
import addtool.analysisongame.GameVisitor;

import java.util.*;

public class State implements Visited<GameVisitor> {

    private final Map<Move, Distribution<State>> successors = new HashMap<>();
    private final Valuation valuation;
    public final Player player;
    private boolean newS = true;

    protected State(Valuation valuation, Player player) {
        this.valuation = valuation;
        this.player = player;
    }

    @Override
    public void accept(GameVisitor visitor) {
        if(!visitor.visited(this)) {
            for (Distribution<State> d : successors.values()) {
                d.getSupport().forEach(s -> s.accept(visitor));
            }
            visitor.visit(this);
        }
    }

    @Override
    public void accept(GameVisitor visitor, Direction direction) {
        if(direction == Direction.DOWN){
            accept(visitor);
        }
        else {
            throw new UnsupportedOperationException("No visiting in up-direction allowed.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        State state = (State) o;

        if(player != state.player) return false;

        if (this.valuation == null) {
            return state.getValuation() == null;
        } else {
            return this.valuation.equals(state.valuation);
        }
    }

    public HashMap<BasicEvent, Value> attemptedBasicEvents(){
        HashMap<BasicEvent, Value> res = new HashMap<>();
        Map<BasicEvent, Value> bes = valuation.getBasicEvents();
        for(Map.Entry<BasicEvent, Value> entry : bes.entrySet()){
            if(entry.getValue() != Value.UNDECIDED){
                res.put(entry.getKey(), entry.getValue());
            }
        }
        return res;
    }

    @Override
    public int hashCode() {
        int result = valuation != null ? valuation.hashCode() : 0;
        result = 31 * result + (player != null ? player.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "(" + valuation + ", " + player + ')';
    }

    public Value rootValue() {
        return this.valuation.getRootValue();
    }


    public void addSuccessor(Move move, Distribution<State> states) {
        successors.put(move, states);
    }

    public Distribution<State> getSuccessor(Move move) {
        return successors.get(move);
    }

    public Valuation getValuation() {
        return valuation;
    }

    public boolean isNew() {
        return this.newS;
    }

    public void setDiscovered(){
        this.newS = false;
    }

    public Set<Move> getAvailableMoves() {
        return successors.keySet();
    }

    public ADD getADD() {
        return valuation.getADD();
    }
}
