package addtool.dot2add;

import java.util.Objects;

/**
 * wrapper for tokens.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 26.02.15
 * @see TokenType
 */
class Token {

    private final String string;
    private final TokenType tokenType;

    public Token(String string, TokenType tokenType) {
        this.string = string;
        this.tokenType = tokenType;
    }

    public String getString() {
        return string;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Token token = (Token) o;

        if (!Objects.equals(string, token.string)) return false;
        return tokenType == token.tokenType;
    }

    @Override
    public int hashCode() {
        int result = string != null ? string.hashCode() : 0;
        result = 31 * result + (tokenType != null ? tokenType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        if (tokenType == TokenType.CONSTANT) {
            return "(" + tokenType + ',' + string + ')';
        } else {
            return tokenType.toString();
        }

    }
}
