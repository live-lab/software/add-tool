package addtool.dot2add;

/**
 * If sink player is invalid.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 26.02.15
 */
public class WrongValueForSinkException extends Exception {

    public WrongValueForSinkException() {
        super("The label \"sink\" must get \"attacker\" or \"defender\" as value!");
    }
}
