package addtool.dot2add;

/**
 * Adapter to allow usage of different graphviz readers
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 08.09.2021
 */
public interface NodeAdapter {
    static NodeAdapter of(Object o){
        /*if(o instanceof com.alexmerz.graphviz.objects.Node){
            return new MerzAdapter((com.alexmerz.graphviz.objects.Node)o);
        }*/

        if(o instanceof guru.nidi.graphviz.model.MutableNode){
            return new NidiAdapter((guru.nidi.graphviz.model.MutableNode)o);
        }
        throw new UnsupportedOperationException("This type of node has no default implementation.");
    }
    String getAttribute(String key);
    String getId();
}
