package addtool.dot2add;

/*class MerzAdapter implements NodeAdapter{
    com.alexmerz.graphviz.objects.Node node;
    MerzAdapter(com.alexmerz.graphviz.objects.Node n){
        node=n;
    }

    @Override
    public String getAttribute(String key) {
        return node.getAttribute(key);
    }

    @Override
    public String getId() {
        return node.getId().getId();
    }
}*/

/**
 * Adapter for new dependency
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 08.09.2021
 * @see guru.nidi.graphviz.model.MutableNode
 */
class NidiAdapter implements NodeAdapter {
    final guru.nidi.graphviz.model.MutableNode node;
    static int id = 1;

    NidiAdapter(guru.nidi.graphviz.model.MutableNode n) {
        node = n;
    }

    @Override
    public String getAttribute(String key) {
        return (String) node.get(key);
    }

    @Override
    public String getId() {
        return node.name().toString();
    }

}
