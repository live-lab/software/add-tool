package addtool.dot2add;

/**
 * To be thrown if wrong label is used in dot file for a vertex.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 26.02.15
 */
public class DifferentLabelExpectedException extends Exception {

    public DifferentLabelExpectedException(String message) {
        super(message);
    }
}
