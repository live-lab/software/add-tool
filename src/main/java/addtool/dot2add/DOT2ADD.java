package addtool.dot2add;

import addtool.add.io.ADDImport;
import addtool.add.model.ADD;
import addtool.helpers.TranslationTable;

import java.io.File;
import java.io.FileReader;

public class DOT2ADD implements ADDImport {
    @Override
    public String getName() {
        return TranslationTable.DOT;
    }

    @Override
    public String getFileExtension() {
        return ".dot";
    }

    @Override
    public ADD importModel(File f) throws WrongValueForLabelOperatorException, NoValueSpecifiedException, WrongStructureADDException, WrongValueForSinkException, java.io.IOException {
        FileReader in = new FileReader(f);
        ADD model = ParserDOT.parseADD(in);
        if(model!=null)model.validateConnections();
        return model;
    }
}
