package addtool.dot2add;

import org.jscience.mathematics.number.Rational;

/**
 * Converts a String to a {@link Rational}.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 9/16/15
 */
public final class RationalFromString {

    private RationalFromString() {
    }

    /**
     * The method getRationalFromString computes a rational number from the
     * given String if the String indeed represents a correct rational number.
     * This version also accounts for , instead of . for decimal and percentages
     *
     * @param s object of type String
     * @return if the input parameter s represents a correct rational number, the rational number will
     * be returned as object of type Rational
     * @throws NumberFormatException if s does not represent a correct rational number
     */
    public static Rational getRationalFromStringLocale(String s) {
        if(s.contains(",")&&!s.contains(".")){
            s=s.replaceAll(",",".");
        }
        if(s.endsWith("%")){
            Rational r=getRationalFromString(s.substring(0,s.length()-1));
            return r.divide(Rational.valueOf(100,1));
        }else{
            return getRationalFromString(s);
        }
    }
    /**
     * The method getRationalFromString computes a rational number from the
     * given String if the String indeed represents a correct rational number.
     *
     * @param s object of type String
     * @return if the input parameter s represents a correct rational number, the rational number will
     * be returned as object of type Rational
     * @throws NumberFormatException if s does not represent a correct rational number
     */
    public static Rational getRationalFromString(String s) {
        Rational number = Rational.ZERO.copy();
        boolean negative = false;

        if (s == null || s.isEmpty()) {
            return number;
        }

        // check whether number is negative
        if (s.charAt(0) == '-') {
            negative = true;
            s = s.substring(1);
        }

        //parse part before "."
        while (!s.isEmpty() && s.charAt(0) != '.') {
            if (Character.isDigit(s.charAt(0))) {
                number = number.times(10);
                number = number.plus(Rational.ONE.times(Character.digit(s.charAt(0), 10)));
            } else {
                throw new NumberFormatException("Not a double " + s + '!');
            }
            s = s.substring(1);
        }

        //check whether there are numbers after the "."
        if (!s.isEmpty()) {
            s = s.substring(1);
        }

        //parse part after "."
        Rational dividend = Rational.ONE.divide(Rational.ONE.times(10));
        while (!s.isEmpty() && s.charAt(0) != 'E') {
            if (Character.isDigit(s.charAt(0))) {
                number = number.plus(dividend.times(Character.digit(s.charAt(0), 10)));
            } else {
                throw new NumberFormatException("Not a double " + s + '!');
            }
            s = s.substring(1);
            dividend = dividend.divide(Rational.ONE.times(10));
        }
        if(s.startsWith("E")){
            //Handle exponent
            int exp=Integer.parseInt(s.substring(1));
            Rational mult=Rational.ONE;
            Rational ten=Rational.ONE.times(10);
            if(exp<0){
                for(int i=exp;i<0;i++){
                    mult=mult.divide(ten);
                }
            }else{
                for(int i=0;i<exp;i++){
                    mult=mult.times(ten);
                }
            }
            number=number.times(mult);
        }

        //change sign if number is negative
        if (negative) {
            number = number.times(-1);
        }

        return number;
    }
}
