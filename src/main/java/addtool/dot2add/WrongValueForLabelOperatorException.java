package addtool.dot2add;

/**
 * If there is a mismatch between label and value.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 26.02.15
 */
public class WrongValueForLabelOperatorException extends Exception {

    public WrongValueForLabelOperatorException(String message) {
        super(message);
    }
}
