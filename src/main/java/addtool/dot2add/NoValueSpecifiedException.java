package addtool.dot2add;

/**
 * To be thrown if a required item is missing in the vertex data.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 26.02.15
 */
public class NoValueSpecifiedException extends Exception {

    public NoValueSpecifiedException(String message) {
        super(message);
    }

}
