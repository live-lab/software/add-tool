package addtool.dot2add;

import addtool.add.model.*;
import addtool.add.operators.Comparison;
import addtool.helpers.String2Values;
import addtool.helpers.TranslationTable;
import org.jscience.mathematics.number.Rational;
import addtool.timeseries.PACTuple;

import java.util.HashMap;

/**
 * In this class, all objects of type NodeAdapter are always part of the universal graph structure read from the
 * dot-file and parsed by the library. In addition, all methods in this class aim at translating NodeAdapters of type NodeAdapter
 * to vertices of the attack-defence graphs (internal representation).
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 9/16/15
 */
public final class ParseVertices {


    private ParseVertices() {
    }

    /**
     * parse a NodeAdapter representing a BE to a State of type BE.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type BE
     * @throws NoValueSpecifiedException if data about cost or probability is missing
     * @throws WrongValueForLabelOperatorException if the probability is not inside [0,1]
     */
    static Vertex parseBE(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) throws NoValueSpecifiedException, WrongValueForLabelOperatorException {

        // get information from automatically parsed NodeAdapter
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String distribution = NodeAdapter.getAttribute(TranslationTable.DISTRIBUTION);
        String probability = NodeAdapter.getAttribute(TranslationTable.PROBABILITY);
        String uncertainty = NodeAdapter.getAttribute(TranslationTable.UNCERTAINTY);
        String probabilitydelta = NodeAdapter.getAttribute(TranslationTable.PROBABILITYDELTA);
        String costExe = NodeAdapter.getAttribute(TranslationTable.COST_EXECUTION);
        String costuncertainty = NodeAdapter.getAttribute(TranslationTable.COST_UNCERTAINTY);
        String costprobabilitydelta = NodeAdapter.getAttribute(TranslationTable.COST_PROBABILITYDELTA);
        String delayExe = NodeAdapter.getAttribute(TranslationTable.DELAY_EXECUTION);
        String delayuncertainty = NodeAdapter.getAttribute(TranslationTable.DELAY_UNCERTAINTY);
        String delayprobabilitydelta = NodeAdapter.getAttribute(TranslationTable.DELAY_PROBABILITYDELTA);
        // String costD = NodeAdapter.getAttribute(TranslationTable.COST_DELAY);
        String id = NodeAdapter.getId();

        // check that all necessary labels have a value
        if (costExe == null || probability == null || distribution == null) {
            throw new NoValueSpecifiedException("No value for a mandatory label for the BE" + NodeAdapter + " has been defined!");
        }


        Vertex v;

        // compute probability
        Rational prob = RationalFromString.getRationalFromString(probability);
        Rational unc = RationalFromString.getRationalFromString(uncertainty);
        Rational delta = RationalFromString.getRationalFromString(probabilitydelta);

        // check whether probability is in interval [0,1]
        if (!((prob.compareTo(Rational.ZERO) >= 0) || (prob.compareTo(Rational.ONE) <= 0))) {
            throw new WrongValueForLabelOperatorException("Probability for basic event " + NodeAdapter
                    + " is not in the interval [0,1]");
        }

        // compute cost vectors (for multidimensional costs use parseCosts)
        Rational costsExe = String2Values.parseCost(costExe);
        Rational costsuncert = String2Values.parseCost(costuncertainty);
        Rational costsdelta = String2Values.parseCost(costprobabilitydelta);


        Rational delaysExe = String2Values.parseCost(delayExe);
        Rational delaysuncert = String2Values.parseCost(delayuncertainty);
        Rational delaysdelta = String2Values.parseCost(delayprobabilitydelta);
        // Rational[] costsD = String2Values.parseCosts(costD);

        // generate correct event depending on label distribution
        if (TranslationTable.ATTACKER.contains(distribution)) {
            //(String name, PACTuple successProbability, PACTuple cost, PACTuple delay, String id, Player player){
            v = new BasicEventPlayer(name, new PACTuple(prob,unc,delta),new PACTuple(costsExe,costsuncert,costsdelta),
                    new PACTuple(delaysExe,delaysuncert,delaysdelta), id, Player.Attacker);
        } else if (TranslationTable.DEFENDER.contains(distribution)) {
            v = new BasicEventPlayer(name, new PACTuple(prob,unc,delta),new PACTuple(costsExe,costsuncert,costsdelta),
                    new PACTuple(delaysExe,delaysuncert,delaysdelta), id, Player.Defender);
        } else {
            v = new BasicEventTime(name, new PACTuple(prob,unc,delta),new PACTuple(costsExe,costsuncert,costsdelta),
                    new PACTuple(delaysExe,delaysuncert,delaysdelta), id, distribution);
        }

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator Not to a State of type Not.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type HConstraint
     */
    static Vertex parseNot(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) {
        // the only special about not is the name and the id
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String id = NodeAdapter.getId();

        Vertex v = new Not(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator NotTrue to a State of type NotTrue.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type HConstraint
     */
    static Vertex parseNotTrue(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) {
        // the only special about not is the name and the id
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String id = NodeAdapter.getId();

        Vertex v = new NotTrue(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }


    /**
     * parse a NodeAdapter representing the operator Nat to a State of type Nat.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type HConstraint
     */
    static Vertex parseNat(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) {
        // the only special about not is the name and the id
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String id = NodeAdapter.getId();

        Vertex v = new Nat(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator Not to a State of type Not.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type HConstraint
     */
    static Vertex parseIvr(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) {
        // the only special about ivr is the name and the id
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String id = NodeAdapter.getId();

        Vertex v = new Ivr(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the Countermeasure of a connection.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type HConstraint
     */
    static Vertex parseCm(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) {
        // the only special about ivr is the name and the id
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String id = NodeAdapter.getId();

        Vertex v = new Countermeasure(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator cost to a State of type Cost.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type Cost
     * @throws NoValueSpecifiedException if bounds or costs are missing
     */
    static Vertex parseCost(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) throws NoValueSpecifiedException {
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String id = NodeAdapter.getId();
        String bound = NodeAdapter.getAttribute(TranslationTable.BOUND);
        String operatorCost = NodeAdapter.getAttribute(TranslationTable.COMPARISON);

        // check consistency of label values
        if (bound == null || operatorCost == null) {
            throw new NoValueSpecifiedException("For the State " + NodeAdapter + " labelled with <Cost> exists a mandatory label without value!");
        }

        Comparison[] c = String2Values.getComparison(operatorCost);
        Rational[] b = String2Values.parseCosts(bound);

        Vertex v = new Cost(name, b, c, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator trigger to a State of type Trigger.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type Trigger
     */
    static Vertex parseTrigger(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) {
        String name = NodeAdapter.getAttribute("name");
        String id = NodeAdapter.getId();

        Vertex v = new Trigger(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator reset to a State of type Reset.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type Reset
     */
    static Vertex parseReset(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) {
        String name = NodeAdapter.getAttribute("name");
        String id = NodeAdapter.getId();

        Vertex v = new Reset(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator seq to a State of type SAnd.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type SAnd
     * @throws NoValueSpecifiedException if the order is missing
     */
    static Vertex parseSAnd(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) throws NoValueSpecifiedException {
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String order = NodeAdapter.getAttribute(TranslationTable.ORDER);
        String id = NodeAdapter.getId();

        if (order == null) {
            throw new NoValueSpecifiedException("For the State " + NodeAdapter + " labelled with <SAnd> exists a mandatory label without value!");
        }

        Vertex v = new SAnd(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator sor to a State of type SOr.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type SOr
     * @throws NoValueSpecifiedException If order is missing
     */
    static Vertex parseSOr(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) throws NoValueSpecifiedException {
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String order = NodeAdapter.getAttribute(TranslationTable.ORDER);
        String id = NodeAdapter.getId();

        if (order == null) {
            throw new NoValueSpecifiedException("For the State " + NodeAdapter + " labelled with <SOr> exists a mandatory label without value!");
        }

        Vertex v = new SOr(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator sor to a State of type SOr.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type If
     * @throws NoValueSpecifiedException if does not contain the first/second data
     */
    static Vertex parseIf(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) throws NoValueSpecifiedException {
        String name = NodeAdapter.getAttribute(TranslationTable.NAME);
        String first = NodeAdapter.getAttribute(TranslationTable.FIRST);
        String second = NodeAdapter.getAttribute(TranslationTable.SECOND);
        String id = NodeAdapter.getId();

        if (first == null || second == null) {
            throw new NoValueSpecifiedException("For the State " + NodeAdapter + " labelled with <If> exists a mandatory label without value!");
        }

        Vertex v = new If(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator and to a State of type And.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type And
     */
    static Vertex parseAnd(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) {
        String name = NodeAdapter.getAttribute("name");
        String id = NodeAdapter.getId();

        Vertex v = new And(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(NodeAdapter.getId(), v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

    /**
     * parse a NodeAdapter representing the operator or to a State of type Or.
     *
     * @param id2Vertex   a map, which maps ids from the universal graph model to respective vertices of the internal representation of adgs
     * @param name2Vertex a map, which maps names from NodeAdapters from of the universal graph model to respective vertices of the internal representation of adgs
     * @param vertex2NodeAdapter a map, which maps vertices of the internal adg representation to the original NodeAdapters from the universal graph structure
     * @param NodeAdapter        an object of type NodeAdapter from the universal graph structure
     * @return an object of type Or
     */
    static Vertex parseOr(HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2NodeAdapter, NodeAdapter NodeAdapter) {
        String name = NodeAdapter.getAttribute("name");
        String id = NodeAdapter.getId();

        Vertex v = new Or(name, id);

        if (name != null) {
            name2Vertex.put(name, v);
        }

        id2Vertex.put(id, v);
        vertex2NodeAdapter.put(id, NodeAdapter);
        return v;
    }

}
