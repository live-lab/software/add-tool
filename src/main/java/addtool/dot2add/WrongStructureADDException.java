package addtool.dot2add;

/**
 * To be thrown if the structure is invalid.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 26.02.15
 */
public class WrongStructureADDException extends Exception {

    public WrongStructureADDException(String message) {
        super(message);
    }
}
