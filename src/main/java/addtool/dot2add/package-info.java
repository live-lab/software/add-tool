/**
 * Manages ADD import from Dot-format.
 * See ParserDOT.parseADD
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 16.09.15
 */
package addtool.dot2add;