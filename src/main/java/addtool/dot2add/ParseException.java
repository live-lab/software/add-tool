package addtool.dot2add;

/**
 * To be thrown if Dot-Parsing goes wrong.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 26.02.15
 */
public class ParseException extends Exception {

    public ParseException() {
        super("HCConstraint has wrong syntax!");
    }

}
