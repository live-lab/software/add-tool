package addtool.dot2add;


import addtool.add.model.*;
import guru.nidi.graphviz.model.*;
import guru.nidi.graphviz.parse.Parser;
import addtool.helpers.Constants;
import addtool.helpers.ExceptionHandler;
import addtool.helpers.TranslationTable;
import addtool.helpers.Values2String;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.*;

/**
 * A class to convert dot-files to add.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 16.09.15
 * @see ADD
 */
public final class ParserDOT {

    private ParserDOT() {
    }

    /**
     * This method first uses the library to parse the Dot-File into a generic graph structure and will then parse the given
     * graph structure into an attack-defence graph.
     *
     * @return an Attack-Defence Graph if the given file indeed specifies a correct graph
     * @throws WrongValueForLabelOperatorException there exists a State in the dot-file, which has an invalid (e.g. unknown) label
     * @throws NoValueSpecifiedException           there exists a necessary label of an operator without value
     * @throws WrongStructureADDException          the adg is not acyclic, reset or transition edges are not specified correctly, IDs or names are not unique
     * @throws WrongValueForSinkException          the adg sink is neither of the attacker nor the defender
     */
    public static ADD parseADD(FileReader in) throws WrongValueForLabelOperatorException, NoValueSpecifiedException, WrongStructureADDException, WrongValueForSinkException, IOException {
        // a mapping form ids to vertices of the adg
        HashMap<String, Vertex> id2Vertex = new HashMap<>();

        // a mapping from names (mainly BEs) to vertices of the adg
        HashMap<String, Vertex> name2Vertex = new HashMap<>();

        // a mapping from vertices of adgs (internal representation) to nodes of the universal graph structure of the library
        HashMap<String, NodeAdapter> vertex2Node = new HashMap<>();

        // the parser object reading the dot-file (library object)
        Parser parser = new Parser();

        // a mapping from vertices to predecessors
        HashMap<String, ArrayList<Vertex>> vertex2Predecessor = new HashMap<>();

        // a mapping from vertices to successors
        HashMap<String, ArrayList<Vertex>> vertex2Successor = new HashMap<>();
        //Convert Reader to string buffer but by ignoring the comments
        BufferedReader br=new BufferedReader(in);
        String ls=System.getProperty("line.separator");
        StringBuilder buff=new StringBuilder();
        List<String> comment=new ArrayList<>();
        try {
            for(String line=br.readLine();line!=null;line=br.readLine()){
                if(!line.startsWith(Constants.COMMENT)){
                    buff.append(line).append(ls);
                }else{
                    comment.add(line.substring(Constants.COMMENT.length()));
                }
            }
        } catch (IOException e) {
            ExceptionHandler.logException(e);
        }
        // existing .dot-parser to get underlying graph structure
        MutableGraph g=parser.read(buff.toString());

        //get all existing vertices in the graph

        ArrayList<MutableGraph> gl = new ArrayList<>(g.graphs());
        ArrayList<MutableNode> nodes = new ArrayList<>(g.nodes());


        //parse vertices to the correct adg.model.ADD structure
        Set<Vertex> vertices = parseVertices(nodes, id2Vertex, name2Vertex, vertex2Node);

        //establish double-link structure between vertices
        ArrayList<Link> edges = new ArrayList<>(g.edges());

        parseEdges(vertices, edges, vertex2Predecessor, vertex2Successor, id2Vertex, vertex2Node);

        //check whether adg.model.ADD is correct
        Set<Vertex> noSuccessor = checkADG(vertices, vertex2Successor, id2Vertex, vertex2Node, vertex2Predecessor);
        ADD ret=new ADD(id2Vertex, noSuccessor, true);
        ret.setComments(comment);
        return ret;
    }

    /**
     * parses vertices of the specified adg (by label).
     *
     * @param nodes       list of nodes from the universal graph structure provided by the library to parse dot-files
     * @param id2Vertex     An empty map to be filled with id and vertex by the parsing efforts
     * @param name2Vertex   An empty map later containing vertex names as keys and the objects as values
     * @param vertex2Node   A mapping between Dot nodes and newly created vertices
     * @return the set of vertices of the adg
     * @throws NoValueSpecifiedException           there exists a necessary label of an operator without value
     * @throws WrongValueForLabelOperatorException there exists a State in the dot-file, which has an invalid (e.g. unknown) label
     */
    private static Set<Vertex> parseVertices(ArrayList<MutableNode> nodes, HashMap<String, Vertex> id2Vertex, HashMap<String, Vertex> name2Vertex, HashMap<String, NodeAdapter> vertex2Node) throws NoValueSpecifiedException, WrongValueForLabelOperatorException {
        Set<Vertex> vertices = new HashSet<>();
        for (MutableNode mnode : nodes) {
            NodeAdapter node=NodeAdapter.of(mnode);
            String gate = node.getAttribute(TranslationTable.GATE);
            Vertex v;
            if (TranslationTable.BASIC_EVENT.contains(gate)) {
                v=(ParseVertices.parseBE(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.AND.contains(gate)) {
                v=(ParseVertices.parseAnd(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.OR.contains(gate)) {
                v=(ParseVertices.parseOr(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.SAND.contains(gate)) {
                v=(ParseVertices.parseSAnd(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.SOR.contains(gate)) {
                v=(ParseVertices.parseSOr(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.IF.contains(gate)) {
                v=(ParseVertices.parseIf(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.COST.contains(gate)) {
                v=(ParseVertices.parseCost(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.IVR.contains(gate)) {
                v=(ParseVertices.parseIvr(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.CM.contains(gate)) {
                v=(ParseVertices.parseCm(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.NOT.contains(gate)) {
                v=(ParseVertices.parseNot(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.NOT_TRUE.contains(gate)) {
                v=(ParseVertices.parseNotTrue(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.NAT.contains(gate)) {
                v=(ParseVertices.parseNat(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.TRIGGER.contains(gate)) {
                v=(ParseVertices.parseTrigger(id2Vertex, name2Vertex, vertex2Node, node));
            } else if (TranslationTable.RESET.contains(gate)) {
                v=(ParseVertices.parseReset(id2Vertex, name2Vertex, vertex2Node, node));
            } else {
                throw new WrongValueForLabelOperatorException(gate + " not known for " + node + '!');
            }
            String cbr=node.getAttribute(TranslationTable.CANBEROOT);
            if(cbr!=null){
                v.setCanBeRoot(cbr.equals("1"));
            }
            String comment=node.getAttribute(TranslationTable.COMMENT);
            if(comment!=null){
                v.setName(comment);
            }
            String xpos=node.getAttribute(TranslationTable.XPOS);
            String ypos=node.getAttribute(TranslationTable.YPOS);
            if(xpos!=null&&ypos!=null){
                v.setLocation(
                        new Point(
                                Values2String.id2Int(xpos,0),
                                Values2String.id2Int(ypos,0)));
            }
            vertices.add(v);
        }

        return vertices;
    }

    /**
     * parses edges of the adg.
     *
     * @param vertices    set of all parsed vertices
     * @param edges       list of edges of the universal graph structure (library)
     * @param id2Vertex     A map with the unique id as key and the corresponding vertex as value
     * @param vertex2Node   A map of vertices to their dot-nodes
     * @throws WrongStructureADDException          the adg is not acyclic, reset or transition edges are not specified correctly, IDs or names are not unique
     * @throws WrongValueForLabelOperatorException there exists a State in the dot-file, which has an invalid (e.g. unknown) label
     */
    private static void parseEdges(Set<Vertex> vertices, ArrayList<Link> edges, HashMap<String, ArrayList<Vertex>> vertex2Predecessor, HashMap<String, ArrayList<Vertex>> vertex2Successor, HashMap<String, Vertex> id2Vertex, HashMap<String, NodeAdapter> vertex2Node) throws WrongStructureADDException, WrongValueForLabelOperatorException {

        //look at each edge whether it is a normal edge, a trigger edge or a reset edge
        for (Link e : edges) {

            Vertex start = id2Vertex.get(e.from().name().toString());
            Vertex end = id2Vertex.get(e.to().name().toString());

            String type = (String)e.get(TranslationTable.TYPE);

            if (type != null) {
                if (TranslationTable.TRIGGER.contains(type)) {
                    ((Trigger) start).addToTrigger((BasicEvent) end);
                    ((BasicEvent) end).setTrigger_state(TriggerState.TRIGGER_ABLE);
                    continue;
                } else if (TranslationTable.RESET.contains(type)) {
                    ((Reset) start).addToReset((BasicEvent) end);
                    ((BasicEvent) end).setResettable(true);
                    continue;
                } else {
                    throw new WrongValueForLabelOperatorException("Value of label <type> may only be <trigger> or <reset>!");
                }
            }

            //only executed if "special == null (continue or exception in all cases of switch-statement)
            if (vertex2Successor.containsKey(start.getId())) {
                ArrayList<Vertex> succ = vertex2Successor.get(start.getId());
                succ.add(end);
            } else {
                ArrayList<Vertex> successor = new ArrayList<>();
                successor.add(end);
                vertex2Successor.put(start.getId(), successor);
            }

            if (vertex2Predecessor.containsKey(end.getId())) {
                ArrayList<Vertex> pred = vertex2Predecessor.get(end.getId());
                pred.add(start);
            } else {
                ArrayList<Vertex> predecessor = new ArrayList<>();
                predecessor.add(start);
                vertex2Predecessor.put(end.getId(), predecessor);
            }
        }

        //establish double-linked structure of predecessors and successors and check consistency
        for (Vertex v : vertices) {
            if (!vertex2Predecessor.containsKey(v.getId())) {
                if (!(v instanceof BasicEvent)) {
                    throw new WrongStructureADDException("Operator " + vertex2Node.get(v.getId()) + " has no predecessors!");
                }
            } else if (v instanceof And || v instanceof Or) {
                ArrayList<Vertex> predecessor = vertex2Predecessor.get(v.getId());

                if (predecessor.size() < 2) {
                    throw new WrongStructureADDException("Binary operator " + vertex2Node.get(v.getId()) + " only " + predecessor.size() + " predecessor (" + predecessor + ")!");
                }

                ((NaryOp) v).addPredecessors(predecessor);
            } else if (v instanceof SAnd || v instanceof SOr) {
                ArrayList<Vertex> predecessor = vertex2Predecessor.get(v.getId());
                ArrayList<Vertex> copy = new ArrayList<>(predecessor);
                NodeAdapter node = vertex2Node.get(v.getId());
                String[] order = ParserDOT.parseOrder(node.getAttribute(TranslationTable.ORDER));

                // SAnd or SOr with just one predecessor does not work
                if (predecessor.size() < 2) {
                    throw new WrongStructureADDException("Binary operator " + vertex2Node.get(v.getId()) + " only " + predecessor.size() + " predecessor (" + predecessor + ")!");
                }

                //add all predecessors in the right order
                for (String orderString:order) {
                    Vertex pred = id2Vertex.get(orderString);
                    if (!predecessor.contains(pred)) {
                        System.out.println(predecessor);
                        System.out.println(pred);
                        System.out.println(node.getAttribute(TranslationTable.ORDER));
                        throw new WrongStructureADDException("Real predecessors and specified order do not match!");
                    }
                    copy.remove(pred);
                    v.addPredecessor(pred);
                }

                if (!copy.isEmpty()) {
                    throw new WrongStructureADDException("Real predecessors and specified order do not match!");
                }

            } else if (v instanceof If) {
                ArrayList<Vertex> predecessor = vertex2Predecessor.get(v.getId());
                NodeAdapter node = vertex2Node.get(v.getId());
                String first = node.getAttribute(TranslationTable.FIRST);
                String second = node.getAttribute(TranslationTable.SECOND);

                Vertex firstInput = id2Vertex.get(first);
                Vertex secondInput = id2Vertex.get(second);

                if (predecessor.size() != 2 || !predecessor.contains(firstInput) || !predecessor.contains(secondInput)) {
                    throw new WrongStructureADDException("Binary operator " + vertex2Node.get(v.getId()) + " has wrong number of predecessors!");
                }

                v.addPredecessor(firstInput);
                v.addPredecessor(secondInput);
            } else if (v instanceof UnaryOp) {
                ArrayList<Vertex> predecessor = vertex2Predecessor.get(v.getId());

                if (predecessor.size() != 1) {
                    throw new WrongStructureADDException("Unary operator " + vertex2Node.get(v.getId()) + " has wrong number of predecessors!");
                }

                v.addPredecessor(predecessor.get(0));
            }
        }
    }

    /**
     * check consistency of adg, for example whether the underlying graph is cycle free, check sinks.
     *
     * @param vertices           set of all vertices of the adg
     * @param vertex2Successor  A map containing all successors of a vertex
     * @param id2Vertex         A map with unique id as key and the vertex as value
     * @param vertex2Node       A map of vertices to their dot-nodes
     * @param vertex2Predecessor A map containing all predecessors of a vertex
     * @throws WrongValueForSinkException if the sink is neither owned by the attacker nor the defender
     */
    private static Set<Vertex> checkADG(Set<Vertex> vertices, HashMap<String, ArrayList<Vertex>> vertex2Successor, HashMap<String, Vertex> id2Vertex, HashMap<String, NodeAdapter> vertex2Node, HashMap<String, ArrayList<Vertex>> vertex2Predecessor) throws WrongValueForSinkException {
        //only triggerable basic events are triggered
        vertices.stream().filter(vertex -> vertex instanceof Trigger).forEach(vertex -> {
            for (BasicEvent b : ((Trigger) vertex).getToTrigger()) {
                b.setTrigger_state(TriggerState.TRIGGER_ABLE);
            }
        });

        //only resettable basic events are reset
        vertices.stream().filter(vertex -> vertex instanceof Reset).forEach(vertex -> {
            for (BasicEvent b : ((Reset) vertex).getToReset()) {
                b.setResettable(true);
            }
        });


        //check whether the underlying graph is cycle-free
        Set<Vertex> noSuccessor = new HashSet<>(vertices);

        Set<Vertex> allVertices = new HashSet<>(vertices);

        //delete all vertices, which have a successor from "NoSuccessor"
        for (String s : vertex2Successor.keySet()) {
            noSuccessor.remove(id2Vertex.get(s));
        }

        //Removed Cycle Check for more complex models
        /*
        for (Vertex vertex : noSuccessor) {
            checkNoCycle(vertex, new ArrayList<>(), allVertices, vertex2Predecessor);
        }*/

        //check for each sink, whether there exists the "sink" label
        for (Vertex v : noSuccessor) {
            NodeAdapter n = vertex2Node.get(v.getId());
            String s = n.getAttribute(TranslationTable.SINK);
            if (s != null) {
                if (TranslationTable.ATTACKER.contains(s)) {
                    v.setSinkAttacker(true);
                } else if (TranslationTable.DEFENDER.contains(s)) {
                    v.setSinkDefender(true);
                } else {
                    throw new WrongValueForSinkException();
                }
            }
        }

        /*if (!allVertices.isEmpty()) {
            throw new WrongStructureADDException("ADG is not acyclic!");
        }*/
        return noSuccessor;
    }

    private static void checkNoCycle(Vertex vertex, List<Vertex> seenSoFar, Set<Vertex> allVertices, HashMap<String, ArrayList<Vertex>> vertex2Predecessor) throws WrongStructureADDException {
        allVertices.remove(vertex);
        if (vertex2Predecessor.containsKey(vertex.getId())) {
            for (Vertex predecessor : vertex2Predecessor.get(vertex.getId())) {
                if (seenSoFar.contains(predecessor)) {
                    throw new WrongStructureADDException("The ADG is not acyclic!");
                } else {
                    ArrayList<Vertex> copy = new ArrayList<>(seenSoFar);
                    copy.add(vertex);
                    checkNoCycle(predecessor, copy, allVertices, vertex2Predecessor);
                }
            }
        }
    }

    private static String[] parseOrder(String s) {
        return s.split(",");
    }
}
