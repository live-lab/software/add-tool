package addtool.dot2add;

/**
 * Tokens to represent represent input stream of logical formulas..
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 26.02.15
 */
public enum TokenType {
    AND, OR, EQUIV, IMPL, NEG, FALSE, TRUE, CONSTANT, BRACKETL, BRACKETR
}
