package addtool.add2dot;

import addtool.add.io.ADDExport;
import addtool.add.model.ADD;
import addtool.helpers.Constants;
import addtool.helpers.TranslationTable;
import addtool.helpers.preferences.PreferenceManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * This class is used to start saving ADDs as DOT-Files (easy visualization, saving format).
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 17.10.15
 * @since 17.10.15
 */
public final class ADD2Dot implements ADDExport {

    public ADD2Dot() {
    }

    public static void writeADD2DotFile(PrintWriter writer, ADD adg) {
        if (writer == null || adg == null) {
            throw new IllegalArgumentException("ADD and File cannot be null!");
        }
        boolean authorWritten=false;
        List<String> comments=adg.getComments();
        for(String s:comments){
            if(s.contains("@author")){
                authorWritten=true;
            }
            writer.println(Constants.COMMENT+s);
        }
        //ADD comment with data
        if(!authorWritten){
            String author= PreferenceManager.getPreferenceManager().getPreference("author").getValueString();
            if(author!=null) {
                writer.println(Constants.COMMENT + "@author: " + author);
            }
        }


        writer.println("digraph {"); // begin of graph

        //write vertices to file
        DotCodeVertices.writeVertices2DotFile(writer, adg);
        writer.println();
        writer.println();

        //write edges to file
        DotCodeEdges.writeEdges2DotFile(writer, adg);

        writer.println("}"); // closing bracket of graph
        writer.close();
    }

    @Override
    public String getName() {
        return TranslationTable.DOT;
    }

    @Override
    public String getFileExtension() {
        return ".dot";
    }

    @Override
    public void exportModel(ADD model, File f) throws IOException {
        PrintWriter writer=new PrintWriter(f, StandardCharsets.UTF_8);
        ADD2Dot.writeADD2DotFile(writer, model);
    }
}
