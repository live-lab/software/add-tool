package addtool.add2dot;

import addtool.add.model.ADD;
import addtool.add.model.Reset;
import addtool.add.model.Trigger;
import addtool.add.model.Vertex;
import addtool.helpers.TranslationTable;

import java.io.PrintWriter;

/**
 * translation of edges to DOT-files.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 9/18/15
 * @since 9/18/15
 */
final class DotCodeEdges {

    private DotCodeEdges() {
    }

    /**
     * Outputs all edges of a model to the dot-file.
     * @param writer writer to file
     * @param add    ADD to translate
     */
    public static void writeEdges2DotFile(PrintWriter writer, ADD add) {
        for (Vertex v : add) {

            // for each State we write the transition to its successor to the DOT-file
            DotCodeEdges.writeNormalEdge2DotFile(writer, v);

            // for Resets and Trigger, also the trigger and reset edges are written to the file
            if (v instanceof Reset) {
                DotCodeEdges.writeResetEdge2DotFile(writer, (Reset) v);
            }
            if (v instanceof Trigger) {
                DotCodeEdges.writeTriggerEdge2DotFile(writer, (Trigger) v);
            }
        }
    }

    private static void writeNormalEdge2DotFile(PrintWriter writer, Vertex v) {
        /*if(v instanceof INaryOp){
            INaryOp inp=(INaryOp)v;
            for (Vertex predecessor : inp.getPredecessors()) {
                writer.println(predecessor.getId() + "->" + inp.getId() + ";");
            }
        }else{*/
            for (Vertex successor : v.getSuccessors()) {
                writer.println(String.format("%s->%s;",v.getId(),successor.getId()));
            }
       // }
    }

    private static void writeResetEdge2DotFile(PrintWriter writer, Reset reset) {
        for (Vertex res : reset.getToReset()) {
            String format="%s->%s[%s=\"reset\",%s=blue,%s=dotted]";
            writer.println(String.format(format,reset.getId(),res.getId(),TranslationTable.TYPE,TranslationTable.COLOR,TranslationTable.STYLE));
            //writer.println(reset.getId() + "->" + res.getId() + "[" + TranslationTable.TYPE + "=\"reset\"," + TranslationTable.COLOR + "=blue," + TranslationTable.STYLE + "=dotted]");
        }
        /*if(reset instanceof INaryOp){
            INaryOp inp=(INaryOp)reset;
            for (Vertex predecessor : inp.getPredecessors()) {
                writer.println(predecessor.getId() + "->" + inp.getId() + ";");
            }
        }*/
    }

    private static void writeTriggerEdge2DotFile(PrintWriter writer, Trigger trigger) {
        for (Vertex trig : trigger.getToTrigger()) {
            String format="%s->%s[%s=\"trigger\",%s=red,%s=dotted]";
            writer.println(String.format(format,trigger.getId(),trig.getId(),TranslationTable.TYPE,TranslationTable.COLOR,TranslationTable.STYLE));
            //writer.println(trigger.getId() + "->" + trig.getId() + "[" + TranslationTable.TYPE + "=\"trigger\"," + TranslationTable.COLOR + "=red," + TranslationTable.STYLE + "=dotted]");
        }
        /*if(trigger instanceof INaryOp){
            INaryOp inp=(INaryOp)trigger;
            for (Vertex predecessor : inp.getPredecessors()) {
                writer.println(predecessor.getId() + "->" + inp.getId() + ";");
            }
        }*/
    }

}
