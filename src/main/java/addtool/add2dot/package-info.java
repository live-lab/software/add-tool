/**
 * This package formats ADDs in dot-format
 * see ADD2Dot.writeADD2DotFile
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 17.10.15
 */
package addtool.add2dot;