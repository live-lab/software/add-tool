package addtool.add2dot;

import addtool.add.model.*;
import addtool.helpers.TranslationTable;
import addtool.helpers.Values2String;
import addtool.timeseries.PACTuple;

import java.io.PrintWriter;

/**
 * translation of vertices to DOT-files.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 9/18/15
 * @since 9/18/15
 */
final class DotCodeVertices {

    private DotCodeVertices() {
    }

    /**
     * Writes all vertices to a dot file.
     * @param writer writer to file
     * @param add    ADD to translate
     */
    public static void writeVertices2DotFile(PrintWriter writer, ADD add) {
        for (Vertex v : add) {

            //write id of vertex to file
            writer.print(v.getId() + ' ');
            // write vertex-dependent code to file
            switch (v.getClass().getSimpleName()) {
                case "And", "Or", "Ivr", "Countermeasure", "Not", "NotTrue", "Nat", "Reset", "Trigger" -> writer.print(buildBasicOperator(v.getClass().getSimpleName().toUpperCase(), v));
                case "SAnd" -> DotCodeVertices.writeSand2DotFile(writer, (SAnd) v);
                case "SOr" -> DotCodeVertices.writeSor2DotFile(writer, (SOr) v);
                case "Cost" -> DotCodeVertices.writeCost2DotFile(writer, (Cost) v);
                case "If" -> DotCodeVertices.writeIf2DotFile(writer, (If) v);
                case "BasicEventPlayer" -> DotCodeVertices.writeBEPlayer2DotFile(writer, (BasicEventPlayer) v);
                case "BasicEventTime" -> DotCodeVertices.writeBETime2DotFile(writer, (BasicEventTime) v);
            }
            /*if (v instanceof And) {
                DotCodeVertices.writeAnd2DotFile(writer, (And) v);
            }
            if (v instanceof Or) {
                DotCodeVertices.writeOrTime2DotFile(writer, (Or) v);
            }
            if (v instanceof SAnd) {
                DotCodeVertices.writeSand2DotFile(writer, (SAnd) v);
            }
            if (v instanceof SOr) {
                DotCodeVertices.writeSor2DotFile(writer, (SOr) v);
            }
            if (v instanceof Trigger) {
                DotCodeVertices.writeTrigger2DotFile(writer, (Trigger) v);
            }
            if (v instanceof Reset) {
                DotCodeVertices.writeReset2DotFile(writer, (Reset) v);
            }
            if (v instanceof Cost) {
                DotCodeVertices.writeCost2DotFile(writer, (Cost) v);
            }
            if (v instanceof Not) {
                DotCodeVertices.writeNot2DotFile(writer, (Not) v);
            }
            if (v instanceof Nat) {
                DotCodeVertices.writeNat2DotFile(writer, (Nat) v);
            }
            if (v instanceof Ivr) {
                DotCodeVertices.writeIvr2DotFile(writer, (Ivr) v);
            }
            if (v instanceof Countermeasure) {
                DotCodeVertices.writeCM2DotFile(writer, (Countermeasure) v);
            }
            if (v instanceof If) {
                DotCodeVertices.writeIf2DotFile(writer, (If) v);
            }
            if (v instanceof BasicEventPlayer) {
                DotCodeVertices.writeBEPlayer2DotFile(writer, (BasicEventPlayer) v);
            }
            if (v instanceof BasicEventTime) {
                DotCodeVertices.writeBETime2DotFile(writer, (BasicEventTime) v);
            }*/

            // end line
            writer.println(";");
        }
    }


    private static void writeBETime2DotFile(PrintWriter writer, BasicEventTime be) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.BASIC_EVENT.get(0) + "\","
                + TranslationTable.NAME + "=\"" + be.getName() + "\"," + TranslationTable.DISTRIBUTION + "=\"" + be.getDistribution()+ "\","
               /* + "\"," + TranslationTable.COST_EXECUTION + "=\"" + Values2String.string2Costs(be.getCost()) + "\","
                // + TranslationTable.COST_DELAY + "=\"" + "\","
                + TranslationTable.PROBABILITY + "=\"" + be.getSuccessProbability().doubleValue() + "\","
                + TranslationTable.UNCERTAINTY + "=\"" + be.getUncertainty().doubleValue() + "\","
                + TranslationTable.PROBABILITYDELTA + "=\"" + be.getProbabilityDelta().doubleValue() + "\""*/
                + getBECommon2DotFile(be)
                + writeSink2DotFile(be) + ']');
    }

    private static void writeBEPlayer2DotFile(PrintWriter writer, BasicEventPlayer be) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.BASIC_EVENT.get(0)
                + "\"," + TranslationTable.NAME + "=\"" + be.getName() + "\"," + TranslationTable.DISTRIBUTION + "=\"" + be.getPlayer()+ "\","
                /*+ "\"," + TranslationTable.COST_EXECUTION + "=\"" + Values2String.string2Costs(be.getCost()) + "\","
                // + TranslationTable.COST_DELAY + "=\"" + "\","
                + TranslationTable.PROBABILITY + "=\"" + be.getSuccessProbability().doubleValue() + "\","
                + TranslationTable.UNCERTAINTY + "=\"" + be.getUncertainty().doubleValue() + "\","
                + TranslationTable.PROBABILITYDELTA + "=\"" + be.getProbabilityDelta().doubleValue() + "\""*/
                + getBECommon2DotFile(be)
                + writeSink2DotFile(be) + ']');
    }
    private static String getBECommon2DotFile(BasicEvent be){
        PACTuple cost=be.getCosts();
        PACTuple delay=be.getDelay();
        return TranslationTable.PROBABILITY + "=\"" + be.getSuccessProbability().doubleValue() + "\","
                + TranslationTable.UNCERTAINTY + "=\"" + be.getUncertainty().doubleValue() + "\","
                + TranslationTable.PROBABILITYDELTA + "=\"" + be.getProbabilityDelta().doubleValue() + "\""
                + TranslationTable.COST_EXECUTION + "=\"" + cost.getValue().doubleValue() + "\","
                + TranslationTable.COST_UNCERTAINTY + "=\"" + cost.getUncertainty().doubleValue() + "\","
                + TranslationTable.COST_PROBABILITYDELTA + "=\"" + cost.getDelta().doubleValue() + "\""
                + TranslationTable.DELAY_EXECUTION + "=\"" + delay.getValue().doubleValue() + "\","
                + TranslationTable.DELAY_UNCERTAINTY + "=\"" + delay.getUncertainty().doubleValue() + "\","
                + TranslationTable.DELAY_PROBABILITYDELTA + "=\"" + delay.getDelta().doubleValue() + "\"";
    }

    private static void writeAnd2DotFile(PrintWriter writer, And and) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.AND.get(0) + "\",label=\""+and.getOperatorName()+ '"' + writeSink2DotFile(and) + ']');
    }

    private static void writeOrTime2DotFile(PrintWriter writer, Or or) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.OR.get(0) + "\",label=\""+or.getOperatorName()+ '"' + writeSink2DotFile(or) + ']');
    }

    private static void writeSand2DotFile(PrintWriter writer, SAnd sand) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.SAND.get(0) + "\",label=\""+sand.getOperatorName()+"\"," + TranslationTable.ORDER + "=\"" + Values2String.getIdList(sand.getPredecessors()) + '"' + writeSink2DotFile(sand) + ']');
    }

    private static void writeSor2DotFile(PrintWriter writer, SOr sor) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.SOR.get(0) + "\",label=\""+sor.getOperatorName()+"\"," + TranslationTable.ORDER + "=\"" + Values2String.getIdList(sor.getPredecessors()) + '"' + writeSink2DotFile(sor) + ']');
    }

    private static void writeIf2DotFile(PrintWriter writer, If guard) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.IF.get(0) + "\",label=\""+guard.getOperatorName()+"\"," + TranslationTable.FIRST + "=\"" + guard.getPredecessor01().getId() + "\"," + TranslationTable.SECOND + "=\"" + guard.getPredecessor02().getId() + '"' + writeSink2DotFile(guard) + ']');
    }

    private static void writeIvr2DotFile(PrintWriter writer, Ivr ivr) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.IVR.get(0) + "\",label=\""+ivr.getOperatorName()+ '"' + writeSink2DotFile(ivr) + ']');
    }

    private static void writeCM2DotFile(PrintWriter writer, Countermeasure cm) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.CM.get(0) + "\",label=\""+cm.getOperatorName()+ '"' + writeSink2DotFile(cm) + ']');
    }

    private static void writeNot2DotFile(PrintWriter writer, Not not) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.NOT.get(0) + "\",label=\""+not.getOperatorName()+ '"' + writeSink2DotFile(not) + ']');
    }
    private static void writeNat2DotFile(PrintWriter writer, Nat nat) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.NAT.get(0) + "\",label=\""+nat.getOperatorName()+ '"' + writeSink2DotFile(nat) + ']');
    }

    private static void writeReset2DotFile(PrintWriter writer, Reset reset) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.RESET.get(0) + "\",label=\""+reset.getOperatorName()+ '"' + writeSink2DotFile(reset) + ']');
    }

    private static void writeTrigger2DotFile(PrintWriter writer, Trigger trigger) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.TRIGGER.get(0) + "\",label=\""+trigger.getOperatorName()+ '"' + writeSink2DotFile(trigger) + ']');
    }

    private static void writeCost2DotFile(PrintWriter writer, Cost cost) {
        writer.print('[' + TranslationTable.GATE + "=\"" + TranslationTable.COST.get(0) + "\",label=\""+cost.getOperatorName()+"\"," + TranslationTable.BOUND + "=\"" + Values2String.string2Costs(cost.getBound()) + "\"," + TranslationTable.COMPARISON + "=\"" + Values2String.string2Comparisons(cost.getOp()) + '"' + writeSink2DotFile(cost) + ']');
    }
    private static String buildBasicOperator(String opcode,Vertex operator){
        String format="[%s=\"%s\",label=\"%s\"%s]";
        return String.format(format,TranslationTable.GATE,opcode,operator.getOperatorName(),writeSink2DotFile(operator));
    }

    private static String writeSink2DotFile(Vertex vertex) {
        String out="";
        if (vertex.isSinkAttacker() && vertex.getSuccessors().isEmpty()) {
            out+= ',' + TranslationTable.SINK + '=' + '"' + TranslationTable.ATTACKER.get(0) + '"';
        } else if (vertex.isSinkDefender() && vertex.getSuccessors().isEmpty()) {
            out+= ',' + TranslationTable.SINK + '=' + '"' + TranslationTable.DEFENDER.get(0) + '"';
        }
        out+= ',' + TranslationTable.CANBEROOT + '=' + '"' + (vertex.canBeRoot()?"1":"0") + '"';
        out+= ',' + TranslationTable.COMMENT + '=' + '"' + vertex.getName() + '"';
        out+= ',' + TranslationTable.XPOS + '=' + '"' + vertex.getLocation().x + '"';
        out+= ',' + TranslationTable.YPOS + '=' + '"' + vertex.getLocation().y + '"';
        return out;
    }

}
