package addtool.add2dot;

import addtool.add.model.ADD;
import addtool.dot2add.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * Script to revalidate connections in older {@link ADD}.
 * Should not be of much use in current days.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since  9.07.2020
 */
public final class MainUpdateDot {
    private MainUpdateDot() {
    }

    public static void main(String[] args) throws NoValueSpecifiedException, WrongStructureADDException, WrongValueForLabelOperatorException, WrongValueForSinkException, IOException {
        if(args.length==0){
            args = new String[]{Path.of("test","ADDs","file-437.dot").toString()};//Export file from XML2Dot
        }
        FileReader in = new FileReader(args[0]);
        ADD add = ParserDOT.parseADD(in);

        add.validateConnections();

        PrintWriter writer = new PrintWriter(args[0], StandardCharsets.UTF_8);
        ADD2Dot.writeADD2DotFile(writer, add);
    }
}
