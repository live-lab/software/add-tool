package addtool.add2dot;

import addtool.add.model.ADD;
import addtool.add.model.BasicEvent;
import addtool.add.model.Vertex;
import addtool.dot2add.*;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Experiment to parametrize an {@link ADD} in operators and the {@link BasicEvent} in their hierarchy.
 * Did not work as hopped. Left in here for further exploration.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 28.07.2020
 */
public final class MainDOT2CSV {
    private MainDOT2CSV() {
    }

    public static void main(String... args) throws NoValueSpecifiedException, WrongStructureADDException, WrongValueForLabelOperatorException, WrongValueForSinkException, IOException {
        String infile= Path.of("test","ADDs","file-436.dot").toString();
        if(args.length>=1){
            infile=args[0];
        }
        String outfile=infile+".csv";
        if(args.length>=2){
            outfile=args[1];
        }
        FileReader in = new FileReader(infile);
        ADD add = ParserDOT.parseADD(in);

        List<Vertex> allBE= new ArrayList<>(add.getBasicEvents());
        List<Vertex> ops=add.getOperators();
        //Build Vectors
        PrintWriter pw =new PrintWriter(outfile);
        pw.println("Operator;Player;"+allBE.stream().map(Vertex::getId).collect(Collectors.joining(";")));
        for(Vertex v : ops){
            List<Boolean> beVector=getEmptyVector(allBE.size());
            List<BasicEvent> bes=v.getDownstreamBEs();
            bes.forEach(b->beVector.set(allBE.indexOf(b),true));

            pw.println(v.getOperatorName()+";1;"+beVector.stream().map(b->b?"1":"0").collect(Collectors.joining(";")));
        }
        pw.close();
    }
    public static List<Boolean> getEmptyVector(int size){
        ArrayList<Boolean> feve=new ArrayList<>();
        for(int i=0;i<size;i++){
            feve.add(false);
        }
        return feve;
    }
}

