package addtool.add.io;

import addtool.add.model.ADD;
import addtool.helpers.Constants;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Adapter interface for export formats of ADD
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @see ADD
 */
public interface ADDExport {
    /**
     * Get method name which can be displayed to humans or used as a key. Should be unique
     * @return the string key
     */
    String getName();
    /**
     * Get File Extension of this method
     * @return extension e.g. ".dot"
     */
    String getFileExtension();

    /**
     * Export the model to a given file
     * @param model an ADD to export
     * @param f the target File
     * @throws Exception Any exception you have to throw during the process
     */
    void exportModel(ADD model, File f) throws Exception;


    /**
     * Get selected ADDExport
     * Will only find classes that have an accessible constructor with no arguments
     * @param checkName Name of the Type {@link ADDExport#getName()}
     * @return an object if there exists a corresponding class, null otherwise
     */
    static ADDExport getMethodByParam(Predicate<ADDExport> checkName){
        Set<Class<? extends ADDExport>> classes = Constants.findAllMatchingTypes("",ADDExport.class);
        return classes.stream().map(c->{try{
                    return c.getConstructor().newInstance();
                } catch (InvocationTargetException |InstantiationException|IllegalAccessException|NoSuchMethodException e) {
                    return null;
                }}).filter(o-> Objects.nonNull(o) && checkName.test(o)).
                //We want the longest at the first position
                        sorted(Comparator.comparingInt(ai -> -ai.getFileExtension().length())).
                findFirst().orElse(null);
    }

    /**
     * Enter the Name and ADDImport Implementation.
     * @param name A name from {@link ADDExport#getName()}
     * @return An instance of a corresponding Object
     */
    static ADDExport getMethodByName(String name){
        return getMethodByParam(a->a.getName().equals(name));
    }
    /**
     * Checks if a string ends with a specific char-sequence specified by the checker objects.
     * @param path A file path or extension
     * @return An instance of a corresponding Object
     * @see ADDExport#getFileExtension()
     */
    static ADDExport getMethodByExtension(String path){
        return getMethodByParam(a->path.endsWith(a.getFileExtension()));
    }

    /**
     * Lists all Classes implementing the Interface in the classpath.
     * Will only find classes that have an accessible constructor with no arguments
     * @see ADDExport#getName()
     * @return An Array of all Class names
     */
    static String[] getOptions(){
        Set<Class<? extends ADDExport>> classes = Constants.findAllMatchingTypes("",ADDExport.class);
        return classes.stream().map(c->{try{
            return c.getConstructor().newInstance().getName();
        } catch (InvocationTargetException|InstantiationException|IllegalAccessException|NoSuchMethodException e) {
            return null;
        }}).filter(Objects::nonNull).toArray(String[]::new);
    }
}
