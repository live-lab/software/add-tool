/**
 * Package to define interfaces for import and export of {@link addtool.add.model.ADD}.
 */
package addtool.add.io;