package addtool.add.model;

@Deprecated
public interface IBasicEventPlayer extends IBasicEvent {

    boolean isAttacker();

    boolean isDefender();

    boolean isPlayer(Player p);
}
