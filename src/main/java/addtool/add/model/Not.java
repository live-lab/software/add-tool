package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;

/**
 * three-valued logic NOT.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class Not extends UnaryOp{

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public Not(String name, String id) {
        super(name,id);
    }

    @Override
    public String getOperatorName() {
        return "Not";
    }

    @Override
    public Rational getDisruptionProb(){
        return Rational.ONE.minus(getPredecessor().getDisruptionProb());
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }

    @Override
    public void evaluate() {
        switch (this.getPredecessor().getValue()) {
            case FALSE -> this.setValue(Value.TRUE);
            case TRUE -> this.setValue(Value.FALSE);
            case UNDECIDED -> this.setValue(Value.UNDECIDED);
        }
        if(this.getValue()!=Value.UNDECIDED)
            super.evaluate();
    }

    @Override
    public Vertex copy() {
        Not an=new Not(this.getName(),this.getId());
        this.writeValuesTo(an);
        return an;
    }
}