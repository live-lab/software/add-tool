package addtool.add.model;

import addtool.analysisonadd.CostHeuristic;
import addtool.analysisonadd.DelayHeuristic;
import addtool.analysisonadd.PrintType;
import addtool.analysisonadd.ProbabilityPACHeuristic;
import addtool.dot2add.WrongStructureADDException;
import addtool.feedback.FeedbackTuple;
import addtool.helpers.TranslationTable;
import addtool.helpers.Values2String;
import addtool.timeseries.PACTuple;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Consider this class being something like the StringBuilder.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 13.07.2020
 */
public class ADDBuilder {
    /**
     * General method to print vertex property.
     * @param v Vertex to print
     * @param out PrintStream to print to
     * @param name The Property name
     * @param depth The current depth (determines the amount of tabs)
     * @param cost A PACTuple to print out
     * @param printType Data if the call ist recursive or pac should be printed
     */
    private static void printModelProp(Vertex v, PrintStream out,String name, int depth,
                                       Map<String,PACTuple> cost,PrintType printType){
        out.print("\t".repeat(depth));
        PACTuple pt=cost.get(v.getId());
        String os=String.format("%s %s: %s %s %s: %f",v.getId(),getResource("operator_short"),
                v.getOperatorName(),getResource("has"),name,pt.getValue().doubleValue());
        if(printType==PrintType.PAC||printType==PrintType.PAC_RECURRENT){
            os+=(String.format(", ε: %f, ẟ: %f",pt.getUncertainty().doubleValue(),pt.getDelta().doubleValue()));
        }
        out.println(os);
        if(printType==PrintType.RECURRENT||printType==PrintType.PAC_RECURRENT){
            v.getPredecessors().forEach(k->printModelProp(k,out,name,depth+1,cost,printType));
        }
    }
    /**
     * Converts the print-functions that take a PrintStream to a String.
     * @param model The model to print
     * @param fkt the function with signature ADD,PrintStream
     * @return A String after printing
     * @see ADDBuilder#printModel(ADD)
     * @see ADDBuilder#printModel(ADD, PrintStream)
     * @see ADDBuilder#printModelCost(ADD)
     * @see ADDBuilder#printModelCost(ADD, PrintStream)
     * @see ADDBuilder#printModelDelay(ADD)
     * @see ADDBuilder#printModelDelay(ADD, PrintStream)
     */
    public static String convertStreamToString(ADD model, BiConsumer<ADD,PrintStream> fkt) {
        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        String utf8 = StandardCharsets.UTF_8.name();
        try (PrintStream ps = new PrintStream(bas, true, utf8)) {
            fkt.accept(model, ps);
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
            System.err.println("Encoding "+utf8+" is not supported");
        }
        String da = null;
        try {
            da = bas.toString(utf8);
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
            System.err.println("Encoding "+utf8+" is not supported");
        }
        return da;
    }

    /**
     * Prints model probabilities to PrintStream, recursive Method.
     *
     * @param model The ADD to print
     * @param out   PrintStream the values will be printed to
     * @param printType  {@link PrintType}
     */
    public static void printModel(ADD model, PrintStream out, PrintType printType) {
        Map<BasicEvent, PACTuple> map = model.getBasicEvents().stream()
                .map(v -> (BasicEvent) v)
                .collect(Collectors.toMap(v -> v, BasicEvent::getSuccessProbabilityTuple));
        ProbabilityPACHeuristic ch = new ProbabilityPACHeuristic(map);
        model.accept(ch);
        model.getNoSuccessor().forEach(
                v -> printModelProp(v, out, getResource("probability"), 0, ch.getProbbyId(), printType)
        );
    }
    @Deprecated
    public static void printModelOld(Vertex v, PrintStream out, int depth,boolean pac,boolean recur) {
        out.print("\t".repeat(depth));
        String os = String.format("%s %s: %s %s %s: %f", v.getId(),getResource("operator_short"),
                v.getOperatorName(),getResource("has"),
                getResource("probability"),v.getDisruptionProb().doubleValue());
        if (pac) {
            os += (String.format(", ε: %f, ẟ: %f", v.getUncertainty().doubleValue(),
                    v.getProbabilityDelta().doubleValue()));
        }
        out.println(os);
        if (recur) {
            v.getPredecessors().forEach(k -> printModelOld(k, out, depth + 1, pac, true));
        }
    }
    @Deprecated
    public static void printModelOld(ADD model, PrintStream out,boolean pac,boolean recur){
        model.getNoSuccessor().forEach(v->printModelOld(v,out,0,pac,recur));
    }

        /**
         * Adds an amount to every ID in a diagram.
         *
         * @param mod     Diagram to change
         * @param shiftBy integer to shift ID by
         * @return the new diagram
         */
    public static ADD shiftIds(ADD mod, int shiftBy) {
        Map<String, Vertex> originalMap = mod.getId2Vertex();
        Map<String, Vertex> newMap = new HashMap<>();
        originalMap.values().forEach(v -> {
            Vertex v2=v.copy();
            //v2.setID(Values2String.id2Int(v2.getId())+shiftBy+"");
            newMap.put(v2.getId(),v2);
        });
        //Rewrite all connections to copied values
        Function<Vertex,Vertex> mapConnected=(p)->{
            if(newMap.containsKey(p.getId())){
                return newMap.get(p.getId());
            }else{
                Vertex pn=p.copy();
                newMap.put(pn.getId(),pn);
                return pn;
            }
        };

        originalMap.values().forEach(v -> {
            Vertex newVertex=newMap.get(v.getId());
            v.getPredecessors().stream().map(mapConnected).forEach(newVertex::addPredecessor);
            v.getSuccessors().stream().map(mapConnected).forEach(newVertex::addSuccessor);
        });
        Set<Vertex> noSuc=mod.getNoSuccessor().stream().map(v->newMap.get(v.getId())).collect(Collectors.toSet());
        ADD result;
        if(shiftBy!=0){
            //shift ids
            HashMap<String, Vertex> map3 = new HashMap<>();
            newMap.forEach((k, v) -> {
                String nid=String.valueOf(Values2String.id2Int(v.getId()) + shiftBy) ;
                v.setID(nid);
                map3.put(v.getId(), v);
            });
            result=new ADD(map3,noSuc,true);
        }else{
            result=new ADD(newMap,noSuc,true);
        }
        result.setComments(mod.getComments());
        return result;
    }
    /**
     * Deep-copies an ADD using the {@link ADDBuilder#shiftIds(ADD, int) shiftIds} method with shiftBy=0.
     * @param mod The model to copy
     * @return The copied ADD
     */
    public static ADD copyADD(ADD mod){
        if(mod==null){
            return null;
        }
        return shiftIds(mod,0);
    }
    /**
     * Factory Method for Operators (excluding Basic Events).
     * @param Key The Operator Key. It has to comply with the Values in the {@link TranslationTable TranslationTable}
     * @return a new Operator
     */
    public static Vertex buildOperatorForKey(String Key){
        return buildOperatorForKey(Key,"99999","999999");
    }
    /**
     * Factory Method for Operators (excluding Basic Events).
     * @param Key The Operator Key. It has to comply with the Values in the {@link TranslationTable TranslationTable}
     * @param Id A string for the ID
     * @param name A name for the BE
     * @return a new Operator
     */
    public static Vertex buildOperatorForKey(String Key, String Id, String name){
        Vertex out;
        if(TranslationTable.AND.contains(Key)){
            out=new And(name,Id);
        }else if(TranslationTable.OR.contains(Key)){
            out=new Or(name,Id);
        }else if(TranslationTable.SAND.contains(Key)){
            out=new SAnd(name,Id);
        }else if(TranslationTable.SOR.contains(Key)){
            out=new SOr(name,Id);
        }else if(TranslationTable.IVR.contains(Key)){
            out=new Ivr(name,Id);
        }else if(TranslationTable.NAT.contains(Key)){
            out=new Nat(name,Id);
        }else if(TranslationTable.NOT.contains(Key)){
            out=new Not(name,Id);
        }else if(TranslationTable.NOT_TRUE.contains(Key)){
            out=new NotTrue(name,Id);
        }else if(TranslationTable.RESET.contains(Key)){
            out=new Reset(name,Id);
        }else if(TranslationTable.TRIGGER.contains(Key)){
            out=new Trigger(name,Id);
        }else if(TranslationTable.BASIC_EVENT.contains(Key)){
            throw new UnsupportedOperationException("BasicEvents are not built here");
        }else if(TranslationTable.BASIC_EVENT_ATTACKER.contains(Key)){
            out=new BasicEventPlayer(name,PACTuple.SURE_TUPLE,PACTuple.SURE_TUPLE,PACTuple.SURE_TUPLE,Id,Player.Attacker);
        }else if(TranslationTable.BASIC_EVENT_DEFENDER.contains(Key)){
            out=new BasicEventPlayer(name,PACTuple.SURE_TUPLE,PACTuple.SURE_TUPLE,PACTuple.SURE_TUPLE,Id,Player.Defender);
        }else if(TranslationTable.BASIC_EVENT_TIME.contains(Key)){
            out=new BasicEventTime(name,PACTuple.SURE_TUPLE,PACTuple.SURE_TUPLE,PACTuple.SURE_TUPLE,Id,"normal");
        }else{
            throw new UnsupportedOperationException("Not implemented yet.");
        }
        return out;
    }
    /**
     * Constructs an ADD by just giving it all Vertices to integrate
     * Includes an internal call to {@link ADD#validateConnections() ADD.validateconnections}
     * @param vertices List of Vertices to build diagram of
     * @return an ADD containing these Vertices
     */
    public static ADD of(Vertex... vertices){
        Map<String,Vertex> id2vertex=Arrays.stream(vertices).collect(Collectors.toMap(Vertex::getId,v->v));
        Set<Vertex> noSuccessors=id2vertex.values().stream()
                .filter(v->v.getSuccessors().isEmpty())
                .collect(Collectors.toSet());
        ADD out=new ADD(id2vertex,noSuccessors,true);
        out.validateConnections();
        return out;
    }
    /**
     * Prints model probabilities to String.
     * Includes recursive call and PAC-values
     * @param model The model to print
     * @return A String with the probability data
     * @see ADDBuilder#convertStreamToString(ADD, BiConsumer)
     * @see ADDBuilder#printModel(ADD, PrintStream)
     */
    public static String printModel(ADD model){
        return convertStreamToString(model,ADDBuilder::printModel);
    }
    /**
     * Prints model probabilities to PrintStream.
     * Includes recursive call and PAC-values
     * @param model The model to print
     * @param out PrintStream the values will be printed to
     * @see ADDBuilder#printModel(ADD, PrintStream, PrintType)
     */
    public static void printModel(ADD model, PrintStream out){
        printModel(model,out,PrintType.PAC_RECURRENT);
    }
    /**
     * Prints model costs to String.
     * Includes recursive call and PAC-values
     * @param model The model to print
     * @return A String with the probability data
     * @see ADDBuilder#convertStreamToString(ADD, BiConsumer)
     * @see ADDBuilder#printModelCost(ADD, PrintStream)
     */
    public static String printModelCost(ADD model){
        return convertStreamToString(model,ADDBuilder::printModelCost);
    }
    /**
     * Prints model cost to PrintStream.
     * Includes recursive call and PAC-values
     * @param model The model to print
     * @param out PrintStream the values will be printed to
     * @see ADDBuilder#printModelCost(ADD, PrintStream, PrintType)
     */
    public static void printModelCost(ADD model, PrintStream out){
        printModelCost(model,out,PrintType.PAC_RECURRENT);
    }
    /**
     * Prints model costs to a PrintStream.
     * @param model The model to print
     * @param out PrintStream the values will be printed to
     * @param printType {@link PrintType}
     * @see ADDBuilder#printModelProp(Vertex, PrintStream, String, int, Map, PrintType)
     */
    public static void printModelCost(ADD model, PrintStream out,PrintType printType){
        Map<BasicEvent, PACTuple> map= model.getBasicEvents().stream()
                .map(v->(BasicEvent)v)
                .collect(Collectors.toMap(v->v, BasicEvent::getCosts));
        CostHeuristic ch=new CostHeuristic(map);
        model.accept(ch);
        model.getNoSuccessor().forEach(v->printModelProp(v,out,getResource("cost"),0,ch.getCostById(),printType));
    }
    /**
     * Prints model delay to String.
     * Includes recursive call and PAC-values
     * @param model The model to print
     * @return A String with the probability data
     * @see ADDBuilder#convertStreamToString(ADD, BiConsumer)
     * @see ADDBuilder#printModelDelay(ADD, PrintStream)
     */
    public static String printModelDelay(ADD model){
        return convertStreamToString(model,ADDBuilder::printModelDelay);
    }
    /**
     * Prints model delay to PrintStream.
     * Includes recursive call and PAC-values
     * @param model The model to print
     * @param out PrintStream the values will be printed to
     * @see ADDBuilder#printModelDelay(ADD, PrintStream, PrintType)
     */
    public static void printModelDelay(ADD model, PrintStream out){
        printModelDelay(model,out,PrintType.PAC_RECURRENT);
    }
    /**
     * Prints model delay to a PrintStream.
     * @param model The model to print
     * @param out PrintStream the values will be printed to
     * @param printType {@link PrintType}
     * @see ADDBuilder#printModelProp(Vertex, PrintStream, String, int, Map, PrintType)
     */
    public static void printModelDelay(ADD model, PrintStream out,PrintType printType){
        Map<BasicEvent, PACTuple> map= model.getBasicEvents().stream()
                .map(v->(BasicEvent)v)
                .collect(Collectors.toMap(v->v, BasicEvent::getDelay));
        DelayHeuristic ch=new DelayHeuristic(map);
        model.accept(ch);
        model.getNoSuccessor().forEach(v->printModelProp(v,out,getResource("delay"),0,ch.getDelayById(),printType));
    }
    public static boolean canCrossOver(ADDBuilder builder1,ADDBuilder builder2){
        return !checkCrossOvers(builder1,builder2).isEmpty();
    }
    private static List<Vertex[]> checkCrossOvers(ADDBuilder builder1,ADDBuilder builder2){
        if(builder1==null||builder2==null)return new ArrayList<>();
        Map<Vertex, List<BasicEvent>> signature1=builder1.getSignatures();
        Map<Vertex, List<BasicEvent>> signature2=builder2.getSignatures();
        List<Vertex[]> results=new ArrayList<>();
        for(Map.Entry<Vertex,List<BasicEvent>> e1:signature1.entrySet()){
            for(Map.Entry<Vertex,List<BasicEvent>> e2:signature2.entrySet()){
                if(e1.getValue().size()==e2.getValue().size()){
                    boolean match=true;
                    for(int i=0;i<e1.getValue().size();i++){
                        if(!e1.getValue().get(i).getId().equals(e2.getValue().get(i).getId())){
                            //Possible as the lists are ordered by ID
                            match=false;
                            break;
                        }
                    }
                    /*
                     * The Check for operator class is needed to avoid Unary stacking.
                     * As unary-operators have the same signature as nary-operators below them
                     */
                    if(match&&
                            ((e1.getKey() instanceof NaryOp&&e2.getKey() instanceof NaryOp)||
                            (e1.getKey() instanceof UnaryOp&&e2.getKey() instanceof UnaryOp))

                    ){
                        results.add(new Vertex[]{e1.getKey(),e2.getKey()});
                    }
                }
            }
        }
        return results;
    }
    public static boolean crossOver(ADDBuilder builder1,ADDBuilder builder2){
        return crossOver(builder1,builder2,new Random());
    }
    public static boolean crossOver(ADDBuilder builder1,ADDBuilder builder2, Random rand){
        if(builder1==null||builder2==null)return false;
        List<Vertex[]> results=checkCrossOvers(builder1,builder2);

        if(results.isEmpty())return false;

        //Now cut the trees out
        Vertex[] toUse=results.get(rand.nextInt(results.size()));

        builder1.cutSubtree(toUse[0]);
        builder2.cutSubtree(toUse[1]);

        List<Vertex> successors1 = toUse[0].getSuccessors();
        List<Vertex> successors2 = toUse[1].getSuccessors();

        successors1.forEach(s->{
            toUse[0].removeSuccessor(s);
            toUse[1].addSuccessor(s);
        });

        successors2.forEach(s->{
            toUse[1].removeSuccessor(s);
            toUse[0].addSuccessor(s);
        });

        builder1.addSubtree(toUse[1]);
        builder2.addSubtree(toUse[0]);

        return true;
    }

    /**
     * The model to perform operations on.
     * @see ADD
     */
    //private ADD model;

    private final Map<String, Vertex> id2Vertex;
    private Collection<FeedbackTuple> logger;

    /**
     * Initialize the Builder with a model.
     * @param model the model (will not be copied)
     */
    public ADDBuilder(ADD model){
        id2Vertex=ADDBuilder.copyADD(model).getId2Vertex();
        logger=new ArrayList<>();
    }
    private Map<String, Vertex> getId2Vertex(){
        return new HashMap<>(id2Vertex);
    }
    private Set<Vertex> getNoSuccessor(){
        return id2Vertex.values().stream().filter(v->v.getSuccessors().isEmpty()).collect(Collectors.toSet());
    }
    private int getMaxID(){
        return id2Vertex.values().stream().mapToInt(v->{
                try{
                    return Integer.parseInt(v.getId());
                }catch (NumberFormatException ex){
                    return -1;
                }
            }).max().orElse(-1);
    }
    private String getFreeID(){
        return getFreeID(new Random());
    }
    private String getFreeID(Random rand){
        return getFreeID(id2Vertex,rand);
    }
    public static String getFreeID(Map<String,Vertex> id2Vertex,Random rand){
        String id;
        do{
            id=String.valueOf(rand.nextInt(10000));
        }while(id2Vertex.containsKey(id));
        return id;
    }

    public void setLogger(Collection<FeedbackTuple> logger) {
        this.logger=new ArrayList<>(logger);
    }
    public Collection<FeedbackTuple> getLogger() {
        return new ArrayList<>(logger);
    }

    /**
     * Notifies all Successors of a Vertex about a change o p2.
     * Now replaced by {@link Vertex#replacePredecessor(Vertex, Vertex)}
     * @param p1 The Vertex to remove from its successors
     * @param p2 The Vertex to register at the successors
     */
    @Deprecated
    private static void notifySuccessors(Vertex p1, Vertex p2){
        p1.getSuccessors().forEach(v->{
            if(v instanceof NaryOp){
                NaryOp nary=(NaryOp)v;
                List<Vertex> predecessors=nary.getPredecessors();
                predecessors.remove(p1);
                predecessors.add(p2);
                nary.clearPredecessors();
                nary.addPredecessors(predecessors);
            }else if(v instanceof UnaryOp){
                UnaryOp unary=(UnaryOp)v;
                unary.setPredecessor(p2);
            }
        });
    }
    /**
     * Maps List of vertices to current copy of original model.
     * @param list A list of vertices from the original model
     * @return the vertices in the current {@link ADDBuilder#id2Vertex}
     */
    private List<Vertex> mapToCurrent(Collection<Vertex> list){
        return list.stream().map(v->id2Vertex.get(v.getId())).collect(Collectors.toList());
    }
    /**
     * Maps vertex to current copy of original model.
     * @param v A vertex from the original model
     * @return the vertices in the current {@link ADDBuilder#id2Vertex}
     */
    private Vertex mapToCurrent(Vertex v){
        return id2Vertex.get(v.getId());
    }
    private void cutSubtree(Vertex v){
        id2Vertex.remove(v.getId());
        v.getPredecessors().forEach(this::cutSubtree);
    }
    private void addSubtree(Vertex v){
        id2Vertex.put(v.getId(),v);
        v.getPredecessors().forEach(this::addSubtree);
    }
    /**
     * Joins two diagrams on arbitrary position using an {@link UnaryOp Unary-Operator}.
     * @param joiner Operator to join parts
     * @param model2 Second Diagram to join with
     * @param predecessor Predecessor Vertex
     * @param successor Successor vertex
     * @param one2two if true the successor is from model 2; if false vice versa
     * @return ADDBuilder with new Object
     */
    public ADDBuilder join(UnaryOp joiner,ADD model2,Vertex predecessor,Vertex successor,boolean one2two) {
        if(joiner==null||predecessor==null||successor==null){
            throw new IllegalArgumentException("Joiner, Predecessor, Successor can't be NULL");
        }
        //rebuild IDs
        int maxID=getMaxID();//to make sure zero indexed trees don't break the system
        ADD model2_s=shiftIds(model2,maxID);
        //Convert vertices to new from model2
        Vertex currentSuccessor;
        Vertex currentPredecessor;
        if(one2two){
            currentSuccessor= Stream.of(successor).map(v->(String.valueOf(Values2String.id2Int(v.getId()) + maxID)))
                    .map(model2_s::getVertex2Id)
                    .collect(Collectors.toList()).get(0);
            currentPredecessor=mapToCurrent(predecessor);
        }else{
            currentPredecessor= Stream.of(predecessor).map(v->(String.valueOf(Values2String.id2Int(v.getId()) + maxID)))
                    .map(model2_s::getVertex2Id)
                    .collect(Collectors.toList()).get(0);
            currentSuccessor=mapToCurrent(successor);
        }

        //Put entries
        id2Vertex.putAll(model2_s.getId2Vertex());

        //Connect joiner
        joiner.addPredecessor(currentPredecessor);
        joiner.addSuccessor(currentSuccessor);

        id2Vertex.put(joiner.getId(),joiner);

        return this;
    }

    /**
     * Joins models with specific {@link NaryOp Nary-Operator}.
     * @param joiner Operator to join parts
     * @param model2 Second Diagram to join with
     * @param predecessor_m1 List of predecessors from Diagram one
     * @param predecessor_m2 List of predecessors from Diagram two
     * @param suc_m1 List of successors from Diagram one
     * @param suc_m2 List of successors from Diagram two
     * @return ADDBuilder with joined model
     * @throws WrongStructureADDException If predecessor-lists are empty
     */
    public ADDBuilder join(NaryOp joiner,ADD model2,List<Vertex> predecessor_m1,List<Vertex> predecessor_m2,
                           List<Vertex> suc_m1,List<Vertex> suc_m2)throws WrongStructureADDException {
        if(predecessor_m1.isEmpty()||predecessor_m2.isEmpty()){
            throw new WrongStructureADDException("Predecessors can not be empty!");
        }
        if(joiner==null){
            throw new IllegalArgumentException("Joiner can't be NULL");
        }
        //rebuild IDs
        int maxID=getMaxID()+1;//to make sure zero indexed trees don't break the system
        ADD model2_s=shiftIds(model2,maxID);
        //Convert vertices to new from model2
        List<Vertex> predecessors_m2=predecessor_m2.stream()
                .map(v->String.valueOf(Values2String.id2Int(v.getId()) + maxID ))
                .map(model2_s::getVertex2Id)
                .collect(Collectors.toList());
        List<Vertex> successors_m2=suc_m2.stream()
                .map(v->String.valueOf(Values2String.id2Int(v.getId()) + maxID ))
                .map(model2_s::getVertex2Id)
                .collect(Collectors.toList());


        //Connect joiner

        List<Vertex> predecessors= Stream.of(mapToCurrent(predecessor_m1),predecessors_m2)
                .flatMap(List::stream)
                .collect(Collectors.toList());
        joiner.addPredecessors(predecessors);
        List<Vertex> successors= Stream.of(mapToCurrent(suc_m1),successors_m2)
                .flatMap(List::stream)
                .collect(Collectors.toList());
        joiner.addSuccessors(successors);

        id2Vertex.put(joiner.getId(),joiner);
        //Put entries
        id2Vertex.putAll(model2_s.getId2Vertex());

        return this;
    }
    /**
     * Joins models root with model2s root.
     * @throws WrongStructureADDException if one of both Diagram has more than one root
     * @param model2 The second diagram [which will be deep copied and shifted]
     * @param joiner_key Joining OperatorKey like And, Or
     * @return ADDBuilder containing the new diagram
     */
    public ADDBuilder simpleJoin(String joiner_key,ADD model2)throws WrongStructureADDException {
        Vertex joiner= buildOperatorForKey(joiner_key);
        if(! (joiner instanceof NaryOp)){
            throw new UnsupportedOperationException("Joiner must be NaryOp");
        }
        return simpleJoin((NaryOp)joiner,model2);
    }
    /**
     * Joins models root with model2s root.
     * @throws WrongStructureADDException if one of both diagram has more than one root
     * @param model2 The second diagram [which will be Deep copied and shifted]
     * @param joiner Joining Operator
     * @return ADDBuilder containing the new diagram
     */
    public ADDBuilder simpleJoin(NaryOp joiner,ADD model2)throws WrongStructureADDException {
        if(getNoSuccessor().size()!=1||model2.getNoSuccessor().size()!=1){
            throw new WrongStructureADDException("Simple Join just works with Diagrams having ONE Endpoint. " +
                    "Try different Join-methods");
        }
        if(joiner==null){
            throw new IllegalArgumentException("Joiner can't be NULL");
        }
        //rebuild IDs
        int maxID=getMaxID()+1;//to make sure zero indexed trees don't break the system
        //ADD model2_org=model2;//only for debugging
        model2=shiftIds(model2,maxID);


        //Connect joiner
        Vertex node1=getNoSuccessor().iterator().next();
        node1.addSuccessor(joiner);
        Vertex node2=model2.getNoSuccessor().iterator().next();
        node2.addSuccessor(joiner);

        //Put entries
        id2Vertex.putAll(model2.getId2Vertex());

        maxID=model2.getMaxID();
        joiner.setID(String.valueOf(maxID+1));
        id2Vertex.put(joiner.getId(),joiner);
        ADD.validateConnections(id2Vertex);
        //model=new ADD(id2Vertex,Set.of(joiner),true);
        //Build complete Map
        return this;
    }

    /**
     * Adds a {@link UnaryOp UnaryOp} on top of the root.
     * @throws WrongStructureADDException if the diagram has more than one root
     * @param joiner Joining Operator
     * @return ADDBuilder containing the new diagram
     */
    public ADDBuilder simpleJoin(UnaryOp joiner)throws WrongStructureADDException {
        if(getNoSuccessor().size()!=1){
            throw new WrongStructureADDException("Simple Join just works with Diagrams having ONE Endpoint. " +
                    "Try different Join-methods");
        }
        if(joiner==null){
            throw new IllegalArgumentException("Joiner can't be NULL");
        }

        //Connect joiner
        Vertex node1=getNoSuccessor().iterator().next();
        node1.addSuccessor(joiner);

        //joiner.setPredecessor(node1);
        id2Vertex.put(joiner.getId(),joiner);

        //model=new ADD(id2Vertex,Set.of(joiner),true);
        //Build complete Map
        return this;
    }

    /**
     * Adds a {@link UnaryOp UnaryOp} in a specific position.
     * If Target Vertex has Successors the {@link ADD#getNoSuccessor() noSuccessor} size will increase
     * @param joiner Joining Operator
     * @param attachTo The Vertex to add the Operator as Successor
     * @return ADDBuilder containing the new diagram
     */
    public ADDBuilder simpleJoin(UnaryOp joiner,Vertex attachTo) {
        if(joiner==null){
            throw new IllegalArgumentException("Joiner can't be NULL");
        }

        //Connect joiner
        attachTo.addSuccessor(joiner);

        //joiner.setPredecessor(attachTo);
        id2Vertex.put(joiner.getId(),joiner);

        //Build complete Map
        return this;
    }
    /*public ADDBuilder switchRandomOperator(){
        return switchRandomOperator(new Random());
    }*/
    public ADDBuilder switchRandomOperator(Random rand,List<Vertex> ops){
        return switchRandomOperator(rand, (Vertex) null,null,ops);//Casting needed for Compiler
    }

    /**
     * Changes one operator with another of the Same Superclass.
     * @param rand  A random number generator {@link Random}
     * @param classFrom A String to be used in {@link ADDBuilder#buildOperatorForKey(String)}
     * @param classTo A String to be used in {@link ADDBuilder#buildOperatorForKey(String)}
     * @return The ADDBuilder
     */
    public ADDBuilder switchRandomOperator(Random rand,String classFrom,String classTo,List<Vertex> ops){
        return switchRandomOperator(rand,buildOperatorForKey(classFrom),buildOperatorForKey(classTo),ops);
    }

    /**
     * Changes one operator with another of the Same Superclass.
     * @param classFrom A Vertex to represent the Class the source vertex has to be from
     * @param classTo A Vertex to represent the Class the Target should be switched to
     * @param rand  A random number generator {@link Random}
     * @return The ADDBuilder
     */
    public ADDBuilder switchRandomOperator(Random rand,Vertex classFrom,Vertex classTo,List<Vertex> ops){
        List<Vertex> operators=id2Vertex.values().stream()
                .filter(v->!(v instanceof BasicEvent)&&(classFrom==null||classFrom.getClass().equals(v.getClass())))
                .collect(Collectors.toList());
        Vertex toReplace=operators.get(rand.nextInt(operators.size()));
        Predicate<Vertex> condition;
        if(toReplace instanceof NaryOp){
            condition=v->v instanceof NaryOp&&
                    !v.getOperatorName().equals(toReplace.getOperatorName())&&
                    (classTo==null||classTo.getClass().equals(v.getClass()));
        }else {//UNary
            condition=v->v instanceof UnaryOp&&!v.getOperatorName().equals(toReplace.getOperatorName())&&
                    (classTo==null||classTo.getClass().equals(v.getClass()));
        }
        List<Vertex> filtered=ops.stream().
                filter(condition).
                collect(Collectors.toList());
        if(filtered.isEmpty()) {
            return this;
        }
        Vertex toPut=filtered.get(rand.nextInt(filtered.size())).copy();
        toReplace.replaceWith(toPut);

        toPut.setID(toReplace.getId());
        toPut.setName(toReplace.getName());

        id2Vertex.replace(toPut.getId(),toPut);
        logger.add(new FeedbackTuple(toPut,
                FeedbackTuple.MESSAGE_TYPE.INFORMATION,
                "Operator "+toPut.getId()+" got changed"));
        //ADD.validateConnections(id2Vertex);
        return this;
    }

    public ADDBuilder switchBasicEvent(){
        return switchBasicEvent(new Random());
    }
    /**
     * Switches the position of two BasicEvents in the Hierarchy.
     * @param rand A RNG
     * @return The Builder
     */
    public ADDBuilder switchBasicEvent(Random rand){
        List<Vertex> bes=id2Vertex.values().stream().filter(v->v instanceof BasicEvent).collect(Collectors.toList());
        if(bes.size()<2) {
            return this;
        }
        int i1=-1;
        int i2;
        do{
            if(i1==-1) {
                i1=rand.nextInt(bes.size());
            }
            i2=rand.nextInt(bes.size());
        }while (i1==i2);

        Vertex b1=bes.get(i1);
        Vertex b2=bes.get(i2);

        List<Vertex> b1Successors=b1.getSuccessors();
        b1Successors.forEach(s->s.replacePredecessor(b1,b2));

        List<Vertex> b2Successors=b2.getSuccessors();
        b1Successors.forEach(s->s.replacePredecessor(b2,b1));

        logger.add(new FeedbackTuple(b1,
                FeedbackTuple.MESSAGE_TYPE.INFORMATION,
                "BE "+b1.getId()+" got switched"));
        logger.add(new FeedbackTuple(b2,
                FeedbackTuple.MESSAGE_TYPE.INFORMATION,
                "BE "+b2.getId()+" got switched"));

        ADD.validateConnections(id2Vertex);
        return this;
    }

    /**
     * Inserts a Unary Operator at an arbitrary position by pushing its predecessor down the line.
     * @param rand Random to find Operator
     * @return The Builder
     */
    public ADDBuilder insertUnary(Random rand,List<Vertex> ops){
        //ADDBuilder.printModelCost(get(),System.out,false,true);
        List<Vertex> vertices=id2Vertex.values().stream().
                filter(v->!(v instanceof UnaryOp)).
                filter(v->v.getSuccessors().stream().noneMatch(v2->v2 instanceof UnaryOp)).
                collect(Collectors.toList());
        if(vertices.isEmpty()) {
            return this;
        }
        List<Vertex> operators=ops.stream().
                filter(v->v instanceof UnaryOp).
                collect(Collectors.toList());
        if(operators.isEmpty()) {
            return this;
        }
        UnaryOp operator=(UnaryOp)operators.get(rand.nextInt(operators.size())).copy();
        String id=getFreeID(rand);
        operator.setID(id);
        operator.setName(id);

        Vertex predecessor=vertices.get(rand.nextInt(vertices.size()));
        List<Vertex> successors=predecessor.getSuccessors();
        successors.forEach(s->s.replacePredecessor(predecessor,operator));

        predecessor.addSuccessor(operator);

        id2Vertex.put(id,operator);

        logger.add(new FeedbackTuple(operator,
                FeedbackTuple.MESSAGE_TYPE.INFORMATION,
                "Unary "+operator.getId()+" inserted"));
        return this;
    }

    /**
     * Removes a Unary Operator of the grid.
     * @param rand Random to find Operator
     * @return The Builder
     */
    public ADDBuilder removeUnary(Random rand){
        List<Vertex> operators=id2Vertex.values().stream().
                filter(v->v instanceof UnaryOp).
                collect(Collectors.toList());
        if(operators.isEmpty()) {
            return this;
        }
        UnaryOp operator=(UnaryOp)operators.get(rand.nextInt(operators.size()));
        Vertex predecessor=operator.getPredecessor();
        operator.removePredecessor(predecessor);
        operator.getSuccessors().forEach(s->s.replacePredecessor(operator,predecessor));

        id2Vertex.remove(operator.getId(),operator);

        logger.add(new FeedbackTuple(predecessor,
                FeedbackTuple.MESSAGE_TYPE.INFORMATION,
                "Unary over "+predecessor.getId()+" removed"));
        return this;
    }

    /**
     * Reorders an Operators Predecessors randomly.
     * This Method only makes sense if there are sequential Operators in the Model
     * @param rand A RNG
     * @return The Builder
     */
    public ADDBuilder shufflePredecessorsInSequential(Random rand){
        List<Vertex> sequential=id2Vertex.values().stream().
                filter(v->v instanceof NaryOp&&((NaryOp)v).isSequential()).
                collect(Collectors.toList());
        if(sequential.isEmpty()) {
            return this;
        }
        NaryOp op=(NaryOp)sequential.get(rand.nextInt(sequential.size()));
        List<Vertex> predecessors=op.getPredecessors();
        Collections.shuffle(predecessors,rand);
        op.clearPredecessors();
        op.addPredecessors(predecessors);

        logger.add(new FeedbackTuple(op,
                FeedbackTuple.MESSAGE_TYPE.INFORMATION,
                "Sequential Operator "+op.getId()+" got order changed"));
        return this;
    }

    /**
     * Returns a map of all Operator signatures.
     * The root and BasicEvents are excluded
     * @return A Map having the Vertex as Key and its BEs as an ordered (by ID) list
     * @see Vertex#getDownstreamBEs()
     */
    public Map<Vertex,List<BasicEvent>> getSignatures(){
        return getId2Vertex().values().stream().
                filter(v->!v.getSuccessors().isEmpty()&&!(v instanceof BasicEvent)).
                distinct().
                collect(Collectors.toMap(v->v,v->{
                    List<BasicEvent> bes=v.getDownstreamBEs();
                    bes.sort(Comparator.comparing(Vertex::getId, String::compareTo));
                    return bes;
                }));
    }

    /**
     * Returns the current model state
     * For safety the model is a deep copy of the state to avoid side effects in later calls.
     * @return a deep copied model representing the state
     */
    public ADD get(){
        ADD.validateConnections(id2Vertex);
        ADD output=ADDBuilder.copyADD(new ADD(getId2Vertex(),getNoSuccessor(),true));
        //Remap logger
        List<FeedbackTuple> neu=logger.stream().map(ftp->{
            Map<String,Vertex> id2VertexNeu=output.getId2Vertex();
            Vertex v1=ftp.getV1()==null?null:id2VertexNeu.get(ftp.getV1().getId());
            Vertex v2=null;
            if(ftp.getV2()!=null) {
                v2=id2VertexNeu.get(ftp.getV2().getId());
            }

            if(v1==null){
                return new FeedbackTuple(ftp.getType(),ftp.getText());
            }else if(v2==null){
                return new FeedbackTuple(v1,ftp.getType(),ftp.getText());
            }else{
                return new FeedbackTuple(v1,v2,ftp.getType(),ftp.getText());
            }
        }).collect(Collectors.toList());
        logger.clear();
        logger.addAll(neu);

        return output;
    }



    /**
     * Creates a List of all Valid Sequences (specified as a List of Ids) for an ADD.
     * @param maxLen the maximum length allowed for these sequences
     * @return A List with all Sequences (as String-List each) with given length constraint
     * */
    public List<List<String>> getAllValidSequences(int maxLen){
        return getAllValidSequences(new ArrayList<>(),maxLen);
    }
    /**
     * Creates a List of all Valid Sequences (specified as a List of Ids) for an ADD.
     * @param par a Sequence that has already been executed
     * @param maxLen the maximum length allowed for these sequences
     * @return A List with all Sequences (as String-List each) with given length constraint
     * */
    public List<List<String>> getAllValidSequences(List<String> par,int maxLen){
        List<List<String>> out=new ArrayList<>();
        if(par.size()>=maxLen){
            return out;
        }
        ADD model=get();
        model.getBasicEvents().forEach(b->{
                List<String> clone=new ArrayList<>(par);
                clone.add(b.getId());
                if(model.isValidSequence(clone)){
                    out.add(clone);
                    out.addAll(getAllValidSequences(clone,maxLen));
                }
        });
        return out;
    }
    /**
     * Concurrency able Version of {@link ADDBuilder#getAllValidSequences(List, int) getAllValidSequences}.
     * @param par a Sequence that has already been executed
     * @param maxLen the maximum length allowed for these sequences
     * @param res The ConcurrentList to write Sequences to
     * @param maxAm The maximum amount of sequences to generate
     * */
    public void getAllValidSequences(List<String> par, int maxLen,
                                     CopyOnWriteArrayList<List<String>> res,int maxAm){
        if(par.size()>=maxLen){
            return;
        }
        if(res.size()>=maxAm){
            return;
        }
        ADD model=get();
        model.getBasicEvents().forEach(b->{
            List<String> clone=new ArrayList<>(par);
            clone.add(b.getId());
            if(model.isValidSequence(clone)){
                res.add(clone);
                if(!model.postCheckSequence()){
                    //do not go deeper if diagram is already true
                    getAllValidSequences(clone,maxLen,res,maxAm);
                }
            }
        });
    }
}
