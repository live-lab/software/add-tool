package addtool.add.model;

import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;
import addtool.timeseries.PACTuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * common superclass of both types of basic events; stores costs, success probability and trigger-ability and reset-ability.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 9/18/15
 * @since 9/18/15
 */
public abstract class BasicEvent extends Vertex {
    private static Random rand=new Random(123);
    public static void setSeed(int seed){
        rand=new Random(seed);
    }

    /**
     * PACTuples for the properties success-probability, cost and delay.
     *
     * @see PACTuple
     */
    private PACTuple successProbability;
    private PACTuple cost;
    private PACTuple delay;
    /**
     * If the basic event is reset-able by a reset node.
     */
    private boolean resettable=false;
    /**
     * The current trigger state.
     *
     * @see TriggerState
     */
    TriggerState trigger_state = TriggerState.NOT_TRIGGER_ABLE;

    /**
     * Creates a new basic event.
     * In this case all properties are exact
     * @param name Name for the event
     * @param successProbability a rational determining the success probability
     * @param cost a rational for the cost
     * @param id The id
     */
    public BasicEvent(String name, Rational successProbability, Rational cost, String id){
        this(name,successProbability,cost,id,Rational.ZERO,Rational.ONE);
    }
    /**
     * Creates a new basic event.
     * In this case all properties except success probability
     * @param name Name for the event
     * @param successProbability a PACTuple for the success probability
     * @param cost a rational for the cost
     * @param id The id
     */
    public BasicEvent(String name, PACTuple successProbability, Rational cost, String id){
        this(name,successProbability,new PACTuple(cost,Rational.ZERO,Rational.ONE),new PACTuple(Rational.ZERO,Rational.ZERO,Rational.ONE),id);
    }

    public void randomize() {
        this.successProbability = new PACTuple(Rational.valueOf((long) (Math.random()*10), 10), Rational.ZERO, Rational.ONE);
        this.cost = new PACTuple(Rational.valueOf((long) (Math.random()*10), 1), Rational.ZERO, Rational.ONE);
    }

    public BasicEvent(String name, PACTuple successProbability, PACTuple cost, PACTuple delay, String id){
        super(name,id);
        this.successProbability=successProbability;
        this.cost=cost;
        this.delay=delay;
    }
    public BasicEvent(String name, Rational successProbability, Rational cost, String id,Rational uncertainty,Rational delta){
        this(name,new PACTuple(successProbability,uncertainty,delta),cost,id);
    }

    public Rational getSuccessProbability() {
        return successProbability.getValue();
    }

    public PACTuple getSuccessProbabilityTuple() {
        return successProbability.copy();
    }

    public Rational getCost() {
        return cost.getValue();
    }

    public PACTuple getCosts() {
        return cost.copy();
    }

    public PACTuple getDelay() {
        return delay.copy();
    }

    public TriggerState getTrigger_state() {
        return trigger_state;
    }

    @Override
    public Rational getDisruptionProb() {
        return this.getSuccessProbability();
    }

    @Override
    public Rational getUncertainty() {
        return successProbability.getUncertainty();
    }

    @Override
    public Rational getProbabilityDelta() {
        return successProbability.getDelta();
    }


    public boolean isTriggerAble() {
        return getTrigger_state() != TriggerState.NOT_TRIGGER_ABLE;
    }

    public boolean isResettable() {
        return resettable;
    }

    @Override
    public List<Vertex> getPredecessors() {
        return new ArrayList<>();
    }

    @Override
    public void addPredecessor(Vertex v,boolean notify) {}

    @Override
    public void removePredecessor(Vertex v,boolean notify) {}

    @Override
    public Vertex replacePredecessor(Vertex oldVertex, Vertex newVertex,boolean notify) {
        return newVertex;
    }

    @Override
    public List<BasicEvent> getDownstreamBEs(){
        return List.of(this);
    }

    public void setSuccessProbability(PACTuple successProbability) {
        this.successProbability=successProbability.copy();
    }
    public void setSuccessProbability(Rational successProbability) {
        this.successProbability=this.successProbability.setValue(successProbability);
    }
    public void setUncertainty(Rational unc){
        this.successProbability=successProbability.setUncertainty(unc);
    }
    public void setProbabilityDelta(Rational delta){
        this.successProbability=this.successProbability.setDelta(delta);
    }

    public void setTrigger_state(TriggerState ts){
        trigger_state=ts;
    }
    public void setCost(Rational cost) {
        this.cost=this.cost.setValue(cost);
        if(cost==null){
            cost=Rational.ZERO;
        }
        this.cost=this.cost.setValue(cost);
    }
    public void setCost(PACTuple cost) {
        if(cost!=null)
            this.cost=cost.copy();
    }
    public void setDelay(PACTuple delay) {
        if(delay!=null)
            this.delay=delay.copy();
    }

    public void setResettable(boolean resettable) {
        this.resettable = resettable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BasicEvent that = (BasicEvent) o;

        if (getTrigger_state() != that.getTrigger_state()) return false;
        if (resettable != that.resettable) return false;
        if (!Objects.equals(successProbability, that.successProbability))
            return false;
        return Objects.equals(cost, that.cost);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (successProbability != null ? successProbability.hashCode() : 0);
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (isTriggerAble() ? 1 : 0);
        result = 31 * result + (resettable ? 1 : 0);
        return result;
    }

    @Override
    public String saveToString(int depth) {
        return this.toString();
    }

    /**
     * Attempt this Event. State will be changed in the end.
     */
    public Value attempt(){
        if(!canAttempt())return getValue();
        if(rand.nextDouble()>this.getSuccessProbability().doubleValue()){
            //DnD logic, if uniform Double is greater success probability you fail
            this.setValue(Value.FALSE);
        }else{
            this.setValue(Value.TRUE);
        }
        return this.getValue();
    }
    public boolean canAttempt(){
        if(getTrigger_state()== TriggerState.TRIGGER_ABLE)return false;
        return getValue() == Value.UNDECIDED;
    }

    @Override
    public Value evaluate(List<Vertex> predecessorsOrdered, List<Value> valuesOrdered){
        return getValue();
    }

}
