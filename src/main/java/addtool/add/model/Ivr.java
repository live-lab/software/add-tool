package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import addtool.semantics.Value;

/**
 * three-valued logic IVR.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class Ivr extends UnaryOp {

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public Ivr(String name, String id) {
        super(name,id);
    }

    @Override
    public String getOperatorName() {
        return "Ivr";
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }

    @Override
    public void evaluate() {
        Vertex predecessor = this.getPredecessor();

        if (predecessor.getValue() == Value.TRUE) {
            this.setValue( Value.UNDECIDED);
        } else if (predecessor.getValue() == Value.FALSE) {
            this.setValue( Value.FALSE);
        } else {
            this.setValue( Value.TRUE);
        }
        if(this.getValue()!=Value.UNDECIDED)
            super.evaluate();
    }

    @Override
    public Vertex copy() {
        Ivr an=new Ivr(this.getName(),this.getId());
        this.writeValuesTo(an);
        return an;
    }
}