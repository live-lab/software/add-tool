package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import addtool.helpers.Values2String;
import org.jscience.mathematics.number.Rational;
import addtool.timeseries.PACTuple;

import java.util.Objects;

/**
 * timed basic events with their distribution.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 15.03.15
 */
public class BasicEventTime extends BasicEvent {

    /*
     * To use arbitrary sample distributions instead of exact points in time, only the field "distribution", the constructor
     * and the respective getter/setter have to be changed.
     */
    private String distribution;


    /**
     * Constructor for a time-driven basic event.
     *
     * @param successProbability the success probability
     * @param cost               the costs
     * @param distribution       the point in time when the event should happen
     */
    public BasicEventTime(String name, Rational successProbability, Rational cost, String distribution, String id) {
        super(name,successProbability,cost,id);
        if (distribution == null) {
            throw new IllegalArgumentException("ID and distribution of a timed basic event cannot be null!");
        }
        this.distribution = distribution;
    }
    public BasicEventTime(String name, Rational successProbability, Rational cost, String distribution, String id,Rational uncertainty,Rational delta) {
        super(name,successProbability,cost,id,uncertainty,delta);
        if (distribution == null) {
            throw new IllegalArgumentException("ID and distribution of a timed basic event cannot be null!");
        }
        this.distribution = distribution;
    }
    public BasicEventTime(String name, PACTuple successProbability, PACTuple cost, PACTuple delay, String id, String distribution){
        super(name,successProbability,cost,delay,id);
        if (distribution == null) {
            throw new IllegalArgumentException("ID and distribution of a timed basic event cannot be null!");
        }
        this.distribution = distribution;
    }

    @Override
    public String toString() {
        return this.getName() + '{' +
                "Prob=" + Values2String.Rational2string(this.getSuccessProbability(),4) +
                ", cost=" + this.getCost().doubleValue() +
                ", dist=" + this.distribution + '}';
    }

    public String getDistribution() {
        return distribution;
    }

    public void setDistribution(String s) {
        this.distribution = s;
    }

    @Override
    public String getOperatorName() {
        return "BETime";
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BasicEventTime that = (BasicEventTime) o;

        return Objects.equals(distribution, that.distribution);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (distribution != null ? distribution.hashCode() : 0);
        return result;
    }


    @Override
    public Vertex copy() {
        BasicEventTime an=new BasicEventTime(this.getName(),this.getSuccessProbabilityTuple(),this.getCosts(),this.getDelay(),this.getId(),this.getDistribution());
        this.writeValuesTo(an);
        return an;
    }
}
