package addtool.add.model;

/**
 * General interface for unary operators.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
@Deprecated
public interface IUnaryOp extends IVertex {

    IVertex getPredecessor();

    void setPredecessor(IVertex v1);
}
