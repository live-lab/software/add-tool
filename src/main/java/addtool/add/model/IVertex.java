package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Visited;

import java.util.List;

/**
 * General interface for vertices.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
@Deprecated
public interface IVertex extends Visited<ADDVisitor> {

    void addSuccessor(IVertex v);

    String getName();

    void setName(String s);

    String getId();

    void setID(String s);

    String getOperatorName();

    boolean isSinkAttacker();

    boolean isSinkDefender();

    void setSinkAttacker(boolean t);

    void setSinkDefender(boolean t);

    List<IVertex> getSuccessor();

    List<IVertex> getPredecessors();

    boolean isSink();

    void clearSuccessors();
}
