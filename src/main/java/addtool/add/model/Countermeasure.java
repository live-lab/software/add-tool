package addtool.add.model;

import addtool.analysisonadd.Direction;
import addtool.xml2add.ParserXML;
import addtool.analysisonadd.ADDVisitor;
import addtool.semantics.Value;

/**
 * An operator to describe role switch in ADTool.
 * Should not be used in dot. It is converted on input {@link ParserXML#parse(String)}.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 6.12.2018
 */
public class Countermeasure extends UnaryOp {

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public Countermeasure(String name, String id) {
        super(name,id);
    }

    @Override
    public String getOperatorName() {
        return "Countermeasure";
    }

    @Override
    public void evaluate() {
        this.setValue(this.getPredecessor().getValue());
        //System.err.println("Countermeasure does nothing");
        if(this.getValue()!= Value.UNDECIDED) {
            super.evaluate();
        }
    }

    @Override
    public Vertex copy() {
        Countermeasure an=new Countermeasure(this.getName(),this.getId());
        this.writeValuesTo(an);
        return an;
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }
}
