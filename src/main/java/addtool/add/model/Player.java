package addtool.add.model;

/**
 * enum to distinguish different players, arbitrary many players possible.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 30.01.2015
 */
public enum Player {
    Attacker, Defender;

    public static Player next(Player p){
        if(p==Attacker){
            return Defender;
        }
        else{
            return Attacker;
        }
    }
}
