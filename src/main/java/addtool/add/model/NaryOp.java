package addtool.add.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * common superclass for binary operators If, And, Or, SAnd and SOr.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 18.10.15
 * @since 18.10.15
 */
public abstract class NaryOp extends Vertex {


    /**
     * List of Predecessors.
     */
    private final ArrayList<Vertex> predecessors = new ArrayList<>();


    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public NaryOp(String name,String id){
        super(name,id);
    }

    public Vertex getPredecessor01() {
        return getPredecessor(0);
    }

    public Vertex getPredecessor02() {
        return getPredecessor(1);
    }

    public Vertex getPredecessor(int i) {
        if(i>=predecessors.size()){
            return null;
        }
        return predecessors.get(i);
    }

    /**
     * Get the List of Predecessors.
     * @return a flat copy of the list
     */
    @Override
    public List<Vertex> getPredecessors() {
        return new ArrayList<>(predecessors);
    }

    @Override
    public void addPredecessor(Vertex v,boolean notify) {
        if(v!=null&&!predecessors.contains(v)){
            predecessors.add(v);
            if(notify)v.addSuccessor(this,false);
        }
    }
    /**
     * Empties {@link NaryOp#predecessors}.
     */
    @Override
    public void removePredecessor(Vertex vertex,boolean notify) {
        if(predecessors.contains(vertex)){
            predecessors.remove(vertex);
            if(notify)vertex.removeSuccessor(this,false);
        }
    }

    @Override
    public Vertex replacePredecessor(Vertex oldVertex, Vertex newVertex,boolean notify) {
        if(newVertex!=null&&predecessors.contains(oldVertex)){
            int pos=predecessors.indexOf(oldVertex);
            predecessors.remove(oldVertex);
            predecessors.add(pos,newVertex);
            if(notify){
                oldVertex.removeSuccessor(this,false);
                newVertex.addSuccessor(this,false);
            }
            return oldVertex;
        }
        return newVertex;
    }

    public boolean isSequential(){return false;}

    /**
     * Executes {@link NaryOp#setPredecessor(int, Vertex)} with as setPredecessor(0,v1).
     * @param v1 A Vertex to add to the predecessor list
     */
    @Deprecated
    public void setPredecessor01(Vertex v1) {
        setPredecessor(0, v1);
    }

    /**
     * Executes {@link NaryOp#setPredecessor(int, Vertex)} with as setPredecessor(1,v2).
     * @param v2 A Vertex to add to the predecessor list
     */
    @Deprecated
    public void setPredecessor02(Vertex v2) {
        setPredecessor(1, v2);
    }

    /**
     * Adds a Vertex to {@link NaryOp#predecessors}.
     * @param i The index an element has to have after insertion
     * @param vertex A vertex to add
     * @throws IllegalArgumentException If the position after insertion is not i
     * @see NaryOp#addPredecessors(Collection)
     */
    @Deprecated
    public void setPredecessor(int i, Vertex vertex) {
        if(vertex==null||predecessors.contains(vertex)){
            return;
        }
        predecessors.add(vertex);
        if (predecessors.get(i) != vertex) {//here the != is on purpose as we are looking for the exact same object
            throw new IllegalArgumentException("setPredecessors used the wrong way");
        }
    }

    /**
     * Adds a collection of vertices to {@link NaryOp#predecessors}.
     * The list is filtered for null values
     * @param predecessors A Collection to add. null will be filtered
     */
    public void addPredecessors(Collection<Vertex> predecessors) {
        predecessors.forEach(this::addPredecessor);
    }

    /**
     * Empties {@link NaryOp#predecessors}.
     */
    public void clearPredecessors() {
        predecessors.clear();
    }


    @Override
    protected void writeValuesTo(Vertex v) {
        super.writeValuesTo(v);
    }

    /*
    @Override
    public void accept(ADDVisitor visitor) {
        for (Vertex predecessor : predecessors) {
            predecessor.accept(visitor);
        }
        visitor.visit(this);
    } */

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        if (!super.equals(o)){
            return false;
        }

        NaryOp naryOp = (NaryOp) o;

        // equals for AbstractLists checks element-wise equality
        return this.saveHashCode(0)==naryOp.saveHashCode(0);
    }

    @Override
    public int saveHashCode(int depth) {
        int result = super.saveHashCode(depth);
        //result = 31 * result +
        if(depth<= HASH_MAX_DEPTH) {
            for (Vertex predecessor : predecessors) {
                if (predecessor != null) {
                    result = 31 * result + predecessor.saveHashCode(depth + 1);
                }
            }
        }
        return result;
    }
    @Override
    public String saveToString(int depth) {
        if(depth> HASH_MAX_DEPTH){
            return "...";
        }
        return getOperatorName() + predecessors.stream()
                .map(v->v.saveToString(depth+1))
                .collect(Collectors.joining(",","[","]"));
    }
}
