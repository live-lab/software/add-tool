package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;
import addtool.timeseries.PACTuple;

import java.util.stream.Collectors;

/**
 * conditional operator.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class If extends NaryOp {

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public If(String name, String id) {
        super(name,id);
    }

    @Override
    public Rational getDisruptionProb(){
        return PAC.calcProduct(getPredecessors(), PAC::getDisruptionProb);
    }
    @Override
    public Rational getUncertainty(){
        return PAC.calcProductUncertainty(getPredecessors().stream().map(v -> new PACTuple(v.getDisruptionProb(), v.getUncertainty(), v.getProbabilityDelta())).collect(Collectors.toList()));
    }
    @Override
    public Rational getProbabilityDelta(){
        return PAC.calcProduct(getPredecessors(), PAC::getProbabilityDelta);
    }

    @Override
    public String getOperatorName() {
        return "If";
    }

    @Override
    public String toString() {
        return "If(Input: " + getPredecessor01() + ", Guard: " + getPredecessor02() + ')';
    }

    @Override
    public void evaluate() {
        if(getPredecessor02().getValue()== Value.TRUE){
            setValue(getPredecessor01().getValue());
        }else{
            setValue(Value.UNDECIDED);
        }
    }


    @Override
    public Vertex copy() {
        If an=new If(this.getName(),this.getId());
        this.writeValuesTo(an);
        return an;
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }
}
