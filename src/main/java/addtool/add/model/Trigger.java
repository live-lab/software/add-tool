package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import addtool.semantics.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * representation of the operator reset together with its reset edges.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class Trigger extends UnaryOp {
    /**
     * A list of basic events, that will be available after the trigger value is SUCCESS.
     * @see Value
     */
    private List<BasicEvent> toTrigger = new ArrayList<>();

    public Trigger(String name, String id) {
        super(name,id);
        this.setCanBeRoot(false);
    }

    @Override
    public String getOperatorName() {
        return "Trigger";
    }
    public List<BasicEvent> getToTrigger() {
        return new ArrayList<>(toTrigger);
    }

    @Override
    public void setValue(Value v){
        super.setValue(v);
        if(getValue()==Value.UNDECIDED){
            toTrigger.forEach(b->b.setTrigger_state(TriggerState.TRIGGER_ABLE));//On reset, reset trigger state
        }
    }

    public void addToTrigger(BasicEvent v) {
        if (!toTrigger.contains(v)) {
            this.toTrigger.add(v);
        }
    }

    public void clearToTrigger() {
        toTrigger = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Trigger(" + getPredecessor() + ')';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Trigger trigger = (Trigger) o;

        return Objects.equals(toTrigger, trigger.toTrigger);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (toTrigger != null ? toTrigger.hashCode() : 0);
        return result;
    }
    @Override
    public void evaluate(){
        this.setValue(this.getPredecessor().getValue());
          if(getValue()== Value.TRUE){
              toTrigger.forEach(b->b.setTrigger_state(TriggerState.TRIGGER_ABLE_TRIGGERED));
          }
          super.evaluate();
    }

    @Override
    public Vertex copy() {
        Trigger an=new Trigger(this.getName(),this.getId());
        an.toTrigger= new ArrayList<>(this.toTrigger);
        this.writeValuesTo(an);
        return an;
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }
}

