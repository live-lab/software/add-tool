package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import addtool.analysisonadd.Visited;
import addtool.helpers.Values2String;
import addtool.learning.Sequence;
import addtool.semantics.Value;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents ADDs as a collection of vertices. Edges, trigger edges and reset edges are stored as doubled-link
 * structure within the vertices.
 *
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 5.12.2018
 */
public class ADD implements Iterable<Vertex>, Visited<ADDVisitor> {
    /**
     * Checks all connections in a Map of String, Vertex.
     * @param id2Vertex The Vertex Map to reconfigure
     * @see ADD#validateConnections()
     */
    public static void validateConnections(Map<String,Vertex> id2Vertex){
        id2Vertex.values().forEach(op->{
            op.getPredecessors().forEach(pr->{
                if(!pr.getSuccessors().contains(op)&&!pr.equals(op)) {
                    pr.addSuccessor(op,false);//No need to notify op as it obviously knows about pr
                }
            });
            List<Vertex> suc=op.getSuccessors().stream()
                    .filter(o->(o instanceof UnaryOp||o instanceof NaryOp)&&!o.equals(op))
                    .collect(Collectors.toList());//Filters for BEs in Line too
            op.clearSuccessors();
            op.addSuccessors(suc);

            op.getSuccessors().stream().filter(o->(o instanceof BasicEvent)).forEach(su->{

            });
        });
    }

    //all information are contained in the vertices, so we do not need to store other information
    /**
     * A Set of all vertices that have no successors
     * The Set is not monitored. Changes on the contained vertices will not automatically change the set
     */
    private final Set<Vertex> noSuccessor;
    /**
     * A Map containing all listed Vertices of an ADD. The key is the unique ID of the vertex.
     * The Members of {@link #noSuccessor noSuccessor} should be included
     * There can be side effects in some operations on the vertex id.
     * Use {@link #makeIDsConsistent makeIDsConsistent} to avoid these.
     */
    private Map<String, Vertex> id2Vertex;
    /**
     * The Top Vertex for the Attacker.
     * Defined by the only Vertex that returns true on {@link Vertex#isSinkAttacker() Vertex.isSinkAttacker()}
     */
    private Vertex topAtt;
    /**
     * The Top Vertex for the Defender.
     * Defined by the only Vertex that returns true on {@link Vertex#isSinkDefender() Vertex.isSinkDefender()}
     * @see #topAtt Top Attackernode
     */
    private Vertex topDef;
    /**
     *  An int representing the maximum ID of the Vertices used.
     *  It is not entirely save as Strings are technically allowed as Vertex ID.
     *  It is computed in {@link #computeMaxID() computeMaxID()}, which is triggered by compute=true in the constructor.
     */
    private int maxID;

    /**
     * Comments read from file.
     */
    private List<String> comments;

    /**
     * Creates a new ADD.
     * The Map and Set are not copied, therefore be careful with modifications after constructing the object.
     * @param id2Vertex {@link #id2Vertex see id2Vertex}
     * @param noSuccessor {@link #noSuccessor see noSuccessor}
     * @param compute If true {@link #computeMaxID() computeMaxID()} is called.
     */
    public ADD(Map<String, Vertex> id2Vertex, Set<Vertex> noSuccessor, boolean compute) {
        if (id2Vertex == null || noSuccessor == null) {
            throw new IllegalArgumentException("Map cannot be null!");
        }
        this.id2Vertex = new HashMap<>(id2Vertex);
        this.noSuccessor = new HashSet<>(noSuccessor);
        for (Vertex v : this) {
            if (v.isSinkAttacker()) {
                topAtt = v;
            }
            if (v.isSinkDefender()) {
                topDef = v;
            }
        }
        if (compute) {
            // updates field maxID
            this.computeMaxID();
        }
        comments=new ArrayList<>();
    }

    /**
     * Creates a flat copy of the id2vertex map.
     * @return A flat copy of {@link #id2Vertex id2Vertex}
     */
    public Map<String, Vertex> getId2Vertex() {
        return id2Vertex.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
    }

    /**
     * returns all vertices.
     * @return Values of {@link #id2Vertex id2Vertex}
     */
    public Collection<Vertex> getVertices() {
        return id2Vertex.values();
    }

    /**
     * Returns maximum id used in the model.
     * @return {@link #maxID maxID}
     */
    public int getMaxID() {
        return getMaxID(false);
    }

    /**
     * Returns maximum id used in the model.
     * @param computeIfZero if true {@link #maxID maxID} is computed if it was zero.
     * @return {@link #maxID maxID}
     */
    public int getMaxID(boolean computeIfZero) {
        if(maxID==0&&computeIfZero){
            this.computeMaxID();
        }
        return maxID;
    }

    /**
     * Gets the {@link #topAtt Top Node for the Attacker}.
     * If none is specified return any node of {@link #noSuccessor noSuccessor}.
     * @return {@link #topAtt topAtt} or {@link #noSuccessor a member of noSuccessor}
     */
    public Vertex getTopAtt() {
        if (topAtt == null) {
            return getNoSuccessor().stream().findAny().orElse(null);
        }
        return topAtt;
    }

    public void setTopAtt(Vertex v){
        this.topAtt = v;
    }

    /**
     * Gets the {@link #topAtt Top Node for the Attacker} or if it is null {@link #topDef Top Node for the Defender}.
     * If none is specified return any node of {@link #noSuccessor noSuccessor}.
     * @return {@link #topAtt topAtt}, {@link #topDef topDef} or {@link #noSuccessor a member of noSuccessor}
     */
    public Vertex getTop() {
        if (topAtt == null) {
            if(topDef==null){
                return getNoSuccessor().stream().findAny().orElse(null);
            }
            return topDef;
        }
        return topAtt;
    }
    public void setComments(List<String> comment){
        comments=new ArrayList<>(comment);
    }
    public List<String> getComments(){
        return  new ArrayList<>(comments);
    }

    /**
     * Returns the maximum dimension of costs on basic events in the model.
     * Will return 1 all the time, as more dimensions are currently unsupported.
     * @return the maximal number of different costs a basic event uses
     */
    @SuppressWarnings("UnnecessaryLocalVariable")
    public int getMaxCostDimension() {
        int i = 1;

        /* for (Vertex v : id2Vertex.values()) {
            if (v instanceof IBasicEvent) {
                if (((IBasicEvent) v).getCost().length > i) {
                    i = ((IBasicEvent) v).getCost().length;
                }
            }

            if (v instanceof Cost) {
                if (((Cost) v).getBound().length > i) {
                    i = ((Cost) v).getBound().length;
                }
            }
        }*/

        return i;
    }

    /**
     * Returns a vertex from the model with a given id.
     * @param id Key for the specified vertex.
     * @return Vertex specified with id in {@link #id2Vertex id2Vertex}
     */
    public Vertex getVertex2Id(String id) {
        return id2Vertex.get(id);
    }

    /**
     * Returns a stream with the basic events in the model.
     * @return A Stream of all BasicEvents in {@link #id2Vertex id2Vertex}
     * @see #getOperatorStream()
     */
    private Stream<BasicEvent> getBasicEventStream(){
        return id2Vertex.values().stream().filter(v->(v instanceof BasicEvent)).map(v -> (BasicEvent) v);
    }

    /**
     * Returns a stream with the basic events in the model.
     * @return A Stream of all BasicEvents in {@link #id2Vertex id2Vertex}
     * @see #getOperatorStream()
     */
    private Stream<BasicEventPlayer> getBasicEventPStream(){
        return id2Vertex.values().stream().filter(v->(v instanceof BasicEventPlayer)).map(v -> (BasicEventPlayer) v);
    }

    /**
     * Returns a stream of all non basic events in the model.
     * @return A Stream of all Operators (not BasicEvents) in {@link #id2Vertex id2Vertex}
     * @see #getBasicEventStream()
     */
    private Stream<Vertex> getOperatorStream(){
        return id2Vertex.values().stream().filter(v->(!(v instanceof BasicEvent)));
    }

    /**
     * Returns a list with all operators.
     * @return A List containing all operators (not basic events) of {@link #id2Vertex id2Vertex}
     */
    public List<Vertex> getOperators(){
        return getOperatorStream().collect(Collectors.toList());
    }

    /**
     * Returns a set of all basic events.
     * @return A Set containing all basic events of {@link #id2Vertex id2Vertex}
     */
    public Set<Vertex> getBasicEvents(){
        return getBasicEventStream().collect(Collectors.toSet());
    }

    /**
     * Returns a set of all basic events with matching reference type.
     * @return A Set containing all basic events of {@link #id2Vertex id2Vertex}
     */
    public Set<BasicEventPlayer> getBasicEventsAsBEs(){
        return getBasicEventPStream().collect(Collectors.toSet());
    }

    /**
     * Returns all basic event ids.
     * @return A Set containing the {@link Vertex#getId() Ids} of all basic events in {@link #id2Vertex id2Vertex}
     */
    public Set<String> getBasicEventIds(){
        return getBasicEventStream().map(Vertex::getId).collect(Collectors.toSet());
    }

    /**
     * Returns a set with all vertices that do not have successors.
     * @return All Members of {@link #noSuccessor noSuccessor} that can be root
     * @see Vertex#canBeRoot() Vertex canberoot
     */
    public Set<Vertex> getNoSuccessor() {
        return getNoSuccessor(true);
    }

    /**
     * Returns a set with all vertices that do not have successors.
     * @param onlyAllowed if true return only Vertices that can be root
     * @return All Members of {@link #noSuccessor noSuccessor}
     * @see Vertex#canBeRoot() Vertex canberoot
     */
    public Set<Vertex> getNoSuccessor(boolean onlyAllowed) {
        return noSuccessor.stream().filter(v->!onlyAllowed||v.canBeRoot()).collect(Collectors.toSet());
    }

    /**
     * update the maximal ID field.
     */
    private void computeMaxID() {
        // search for max id
        int i = 0;
        for (String s : id2Vertex.keySet()) {
            int tmp = Values2String.id2Int(s);
            if (tmp > i) {
                i = tmp;
            }
        }

        maxID = i;
    }
    /**
     * Updates {@link #id2Vertex id2Vertex} to have Keys and {@link Vertex#getId() Vertex.getId()} match again.
     * Executes {@link #computeMaxID() computeMaxID()} afterwards.
     */
    public void makeIDsConsistent() {
        int i = 0;
        HashMap<String, Vertex> newMap = new HashMap<>();
        for (Vertex vertex : id2Vertex.values() ){
            String id = String.valueOf(i);
            vertex.setID(id);
            newMap.put(id, vertex);
            i++;
        }
        this.id2Vertex = newMap;
        this.computeMaxID();
    }

    /**
     * Checks all internal edges.
     * This method should be called after generation
     * it will check all successors and predecessors and their connections in both directions
     */
    public void validateConnections(){
        ADD.validateConnections(id2Vertex);
    }

    @Override
    public Iterator<Vertex> iterator() {
        return id2Vertex.values().iterator();
    }

    /**
     * This method inserts a new State with a unique and new ID into an ADD.
     * {@link #noSuccessor noSuccessor} and {@link #maxID maxID} will not be updated
     * @param vertex State, not contained in the ADD so far; not null
     */
    public void addVertex(Vertex vertex) {
        if (vertex == null) {
            throw new IllegalArgumentException("Given Vertex is null!");
        } else if (id2Vertex.containsKey(vertex.getId())) {
            throw new IllegalArgumentException("ID " + vertex.getId() + " is already used in the ADD");
        } else {
            id2Vertex.put(vertex.getId(), vertex);

            if (Values2String.id2Int(vertex.getId()) > getMaxID()) {
                maxID = Values2String.id2Int(vertex.getId());
            }
        }

    }

    /**
     * This Method removes a State from an ADD if it is contained.
     *  Computes {@link #computeMaxID() computeMaxID()} after removal.
     * @param vertex vertex to remove; not null
     */
    public void removeVertex(Vertex vertex) {
        if (vertex == null) {
            throw new IllegalArgumentException("Vertex should not be null!");
        }
        id2Vertex.remove(vertex.getId());
        if (maxID == Values2String.id2Int(vertex.getId())) {
            this.computeMaxID();
        }
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                noSuccessor.forEach(x -> x.accept(visitor, direction));
            }
            case UP -> {
                getBasicEventsAsBEs().forEach(v -> v.accept(visitor, direction));
            }
        }
    }

    @Override
    public String toString() {
        Vertex top=getTop();
        if(top==null){
            top=getNoSuccessor().iterator().next();
        }
        return "ADD{" +  top + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ADD iVertices = (ADD) o;

        if (!(Objects.equals(id2Vertex, iVertices.id2Vertex))) {
            return false;
        }
        if (!Objects.equals(noSuccessor, iVertices.noSuccessor)){
            return false;
        }
        if (!Objects.equals(topAtt, iVertices.topAtt)){
            return false;
        }
        return Objects.equals(topDef, iVertices.topDef);
    }

    @Override
    public int hashCode() {
        int result = id2Vertex != null ? id2Vertex.hashCode() : 0;
        result = 31 * result + (noSuccessor != null ? noSuccessor.hashCode() : 0);
        result = 31 * result + (topAtt != null ? topAtt.hashCode() : 0);
        result = 31 * result + (topDef != null ? topDef.hashCode() : 0);
        result = 31 * result + maxID;
        return result;
    }

    /**
     * The Method checks a Sequence of basic events (specified by their Ids) to check if the overall result is true.
     * @param Ids A Sequence of taken basic events
     * @return if the Diagram evaluated true on any end point
     */
    @Deprecated
    public boolean evaluateSequence(String... Ids){
        return evaluateSequence(Arrays.asList(Ids));
    }
    /**
     * The Method checks a Sequence of basic events by their Ids to check if the overall result is true.
     * @param sequence A Sequence of taken basic events
     * @return if the diagram evaluated true on any end point
     */
    @Deprecated
    public boolean evaluateSequence(Sequence sequence){
        return evaluateSequence(sequence.getEvents());
    }
    /**
     * The Method checks a Sequence of basic events by their Ids to check if the overall result is true.
     * @param Ids A Sequence of taken basic events
     * @return if the diagram evaluated true on any end point
     */
    @Deprecated
    public boolean evaluateSequence(List<String> Ids){
        if(Ids==null){
            return false;
        }
        //Reset all points
        id2Vertex.values().forEach(v->v.setValue(Value.UNDECIDED));
        //evaluate NAT-Nodes. Multiple times in case propagation did not work right
        id2Vertex.values().stream().filter(v->v instanceof Nat|| v instanceof NotTrue).forEach(Vertex::evaluate);
        id2Vertex.values().stream().filter(v->v instanceof Nat|| v instanceof NotTrue).forEach(Vertex::evaluate);
        id2Vertex.values().stream().filter(v->v instanceof Nat|| v instanceof NotTrue).forEach(Vertex::evaluate);

        return Ids.stream().anyMatch(id->{
            Vertex v=id2Vertex.get(id);
            if(v==null){
                System.err.println("Warning: Vertex with ID "+id+" not in ADD");
                System.err.println(id2Vertex.keySet());
                return false;
            }

            if(v instanceof BasicEvent){
                ((BasicEvent)v).attempt();
            }else{
                System.err.println("Warning: non-BasicEvents should not be attempted directly in Sequence! for "
                        +id);
                v.setValue(Value.TRUE);
            }
            v.evaluate();

            return noSuccessor.stream().anyMatch(o->o.getValue()== Value.TRUE);
        });
    }

    /**
     * Directly after running {@link #isValidSequence(String...) isvalidsequence},
     * determine if the sequence was successful
     * @return if the Diagram evaluated true on any end point
     */
    public boolean postCheckSequence(){
        return noSuccessor.stream().anyMatch(o->o.getValue()== Value.TRUE);
    }
    /**
     * The Method checks a Sequence if just events are attempted that are valid (not yet failed or not triggered).
     * @param Ids A Sequence of taken basic events
     * @return if the sequence is valid
     */
    public boolean isValidSequence(List<String> Ids){
        if(Ids==null){
            return false;
        }
        //Reset all points
        id2Vertex.values().forEach(v->v.setValue(Value.UNDECIDED));

        return Ids.stream().allMatch(id->{
            Vertex v=id2Vertex.get(id);
            if(v instanceof BasicEvent){
                if(!((BasicEvent)v).canAttempt()){
                    return false;
                }
                ((BasicEvent)v).attempt();
            }else{
                System.err.println("Warning: non-BasicEvents should not be attempted directly in Sequence! for "
                        +v.getId());
                v.setValue(Value.TRUE);
            }
            v.evaluate();

            return true;
        });
    }
    /**
     * The Method checks a Sequence if just events are attempted that are valid (not yet failed or not triggered).
     * @param Ids A Sequence of taken basic events
     * @return if the sequence is valid
     */
    public boolean isValidSequence(String... Ids){
        return isValidSequence(Arrays.asList(Ids));
    }

    public Set<Vertex> getReachableVertices() {
        List<Vertex> worklist = new LinkedList<>();
        worklist.add(getTop());
        Set<Vertex> vertices = new HashSet<>();
        vertices.add(getTop());
        while(!worklist.isEmpty()){
            Vertex cur = worklist.remove(0);
            for(Vertex predecessor : cur.getPredecessors()){
                if(!vertices.contains(predecessor)){
                    worklist.add(predecessor);
                    vertices.add(predecessor);
                }
            }
        }
        return vertices;
    }
}
