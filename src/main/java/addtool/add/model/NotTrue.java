package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;

/**
 * three-valued logic NOT.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class NotTrue extends UnaryOp{

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public NotTrue(String name, String id) {
        super(name,id);
    }

    @Override
    public String getOperatorName() {
        return "NotTrue";
    }

    @Override
    public Rational getDisruptionProb(){
        if(getPredecessor()==null){
            return Rational.ONE;
        }
        return Rational.ONE.minus(getPredecessor().getDisruptionProb());
    }
    /*@Override
    public String toString() {
        return "Not";
    }*/

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }

    @Override
    public void evaluate() {
        switch (this.getPredecessor().getValue()) {
            case FALSE, UNDECIDED -> this.setValue(Value.TRUE);
            case TRUE -> this.setValue(Value.FALSE);
        }
        if(this.getValue()!=Value.UNDECIDED)
            super.evaluate();
    }

    @Override
    public Vertex copy() {
        NotTrue an=new NotTrue(this.getName(),this.getId());
        this.writeValuesTo(an);
        return an;
    }
}