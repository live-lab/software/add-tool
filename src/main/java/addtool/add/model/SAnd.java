package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;
import addtool.timeseries.PACTuple;

import java.util.List;
import java.util.stream.Collectors;

/**
 * representation of the operator sequential and -> not necessary for functional completeness,
 * but allows more efficient implementation.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class SAnd extends NaryOp{
    /**
     * A boolean for {@link SAnd#evaluate()} that saves if the sequence is still intact.
     */
    private boolean sequenceIntact =true;

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public SAnd(String name, String id) {
        super(name,id);
    }

    @Override
    public String getOperatorName() {
        return "SAnd";
    }
    @Override
    public Rational getDisruptionProb(){
        return PAC.calcProduct(getPredecessors(), PAC::getDisruptionProb);
    }
    @Override
    public Rational getUncertainty(){
        return PAC.calcProductUncertainty(getPredecessors().stream().map(v -> new PACTuple(v.getDisruptionProb(), v.getUncertainty(), v.getProbabilityDelta())).collect(Collectors.toList()));
    }
    @Override
    public Rational getProbabilityDelta(){
        return PAC.calcProduct(getPredecessors(), PAC::getProbabilityDelta);
    }

    @Override
    public void setValue(Value s){
        super.setValue(s);
        if(s== Value.UNDECIDED) {
            //Reset control variable
            sequenceIntact =true;
        }
    }
    @Override
    public void evaluate() {
        //reset sequence boolean if none are active
        if(this.getPredecessors().stream().allMatch(v->v.getValue()==Value.UNDECIDED)){
            this.setValue(Value.UNDECIDED);
        }else if(!sequenceIntact){
            this.setValue(Value.FALSE);
        }else{
            List<Vertex> predecessors =this.getPredecessors();
            int state=0;//0
            for (Vertex pre : predecessors){
                if(pre.getValue()==Value.TRUE){
                    if(state!=0) {
                        sequenceIntact = false;
                        this.setValue(Value.FALSE);
                        break;
                    }
                    //else just stay on state=0
                }else if(pre.getValue()==Value.UNDECIDED){
                    state=1;
                }else if(pre.getValue()==Value.FALSE){
                    sequenceIntact = false;
                    state=1;
                    this.setValue(Value.FALSE);
                    break;
                }
            }
            this.setValue(state==0?Value.TRUE:this.getValue());//CAVE side effect on sequenceIntact
        }
        super.evaluate();
    }

    @Override
    public Vertex copy() {
        SAnd an=new SAnd(this.getName(),this.getId());
        this.writeValuesTo(an);
        return an;
    }
    @Override
    public boolean isSequential(){return true;}

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }
}
