package addtool.add.model;


import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;

public class UnaryLogicOp extends UnaryOp {

    private final Value valTrue;
    private final Value valFalse;
    private final Value valUndecided;

    /**
     * Uses Superclass constructor.
     *
     * @param name A name for the Vertex. If null, id will be used
     * @param id   The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public UnaryLogicOp(String name, String id, Value valTrue, Value valFalse, Value valUndecided) {
        super(name, id);
        this.valTrue = valTrue;
        this.valFalse = valTrue;
        this.valUndecided = valTrue;
    }

    @Override
    public String getOperatorName() {
        return "GeneralUnaryLogicOp";
    }

    @Override
    public Rational getDisruptionProb() {
        return Rational.ONE.minus(getPredecessor().getDisruptionProb());
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                // visitor.visit(this);
            }
            case UP -> {
                // visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }

    @Override
    public void evaluate() {
        switch (this.getPredecessor().getValue()) {
            case TRUE -> this.setValue(valTrue);
            case FALSE -> this.setValue(valFalse);
            case UNDECIDED -> this.setValue(valUndecided);
        }
        if (this.getValue() != Value.UNDECIDED)
            super.evaluate();
    }

    @Override
    public Vertex copy() {
        UnaryLogicOp an = new UnaryLogicOp(this.getName(), this.getId(), valTrue, valFalse, valUndecided);
        this.writeValuesTo(an);
        return an;
    }
}