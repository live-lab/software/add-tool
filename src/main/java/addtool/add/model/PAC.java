package addtool.add.model;

import org.jscience.mathematics.number.Rational;
import addtool.timeseries.PACTuple;

import java.util.List;
import java.util.function.Function;

/**
 * Contains basic mathematical operation s for PAC propagation.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 28.07.2020
 */
public interface PAC {
    static <T extends PACTuple> Rational calcProbabilityDelta(List<T> predecessors) {
        Rational identity=Rational.ZERO;
        for(PACTuple p:predecessors){
            identity=calcProbabilityDelta(identity,p.getDelta());
        }
        return identity;
    }

    public static Rational calcProbabilityDelta(Rational u1, Rational u2){
        return Rational.ONE.minus(Rational.ONE.minus(u1).times(Rational.ONE.minus(u2)));
    }
    static <T extends PACTuple> Rational calcAndUncertainty(T val1,T val2) {
        Rational part1 = val1.getValue().times(val2.getUncertainty());
        Rational part2 = val2.getValue().times(val1.getUncertainty());
        Rational part3 = val1.getUncertainty().times(val2.getUncertainty());
        return part1.plus(part2).plus(part3);
    }
    static <T extends PACTuple> Rational calcOrUncertainty(T val1,T val2) {
        return calcAndUncertainty(val1,val2).plus(val1.getUncertainty()).plus(val2.getUncertainty());
    }

    @Deprecated
    static <T extends PACTuple> Rational calcProductUncertainty(List<T> predecessors) {
        if (predecessors.isEmpty()) {
            return Rational.ONE;
        }
        Rational all = multiplyAll(predecessors);
        T first = predecessors.get(0);
        Rational res = calcProductUncertaintyRecursive(predecessors, 1);
        return res.minus(all);
    }

    @Deprecated
    private static <T extends PACTuple> Rational calcProductUncertaintyRecursive(List<T> predecessors, int i) {
        if (predecessors.size() == i) {
            return Rational.ONE;
        } else {
            T current = predecessors.get(i);
            Rational rec = calcProductUncertaintyRecursive(predecessors, i + 1);
            Rational res = rec.times(current.getUncertainty());
            res = res.plus(rec.times(current.getValue()));
            return res;
        }
    }

    public static <T extends PACTuple> Rational multiplyAll(List<T> predecessors) {
        Rational res = Rational.ONE;
        for (T p : predecessors) {
            res = res.times(p.getValue());
        }
        return res;
    }

    @Deprecated
    static <T extends PAC> Rational calcSumUncertainty(List<T> predecessors) {
        Rational sum = Rational.ZERO;
        for (T p : predecessors) {
            Rational uncertainty = p.getUncertainty();
            sum = sum.plus(uncertainty);
        }
        return sum;
    }

    static <T extends PAC> Rational calcProduct(List<T> predecessors, Function<T, Rational> map) {
        Rational prod = Rational.ONE;
        for (T p : predecessors) {
            prod = prod.times(map.apply(p));
        }
        return prod;
    }

    static <T extends PAC> Rational calcSum(List<T> predecessors, Function<T, Rational> map) {
        Rational sum = Rational.ZERO;
        for (T p : predecessors) {
            sum = sum.plus(map.apply(p));
        }
        return sum;
    }

    static Rational calcUncertaintyForMax(List<PACTuple> predecessors) {
        Rational unc = Rational.ZERO;
        for (PACTuple p : predecessors) {
            if (unc.isLessThan(p.getUncertainty())) {
                unc = p.getUncertainty();
            }

        }
        return unc;
    }

    static Rational calcUncertaintyForMin(List<PACTuple> predecessors) {
        return calcUncertaintyForMax(predecessors);
    }

    Rational getDisruptionProb();
    Rational getUncertainty();
    Rational getProbabilityDelta();

}
