package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.helpers.TranslationTable;
import addtool.analysisonadd.Visited;
import addtool.semantics.Value;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * general representation of a vertex.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 18.10.15
 */
public abstract class Vertex implements Visited<ADDVisitor>,PAC,java.io.Serializable {
    /**
     * The maximum depth for {@link Vertex#toString()} and {@link Vertex#hashCode()}.
     */
    public static final int HASH_MAX_DEPTH =20;
    /**
     * A List of all successor nodes coming after the vertex.
     */
    private final List<Vertex> successor = new ArrayList<>();
    /**
     * if the Vertex is the root for the attacker.
     */
    private boolean isSinkAttacker = false;
    /**
     * if the Vertex is the root for the defender.
     */
    private boolean isSinkDefender = false;
    /**
     * String name for the action.
     */
    private String name;
    /**
     * String representing the id. Only ints converted to string are save.
     */
    private String id;
    /**
     * The current state of a Vertex.
     * @see Value
     */
    private Value active=Value.UNDECIDED;
    /**
     * Determines if evaluation should be handed to successors.
     * Is set to false for one execution in {@link Vertex#evaluate(List, List)}
     */
    private boolean evalSuccessor = true;
    /**
     * A point for representing in the editor.
     */
    private Point screenLocation=new Point(0,0);

    /**
     * If this Vertex can be specified as root node.
     * Usually true except for {@link Trigger Trigger} and {@link Reset Reset}
     */
    protected boolean canBeRoot =true;

    /**
     * Basic Constructor containing name and id.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     */
    public Vertex (String name,String id){
        if (id == null) {
            throw new IllegalArgumentException("id cannot be null!");
        }
        try{
            Integer.parseInt(id);
        }catch(NumberFormatException ex){
            System.err.println("Warning Vertex ID should be parsable to integer. This may cause trouble later.");
        }
        this.setID(id);
        this.setName(Objects.requireNonNullElse(name, id));
    }
    /**
     * Returns the OperatorName of the Vertex e.g. AND.
     * @return A String for the Operator-name
     * @see TranslationTable
     */
    public abstract String getOperatorName();

    /**
     * Returns the OperatorName of the Vertex e.g. And in the language pack the user chose
     * @return A String for the operator name in the users language
     */
    public String getClearOperatorName(){
        return getResource(getOperatorName());
    }

    /**
     * Returns all Successors of a Vertex.
     * @return a flat copy of all entries
     */
    public List<Vertex> getSuccessors() {
        return new ArrayList<>(successor);
    }


    public boolean canBeRoot() {
        return canBeRoot;
    }
    /**
     * Checks if Vertex has no Successors.
     * @return true if {@link Vertex#successor} is empty
     */
    public boolean isSink() {
        return this.successor.isEmpty();
    }
    public boolean isSinkDefender() {
        return isSinkDefender;
    }
    public boolean isSinkAttacker() {
        return isSinkAttacker;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public Value getValue() {
        return this.active;
    }

    public void setCanBeRoot(boolean canBeRoot) {
        this.canBeRoot = canBeRoot;
    }

    public void setValue(Value s) {
        this.active = s;
    }

    public void setSinkAttacker(boolean t) {
        this.isSinkAttacker = t;
    }

    public void setSinkDefender(boolean t) {
        this.isSinkDefender = t;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setID(String id) {
        this.id = id;
    }

    public Point getLocation(){
        return new Point(screenLocation);
    }
    public void setLocation(Point p){
        screenLocation=new Point(p);
    }

    public abstract List<Vertex> getPredecessors();

    public void addPredecessor(Vertex v){
        addPredecessor(v,true);
    }
    protected abstract void addPredecessor(Vertex v,boolean notify);

    public void removePredecessor(Vertex v){
        removePredecessor(v,true);
    }
    protected abstract void removePredecessor(Vertex v,boolean notify);

    public Vertex replacePredecessor(Vertex oldVertex,Vertex newVertex){
        return replacePredecessor(oldVertex,newVertex,true);
    }
    protected abstract Vertex replacePredecessor(Vertex oldVertex,Vertex newVertex,boolean notify);

    /**
     * Adds a new Successor if v != null and not already inside the list
     * The method works in both directions. v gets set the vertex as predecessor
     * @param v A new vertex that is a successor to the object
     */
    public void addSuccessor(Vertex v) {
        addSuccessor(v,true);
    }
    /**
     * Adds a new Successor if v != null and not already inside the list.
     * @param v A new vertex that is a successor to the object
     * @param notify if the signal should be passed on tho the new successor
     */
    protected void addSuccessor(Vertex v,boolean notify) {
        if (v!=null&&!successor.contains(v)&&!v.equals(this)) {
            successor.add(v);
            if(notify)v.addPredecessor(this);
        }
    }

    /**
     * Removes a specific Successor of the list.
     * The method also removes the vertex from v's predecessors
     * @param v A vertex that should be successor of the used vertex
     */
    public void removeSuccessor(Vertex v) {
        removeSuccessor(v,true);
    }
    /**
     * Removes a specific Successor of the list.
     * The method also removes the vertex from v's predecessors
     * @param v A vertex that should be successor of the used vertex
     * @param notify if the signal should be passed on tho the old successor
     */
    protected void removeSuccessor(Vertex v,boolean notify) {
        if (v!=null&&successor.contains(v)) {
            this.successor.remove(v);
            if(notify)v.removePredecessor(this);
        }
    }

    /**
     * Executes {@link Vertex#addSuccessor(Vertex)} on every object in the list.
     * @param vl A list of vertices to add
     */
    public void addSuccessors(List<Vertex> vl) {
        vl.forEach(this::addSuccessor);
    }

    /**
     * Empties the {@link Vertex#successor}-list.
     */
    public void clearSuccessors() {
        new ArrayList<>(successor).forEach(this::removeSuccessor);
    }

    public Vertex replaceSuccessor(Vertex oldVertex, Vertex newVertex) {
        return replaceSuccessor(oldVertex,newVertex,true);
    }
    protected Vertex replaceSuccessor(Vertex oldVertex, Vertex newVertex,boolean notify) {
        if(newVertex!=null&&successor.contains(oldVertex)){
            int pos=successor.indexOf(oldVertex);
            successor.remove(oldVertex);
            successor.add(pos,newVertex);
            if(notify){
                oldVertex.removePredecessor(this,false);
                newVertex.addPredecessor(this,false);
            }
            return oldVertex;
        }
        return newVertex;
    }

    /**
     * Changes all Predecessors and Successors of this vertex to another.
     * @param v The Vertex to replace this with
     * @see Vertex#replacePredecessor(Vertex, Vertex)
     * @see Vertex#replaceSuccessor(Vertex, Vertex)
     */
    public void replaceWith(Vertex v){
        getSuccessors().forEach(successor->successor.replacePredecessor(this,v));
        getPredecessors().forEach(predecessor->predecessor.replaceSuccessor(this,v));
    }

    /**
     * Helper method for Copy-function.
     * Copies all common properties of a vertex like id, name
     * It does not handle pre/successors
     * Do them manually
     * @param v Vertex of same class as origin
     */
    protected void writeValuesTo(Vertex v) {
        if (!v.getClass().equals(this.getClass())) {
            throw new IllegalArgumentException("Wrong class in writeValuesTo. Objects have to be from same class");
        }
        v.setLocation(this.getLocation());
        //v.addSuccessors(this.getSuccessors());
        v.setSinkAttacker(this.isSinkAttacker());
        v.setSinkDefender(this.isSinkDefender());
    }


    /**
     * Is called by {@link Vertex#toString() toString()} to avoid StackOverflow.
     * @param depth current depth
     * @return A String until the maximum depth
     * @see Vertex#HASH_MAX_DEPTH
     */
    public abstract String saveToString(int depth);


    /**
     * Deep-Copies a Vertex and it's dependencies.
     * @return the new Vertex
     */
    public abstract Vertex copy();

    @Override
    public String toString() {
        return saveToString(0);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }

        Vertex vertex = (Vertex) o;

        if (isSinkAttacker != vertex.isSinkAttacker) {
            return false;
        }
        if (isSinkDefender != vertex.isSinkDefender){
            return false;
        }
        //BasicEvents override this method
        return this.getOperatorName().equals(vertex.getOperatorName());
    }

    @Override
    public int hashCode() {
        return saveHashCode(1);
    }

    /**
     * Is called by {@link Vertex#hashCode() hashCode()} to avoid StackOverflow.
     * @param depth current depth
     * @return An integer as hashCode
     * @see Vertex#HASH_MAX_DEPTH
     */
    public int saveHashCode(int depth){
        int result = (isSinkAttacker ? 1 : 0);
        result = 31 * result + (isSinkDefender ? 1 : 0);
        //BasicEvents override this method
        result = 31 * result + getOperatorName().hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);

        return result;
    }

    /**
     * Evaluates the Vertex based on it's predecessors.
     * Superclass method only passes command to Successors if {@link Vertex#evalSuccessor} is true
     */
    public void evaluate() {
        if (evalSuccessor){
            this.getSuccessors().forEach(Vertex::evaluate);
        }
    }

    /**
     * Concrete method to evaluate sequence on a given Vertex and its children.
     * @param predecessorsOrdered A ordered list of Predecessors to execute
     * @param valuesOrdered The corresponding Values to predecessorsOrdered
     * @return the vertex value after execution
     */
    public Value evaluate(List<Vertex> predecessorsOrdered,List<Value> valuesOrdered){
        this.setValue(Value.UNDECIDED);
        getPredecessors().forEach(v->v.setValue(Value.UNDECIDED));
        for(int i=0;i<predecessorsOrdered.size()&&i<valuesOrdered.size();i++){
            predecessorsOrdered.get(i).setValue(valuesOrdered.get(i));
            //TODO make evalSuccessor more elegant
            evalSuccessor =false;//Avoid evaluation to be pushed to Successors, as we don't care for them
            evaluate();
            evalSuccessor =true;
        }
        return this.getValue();
    }

    /**
     * Returns a List of all BEs in the hierarchy of a vertex.
     * @return A list of BasicEvents (may include duplicates)
     */
    public List<BasicEvent> getDownstreamBEs(){
        List<BasicEvent> out=new ArrayList<>();
        getPredecessors().forEach(v->out.addAll(v.getDownstreamBEs()));
        return out;
    }
}


