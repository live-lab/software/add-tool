package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import addtool.semantics.Value;

/**
 * three-valued logic NOT.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class Nat extends UnaryOp{

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public Nat(String name, String id) {
        super(name,id);
    }

    @Override
    public String getOperatorName() {
        return "Nat";
    }

    /*@Override
    public String toString() {
        return "Nat";
    }*/

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }

    /*@Override
    public void setValue(Value v){
        super.setValue(v);
        if(this.getPredecessor().getValue()==Value.UNDECIDED){
            this.evaluate();
        }
    }*/
    @Override
    public void evaluate() {
        if(this.getPredecessor()==null){
            super.setValue(Value.TRUE);
        }else{
            switch (this.getPredecessor().getValue()) {
                //We need to call super.setValue to avoid infinite loop
                case FALSE, TRUE -> super.setValue(Value.FALSE);
                case UNDECIDED -> super.setValue(Value.TRUE);
            }
        }
        if(this.getValue()!=Value.UNDECIDED) {
            super.evaluate();
        }
    }

    @Override
    public Vertex copy() {
        Nat an=new Nat(this.getName(),this.getId());
        this.writeValuesTo(an);
        return an;
    }
}