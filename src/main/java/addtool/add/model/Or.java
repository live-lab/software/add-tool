package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;
import addtool.timeseries.PACTuple;

import java.util.List;
import java.util.stream.Collectors;

/**
 * three-valued logic OR.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class Or extends NaryOp {

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public Or(String name, String id) {
        super(name, id);
    }

    @Override
    public String getOperatorName() {
        return "Or";
    }

    @Override
    public Rational getDisruptionProb(){
        List<Vertex> pre=getPredecessors();
        return PAC.calcSum(pre, PAC::getDisruptionProb)
                .minus(PAC.calcProduct(pre, PAC::getDisruptionProb)
                        .times(pre.size() - 1));
    }

    @Override
    public Rational getUncertainty(){
        //TODO uncertainty in n dimensions
        List<Vertex> pre=getPredecessors();
        return PAC.calcSumUncertainty(pre).plus(PAC.calcProductUncertainty(getPredecessors().stream().map(v -> new PACTuple(v.getDisruptionProb(), v.getUncertainty(), v.getProbabilityDelta())).collect(Collectors.toList())));
    }

    @Override
    public Rational getProbabilityDelta(){
        return Rational.ONE.minus(PAC.calcProduct(getPredecessors(), p->Rational.ONE.minus(p.getProbabilityDelta())));
    }

    @Override
    public void evaluate() {
        this.setValue(this.getPredecessors().stream()
                        .map(Vertex::getValue)
                        .reduce(Value.FALSE, Value::or));
        super.evaluate();
    }

    @Override
    public Vertex copy() {
        Or an=new Or(this.getName(),this.getId());
        this.writeValuesTo(an);
        return an;
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }
}
