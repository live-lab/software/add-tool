package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;
import addtool.timeseries.PACTuple;

import java.util.List;
import java.util.stream.Collectors;

/**
 * representation of the operator sequential or -> not necessary for functional completeness,
 * but allows more efficient implementation.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class SOr extends NaryOp {
    /**
     * A boolean for {@link SOr#evaluate()} that saves if the sequence is still intact.
     */
    boolean sequenceIntact =true;

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public SOr(String name, String id) {
        super(name,id);
    }

    @Override
    public Rational getDisruptionProb(){
        List<Vertex> pre=getPredecessors();
        return PAC.calcSum(pre, PAC::getDisruptionProb)
                .minus(PAC.calcProduct(pre, PAC::getDisruptionProb)
                .times(pre.size()-1));
    }
    @Override
    public Rational getUncertainty(){
        //TODO uncertainty in n dimensions
        List<Vertex> pre=getPredecessors();
        return PAC.calcSumUncertainty(pre).plus(PAC.calcProductUncertainty(getPredecessors().stream().map(v -> new PACTuple(v.getDisruptionProb(), v.getUncertainty(), v.getProbabilityDelta())).collect(Collectors.toList())));
    }
    @Override
    public Rational getProbabilityDelta(){
        return Rational.ONE.minus(PAC.calcProduct(getPredecessors(), p->Rational.ONE.minus(p.getProbabilityDelta())));
    }
    @Override
    public boolean isSequential(){return true;}
    @Override
    public String getOperatorName() {
        return "SOr";
    }

    @Override
    public void setValue(Value s){
        super.setValue(s);
        if(s==Value.UNDECIDED){
            sequenceIntact =true;
        }
    }

    // TODO Alternative übernehmen
    @Override
    public void evaluate() {
        //reset sequence boolean if none are active
        if(this.getPredecessors().stream().allMatch(v->v.getValue()==Value.UNDECIDED)){
            this.setValue(Value.UNDECIDED);
        }else if(!sequenceIntact){
            this.setValue(Value.FALSE);
        }else{
            List<Vertex> predecessors=this.getPredecessors();
            int state=0;//0=fresh, 1=seq_na, 2=seq_fail, 3=seq_succeed, 50=success 98=na after fails, 99=fail
            for (Vertex pre :predecessors){
                switch(pre.getValue()){
                    case UNDECIDED:
                        if(state==0){
                            state=1;
                        }
                        if(state==2){
                            state=98;
                        }
                        break;
                    case FALSE:
                        if(state==0){
                            state=2;
                        }
                        break;
                    case TRUE:
                        if(state==0){
                            state=3;
                        }
                        if(state==1||state==50||state==98){
                            state=99;//EDIT: CHECK if two success in row really give -1
                        }
                        if(state==2){
                            state=50;
                        }
                        break;
                }
            }
            switch (state) {
                case 0, 1, 98 -> this.setValue(Value.UNDECIDED);
                case 2 -> this.setValue(Value.FALSE);
                case 3, 50 -> this.setValue(Value.TRUE);
                case 99 -> {
                    this.setValue(Value.FALSE);
                    sequenceIntact = false;
                }
            }
        }
        super.evaluate();
    }

    @Override
    public Vertex copy() {
        SOr an=new SOr(this.getName(),this.getId());
        this.writeValuesTo(an);
        return an;
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }
}
