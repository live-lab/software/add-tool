package addtool.add.model;

import addtool.add.operators.Comparison;
import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;

import java.util.Arrays;

/**
 * representation of the operator Cost, equipped with operators and bounds.
 *
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 18.10.15
 */
public class Cost extends UnaryOp {

    /**
     * Cost bounds for the operator.
     */
    private Rational[] bounds;
    /**
     * The comparison operators for the bounds.
     * @see Comparison
     */
    private Comparison[] operators;

    /**
     * Constructs a new cost-operator.
     * @param name name for the vertex
     * @param bound set the bounds for the cost gate
     * @param op set the operators for comparing bounds with actual value
     * @param id id for the vertex
     * @see Vertex#Vertex(String, String)
     */
    public Cost(String name, Rational[] bound, Comparison[] op, String id) {
        super(name,id);

        // check for correct state of bounds and operators
        if (bound == null || op == null) {
            throw new IllegalArgumentException("Bound and Comparison cannot be null!");
        }
        if (bound.length != op.length) {
            throw new IllegalArgumentException("Bound and Comparison do not have the same size!");
        }

        this.bounds = Arrays.copyOf(bound,bound.length);
        this.operators = Arrays.copyOf(op,op.length);
    }

    @Override
    public String getOperatorName() {
        return "Cost";
    }

    public Comparison[] getOp() {
        return Arrays.copyOf(operators,operators.length);
    }

    public Rational[] getBound() {
        return Arrays.copyOf(bounds,bounds.length);
    }

    public void setOp(Comparison[] op) {
        this.operators = Arrays.copyOf(op,op.length);
    }

    public void setBound(Rational[] bound) {
        this.bounds = Arrays.copyOf(bound,bound.length);
    }


    @Override
    public String toString() {
        return "Cost(" + Arrays.toString(operators) + ")(" + getPredecessor() + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)){
            return false;
        }

        Cost cost = (Cost) o;

        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(bounds, cost.bounds)) {
            return false;
        }
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(operators, cost.operators);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(bounds);
        result = 31 * result + Arrays.hashCode(operators);
        return result;
    }

    @Override
    public void evaluate() {
        this.setValue(this.getPredecessor().getValue());
        System.err.println("Cost does nothing");
        if(this.getValue()!= Value.UNDECIDED) {
            super.evaluate();
        }
    }

    @Override
    public Vertex copy() {
        Cost an=new Cost(this.getName(),this.getBound(),this.getOp(),this.getId());
        this.writeValuesTo(an);
        return an;
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }
}
