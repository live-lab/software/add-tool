package addtool.add.model;

public enum TriggerState {
    /**
     * can be triggered but is not yet.
     */
    TRIGGER_ABLE,
    /**
     * can be triggered and is triggered.
     */
    TRIGGER_ABLE_TRIGGERED,
    /**
     * Does not need to be triggered.
     */
    NOT_TRIGGER_ABLE
}
