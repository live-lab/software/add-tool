package addtool.add.model;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.jscience.mathematics.number.Rational;

import java.util.List;

/**
 * common superclass for unary operators Cost, Not, IVR, Trigger and Reset.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 18.10.15
 */
public abstract class UnaryOp extends Vertex  {

    /**
     * The one and only predecessor of this operator.
     */
    private Vertex predecessor=null;

    /**
     * Uses Superclass constructor.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public UnaryOp(String name,String id){
        super(name,id);
    }

    public Vertex getPredecessor() {
        return predecessor;
    }

    @Override
    public List<Vertex> getPredecessors() {
        if(getPredecessor()==null)return List.of();
        return List.of(getPredecessor());
    }

    @Override
    public Rational getDisruptionProb(){
        if(getPredecessor()==null){
            return Rational.ONE;
        }
        return getPredecessor().getDisruptionProb();
    }

    @Override
    public Rational getUncertainty(){
        if(getPredecessor()==null){
            return Rational.ZERO;
        }
        return getPredecessor().getUncertainty();
    }

    @Override
    public Rational getProbabilityDelta(){
        if(getPredecessor()==null){
            return Rational.ONE;
        }
        return getPredecessor().getProbabilityDelta();
    }

    public void setPredecessor(@NonNull Vertex v) {
        if(v==null){
            return;
        }
        this.predecessor = v;
    }

    public void clearPredecessor() {
        this.predecessor = null;
    }

    @Override
    public void addPredecessor(Vertex v, boolean notify) {
        if(v==null||v.equals(predecessor))return;

        if(getPredecessor()!=null){
            removePredecessor(predecessor,true);
        }
        setPredecessor(v);
        if(notify){
            v.addSuccessor(this,false);
        }
    }

    @Override
    public void removePredecessor(Vertex v,boolean notify) {
        if(v.equals(this.getPredecessor())){
            clearPredecessor();
            if(notify){
                v.removeSuccessor(this,false);
            }
        }
    }

    @Override
    protected Vertex replacePredecessor(Vertex oldVertex, Vertex newVertex, boolean notify) {
        if(this.getPredecessor().equals(oldVertex)){
            setPredecessor(newVertex);
            if(notify){
                oldVertex.removeSuccessor(this,false);
                newVertex.addSuccessor(this,false);
            }
            return oldVertex;
        }else{
            return newVertex;
        }
    }
    /*
    @Override
    public void accept(ADDVisitor visitor) {
        predecessor.accept(visitor);
        visitor.visit(this);
    } */

    @Override
    public String toString() {
        return this.getOperatorName() + '(' + predecessor + ')';
    }

    @Override
    public String saveToString(int depth) {
        if(depth> HASH_MAX_DEPTH){
            return "...";
        }
        return this.getOperatorName() + '(' + (predecessor==null?"null":predecessor.saveToString(depth+1)) + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (!super.equals(o)){
            return false;
        }


        UnaryOp unaryOp = (UnaryOp) o;
                                    //switched equals to hashcode to avoid stack overflow
                                    //predecessor.hashCode()==(unaryOp.predecessor.hashCode())
        return unaryOp.hashCode()==this.hashCode();
    }

    @Override
    public int saveHashCode(int depth) {
        int result = super.saveHashCode(depth);
        result = 31 * result +
                (predecessor != null&&depth<= HASH_MAX_DEPTH ?
                predecessor.saveHashCode(depth+1) :
                0);
        return result;
    }
    @Override
    protected void writeValuesTo(Vertex v) {
        super.writeValuesTo(v);
    }
}
