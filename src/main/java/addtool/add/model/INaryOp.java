package addtool.add.model;

import java.util.Collection;

/**
 * General interface for NaryOperators.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
@Deprecated
public interface INaryOp extends IVertex {

    IVertex getPredecessor01();

    void setPredecessor01(IVertex v1);

    IVertex getPredecessor02();

    void setPredecessor02(IVertex v2);

    IVertex getPredecessor(int i);

    void setPredecessor(int i, IVertex vertex);

    void addPredecessors(Collection<IVertex> predecessors);
}
