package addtool.add.model;

import org.jscience.mathematics.number.Rational;

/**
 * general interface for basic events.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
@Deprecated
public interface IBasicEvent extends IVertex {

    Rational getSuccessProbability();

    void setSuccessProbability(Rational d);

    Rational getCost();

    void setCost(Rational d);

    boolean isTriggerable();

    void setTriggerable(boolean b);

    boolean isResettable();

    void setResettable(boolean b);
}
