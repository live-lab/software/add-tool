/**
 * This package holds all components of ADDs.
 * @see addtool.add.model.ADD
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 5.12.2018
 */
package addtool.add.model;