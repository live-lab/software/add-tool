package addtool.add.model;

import addtool.analysisonadd.ADDVisitor;
import addtool.analysisonadd.Direction;
import addtool.semantics.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * representation of the operator reset together with its reset edges.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public class Reset extends UnaryOp {
    /**
     * A list of all basic events to reset.
     */
    private List<BasicEvent> toReset = new ArrayList<>();
    /**
     * The order of the basic events.
     */
    private List<BasicEvent> order;
    /**
     * Uses Superclass constructor
     * and sets {@link Vertex#setCanBeRoot(boolean)} to false.
     * @param name A name for the Vertex. If null, id will be used
     * @param id The id for the vertex. should be unique in an ADD and parsable to an integer
     * @see Vertex#Vertex(String, String)
     */
    public Reset(String name, String id) {
        super(name,id);
        setCanBeRoot(false);
        order=new ArrayList<>();
    }

    @Override
    public String getOperatorName() {
        return "Reset";
    }

    /**
     * Returns events to be reset by this node.
     * @return A flat copy of the {@link Reset#toReset} list
     */
    public List<BasicEvent> getToReset() {
        return new ArrayList<>(toReset);
    }

    /**
     * Returns order list.
     * @return A flat copy of the {@link Reset#order} list
     */
    public List<BasicEvent> getOrder() {
        return new ArrayList<>(order);
    }

    /**
     * Adds a Basic Event to the {@link Reset#toReset} list, if it is not contained yet.
     * @param v The basic event to add
     */
    public void addToReset(BasicEvent v) {
        if (!toReset.contains(v)) {
            this.toReset.add(v);
        }
    }

    /**
     *  Sets the List {@link Reset#order}.
     * @param order A list of basic events
     */
    public void setOrder(List<BasicEvent> order) {
        this.order = new ArrayList<>(order);
    }

    /**
     *  Empties the {@link Reset#toReset} list.
     */
    public void clearToReset() {
        toReset.clear();
    }

    @Override
    public String toString() {
        return "Reset(" + getPredecessor() + ')';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)){
            return false;
        }

        Reset reset = (Reset) o;

        if (!Objects.equals(toReset, reset.toReset)) {
            return false;
        }
        return Objects.equals(order, reset.order);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (toReset != null ? toReset.hashCode() : 0);
        result = 31 * result + (order != null ? order.hashCode() : 0);
        return result;
    }

    @Override
    public void evaluate() {
        if(this.getPredecessor().getValue()== Value.TRUE){
            this.setValue(Value.TRUE);
            this.toReset.forEach(b->{
                b.setValue(Value.UNDECIDED);
                if(b.getTrigger_state()== TriggerState.TRIGGER_ABLE_TRIGGERED) {
                    b.setTrigger_state(TriggerState.TRIGGER_ABLE);
                }
            });
            //Make evaluation after all Nodes were reset
            this.toReset.forEach(Vertex::evaluate);
        }
        //TODO check if false results in false
        super.evaluate();
    }


    @Override
    public Vertex copy() {
        Reset an=new Reset(this.getName(),this.getId());
        an.toReset= new ArrayList<>(this.toReset);
        an.order= new ArrayList<>(this.order);
        this.writeValuesTo(an);
        return an;
    }

    @Override
    public void accept(ADDVisitor visitor) {
        accept(visitor, Direction.DOWN);
    }

    @Override
    public void accept(ADDVisitor visitor, Direction direction) {
        switch (direction) {
            case DOWN -> {
                this.getPredecessors().forEach(v -> v.accept(visitor, direction));
                visitor.visit(this);
            }
            case UP -> {
                visitor.visit(this);
                this.getSuccessors().forEach(v -> v.accept(visitor, direction));
            }
        }
    }
}

