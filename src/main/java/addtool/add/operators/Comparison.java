package addtool.add.operators;

import addtool.helpers.TranslationTable;
import org.jscience.mathematics.number.Rational;

/**
 * operators used to compare costs to bounds at cost operators.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 18.10.15
 */
public enum Comparison {

    LEQ {
        @Override
        public String toString() {
            return TranslationTable.LEQ.get(0);
        }
    }, LE {
        @Override
        public String toString() {
            return TranslationTable.LE.get(0);
        }
    }, GEQ {
        @Override
        public String toString() {
            return TranslationTable.GEQ.get(0);
        }
    }, GE {
        @Override
        public String toString() {
            return TranslationTable.GE.get(0);
        }
    }, EQUAL {
        @Override
        public String toString() {
            return TranslationTable.EQ.get(0);
        }
    }, NEQ {
        @Override
        public String toString() {
            return TranslationTable.NEQ.get(0);
        }
    };


    @Override
    public abstract String toString();

    /**
     * evaluates a given comparison on two given rational numbers.
     *
     * @param c  the comparison to evaluate
     * @param r1 the first (left) operand of the comparison
     * @param r2 the second (right) operand of the comparison
     * @return the boolean evaluation of comparison on the two rational numbers
     */
    public static boolean evaluate(Comparison c, Rational r1, Rational r2) {
        return switch (c) {
            case LEQ -> (r1.equals(r2) || r1.isLessThan(r2));
            case LE -> r1.isLessThan(r2);
            case GEQ -> (r1.equals(r2) || r1.isGreaterThan(r2));
            case GE -> r1.isGreaterThan(r2);
            case EQUAL -> r1.equals(r2);
            case NEQ -> !r1.equals(r2);
            default -> throw new IllegalArgumentException("No such comparison defined!");
        };

    }
}
