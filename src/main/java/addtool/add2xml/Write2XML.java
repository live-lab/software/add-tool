package addtool.add2xml;

import addtool.add.model.*;
import addtool.helpers.Properties;
import org.jscience.mathematics.number.Rational;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.*;
import java.util.stream.Collectors;

public final class Write2XML {
    public static final String DOMAIN_COST = "MinCost1";
    public static final String DOMAIN_PROB = "ProbSucc2";
    public static final String ADT_Prob = "lu.uni.adtool.domains.adtpredefined.ProbSucc";
    public static final String ADT_COST = "lu.uni.adtool.domains.adtpredefined.MinCost";
    public static final String SAT_REAL = "lu.uni.adtool.domains.custom.SandRealDomain";
    /**
     * To store cost and probabilities before reforming model.
     */
    private static final Map<String, Rational> probCache = new HashMap<>();
    private static final Map<String, Rational> costCache = new HashMap<>();

    private Write2XML() {
    }

    /**
     * This Method changes all negation constellations to counter-measures and adds them to the lower layer.
     * @param add The model to reform
     * @return the reformed model
     */
    public static ADD reformModel(ADD add) {
        probCache.clear();
        costCache.clear();
        ADD model = ADDBuilder.copyADD(add);
        Map<String, Vertex> id2vertex = model.getId2Vertex();
        model.getNoSuccessor().forEach(v -> iterateNot(id2vertex, v));
        Set<Vertex> noSuccessors = id2vertex.values().stream().
                filter(v -> v.getSuccessors().isEmpty()).collect(Collectors.toSet());

        return new ADD(id2vertex, noSuccessors, true);
    }

    private static Map<String, Vertex> iterateNot(Map<String, Vertex> id2vertex, Vertex v) {
        if (v == null) return id2vertex;
        if (v instanceof Not || v instanceof NotTrue) {
            List<Vertex> successors = v.getSuccessors();
            if (successors.stream().anyMatch(v2 -> !(v2 instanceof And) || v2.getPredecessors().size() > 2)) {
                System.err.println("Reformulation in Countermeasure was ambiguous and is skipped. At: " + v.getId());
                System.err.println(successors);
                successors.get(0).getPredecessors().stream().distinct().forEach(System.err::println);
                return id2vertex;
            }
            if (((UnaryOp) v).getPredecessor() == null) {
                System.err.println("NOT " + v.getId() + " does not have a predecessor! Is this a valid model?");
                return id2vertex;
            }

            //Creating CM Vertex
            String id = ADDBuilder.getFreeID(id2vertex, new Random());
            Countermeasure cm = new Countermeasure(id, id);
            id2vertex.put(id, cm);
            //Replacing the Not by the CM
            Vertex vp = ((UnaryOp) v).getPredecessor();
            v.replaceWith(cm);
            cleanConnections(id2vertex, v);

            //Now Check for new Successor node
            for (Vertex successor : cm.getSuccessors()) {
                successor.getPredecessors().stream().filter(p -> !p.getId().equals(cm.getId())).
                        forEach(p -> {
                            Vertex predecessor = p;
                            if (p instanceof BasicEvent) {
                                //save p and costs for later output
                                probCache.put(p.getId(), p.getDisruptionProb());
                                costCache.put(p.getId(), ((BasicEvent) p).getCost());
                                //Basic Events can not have predecessors. ADTool treats them as Or with a countermeasure attached
                                predecessor = new Or(p.getName(), p.getId());
                                p.replaceWith(predecessor);
                                cleanConnections(id2vertex, p);
                                id2vertex.put(p.getId(), predecessor);
                            }
                            predecessor.addPredecessor(cm);
                            predecessor.removeSuccessor(successor);
                            predecessor.addSuccessors(successor.getSuccessors());
                        });
                cm.removeSuccessor(successor);
                successor.clearSuccessors();
                cleanConnections(id2vertex, successor);
            }

            return iterateNot(id2vertex, vp);
        } else {
            Collection<Vertex> predecessors = v.getPredecessors();
            Map<String, Vertex> id2vertexIterate = id2vertex;
            for (Vertex v2 : predecessors) {
                id2vertexIterate = iterateNot(id2vertexIterate, v2);
            }
            return id2vertexIterate;
        }
    }

    private static void cleanConnections(Map<String, Vertex> id2vertex, Vertex v) {
        id2vertex.remove(v.getId());
    }

    public static void write2XML(ADD add, Document document) {
        add = reformModel(add);
        Element root;
        if (Properties.isADT(add)) {
            root = document.createElement("adtree");
        } else if (Properties.isSAT(add)) {
            root = document.createElement("sandtree");
        } else {
            throw new UnsupportedOperationException("ADD " + add + " \n cannot be specified in xml for ADTool.");
        }
        document.appendChild(root);
        handleVertex(add.getTop(), root, document);
        appendDomains(add, root, document);
    }

    private static void appendDomains(ADD add, Element root, Document document) {
        if (Properties.isADT(add)) {
            /*
            	<domain id="ProbSucc3">
                    <class>lu.uni.adtool.domains.adtpredefined.ProbSucc</class>
                    <tool>ADTool2</tool>
                </domain>
                <domain id="MinCost1">
                    <class>lu.uni.adtool.domains.adtpredefined.MinCost</class>
                    <tool>ADTool2</tool>
                </domain>
             */
            Element label_prob = document.createElement("domain");
            Attr attributeDomain = document.createAttribute("id");
            attributeDomain.setValue(DOMAIN_PROB);
            label_prob.setAttributeNode(attributeDomain);

            label_prob.appendChild(createSimpleNode(document, "class", ADT_Prob));
            label_prob.appendChild(createSimpleNode(document, "tool", "ADTool2"));

            root.appendChild(label_prob);


            label_prob = document.createElement("domain");
            attributeDomain = document.createAttribute("id");
            attributeDomain.setValue(DOMAIN_COST);
            label_prob.setAttributeNode(attributeDomain);

            label_prob.appendChild(createSimpleNode(document, "class", ADT_COST));
            label_prob.appendChild(createSimpleNode(document, "tool", "ADTool2"));

            root.appendChild(label_prob);
        } else if (Properties.isSAT(add)) {
            /*
            	<domain id="ProbSucc2">
                    <class>lu.uni.adtool.domains.custom.SandRealDomain</class>
                    <tool>ADTool2</tool>
                    <name>Proba</name>
                    <description>Probability of failure</description>
                    <or>(x + y) - x * y</or>
                    <and>x * y</and>
                    <sand>x * y</sand>
                    <defaultValue>0.0</defaultValue>
                </domain>
             */
            Element label_prob = document.createElement("domain");
            Attr attributeDomain = document.createAttribute("id");
            attributeDomain.setValue(DOMAIN_PROB);
            label_prob.setAttributeNode(attributeDomain);

            label_prob.appendChild(createSimpleNode(document, "class", SAT_REAL));
            label_prob.appendChild(createSimpleNode(document, "tool", "ADTool2"));
            label_prob.appendChild(createSimpleNode(document, "name", "Success Probability"));
            label_prob.appendChild(createSimpleNode(document, "description", "Probability of Success"));
            label_prob.appendChild(createSimpleNode(document, "or", "(x + y) - x * y"));
            label_prob.appendChild(createSimpleNode(document, "and", "x * y"));
            label_prob.appendChild(createSimpleNode(document, "sand", "x * y"));
            label_prob.appendChild(createSimpleNode(document, "defaultValue", "0.0"));

            root.appendChild(label_prob);
            /*
                <domain id="MinCost1">
                    <class>lu.uni.adtool.domains.custom.SandRealDomain</class>
                    <tool>ADTool2</tool>
                    <name>Proba</name>
                    <description>Probability of failure</description>
                    <or>min(x,y)</or>
                    <and>x + y</and>
                    <sand>x + y</sand>
                    <defaultValue>0.0</defaultValue>
                </domain>
             */

            label_prob = document.createElement("domain");
            attributeDomain = document.createAttribute("id");
            attributeDomain.setValue(DOMAIN_COST);
            label_prob.setAttributeNode(attributeDomain);

            label_prob.appendChild(createSimpleNode(document, "class", SAT_REAL));
            label_prob.appendChild(createSimpleNode(document, "tool", "ADTool2"));
            label_prob.appendChild(createSimpleNode(document, "name", "Minimum Cost"));
            label_prob.appendChild(createSimpleNode(document, "description", "Minimal Cost"));
            label_prob.appendChild(createSimpleNode(document, "or", "min(x,y)"));
            label_prob.appendChild(createSimpleNode(document, "and", "x + y"));
            label_prob.appendChild(createSimpleNode(document, "sand", "x + y"));
            label_prob.appendChild(createSimpleNode(document, "defaultValue", "0.0"));

            root.appendChild(label_prob);
        }
    }

    private static void handleVertex(Vertex vertex, Element root, Document document) {
        Element child = document.createElement("node");
        //System.out.println("Writing: "+vertex.getOperatorName()+"§"+vertex.getId());
        if (vertex instanceof Countermeasure) {
            Attr counter = document.createAttribute("switchRole");
            counter.setValue("yes");
            child.setAttributeNode(counter);
            vertex = ((Countermeasure) vertex).getPredecessor();
        }
        //if (!vertex.getId().equals(vertex.getName())) {
        //As label is needed in AddTool, we always set it
        Element label = document.createElement("label");
        label.appendChild(document.createTextNode(vertex.getName().equals(vertex.getId()) ? vertex.getOperatorName() : vertex.getName()));//
        child.appendChild(label);
        if (vertex instanceof BasicEvent) {
            child.appendChild(createDomainLabel(document, DOMAIN_PROB, vertex.getDisruptionProb()));
            child.appendChild(createDomainLabel(document, DOMAIN_COST, ((BasicEvent) vertex).getCost()));
        } else if (probCache.containsKey(vertex.getId())) {
            //Write the values we preserved earlier
            child.appendChild(createDomainLabel(document, DOMAIN_PROB, probCache.get(vertex.getId())));
            child.appendChild(createDomainLabel(document, DOMAIN_COST, costCache.get(vertex.getId())));
        }
        //}
        Attr attribute = document.createAttribute("refinement");
        attribute.setValue(refinement(vertex));
        child.setAttributeNode(attribute);
        root.appendChild(child);

        for (Vertex v : vertex.getPredecessors()) {
            handleVertex(v, child, document);
        }

    }

    private static String refinement(Vertex vertex) {
        String s = "";

        if (vertex instanceof SAnd) {
            s = "sequential";
        } else if (vertex instanceof And) {
            s = "conjunctive";
        } else if (vertex instanceof Or) {
            s = "disjunctive";
        } else if (vertex instanceof BasicEvent) {
            s = "disjunctive";
        } else if (vertex instanceof Countermeasure) {
            //treat differently
        } else {
            System.out.println("ERROR: " + vertex.getId() + '|' +
                    vertex.getOperatorName() + '|' +
                    vertex.getSuccessors().size() + '|' +
                    vertex.getPredecessors().size() + '|' + vertex.getPredecessors().stream().
                    map(v -> v.getId() + '#' + v.getOperatorName()).collect(Collectors.joining(",")));
            throw new IllegalArgumentException("This ADD cannot be translated Class: " + vertex.getClass());
        }

        return s;
    }

    private static Element createDomainLabel(Document document, String domain, Rational value) {
        Element label_prob = document.createElement("parameter");
        Attr attributeDomain = document.createAttribute("domainId");
        attributeDomain.setValue(domain);
        label_prob.setAttributeNode(attributeDomain);
        Attr attributeCategory = document.createAttribute("category");
        attributeCategory.setValue("basic");
        label_prob.setAttributeNode(attributeCategory);
        label_prob.appendChild(document.createTextNode(String.valueOf(value.doubleValue())));
        return label_prob;
    }

    private static Element createSimpleNode(Document document, String label, String content) {
        Element node = document.createElement(label);
        node.appendChild(document.createTextNode(content));
        return node;
    }

}
