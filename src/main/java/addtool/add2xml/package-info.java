/**
 * Transforms ADD to XML Export
 * See Write2XML.write2XML
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 17.10.15
 */
package addtool.add2xml;