package addtool.add2xml;

import addtool.add.model.ADD;
import addtool.dot2add.*;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Main class for conversion to xml.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 14.04.2019
 * @see Write2XML
 */
public final class MainDot2XML {

    private MainDot2XML() {
    }

    public static void main(String... args) throws ParserConfigurationException, NoValueSpecifiedException, WrongStructureADDException, WrongValueForLabelOperatorException, WrongValueForSinkException, TransformerException, IOException {
        String infile=Path.of("test","Casestudy","casestudy_falconx_v3.dot").toString();
        if(args.length>=1){
            infile=args[0];
        }
        String outfile=infile+".xml";
        if(args.length>=2){
            outfile=args[1];
        }
        FileReader in = new FileReader(infile);
        ADD add = ParserDOT.parseADD(in);

        // create DocumentBuilder
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        //create document
        Document document = docBuilder.newDocument();
        Write2XML.write2XML(add, document);

        //bring document to shape and stream to file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File(outfile));
        transformer.transform(source, result);
    }
}
