package addtool.add2xml;

import addtool.add.io.ADDExport;
import addtool.add.model.ADD;
import addtool.helpers.TranslationTable;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class ADD2XML implements ADDExport {
    @Override
    public String getName() {
        return TranslationTable.XML;
    }

    @Override
    public String getFileExtension() {
        return ".xml";
    }

    @Override
    public void exportModel(ADD model, File f) throws javax.xml.parsers.ParserConfigurationException, javax.xml.transform.TransformerException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document document = docBuilder.newDocument();
        Write2XML.write2XML(model, document);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        // pretty print XML
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(f);
        transformer.transform(source, result);
    }
}
