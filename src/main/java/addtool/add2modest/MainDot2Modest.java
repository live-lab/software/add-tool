package addtool.add2modest;

import addtool.add.model.ADD;
import addtool.dot2add.ParserDOT;

import java.io.FileReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * starting point to translate dot files specifying ADDs syntactically to modest representing ADD semantics.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 15.03.15
 */
public final class MainDot2Modest {

    private MainDot2Modest() {
    }

    public static void main(String... args) throws addtool.dot2add.WrongValueForLabelOperatorException, addtool.dot2add.NoValueSpecifiedException, addtool.dot2add.WrongStructureADDException, addtool.dot2add.WrongValueForSinkException, java.io.IOException {
        String infile= Path.of("test","Casestudy","casestudy_falconx.dot").toString();
        if(args.length>=1){
            infile=args[0];
        }
        String outfile="simple.modest";
        if(args.length>=2){
            outfile=args[1];
        }
        FileReader in = new FileReader(infile);
        ADD adg = ParserDOT.parseADD(in);
        adg.validateConnections();

        PrintWriter writer = new PrintWriter(outfile, StandardCharsets.UTF_8);
        ADD2Modest.translateADD(adg, writer);
        writer.close();
    }
}
