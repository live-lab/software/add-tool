package addtool.add2modest;

import java.io.PrintWriter;

/**
 * Controls Modest output process.
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 10/12/15
 */
final class ModestCodeController {

    private static final String SEPARATION = "  ";

    private ModestCodeController() {
    }

    public static void writeModestCode2File(PrintWriter writer) {
        writer.println("process Controller(){");
        writer.println("bool canPropagate = false;");
        writer.println("bool doBreak = false;");
        writer.println();
        writer.println("urgent(true) " + ModestCodeDeclaration.getActionPropagate() + ';');
        writer.println("do{");
        writer.println(SEPARATION + "::urgent(true) " + ModestCodeDeclaration.getActionTriggersDone() + " {= doBreak=false =};");
        writer.println(SEPARATION + "  urgent(true) " + ModestCodeDeclaration.getActionResetsDone() + ';');
        writer.println(SEPARATION + "   when(wantToSchedule) urgent(wantToSchedule) propagate {= wantToSchedule=false =}");
        writer.println(SEPARATION + '}'); // closing bracket outer do
        writer.println(SEPARATION + SEPARATION + '}'); // closing bracket process
    }
}