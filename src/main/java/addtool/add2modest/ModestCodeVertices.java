package addtool.add2modest;

import addtool.add.model.*;
import addtool.helpers.Values2String;
import addtool.semantics.Value;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * method to generate modest process per State, straightforward translation of STAs to Modest using Modest semantics.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 9/16/15
 */
final class ModestCodeVertices {

    private static final String SEPARATION = "  ";
    private static final String UT = "urgent(true) ";

    private ModestCodeVertices() {
    }

    /*private static void writeModestCode2File(PrintWriter writer, And and) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(and) + "(){");
        writer.println("do{");
        writer.println(indent(1) + "::alt{");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getTrueAction(and.getPredecessor01()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(and.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(and.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(and.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(and));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getFalseAction(and.getPredecessor01()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(and.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(and.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(and.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getUndefinedAction(and.getPredecessor01()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(and.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(and.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(and.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(and));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getTrueAction(and.getPredecessor02()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(and.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(and.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(and.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(and));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getFalseAction(and.getPredecessor02()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(and.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(and.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(and.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getUndefinedAction(and.getPredecessor02()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(and.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(and.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getFalseAction(and));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(and.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(and));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(1) + "}"); //bracket alt
        writer.println("}"); //bracket do
        writer.println("}"); //bracket process
    }*/
    private static void writeModestCode2Filerec(PrintWriter writer, Vertex v, ArrayList<Vertex> preds, ArrayList<Value> used, int depth){
        List<Vertex> todo=v.getPredecessors().stream().filter(ve->!preds.contains(ve)).collect(Collectors.toList());
        if(todo.isEmpty()){
            Value val=v.evaluate(preds,used);
            writer.print(' ' +UT + ModestCodeDeclaration.getAction(v,val));
            writer.print(getStateString(v,val));
            writer.println();
            return;
        }
        writer.println();
        writer.println(indent(depth) + (depth==1?"::":"")+"alt{");
        for(Vertex v2:todo){
            for(Value val:Value.values()){
                writer.print(indent(depth+1) + "::" + ModestCodeDeclaration.getAction(v2,val) + ';');
                ArrayList<Vertex> preds2=(ArrayList<Vertex>)preds.clone();
                preds2.add(v2);
                ArrayList<Value> used2=(ArrayList<Value>)used.clone();
                used2.add(val);
                writeModestCode2Filerec(writer, v, preds2, used2, depth+2);
            }
        }
        writer.println(indent(depth) + '}'); //bracket alt
    }
    private static void writeModestCode2Filerec(PrintWriter writer, Vertex v){
        writeModestCode2Filerec(writer,v,new ArrayList<>(),new ArrayList<>(),1);
    }

    /**
     * Converts Node to modest notation.
     * Should work for Unarys as well, but for now just tested on Nnary
     * @param writer Printwriter to output to
     * @param nary Vertex to write
     */
    private static void writeModestCode2File(PrintWriter writer, NaryOp nary) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(nary) + "(){");
        writer.println("do{");
        writeModestCode2Filerec(writer,nary);
        writer.println("}"); //bracket do
        writer.println("}"); //bracket process
    }
    private static void writeModestCode2File(PrintWriter writer, UnaryOp nary) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(nary) + "(){");
        writer.println("do{");
        writeModestCode2Filerec(writer,nary);
        writer.println("}"); //bracket do
        writer.println("}"); //bracket process
    }/*
    private static void writeModestCode2File(PrintWriter writer, Or or) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(or) + "(){");
        writer.println("do{");
        writer.println(indent(1) + "::alt{");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor01()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor01()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getFalseAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor01()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor02()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor02()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getFalseAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor02()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(1) + "}"); //bracket alt
        writer.println("}"); //bracket do
        writer.println("}"); //bracket process
    }
    private static void writeModestCode2File(PrintWriter writer, SOr or) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(or) + "(){");
        writer.println("do{");
        writer.println(indent(1) + "::alt{");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor01()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor01()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getFalseAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor01()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor02()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor02()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor02()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getFalseAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(2) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor02()) + ";");
        writer.println(indent(3) + "alt{");
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getTrueAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getTrueAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getFalseAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(4) + "::" + ModestCodeDeclaration.getUndefinedAction(or.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(or));
        writer.println(indent(3) + "}");
        writer.println();

        writer.println(indent(1) + "}"); //bracket alt
        writer.println("}"); //bracket do
        writer.println("}"); //bracket process
    }*/
    private static void writeModestCode2File(PrintWriter writer, Not not) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(not) + "(){");
        writer.println("do{");
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getTrueAction(not.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getFalseAction(not));
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getFalseAction(not.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getTrueAction(not));
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getUndefinedAction(not.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(not));
        writer.println("}"); // bracket do
        writer.println("}"); //bracket process
    }

    private static void writeModestCode2File(PrintWriter writer, Nat nat) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(nat) + "(){");
        writer.println("do{");
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getTrueAction(nat.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getFalseAction(nat));
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getFalseAction(nat.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getTrueAction(nat));
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getUndefinedAction(nat.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(nat));
        writer.println("}"); // bracket do
        writer.println("}"); //bracket process
    }

    private static void writeModestCode2File(PrintWriter writer, Ivr ivr) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(ivr) + "(){");
        writer.println("do{");
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getTrueAction(ivr.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(ivr));
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getFalseAction(ivr.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getFalseAction(ivr));
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getUndefinedAction(ivr.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getTrueAction(ivr));
        writer.println("}"); // bracket do
        writer.println("}"); // bracket process
    }

    private static void writeModestCode2File(PrintWriter writer, Reset reset) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(reset) + "(){");
        writer.println("bool isTrue = false;");
        writer.println("do{");

        // need to reset
        writer.println(indent(1) + "::when(!isTrue) " + ModestCodeDeclaration.getTrueAction(reset.getPredecessor()) + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getTrueAction(reset) + "{= isTrue=true =};");
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionTriggersDone() + ';');
        for (BasicEvent be : reset.getToReset()) {
            writer.println(indent(2) + UT + ModestCodeDeclaration.getResetAction(be) + ';');
        }
        writer.println(indent(2) + ModestCodeDeclaration.getActionResetsDone());
        writer.println();

        // true but do not reset
        writer.println(indent(1) + "::when(isTrue) " + ModestCodeDeclaration.getTrueAction(reset.getPredecessor()) + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getTrueAction(reset) + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionTriggersDone() + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionResetsDone());

        // false
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getFalseAction(reset.getPredecessor()) + ';' + UT + ModestCodeDeclaration.getFalseAction(reset) + "{= isTrue=false =};");
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionTriggersDone() + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionResetsDone());

        // undefined
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getUndefinedAction(reset.getPredecessor()) + ';' + UT + ModestCodeDeclaration.getUndefinedAction(reset) + "{= isTrue=false =};");
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionTriggersDone() + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionResetsDone());

        writer.println("}"); // bracket do
        writer.println("}"); // bracket process
    }

    private static void writeModestCode2File(PrintWriter writer, Trigger trigger) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(trigger) + "(){");
        writer.println("bool isTrue = false;");
        writer.println("do{");

        // need to trigger
        writer.println(indent(1) + "::when(!isTrue) " + ModestCodeDeclaration.getTrueAction(trigger.getPredecessor()) + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getTrueAction(trigger) + "{= isTrue=true =};");
        for (BasicEvent be : trigger.getToTrigger()) {
            writer.println(indent(2) + UT + ModestCodeDeclaration.getTriggerAction(be) + ';');
        }
        writer.println(indent(2) + ModestCodeDeclaration.getActionTriggersDone());
        writer.println();

        // true but do not trigger
        writer.println(indent(1) + "::when(isTrue) " + ModestCodeDeclaration.getTrueAction(trigger.getPredecessor()) +
                ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getTrueAction(trigger) + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionTriggersDone());

        // false
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getFalseAction(trigger.getPredecessor()) + ';' + UT + ModestCodeDeclaration.getFalseAction(trigger) + "{= isTrue=false =};");
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionTriggersDone());

        // undefined
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getUndefinedAction(trigger.getPredecessor()) + ';' + UT + ModestCodeDeclaration.getUndefinedAction(trigger) + "{= isTrue=false =};");
        writer.println(indent(2) + UT + ModestCodeDeclaration.getActionTriggersDone());
        writer.println("}"); // bracket do
        writer.println("}"); // bracket process
    }

    private static void writeModestCode2File(PrintWriter writer, Cost cost) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(cost) + "(){");
        writer.println("int locked = 0;");
        writer.println("do{");

        // input true
        writer.println(indent(1) + "::when(locked==1) " + ModestCodeDeclaration.getTrueAction(cost.getPredecessor()) + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getTrueAction(cost));
        writer.println(indent(1) + "::when(locked==-1) " + ModestCodeDeclaration.getTrueAction(cost.getPredecessor()) + ';');
        writer.println(indent(2) + UT + ModestCodeDeclaration.getFalseAction(cost));
        writer.println(indent(1) + "::when(locked==0) " + ModestCodeDeclaration.getTrueAction(cost.getPredecessor()) + ';');
        writer.println(indent(2) + "alt{");
        writer.println(indent(3) + "::when(" + Values2String.costConstraint(cost) + ") " + UT + ModestCodeDeclaration.getTrueAction(cost) + " {= locked=1 =}");
        writer.println(indent(3) + "::when(!(" + Values2String.costConstraint(cost) + ")) " + UT + ModestCodeDeclaration.getTrueAction(cost) + " {= locked=-1 =}");
        writer.println(indent(2) + '}');

        // input false
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getFalseAction(cost.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getFalseAction(cost) + "{= locked=0 =}");

        // input undefined
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getUndefinedAction(cost.getPredecessor()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(cost) + "{= locked=0 =}");

        writer.println("}"); // closing bracket do
        writer.println("}"); // bracket process
    }

    private static void writeModestCode2File(PrintWriter writer, If guard) {
        // predecessor 1 is input, predecessor 2 is guard
        writer.println("process " + ModestCodeDeclaration.getProcessName(guard) + "(){");
        writer.println("int locked = 0;");
        writer.println("int valGuard = 0;");
        writer.println("do{");

        // input false
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getFalseAction(guard.getPredecessor01()) + ';');
        writer.println(indent(2) + "alt{");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getFalseAction(guard.getPredecessor02()));
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getTrueAction(guard.getPredecessor02()));
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getUndefinedAction(guard.getPredecessor02()));
        writer.println(indent(2) + "};");
        writer.println(indent(2) + UT + ModestCodeDeclaration.getFalseAction(guard) + " {= locked=0 =}");

        // input undefined
        writer.println(indent(1) + "::" + ModestCodeDeclaration.getUndefinedAction(guard.getPredecessor01()) + ';');
        writer.println(indent(2) + "alt{");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getFalseAction(guard.getPredecessor02()));
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getTrueAction(guard.getPredecessor02()));
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getUndefinedAction(guard.getPredecessor02()));
        writer.println(indent(2) + "};");
        writer.println(indent(2) + UT + ModestCodeDeclaration.getUndefinedAction(guard) + " {= locked=0 =}");

        // input true and locked
        writer.println(indent(1) + "::when(locked!=0) " + ModestCodeDeclaration.getTrueAction(guard.getPredecessor01()) + ';');
        writer.println(indent(2) + "alt{");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getFalseAction(guard.getPredecessor02()));
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getTrueAction(guard.getPredecessor02()));
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getUndefinedAction(guard.getPredecessor02()));
        writer.println(indent(2) + "};");
        writer.println(indent(2) + "alt{");
        writer.println(indent(3) + "::when(locked==1) " + UT + ModestCodeDeclaration.getTrueAction(guard));
        writer.println(indent(3) + "::when(locked==-1) " + UT + ModestCodeDeclaration.getFalseAction(guard));
        writer.println(indent(2) + '}');

        // input true and not locked
        writer.println(indent(1) + "::when(locked==0) " + ModestCodeDeclaration.getTrueAction(guard.getPredecessor01()) + ';');
        writer.println(indent(2) + "alt{");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getFalseAction(guard.getPredecessor02()) + " {= locked=-1 =}");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getTrueAction(guard.getPredecessor02()) + " {= locked=1 =}");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getUndefinedAction(guard.getPredecessor02()) + " {= locked=-1 =}");
        writer.println(indent(2) + "};");
        writer.println(indent(2) + "alt{");
        writer.println(indent(3) + "::when(locked==1) " + UT + ModestCodeDeclaration.getTrueAction(guard));
        writer.println(indent(3) + "::when(locked==-1) " + UT + ModestCodeDeclaration.getFalseAction(guard));
        writer.println(indent(2) + '}');

        //guard first
        writer.println(indent(1) + "::alt{");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getTrueAction(guard.getPredecessor02()) + " {= valGuard=1 =}");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getFalseAction(guard.getPredecessor02()) + " {= valGuard=-1 =}");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getUndefinedAction(guard.getPredecessor02()) + " {= valGuard=0 =}");
        writer.println(indent(2) + "};"); // closing bracket alt
        writer.println(indent(2) + "alt{");
        writer.println(indent(3) + "::when(locked!=0) " + ModestCodeDeclaration.getTrueAction(guard.getPredecessor01()) + ';');
        writer.println(indent(4) + "alt{");
        writer.println(indent(5) + "::when(locked==-1) " + UT + ModestCodeDeclaration.getFalseAction(guard));
        writer.println(indent(5) + "::when(locked==1) " + UT + ModestCodeDeclaration.getTrueAction(guard));
        writer.println(indent(4) + '}');
        writer.println(indent(3) + "::when(locked==0) " + ModestCodeDeclaration.getTrueAction(guard.getPredecessor01()) + ';');
        writer.println(indent(4) + "alt{");
        writer.println(indent(5) + "::when(valGuard==1) " + UT + ModestCodeDeclaration.getTrueAction(guard) + " {= locked=1 =}");
        writer.println(indent(5) + "::when(valGuard!=1) " + UT + ModestCodeDeclaration.getFalseAction(guard) + " {= locked=-1 =}");
        writer.println(indent(4) + '}'); // closing bracket alt
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getFalseAction(guard.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getFalseAction(guard) + " {= locked=0 =}");
        writer.println(indent(3) + "::" + ModestCodeDeclaration.getUndefinedAction(guard.getPredecessor01()) + "; " + UT + ModestCodeDeclaration.getUndefinedAction(guard) + " {= locked=0 =}");
        writer.println("}"); // closing bracket alt

        writer.println("}"); // closing bracket do
        writer.println("}"); // bracket process
    }

    // currently ignoring delaying costs
    private static void writeModestCode2File(PrintWriter writer, BasicEvent be) {
        writer.println("process " + ModestCodeDeclaration.getProcessName(be) + "(){");
        if (be instanceof BasicEventTime) {
            writer.println("clock c;");
            writer.println("real tp;");
            writer.println(UT + "tau {= tp=" + ((BasicEventTime) be).getDistribution() + ", c=0 =};");
        } else {
            writer.println("bool blockPropagate = false;");
        }
        writer.println("do{");

        if (be.isTriggerAble()) {
            writer.println(indent(1) + "do{");
            writer.println(indent(2) + "::" + writeModestCode2FilePropagate(be, 0));
            writer.println(indent(2) + "::" + ModestCodeDeclaration.getTriggerAction(be) + "; " + UT + "break");
            writer.println("};");
            if (be instanceof BasicEventTime) {
                writer.println(indent(1) + UT + "tau {= c=0, tp=" + ((BasicEventTime) be).getDistribution() + " =};");
            }
        }

        writer.println(indent(1) + "alt{");
        if (be instanceof BasicEventTime) {
            writer.println(indent(2) + "::when(c<=tp+" + ModestCodeDeclaration.getEpsilon() + ") " + writeModestCode2FilePropagate(be, 0));
            writer.print(indent(2) + "::when(c>=tp) urgent(c>=tp) tau {= wantToSchedule=true, ");
        } else {
            writer.println(indent(2) + "::when(!blockPropagate)" + writeModestCode2FilePropagate(be, 0));
            writer.print(indent(2) + "::" + ModestCodeDeclaration.getExecutionAction(be) + " {= blockPropagate=true, wantToSchedule=true =};");
            writer.println(indent(3) + UT + "tau" + " {= ");
        }
/*
        for (int i = 0; i < be.getCost().length; i++) {
            writer.print(ModestCodeDeclaration.getVariableCost(i) + "+=" + be.getCost()[i].doubleValue());

            if (i < be.getCost().length - 1) {
                writer.print(", ");
            }
        }*/
        writer.print(ModestCodeDeclaration.getVariableCost() + "+=" + be.getCost().doubleValue());
        writer.println(" =};");

        if (be.isResettable()) {
            writeModestCode2FileCommonResettable(writer, be, 3);
        } else {
            writeModestCode2FileCommonNonResettable(writer, be, 3);
        }

        writer.println("}"); // bracket alt
        writer.println("}"); // bracket do
        writer.println("}"); // bracket process
    }

    private static void writeModestCode2FileCommonNonResettable(PrintWriter writer, BasicEvent be, int initialIndent) {
        writer.println(indent(initialIndent) + "palt{");
        writer.println(indent(initialIndent + 1) + ':' + be.getSuccessProbability().doubleValue() + ":do{" + UT + ModestCodeDeclaration.getActionPropagate() + ';');
        writer.println(indent(initialIndent + 2) + UT + ModestCodeDeclaration.getTrueAction(be) +getStateString(be,Value.TRUE) + '}');
        writer.println(indent(initialIndent + 1) + ":1-" + be.getSuccessProbability().doubleValue() + ":do{" + UT + ModestCodeDeclaration.getActionPropagate() + ';');
        writer.println(indent(initialIndent + 2) + UT + ModestCodeDeclaration.getFalseAction(be) +getStateString(be,Value.FALSE) + '}');
        writer.println(indent(initialIndent) + '}'); // closing bracket palt
    }

    private static String writeModestCode2FilePropagate(BasicEvent be, int initialIndent) {
        return indent(initialIndent) + ModestCodeDeclaration.getActionPropagate() + "; " + UT + ModestCodeDeclaration.getUndefinedAction(be);
    }
    private static String getStateString(Vertex v, Value val){
        if(!v.getSuccessors().isEmpty() ||val==Value.UNDECIDED)return "";
        String out="{= state = "+(val==Value.TRUE?1:-1);
        if(v.isSinkAttacker()){
            out+=", attack_state = "+(val==Value.TRUE?1:-1);
        }
        if(v.isSinkDefender()){
            out+=", def_state = "+(val==Value.TRUE?1:-1);
        }
        return out+" =}";
    }
    private static void writeModestCode2FileCommonResettable(PrintWriter writer, BasicEvent be, int initialIndent) {
        writer.println(indent(initialIndent) + "palt{");
        //success
        writer.println(indent(initialIndent + 1) + ':' + be.getSuccessProbability().doubleValue() + ':' + UT + ModestCodeDeclaration.getActionPropagate() + "; do{" + UT + ModestCodeDeclaration.getTrueAction(be) +
                getStateString(be,Value.TRUE)+ ';');
        writer.println(indent(initialIndent + 1) + "alt{");
        if (be instanceof BasicEventTime) {
            writer.println(indent(initialIndent + 2) + "::" + ModestCodeDeclaration.getResetAction(be) + "; " + UT + "tau" + " {= wantToSchedule=true, c=0, tp=" + ((BasicEventTime) be).getDistribution() + " =}; " + UT + "break");
        } else if (be instanceof BasicEventPlayer) {
            writer.println(indent(initialIndent + 2) + "::" + ModestCodeDeclaration.getResetAction(be) + "; " + UT + "tau" + " {= wantToSchedule=true =}; " + UT + "break");
        }
        writer.println(indent(initialIndent + 2) + "::" + ModestCodeDeclaration.getActionPropagate() + '}'); // closing bracket do
        writer.println(indent(initialIndent + 1) + '}'); //closing bracket alt
        writer.println();
        //failure
        writer.println(indent(initialIndent + 1) + ":1-" + be.getSuccessProbability().doubleValue() + ':' + UT + ModestCodeDeclaration.getActionPropagate() + "; do{" + UT + ModestCodeDeclaration.getFalseAction(be)+
                getStateString(be,Value.FALSE)+ ';');
        writer.println(indent(initialIndent + 1) + "alt{");
        if (be instanceof BasicEventTime) {
            writer.println(indent(initialIndent + 2) + "::" + ModestCodeDeclaration.getResetAction(be) + "; " + UT + "tau" + " {= c=0, tp=" + ((BasicEventTime) be).getDistribution() + ", wantToSchedule=true =}; " + UT + "break");
        } else if (be instanceof BasicEventPlayer) {
            writer.println(indent(initialIndent + 2) + "::" + ModestCodeDeclaration.getResetAction(be) + "; " + UT + "tau" + " {= wantToSchedule=true =}; " + UT + "break");
        }
        writer.println(indent(initialIndent + 2) + "::" + ModestCodeDeclaration.getActionPropagate() + '}'); // closing bracket do
        writer.println(indent(initialIndent + 1) + '}'); //closing bracket alt
        writer.println(indent(initialIndent) + '}'); // closing bracket palt
    }


    public static void writeModestCode2File(PrintWriter writer, Vertex v) {
        if (v instanceof If) {
            ModestCodeVertices.writeModestCode2File(writer, (If) v);
        }else if (v instanceof NaryOp) {
            ModestCodeVertices.writeModestCode2File(writer, (NaryOp) v);
        }

        if (v instanceof Trigger) {
            ModestCodeVertices.writeModestCode2File(writer, (Trigger) v);
        }else if (v instanceof Reset) {
            ModestCodeVertices.writeModestCode2File(writer, (Reset) v);
        }else if (v instanceof Cost) {
            ModestCodeVertices.writeModestCode2File(writer, (Cost) v);
        }else if (v instanceof UnaryOp) {
            ModestCodeVertices.writeModestCode2File(writer, (UnaryOp)v);
        }
        /*if (v instanceof Not) {
            ModestCodeVertices.writeModestCode2File(writer, (Not) v);
        }
        if (v instanceof Nat) {
            ModestCodeVertices.writeModestCode2File(writer, (Nat) v);
        }*/
        if (v instanceof BasicEvent) {
            ModestCodeVertices.writeModestCode2File(writer, (BasicEvent) v);
        }
    }

    private static String indent(int i) {
        return SEPARATION.repeat(Math.max(0, i));
    }

}
