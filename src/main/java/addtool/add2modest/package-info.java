/**
 * Manages ADD to Modest transformation
 * See ADD2Modest.translateADD
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 16.09.15
 */
package addtool.add2modest;