package addtool.add2modest;

import addtool.add.model.Vertex;
import addtool.semantics.Value;

/**
 * collection to generate action and variable declarations and names.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @version 9/16/15
 */
public final class ModestCodeDeclaration {

    private ModestCodeDeclaration() {
    }

    private static String getActionToVertex(Vertex v, String action) {
        return action + getCodeToVertex(v);
    }
    public static String getCodeToVertex(Vertex v) {
        return v.getOperatorName() + (v.getId()).toUpperCase();
    }

    private static String getActionDeclarationToVertex(Vertex v, String action) {
        return "action " + getActionToVertex(v, action) + ';';

    }

    public static String getResetAction(Vertex v) {
        return getActionToVertex(v, "reset");
    }

    public static String getDeclarationTriggerAction(Vertex v) {
        return getActionDeclarationToVertex(v, "trigger");
    }


    public static String getDeclarationResetAction(Vertex v) {
        return getActionDeclarationToVertex(v, "reset");
    }

    public static String getTriggerAction(Vertex v) {
        return getActionToVertex(v, "trigger");
    }

    public static String getDeclarationScheduleAction(Vertex v) {
        return getActionDeclarationToVertex(v, "schedule");
    }

    public static String getScheduleAction(Vertex v) {
        return getActionToVertex(v, "schedule");
    }

    public static String getDeclarationExecutionAction(Vertex v) {
        return getActionDeclarationToVertex(v, "execute");
    }

    public static String getExecutionAction(Vertex v) {
        return getActionToVertex(v, "execute");
    }

    public static String getDeclarationTrueAction(Vertex v) {
        return getActionDeclarationToVertex(v, "tt");
    }

    public static String getTrueAction(Vertex v) {
        return getActionToVertex(v, "tt");
    }
    public static String getAction(Vertex v, Value va) {
        return switch (va) {
            case TRUE -> getTrueAction(v);
            case FALSE -> getFalseAction(v);
            case UNDECIDED -> getUndefinedAction(v);
        };
    }

    public static String getDeclarationUndefinedAction(Vertex v) {
        return getActionDeclarationToVertex(v, "uu");
    }

    public static String getUndefinedAction(Vertex v) {
        return getActionToVertex(v, "uu");
    }

    public static String getDeclarationFalseAction(Vertex v) {
        return getActionDeclarationToVertex(v, "ff");
    }

    public static String getFalseAction(Vertex v) {
        return getActionToVertex(v, "ff");
    }

    public static String getProcessName(Vertex v) {
        return v.getOperatorName() + v.getId().toUpperCase();
    }

    public static String getActionDeclarationResetsDone() {
        return "action " + getActionResetsDone() + ';';
    }

    public static String getActionResetsDone() {
        return "resetsDone";
    }

    public static String getActionDeclarationTriggersDone() {
        return "action " + getActionTriggersDone() + ';';
    }

    public static String getActionCostChanges() {
        return "costChanges";
    }

    public static String getActionDeclarationCostChanges() {
        return "action " + getActionTriggersDone() + ';';
    }

    public static String getActionTriggersDone() {
        return "triggersDone";
    }

    public static String getActionDeclarationPropagate() {
        return "action " + getActionPropagate() + ';';
    }

    public static String getActionPropagate() {
        return "propagate";
    }

    public static String getEpsilon() {
        return "epsilon";
    }

    public static String getEpsilonDeclaration() {
        return "real " + getEpsilon() + " = 0.00001;";
    }

    public static String getVariableCost(int i) {
        return "globalCosts" + i;
    }

    public static String getVariableCost() {
        return "globalCosts";
    }

    public static String getVariableCostDeclaration(int i) {
        return "real " + getVariableCost(i) + "= 0;";
    }
    public static String getVariableCostDeclaration() {
        return "real " + getVariableCost() + "= 0;";
    }
    public static String getStateDeclaration() {
        return "\n" +
                "int state=0;\n" +
                "property P_won = Pmin(<> (state==1));\n" +
                "property P_los = Pmin(<> (state==-1));\n" +
                "int attack_state=0;\n" +
                "int def_state=0;\n" +
                "property Att_won = Pmin(<> (attack_state==1 && def_state!=1 ));\n" +
                "property Def_won = Pmin(<> (def_state==1 && attack_state!=1));\n" +
                "property Both_won = Pmin(<> (def_state==1 && attack_state==1));\n" +
                "property None_won = Pmin(<> (def_state!=1 && attack_state!=1));";
    }


}
