package addtool.add2modest;


import addtool.add.io.ADDExport;
import addtool.add.model.ADD;
import addtool.add.model.BasicEventPlayer;
import addtool.add.model.BasicEvent;
import addtool.add.model.Vertex;
import addtool.helpers.TranslationTable;

import java.io.File;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * starting point to translate ADD syntax to ADD semantics in Modest Terms.
 *
 * @author Julia Kraemer juliadk@mail.upb.de
 * @since 15.03.15
 */
public final class ADD2Modest implements ADDExport {

    public ADD2Modest() {
    }

    public static void translateADD(ADD add, PrintWriter writer) {
        initialisationGlobalVariables(add, writer);
        initialisationActions(add, writer);

        List<BasicEvent> bes = new ArrayList<>();
        for (Vertex v : add) {
            if (v instanceof BasicEvent) {
                bes.add((BasicEvent) v);
            }
        }

        ModestCodeController.writeModestCode2File(writer);

        writeCodeForVertices(add, writer);
        writeCodeForCompleteSTA(add, writer);
    }

    // initialize global variables (epsilon for precision, global variables for timing resets/triggers)
    private static void initialisationGlobalVariables(ADD add, PrintWriter writer) {
        // synchronisation actions
        writer.println("bool wantToSchedule=false;");
        writer.println(ModestCodeDeclaration.getEpsilonDeclaration());
        writer.println(ModestCodeDeclaration.getActionDeclarationPropagate());
        writer.println(ModestCodeDeclaration.getActionDeclarationResetsDone());
        writer.println(ModestCodeDeclaration.getActionDeclarationTriggersDone());
        writer.println();

        if(add.getMaxCostDimension()==1){
            writer.println(ModestCodeDeclaration.getVariableCostDeclaration());
        }else {
            // cost variables
            for (int i = 0; i < add.getMaxCostDimension(); i++) {
                writer.println(ModestCodeDeclaration.getVariableCostDeclaration(i));
            }
        }
        writer.println(ModestCodeDeclaration.getStateDeclaration());
    }

    // defining actions for all the processes
    private static void initialisationActions(ADD add, PrintWriter writer) {
        // actions for the processes
        for (Vertex v : add) {
            writer.println(ModestCodeDeclaration.getDeclarationTrueAction(v));
            writer.println(ModestCodeDeclaration.getDeclarationFalseAction(v));
            writer.println(ModestCodeDeclaration.getDeclarationUndefinedAction(v));

            // basic events can be triggered, scheduled and reset
            if (v instanceof BasicEvent) {
                if (((BasicEvent) v).isTriggerAble()) {
                    writer.println(ModestCodeDeclaration.getDeclarationTriggerAction(v));
                }
                if (((BasicEvent) v).isResettable()) {
                    writer.println(ModestCodeDeclaration.getDeclarationResetAction(v));
                }
                writer.println(ModestCodeDeclaration.getDeclarationScheduleAction(v));

                if (v instanceof BasicEventPlayer) {
                    writer.println(ModestCodeDeclaration.getDeclarationExecutionAction(v));
                }
            }
            writer.println();
        }
    }

    // entrance point for code generation of vertices
    private static void writeCodeForVertices(ADD add, PrintWriter writer) {
        for (Vertex v : add) {
            ModestCodeVertices.writeModestCode2File(writer, v);
            writer.println();
            writer.println();
        }
    }

    // synthesis of central STA scheduling components
    private static void writeCodeForCompleteSTA(ADD add, PrintWriter writer) {
        writer.print("par{::Controller()");
        for (Vertex v : add) {
            writer.print("::" + ModestCodeDeclaration.getProcessName(v) + "()");
        }
        writer.println("}");
    }

    @Override
    public String getName() {
        return TranslationTable.MODEST;
    }

    @Override
    public String getFileExtension() {
        return ".modest";
    }

    @Override
    public void exportModel(ADD model, File f) throws java.io.IOException {
        PrintWriter writer=new PrintWriter(f, StandardCharsets.UTF_8);
        translateADD(model, writer);
        writer.close();
    }
}

