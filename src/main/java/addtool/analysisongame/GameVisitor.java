package addtool.analysisongame;

import addtool.semantics.Game;
import addtool.semantics.State;

public interface GameVisitor {

    void visit(State state);

    Game getGame();

    boolean visited(State state);
}
