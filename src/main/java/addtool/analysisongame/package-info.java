/**
 * Analyses an ADD using the Visitor pattern. At the moment not in use. But may be expanded later.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 16.09.15
 */
package addtool.analysisongame;