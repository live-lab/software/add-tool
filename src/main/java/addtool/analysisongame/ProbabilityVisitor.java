package addtool.analysisongame;

import addtool.add.model.Player;
import addtool.semantics.*;
import org.jscience.mathematics.number.Rational;

import java.util.HashMap;

/**
 * assumes games to be acyclic.
 */
public class ProbabilityVisitor implements GameVisitor {

    private final Game game;
    private final HashMap<State, Rational> values = new HashMap<>();
    private final HashMap<State, Move> moves = new HashMap<>();

    public ProbabilityVisitor(Game game) {
        this.game = game;
    }

    // Calculates and returns the Probability of the root event.
    public Rational getRootProb(){
        this.game.accept(this);
        return this.getValue(this.game.initialState());
    }

    @Override
    public void visit(State state) {
        if (values.containsKey(state)) {
            // already treated this state
        } else if (state.rootValue() == Value.FALSE) {
            values.put(state, Rational.ZERO);
        } else if (state.rootValue() == Value.TRUE) {
            values.put(state, Rational.ONE);
        } else {
            if (state.player == Player.Attacker) {
                Rational max = Rational.ZERO;
                Move best = null;
                for (Move move : state.getAvailableMoves()) {
                    Rational valueMove = computeValue(state, move);
                    if (max.isLessThan(valueMove)) {
                        max = valueMove;
                        best = move;
                    }
                }
                values.put(state, max);
                moves.put(state, best);
            } else {
                Rational min = Rational.ONE;
                Move best = null;
                for (Move move : state.getAvailableMoves()) {
                    Rational valueMove = computeValue(state, move);
                    // TODO greater equals better to avoid the 'null' Move being selected
                    if (min.isGreaterThan(valueMove)) {
                        min = valueMove;
                        best = move;
                    }
                }
                values.put(state, min);
                moves.put(state, best);
            }
        }
    }

    @Override
    public Game getGame() {
        return game;
    }

    @Override
    public boolean visited(State state) {
        return this.values.containsKey(state);
    }


    private Rational computeValue(State state, Move move) {
        Rational probability = Rational.ZERO;
        Distribution<State> dist = state.getSuccessor(move);

        for (State succ : dist) {
            Rational valueSucc = values.get(succ);
            Rational probSucc = dist.lookUp(succ);

            // use Bellmann equation here
            probability = probability.plus(valueSucc.times(probSucc));
        }

        return probability;
    }

    @Override
    public String toString() {
        return "ProbabilityVisitor{" + values + '}';
    }

    public Rational getValue(State state) {
        return values.get(state);
    }
}