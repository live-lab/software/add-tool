package addtool.analysisongame;

import addtool.add.model.BasicEvent;
import addtool.add.model.Player;
import addtool.semantics.*;
import org.jscience.mathematics.number.Rational;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * A visitor analyse cost on an ADD.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 8.11.2018
 */
public class CostVisitor implements GameVisitor {

    private final Game game;
    private final HashMap<State, Rational> values = new HashMap<>();
    private final HashMap<State, Move> moves = new HashMap<>();

    public CostVisitor(Game game) {
        this.game = game;
    }

    // Calculates and returns the Cost of the root event.
    public Rational getRootCost(){
        this.game.accept(this);
        return this.getValue(this.game.initialState());
    }

    @Override
    public void visit(State state) {
        if (values.containsKey(state)) {
            // Already visited
        } else if (state.rootValue() != Value.UNDECIDED) {
            values.put(state, Rational.ZERO);
            moves.put(state, null);
        } else {
            if (state.player == Player.Defender) {
                Rational max = Rational.ZERO;
                Move best = null;
                for (Move move : state.getAvailableMoves()) {
                    Rational valueMove = computeValue(state, move);
                    if (max.isLessThan(valueMove) || max.isZero()) {
                        max = valueMove;
                        best = move;
                    }
                }
                values.put(state, max);
                moves.put(state, best);
            } else {
                Rational min = null;
                Move best = null;
                Set<Move> available = state.getAvailableMoves();
                for (Move move : available) {
                    if (min == null) {
                        min = computeValue(state, move);
                        best = move;
                    } else {
                        Rational valueMove = computeValue(state, move);
                        if (min.isGreaterThan(valueMove)) {
                            min = valueMove;
                            best = move;
                        }
                    }
                }
                values.put(state, min);
                moves.put(state, best);
            }
        }
    }

    @Override
    public Game getGame() {
        return game;
    }

    @Override
    public boolean visited(State state) {
        return this.values.containsKey(state);
    }

    public Map<State, Move> getBest() {
        return new HashMap<>(moves);
    }


    private Rational computeValue(State state, Move move) {
        Distribution<State> dist = state.getSuccessor(move);

        Rational sum = Rational.ZERO;
        for (State succ : dist) {
            Rational valueSucc = values.get(succ);
            Rational probSucc = dist.lookUp(succ);

            sum = sum.plus(valueSucc.times(probSucc));
        }
        if (state.player == Player.Attacker) {
            for (BasicEvent b : move) {
                sum = sum.plus(b.getCost());
            }
        }
        return sum;
    }

    @Override
    public String toString() {
        return "CostVisitor{" + "values=" + values + "\n" +
                ", moves=" + moves +
                '}';
    }


    public Rational getValue(State state) {
        return values.get(state);
    }
}
