package addtool.add2uppaal;

/**
 * Specifies labels for UPPAAL export.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 25.09.2020
 */
public class Label {
    private int x;
    private int y;
    private final String type;
    private final String content;
    private final boolean locationSet;
    public Label(String type,String content,int x,int y){
        this.type=type;
        this.content=content;
        this.x=x;
        this.y=y;
        locationSet =true;
    }
    public Label(String type,String content){
        this.type=type;
        this.content=content;
        locationSet =false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

    public boolean isLocationSet() {
        return locationSet;
    }
}
