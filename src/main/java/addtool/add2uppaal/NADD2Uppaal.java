package addtool.add2uppaal;

import addtool.add.io.ADDExport;
import addtool.add2modest.ModestCodeDeclaration;
import addtool.add.model.*;
import addtool.helpers.TranslationTable;
import org.jscience.mathematics.number.Rational;
import addtool.semantics.Value;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Main class to convert {@link ADD} to Uppaal
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 11.09.2020
 * @see NADD2Uppaal#writeADD2UppaalFile(PrintWriter, ADD, QueryState)
 */
public final class NADD2Uppaal implements ADDExport {
    public NADD2Uppaal() {
    }

    @Override
    public String getName() {
        return TranslationTable.UPPAAL;
    }

    @Override
    public String getFileExtension() {
        return ".uppaal.xml";
    }

    @Override
    public void exportModel(ADD model, File f) throws IOException {
        PrintWriter writer=new PrintWriter(f, StandardCharsets.UTF_8);
        NADD2Uppaal.writeADD2UppaalFile(writer, model, NADD2Uppaal.QueryState.BOTH);
    }

    public enum QueryState{
        SUCCESS,FAILURE,BOTH;
        public static QueryState of(String s){
            if(s.equalsIgnoreCase(SUCCESS.toString())){
                return SUCCESS;
            }
            if(s.equalsIgnoreCase(FAILURE.toString())){
                return FAILURE;
            }
            if(s.equalsIgnoreCase(BOTH.toString())){
                return BOTH;
            }
            return null;
        }
    }
    static AtomicInteger idCounter =new AtomicInteger(0);
    public static final String SYNCHRO="synchronisation";
    public static final String PROBABILITY="probability";
    public static final String ASSIGNMENT="assignment";
    public static final String COST="costvar";
    public static final String GUARD="guard";
    public static final String PROPAGATE="propagate";
    public static final String STEPS="steps";
    public static final String STATE="state_";
    public static final String UPDATE="update_";
    public static final String TIMESTAMP="timestamp_";
    public static final String TRIGGER="trigger_";
    public static final String UPDATE_METHOD="updateSuccs";
    public static final String UPDATE_SELF="updateSelf";
    public static final int STEP=100;
    public static final int STEP_TEXT=30;
    /**
     * scripts.Main Method to convert an ADD in UPPAAL format
     * @param writer Writer to write to. May be a file or something else
     * @param adg The ADD to output
     */
    public static void writeADD2UppaalFile(PrintWriter writer, ADD adg, QueryState qs) {
        if (writer == null || adg == null) {
            throw new IllegalArgumentException("ADD and File cannot be null!");
        }
        idCounter =new AtomicInteger(0);
        writer.println("<nta>"); // begin of graph
        writer.println("<declaration>"); // channel declaration
        adg.getId2Vertex().values().stream().map(NADD2Uppaal::getVarDecStr).forEach(writer::println);
        writer.println("int "+STEPS+"=1;");
        writer.println("double "+COST+"=0.0;");
        writer.println("urgent broadcast chan "+PROPAGATE+ ';');
        writer.println("</declaration>"); // End of declaration

        writer.println();

        adg.getId2Vertex().values().forEach(v->printVertex(writer,v,adg));

        printWon(writer,adg.getNoSuccessor());

        writer.println("<system>"); // System components

        writer.println(adg.getId2Vertex().values().stream().
                map(ModestCodeDeclaration::getCodeToVertex).collect(Collectors.joining(",","system ",",Won;")));

        writer.println("</system>"); // End System components
        writer.println("<queries>"); // Queries
        if(qs.equals(QueryState.SUCCESS)||qs.equals(QueryState.BOTH)) {
            writer.println("\t\t<query>\n" +
                    "\t\t\t<formula>Pr[#&lt;=1000000](&lt;&gt; Won.Success)\n" +
                    "\t\t\t</formula>\n" +
                    "\t\t\t<comment>\n" +
                    "\t\t\t</comment>\n" +
                    "\t\t</query>");
        }
        if(qs.equals(QueryState.FAILURE)||qs.equals(QueryState.BOTH)){
        writer.println("\t\t<query>\n" +
                "\t\t\t<formula>Pr[#&lt;=1000000](&lt;&gt; Won.Failed)\n" +
                "\t\t\t</formula>\n" +
                "\t\t\t<comment>\n" +
                "\t\t\t</comment>\n" +
                "\t\t</query>");
        }
        writer.println("</queries>"); // End of Queries
        writer.println("</nta>"); // End of graph
        writer.close();
    }

    private static void printWon(PrintWriter writer, Set<Vertex> noSuccessors) {

        writer.println("<template>");
        writer.println("\t<name>Won</name>");
        Location init=new Location("id"+ idCounter.getAndIncrement(),0,0);
        Location succ=new Location("id"+ idCounter.getAndIncrement(),-4*STEP,(noSuccessors.size()+4)*STEP_TEXT,"Success");
        Location fail=new Location("id"+ idCounter.getAndIncrement(),4*STEP,(noSuccessors.size()+4)*STEP_TEXT,"Failed");

        writer.println(init.getUppaalDesc());
        writer.println(succ.getUppaalDesc());
        writer.println(fail.getUppaalDesc());
        writer.println(init.getUppaalInit());
        int pos=1;
        for(Vertex nosuc:noSuccessors){
            for(Value val:Value.values()){
                Point p1=new Point(init.getX(), init.getY()+STEP_TEXT*pos);
                if(val==Value.TRUE){
                    Point p2=new Point(succ.getX(), init.getY()+STEP_TEXT*pos);
                    writer.println(init.getUppaalConnection(succ, List.of(new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!',p2.x+STEP_TEXT*6,p1.y),
                            new addtool.add2uppaal.Label(GUARD,getStateVarName(nosuc)+"==1",p2.x+STEP_TEXT,p1.y)),
                            List.of(p1,p2)));
                }else if(val ==Value.FALSE){
                    Point p2=new Point(fail.getX(), init.getY()+STEP_TEXT*pos);
                    writer.println(init.getUppaalConnection(fail,List.of(new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!',p1.x+STEP_TEXT*6,p1.y),
                            new addtool.add2uppaal.Label(GUARD,getStateVarName(nosuc)+"==-1",p1.x+STEP_TEXT,p1.y)),
                            List.of(p1,p2)));
                }
            }
            pos++;
        }

        /*noSuccessors.forEach(v->{
            for(Value val:Value.values()){
                if(val==Value.TRUE){
                    writer.println(init.getUppaalConnection(succ, new Label(SYNCHRO,PROPAGATE+"!"),
                            new Label(GUARD,getStateVarName(v)+"==1")));
                }else if(val ==Value.FALSE){
                    writer.println(init.getUppaalConnection(fail,new Label(SYNCHRO,PROPAGATE+"!"),
                            new Label(GUARD,getStateVarName(v)+"==-1")));
                }
            }
        });*/
        writer.println("</template>");
    }
    private static String getVarDecStr(Vertex v){
        return String.format("int %s=0,%s=0,%s=0;",getUpdateVarName(v),getStateVarName(v),getTimestampVarName(v));
    }
    private static String getUpdateVarName(Vertex v){
        return UPDATE+ModestCodeDeclaration.getCodeToVertex(v);
    }
    private static String getStateVarName(Vertex v){
        return STATE+ModestCodeDeclaration.getCodeToVertex(v);
    }
    private static String getTimestampVarName(Vertex v){
        return TIMESTAMP+ModestCodeDeclaration.getCodeToVertex(v);
    }
    private static String getTriggerVarName(Vertex v){
        return TIMESTAMP+ModestCodeDeclaration.getCodeToVertex(v);
    }
    /**
     * Prints the automata for a Vertex to the Writer.
     * Method prints basic frame and redirects to printVertexSub for BasicEvents/NaryOp/UnaryOp
     * @param writer Writer to write to
     * @param v Vertex to build automata of
     * @param model The ADD (just used for BEs to build Trigger connections, otherwise it may be null)
     */
    private  static void printVertex(PrintWriter writer,Vertex v,ADD model){

        writer.println("<template>");
        writer.println(String.format("\t<name>%s</name>",ModestCodeDeclaration.getCodeToVertex(v)));
        writer.println(String.format("<declaration>void %s(){",UPDATE_METHOD));
        v.getSuccessors().stream().map(v2->getUpdateVarName(v2)+"=1;").forEach(writer::println);
        if(v instanceof Reset){//Resets
            ((Reset)v).getToReset().stream().map(v2->getUpdateVarName(v2)+"=1;").forEach(writer::println);
        }
        writer.println("}");

        writer.println(String.format("void %s(int stat){",UPDATE_SELF));
        writer.println(getStateVarName(v)+"=stat;");
        writer.println(getTimestampVarName(v)+ '=' +STEPS+ ';');
        writer.println(STEPS+"++;");
        if(v instanceof BasicEvent)writer.println(COST+ '=' +COST+ '+' +((BasicEvent)v).getCost().doubleValue()+ ';');
        writer.println("}</declaration>");

        if(v instanceof BasicEvent){
            printBEsub(writer,(BasicEvent)v,model);
        }else if(v instanceof NaryOp ||v instanceof UnaryOp){
            printOperatorsub(writer,v);
        }
        writer.println("</template>");
    }/**
     * Outputs a BasicEvent. Use printVertex to call this Method as it adds some Basic Information
     * This is sort of hardcoded as any basic Event follows the same Automata structure
     * @param writer Writer to write to
     * @param v BasicEvent to output
     * @param model The model. It is needed to create Trigger guard
     */
    private  static void printBEsub(PrintWriter writer,BasicEvent v,ADD model){
        Location init=new Location("id"+ idCounter.getAndIncrement(),0,0,"INIT");
        Location success=new Location("id"+ idCounter.getAndIncrement(),4*STEP,-STEP,"SUCCESS");
        Location failed=new Location("id"+ idCounter.getAndIncrement(),4*STEP,STEP,"FAILED");
        Location meta=new Location("id"+ idCounter.getAndIncrement(),6*STEP,-2*STEP);
        Location branch=new BranchPoint("id"+ idCounter.getAndIncrement(),2*STEP,0);
        writer.println(init.getUppaalDesc());
        writer.println(success.getUppaalDesc());
        writer.println(failed.getUppaalDesc());
        writer.println(meta.getUppaalDesc());
        //Branchpoints must be last
        writer.println(branch.getUppaalDesc());
        writer.println(init.getUppaalInit());

        if(v.isTriggerAble()){
            //find all triggers connected to this
            String guard=model.getId2Vertex().values().stream().filter(ver->(ver instanceof Trigger&&((Trigger)ver).getToTrigger().contains(v)))
                    .map(ver->getStateVarName(ver)+"==1").collect(Collectors.joining("||"));
            writer.println(init.getUppaalConnection(branch,new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!',init.getX()+STEP_TEXT, init.getY()+STEP_TEXT),
                    new addtool.add2uppaal.Label(ASSIGNMENT,getStateVarName(v)+"=0",init.getX()+STEP_TEXT, init.getY()+2*STEP_TEXT),
                    new addtool.add2uppaal.Label(GUARD,guard,init.getX()+STEP_TEXT, init.getY())));
        }else{
            writer.println(init.getUppaalConnection(branch,new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!',init.getX()+STEP_TEXT, init.getY())
                    ,new addtool.add2uppaal.Label(ASSIGNMENT,getStateVarName(v)+"=0",init.getX()+STEP_TEXT, init.getY()+STEP_TEXT)));
        }

        //Non deterministic edges
        Rational succ=v.getSuccessProbability();
        double p1=succ.doubleValue();
        double p2=1-p1;
        String z1= String.valueOf((int) (p1 * 1000000));
        String z2= String.valueOf((int) (p2 * 1000000));
        if(p1!=0)writer.println(branch.getUppaalConnection(success,List.of(new addtool.add2uppaal.Label(PROBABILITY ,z1,branch.getX(),success.getY()),
                new addtool.add2uppaal.Label(ASSIGNMENT,UPDATE_SELF+"(1)",branch.getX(),success.getY()+STEP_TEXT)),
                List.of(new Point(branch.getX(), success.getY() ))));
        if(p2!=0)writer.println(branch.getUppaalConnection(failed,List.of(new addtool.add2uppaal.Label(PROBABILITY ,z2,branch.getX(),failed.getY()-STEP_TEXT),
                new addtool.add2uppaal.Label(ASSIGNMENT,UPDATE_SELF+"(-1)",branch.getX(),failed.getY()-2*STEP_TEXT)),
                List.of(new Point(branch.getX(), failed.getY()))));

        writer.println(success.getUppaalConnection(meta,List.of(new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!', success.getX()+STEP_TEXT, success.getY()-STEP_TEXT),
                new addtool.add2uppaal.Label(ASSIGNMENT,UPDATE_METHOD+"()", success.getX()+STEP_TEXT, success.getY()-2*STEP_TEXT)),
                List.of(new Point(meta.getX(),success.getY()))));
        writer.println(failed.getUppaalConnection(meta,List.of(new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!', failed.getX()+STEP_TEXT, failed.getY()-STEP_TEXT),
                new addtool.add2uppaal.Label(ASSIGNMENT,UPDATE_METHOD+"()", failed.getX()+STEP_TEXT, failed.getY()-2*STEP_TEXT)),
                List.of(new Point(meta.getX(),failed.getY()))));

        writer.println(meta.getUppaalConnection(init,List.of(
                new addtool.add2uppaal.Label(GUARD,getUpdateVarName(v)+"==1", init.getX(), meta.getY() ),
                new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!', init.getX(), meta.getY()+STEP_TEXT),
                new addtool.add2uppaal.Label(ASSIGNMENT,getUpdateVarName(v)+"=0", init.getX(), meta.getY()+STEP_TEXT*2)),
                List.of(new Point(init.getX(),meta.getY()))));
    }
    /**
     * Outputs a UnaryOperator in Uppaal. The automata is reset-compatible as it returns to the init-state after sending a result
     * @param writer Writer to write to
     * @param v UnaryOperator to convert
     */
    private  static void printOperatorsub(PrintWriter writer, Vertex v){
        //Preload data about needed connections
        List<Vertex> preds=v.getPredecessors();
        List<Statement> toundic=new ArrayList<>();
        List<Statement> totrue=new ArrayList<>();
        List<Statement> tofalse=new ArrayList<>();
        printLayers(v,preds,toundic,totrue,tofalse, new ArrayList<>(), new ArrayList<>());
        //Define Locations and Print their Description
        Location init=new Location("id"+ idCounter.getAndIncrement(),0,0,"INIT");
        writer.println(init.getUppaalDesc());

        Location finalfalse=new Location("id"+ idCounter.getAndIncrement(),12*STEP,((int)tofalse.stream().distinct().count()+2)*STEP_TEXT,"FAILED");
        writer.println(finalfalse.getUppaalDesc());

        Location finaltrue=new Location("id"+ idCounter.getAndIncrement(),12*STEP,-((int)toundic.stream().distinct().count()+2)*STEP_TEXT,"TRUE");
        writer.println(finaltrue.getUppaalDesc());

        Location finalundic=new Location("id"+ idCounter.getAndIncrement(),12*STEP,0,"UNDECIDED");
        writer.println(finalundic.getUppaalDesc());

        Location meta=new Location("id"+ idCounter.getAndIncrement(),-STEP,0);
        writer.println(meta.getUppaalDesc());

        //Set starting location
        writer.println(init.getUppaalInit());

        //Print Edges
        writer.println(meta.getUppaalConnection(init,new addtool.add2uppaal.Label(GUARD,getUpdateVarName(v)+"==1", meta.getX(), meta.getY()+STEP_TEXT),
                new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!', meta.getX(), meta.getY()+2*STEP_TEXT),
                new addtool.add2uppaal.Label(ASSIGNMENT,getUpdateVarName(v)+"=0", meta.getX(), meta.getY()+3*STEP_TEXT)));

        writer.println(finalfalse.getUppaalConnection(meta,List.of(new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!',finalfalse.getX(),finalfalse.getY()+STEP_TEXT),
                new addtool.add2uppaal.Label(ASSIGNMENT,UPDATE_METHOD+"()",finalfalse.getX(),finalfalse.getY()+2*STEP_TEXT)),
                List.of(new Point(finalfalse.getX(),finalfalse.getY()+STEP),new Point(meta.getX(),finalfalse.getY()+STEP))));
        writer.println(finaltrue.getUppaalConnection(meta,List.of(new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!',finaltrue.getX()+2*STEP,finaltrue.getY()),
                new addtool.add2uppaal.Label(ASSIGNMENT,UPDATE_METHOD+"()",finaltrue.getX()+2*STEP,finaltrue.getY()+STEP_TEXT)),
                List.of(new Point(finaltrue.getX()+2*STEP,finaltrue.getY()),new Point(finalfalse.getX()+2*STEP,finalfalse.getY()+STEP),
                        new Point(meta.getX(),finalfalse.getY()+STEP))));
        writer.println(finalundic.getUppaalConnection(meta,List.of(new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!',finalundic.getX()+STEP,finalundic.getY()),
                new addtool.add2uppaal.Label(ASSIGNMENT,UPDATE_METHOD+"()",finalundic.getX()+STEP,finalundic.getY()+STEP_TEXT)),
                List.of(new Point(finalundic.getX()+STEP,finalundic.getY()),new Point(finalundic.getX()+STEP,finalfalse.getY()+STEP),
                        new Point(meta.getX(),finalfalse.getY()+STEP))));


        processConnections(writer,v,init,finaltrue,totrue,Value.TRUE);
        processConnections(writer,v,init,finalfalse,tofalse,Value.FALSE);
        processConnections(writer,v,init,finalundic,toundic,Value.UNDECIDED);
    }

    private static void processConnections(PrintWriter writer, Vertex op,Location source, Location target, List<Statement> totrue, Value val) {
        totrue=totrue.stream().distinct().collect(Collectors.toList());

        for(int i=0;i<totrue.size();i++){
            String s=totrue.get(i).buildAnd();
            addtool.add2uppaal.Label update=new addtool.add2uppaal.Label(ASSIGNMENT,getStateVarName(op)+ '=' +val.getInt(),source.getX()+STEP_TEXT,target.getY()-i*STEP_TEXT);
            addtool.add2uppaal.Label prop=new addtool.add2uppaal.Label(SYNCHRO,PROPAGATE+ '!',source.getX()+STEP_TEXT*6,target.getY()-i*STEP_TEXT);
            addtool.add2uppaal.Label guard=new Label(GUARD,s,source.getX()+STEP_TEXT*10,target.getY()-i*STEP_TEXT);
            Point p1=new Point(source.getX()+STEP_TEXT, source.getY());
            Point p2=new Point(source.getX()+STEP_TEXT, target.getY()-i*STEP_TEXT);
            Point p3=new Point(target.getX()-STEP_TEXT, target.getY()-i*STEP_TEXT);
            Point p4=new Point(target.getX()-STEP_TEXT, target.getY());
            writer.println(source.getUppaalConnection(target,List.of(prop,guard,update),List.of(p1,p2,p3,p4)));
        }
    }

    /**
     * Recursive Method to Handle Multiple Layers in NaryOperators.
     * @param done List of Vertices allready visited
     * @param done_vals List of Values corresponding to done
     */
    private  static void printLayers(Vertex op,List<Vertex> preds, List<Statement> toundic, List<Statement>  totrue, List<Statement>  tofalse, ArrayList<Vertex> done, ArrayList<Value> done_vals){
        AtomicBoolean anydone=new AtomicBoolean(false);
        preds.stream().filter(v->!done.contains(v)).forEach(v->{
            anydone.set(true);
            //Extend List with new node. Branching with for Each.
            ArrayList<Vertex> n_done=(ArrayList<Vertex>)done.clone();
            n_done.add(v);

            for(Value val:Value.values()){//Iterate all possible Values
                    //Check how NaryOperator will evaluate with the given sequence
                    ArrayList<Value> n_done_vals = (ArrayList<Value>) done_vals.clone();
                    n_done_vals.add(val);
                    printLayers(op,preds,toundic,totrue,tofalse,n_done,n_done_vals);
            }
        });
        if(!anydone.get()){
            //No Vertices remaining
            Value res=op.evaluate(done,done_vals);
            Statement guard=createGuard(done,done_vals,(op instanceof NaryOp&&((NaryOp)op).isSequential()));
            switch (res) {
                case TRUE -> totrue.add(guard);
                case FALSE -> tofalse.add(guard);
                case UNDECIDED -> toundic.add(guard);
            }
        }
    }
    private static Statement createGuard(ArrayList<Vertex> done, ArrayList<Value> done_vals,boolean respectorder){
        List<String> statements=new ArrayList<>();
        for(int i=0;i<done.size();i++){
            statements.add(getStateVarName(done.get(i))+"=="+done_vals.get(i).getInt());
            if(respectorder&&i<done.size()-1){
                statements.add(getTimestampVarName(done.get(i))+"&lt;="+getTimestampVarName(done.get(i+1)));
            }
        }
        return new Statement(statements);
    }
}
