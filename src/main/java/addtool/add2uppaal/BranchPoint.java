package addtool.add2uppaal;

/**
 * This class is an Extension to Location.
 * It represents in Uppaal the branching point before non deterministic decisions.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 31.08.2020
 */
public class BranchPoint extends Location{
    public BranchPoint(String name, int x, int y) {
        super(name, x, y);
    }
    @Override
    public String getUppaalDesc(){
        String out="\t<branchpoint id=\"%s\" x=\"%d\" y=\"%d\"></branchpoint>"; //NON-NLS
        return String.format(out, getId(),getX(),getY());
    }
}
