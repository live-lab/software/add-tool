package addtool.add2uppaal;

import java.awt.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Represents the Vertices in Uppaal.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 31.08.2020
 */
public class Location {
    private String id;
    private String name;
    /**
     * Both variables have just graphical reason. They represent the location in the uppaal Editor
     */
    private int x;
    private int y;
    public Location(String id,int x,int y){
        this.id =id;
        this.x=x;
        this.y=y;
    }public Location(String id,int x,int y,String name){
        this(id,x,y);
        this.name=name;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     * Creates the Uppaal description for this Location.
     * @return String representing this node in uppaal
     */
    public String getUppaalDesc(){
        String out="\t<location id=\"%s\" x=\"%d\" y=\"%d\">";
        out=String.format(out, id,x,y);
         if(name!=null){
             out+=String.format("\n<name x=\"%d\" y=\"%d\">%s</name>\n",x,y,name);
         }
        out+="</location>";
        return out;
    }

    /**
     * Creates the Uppaal description for a starting point in an automata.
     * @return String representing this node being the starting location of an automata
     */
    public String getUppaalInit(){
        String out="\t<init ref=\"%s\"/>";
        return String.format(out, id);
    }

    /**
     * Write an Edge between this and another node with a single label.
     * @param l2 Second Node is used as Target for Connection
     * @param label label "kind" for the label
     * @param val Value for the label
     * @return A String representation in Uppaal
     */
    public String getUppaalConnection(Location l2,String label,String val){
        if(label==null||val==null)return getUppaalConnection(l2);

        return getUppaalConnection(l2, List.of(new addtool.add2uppaal.Label(label,val)),List.of());
    }

    /**
     * Write an Edge between this and another node with a given List of Attributes.
     * @param l2 Second Node is used as Target for Connection
     * @param labels A collection of Entries with the Key being the "kind" field in the description and the value being the value
     * @return A String representation in Uppaal
     */
    public String getUppaalConnection(Location l2, addtool.add2uppaal.Label...labels){
        return getUppaalConnection(l2,Arrays.asList(labels),List.of());
    }

    /**
     * Write an Edge between this and another node with a given List of Attributes.
     * @param l2 Second Node is used as Target for Connection
     * @param labels A collection of Entries with the Key being the "kind" field in the description and the value being the value
     * @return A String representation in Uppaal
     */
    public String getUppaalConnection(Location l2, Collection<addtool.add2uppaal.Label>labels, Collection<Point> nails){
        StringBuilder out=new StringBuilder();
        out.append("\t<transition>\n");
        out.append("\t\t<source ref=\"%s\"/>\n");
        out.append("\t\t<target ref=\"%s\"/>\n");

        out=new StringBuilder(String.format(out.toString(), id,l2.getId()));
        for(Label l:labels){
            int labelX=l.isLocationSet()?l.getX():(x+l2.getX())/2;
            int labelY=l.isLocationSet()?l.getY():(y+l2.getY())/2;
            out.append(String.format("\t\t<label kind=\"%s\" x=\"%d\" y=\"%d\">%s</label>\n",l.getType(),labelX,labelY,l.getContent()));
        }
        for(Point p:nails){
            out.append(String.format("\t\t<nail x=\"%d\" y=\"%d\"/>\n",p.x,p.y));
        }
        out.append("\t</transition>");
        return out.toString();
    }

    /**
     * Write an Edge between this and another node without Labels.
     * @param l2 Second Node is used as Target for Connection
     * @return A String representation in Uppaal
     */
    public String getUppaalConnection(Location l2){
        return getUppaalConnection(l2,List.of(),List.of());
    }

    @Override
    public String toString() {
        return getUppaalDesc();
    }

}
