package addtool.add2uppaal;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Statement {
    private final ArrayList<String> parts;
    public Statement(List<String> parts){
        this.parts=new ArrayList<>();
        this.parts.addAll(parts);
        this.parts.sort(Comparator.naturalOrder());
    }
    @Override
    public boolean equals(Object o){
        if(o instanceof Statement){
            Statement stat2=(Statement)o;
            if(stat2.parts.size()!=this.parts.size())return false;
            ArrayList<String> pcheck2=(ArrayList<String>)stat2.parts.clone();
            for(String s:parts){
                pcheck2.remove(s);
            }
            return pcheck2.isEmpty();
        }
        return false;
    }
    @Override
    public int hashCode(){
        return parts.hashCode();
    }
    @Override
    public String toString(){
        return parts.toString();
    }
    public String build(String op){
        return String.join(op, parts);
    }
    public String buildAnd(){
        return build("&amp;&amp;");
    }
    public String buildOr(){
        return build("||");
    }
}
