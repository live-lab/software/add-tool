package addtool.add2uppaal;

import addtool.add.model.ADD;
import addtool.dot2add.ParserDOT;

import java.io.FileReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * Main method for conversion to UPPAAL.
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 31.08.2020
 * @see NADD2Uppaal#writeADD2UppaalFile
 */
public final class MainDot2Uppaal {

    private MainDot2Uppaal() {
    }

    public static void main(String... args) throws java.io.FileNotFoundException, addtool.dot2add.WrongValueForLabelOperatorException, addtool.dot2add.NoValueSpecifiedException, addtool.dot2add.WrongStructureADDException, addtool.dot2add.WrongValueForSinkException, java.io.IOException {
        String infile= Path.of("test","ADDs","_file-747.dot").toString();
            if(args.length>=1){
            infile=args[0];
        }
        String outfile=Path.of("test","ADDs","simple_uppaal.xml").toString();
            if(args.length>=2){
            outfile=args[1];
        }
        FileReader in = new FileReader(infile);
        ADD adg = ParserDOT.parseADD(in);
            adg.validateConnections();
        NADD2Uppaal.QueryState qs= NADD2Uppaal.QueryState.BOTH;
        if(args.length>=3){
            NADD2Uppaal.QueryState tmp= NADD2Uppaal.QueryState.of(args[2]);
            if(tmp!=null)qs=tmp;
        }
        PrintWriter writer = new PrintWriter(outfile, StandardCharsets.UTF_8);
        NADD2Uppaal.writeADD2UppaalFile(writer,adg,qs);

        //adg.getNoSuccessor().stream().map(v-> ModestCodeDeclaration.getCodeToVertex(v)+"-"+v.getDisruptionProb().doubleValue()).forEach(System.out::println);
    }
}
