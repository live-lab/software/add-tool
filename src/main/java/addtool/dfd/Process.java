package addtool.dfd;


import com.mxgraph.util.mxConstants;

public class Process extends Entity{
    private String privilegeLevel="";

    public String getPrivilegeLevel() {
        return privilegeLevel;
    }

    public void setPrivilegeLevel(String privilegeLevel) {
        this.privilegeLevel = privilegeLevel;
    }

    @Override
    public String getLineType() {
        return "solid";
    }

    @Override
    public String getShape() {
        return mxConstants.SHAPE_ELLIPSE;
    }
}
