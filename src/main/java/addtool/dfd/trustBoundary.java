package addtool.dfd;

import com.mxgraph.util.mxConstants;

import static com.mxgraph.util.mxConstants.*;
import static com.mxgraph.util.mxConstants.NONE;

public class trustBoundary extends Line{
    @Override
    public String getLineType() {
        return "dashed";
    }

    @Override
    public String getShape() {
        return STYLE_STARTARROW+"="+NONE+";"+STYLE_ENDARROW+"="+NONE+";"+STYLE_DASHED+"=true;";
    }
    @Override
    public String getColor(){
        return "#03fc1c";
    }
}
