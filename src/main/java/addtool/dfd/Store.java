package addtool.dfd;

import static addtool.nui.style.nStoreShape.SHAPE_STORE;

public class Store extends Entity{
    private boolean isLog=false;
    private boolean storesCredentials=false;
    private boolean isEncrypted=false;
    private boolean isSigned=false;

    public boolean isLog() {
        return isLog;
    }

    public void setLog(boolean log) {
        isLog = log;
    }

    public boolean isStoresCredentials() {
        return storesCredentials;
    }

    public void setStoresCredentials(boolean storesCredentials) {
        this.storesCredentials = storesCredentials;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public void setEncrypted(boolean encrypted) {
        isEncrypted = encrypted;
    }

    public boolean isSigned() {
        return isSigned;
    }

    public void setSigned(boolean signed) {
        isSigned = signed;
    }

    @Override
    public String getLineType() {
        //TODO only draw TOP and Bottom line
        return "solid";
    }

    @Override
    public String getShape() {
        return SHAPE_STORE;
    }
}
