package addtool.dfd;

import static com.mxgraph.util.mxConstants.*;

public class DataFlow extends Line{
    Entity source,target;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public boolean isEncrypted() {
        return encrypted;
    }

    public void setEncrypted(boolean encrypted) {
        this.encrypted = encrypted;
    }

    public boolean isPublicNetwork() {
        return publicNetwork;
    }

    public void setPublicNetwork(boolean publicNetwork) {
        this.publicNetwork = publicNetwork;
    }

    private String protocol;
    private boolean encrypted;
    private boolean publicNetwork;
    public DataFlow(){
        super();
        protocol="";
        encrypted=false;
        publicNetwork=false;
    }

    @Override
    public String getLineType() {
        return "solid"; //NON-NLS
    }

    @Override
    public String getShape() {
        return STYLE_STARTARROW+"="+NONE+";"+STYLE_ENDARROW+"="+ARROW_CLASSIC+";";
    }

    public void setSource(Entity e){
        source=e;
    }
    public void setTarget(Entity e){
        target=e;
    }
}
