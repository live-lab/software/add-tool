package addtool.dfd;

import addtool.helpers.Threat;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.util.UUID;

public abstract class Entity {
    private Rectangle border;
    private String name;
    private List<Threat> threats;
    private String ID;
    private boolean isOutOfScope;
    private String reasonOutOfScope;
    public Entity(){
        name=getClass().getSimpleName();
        threats=new ArrayList<>();
        ID= UUID.randomUUID().toString();
        border=new Rectangle(0,0,100,50);
        isOutOfScope=false;
        reasonOutOfScope="";
    }

    public Point getLocation(){
        return border.getLocation();
    }
    public void setLocation(@NotNull Point p){
        border.setLocation(p);
    }
    public void setBoundaries(@NotNull Rectangle r){
        border=r.getBounds();
    }
    public Rectangle getBoundaries(){
        return border.getBounds();
    }
    public boolean addThreat(Threat t){
        return threats.add(t);
    }
    public boolean removeThreat(Threat t){
        return threats.remove(t);
    }
    public List<Threat> getThreats(){
        return new ArrayList<>(threats);
    }
    public void replaceThreats(List<Threat> newList){
        threats.clear();
        threats.addAll(newList);
    }
    public void setName(@NotNull String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }

    public String getColor(){
        return threats.stream().anyMatch(Threat::isOpen)?"#fd2d00":"#000000";
    }
    public abstract String getLineType();
    public abstract String getShape();

    public String toString(){
        return String.format("[%s;%s;%s]",getName(),getBoundaries().toString(),threats.toString());
    }
    public String getID(){
        return ID;
    }
    public void setID(@NotNull String ID){
        this.ID=ID;
    }
    public void assignNewID(){
        ID= UUID.randomUUID().toString();
    }
    public boolean isOutOfScope(){
        return isOutOfScope;
    }
    public void setOutOfScope(boolean b){
        isOutOfScope=b;
    }
    public String getReasonOutOfScope(){
        return reasonOutOfScope;
    }
    public void setReasonOutOfScope(String s){
        reasonOutOfScope=s;
    }
}
