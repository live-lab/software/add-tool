package addtool.dfd;

public class Actor extends Entity{
    private boolean providesAuthentication =false;

    public boolean isProvidesAuthentication() {
        return providesAuthentication;
    }

    public void setProvidesAuthentication(boolean providesAuthentication) {
        this.providesAuthentication = providesAuthentication;
    }
    @Override
    public String getLineType() {
        return "solid";
    }

    @Override
    public String getShape() {
        return "rectangle";
    }
}
