package addtool.dfd;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

public abstract class Line extends Entity{
    private Collection<Point> points;
    /**
     * UUID of source and target if applicable
     */
    private String source;
    private String target;

    public Line(){
        points=new ArrayList<>();
    }
    public Collection<Point> getPoints(){
        return new ArrayList<>(points);
    }
    public boolean addPoint(Point p){
        return points.add(p);
    }
    public boolean removePoint(Point p){
        return points.remove(p);
    }
    public void replacePoints(Collection<Point> newPoints){
        points.clear();
        points.addAll(newPoints);
    }
    public int getWidth(){
        int min=0,max=0;
        for(Point p:points){
            if(p.x<min){
                min=p.x;
            }
            if(p.x>max){
                max=p.x;
            }
        }
        return max-min;
    }
    public int getHeigth(){
        int min=0,max=0;
        for(Point p:points){
            if(p.y<min){
                min=p.y;
            }
            if(p.y>max){
                max=p.y;
            }
        }
        return max-min;
    }
    @Override
    public String toString(){
        return String.format("{%s~%s}",super.toString(),points.toString());
    }

    public boolean hasSource(){
        return source!=null;
    }
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    public boolean hasTarget(){
        return target!=null;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
