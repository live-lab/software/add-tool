package addtool.dfd;

import java.util.HashSet;
import java.util.Set;

public class DFD {
    Set<Entity> entities;
    public DFD(){
        entities=new HashSet<>();
    }
    public Set<Entity> getEntities(){
        return new HashSet<>(entities);
    }
    public void addEntity(Entity e){
        while(entities.stream().anyMatch(entity -> e.getID().equals(entity.getID()))&&!e.equals(entities)){
            e.assignNewID();
        }
        entities.add(e);
    }
    public String toString(){
        return entities.toString();
    }
}
