package addtool.dfd.io;

import addtool.dfd.*;
import addtool.dfd.Process;
import addtool.helpers.ExceptionHandler;
import addtool.helpers.STRIDE;
import addtool.helpers.Threat;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.awt.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.UUID;

import static addtool.dfd.io.dfdIOConstants.*;
import static java.lang.Math.toIntExact;

public class DFDImporter {
    public static JSONObject readFile(File f){
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(f)) {
            //Read JSON file
            return (JSONObject) jsonParser.parse(reader);
        }catch (IOException|ParseException ex){
            ex.printStackTrace();
            ExceptionHandler.logException(ex);
            return null;
        }
    }
    public static DFD importDFD(File f){
        return importDFD(extractFirstDiagram(readFile(f)));
    }
    public static JSONObject extractFirstDiagram(JSONObject object){
        JSONArray diagrams=(JSONArray)((JSONObject)object.get("detail")).get("diagrams");
        JSONObject diagram=(JSONObject) diagrams.get(0);
        return (JSONObject) diagram.get("diagramJson");
    }
    /**
     *
     * @param jsonObject a diagramJson Object from Threat dragon file format
     * @return A dfd represented in the object
     */
    public static DFD importDFD(JSONObject jsonObject){
        DFD out=new DFD();
        JSONArray entities=(JSONArray) jsonObject.get(ENTITIES);
        for(Object o:entities){
            if(o instanceof JSONObject){
                JSONObject object=(JSONObject)o;
                JSONObject position=(JSONObject)object.getOrDefault(POSITION,new JSONObject());
                JSONObject size=(JSONObject)object.getOrDefault(SIZE,new JSONObject());
                Rectangle r=new Rectangle(toIntExact((long)position.getOrDefault(X,0L)),
                        toIntExact((long)position.getOrDefault(Y,0L)),
                        toIntExact((long)size.getOrDefault(WIDTH,100L)),
                        toIntExact((long)size.getOrDefault(HEIGHT,50L)));
                JSONObject attrs=(JSONObject) (object.containsKey(LABELS)?
                        (JSONObject) ((JSONArray)object.get(LABELS)).get(0):
                        object).
                            getOrDefault(ATTRS,new JSONObject());
                String name=(String)((JSONObject)attrs.getOrDefault(TEXT,new JSONObject())).getOrDefault(TEXT,"");
                String className=(String)object.getOrDefault(CLASS,Process_TYPE);
                String id=(String)object.getOrDefault(ID, UUID.randomUUID());
                boolean isOutOfScope=(boolean)object.getOrDefault(OUT_OF_SCOPE,false);
                String reasonOutOfScope=(String) object.getOrDefault(REASON_OUT_OF_SCOPE,"");

                Entity entity;
                switch (className){
                    case DATA_FLOW_TYPE->entity=new DataFlow();
                    case TRUST_BOUNDARY_TYPE->entity=new trustBoundary();
                    case ACTOR_TYPE->entity=new Actor();
                    case STORE_TYPE->entity=new Store();
                    default -> entity=new Process();
                }
                entity.setBoundaries(r);
                entity.setName(name);
                entity.setID(id);
                entity.setOutOfScope(isOutOfScope);
                entity.setReasonOutOfScope(reasonOutOfScope);

                if(entity instanceof Actor){
                    ((Actor) entity).setProvidesAuthentication((boolean)object.getOrDefault(PROVIDES_AUTHENTICATION,false));
                }
                if(entity instanceof Store){
                    ((Store) entity).setLog((boolean)object.getOrDefault(IS_LOG,false));
                    ((Store) entity).setStoresCredentials((boolean)object.getOrDefault(STORES_CREDENTIALS,false));
                    ((Store) entity).setEncrypted((boolean)object.getOrDefault(IS_ENCRYPTED,false));
                    ((Store) entity).setSigned((boolean)object.getOrDefault(IS_SIGNED,false));
                }
                if(entity instanceof Process){
                    ((Process) entity).setPrivilegeLevel((String)object.getOrDefault(PRIVILEGE_LEVEL,""));
                }
                if(entity instanceof DataFlow){
                    ((DataFlow) entity).setEncrypted((boolean)object.getOrDefault(IS_ENCRYPTED,false));
                    ((DataFlow) entity).setPublicNetwork((boolean)object.getOrDefault(IS_PUBLIC_NETWORK,false));
                    ((DataFlow) entity).setProtocol((String)object.getOrDefault(PROTOCOL,""));
                }

                JSONArray threats= (JSONArray) object.getOrDefault(THREATS,new JSONArray());
                for (Object to:threats){
                    if(to instanceof JSONObject){
                        JSONObject threatObject=(JSONObject) to;
                        Threat threat=new Threat();
                        threat.setTitle((String)threatObject.getOrDefault(THREAT_TITLE,THREAT_TITLE));
                        threat.setDescription((String) threatObject.getOrDefault(THREAT_DESCRIPTION,THREAT_DESCRIPTION));
                        threat.setMitigations((String) threatObject.getOrDefault(THREAT_MITIGATIONS,THREAT_MITIGATIONS));
                        threat.setOpen(threatObject.getOrDefault(THREAT_OPEN,THREAT_STATUS_OPEN).equals(THREAT_STATUS_OPEN));
                        threat.setStride(STRIDE.read((String) threatObject.getOrDefault(THREAT_STRIDE,STRIDE.SPOOFING.toString())));
                        threat.setSeverity(Threat.Severity.valueOfInsensitive((String) threatObject.getOrDefault(THREAT_SEVERITY, Threat.Severity.HIGH.toString())));
                        entity.addThreat(threat);
                    }
                }
                if(entity instanceof Line){
                    Line line=(Line)entity;
                    if(object.containsKey(SOURCE)){
                        JSONObject source=(JSONObject) object.get(SOURCE);
                        if(source.containsKey(ID)){
                            line.setSource((String)source.get(ID));
                        }else{
                            line.addPoint(new Point(
                                    toIntExact((long)source.getOrDefault(X,0L)),
                                    toIntExact((long)source.getOrDefault(Y,0L))));
                        }
                    }
                    JSONArray points= (JSONArray) object.get(POINTS);
                    for (Object po:points){
                        if(po instanceof JSONObject){
                            JSONObject pointObject=(JSONObject) po;
                            line.addPoint(
                                    new Point(
                                            toIntExact((long)pointObject.getOrDefault(X,0L)),
                                            toIntExact((long)pointObject.getOrDefault(Y,0L))));
                        }
                    }
                    if(object.containsKey(TARGET)){
                        JSONObject target=(JSONObject) object.get(TARGET);
                        if(target.containsKey(ID)){
                            line.setTarget((String)target.get(ID));
                        }else{
                            line.addPoint(new Point(
                                    toIntExact((long)target.getOrDefault(X,0)),
                                    toIntExact((long)target.getOrDefault(Y,0))));
                        }
                    }
                }
                out.addEntity(entity);
            }
        }
        return out;
    }
    public static boolean stringToBool(String s){
        return s.equals("Yes");
    }
}
