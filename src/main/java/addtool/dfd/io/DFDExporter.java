package addtool.dfd.io;

import addtool.dfd.*;
import addtool.dfd.Process;
import addtool.helpers.ExceptionHandler;
import addtool.helpers.Threat;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;
import java.util.List;

import static addtool.dfd.io.dfdIOConstants.*;

public class DFDExporter {
    public static JSONObject export(DFD dfd){
        JSONObject jsonObject = new JSONObject();

        JSONArray entities=new JSONArray();
        Set<Entity> entitySet=dfd.getEntities();
        int z=0;
        for(Entity e:entitySet){
            JSONObject eo = new JSONObject();
            eo.put(ID,e.getID());
            eo.put(OUT_OF_SCOPE,e.isOutOfScope());
            eo.put(REASON_OUT_OF_SCOPE,e.getReasonOutOfScope());
            JSONObject position=new JSONObject();
            JSONObject size=new JSONObject();
            Rectangle r=e.getBoundaries();
            position.put(X,r.x);
            position.put(Y,r.y);
            size.put(WIDTH,r.width);
            size.put(HEIGHT,r.height);
            eo.put(POSITION,position);
            eo.put(SIZE,size);
            eo.put(Z,z++);

            if(e instanceof Actor){
                eo.put(CLASS,ACTOR_TYPE);
                eo.put(PROVIDES_AUTHENTICATION,((Actor) e).isProvidesAuthentication());
            }
            if(e instanceof Store){
                eo.put(CLASS,STORE_TYPE);
                eo.put(IS_LOG,((Store) e).isLog());
                eo.put(STORES_CREDENTIALS,((Store) e).isStoresCredentials());
                eo.put(IS_ENCRYPTED,((Store) e).isEncrypted());
                eo.put(IS_SIGNED,((Store) e).isSigned());
            }
            if(e instanceof Process){
                eo.put(CLASS,Process_TYPE);
                eo.put(PRIVILEGE_LEVEL,((Process) e).getPrivilegeLevel());
            }
            if(e instanceof trustBoundary){
                eo.put(CLASS,TRUST_BOUNDARY_TYPE);

            }
            if(e instanceof DataFlow){
                eo.put(CLASS,DATA_FLOW_TYPE);
                eo.put(IS_ENCRYPTED,((DataFlow) e).isEncrypted());
                eo.put(IS_PUBLIC_NETWORK,((DataFlow) e).isPublicNetwork());
                eo.put(PROTOCOL,((DataFlow) e).getProtocol());
            }

            JSONArray threats=new JSONArray();
            List<Threat> threatList=e.getThreats();
            for(Threat t:threatList){
                JSONObject to = new JSONObject();
                to.put(THREAT_TITLE,t.getTitle());
                to.put(THREAT_DESCRIPTION,t.getDescription());
                to.put(THREAT_MITIGATIONS,t.getMitigations());
                to.put(THREAT_OPEN,t.isOpen()?THREAT_STATUS_OPEN:THREAT_STATUS_Mitigated);
                to.put(THREAT_STRIDE,t.getStride().getName());
                to.put(THREAT_SEVERITY,t.getSeverity().getName());

                threats.put(to);//.add(to);
            }
            eo.put(THREATS,threats);

            JSONObject name=new JSONObject();
            JSONObject text=new JSONObject();
            name.put(TEXT,text);
            text.put(TEXT,e.getName());
            text.put("font-weight","400");
            text.put("font-size","small");


            if(e instanceof Line){
                Line line=(Line)e;
                if(line.hasSource()){
                    eo.put(SOURCE,line.getSource());
                }
                if(line.hasTarget()){
                    eo.put(TARGET,line.getTarget());
                }
                JSONArray points=new JSONArray();
                Collection<Point> pointCollection=line.getPoints();
                for(Point p:pointCollection){
                    JSONObject po = new JSONObject();
                    po.put(X,p.x);
                    po.put(Y,p.y);
                    points.put(po);
                }
                eo.put(POINTS,points);

                JSONArray labels=new JSONArray();
                JSONObject label=new JSONObject();
                label.put("position",0.5);
                label.put(ATTRS,name);
                labels.put(label);
            }else{
                eo.put(ATTRS,name);
            }

            entities.put(eo);


        }
        jsonObject.put(ENTITIES,entities);
        return jsonObject;
    }
    public static String createJSONString(DFD model){
        return export(model).toString(4);
    }
    public static boolean writeJSONString(DFD model, File file){
        JSONObject jsonObject=addThreatDragonHeader(export(model));
        try {
            FileWriter fileWriter = new FileWriter(file.getAbsolutePath());
            fileWriter.write(jsonObject.toString(4));
            fileWriter.close();
        } catch (IOException e) {
            ExceptionHandler.logException(e);
            return false;
        }
        return true;
    }
    public static String boolToString(boolean b){
        return b?"Yes":"No";
    }
    public static JSONObject addThreatDragonHeader(JSONObject diagram){
        JSONObject master=new JSONObject();
        JSONObject summary=new JSONObject();
        summary.put("title","Dummy");
        summary.put("owner","Dummy");
        summary.put("description","Dummy");
        summary.put("id",0);
        master.put("summary",summary);

        JSONObject detail=new JSONObject();
        JSONArray contributors=new JSONArray();
        JSONObject contributor=new JSONObject();
        contributor.put("name","Max Mustermann");
        contributor.put("$$hashKey","object:11");
        contributors.put(contributor);
        detail.put("contributors",contributors);

        JSONArray diagrams=new JSONArray();
        detail.put("diagrams",diagrams);
        JSONObject wrap=new JSONObject();
        diagrams.put(wrap);
        wrap.put("title","Dummy");
        wrap.put("thumbnail","");
        wrap.put("id",0);

        wrap.put("diagramJson",diagram);

        JSONObject size=new JSONObject();
        size.put("height",594);
        size.put("width",860);
        wrap.put("size",size);
        wrap.put("diagramType","STRIDE");


        master.put("detail",detail);
        return master;
    }
}
