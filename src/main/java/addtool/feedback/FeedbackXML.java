package addtool.feedback;

import addtool.add.model.ADD;
import addtool.helpers.TranslationTable;

import java.util.ArrayList;
import java.util.Collection;

/**
 * for a given ADD model decide whether it can be translated to a SAND attack tree
 * or and attack-defense tree for the ADTool 2.0.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 07.10.2020
 */
public class FeedbackXML implements FeedbackGenerator {

    private final Collection<FeedbackTuple> sandAT=new ArrayList<>();
    private final Collection<FeedbackTuple> adt=new ArrayList<>();

    @Override
    public Collection<FeedbackTuple> getFeedback(ADD model) {
        sandAT.clear();
        adt.clear();

        XMLVisitor visitor = new XMLVisitor(sandAT, adt);
        model.accept(visitor);

        Collection<FeedbackTuple> complete = new ArrayList<>();
        complete.addAll(sandAT);
        complete.addAll(adt);
        return complete;
    }

    /**
     * Checks if it is an sequential AT
     * @return true if it fulfills sequential and at requirements
     */
    public boolean isSANDAttackTree() {
        return sandAT.isEmpty();
    }

    public boolean isADT() {
        return adt.isEmpty();
    }


    @Override
    public String getName(){
        return TranslationTable.ADTOOL;
    }
}
