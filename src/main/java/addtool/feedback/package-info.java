/**
 * Package handling feedback generation on {@link addtool.add.model.ADD}
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 21.09.2021
 * @see addtool.feedback.FeedbackGenerator
 */
package addtool.feedback;