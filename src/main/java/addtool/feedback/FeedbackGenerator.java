package addtool.feedback;

import addtool.add.model.ADD;
import addtool.add.model.Vertex;
import addtool.helpers.Constants;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * interface to implement if classes generate feedback.
 * which should be visualized in the GUI or the commandline
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.de
 * @since 02.10.2020
 */
@FunctionalInterface
public interface FeedbackGenerator {

    /**
     * just an example to show how an FeedbackGenerator can be implemented.
     * here, the FeedbackGenerator takes some model and a vertex without successor (if it exists) and marks the edge as
     * ok and the predecessor with a warning
     *
     * @return An empty Feedback generator, giving a warning and an ok modification
     */
    static FeedbackGenerator getExample() {
        return model -> {
            Vertex suc = model.getNoSuccessor().stream().findFirst().orElse(null);
            if (suc == null) return List.of();
            Vertex predecessor = suc.getPredecessors().stream().findAny().orElse(null);
            if (predecessor == null) return List.of();
            return List.of(new FeedbackTuple(predecessor, FeedbackTuple.MESSAGE_TYPE.WARNING, "Not right"), new FeedbackTuple(predecessor, suc, FeedbackTuple.MESSAGE_TYPE.OK, "fine"));
        };
    }

    /**
     * Returns an empty FeedbackGenerator.
     * @return A FeedbackGenerator without content
     */
    static FeedbackGenerator getEmpty() {
        return model -> List.of();
    }

    /**
     * main method to receive feedback for a model.
     *
     * @param model An {@link ADD} to generate Feedback on
     * @return A collection of {@link FeedbackTuple}
     */
    Collection<FeedbackTuple> getFeedback(ADD model);

    /**
     * Returns a name for the method
     * @return by default the simple name of the class
     */
    default String getName(){
        return this.getClass().getSimpleName();
    }

    /**
     * Returns an icon for the method if one exists
     * @return the icon or null if none is prepared
     */
    default Icon getIcon(){
        return null;
    }

    /**
     * Get selected FeedbackGenerator.
     * Will only find classes that have an accessible constructor with no arguments
     * @param name Name of the Type {@link FeedbackGenerator#getName()}
     * @return an object if there exists a corresponding class, null otherwise
     */
    static FeedbackGenerator getMethodByName(String name){
        Set<Class<? extends FeedbackGenerator>> classes = Constants.findAllMatchingTypes("",FeedbackGenerator.class);
        return classes.stream().map(c->{try{
            return c.getConstructor().newInstance();
        } catch (InvocationTargetException |InstantiationException|IllegalAccessException|NoSuchMethodException e) {
            return null;
        }}).filter(o-> Objects.nonNull(o) && o.getName().equals(name)).findFirst().orElse(null);
    }

    /**
     * Lists all Classes implementing the Interface in the classpath.
     * Will only find classes that have an accessible constructor with no arguments
     * @see FeedbackGenerator#getName()
     * @return An Array of all Class names
     */
    static String[] getOptions(){
        Set<Class<? extends FeedbackGenerator>> classes = Constants.findAllMatchingTypes("",FeedbackGenerator.class);
        return classes.stream().map(c->{try{
            return c.getConstructor().newInstance().getName();
        } catch (InvocationTargetException|InstantiationException|IllegalAccessException|NoSuchMethodException e) {
            return null;
        }}).filter(Objects::nonNull).toArray(String[]::new);
    }
}
