package addtool.feedback;

import addtool.add.model.*;
import addtool.analysisonadd.ADDVisitor;
import addtool.helpers.TranslationTable;

import java.util.ArrayList;
import java.util.Collection;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Feedback for output to PRISM.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 07.10.2020
 */
public class FeedbackPRISM implements FeedbackGenerator {

    @Override
    public Collection<FeedbackTuple> getFeedback(ADD model) {
        Collection<FeedbackTuple> feedback = new ArrayList<>();
        model.accept(new ADDVisitor() {

            @Override
            public void visit(BasicEventPlayer basicEvent) {

            }

            @Override
            public void visit(BasicEventTime basicEvent) {

            }

            @Override
            public void visit(And and) {

            }

            @Override
            public void visit(Or or) {

            }

            @Override
            public void visit(SAnd sAnd) {

            }

            @Override
            public void visit(SOr sOr) {

            }

            @Override
            public void visit(If ifGate) {

            }

            @Override
            public void visit(Not not) {

            }

            @Override
            public void visit(NotTrue not) {

            }

            @Override
            public void visit(Nat nat) {

            }

            @Override
            public void visit(Ivr ivr) {

            }

            @Override
            public void visit(Trigger trigger) {

            }

            @Override
            public void visit(Reset vertex) {
                feedback.add(new FeedbackTuple(vertex, FeedbackTuple.MESSAGE_TYPE.ERROR, getResource("prism_feedback_reset_not_supported")));
            }

            @Override
            public void visit(Cost vertex) {

            }

            @Override
            public void visit(Countermeasure vertex) {

            }
        });

        //Translation to games can be very slow, TODO remove these information if performance improves
        if (model.getId2Vertex().size() > 20 && model.getId2Vertex().size() < 30) {
            feedback.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.INFORMATION, getResource("prism_feedback_size_slow")));
        }
        if (model.getId2Vertex().size() >= 30) {
            feedback.add(new FeedbackTuple(FeedbackTuple.MESSAGE_TYPE.INFORMATION, getResource("prism_feedback_size_not_finish")));
        }

        return feedback;
    }

    @Override
    public String getName(){
        return TranslationTable.PRISM;
    }
}
