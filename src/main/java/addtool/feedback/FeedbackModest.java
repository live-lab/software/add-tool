package addtool.feedback;

import addtool.add.model.ADD;
import addtool.add.model.BasicEventPlayer;
import addtool.add.model.Vertex;
import addtool.helpers.TranslationTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import static addtool.helpers.preferences.LanguageSupport.getResource;

/**
 * Feedback for MODEST.
 * @author Julia Eisentraut julia.kraemer@in.tum.de
 * @since 07.10.2020
 */
public class FeedbackModest implements FeedbackGenerator {
    @Override
    public Collection<FeedbackTuple> getFeedback(ADD model) {
        Set<Vertex> bes = model.getBasicEvents();
        Collection<FeedbackTuple> feedback = new ArrayList<>();

        for (Vertex v : bes) {
            if (v instanceof BasicEventPlayer) {
                feedback.add(new FeedbackTuple(v, FeedbackTuple.MESSAGE_TYPE.WARNING, getResource("modest_feedback_beplayer")));
            }
        }
        return feedback;
    }
    @Override
    public String getName(){
        return TranslationTable.MODEST;
    }

}
