package addtool.feedback;

import addtool.add.model.Vertex;

import java.awt.*;
import java.util.Objects;

/**
 * A class to handle Tuple of format Data.
 * Feedback tuples contain a message type (error, warning, information or ok), a text (detailed description of the message) and a vertex or an edge, which the feedback refers to
 * @author Florian Dorfhuber florian.dorfhuber@in.tum.des
 * @since 02.10.2020
 */
public class FeedbackTuple implements Comparable<FeedbackTuple> {
    //only two vertices if Feedback is for edge
    private Vertex v1;
    private Vertex v2;

    /**
     * generate feedback tuple for vertex.
     *
     * @param v1   vertex with problem
     * @param type type of error message
     * @param text tooltip text
     */
    public FeedbackTuple(Vertex v1, MESSAGE_TYPE type, String text) {
        this(v1, null, type, text);
    }

    private boolean isEdge;
    private boolean isGeneral = false;

    /**
     * generate feedback tuple for general feedback.
     *
     * @param type The {@link MESSAGE_TYPE} for the tuple
     * @param text A message further explaining the event
     */
    public FeedbackTuple(MESSAGE_TYPE type, String text) {
        this(null, null, type, text);
    }

    private MESSAGE_TYPE type;
    private String text;

    /**
     * generate feedback tuple for edge (or vertex if v2==null).
     *
     * @param v1 A first vertex for the edge
     * @param v2 A second vertex for the edge. Can be null
     * @param type {@link MESSAGE_TYPE}
     * @param text A message further explaining the event
     */
    public FeedbackTuple(Vertex v1, Vertex v2, MESSAGE_TYPE type, String text) {
        this.v1 = v1;
        this.v2 = v2;
        this.type = type;
        this.text = text;

        isEdge = this.v2 != null;
        if (v1 == null && v2 == null) {
            this.isGeneral = true;
        }

    }

    /**
     * Updates the target vertex
     * @param v2 the new vertex to reference
     */
    public void setV2(Vertex v2) {
        this.v2 = v2;
        isEdge = this.v2 != null;
    }

    /**
     * Checks if the tuple references an edge. i.e. the second vertex is not null
     * @return true if it references an edge
     */
    public boolean isEdge() {
        return isEdge;
    }

    /**
     * Get the first vertex
     * @return a vertex referenced in this tuple
     */
    public Vertex getV1() {
        return v1;
    }

    /**
     * Updates the source vertex
     * @param v1 the new vertex to reference
     */
    public void setV1(Vertex v1) {
        this.v1 = v1;
    }

    /**
     * Get the second vertex
     * @return a vertex referenced in this tuple, null if it is not an edge
     */
    public Vertex getV2() {
        return v2;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof FeedbackTuple && compareTo((FeedbackTuple) o) == 0;
    }

    @SuppressWarnings("ObjectInstantiationInEqualsHashCode")
    @Override
    public int hashCode() {
        return Objects.hash(v1, v2, isEdge, isGeneral, type, text);
    }

    /**
     * Levels of messages
     */
    public enum MESSAGE_TYPE {
        ERROR(Color.RED),
        WARNING(Color.ORANGE),
        INFORMATION(Color.GRAY),
        OK(Color.GREEN);
        private final Color color;

        MESSAGE_TYPE(Color c) {
            color = c;
        }

        /**
         * Get the color for the message type. e.g. red for errors
         * @return a color
         */
        public Color getColor() {
            return color;
        }
    }

    /**
     * Returns the message type or log level for this tuple
     * @return the messagte type
     * @see MESSAGE_TYPE
     */
    public MESSAGE_TYPE getType() {
        return type;
    }

    /**
     * Updates the message type of the tuple
     * @param type the new message type
     * @see MESSAGE_TYPE
     */
    public void setType(MESSAGE_TYPE type) {
        this.type = type;
    }

    /**
     * Returns the message of the tuple
     * @return the message
     */

    public String getText() {
        return text;
    }

    /**
     * Update the message text
     * @param text the new message
     */
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int compareTo(FeedbackTuple o) {
        if (o == null) return -99;

        return this.getType().toString().compareTo((o).getType().toString());
    }

    /**
     * Returns if the feedback is general i.e. not referencing a vertex
     * @return true if no vertex is involved
     */
    public boolean isGeneral() {
        return isGeneral;
    }
}

