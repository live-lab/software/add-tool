package addtool.feedback;

import addtool.add.model.*;
import addtool.analysisonadd.ADDVisitor;

import java.util.Collection;

import static addtool.helpers.preferences.LanguageSupport.getResource;

public class XMLVisitor implements ADDVisitor {

    private final Collection<FeedbackTuple> sandAT;
    private final Collection<FeedbackTuple> adt;

    //Assignment musst be directly as we rely on these exact collections on the outside world
    @SuppressWarnings("AssignmentOrReturnOfFieldWithMutableType")
    public XMLVisitor(Collection<FeedbackTuple> sandAT, Collection<FeedbackTuple> adt) {
        this.sandAT = sandAT;
        this.adt = adt;
    }

    /*
    @Override
    public void visit(Vertex vertex) {
        if (vertex instanceof SOr || vertex instanceof BasicEventTime || vertex instanceof Cost || vertex instanceof If
                || vertex instanceof Ivr || vertex instanceof Nat || vertex instanceof Not || vertex instanceof Reset
                || vertex instanceof Trigger) {
            FeedbackTuple f = new FeedbackTuple(vertex, FeedbackTuple.MESSAGE_TYPE.ERROR, "Gate " + vertex.getName() + " is not allowed in XML format.");
            sandAT.add(f);
            adt.add(f);
        }
        if (vertex instanceof SAnd) {
            adt.add(new FeedbackTuple(vertex, FeedbackTuple.MESSAGE_TYPE.WARNING, "The ADTool 2.0 can only process SAND nodes in SAND attack trees (and thus, not in combination with defenses)"));
        }
        if (vertex instanceof BasicEventPlayer && ((BasicEventPlayer) vertex).isDefender()) {
            sandAT.add(new FeedbackTuple(vertex, FeedbackTuple.MESSAGE_TYPE.WARNING, "The ADTool 2.0 can only process defenses in AND-OR-Trees."));
        }
    } */

    @Override
    public void visit(BasicEventPlayer basicEvent) {
        if (basicEvent.isDefender()) {
            sandAT.add(new FeedbackTuple(basicEvent, FeedbackTuple.MESSAGE_TYPE.WARNING, getResource("adtool_feedback_defender")));
        }
    }

    @Override
    public void visit(BasicEventTime basicEvent) {
        FeedbackTuple f = getGateTuple(basicEvent);
        sandAT.add(f);
        adt.add(f);
    }

    @Override
    public void visit(And and) {

    }

    @Override
    public void visit(Or or) {

    }

    @Override
    public void visit(SAnd sAnd) {
        adt.add(new FeedbackTuple(sAnd, FeedbackTuple.MESSAGE_TYPE.WARNING, getResource("adtool_feedback_sand")));
    }

    @Override
    public void visit(SOr sOr) {

    }
    private FeedbackTuple getGateTuple(Vertex gate){
        return new FeedbackTuple(gate, FeedbackTuple.MESSAGE_TYPE.ERROR,
                getResource("adtool_feedback_gate")+" " + gate.getName() +
                        " "+getResource("adtool_feedback_gate_not_allowed"));
    }
    @Override
    public void visit(If ifGate) {
        FeedbackTuple f = getGateTuple(ifGate);;
        sandAT.add(f);
        adt.add(f);
    }

    @Override
    public void visit(Not not) {
        FeedbackTuple f = getGateTuple(not);
        sandAT.add(f);
        adt.add(f);
    }

    @Override
    public void visit(NotTrue not) {
        //This will be converted to countermeasure
    }

    @Override
    public void visit(Nat nat) {
        FeedbackTuple f = getGateTuple(nat);
        sandAT.add(f);
        adt.add(f);
    }

    @Override
    public void visit(Ivr ivr) {
        FeedbackTuple f = getGateTuple(ivr);
        sandAT.add(f);
        adt.add(f);
    }

    @Override
    public void visit(Trigger trigger) {
        FeedbackTuple f = getGateTuple(trigger);
        sandAT.add(f);
        adt.add(f);
    }

    @Override
    public void visit(Reset reset) {
        FeedbackTuple f = getGateTuple(reset);
        sandAT.add(f);
        adt.add(f);
    }

    @Override
    public void visit(Cost vertex) {
        FeedbackTuple f = getGateTuple(vertex);
        sandAT.add(f);
        adt.add(f);
    }

    @Override
    public void visit(Countermeasure vertex) {

    }
}
