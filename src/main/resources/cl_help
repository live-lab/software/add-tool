Usage: add-tool.jar [Task] ...
    no argument                     Starts the graphical user interface

    -a [mode] [file] [options]      Analysis on Diagram
        prob                        Creates a topdown list of all Vertices with their Success-Probability
        cost                        Creates a topdown list of all Vertices with their Cost
        |---pac                     Include Uncertainty and Delta
        |---root                    Only analyses all root nodes
        |---srt                     Saves runtime to stat.csv (data will be appended)

    -c [mode] [infile] [outfile] [options]
                                    Converts diagram-files in other formats
        dot2xml                     Converts dot-file to xml-file for use in ADT-Tool
        dot2prism                   Converts dot Diagram in PRISM-Game
        dot2modest                  Converts dot Diagram in MODEST
        dot2semantics               Converts dot Diagram in Semantics
        dot2uppaal               	Converts dot Diagram in Uppaal
        dot2csv                     Converts dot Diagram in CSV for metric-analysis on game
        xml2dot                     Converts xml in dot
        auto                        default option if no mode is given. Filename-endings are used to determine command
        |---srt                     Saves runtime to stat_c.csv (data will be appended)
        |---timeout                 Sets conversion timeout in ms [default: timeout=3000]
        |---[query]                 Uppaal only either SUCCESS, FAILURE, BOTH to determine which query should be written

    -f [mode] [file]                Gives feedback on a model (mode is not case-sensitive)
        MODEST                      Feedback on a conversion to MODEST
        UPPAAL                      Feedback on a conversion to UPPAAL
        XML                         Feedback on a conversion to XML
        PRISM                       Feedback on a conversion to PRISM

    -g [target-folder] [options]     Generates a bunch of diagrams in the target folder
        advjoin                     The rate of cross-links in generation [default: advjoin=0.1]
        maxpred                     Maximum number of Predecessors per Vertex [default: maxpred=4]
        maxsuc                      Maximum number of Successors per Vertex [default: maxpred=3]
        maxcost                     Maximum cost per Basic Event [default: maxcost=100]
        numb                        Number of BasicEvents and cycles. Total Number of ADDs is n² [default: numb=40]
        maxnumb                     Maximum Number of nodes in an ADD. Execution will abort after reaching it [default: maxnumb=1200]
        minnumb                     Minimum Number of nodes in an ADD. All models below this line will not be saved [default: minnumb=0]
        be                          The code for the set of Basic Events [default: be=3]
                                    1=Time; 2=Attacker; 3=Attacker & Defender; 4=Time & Attacker; 5=All;
        ops                         The operator code upper bound (excluded) [default: ops=2]
                                    0=AND; 1=OR; 2=SAND; 3=SOR; 4=NOT; 5=NAT; 11=AND,OR,NOT

    -h                              Help (displays this message)

    -p [file] [datafile]  [options] Calculates PAC-estimates for a value. Datafile denotes a csv with the data to analyse
        model                       The Model to calculate available are gauss [Gaussian] and arima [ARIMA] [default: model=arima]
        type                        The type of information gained cost, prob, delay [default: type=prob]
        divid                       Dividend column can be left out if datafile only has one column
        divis                       Divisor column (optional)
        delta                       Probability delta [default: delta=0.05]
        id                          The BasicEvent ID to assign the values to
        timeout                     The Operations timeout in seconds [default: timeout=180]

    -l [infile] [outfile] [options] Executes the learning algorithm for adt (currently GA). Inputs either .dot or .trace files
        seed                        The seed for all random processes
        epochs                      Number of generations that should be done [default: epochs=50]
        pool                        The number of models that are passed to the next generation [default: pool=20]
        width                       The number of change cycles per epoch [default: width=30]
        actions                     The maximum number of actions performed on a model in a cycle [default: actions=7]
        mutation                    The mutation rate as double. Cross-over rate ist 1-mutation rate. [default: mutation=0.5]
        epsilon                     The amount of random models per generation. [default: epsilon=5]
        traces                      The amount of sequences that should be generated (only applicable in .dot files) [default: traces=1000]
        train                       The splitting between train/test set. On 0 or 1 there will be now difference between them. [default: train=0.8]
        meta                        If meta-data should be put out. This includes accuracy data and csv-files [default: meta=0]
        er                          If epoch-results should be put out. [default: er=0]
        outputtraces                If traces should be put out. [default: outputtraces=0]
        ordered                     If the sequences should be ordered if compared for uniqueness [default: ordered=0]
        success                     The number of successes in generating sequences. -1 deactivates the bound [default: success=-1]
        successrate                 The rate successes in generating sequences. -1 deactivates the bound [default: successrate=-1]
        suppresswarnings            Suppresses the warning if to less sequences could be generated [default: suppresswarnings=0]
        showeditor                  If the editor should be started after finishing learning. Always disabled in bulk-mode [default: showeditor=1]
        timeout                     A timeout in minutes for the current call. -1 deactivates it [default: timeout=-1]
                                    After the timeout the algorithm will finish the current file. After 1.2*timeout it will shutdown hard.
        weight                      The weight (double 0<=weight<=1) for successes in the fitness-function. The weight for failures is 1-weight.
                                    Select -1 if you want to only use accuracy [default: weight=-1]
        ops                         The set of operators allowed to use. [default: ops=1]
                                    1=AND,OR; 2=AND,OR,NOT; 3=AND,OR,SAND,SOR; 4=AND,OR,SAND,SOR,NAT; 5=AND,OR,NOT,SAND,SOR,NAT;
    -le [infile] [data] [target]    Evaluates a model on a given Set of constraints (Sensitivity and Specificity)
    -lv [infile] [outfile] [options]Does cross-validation on models only use on .dot options as in -l



The MIT License (MIT)
Copyright (c) 2021
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction,